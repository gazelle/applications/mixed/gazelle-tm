# Gazelle Test Management

<!-- TOC -->
* [Gazelle Test Management](#gazelle-test-management)
  * [Build and tests](#build-and-tests)
  * [Installation manual](#installation-manual)
    * [Configuration for sso-client-v7](#configuration-for-sso-client-v7)
  * [Administration Guide](#administration-guide)
  * [User manual](#user-manual)
  * [Release note](#release-note)
<!-- TOC -->

## Build and tests

The project must be build using JDK-8 and run on a JDK-7 environment.

```shell
mvn clean install
```

## Installation manual

The Gazelle Test Management installation manual is available here: 
https://gazelle.ihe.net/gazelle-documentation/Test-Management/installation.html

### Configuration for sso-client-v7

As Gazelle Test Management depends on sso-client-v7, it needs to be configured properly to work. You can follow the README of sso-client-v7 
available here: [sso-client-v7](https://gitlab.inria.fr/gazelle/library/sso-client-v7)

## Administration Guide

The Gazelle Test Management administration guide is available here: 
https://gazelle.ihe.net/gazelle-documentation/Test-Management/admin.html

## User manual

The Gazelle Test Management user manual is available here:
https://gazelle.ihe.net/gazelle-documentation/Test-Management/user.html

## Release note

The Gazelle TM release note is available here: https://gazelle.ihe.net/gazelle-documentation/Test-Management/release-note.html