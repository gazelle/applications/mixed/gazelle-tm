# Library used for TM

## JavaScript

### Chart.js 2.9.4

Added on august 2022

This library is used for the pie charts on the home page.

Link to documentation : https://www.chartjs.org/docs/2.9.4/

### chartjs-plugin-datalabels 1.0.0

Added on september 2022

Requirement : Chart.js >= 2.7.0 <3.X.X


This library is used to show the value of the pie charts slices without hovering it.

Link to documentation : https://v1_0_0--chartjs-plugin-datalabels.netlify.app/guide/