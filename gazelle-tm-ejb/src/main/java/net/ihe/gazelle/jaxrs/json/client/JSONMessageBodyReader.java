package net.ihe.gazelle.jaxrs.json.client;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyReader;
import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

/**
 * Resteasy Json message body reader.
 */
public class JSONMessageBodyReader implements MessageBodyReader<Object> {

    @Override
    public boolean isReadable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        // returns true if media type is application/json
        if (mediaType == null) {
            return false;
        } else if (String.class.equals(type)) {
            return false;
        } else {
            String subtype = mediaType.getSubtype();
            boolean isApplicationMediaType = "application".equals(mediaType.getType());
            return isApplicationMediaType && "json".equalsIgnoreCase(subtype) || subtype.endsWith("+json") || subtype.equalsIgnoreCase("x-ndjson") || mediaType.isWildcardSubtype() && (mediaType.isWildcardType() || isApplicationMediaType);
        }
    }

    @Override
    public Object readFrom(Class<Object> type, Type genericType, Annotation[] annotations, MediaType mediaType,
                           MultivaluedMap<String, String> httpHeaders, InputStream entityStream) throws IOException {
        // delegates to ObjectMapperInstance
        return ObjectMapperInstance.readFrom(type, genericType, entityStream);
    }
}
