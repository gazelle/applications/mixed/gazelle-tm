package net.ihe.gazelle.jaxrs.json.client;

import java.util.Objects;

/**
 * JAXRS Client key (for caching)
 */
class ClientKey {

    private final Class<?> contract;

    private final String baseUrl;

    /**
     * New Client key.
     *
     * @param contract the JAXRS contract
     * @param baseUrl  the server base url
     */
    ClientKey(Class<?> contract, String baseUrl) {
        this.contract = contract;
        this.baseUrl = baseUrl;
    }

    /**
     * Gets contract.
     *
     * @return the contract
     */
    Class<?> getContract() {
        return contract;
    }

    /**
     * Gets base url.
     *
     * @return the base url
     */
    String getBaseUrl() {
        return baseUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ClientKey clientKey = (ClientKey) o;

        if (!Objects.equals(contract, clientKey.contract)) return false;
        return Objects.equals(baseUrl, clientKey.baseUrl);
    }

    @Override
    public int hashCode() {
        int result = contract != null ? contract.hashCode() : 0;
        result = 31 * result + (baseUrl != null ? baseUrl.hashCode() : 0);
        return result;
    }
}
