/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.users.action;

import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import net.ihe.gazelle.ssov7.authn.interlay.adapter.GazelleIdentityImpl;
import net.ihe.gazelle.ssov7.gum.client.application.User;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.security.Credentials;
import org.jboss.seam.security.Identity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

/**
 * <b>Class Description : </b>Authentication Module<br>
 * <br>
 * This class authenticates a user with the Gazelle application.This module check that the login/password are stored in the database, and grant the
 * access depending of the user's role.
 *
 * @author Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @version 1.0 - 2008, April 28
 * @class Authenticator.java
 * @package net.ihe.gazelle.common.authentication
 * @see > Jchatel@irisa.fr - http://www.ihe-europe.org
 */
@Name("authenticator")
public class Authenticator {
    // ~ Attributes ////////////////////////////////////////////////////////////////////////////////////////////////////

    private static final Logger LOG = LoggerFactory.getLogger(Authenticator.class);

    /**
     * Identity to get informations about the user in the session context
     */
    @In(create = true)
    GazelleIdentity identity;

    @In
    Credentials credentials;

    /**
     * entityManager is the interface used to interact with the persistence context.
     */
    @In
    private EntityManager entityManager;

    private User selectedUser;

    private List<User> users;

    /**
     * Activation Code sent by the user, clicking on the activation URL in the email sent after new account registration
     */
    @In(required = false)
    private String activationCode;

    /**
     * ChangePasswordCode sent by the user, clicking on the changePassword URL in the email sent after a password assistance (password lost)
     */
    private String changePasswordCode;

    // ~ Methods ///////////////////////////////////////////////////////////////////////////////////////////////////////

    public static void resetSession(User user) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("void resetSession");
        }
        Contexts.getSessionContext().set("selectedUser", user);
    }

//    /**
//     * Get the user with the same activation code, waiting for an activation
//     *
//     * @return User : user account to activate
//     */
//    public static GumUser findUserWithActivationCode(String code) {
//        if (LOG.isDebugEnabled()) {
//            LOG.debug("User findUserWithActivationCode");
//        }
//        GumUserQuery query = new GumUserQuery();
//        query.activationCode().eq(code);
//        List<User> foundUsers = query.getList();
//
//        if (foundUsers.size() != 1) {
//            LOG.error("-----------------------------------------------------------------------------");
//            LOG.error("None or several USERS found with the searched activation code ! ONE IS REQUIRED ! FATAL Error ! FOUND USERS = "
//                    + foundUsers.size());
//
//            for (int l = 0; l < foundUsers.size(); l++) {
//                LOG.error(l + " - User = Id(" + foundUsers.get(l).getId() + ") " + foundUsers.get(l).getUsername());
//
//            }
//
//            LOG.error("-----------------------------------------------------------------------------");
//
//            return null;
//        }
//
//        return foundUsers.get(0);
//    }

    /**
     * Get the activation code - code in the email sent after a user account registration
     *
     * @return String activationCode
     */
    public String getActivationCode() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getActivationCode");
        }
        return activationCode;
    }

    /**
     * Set the activation code - code in the email sent after a user account registration
     *
     * @param activationCode activationCode
     */
    public void setActivationCode(String activationCode) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("setActivationCode");
        }
        this.activationCode = activationCode;
    }

    /**
     * Set Roles and access levels to the given User
     *
     * @param user the current user
     */
    private void setRolesAndAccessLevels(final User user, GazelleIdentityImpl identitySeam) {
        List<String> foundRoles = new ArrayList<>(user.getGroupIds());
        for (int i = 0; i < foundRoles.size(); i++) {
            identitySeam.addRole(foundRoles.get(i));
        }
    }

//    /**
//     * Active a new user account (after the email validation) for the Gazelle application We check if this code is waiting for an activation, and
//     * we active the associated account
//     *
//     * @return String : determinate where to redirect the user (check your pages.xml file)
//     */
//    public String activate() {
//        if (LOG.isDebugEnabled()) {
//            LOG.debug("activate");
//        }
//
//        GumUser user = findUserWithActivationCode(activationCode);
//
//        if (user != null) {
//            user.setActivated(true);
//
//            entityManager.merge(user);
//            entityManager.flush();
//
//            Institution activatedInstitution = entityManager.find(Institution.class, user.getInstitution().getId());
//            activatedInstitution.setActivated(true);
//            entityManager.persist(activatedInstitution);
//
//            users = User.getUsersFiltered(activatedInstitution, null);
//
//            return "activated";
//
//        } else {
//            return "notFound";
//        }
//    }

//    /**
//     * @TODO JBM Sould be executed unlogged
//     */
//    public String editUserWithActivationCode() {
//        if (LOG.isDebugEnabled()) {
//            LOG.debug("editUserWithActivationCode");
//        }
//
//        selectedUser = findUserWithActivationCode(activationCode);
//
//        if (selectedUser != null) {
//            Contexts.getSessionContext().set("selectedUser", selectedUser);
//            return "changePassword";
//
//        } else {
//
//            return "notFound";
//        }
//
//    }

    /**
     * Logout method - used to reset roles
     *
     * @return String : loggedOut
     */

    public String logout() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("logout");
        }
        Identity.instance().logout();

        return "loggedOut";
    }
}
