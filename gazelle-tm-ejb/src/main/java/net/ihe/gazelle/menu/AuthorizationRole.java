package net.ihe.gazelle.menu;

import net.ihe.gazelle.common.pages.Authorization;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import org.jboss.seam.Component;

public class AuthorizationRole implements Authorization {

    private final String role;

    private final GazelleIdentity identity = (GazelleIdentity) Component.getInstance("org.jboss.seam.security.identity");

    public AuthorizationRole(String role) {
        super();
        this.role = role;
    }

    @Override
    public boolean isGranted(Object... context) {
        return identity.hasRole(role);
    }

}
