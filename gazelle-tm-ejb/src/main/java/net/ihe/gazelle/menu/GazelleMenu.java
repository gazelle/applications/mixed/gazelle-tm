package net.ihe.gazelle.menu;

import net.ihe.gazelle.common.pages.menu.Menu;
import net.ihe.gazelle.common.pages.menu.MenuEntry;
import net.ihe.gazelle.common.pages.menu.MenuGroup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class GazelleMenu {

    private static final Logger LOG = LoggerFactory.getLogger(GazelleMenu.class);
    private static MenuGroup MENU = null;

    private static GazelleMenu instance = new GazelleMenu();

    private GazelleMenu() {
        super();
    }

    public static synchronized GazelleMenu getInstance(){
        return instance;
    }

    public MenuGroup getMenu() {

        if (LOG.isDebugEnabled()) {
            LOG.debug("final MenuGroup getMenu");
        }
        if (MENU == null) {
            ArrayList<Menu> children = new ArrayList<Menu>();

            children.add(new MenuEntry(Pages.PR_INTRO));
            children.add(new MenuEntry(Pages.PR_SEARCH));
            children.add(new MenuEntry(Pages.PR_STATS));

            children.add(new MenuEntry(Pages.HOME_PAGE));
            children.add(getTechnicalFramework());
            children.add(new MenuEntry(Pages.SINGLE_REGISTRATION));
            children.add(getTestSession());
            children.add(getPreparation());
            children.add(getTesting());
            children.add(new MenuEntry(Pages.CAT_RESULTS));
            children.add(getManageRegistry());
            children.add(getAdministration());

            MENU = new MenuGroup(null, children);
        }
        return MENU;
    }

    public  final MenuGroup refreshMenu() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("final MenuGroup refreshMenu");
        }
        MENU = null;
        return getMenu();
    }

    private  Menu getTechnicalFramework() {
        List<Menu> children = new ArrayList<Menu>();

        children.add(new MenuEntry(Pages.TF_SPECIFICATIONS));
        children.add(new MenuEntry(Pages.TF_OVERVIEW));
        children.add(new MenuEntry(Pages.TF_DOMAINS));
        children.add(new MenuEntry(Pages.TF_PROFILES));
        children.add(new MenuEntry(Pages.TF_ACTORS));
        children.add(new MenuEntry(Pages.TF_OPTIONS));
        children.add(new MenuEntry(Pages.TF_TRANSACTIONS));
        children.add(new MenuEntry(Pages.TF_RULES));
        children.add(new MenuEntry(Pages.TF_STANDARDS));
        children.add(new MenuEntry(Pages.TF_DOCUMENTS));
        children.add(new MenuEntry(Pages.TF_TRANSACTIONS_OPTION_TYPES));
        children.add(new MenuEntry(Pages.TF_AUDIT_MESSAGES));
        children.add(new MenuEntry(Pages.TF_HL7V2_MESSAGE_PROFILES));
        children.add(new MenuEntry(Pages.TF_WEBSERVICE_TRANSACTION_USAGE));
        children.add(new MenuEntry(Pages.TF_CONF_DEPENDING_AIPO));
        children.add(new MenuEntry(Pages.ADMIN_CHECK_TF));

        MenuGroup result = new MenuGroup(Pages.TF, children);
        return result;

    }

    private Menu getTestSession(){
        List<Menu> children = new ArrayList<Menu>();

        //FIXME: Opens the edit session page with an empty form, we want to preload the information about
        //the currently selected session. But the information you're loooking for are in another bean

        //children.add(new MenuEntry(Pages.SYSTEMS_SESSION_EDITSESSION));
        children.add(new MenuEntry(Pages.REGISTRATION_PARTICIPANTS));
        children.add(new MenuEntry(Pages.REGISTRATION_SYSTEMS));
        children.add(new MenuEntry(Pages.REGISTRATION_OVERVIEW));
        children.add(new MenuEntry(Pages.ADMIN_MANAGE_SYSTEMS_TEST_TYPES));
        children.add(new MenuEntry(Pages.ADMIN_MANAGE_MONITORS));
        children.add(new MenuEntry(Pages.ADMIN_MANAGE_INVOICES));
        children.add(new MenuEntry(Pages.ADMIN_CHECK_SESSION));
        children.add(new MenuEntry(Pages.ADMIN_KPI_TESTSINSTANCES));
        children.add(new MenuEntry(Pages.ADMIN_KPI_TESTS));
        children.add(new MenuEntry(Pages.ADMIN_KPI_SYSTEMS));
        children.add(new MenuEntry(Pages.ADMIN_KPI_MONITORS));

        return new MenuGroup(Pages.TEST_SESSION, children);
    }

    private Menu getPreparation() {
        List<Menu> children = new ArrayList<Menu>();

        children.add(new MenuEntry(Pages.CONFIG_NETWORK_OVERVIEW));
        children.add(new MenuEntry(Pages.CONFIG_LIST_VENDOR));
        children.add(new MenuEntry(Pages.ADMIN_MANAGE_CONFIGS_HOSTS));
        children.add(new MenuEntry(Pages.CONFIG_ALL));
        children.add(new MenuEntry(Pages.ADMIN_CHECK_CONF));
        children.add(new MenuEntry(Pages.CONFIG_CERTIFICATES));
        children.add(new MenuEntry(Pages.CONFIG_OIDS));
        children.add(new MenuEntry(Pages.ADMIN_MANAGE_OIDS_LABELS));
        children.add(new MenuEntry(Pages.ADMIN_MANAGE_CONFIGS_OIDS));
        children.add(new MenuEntry(Pages.CAT_PRECAT));

        return new MenuGroup(Pages.PREPARATION, children);
    }

    private Menu getTesting() {

        List<Menu> children = new ArrayList<Menu>();

        children.add(new MenuEntry(Pages.CAT_MONITOR_WORKLIST));
        children.add(new MenuEntry(Pages.TEST_EXECUTION));
        children.add(new MenuEntry(Pages.CAT_INSTANCES));
        children.add(new MenuEntry(Pages.CAT_SAMPLES_SEARCH));
        children.add(new MenuEntry(Pages.CAT_SAMPLES));
        children.add(new MenuEntry(Pages.ADMIN_REGISTRATION_OVERVIEW));
        children.add(new MenuEntry(Pages.SYSTEM_LIST_ALL));
        children.add(new MenuEntry(Pages.CAT_MONITORS));
        children.add(new MenuEntry(Pages.TD_LIST_TESTS));
        children.add(new MenuEntry(Pages.TD_TEST_REQUIREMENTS));
        children.add(new MenuEntry(Pages.TD_LIST_METATESTS));
        children.add(new MenuEntry(Pages.TD_ROLE_IN_TEST));
        children.add(new MenuEntry(Pages.ADMIN_SAMPLES_OBJECT_TYPE));
        children.add(new MenuEntry(Pages.ADMIN_CHECK_TD));


        return new MenuGroup(Pages.TESTING, children);

    }

    private Menu getManageRegistry() {

        List<Menu> children = new ArrayList<Menu>();

        children.add(new MenuEntry(Pages.CAT_SYSTEMS));
        children.add(new MenuEntry(Pages.ADMIN_PR_VALIDATE));
        children.add(new MenuEntry(Pages.ADMIN_PR_CRAWLER));

        return new MenuGroup(Pages.MANAGE_REGISTRY, children);

    }

    private  Menu getAdministration() {
        List<Menu> children = new ArrayList<Menu>();

        children.add(new MenuEntry(Pages.REGISTRATION_COMPANY));
        children.add(new MenuEntry(Pages.ADMIN_MANAGE_USERS));
        children.add(new MenuEntry(Pages.ADMIN_MANAGE_SESSIONS));
        children.add(new MenuEntry(Pages.ADMIN_MANAGE_COMPANIES));
        children.add(new MenuEntry(Pages.ADMIN_MANAGE_CONTACTS));
        children.add(new MenuEntry(Pages.ADMIN_PREFERENCES));
        children.add(new MenuEntry(Pages.ADMIN_USAGE));
        children.add(new MenuEntry(Pages.ADMIN_MANAGE_HOME));

        return new MenuGroup(Pages.ADMIN, children);
    }

    private  Menu getHome() {
        List<Menu> children = new ArrayList<Menu>();
        return new MenuGroup(Pages.HOME, children);
    }

    private  Menu getRegistration() {
        List<Menu> children = new ArrayList<Menu>();

        children.add(new MenuEntry(Pages.REGISTRATION_SYSTEMS));
        children.add(new MenuEntry(Pages.REGISTRATION_CONTACTS));

        children.add(new MenuEntry(Pages.REGISTRATION_FINANCIAL));
        children.add(new MenuEntry(Pages.REGISTRATION_DEMONSTRATIONS));

        return new MenuGroup(Pages.REGISTRATION, children);
    }

        private  Menu getTestDefinitions() {
        List<Menu> children = new ArrayList<Menu>();

        return new MenuGroup(Pages.TD_LIST_TESTS, children);

    }

    private  Menu getConfigurations() {
        List<Menu> children = new ArrayList<Menu>();

        children.add(new MenuEntry(Pages.CONFIG_ALL));

        return new MenuGroup(Pages.CONFIG, children);
    }

    private  Menu getConnectathon() {
        List<Menu> children = new ArrayList<Menu>();

        children.add(getPreCatSubMenu());
        children.add(getCatSubMenu());
        children.add(getInteroperabilitySubMenu());

        return new MenuGroup(Pages.CAT, children);
    }

    private  Menu getPreCatSubMenu() {
        List<Menu> children = new ArrayList<Menu>();
        addPreCatEntries(children);

        return new MenuGroup(Pages.CAT_PRECAT_MENU, children);
    }

    private void addPreCatEntries(List<Menu> children) {

    }

    private  Menu getInteroperabilitySubMenu() {
        List<Menu> children = new ArrayList<Menu>();
        addInteroperabilityEntries(children);

        return new MenuGroup(Pages.INTEROP_MENU, children);
    }

    private  void addInteroperabilityEntries(List<Menu> children) {
        children.add(new MenuEntry(Pages.INTEROP_DASHBOARD));
        children.add(new MenuEntry(Pages.INTEROP_INSTANCES));
        children.add(new MenuEntry(Pages.INTEROP_RESULTS));
        children.add(new MenuEntry(Pages.INTEROP_REPORT));
    }

    private  Menu getCatSubMenu() {
        List<Menu> children = new ArrayList<Menu>();
        addCatEntries(children);

        return new MenuGroup(Pages.CAT_MENU, children);
    }

    private  void addCatEntries(List<Menu> children) {
        children.add(new MenuEntry(Pages.CAT_REPORT_VENDOR));
    }

    private  Menu getAdministrationManage() {
        List<Menu> children = new ArrayList<Menu>();



        children.add(getAdministrationManageSystems());
        children.add(getAdministrationManageConfigs());
        children.add(getAdministrationSamples());


        children.add(getAdministrationManageMonitors());



        children.add(new MenuEntry(Pages.ADMIN_MANAGE_FINANCIAL_SUMMARY));

        return new MenuGroup(Pages.ADMIN_MANAGE, children);
    }

    private  Menu getAdministrationManageSystems() {
        List<Menu> children = new ArrayList<Menu>();

        children.add(new MenuEntry(Pages.REGISTRATION_SYSTEMS));
        children.add(new MenuEntry(Pages.ADMIN_MANAGE_SYSTEMS_LIST));

        return new MenuGroup(Pages.ADMIN_MANAGE_SYSTEMS, children);
    }

    private  Menu getAdministrationManageConfigs() {
        List<Menu> children = new ArrayList<Menu>();

        return new MenuGroup(Pages.ADMIN_MANAGE_CONFIGS, children);
    }

    private  Menu getAdministrationCheck() {
        List<Menu> children = new ArrayList<Menu>();

        children.add(new MenuEntry(Pages.ADMIN_CHECK_REGISTRATION));

        return new MenuGroup(Pages.ADMIN_CHECK, children);
    }

    private  Menu getAdministrationSamples() {
        List<Menu> children = new ArrayList<Menu>();

        return new MenuGroup(Pages.ADMIN_SAMPLES, children);
    }

    private  Menu getAdministrationManageMonitors() {
        List<Menu> children = new ArrayList<Menu>();

        return new MenuGroup(Pages.ADMIN_MANAGE_MONITORS, children);
    }

}
