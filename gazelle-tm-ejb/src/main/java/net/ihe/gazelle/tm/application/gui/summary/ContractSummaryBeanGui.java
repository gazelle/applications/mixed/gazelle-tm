package net.ihe.gazelle.tm.application.gui.summary;

import net.ihe.gazelle.tm.session.TestingSessionService;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.security.Identity;

@Name("contractSummaryBeanGui")
@Scope(ScopeType.PAGE)
public class ContractSummaryBeanGui {

    private TestingSession currentTestingSession;

    @In(value = "testingSessionService",create = true)
    private TestingSessionService testingSessionService;

    /////////////////// init ///////////////////

    @Create
    public void init(){
        currentTestingSession = testingSessionService.getUserTestingSession();
    }

    /////////////////// Checkers ///////////////////

    public boolean showContractSummary(){
        return Identity.instance().isLoggedIn() && currentTestingSession.isContractRequired();
    }


    /////////////////// Links ///////////////////

    public String manageContractButton(){
        return "/financial/financialSummary.seam";
    }

}
