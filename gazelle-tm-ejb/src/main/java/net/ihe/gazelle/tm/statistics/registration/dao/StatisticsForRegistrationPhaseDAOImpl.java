package net.ihe.gazelle.tm.statistics.registration.dao;

import net.ihe.gazelle.tm.statistics.common.StatisticsCommonService;
import net.ihe.gazelle.tm.statistics.registration.StatisticsForRegistrationPhaseDAO;
import net.ihe.gazelle.tm.systems.model.SystemInSession;
import net.ihe.gazelle.tm.systems.model.SystemInSessionQuery;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import java.util.List;

@Scope(ScopeType.STATELESS)
@AutoCreate
@Name(value = "statisticsForRegistrationPhaseDAO")
public class StatisticsForRegistrationPhaseDAOImpl implements StatisticsForRegistrationPhaseDAO {

    @In(value = "statisticsCommonService")
    StatisticsCommonService statisticsCommonService;

    @Override
    public List<SystemInSession> getAllSystemForCurrentTestingSession() {
        SystemInSessionQuery systemInSessionQuery = statisticsCommonService.getSystemInSessionQuery();
        return systemInSessionQuery.getListDistinct();
    }

}
