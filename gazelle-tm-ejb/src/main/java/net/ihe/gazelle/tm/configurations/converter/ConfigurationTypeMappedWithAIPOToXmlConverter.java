package net.ihe.gazelle.tm.configurations.converter;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.tf.model.ActorIntegrationProfileOption;
import net.ihe.gazelle.tf.model.ActorIntegrationProfileOptionQuery;
import net.ihe.gazelle.tf.model.Transaction;
import net.ihe.gazelle.tf.model.WSTransactionUsage;
import net.ihe.gazelle.tm.configurations.model.*;
import net.ihe.gazelle.tm.configurations.model.export.ConfigurationTypeMappedWithAIPOIE;

import javax.persistence.EntityManager;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ConfigurationTypeMappedWithAIPOToXmlConverter {

    public String configurationsToXml(List<ConfigurationTypeMappedWithAIPO> aipoRules) throws JAXBException {

        ConfigurationTypeMappedWithAIPOIE aipoRulesIE = this.listToIE(aipoRules);

        JAXBContext jaxbContext = JAXBContext.newInstance(ConfigurationTypeMappedWithAIPOIE.class, ConfigurationTypeMappedWithAIPO.class,
                ConfigurationTypeWithPortsWSTypeAndSopClass.class, WebServiceType.class, WSTransactionUsage.class,
                SopClass.class, TransportLayer.class, ConfigurationType.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        StringWriter sw = new StringWriter();
        jaxbMarshaller.marshal(aipoRulesIE, sw);
        return sw.toString();
    }

    private ConfigurationTypeMappedWithAIPOIE listToIE(List<ConfigurationTypeMappedWithAIPO> configurationTypeMappedWithAIPOS){
        return new ConfigurationTypeMappedWithAIPOIE(configurationTypeMappedWithAIPOS);
    }

    public ConfigurationTypeMappedWithAIPOIE extractAndCheckConfigurations(String xmlString) throws JAXBException{
        EntityManager entityManager = EntityManagerService.provideEntityManager();

        ConfigurationTypeMappedWithAIPOIE configurationIE = xmlToConfigurations(xmlString);
        configurationIE = checkConfigurations(configurationIE, entityManager);
        configurationIE = searchForDuplicate(configurationIE, entityManager);
        return configurationIE;
    }

    public ConfigurationTypeMappedWithAIPOIE xmlToConfigurations(String xmlString) throws JAXBException {

        JAXBContext jaxbContext = JAXBContext.newInstance(ConfigurationTypeMappedWithAIPOIE.class, ConfigurationTypeMappedWithAIPO.class,
                ConfigurationTypeWithPortsWSTypeAndSopClass.class, WebServiceType.class, WSTransactionUsage.class,
                SopClass.class, TransportLayer.class, ConfigurationType.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

        Reader reader = new InputStreamReader(new ByteArrayInputStream
                (xmlString.getBytes(StandardCharsets.UTF_8)), StandardCharsets.UTF_8);
        return (ConfigurationTypeMappedWithAIPOIE) unmarshaller.unmarshal(reader);
    }

    private ConfigurationTypeMappedWithAIPOIE checkConfigurations(ConfigurationTypeMappedWithAIPOIE configurationsIE, EntityManager entityManager){

        List<ConfigurationTypeMappedWithAIPO> configurations = configurationsIE.getConfigurationTypeMappedWithAIPOS();
        boolean configOk;
        Iterator iterator = configurations.iterator();
        while (iterator.hasNext()) {
            configOk = true;
            ConfigurationTypeMappedWithAIPO configuration = ((ConfigurationTypeMappedWithAIPO)iterator.next());
            if (existingAipo(configuration.getActorIntegrationProfileOption(), entityManager) == null) {
                configOk = false;
            }
            if (configOk){
                List<ConfigurationTypeWithPortsWSTypeAndSopClass> configurationTypes = configuration.getListOfConfigurationTypes();
                List<ConfigurationTypeWithPortsWSTypeAndSopClass> ignoredTypes = new ArrayList<>();
                Iterator typeIterator = configurationTypes.iterator();
                while (typeIterator.hasNext()) {
                    ConfigurationTypeWithPortsWSTypeAndSopClass configurationType = (ConfigurationTypeWithPortsWSTypeAndSopClass) typeIterator.next();
                    if (!checkConfigurationsType(configurationType, configurationsIE, entityManager)){
                        ignoredTypes.add(configurationType);
                        typeIterator.remove();
                    }
                }
                if (!ignoredTypes.isEmpty()){
                    ConfigurationTypeMappedWithAIPO newIgnoredConfiguration = new ConfigurationTypeMappedWithAIPO();
                    newIgnoredConfiguration.setActorIntegrationProfileOption(configuration.getActorIntegrationProfileOption());
                    newIgnoredConfiguration.setListOfConfigurationTypes(ignoredTypes);
                    configurationsIE.addIgnoredConfiguration(newIgnoredConfiguration);
                }
            }
            if (!configOk){
                iterator.remove();
                configurationsIE.addIgnoredConfiguration(configuration);
            }
        }
        configurationsIE.setConfigurationTypeMappedWithAIPOS(configurations);
        return configurationsIE;
    }

    private boolean checkConfigurationsType(ConfigurationTypeWithPortsWSTypeAndSopClass configurationType,
                                            ConfigurationTypeMappedWithAIPOIE configurationsIE, EntityManager entityManager){
        boolean configTypeOk = true;
        if (configurationType != null){

            //WebServiceType is deprecated
//            if (configurationType.getWebServiceType() != null) {
//                if (configurationType.getWebServiceType().getProfile() != null){
//                    if (IntegrationProfile.getObjectByKeyword(IntegrationProfile.class,
//                            configurationType.getWebServiceType().getProfile().getKeyword()) == null){
//                        configTypeOk = false;
//                        configurationsIE.addUnknownIntegrationProfile(configurationType.getWebServiceType().getProfile().getKeyword());
//                    }
//                }
//            }
            if (configurationType.getWsTRansactionUsage() != null) {
                if (configurationType.getWsTRansactionUsage().getTransaction() != null){
                    if (Transaction.GetTransactionByKeyword(configurationType.getWsTRansactionUsage().getTransaction().getKeyword()) == null){
                        configTypeOk = false;
                        configurationsIE.addUnknownTransaction(configurationType.getWsTRansactionUsage().getTransaction().getKeyword());
                    }
                }
            }
        }
        return configTypeOk;
    }

    public ActorIntegrationProfileOption existingAipo(ActorIntegrationProfileOption aipo, EntityManager entityManager){
        ActorIntegrationProfileOptionQuery aipoQuery = new ActorIntegrationProfileOptionQuery(entityManager);
        if (aipo.getIntegrationProfileOption() != null){
            aipoQuery.integrationProfileOption().keyword().eq(aipo.getIntegrationProfileOption().getKeyword());
        } else {
            aipoQuery.integrationProfileOption().eq(null);
        }
        if (aipo.getActorIntegrationProfile().getActor() != null){
            aipoQuery.actorIntegrationProfile().actor().keyword().eq(aipo.getActorIntegrationProfile().getActor().getKeyword());
        } else {
            aipoQuery.actorIntegrationProfile().actor().eq(null);
        }
        if (aipo.getActorIntegrationProfile().getIntegrationProfile() != null){
            aipoQuery.actorIntegrationProfile().integrationProfile().keyword().eq(aipo.getActorIntegrationProfile().getIntegrationProfile().getKeyword());
        } else {
            aipoQuery.actorIntegrationProfile().integrationProfile().eq(null);
        }
        ActorIntegrationProfileOption aipoFromDB = aipoQuery.getUniqueResult();
        return aipoFromDB;
    }

    private ConfigurationTypeMappedWithAIPOIE searchForDuplicate(ConfigurationTypeMappedWithAIPOIE configurationsIE, EntityManager entityManager){

        List<ConfigurationTypeMappedWithAIPO> configurations = configurationsIE.getConfigurationTypeMappedWithAIPOS();
        Iterator iterator = configurations.iterator();
        while (iterator.hasNext()) {
            ConfigurationTypeMappedWithAIPO configuration = ((ConfigurationTypeMappedWithAIPO)iterator.next());

            if (configuration.getListOfConfigurationTypes().isEmpty()){
                ActorIntegrationProfileOption aipo = existingAipo(configuration.getActorIntegrationProfileOption(), entityManager);

                ConfigurationTypeMappedWithAIPOQuery configurationQuery = new ConfigurationTypeMappedWithAIPOQuery(entityManager);
                configurationQuery.actorIntegrationProfileOption().id().eq(aipo.getId());
                ConfigurationTypeMappedWithAIPO configFromDb = configurationQuery.getUniqueResult();
                if (configFromDb != null ){
                    iterator.remove();
                    configurationsIE.addDuplicatedConfiguration(configuration);
                }
            } else {
                List<ConfigurationTypeWithPortsWSTypeAndSopClass> configurationTypes = configuration.getListOfConfigurationTypes();
                List<ConfigurationTypeWithPortsWSTypeAndSopClass> duplicatedTypes = new ArrayList<>();
                Iterator typeIterator = configurationTypes.iterator();
                while (typeIterator.hasNext()) {
                    ConfigurationTypeWithPortsWSTypeAndSopClass configurationType = (ConfigurationTypeWithPortsWSTypeAndSopClass) typeIterator.next();
                    if (isDuplicateConfigurationsType(configurationType, entityManager)){
                        duplicatedTypes.add(configurationType);
                        typeIterator.remove();
                    }
                }
                if (!duplicatedTypes.isEmpty()){
                    ConfigurationTypeMappedWithAIPO newDuplicatedConfiguration = new ConfigurationTypeMappedWithAIPO();
                    newDuplicatedConfiguration.setActorIntegrationProfileOption(configuration.getActorIntegrationProfileOption());
                    newDuplicatedConfiguration.setListOfConfigurationTypes(duplicatedTypes);
                    configurationsIE.addDuplicatedConfiguration(newDuplicatedConfiguration);
                }
                if (configuration.getListOfConfigurationTypes().isEmpty()){
                    iterator.remove();
                }
            }
        }
        configurationsIE.setConfigurationTypeMappedWithAIPOS(configurations);
        return configurationsIE;
    }

    private boolean isDuplicateConfigurationsType(ConfigurationTypeWithPortsWSTypeAndSopClass configurationType, EntityManager entityManager){

        ConfigurationTypeWithPortsWSTypeAndSopClassQuery query = new ConfigurationTypeWithPortsWSTypeAndSopClassQuery(entityManager);

        if (configurationType.getConfigurationType() != null){
            query.configurationType().typeName().eq(configurationType.getConfigurationType().getTypeName());
            query.configurationType().category().eq(configurationType.getConfigurationType().getCategory());
            query.configurationType().className().eq(configurationType.getConfigurationType().getClassName());
            query.configurationType().usedForProxy().eq(configurationType.getConfigurationType().getUsedForProxy());
        }
        query.portNonSecure().eq(configurationType.getPortNonSecure());
        query.portSecure().eq(configurationType.getPortSecure());

        if (configurationType.getWsTRansactionUsage() != null ){
            if (configurationType.getWsTRansactionUsage().getTransaction() != null){
                query.wsTRansactionUsage().transaction().keyword().eq(configurationType.getWsTRansactionUsage().getTransaction().getKeyword());
            }
            query.wsTRansactionUsage().usage().eq(configurationType.getWsTRansactionUsage().getUsage());
        }

        //WebServiceType is deprecated.
//        if (configurationType.getWebServiceType() != null && configurationType.getWebServiceType().getProfile() != null){
//            query.webServiceType().profile().keyword().eq(configurationType.getWebServiceType().getProfile().getKeyword());
//        }

        if (configurationType.getSopClass() != null){
            query.sopClass().keyword().eq(configurationType.getSopClass().getKeyword());
        }
        if (configurationType.getTransportLayer() != null){
            query.transportLayer().keyword().eq(configurationType.getTransportLayer().getKeyword());
        }

        ConfigurationTypeWithPortsWSTypeAndSopClass result = query.getUniqueResult();
        return result != null;
    }
}
