package net.ihe.gazelle.tm.gazelletest.action;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.list.GazelleListDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.beans.HQLStatistic;
import net.ihe.gazelle.hql.beans.HQLStatisticItem;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import net.ihe.gazelle.hql.paths.HQLSafePathBasic;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import net.ihe.gazelle.ssov7.gum.client.application.Group;
import net.ihe.gazelle.ssov7.gum.client.application.User;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import net.ihe.gazelle.ssov7.gum.client.interlay.client.UserRestQueryBuilder;
import net.ihe.gazelle.ssov7.gum.client.interlay.client.utils.UserSearchParams;
import net.ihe.gazelle.tm.filter.TMCriterions;
import net.ihe.gazelle.tm.filter.modifier.TestMatchingTestingSession;
import net.ihe.gazelle.tm.gazelletest.model.definition.Test;
import net.ihe.gazelle.tm.gazelletest.model.definition.TestEntity;
import net.ihe.gazelle.tm.gazelletest.model.instance.MonitorInSession;
import net.ihe.gazelle.tm.gazelletest.model.instance.MonitorInSessionQuery;
import net.ihe.gazelle.tm.gazelletest.model.instance.Status;
import net.ihe.gazelle.tm.users.filter.UserFilterImpl;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Remove;
import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Name("kpiMonitors")
@Scope(ScopeType.PAGE)
public class KpiMonitors extends KpiDisplay<MonitorInSession> implements Serializable, QueryModifier<MonitorInSession> {
   private static final Logger LOG = LoggerFactory.getLogger(KpiMonitors.class);
   private static final long serialVersionUID = 7341977222674982652L;
   @In
   private GazelleIdentity identity;

   @Create
   public void create() {
      if (LOG.isDebugEnabled()) {
         LOG.debug("create");
      }
      MonitorInSessionQuery query = new MonitorInSessionQuery();
      HQLCriterionsForFilter<MonitorInSession> criterions = query.getHQLCriterionsForFilter();
      TMCriterions.addTestingSession(criterions, "testing_session", query.testingSession(), identity);
      TMCriterions.addAIPOCriterionsUsingTestInstance(criterions, query.testInstances(), "testing_session");


      criterions.addPath("user_id", query.userId());
      criterions.addPath("active", query.isActivated(), true);

      TestEntity<Test> testEntity = query.monitorTests().test();
      criterions.addPath("test", testEntity);
      criterions.addQueryModifierForCriterion("test", new TestMatchingTestingSession(testEntity, "testing_session"));

      FacesContext fc = FacesContext.getCurrentInstance();
      Map<String, String> requestParametersMap = fc.getExternalContext().getRequestParameterMap();

      criterions.addQueryModifier(this);
      filter = new Filter<>(criterions, requestParametersMap);

      List<HQLStatisticItem> statisticItems = new ArrayList<>();

      statisticItems.add(query.testInstances().test().id().statisticItem());
      statisticItems.add(query.testInstances().testInstanceParticipants().systemInSessionUser().systemInSession()
            .id().statisticItem());

      HQLSafePathBasic<Integer> tiPath = query.testInstances().id();
      HQLSafePathBasic<Status> tiStatusPath = query.testInstances().lastStatus();

      addTiStatistics(statisticItems, tiPath, tiStatusPath);
      filter.setStatisticItems(statisticItems);
      userFilter = new UserFilterImpl(requestParametersMap, identity);
      userFilter.addListener(filter);
   }

   @Override
   public GazelleListDataModel<HQLStatistic<MonitorInSession>> getElements() {
      List<HQLStatistic<MonitorInSession>> res = filter.getListWithStatistics();
      return new GazelleListDataModel<>(res);
   }

   private List<String> idsOf(List<User> users) {
      List<String> userIds = new ArrayList<>();
      for (User user : users) {
         userIds.add(user.getId());
      }
      return userIds;
   }

   public void clear() {
      filter.clear();
      userFilter.clear();
   }


   public String getUrlParameters() {
      if (userFilter != null && userFilter.getFilterValues().containsKey(UserRestQueryBuilder.SEARCH) &&
            userFilter.getFilterValues().get(UserRestQueryBuilder.SEARCH) != null &&
            !userFilter.getFilterValues().get(UserRestQueryBuilder.SEARCH).isEmpty()) {
         return filter.getUrlParameters() + "&" + userFilter.getUrlParameters();
      } else {
         return filter.getUrlParameters();
      }
   }

   public void setUrlParameters(String urlParameters) {
      // don't need to write here just to prevent EL exception from jsf
   }

   @Remove
   @Destroy
   public void destroy() {
      if (LOG.isDebugEnabled()) {
         LOG.debug("destroy");
      }
   }

   @Override
   public void modifyQuery(HQLQueryBuilder<MonitorInSession> hqlQueryBuilder, Map<String, Object> map) {
      String search = userFilter.getFilterValues().get(UserRestQueryBuilder.SEARCH);
      if (search != null && !search.isEmpty()) {
         UserService service = (UserService) Component.getInstance("gumUserService");
         UserSearchParams userSearchParams = new UserSearchParams().setSearch(search)
            .setGroup(Group.GRP_MONITOR)
            .setActivated(true);
         List<User> users = service.searchNoLimit(userSearchParams);
         hqlQueryBuilder.addIn("userId", idsOf(users));
      }
   }
}