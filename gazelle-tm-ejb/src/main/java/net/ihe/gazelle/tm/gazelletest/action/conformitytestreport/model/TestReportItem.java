package net.ihe.gazelle.tm.gazelletest.action.conformitytestreport.model;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "test")
@XmlAccessorType(XmlAccessType.NONE)
public class TestReportItem extends AbstractTestReportModel {

    @XmlElementWrapper(name = "testInstances")
    @XmlElement(name = "testInstance")
    private List<TestInstanceReport> testInstanceReports;


    public TestReportItem() {
        super();
    }

    /**
     * Getter of List of test Instance
     *
     * @return list
     */
    public List<TestInstanceReport> getTestInstanceReports() {
        return testInstanceReports;
    }

    /**
     * Setter of the test Instance List
     *
     * @param testInstanceReports List
     */
    public void setTestInstanceReports(List<TestInstanceReport> testInstanceReports) {
        if (testInstanceReports != null) {
            this.testInstanceReports = new ArrayList<>(testInstanceReports);
        }
    }

    public TestReportItem(String keyword, String name, String testType) {
        super(keyword, name, testType);
    }
}



