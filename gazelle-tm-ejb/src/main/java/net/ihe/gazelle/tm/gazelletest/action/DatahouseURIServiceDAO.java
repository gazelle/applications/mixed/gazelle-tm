package net.ihe.gazelle.tm.gazelletest.action;

import net.ihe.gazelle.tf.model.Actor;
import net.ihe.gazelle.tf.model.Standard;
import net.ihe.gazelle.tm.gazelletest.model.definition.RoleInTest;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestStepsInstance;
import net.ihe.gazelle.tm.systems.model.SystemInSession;

public interface DatahouseURIServiceDAO {

    /**
     * Get the non-null standard by a TestStepsInstance
     * @param testStepsInstance a TestStepsInstance
     * @return the corresponding non-null Standard
     * @throws java.util.NoSuchElementException if no Standard is found
     */
    Standard getStandard(TestStepsInstance testStepsInstance);

    /**
     * Get an Actor by a TestStepsInstance, a RoleInTest and a SystemInSession
     * @param testStepsInstance a TestStepsInstance
     * @param roleInTest a RoleInTest
     * @param systemInSession a SystemInSession
     * @return the corresponding Actor
     * @throws java.util.NoSuchElementException if no Actor is found
     */
    Actor getActor(TestStepsInstance testStepsInstance, RoleInTest roleInTest, SystemInSession systemInSession);
}
