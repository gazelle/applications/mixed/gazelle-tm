package net.ihe.gazelle.tm.statistics.preparation;

import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstance;
import net.ihe.gazelle.tm.session.TestingSessionService;
import net.ihe.gazelle.tm.statistics.common.StatisticsCommonService;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import java.util.List;

@Scope(ScopeType.EVENT)
@AutoCreate
@Name("statisticsForPreparationPhaseService")
public class StatisticsForPreparationPhaseServiceImpl implements StatisticsForPreparationPhaseService {

    @In(value = "statisticsCommonService")
    StatisticsCommonService statisticsCommonService;

    @In(value = "statisticsForPreparationPhaseDao")
    StatisticsForPreparationPhaseDao statisticsForPreparationPhaseDao;

    @In(value = "testingSessionService")
    private TestingSessionService testingSessionService;

    @Override
    public int getNumberOfSessionParticipants() {
        return statisticsCommonService.getNumberOfSessionParticipants();
    }

    @Override
    public int getNumberOfRegisteredInstitutionWithAttendee() {
        return statisticsCommonService.getNumberOfRegisteredInstitutionWithAttendee();
    }

    @Override
    public int getNumberOfRegisteredInstitutionWithSystem() {
        return statisticsCommonService.getNumberOfRegisteredInstitutionWithSystem();
    }

    @Override
    public int getNumberOfRegisteredSystemInSession() {
        return statisticsCommonService.getNumberOfRegisteredSystemInSession();
    }

    @Override
    public int getNumberOfActivatedMonitorInSession() {
        return statisticsForPreparationPhaseDao.getNumberOfActivatedMonitorInSession(testingSessionService.getUserTestingSession());
    }

    @Override
    public int getNumberOfAcceptedSystemInSession() {
        return statisticsForPreparationPhaseDao.getNumberOfAcceptedSystemInSession();
    }

    @Override
    public int getNumberOfNotAcceptedSystemInSession() {
        return statisticsForPreparationPhaseDao.getNumberOfNotAcceptedSystemInSession();
    }

    @Override
    public int getNumberOfAcceptedSupportiveRequest() {
        return statisticsForPreparationPhaseDao.getNumberOfAcceptedSupportiveRequest(testingSessionService.getUserTestingSession());
    }

    @Override
    public int getNumberOfNotYetAcceptedSupportiveRequest() {
        return statisticsForPreparationPhaseDao.getNumberOfNotYetAcceptedSupportiveRequest(testingSessionService.getUserTestingSession());
    }

    @Override
    public int getNumberOfSUTNetworkConfiguration() {
        return statisticsForPreparationPhaseDao.getNumberOfSUTNetworkConfiguration(testingSessionService.getUserTestingSession());
    }

    @Override
    public int getNumberOfSUTNetworkConfigurationAccepted() {
        return statisticsForPreparationPhaseDao.getNumberOfSUTNetworkConfigurationAccepted(testingSessionService.getUserTestingSession());
    }

    @Override
    public int getNumberOfSUTNetworkConfigurationNotAccepted() {
        return statisticsForPreparationPhaseDao.getNumberOfSUTNetworkConfigurationNotAccepted(testingSessionService.getUserTestingSession());
    }

    @Override
    public int getNumberOfGeneratedHosts() {
        return statisticsForPreparationPhaseDao.getNumberOfGeneratedHosts(testingSessionService.getUserTestingSession());
    }

    @Override
    public int getNumberOfGeneratedHostsWithIP() {
        return statisticsForPreparationPhaseDao.getNumberOfGeneratedHostsWithIP(testingSessionService.getUserTestingSession());
    }

    @Override
    public int getNumberOfGeneratedHostsWithoutIP() {
        return statisticsForPreparationPhaseDao.getNumberOfGeneratedHostsWithoutIP(testingSessionService.getUserTestingSession());
    }

    @Override
    public int getNumberOfPreparatoryTests() {
        return statisticsForPreparationPhaseDao.getNumberOfPreparatoryTests(testingSessionService.getUserTestingSession());
    }

    @Override
    public StatisticsCommonService.TestInstanceStats getPreparatoryTestStatistics() {
        List<TestInstance> testInstances = statisticsForPreparationPhaseDao.getAllPreparatoryTestInstances(
                testingSessionService.getUserTestingSession());
        return statisticsCommonService.getStatisticsForTestInstances(testInstances);
    }
}
