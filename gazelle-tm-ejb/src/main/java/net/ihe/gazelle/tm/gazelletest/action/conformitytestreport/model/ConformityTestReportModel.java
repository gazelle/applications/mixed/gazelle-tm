package net.ihe.gazelle.tm.gazelletest.action.conformitytestreport.model;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@XmlRootElement(name = "report")
@XmlAccessorType(XmlAccessType.NONE)
public class ConformityTestReportModel {

    @XmlElement(name = "system", type = SystemModel.class)
    private SystemModel system;

    @XmlElement(name = "organization", type = InstitutionModel.class)
    private InstitutionModel institution;

    @XmlElement(name = "testingSession", type = TestingSessionModel.class)
    private TestingSessionModel testingSession;

    @XmlElementWrapper(name = "aipos")
    @XmlElements({@XmlElement(name = "aipo", type = AIPOReportModel.class)})
    private List<AIPOReportModel> aipoReportModelList = new ArrayList<>();

    /**
     * Getter of list of AIPOs
     *
     * @return List of AIPOs
     */
    public List<AIPOReportModel> getAipoModelList() {

        return new ArrayList<>(aipoReportModelList);
    }

    /**
     * Setter of the list of AIPOs
     *
     * @param aipoReportModelList list
     */
    public void setAipoModelList(List<AIPOReportModel> aipoReportModelList) {
        this.aipoReportModelList = new ArrayList<>(aipoReportModelList);
    }

    /**
     * Setter of System
     *
     * @param keyword
     * @param name
     */
    public void setSystem(Integer idSystem, String name, String keyword, Integer idTestingSession, String testingSessionDescription, Integer organisationId) {
        this.system = new SystemModel(idSystem, name, keyword, idTestingSession, testingSessionDescription, organisationId);
    }

    public void setInstitution(Integer id, String name, String keyWord) {
        this.institution = new InstitutionModel(id, name, keyWord);
    }

    /**
     * Setter of testing Session Name
     *
     * @param idSession id of selected testingSession
     * @param description description of selected testingSession
     * @param startDate startDate of selected testingSession
     * @param endDate endDate of selected testingSession
     * @param registrationDeadlineDate registrationDeadlineDate of selected testingSession
     * @param isActive true if selected tesingSession is active
     */
    public void setTestingSession(Integer idSession, String description, Date startDate, Date endDate, Date registrationDeadlineDate, Boolean isActive, Boolean isClosed, Boolean isHidden) {
        this.testingSession = new TestingSessionModel(idSession, description, startDate, endDate, registrationDeadlineDate, isActive, isClosed, isHidden);
    }

}
