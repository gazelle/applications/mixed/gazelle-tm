package net.ihe.gazelle.tm.application.dao;

import net.ihe.gazelle.tm.application.model.Section;
import net.ihe.gazelle.tm.application.model.SectionQuery;
import net.ihe.gazelle.tm.application.services.SectionDAO;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import javax.persistence.EntityManager;
import java.util.List;


@Name("sectionDAO")
@Scope(ScopeType.STATELESS)
@AutoCreate
public class SectionDAOImpl implements SectionDAO {

    @In
    EntityManager entityManager;

    @Override
    public Section getSection(Integer sectionId) {
        SectionQuery sectionQuery = new SectionQuery(entityManager);
        sectionQuery.id().eq(sectionId);
        return sectionQuery.getUniqueResult();
    }

    @Override
    public Section getSectionByName(String name) {
        SectionQuery sectionQuery = new SectionQuery(entityManager);
        sectionQuery.name().eq(name);
        return sectionQuery.getUniqueResult();
    }

    @Override
    public List<Section> getSections() {
        SectionQuery sectionQuery = new SectionQuery(entityManager);
        return sectionQuery.getList();
    }

    @Override
    public List<Section> getRenderedSections() {
        SectionQuery sectionQuery = new SectionQuery(entityManager);
        sectionQuery.rendered().eq(true);
        return sectionQuery.getList();
    }

    @Override
    public void removeSection(Section section) {
        entityManager.remove(section);
        entityManager.flush();
    }

    @Override
    public void updateSection(Section section) {
        entityManager.merge(section);
        entityManager.flush();
    }
}
