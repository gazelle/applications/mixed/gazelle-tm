package net.ihe.gazelle.tm.systems;

import net.ihe.gazelle.tf.model.Domain;
import net.ihe.gazelle.tf.model.IntegrationProfile;
import net.ihe.gazelle.tm.systems.model.ProfileInTestingSession;
import net.ihe.gazelle.tm.systems.model.TestingSession;

import java.util.List;

public interface ProfileInTestingSessionDAO {

    List<ProfileInTestingSession> getAllProfileInTestingSessionsForCurrentSession(TestingSession selectedTestingSession);
    boolean isProfileInTestingSessionAlreadyCreated(Domain domain, IntegrationProfile integrationProfile,TestingSession selectedTestingSession);
    void saveProfileInTestingSession(ProfileInTestingSession profileInTestingSession);
    void updateProfileInTestingSession(ProfileInTestingSession profileInTestingSession);
}
