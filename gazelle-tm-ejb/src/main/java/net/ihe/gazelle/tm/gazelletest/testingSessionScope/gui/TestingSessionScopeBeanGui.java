package net.ihe.gazelle.tm.gazelletest.testingSessionScope.gui;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.ssov7.gum.client.application.Role;
import net.ihe.gazelle.ssov7.gum.client.application.UserAttributeCommon;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import net.ihe.gazelle.tm.gazelletest.testingSessionScope.ProfileCoverage;
import net.ihe.gazelle.tm.gazelletest.testingSessionScope.ProfileScope;
import net.ihe.gazelle.tm.gazelletest.testingSessionScope.ProfileScopeSortMethod;
import net.ihe.gazelle.tm.gazelletest.testingSessionScope.TestingSessionScopeService;
import net.ihe.gazelle.tm.session.TestingSessionService;
import net.ihe.gazelle.tm.systems.model.ProfileInTestingSession;
import net.ihe.gazelle.tm.systems.model.Testability;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import net.ihe.gazelle.tm.testexecution.SUTCapabilityStatus;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.CellReference;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.util.CellRangeAddress;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.navigation.Pages;
import org.jboss.seam.security.Identity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.persistence.Enumerated;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Serializable;
import java.util.*;

import static net.ihe.gazelle.tm.gazelletest.utils.HSSFCellUtils.getCell;

@Name(value = "testingSessionScopeBeanGui")
@Scope(ScopeType.PAGE)
public class TestingSessionScopeBeanGui implements Serializable, UserAttributeCommon {

    private static final long serialVersionUID = 7085338188572119609L;
    private static Logger log = LoggerFactory.getLogger(TestingSessionScopeBeanGui.class);


    @In(value = "testingSessionService")
    private transient TestingSessionService testingSessionService;

    @In(value = "gumUserService")
    private transient UserService userService;
    private static final Map<Testability, String> testabilityStyle = new HashMap<>();
    static {
        testabilityStyle.put(Testability.TESTABLE, "gzl-label gzl-label-green");
        testabilityStyle.put(Testability.FEW_PARTNERS, "gzl-label gzl-label-orange");
        testabilityStyle.put(Testability.DROPPED, "gzl-label gzl-label-red");
        testabilityStyle.put(Testability.REMOVED_FROM_SESSION, "gzl-label gzl-label-red");
        testabilityStyle.put(Testability.DECISION_PENDING,"gzl-label gzl-label-blue");
    }
    private Filter<ProfileInTestingSession> filter;
    private ProfileScopeSortMethod selectedSort = ProfileScopeSortMethod.PROFILE;

    @Enumerated
    private SUTCapabilityStatus sutCapabilityStatus;

    private Testability selectedTestability;

    @In(value = "testingSessionScopeService")
    private transient TestingSessionScopeService testingSessionScopeService;

    private SUTCapabilityStatus selectedStatus;

    public SUTCapabilityStatus getSelectedStatus() {
        return selectedStatus;
    }

    public List<SUTCapabilityStatus> getStatus(){ return Arrays.asList(sutCapabilityStatus.values()); }


    public void setSelectedStatus(SUTCapabilityStatus selectedStatus) {
        this.selectedStatus = selectedStatus;
    }

    public Testability getSelectedTestability() {
        return selectedTestability;
    }

    public void setSelectedTestability(Testability selectedTestability) {
        this.selectedTestability = selectedTestability;
    }

    public void updateProfileInTestingSession(ProfileInTestingSession profileInTestingSession){
        testingSessionScopeService.updateProfileInTestingSession(profileInTestingSession);
    }

    public void clearFilter(){
        this.filter.clear();
    }

    public void refreshResults(){
        this.filter.modified();
    }
    public Filter<ProfileInTestingSession> getFilter() {
        if (this.filter==null) {
            FacesContext facesContext = FacesContext.getCurrentInstance();
            ExternalContext externalContext = facesContext.getExternalContext();
            this.filter=testingSessionScopeService.getFilter(externalContext.getRequestParameterMap());
        }
        return this.filter;
    }

    public List<ProfileScope> getProfileScopes() {
        List<ProfileScope> profileScopes=testingSessionScopeService.getProfileScopes(getFilter());
        if (selectedSort!=null) {
            Collections.sort(profileScopes, selectedSort.getComparator());
        }
        return profileScopes;
    }

    public List<ProfileScopeSortMethod> getAllSortMethods(){
        return Arrays.asList(ProfileScopeSortMethod.values());
    }

    public List<ProfileCoverage> getProfileCoveragesByProfileScope(ProfileScope profileScope) {
        if (profileScope != null) {
            return testingSessionScopeService.getProfileCoverageForProfileScope(profileScope);
        }
        return new ArrayList<>();
    }

    public List<Testability> getTestabilityValues(){
        return Arrays.asList(Testability.values());
    }

    public List<Testability> getDroppedAndRemoveTestability(){
        List<Testability> testabilities = getTestabilityValues();
        List<Testability> res = new ArrayList<>();
        try {
            for (Testability testability : testabilities) {
                String keyword = testability.getKeyword();
                if (keyword.equals("dropped") || keyword.equals("removed_from_session")) {
                    res.add(testability);
                }
            }
        }
        catch (Exception e){
            log.warn(e.toString());
        }
        return res;
    }

    public int getMaxProfileCoverageForProfileScope(ProfileScope profileScope) {
        return testingSessionScopeService.getMaxProfileCoverageForProfileScope(profileScope);
    }

    public int getCurrentProfileCoverageForProfileScope(ProfileScope profileScope) {
        return testingSessionScopeService.getCurrentProfileCoverageForProfileScope(profileScope);
    }

    public boolean isAdminOrTSM() {
        return Identity.instance().hasRole(Role.ADMIN) || Identity.instance().hasRole(Role.TESTING_SESSION_ADMIN);
    }

    public ProfileScopeSortMethod getSelectedSort() {
        return selectedSort;
    }

    public void setSelectedSort(ProfileScopeSortMethod selectedSort) {
        this.selectedSort = selectedSort;
    }

    public String getFormattedLastChangedDateForProfileScope(ProfileScope profileScope) {
        return testingSessionScopeService.getFormattedLastChangedDateForProfileScope(profileScope);
    }

    public String getCurrentProfileCoverageStyleForProfileScope(ProfileScope profileScope) {
        double maxCoverage = testingSessionScopeService.getMaxProfileCoverageForProfileScope(profileScope);
        double percentage = (testingSessionScopeService.getCurrentProfileCoverageForProfileScope(profileScope) / maxCoverage) * 100.0;
        return "width:" + percentage + "%";
    }

    public void getTestingSessionScope() throws IOException {
        HSSFWorkbook workbook = new HSSFWorkbook();
        CellStyle style = workbook.createCellStyle();
        style.setBorderBottom((short)1);
        style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderTop((short)1);
        style.setTopBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderLeft((short)1);
        style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderRight((short)1);
        style.setRightBorderColor(IndexedColors.BLACK.getIndex());
        HSSFSheet worksheet = workbook.createSheet("Testing Session scope");
        worksheet.createFreezePane(0, 1);

        int colIndex = 0;
        int rowIndex = 0;
        getCell(worksheet, style, rowIndex, colIndex++).setCellValue("Domain name");
        getCell(worksheet, style, rowIndex, colIndex++).setCellValue("Profile name");
        getCell(worksheet, style, rowIndex, colIndex++).setCellValue("Profile keyword");
        getCell(worksheet, style, rowIndex, colIndex++).setCellValue("Testable status");
        getCell(worksheet, style, rowIndex, colIndex++).setCellValue("Comment");

        rowIndex++;

        List<ProfileScope> profileScopes = getProfileScopes();
        for (ProfileScope profileScope : profileScopes) {
            colIndex = 0;
            String domainName = profileScope.getProfileInTestingSession().getDomain().getName();
            String profileName = profileScope.getProfileInTestingSession().getIntegrationProfile().getName();
            String profileKeyword = profileScope.getProfileInTestingSession().getIntegrationProfile().getKeyword();
            String testableStatus = profileScope.getProfileInTestingSession().getTestability().getKeyword();
            String comment = profileScope.getProfileInTestingSession().getComment();
            getCell(worksheet, style, rowIndex, colIndex++).setCellValue(domainName);
            getCell(worksheet, style, rowIndex, colIndex++).setCellValue(profileName);
            getCell(worksheet, style, rowIndex, colIndex++).setCellValue(profileKeyword);
            getCell(worksheet, style, rowIndex, colIndex++).setCellValue(testableStatus);
            getCell(worksheet, style, rowIndex, colIndex++).setCellValue(comment);
            rowIndex++;
        }
        CellReference start = new CellReference(0, 0);
        String startString = start.formatAsString();
        CellReference end = new CellReference(rowIndex - 1, colIndex - 1);
        String endString = end.formatAsString();
        worksheet.setAutoFilter(CellRangeAddress.valueOf(startString + ":" + endString));

        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExternalContext externalContext = facesContext.getExternalContext();

        Pages.getCurrentBaseName();

        HttpServletResponse response = (HttpServletResponse) externalContext.getResponse();
        ServletOutputStream servletOutputStream = response.getOutputStream();
        response.setContentType("application/vnd.ms-excel");
        Integer sessionsId = testingSessionService.getUserTestingSession().getId();
        response.setHeader("Content-Disposition", "attachment;filename=\"testingSession_" + sessionsId + "_scope.xls\"");
        workbook.write(servletOutputStream);
        servletOutputStream.flush();
        servletOutputStream.close();
        facesContext.responseComplete();
    }

    /**
     * Change the status of each profile with the selected testability then update the DataBase
     */
    public void autoEvaluate(){
        TestingSession session = testingSessionService.getUserTestingSession();
        testingSessionScopeService.autoEvaluate(session, selectedTestability, selectedStatus);
        FacesMessages.instance().add("Evaluation done");
    }

    @Override
    public String getUserName(String userId) {
        return userService.getUserDisplayNameWithoutException(userId);
    }
}
