package net.ihe.gazelle.tm.messages;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import net.ihe.gazelle.ssov7.gum.client.application.Role;
import net.ihe.gazelle.tm.application.action.ApplicationManager;
import net.ihe.gazelle.tm.messages.model.Message;
import net.ihe.gazelle.tm.messages.model.MessageQuery;
import net.ihe.gazelle.tm.messages.model.MessageUser;

import javax.persistence.EntityManager;
import java.util.Date;
import java.util.Set;

public class MessageService implements NotificationListener {

   private final ApplicationPreferenceManager applicationPreferenceManager;
   private final ApplicationManager applicationManager;
   private final EntityManager entityManager;

   public MessageService(ApplicationPreferenceManager applicationPreferenceManager,
                         ApplicationManager applicationManager, EntityManager entityManager) {
      this.applicationPreferenceManager = applicationPreferenceManager;
      this.applicationManager = applicationManager;
      this.entityManager = entityManager;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public void sendNotification(Message message) {
      if (areMessagesEnabled()) {
         persistMessage(message);
      }
   }

   public boolean areMessagesEnabled() {
      return this.applicationPreferenceManager.getBooleanValue("use_messages");
   }

   public boolean isMessageFeatureGranted(GazelleIdentity identity) {
      return areMessagesEnabled() &&
            applicationManager.isTestManagement() &&
            identity != null && identity.isLoggedIn() && (
            identity.hasRole(Role.VENDOR) ||
                  identity.hasRole(Role.VENDOR_ADMIN) ||
                  identity.hasRole(Role.MONITOR) ||
                  identity.hasRole(Role.ADMIN) ||
                  identity.hasRole(Role.TESTING_SESSION_ADMIN)
      );
   }

   public int getUnreadCount(GazelleIdentity identity, Date lastReadDate) {
      if (isMessageFeatureGranted(identity) && lastReadDate != null) {
         return countMessages(identity, lastReadDate);
      } else {
         return 0;
      }
   }

   public MessageDataModel getMessages(GazelleIdentity identity) {
      if (isMessageFeatureGranted(identity)) {
         return new MessageDataModel(entityManager, identity.getUsername());
      } else {
         return null;
      }
   }


   private int countMessages(GazelleIdentity identity, Date lastReadDate) {
      MessageQuery query = new MessageQuery(entityManager);
      query.users().userId().eq(identity.getUsername());
      query.date().ge(lastReadDate);
      query.setCachable(true);
      return query.getCount();
   }

   private void persistMessage(Message message) {
      Set<MessageUser> messageUserList = message.getUsers();
      message.setUsers(null);
      message = entityManager.merge(message);
      for (MessageUser messageUser : messageUserList) {
         messageUser.setMessageId(message.getId());
      }
      message.setUsers(messageUserList);
      entityManager.merge(message);
      entityManager.flush();
   }
}
