package net.ihe.gazelle.tm.statistics.testing.gui;


import net.ihe.gazelle.menu.Pages;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import net.ihe.gazelle.ssov7.gum.client.application.Role;
import net.ihe.gazelle.tm.gazelletest.model.instance.MonitorInSession;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstance;
import net.ihe.gazelle.tm.session.TestingSessionService;
import net.ihe.gazelle.tm.statistics.common.StatisticsCommonService;
import net.ihe.gazelle.tm.statistics.testing.StatisticsForTestingPhaseService;
import net.ihe.gazelle.tm.systems.model.SystemInSession;
import net.ihe.gazelle.tm.systems.model.TestingSession;

import net.ihe.gazelle.users.model.Institution;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.security.Identity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Scope(ScopeType.EVENT)
@AutoCreate
@Name(value = "statisticsForTestingPhaseBeanGui")
public class StatisticsForTestingPhaseBeanGui implements Serializable {

   public static final String COLOR_RED_STRING = "\"red\"";
   public static final String COLOR_BLUE_GAZELLE_STRING = "\"#4ca8de\"";
   public static final String COLOR_YELLOW_STRING = "\"#FDE74C\"";
   public static final String COLOR_ORANGE_STRING = "\"orange\"";
   public static final String COLOR_GREY_STRING = "\"#C1C1C1\"";
   public static final String COLOR_GREEN_STRING = "\"#5cb85c\"";
   private static final long serialVersionUID = -6291798638543602085L;

   @In(value = "statisticsForTestingPhaseService")
   private StatisticsForTestingPhaseService statisticsForTestingPhaseService;

   @In
   private GazelleIdentity identity;

   @In(value = "gumUserService")
   private UserService userService;

   @In(value = "testingSessionService")
   private transient TestingSessionService testingSessionService;

   public List<Integer> getTestRunsStatisticsForASystem(SystemInSession system) {
      List<TestInstance> testInstances = statisticsForTestingPhaseService.getTestInstancesForASystem(system);
      StatisticsCommonService.TestInstanceStats testInstanceStats =
            statisticsForTestingPhaseService.getStatisticsForTestInstances(
            testInstances);

      List<Integer> statistics = new ArrayList<>();

      if (!testInstanceStats.isEmpty()) {
         statistics.addAll(Arrays.asList(
               testInstanceStats.getNbOfVerified(),
               testInstanceStats.getNbOfFailed(),
               testInstanceStats.getNbOfInProgress(),
               testInstanceStats.getNbOfTobVerified(),
               testInstanceStats.getNbOfPartiallyVerified(),
               testInstanceStats.getNbOfCritical()));
      }

      return statistics;
   }

   public List<Integer> getStatisticsForAllTestInstances() {
      List<TestInstance> testInstances = statisticsForTestingPhaseService.getAllTestInstancesForCurrentSession();
      StatisticsCommonService.TestInstanceStats testInstanceStats =
            statisticsForTestingPhaseService.getStatisticsForTestInstances(
            testInstances);
      return new ArrayList<>(Arrays.asList(
            testInstanceStats.getNbOfVerified(),
            testInstanceStats.getNbOfFailed(),
            testInstanceStats.getNbOfInProgress(),
            testInstanceStats.getNbOfTobVerified(),
            testInstanceStats.getNbOfPartiallyVerified(),
            testInstanceStats.getNbOfCritical()));
   }


   public List<Integer> getStatisticsForEvaluationPendingTestInstances() {
      StatisticsForTestingPhaseService.PendingTestInstanceStats pendingTestInstanceStats =
            statisticsForTestingPhaseService.getStatisticsForEvaluationPendingTestInstances();

      return new ArrayList<>(
            Arrays.asList(pendingTestInstanceStats.getNbOfToBeAssigned(), pendingTestInstanceStats.getNbOfClaimed(),
                  pendingTestInstanceStats.getNbOfCritical(), pendingTestInstanceStats.getNbOfPartiallyVerified()));
   }

   public List<Integer> getStatisticsLeftOverTestInstances() {
      StatisticsForTestingPhaseService.LeftOverTestInstanceStats leftOverTestInstanceStats =
            statisticsForTestingPhaseService.getStatisticsLeftOverTestInstances();

      return new ArrayList<>(Arrays.asList(leftOverTestInstanceStats.getNbOfAtLeastOneDay(),
            leftOverTestInstanceStats.getNbOfAtLeastOneAndHalfDay(),
            leftOverTestInstanceStats.getNbOfMoreThanTwoDays()));
   }

   public List<Integer> getStatisticsForAllTestInstancesOfCurrentMonitor() {
      List<TestInstance> testInstances = statisticsForTestingPhaseService.getTestInstancesForMonitorInSession(
            MonitorInSession.getActivatedMonitorInSessionForATestingSessionByUser(
                  testingSessionService.getUserTestingSession(), identity.getUsername()));

      StatisticsCommonService.TestInstanceStats testInstanceStats =
            statisticsForTestingPhaseService.getStatisticsForTestInstances(
            testInstances);

      return new ArrayList<>(Arrays.asList(testInstanceStats.getNbOfVerified(), testInstanceStats.getNbOfFailed(),
            testInstanceStats.getNbOfTobVerified(), testInstanceStats.getNbOfPartiallyVerified(),
            testInstanceStats.getNbOfCritical()));
   }

   public int getNumberOfTotalTestInstancesForASystemInSession(SystemInSession system) {
      return statisticsForTestingPhaseService.getNumberOfTotalTestInstanceForASystemInSession(system);
   }

   public int getNumberOfTotalTestInstances() {
      return statisticsForTestingPhaseService.getNumberOfTotalTestInstances();
   }

   public int getNumberOfVerificationPendingTestInstances() {
      return statisticsForTestingPhaseService.getNumberOfVerificationPendingTestInstances();
   }

   public int getNumberOfLeftOverTestInstances() {
      return statisticsForTestingPhaseService.getNumberOfLeftOverTestInstances();
   }

   public int getNumberOfTotalTestInstancesForMonitorInSession() {

      return statisticsForTestingPhaseService.getNumberOfTotalTestInstancesForMonitorInSession(
            MonitorInSession.getActivatedMonitorInSessionForATestingSessionByUser(
                  testingSessionService.getUserTestingSession(),
                  identity.getUsername()));
   }

   public boolean isTestSessionManagerOrAdmin() {
      return Identity.instance().hasRole(Role.TESTING_SESSION_ADMIN) || Identity.instance()
            .hasRole(Role.ADMIN);
   }

   public boolean isVendor() {
      return Identity.instance().hasRole(Role.VENDOR);
   }

   public boolean isMonitor() {
      return Identity.instance().hasRole(Role.MONITOR);
   }

   public boolean isAdminOrTSM() {
      return Identity.instance().hasRole(Role.ADMIN) || Identity.instance()
            .hasRole(Role.TESTING_SESSION_ADMIN);
   }

   public boolean isMonitorOfCurrentSession() {
      return Identity.instance().hasRole(Role.MONITOR)
            && (MonitorInSession.getActivatedMonitorInSessionForATestingSessionByUser(
            testingSessionService.getUserTestingSession(), identity.getUsername()) != null);

   }

   public List<SystemInSession> getSystemsForCurrentUser() {
      return statisticsForTestingPhaseService.getSystemsForCurrentUser();
   }

   public String getSystemKeyword(SystemInSession system) {
      return system.getSystem().getKeyword();
   }

   public String getLinkFilterTestExecutionPage(SystemInSession system) {
      return Pages.TEST_EXECUTION.getMenuLink() + "?" + "system=" + system.getSystem()
            .getId() + "&   institution=" + Institution.getLoggedInInstitution().getId();
   }

   public String getWorklistFilteredForMonitor() {
      TestingSession selectedTestingSession = testingSessionService.getUserTestingSession();
      return Pages.CAT_MONITOR_WORKLIST.getMenuLink() + "?testingSession=" + selectedTestingSession
            .getId()
            + "&monitor=" + MonitorInSession.getActivatedMonitorInSessionForATestingSessionByUser(
              selectedTestingSession, identity.getUsername()).getId();
   }

   public String getAllTestRunsLinkFilteredForMonitor() {
      String userId = userService.getUserById(identity.getUsername()).getId();
      return Pages.CAT_INSTANCES.getMenuLink() + "?testingSession=" + testingSessionService.getUserTestingSession().getId()
            + "&monitorInSession=" + userId;
   }

   public List<String> getColorsForTestInstancesForChart() {
      return Arrays.asList(
            COLOR_GREEN_STRING,
            COLOR_RED_STRING,
            COLOR_GREY_STRING,
            COLOR_BLUE_GAZELLE_STRING,
            COLOR_YELLOW_STRING,
            COLOR_ORANGE_STRING);
   }

   public List<String> getColorsForRemainingWorkForChart() {
      return Arrays.asList(
            COLOR_ORANGE_STRING,
            COLOR_BLUE_GAZELLE_STRING,
            COLOR_RED_STRING,
            COLOR_YELLOW_STRING);
   }

   public List<String> getColorsForLeftOverForChart() {
      return Arrays.asList(
            COLOR_YELLOW_STRING,
            COLOR_ORANGE_STRING,
            COLOR_RED_STRING);
   }

   public List<String> getColorsForTestInstancesForMonitorForChart() {
      return Arrays.asList(
            COLOR_GREEN_STRING,
            COLOR_RED_STRING,
            COLOR_GREY_STRING,
            COLOR_BLUE_GAZELLE_STRING,
            COLOR_ORANGE_STRING);
   }
}
