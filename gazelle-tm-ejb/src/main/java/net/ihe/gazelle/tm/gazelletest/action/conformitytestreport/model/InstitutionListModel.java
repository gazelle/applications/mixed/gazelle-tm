package net.ihe.gazelle.tm.gazelletest.action.conformitytestreport.model;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "organizations")
@XmlAccessorType(XmlAccessType.NONE)
public class InstitutionListModel {

    @XmlElements({@XmlElement(name = "organization", type = InstitutionModel.class)})
    private List<InstitutionModel> institutions = new ArrayList<>();

    public List<InstitutionModel> getInstitutions() {
        return institutions;
    }

    public void setInstitutions(List<InstitutionModel> institutions) {
        this.institutions = institutions;
    }
}
