package net.ihe.gazelle.tm.testexecution;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManagerImpl;
import org.apache.commons.lang.builder.CompareToBuilder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class TestReport implements Comparable<TestReport> {
    private String testCase;
    private String type;
    private String peerType;
    private OptionalityStatus optionality;
    private Integer target;
    private List<TestRunReport> testRunReports;
    private String descriptionLink;
    private String summary;
    private boolean isMetatest;
    private int systemInSessionId;
    private int testRolesId;
    private List<TestReport> equivalentTestReports = new ArrayList<>();
    private List<List<PartnerReport>> equivalentTestReportPartners = new ArrayList<>();

    public boolean isInMetatest() {
        return inMetatest;
    }

    public void setInMetatest(boolean inMetatest) {
        this.inMetatest = inMetatest;
    }

    private boolean inMetatest;
    private final ApplicationPreferenceManager applicationPreferenceManager = new ApplicationPreferenceManagerImpl();
    private List<PartnerReport> partnerReports = new ArrayList<>();

    private enum OptionalityStatus {

        REQUIRED(1, "Required","net.ihe.gazelle.tm.Required"),
        OPTIONAL(2, "Optional","net.ihe.gazelle.tm.Optional"),
        UNSPECIFIED(100, "unspecified","unspecified");
        private int rank;
        private String keyword;
        private String labelToDisplay;


        OptionalityStatus(int rank,String keyword, String labelToDisplay) {
            this.rank = rank;
            this.keyword=keyword;
            this.labelToDisplay = labelToDisplay;
        }

        public int getRank() {
            return rank;
        }

        public String getLabelToDisplay() {
            return labelToDisplay;
        }

        public String getKeyword() {
            return keyword;
        }

        public static OptionalityStatus getStatusByKeyword(String keyword) {
            OptionalityStatus[] tab = values();
            if (keyword != null) {
                for (int i = 0; i < tab.length; i++) {
                    if (tab[i].getKeyword().equals(keyword)) {
                        return tab[i];
                    }
                }
            }
            return UNSPECIFIED;
        }
    }

    public int getSystemInSessionId() {
        return systemInSessionId;
    }

    public void setSystemInSessionId(int systemInSessionId) {
        this.systemInSessionId = systemInSessionId;
    }

    public int getTestRolesId() {
        return testRolesId;
    }

    public void setTestRolesId(int testRolesId) {
        this.testRolesId = testRolesId;
    }

    public String getTestCase() {
        return testCase;
    }

    public void setTestCase(String testCase) {
        this.testCase = testCase;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPeerType() {
        return peerType;
    }

    public void setPeerType(String peerType) {
        this.peerType = peerType;
    }

    public String getOptionalityLabel() {
        return optionality.getLabelToDisplay();
    }
    public String getOptionalityKeyword() {
        return optionality.getKeyword();
    }
    public void setOptionality(String optionality) {
        this.optionality = OptionalityStatus.getStatusByKeyword(optionality);
    }

    public Integer getTarget() {
        return target;
    }

    public void setTarget(Integer target) {
        this.target = target;
    }

    public List<TestRunReport> getTestRunReports() {
        return testRunReports;
    }

    public void setTestRunReports(List<TestRunReport> testRunReports) {
        this.testRunReports = testRunReports;
    }

    public List<List<PartnerReport>> getEquivalentTestReportPartners() {
        return equivalentTestReportPartners;
    }

    public void setDescriptionLink(int testId) {
        if (isMetatest) {
            this.descriptionLink = applicationPreferenceManager.getApplicationUrl() + "testing/metatest/metaTest.seam?id=" + testId;
        } else {
            this.descriptionLink = applicationPreferenceManager.getApplicationUrl() + "test.seam?id=" + testId;
        }
    }

    public List<PartnerReport> getPartnerReports() {
        return partnerReports;
    }

    public void setPartnerReports(List<PartnerReport> partnerReports) {
        this.partnerReports = partnerReports;
    }

    public String getDescriptionLink() {
        return descriptionLink;
    }

    public boolean isMetatest() {
        return isMetatest;
    }

    public void setMetatest(boolean metatest) {
        isMetatest = metatest;
    }

    public List<TestReport> getEquivalentTestReports() {
        return equivalentTestReports;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public int getNumberOfTestRunVerified() {
        int testRunVerified = 0;
        for (TestRunReport testRunReport : this.testRunReports) {
            if (testRunReport.getStatus() == TestRunStatus.VERIFIED || testRunReport.getStatus() == TestRunStatus.SELF_VERIFIED) {
                testRunVerified++;
            }
        }
        return testRunVerified;
    }


    public boolean addEquivalentTestReports(TestReport equivalentTestReport) {
        return this.equivalentTestReports.add(equivalentTestReport);
    }

    public void addAllTestRunReport(List<TestRunReport> testRunReports) {
        this.testRunReports.addAll(testRunReports);
        Collections.sort(this.testRunReports);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TestReport that = (TestReport) o;
        return isMetatest == that.isMetatest && Objects.equals(testCase, that.testCase) && Objects.equals(type, that.type) && Objects.equals(optionality, that.optionality) && Objects.equals(target, that.target) && Objects.equals(descriptionLink, that.descriptionLink) && Objects.equals(summary, that.summary);
    }

    @Override
    public int hashCode() {
        return Objects.hash(testCase, type, optionality, target, descriptionLink, summary, isMetatest);
    }

    @Override
    public int compareTo(TestReport other) {
        return new CompareToBuilder().append(this.type, other.getType())
                .append(this.testCase, other.getTestCase())
                .append(this.optionality.getRank(), other.optionality.getRank())
                .toComparison();

    }
}
