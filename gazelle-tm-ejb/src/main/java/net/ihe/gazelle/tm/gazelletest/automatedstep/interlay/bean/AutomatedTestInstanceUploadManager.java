package net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.bean;

import net.ihe.gazelle.common.fineuploader.FineuploaderListener;
import net.ihe.gazelle.common.servletfilter.FileGenerator;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.instance.service.AutomatedTestStepInstanceInputService;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.*;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.kohsuke.MetaInfServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

@Name("automatedTestInstanceUploadManager")
@Scope(ScopeType.APPLICATION)
@Synchronized(timeout = 10000)
@MetaInfServices(FileGenerator.class)
public class AutomatedTestInstanceUploadManager implements FileGenerator, FineuploaderListener {

    private static final Logger LOG = LoggerFactory.getLogger(AutomatedTestInstanceUploadManager.class);
    @In(value = "automatedTestStepInstanceInputService")
    private AutomatedTestStepInstanceInputService instanceInputService;
    @In
    private GazelleIdentity identity;

    @Override
    @Transactional
    public void uploadedFile(File tmpFile, String filename, String automatedTestStepInstanceInputId, String testStepsInstanceId) throws IOException {
        if (LOG.isDebugEnabled()) {
            LOG.debug("saveFileUploaded");
        }
        if (identity.isLoggedIn()) {
            try {
                byte[] bytes = Files.readAllBytes(Paths.get(tmpFile.getPath()));
                String fileContent = new String(bytes, StandardCharsets.UTF_8);
                Files.delete(tmpFile.toPath());
                instanceInputService.saveInputData(Integer.valueOf(automatedTestStepInstanceInputId), Integer.valueOf(testStepsInstanceId), filename, fileContent);
            } catch (IOException e) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Could not read file");
            }
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Not logged, refresh the page.");
        }
    }

    @Override
    public String getPath() {
        return "/filesteps";
    }

    @Override
    public void process(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
        // nothing to process
    }
}
