package net.ihe.gazelle.tm.messages;

import net.ihe.gazelle.tm.messages.model.Message;

/**
 * Notification produced by the application will be dispatch to all classes implementing this interface
 */
public interface NotificationListener {

    /**
     * Send the notification to all concerned user.
     * @param message Message to send to all concerned user
     */
    void sendNotification(Message message);
}
