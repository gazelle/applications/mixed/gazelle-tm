package net.ihe.gazelle.tm.messages;

import net.ihe.gazelle.tm.messages.model.Message;
import org.jboss.seam.Component;
import org.kohsuke.MetaInfServices;

@MetaInfServices(NotificationListener.class)
public class EmailNotificationManagerSPI implements NotificationListener {

   private final EmailNotificationManager emailNotificationManager;

   public EmailNotificationManagerSPI() {
      emailNotificationManager = new EmailNotificationManager(
            (MessageFormatter) Component.getInstance("messageFormatter"));
   }

   @Override
   public void sendNotification(Message message) {
      emailNotificationManager.sendNotification(message);
   }

}
