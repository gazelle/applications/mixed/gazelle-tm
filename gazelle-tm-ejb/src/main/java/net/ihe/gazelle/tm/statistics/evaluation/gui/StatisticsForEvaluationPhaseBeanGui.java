package net.ihe.gazelle.tm.statistics.evaluation.gui;

import net.ihe.gazelle.menu.Pages;
import net.ihe.gazelle.ssov7.gum.client.application.Role;
import net.ihe.gazelle.tf.model.Domain;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstance;
import net.ihe.gazelle.tm.statistics.common.StatisticsCommonService;
import net.ihe.gazelle.tm.statistics.evaluation.StatisticsForEvaluationPhaseService;
import net.ihe.gazelle.tm.systems.model.System;
import net.ihe.gazelle.tm.systems.model.SystemInSession;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.security.Identity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Scope(ScopeType.EVENT)
@AutoCreate
@Name(value = "statisticsForEvaluationPhaseBeanGui")
public class StatisticsForEvaluationPhaseBeanGui implements Serializable {

    public static final String COLOR_RED_STRING = "\"red\"";
    public static final String COLOR_GREEN_STRING = "\"#5cb85c\"";
    public static final String COLOR_ORANGE_STRING = "\"orange\"";
    public static final String COLOR_GREY_STRING = "\"#C1C1C1\"";
    public static final String COLOR_BLUE_GAZELLE_STRING = "\"#4ca8de\"";
    public static final String COLOR_YELLOW_STRING = "\"#FDE74C\"";
    @In(value = "statisticsForEvaluationPhaseService")
    private StatisticsForEvaluationPhaseService statisticsForEvaluationPhaseService;

    public List<Integer> getCapabilitiesStatisticsForASystemInSession(SystemInSession systemInSession) {
        StatisticsForEvaluationPhaseService.CapabilityStats capabilityStats = statisticsForEvaluationPhaseService.getCapabilitiesStatisticsForASystemInSession(systemInSession);
        int nbNotEvaluated = capabilityStats.getNbOfNotEvaluated() + capabilityStats.getNbOfNoPeer();
        return new ArrayList<>(Arrays.asList(
                capabilityStats.getNbOfPassed(),
                capabilityStats.getNbOfDidNotComplete(),
                capabilityStats.getNbOfAtRisk(),
                nbNotEvaluated,
                capabilityStats.getNbOfEvaluationPending()
        ));
    }

    public List<Integer> getStatisticsForAllTestInstances() {
        List<TestInstance> testInstances = statisticsForEvaluationPhaseService.getAllTestInstancesForCurrentSession();
        StatisticsCommonService.TestInstanceStats testInstanceStats = statisticsForEvaluationPhaseService.getStatisticsForTestInstances(testInstances);

        return new ArrayList<>(Arrays.asList(
                testInstanceStats.getNbOfVerified(),
                testInstanceStats.getNbOfFailed(),
                testInstanceStats.getNbOfInProgress(),
                testInstanceStats.getNbOfTobVerified(),
                testInstanceStats.getNbOfPartiallyVerified(),
                testInstanceStats.getNbOfCritical()));
    }

    public int getNbTotalTestInstancesForCurrentSession() {
        return statisticsForEvaluationPhaseService.getNbTotalTestInstancesForCurrentSession();
    }

    public int getNbTotalCapabilitiesForADomain(Domain domain) {
        return statisticsForEvaluationPhaseService.getNbTotalCapabilitiesForADomain(domain);
    }

    public List<SystemInSession> getSystemsForCurrentUser() {
        return statisticsForEvaluationPhaseService.getSystemsForCurrentUser();
    }

    public List<Domain> getAllPossibleDomains() {
        return Domain.getPossibleDomains();
    }

    public List<Integer> getCapabilitiesStatisticsForADomain(Domain domain) {
        StatisticsForEvaluationPhaseService.CapabilityStats capabilityStats = statisticsForEvaluationPhaseService.getCapabilitiesStatisticsForADomain(domain);
        if (!capabilityStats.isEmpty()) {
            int nbNotEvaluated = capabilityStats.getNbOfNotEvaluated() + capabilityStats.getNbOfNoPeer();
            return new ArrayList<>(Arrays.asList(
                    capabilityStats.getNbOfPassed(),
                    capabilityStats.getNbOfDidNotComplete(),
                    capabilityStats.getNbOfAtRisk(),
                    nbNotEvaluated,
                    capabilityStats.getNbOfEvaluationPending()

            ));
        }
        return new ArrayList<>();
    }

    public boolean isVendor() {
        return Identity.instance().hasRole(Role.VENDOR) || Identity.instance().hasRole(Role.VENDOR_ADMIN);
    }

    public boolean isTestingSessionManagerOrAdmin() {
        return Identity.instance().hasRole(Role.TESTING_SESSION_ADMIN) || Identity.instance().hasRole(Role.ADMIN);
    }

    public String getEvaluationLink(System system) {
        return Pages.CAT_RESULTS.getMenuLink() + "?system=" + system.getId();
    }

    public List<String> getColorsForCapabilitiesForChart() {
        return Arrays.asList(
                COLOR_GREEN_STRING,
                COLOR_RED_STRING,
                COLOR_ORANGE_STRING,
                COLOR_GREY_STRING,
                COLOR_BLUE_GAZELLE_STRING);
    }

    public List<String> getColorsForTestInstancesForChart() {
        return Arrays.asList(
                COLOR_GREEN_STRING,
                COLOR_RED_STRING,
                COLOR_GREY_STRING,
                COLOR_BLUE_GAZELLE_STRING,
                COLOR_YELLOW_STRING,
                COLOR_ORANGE_STRING);
    }

}
