package net.ihe.gazelle.tm.systems;

import net.ihe.gazelle.tf.model.Domain;
import net.ihe.gazelle.tf.model.IntegrationProfile;
import net.ihe.gazelle.tm.systems.model.ProfileInTestingSession;
import net.ihe.gazelle.tm.systems.model.Testability;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import java.util.List;

@Name(value = "profileInTestingSessionService")
@AutoCreate
@Scope(ScopeType.STATELESS)
public class ProfileInTestingSessionServiceImpl implements ProfileInTestingSessionService {

    private static final long serialVersionUID = -3876889706306168045L;
    @In(value = "profileInTestingSessionDAO")
    ProfileInTestingSessionDAO profileInTestingSessionDAO;


    @Override
    public void saveProfileInTestingSession(Domain domain, IntegrationProfile integrationProfile,TestingSession selectedTestingSession) {

        if (isProfileInTestingSessionAlreadyCreated(domain, integrationProfile,selectedTestingSession)){
            profileInTestingSessionDAO.saveProfileInTestingSession(constructProfileInTestingSession(domain, integrationProfile,selectedTestingSession));
        }
    }

    @Override
    public void setTestabilityRemovedFromSession(List<IntegrationProfile> integrationProfiles, TestingSession selectedTestingSession) {
        List<ProfileInTestingSession> profileInTestingSessions = profileInTestingSessionDAO.getAllProfileInTestingSessionsForCurrentSession(selectedTestingSession);
        for (ProfileInTestingSession profileInTestingSession : profileInTestingSessions){
            if(!integrationProfiles.contains(profileInTestingSession.getIntegrationProfile())){
                profileInTestingSession.setTestability(Testability.REMOVED_FROM_SESSION);
                profileInTestingSessionDAO.updateProfileInTestingSession(profileInTestingSession);
            }
        }
    }

    private boolean isProfileInTestingSessionAlreadyCreated(Domain domain, IntegrationProfile integrationProfile,TestingSession selectedTestingSession) {
        return profileInTestingSessionDAO.isProfileInTestingSessionAlreadyCreated(domain, integrationProfile, selectedTestingSession);
    }
    private ProfileInTestingSession constructProfileInTestingSession(Domain domain,IntegrationProfile integrationProfile,TestingSession selectedTestingSession){

        return new ProfileInTestingSession(selectedTestingSession,integrationProfile,domain);
    }
}
