package net.ihe.gazelle.tm.gazelletest.action;

public interface UserManagementURIService {

    String getUserManagementURI();

    String getUserManagementURIFilteredByOrganizationId(String organizationId);
}
