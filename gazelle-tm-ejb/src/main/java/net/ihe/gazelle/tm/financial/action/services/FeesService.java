package net.ihe.gazelle.tm.financial.action.services;

import net.ihe.gazelle.tm.financial.FinancialSummary;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import net.ihe.gazelle.users.model.Institution;

import java.math.BigDecimal;

public interface FeesService {

    FinancialSummary getFees(TestingSession testingSession, Institution institution);

    FinancialSummary getFees(Integer sessionId, Integer organizationId);

    BigDecimal getTotalToPay(FinancialSummary financialSummary);
}
