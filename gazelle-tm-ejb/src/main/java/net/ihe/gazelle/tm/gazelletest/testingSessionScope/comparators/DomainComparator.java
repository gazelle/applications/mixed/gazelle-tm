package net.ihe.gazelle.tm.gazelletest.testingSessionScope.comparators;

import net.ihe.gazelle.tm.gazelletest.testingSessionScope.ProfileScope;

import java.util.Comparator;

public class DomainComparator implements Comparator<ProfileScope> {
    @Override
    public int compare(ProfileScope profileScope1, ProfileScope profileScope2) {
        return profileScope1.getProfileInTestingSession().getDomain().getName().compareTo(profileScope2.getProfileInTestingSession().getDomain().getName());
    }
}
