package net.ihe.gazelle.tm.gazelletest.automatedstep.domain.script;

import java.util.ArrayList;
import java.util.List;

public class MaestroRequest {

    private String clientCallback;
    private String callbackAuthorization;
    private Script script;
    private List<InputContent> inputs;

    public MaestroRequest() {
        // empty for serialization
    }

    public MaestroRequest(String callbackAuthorization, Script script, List<InputContent> inputs) {
        this.callbackAuthorization = callbackAuthorization;
        this.script = script;
        this.inputs = new ArrayList<>(inputs);
    }

    public String getClientCallback() {
        return clientCallback;
    }

    public void setClientCallback(String clientCallback) {
        this.clientCallback = clientCallback;
    }

    public String getCallbackAuthorization() {
        return callbackAuthorization;
    }

    public void setCallbackAuthorization(String callbackAuthorization) {
        this.callbackAuthorization = callbackAuthorization;
    }

    public Script getScript() {
        return script;
    }

    public void setScript(Script script) {
        this.script = script;
    }

    public List<InputContent> getInputs() {
        return new ArrayList<>(inputs);
    }

    public void setInputs(List<InputContent> inputs) {
        this.inputs = new ArrayList<>(inputs);
    }
}
