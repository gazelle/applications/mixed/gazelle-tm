package net.ihe.gazelle.tm.comtool.application;

import net.ihe.gazelle.ssov7.gum.client.application.User;
import net.ihe.gazelle.tm.gazelletest.model.instance.MonitorInSession;
import net.ihe.gazelle.tm.gazelletest.model.instance.Status;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstance;

import javax.ejb.Local;

@Local
public interface ComToolManager {

   // used by TestInstanceManager
   void archiveTestRunChannel(TestInstance testInstance, String testStatus, String lastTestStatus);

   // used by StartTestInstance
   void createTestRunChannel(TestInstance testInstance, User creator);

   // used by TestInstanceManager
   String getInvitationLink(TestInstance testInstance);

   // used by testInstance.xhtml
   boolean isGctEnabled();

   // used by ComToolListener
   void sendTestRunComment(TestInstance testInstance, String username, String inputComment);

   // used by ComToolListener
   void testRunMonitorChanged(TestInstance testInstance, MonitorInSession oldMonitor, MonitorInSession newMonitor);

   // used by ComToolListener
   void testRunStatusChanged(TestInstance testInstance, Status oldStatus, Status newStatus);

   // used by TestInstanceManager
   void unarchiveTestRunChannel(TestInstance testInstance, String testStatus, String lastTestStatus);

   // used by preferenceSectionTM.xhtml
   void updateTestingSessionsUsersAndChannels();

}
