package net.ihe.gazelle.tm.statistics.registration;

import net.ihe.gazelle.tm.statistics.common.StatisticsCommonService;
import net.ihe.gazelle.tm.systems.model.SystemInSession;
import net.ihe.gazelle.tm.systems.model.SystemInSessionRegistrationStatus;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import java.util.List;

@Scope(ScopeType.STATELESS)
@AutoCreate
@Name(value = "statisticsForRegistrationPhaseService")
public class StatisticsForRegistrationPhaseServiceImpl implements StatisticsForRegistrationPhaseService {

    @In(value = "statisticsForRegistrationPhaseDAO")
    StatisticsForRegistrationPhaseDAO statisticsForRegistrationPhaseDAO;

    @In(value = "statisticsCommonService")
    StatisticsCommonService statisticsCommonService;

    @Override
    public int getNumberOfSessionParticipants() {
        return statisticsCommonService.getNumberOfSessionParticipants();
    }

    @Override
    public int getNumberOfRegisteredInstitution() {
        return statisticsCommonService.getNumberOfRegisteredInstitutionWithAttendee();
    }

    @Override
    public int getNumberOfRegisteredSystemInSession() {
        return statisticsCommonService.getNumberOfRegisteredSystemInSession();
    }

    @Override
    public int getNumberOfIntegrationProfilesForCurrentSession() {
        return statisticsCommonService.getNumberOfIntegrationProfilesForCurrentSession();
    }

    @Override
    public int getNumberOfDomainsForCurrentSession() {
        return statisticsCommonService.getNumberOfDomainsForCurrentSession();
    }


    @Override
    public SystemInSessionStats getSystemInSessionStatisticsForCurrentTestingSession() {
        List<SystemInSession> systemInSessions = statisticsForRegistrationPhaseDAO.getAllSystemForCurrentTestingSession();

        int nbAccepted = 0;
        int nbDropped = 0;
        int nbInProgress = 0;
        int nbCompleted = 0;

        for (SystemInSession systemInSession : systemInSessions) {
            if (systemInSession.getAcceptedToSession()) {
                nbAccepted++;
            } else {
                if (systemInSession.getRegistrationStatus() == SystemInSessionRegistrationStatus.DROPPED) {
                    nbDropped++;
                }
                if (systemInSession.getRegistrationStatus() == SystemInSessionRegistrationStatus.IN_PROGRESS) {
                    nbInProgress++;
                }
                if (systemInSession.getRegistrationStatus() == SystemInSessionRegistrationStatus.COMPLETED) {
                    nbCompleted++;
                }
            }

        }
        return new SystemInSessionStats(nbAccepted, nbDropped, nbInProgress, nbCompleted);
    }

    @Override
    public StatisticsCommonService.ProfileInTestingSessionStats getProfileInTestingSessionStatsForCurrentSession() {
        return statisticsCommonService.getProfileInTestingSessionStatsForCurrentSession();
    }
}
