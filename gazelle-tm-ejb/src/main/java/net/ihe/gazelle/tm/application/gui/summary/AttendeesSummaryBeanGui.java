package net.ihe.gazelle.tm.application.gui.summary;


import net.ihe.gazelle.tm.organization.OrganizationService;
import net.ihe.gazelle.tm.session.AttendeeService;
import net.ihe.gazelle.tm.session.TestingSessionService;
import net.ihe.gazelle.tm.systems.SystemUnderTestService;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import net.ihe.gazelle.users.model.Institution;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;

import java.io.Serializable;

@Name("attendeesSummaryBeanGui")
@Scope(ScopeType.PAGE)
public class AttendeesSummaryBeanGui implements Serializable {


    private static final long serialVersionUID = -7012794709166345540L;

    @In(value = "testingSessionService",create = true)
    private TestingSessionService testingSessionService;

    @In(value = "systemUnderTestService",create = true)
    private SystemUnderTestService systemUnderTestService;

    @In(value = "organizationService", create = true)
    private OrganizationService organizationService;

    @In(value = "attendeeService", create = true)
    private AttendeeService attendeeService;

    private TestingSession currentTestingSession;

    private Institution currentInstitution;


    /////////////////// init ///////////////////

    @Create
    public void init(){
        try {
            this.currentTestingSession = testingSessionService.getUserTestingSession();
            this.currentInstitution = organizationService.getCurrentOrganization();
        }
        catch (IllegalArgumentException e){
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "An error has occurred during initialization");
        }
    }


    /////////////////// Data Retrievers ///////////////////

    public long getNumberOfAcceptedSystems(){
        return systemUnderTestService.getNumberOfAcceptedSystems(currentTestingSession, currentInstitution);
    }

    public long getNumberOfRegisteredAttendees(){
        return attendeeService.getNumberOfRegisteredAttendees(currentTestingSession, currentInstitution);
    }

    /////////////////// Checkers ///////////////////

    public boolean isRegistrationEnabled(){
        return testingSessionService.isAttendeesRegistrationEnabled(currentTestingSession);
    }

    /////////////////// Link ///////////////////

    public String yourRegistrationButton(){
        return "/registration/registration.seam";
    }

    public String listAvailablePartnersButton(){
        return "/systems/listSystems.seam";
    }


}
