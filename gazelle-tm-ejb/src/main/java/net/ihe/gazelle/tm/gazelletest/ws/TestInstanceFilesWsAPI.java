package net.ihe.gazelle.tm.gazelletest.ws;

import javax.ejb.Local;
import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Local
@Path("/testinstances/files")
public interface TestInstanceFilesWsAPI {



    @POST
    @Path("/logs")
    @Consumes("application/octet-stream")
    Response uploadTestLogs(@HeaderParam("authorization") String authorizationHeader, byte[] testLogs) ;

    @POST
    @Path("/report")
    @Consumes({"application/xml","application/octet-stream"})
    Response uploadTestReport(@HeaderParam("authorization") String authorizationHeader, byte[] testReport);
}
