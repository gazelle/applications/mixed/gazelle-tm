package net.ihe.gazelle.tm.gazelletest.action.conformitytestreport.model;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "testingSessions")
@XmlAccessorType(XmlAccessType.NONE)
public class TestingSessionListModel {

    @XmlElements({@XmlElement(name = "testingSession", type = TestingSessionModel.class)})
    private List<TestingSessionModel> testingSession = new ArrayList<>();

    public List<TestingSessionModel> getTestingSession() {
        return testingSession;
    }

    public void setTestingSession(List<TestingSessionModel> testingSession) {
        this.testingSession = testingSession;
    }
}
