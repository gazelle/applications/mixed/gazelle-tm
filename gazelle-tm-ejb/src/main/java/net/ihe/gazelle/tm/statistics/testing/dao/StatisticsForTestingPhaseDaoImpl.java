package net.ihe.gazelle.tm.statistics.testing.dao;

import net.ihe.gazelle.tm.gazelletest.model.instance.MonitorInSession;
import net.ihe.gazelle.tm.gazelletest.model.instance.Status;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstance;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstanceParticipantsQuery;
import net.ihe.gazelle.tm.statistics.common.StatisticsCommonService;
import net.ihe.gazelle.tm.statistics.testing.StatisticsForTestingPhaseDao;
import net.ihe.gazelle.tm.systems.model.SystemInSession;
import net.ihe.gazelle.tm.systems.model.SystemInSessionQuery;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import net.ihe.gazelle.users.model.Institution;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import java.util.Arrays;
import java.util.List;

@Scope(ScopeType.STATELESS)
@AutoCreate
@Name(value = "statisticsForTestingPhaseDao")
public class StatisticsForTestingPhaseDaoImpl implements StatisticsForTestingPhaseDao {

    @In(value = "statisticsCommonService")
    StatisticsCommonService statisticsCommonService;

    @Override
    public List<SystemInSession> getSystemsForCurrentUser(TestingSession selectedTestingSession) {
        SystemInSessionQuery systemInSessionQuery = new SystemInSessionQuery();
        systemInSessionQuery.acceptedToSession().eq(true);
        systemInSessionQuery.system().isTool().eq(false);
        systemInSessionQuery.testingSession().eq(selectedTestingSession);
        systemInSessionQuery.system().institutionSystems().institution().eq(Institution.getLoggedInInstitution());

        return systemInSessionQuery.getListDistinct();
    }

    @Override
    public List<TestInstance> getTestInstancesForASystem(SystemInSession system) {
        TestInstanceParticipantsQuery testInstanceParticipantsQuery = getTestInstanceParticipantsQueryForASystemInSession(system);
        return testInstanceParticipantsQuery.testInstance().getListDistinct();
    }

    private TestInstanceParticipantsQuery getTestInstanceParticipantsQueryForASystemInSession(SystemInSession system) {
        TestInstanceParticipantsQuery testInstanceParticipantsQuery = statisticsCommonService.getTestInstanceParticipantsQuery();
        testInstanceParticipantsQuery.systemInSessionUser().systemInSession().eq(system);
        testInstanceParticipantsQuery.systemInSessionUser().systemInSession().system().keyword().order(true, true);
        return testInstanceParticipantsQuery;
    }

    public List<TestInstance> getAllTestInstancesForCurrentSession() {
        TestInstanceParticipantsQuery testInstanceParticipantsQuery = statisticsCommonService.getTestInstanceParticipantsQuery();
        return testInstanceParticipantsQuery.testInstance().getListDistinct();
    }

    @Override
    public List<TestInstance> getTestInstancesWithVerificationPending() {
        TestInstanceParticipantsQuery testInstanceParticipantsQuery = statisticsCommonService.getTestInstanceParticipantsQuery();
        testInstanceParticipantsQuery.testInstance().lastStatus().in(Arrays.asList(Status.CRITICAL, Status.PARTIALLY_VERIFIED, Status.COMPLETED));
        return testInstanceParticipantsQuery.testInstance().getListDistinct();
    }

    @Override
    public List<TestInstance> getLeftOverTestInstances() {
        TestInstanceParticipantsQuery testInstanceParticipantsQuery = statisticsCommonService.getTestInstanceParticipantsQuery();
        testInstanceParticipantsQuery.testInstance().lastStatus().in(Arrays.asList(Status.CRITICAL, Status.COMPLETED));
        return testInstanceParticipantsQuery.testInstance().getListDistinct();
    }

    @Override
    public List<TestInstance> getTestInstancesForMonitorInSession(MonitorInSession monitor) {
        TestInstanceParticipantsQuery testInstanceParticipantsQuery = statisticsCommonService.getTestInstanceParticipantsQuery();
        testInstanceParticipantsQuery.testInstance().monitorInSession().id().eq(monitor.getId());
        return testInstanceParticipantsQuery.testInstance().getListDistinct();
    }

    @Override
    public int getNumberOfTotalTestInstanceForASystemInSession(SystemInSession system) {
        return getTestInstanceParticipantsQueryForASystemInSession(system).testInstance().getCountDistinctOnPath();
    }

    @Override
    public int getNumberOfTotalTestInstancesForMonitorInSession(MonitorInSession monitor) {
        TestInstanceParticipantsQuery testInstanceParticipantsQuery = statisticsCommonService.getTestInstanceParticipantsQuery();
        testInstanceParticipantsQuery.testInstance().monitorInSession().id().eq(monitor.getId());
        return testInstanceParticipantsQuery.testInstance().getCountDistinctOnPath();
    }

    public int getNumberOfTotalTestInstances() {
        TestInstanceParticipantsQuery testInstanceParticipantsQuery = statisticsCommonService.getTestInstanceParticipantsQuery();
        return testInstanceParticipantsQuery.testInstance().getCountDistinctOnPath();
    }

    @Override
    public int getNumberOfVerificationPendingTestInstances() {
        TestInstanceParticipantsQuery testInstanceParticipantsQuery = statisticsCommonService.getTestInstanceParticipantsQuery();
        testInstanceParticipantsQuery.testInstance().lastStatus().in(Arrays.asList(Status.CRITICAL, Status.PARTIALLY_VERIFIED, Status.COMPLETED));
        return testInstanceParticipantsQuery.testInstance().getCountDistinctOnPath();
    }


}
