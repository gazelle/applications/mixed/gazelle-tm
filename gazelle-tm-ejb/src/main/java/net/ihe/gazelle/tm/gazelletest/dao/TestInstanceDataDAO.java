package net.ihe.gazelle.tm.gazelletest.dao;

import net.ihe.gazelle.tm.gazelletest.action.TestInstanceNotFoundException;
import net.ihe.gazelle.tm.gazelletest.action.TestStepDataNotFoundException;
import net.ihe.gazelle.tm.gazelletest.automatedstep.domain.instance.AutomatedTestStepInstanceData;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstance;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestStepsData;

import java.io.IOException;

public interface TestInstanceDataDAO {

    TestStepsData createTestStepData(TestStepsData testStepsData);

    TestStepsData createAutomatedTestStepData(String fileName, Integer testStepsInstanceId);

    TestStepsData instantiateTestStepDataFile(String userId, String fileName);

    TestInstance updateTestInstance(TestInstance testInstance);

    TestInstance getTestInstance(Integer testInstanceId) throws TestInstanceNotFoundException;

    void removeTestStepsDataFromTestStepsInstance(AutomatedTestStepInstanceData inputData);

    TestStepsData getTestStepDataById(Integer testStepsDataId) throws TestStepDataNotFoundException;

    void updateTestInstanceAndAddTestStepData(TestStepsData testStepData, Integer instanceId, String userId) throws TestInstanceNotFoundException;

    void updateTestInstanceAndTestStepDataForLogs(TestStepsData testStepData, Integer instanceId, String userId) throws TestInstanceNotFoundException;

    void saveFileInTestStepsData(Integer testStepsDataId, byte[] bytesInput) throws IOException;

    String getPath(String testStepsDataId);
}
