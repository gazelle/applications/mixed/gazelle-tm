package net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.instance.dao;

import net.ihe.gazelle.tm.gazelletest.domain.AutomatedTestStepInputDomain;
import net.ihe.gazelle.tm.gazelletest.model.instance.AutomatedTestStepInstanceInput;
import net.ihe.gazelle.tm.gazelletest.model.instance.AutomatedTestStepInstanceInputQuery;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstance;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestStepsInstance;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

@Name("automatedTestStepInstanceInputDAO")
@Scope(ScopeType.EVENT)
@AutoCreate
public class AutomatedTestStepInstanceInputDAOImpl implements AutomatedTestStepInstanceInputDAO {

    @In
    private EntityManager entityManager;

    @Override
    public AutomatedTestStepInstanceInput createAutomatedTestStepInstanceInput(AutomatedTestStepInputDomain inputDefinition, Integer testStepsInstanceId) {
        AutomatedTestStepInstanceInput inputInstance = new AutomatedTestStepInstanceInput(testStepsInstanceId, inputDefinition);
        inputInstance = entityManager.merge(inputInstance);
        entityManager.flush();
        return inputInstance;
    }

    @Override
    public List<AutomatedTestStepInstanceInput> getInstanceInputWithoutData(Integer testStepsInstanceId) {
        AutomatedTestStepInstanceInputQuery query = new AutomatedTestStepInstanceInputQuery();
        query.testStepsInstanceId().eq(testStepsInstanceId);
        return query.getList();
    }

    @Override
    public List<AutomatedTestStepInstanceInput> getInstanceInputForTestInstance(TestInstance testInstance) {
        List<AutomatedTestStepInstanceInput> instanceInputList = new ArrayList<>();
        for (TestStepsInstance testStepsInstance : testInstance.getTestStepsInstanceList()) {
            AutomatedTestStepInstanceInputQuery query = new AutomatedTestStepInstanceInputQuery();
            query.testStepsInstanceId().eq(testStepsInstance.getId());
            query.getQueryBuilder().addOrder("id", true);
            instanceInputList.addAll(query.getList());
        }
        return instanceInputList;
    }

    @Override
    public void setTestStepDataIdToInstanceInput(Integer automatedTestStepInstanceInputId, Integer automatedTestStepDataId) {
        AutomatedTestStepInstanceInput input = entityManager.find(AutomatedTestStepInstanceInput.class, automatedTestStepInstanceInputId);
        input.setTestStepDataId(automatedTestStepDataId);
        entityManager.merge(input);
        entityManager.flush();
    }
}
