package net.ihe.gazelle.tm.application.action;

public enum SectionsTypePage {

    ANNOUNCEMENT("announcement","_section_announcement.xhtml"),
    DOCUMENTATION("documentation","_section_documentation.xhtml"),
    TOOL_INDEX("tool_index","_section_tool_index.xhtml"),
    DEFAULT("default","_section_default.xhtml");

    private final String name;
    private final String page;

    SectionsTypePage(String name, String page) {
        this.name = name;
        this.page = page;
    }

    public String getValue() {
        return name;
    }

    public String getPage() {
        return page;
    }

    public static SectionsTypePage of(String name){
        for(SectionsTypePage sectionsTypePage : values()){
            if(sectionsTypePage.name.equals(name)){
                return sectionsTypePage;
            }
        }
        return DEFAULT;
    }
}
