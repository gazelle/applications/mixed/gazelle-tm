package net.ihe.gazelle.tm.gazelletest.action;

import java.io.Serializable;

public class TestInstanceClosedException extends Exception  implements Serializable {

    public TestInstanceClosedException() {
    }

    public TestInstanceClosedException(String s) {
        super(s);
    }

    public TestInstanceClosedException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public TestInstanceClosedException(Throwable throwable) {
        super(throwable);
    }
}
