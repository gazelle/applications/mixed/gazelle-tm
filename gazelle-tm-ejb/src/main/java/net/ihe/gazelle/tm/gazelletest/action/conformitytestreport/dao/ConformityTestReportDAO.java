package net.ihe.gazelle.tm.gazelletest.action.conformitytestreport.dao;

import net.ihe.gazelle.tm.gazelletest.action.conformitytestreport.model.TestInstanceReport;
import net.ihe.gazelle.tm.gazelletest.bean.SAPResultDetailItem;
import net.ihe.gazelle.tm.gazelletest.model.instance.SystemAIPOResultForATestingSession;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstance;
import net.ihe.gazelle.tm.systems.model.System;
import net.ihe.gazelle.tm.systems.model.SystemInSession;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import net.ihe.gazelle.users.model.Institution;

import java.util.List;

public interface ConformityTestReportDAO {

    List<SAPResultDetailItem> computeDetail(SystemAIPOResultForATestingSession systemAIPOResult);
    List<SAPResultDetailItem> computeDetail(SystemAIPOResultForATestingSession systemAIPOResult, boolean isInteroperability);
    List<TestInstanceReport> generateTestInstanceItem(List<TestInstance> tiList);
    List<SystemAIPOResultForATestingSession> getResultOfSystem(System system, TestingSession testingSession);
    List<Institution> getInstitutionQueryList();
    List<Institution> getInstituionList();
    Institution findInstituion(String name);
    Institution findInstitutionById(Integer id);
    List<SystemInSession> getSystemsInSessionForSession(TestingSession session);
    List<SystemInSession> getSystemsInSessionForCompanyNameForSession(String name, TestingSession session);
    SystemInSession getSystemInSessionForSession(System inSystem, TestingSession inTestingSession);
    String getCompanyNameOfInstitutionOfSelectedSystemInSession(SystemInSession systemInSession);
    String getCompanyNameOfInstitutionOfSelectedSystem(System system);
    List<TestingSession> allSessions();
    List<TestingSession> getTestingSessionList();
    List<TestingSession> allSessionsWhereCompanyWasHere(String name);
    TestingSession getTestingSessionById(Integer id);
}
