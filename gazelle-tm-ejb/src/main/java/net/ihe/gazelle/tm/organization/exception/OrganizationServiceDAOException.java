package net.ihe.gazelle.tm.organization.exception;

public class OrganizationServiceDAOException extends RuntimeException{
    public OrganizationServiceDAOException() {
        super();
    }

    public OrganizationServiceDAOException(String message) {
        super(message);
    }

    public OrganizationServiceDAOException(String message, Throwable cause) {
        super(message, cause);
    }

    public OrganizationServiceDAOException(Throwable cause) {
        super(cause);
    }
}
