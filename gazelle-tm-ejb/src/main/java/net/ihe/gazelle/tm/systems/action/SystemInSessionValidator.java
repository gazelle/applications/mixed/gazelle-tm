package net.ihe.gazelle.tm.systems.action;

import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import net.ihe.gazelle.ssov7.gum.client.application.Role;
import net.ihe.gazelle.tm.systems.model.System;
import net.ihe.gazelle.users.model.Institution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by gthomazon on 10/07/17.
 */
public class SystemInSessionValidator {

    private static final Logger LOG = LoggerFactory.getLogger(SystemInSessionValidator.class);

    /**
     * Validate the system keyword filled in the JSF. We check that the system name does not already exist in DB Many cases : - we create a system
     * : - we have to check that the system does not exist
     * in the database - we update a system : - we have to check that the system does not exist in the database, excepting the actual system Note :
     * if you update this method, don't forget to update
     * validateSystemName method, very similar
     *
     * @param begin of the keyword (ie. PACS_AGFA)
     * @param end of the keyword (ie. COPY_2 to get PACS_AGFA_COPY_2)
     * @param inSystem     the system (to know if the system found in the DB is the one edited)
     * @param identity
     * @return boolean : if the validation is done without error (true)
     * @throws SystemActionException : exception
     */
    public static boolean validateSystemKeywordStatic(String begin, String end, System inSystem, GazelleIdentity identity) throws SystemActionException {
        LOG.trace("boolean validateSystemKeywordStatic");


        int results = 0;
        int iFound = 0;
        String currentSystemKeyword = begin;
        if ((end != null) && (end.length() != 0)) {
            currentSystemKeyword = currentSystemKeyword + "_" + end;
        }

        List<System> listOfExistingSystemsForCurrentInstitution =
              getListOfExistingSystemsForCurrentInstitution(inSystem, identity);

        if (listOfExistingSystemsForCurrentInstitution == null) {
            return true;
        }

        for (int i = 0; i < listOfExistingSystemsForCurrentInstitution.size(); i++) {
            if (listOfExistingSystemsForCurrentInstitution.get(i).getKeyword().equalsIgnoreCase(currentSystemKeyword)) {
                results++;
                iFound = i;
            }
        }

        if (results == 0) {
            return true;
        } else if (results == 1) {
            if ((inSystem.getId() != null)
                    && (inSystem.getId().intValue() == listOfExistingSystemsForCurrentInstitution.get(iFound).getId()
                    .intValue())) {
                // We found one system in the database with the same keyword, but it's
                // the current system to update > NO error
                return true;
            } else {
                throw new SystemActionException("gazelle.validator.system.keyword.alreadyExists");
            }
        } else {
            throw new SystemActionException("gazelle.validator.system.keyword.alreadyExists");
        }
    }

    private static List<System> getListOfExistingSystemsForCurrentInstitution(System inSystem, GazelleIdentity identity) {
        List<System> listOfExistingSystemsForCurrentInstitution = null;
        if ((inSystem.getId() != null) &&
              (identity.hasRole(Role.ADMIN) || identity.hasRole(Role.PROJECT_MANAGER))) {
            List<Institution> listOfInstitutions = System.getInstitutionsForASystem(inSystem);
            if ((listOfInstitutions != null) && (!listOfInstitutions.isEmpty())) {
                Institution selectedInstitutionForKeyword = listOfInstitutions.get(0);
                listOfExistingSystemsForCurrentInstitution = System
                        .getSystemsForAnInstitution(selectedInstitutionForKeyword);
            }
        } else {
            listOfExistingSystemsForCurrentInstitution = System.getSystemsForAnInstitution(Institution
                    .getLoggedInInstitution());
        }
        return listOfExistingSystemsForCurrentInstitution;
    }

    public static boolean validateSystemNameAndSystemVersionStatic(String currentSystemName, String version, Integer id, EntityManager
            entityManager) throws SystemActionException {
        Query query = null;
        int results = 0;
        List<System> listSystems = null;

        query = entityManager
                .createQuery("SELECT distinct syst FROM System syst where syst.name = :systemname and syst.version= :systversion");
        query.setParameter("systemname", currentSystemName);
        query.setParameter("systversion", version);

        listSystems = query.getResultList();
        results = listSystems.size();

        if (results == 0) {
            return true;
        } else if (results == 1) {
            if ((id != null) && (id.intValue() == listSystems.get(0).getId().intValue())) {
                // We found one user in the database with the same name, but it's
                // the current system to update > NO error
                return true;
            } else {
                // We found one user in the database with the same name, but it's
                // NOT the current system to update > ERROR
                throw new SystemActionException("gazelle.validator.system.nameAndVersion.alreadyExists");
            }
        } else {
            throw new SystemActionException("gazelle.validator.system.nameAndVersion.alreadyExists");
        }
    }

    private SystemInSessionValidator() {
        // Only static methods, so hide constructor
    }

}
