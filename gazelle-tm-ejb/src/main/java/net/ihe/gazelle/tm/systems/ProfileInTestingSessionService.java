package net.ihe.gazelle.tm.systems;

import net.ihe.gazelle.tf.model.Domain;
import net.ihe.gazelle.tf.model.IntegrationProfile;
import net.ihe.gazelle.tm.systems.model.TestingSession;

import java.io.Serializable;
import java.util.List;

public interface ProfileInTestingSessionService extends Serializable{


    void saveProfileInTestingSession(Domain domain, IntegrationProfile integrationProfile,TestingSession selectedTestingSession);

    void setTestabilityRemovedFromSession(List<IntegrationProfile> integrationProfiles, TestingSession selectedTestingSession);

}
