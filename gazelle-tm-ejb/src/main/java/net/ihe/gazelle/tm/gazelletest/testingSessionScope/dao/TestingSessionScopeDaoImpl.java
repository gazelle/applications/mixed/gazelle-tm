package net.ihe.gazelle.tm.gazelletest.testingSessionScope.dao;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import net.ihe.gazelle.tf.model.*;
import net.ihe.gazelle.tm.filter.TMCriterions;
import net.ihe.gazelle.tm.gazelletest.model.instance.SystemAIPOResultForATestingSession;
import net.ihe.gazelle.tm.gazelletest.model.instance.SystemAIPOResultForATestingSessionQuery;
import net.ihe.gazelle.tm.gazelletest.testingSessionScope.TestingSessionScopeDao;
import net.ihe.gazelle.tm.systems.model.*;
import net.ihe.gazelle.tm.systems.model.System;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Name(value = "testingSessionScopeDao")
@AutoCreate
@Scope(ScopeType.STATELESS)
public class TestingSessionScopeDaoImpl implements TestingSessionScopeDao {
    @In
    private EntityManager entityManager;

    @In
    private GazelleIdentity identity;

    @Override
    public Filter<ProfileInTestingSession> getFilter(Map<String, String> requestParameterMap) {
        ProfileInTestingSessionQuery profileInTestingSessionQuery = new ProfileInTestingSessionQuery();
        HQLCriterionsForFilter<ProfileInTestingSession> criteria= profileInTestingSessionQuery.getHQLCriterionsForFilter();
        TMCriterions.addTestingSession(criteria,"testing_session",profileInTestingSessionQuery.testingSession(),
              identity);
        criteria.addPath("domain",profileInTestingSessionQuery.domain());
        criteria.addPath("integrationProfile",profileInTestingSessionQuery.integrationProfile());
        criteria.addPath("testability",profileInTestingSessionQuery.testability());
        return new Filter<>(criteria,requestParameterMap);
    }

    @Override
    public List<SystemInSession> getSystemsInSessionByActorAndProfile(IntegrationProfile integrationProfile, Actor actor, TestingSession testingSession) {
        ActorIntegrationProfileOptionQuery aipoQuery = new ActorIntegrationProfileOptionQuery();
        aipoQuery.actorIntegrationProfile().actor().id().eq(actor.getId());
        aipoQuery.actorIntegrationProfile().integrationProfile().id().eq(integrationProfile.getId());
        aipoQuery.integrationProfileOption().eq(IntegrationProfileOption.getNoneOption());

        ActorIntegrationProfileOption actorIntegrationProfileOption = aipoQuery.getUniqueResult();

        if (actorIntegrationProfileOption!=null) {
            SystemInSessionQuery systemInSessionQuery = new SystemInSessionQuery();
            systemInSessionQuery.testingSession().id().eq(testingSession.getId());
            systemInSessionQuery.system().systemActorProfiles().actorIntegrationProfileOption().id().eq(actorIntegrationProfileOption.getId());
            systemInSessionQuery.registrationStatus().neq(SystemInSessionRegistrationStatus.DROPPED);
            return systemInSessionQuery.getListDistinct();
        }
        return new ArrayList<>();
    }

    @Override
    public List<IntegrationProfileOption> getProfileOptionsBySAIP(SystemInSession systemInSession,Actor actor, IntegrationProfile integrationProfile ) {
        ActorIntegrationProfileOptionQuery aipoQuery = new ActorIntegrationProfileOptionQuery();
        aipoQuery.actorIntegrationProfile().actor().id().eq(actor.getId());
        aipoQuery.actorIntegrationProfile().integrationProfile().id().eq(integrationProfile.getId());
        List<Integer> allOptionsForProfileActor = aipoQuery.id().getListDistinct();

        SystemActorProfilesQuery systemActorProfilesQuery = new SystemActorProfilesQuery();
        systemActorProfilesQuery.system().id().eq(systemInSession.getSystem().getId());
        systemActorProfilesQuery.actorIntegrationProfileOption().id().in(allOptionsForProfileActor);

        return systemActorProfilesQuery.actorIntegrationProfileOption().integrationProfileOption().getListDistinct();

    }

    @Override
    public void updateProfileInTestingSession(ProfileInTestingSession profileInTestingSession) {
        entityManager.merge(profileInTestingSession);
        entityManager.flush();
    }

    @Override
    public List<SystemAIPOResultForATestingSession> getSystemAIPOResultByTestability(Testability testability, TestingSession testingSession) {
        SystemInSessionQuery systemInSessionQuery = new SystemInSessionQuery();
        systemInSessionQuery.testingSession().id().eq(testingSession.getId());
        systemInSessionQuery.registrationStatus().neq(SystemInSessionRegistrationStatus.DROPPED);
        systemInSessionQuery.system().isTool().eq(false);
        systemInSessionQuery.acceptedToSession().eq(true);
        List<System> notDroppedSystemsInSession = systemInSessionQuery.system().getListDistinct();
        SystemAIPOResultForATestingSessionQuery query = new SystemAIPOResultForATestingSessionQuery();
        query.systemActorProfile().actorIntegrationProfileOption().actorIntegrationProfile().integrationProfile().in(getIntegrationProfile(testability, testingSession));
        query.systemActorProfile().system().systemsInSession().testingSession().id().eq(testingSession.getId());
        query.systemActorProfile().system().in(notDroppedSystemsInSession);
        return query.getList();
    }

    private List<IntegrationProfile> getIntegrationProfile(Testability testability, TestingSession testingSession) {
        ProfileInTestingSessionQuery query = new ProfileInTestingSessionQuery();
        query.testingSession().eq(testingSession);
        query.testability().eq(testability);
        return query.integrationProfile().getListDistinct();
    }
}
