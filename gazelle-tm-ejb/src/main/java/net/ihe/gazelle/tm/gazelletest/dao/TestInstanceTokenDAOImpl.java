package net.ihe.gazelle.tm.gazelletest.dao;

import net.ihe.gazelle.tm.gazelletest.action.TestInstanceTokenCreationException;
import net.ihe.gazelle.tm.gazelletest.action.TestInstanceTokenSavingException;
import net.ihe.gazelle.tm.gazelletest.action.TokenNotFoundException;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstanceToken;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstanceTokenQuery;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Name("testInstanceTokenDAO")
@Scope(ScopeType.EVENT)
@AutoCreate
public class TestInstanceTokenDAOImpl implements TestInstanceTokenDAO {

    @In
    private EntityManager entityManager;

    @Deprecated
    public TestInstanceTokenDAOImpl() {
        //For seam Injection
    }

    public TestInstanceTokenDAOImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void createToken(String tokenValue, String username, Integer stepInstanceId, Timestamp timestamp) throws TestInstanceTokenCreationException, TestInstanceTokenSavingException {
        try {
            TestInstanceToken testInstanceToken = new TestInstanceToken(tokenValue, username, stepInstanceId, timestamp);
            entityManager.persist(testInstanceToken);
            entityManager.flush();
        } catch (IllegalArgumentException exception) {
            throw new TestInstanceTokenCreationException("Invalid parameter to generate TestInstanceToken : " + exception.getMessage());
        } catch (PersistenceException persistenceException) {
            throw new TestInstanceTokenSavingException("Cannot To Save TestInstanceToken check log or cause", persistenceException.getCause());
        }


    }

    @Override
    public List<TestInstanceToken> getAllTokens() {
        TestInstanceTokenQuery query = new TestInstanceTokenQuery(entityManager);
        List<TestInstanceToken> testInstanceTokens = query.getList();
        if(testInstanceTokens == null){
            testInstanceTokens = new ArrayList<>();
        }
        return testInstanceTokens;
    }

    @Override
    public TestInstanceToken getTestInstanceToken(String tokenValue) {
        TestInstanceTokenQuery query = new TestInstanceTokenQuery(entityManager);
        query.value().eq(tokenValue);
        TestInstanceToken retrievedToken = query.getUniqueResult();
        if (retrievedToken != null) {
            return retrievedToken;
        } else {
            throw new TokenNotFoundException("No token found using the token value provided");
        }
    }

    @Override
    public void deleteToken(String tokenValue) throws TokenNotFoundException {
        TestInstanceToken token = getTestInstanceToken(tokenValue);
        entityManager.remove(token);
        entityManager.flush();
    }

    @Override
    public void deleteTokenByTestStepInstance(Integer testStepInstanceId) {
        TestInstanceTokenQuery query = new TestInstanceTokenQuery(entityManager);
        query.testStepsInstanceId().eq(testStepInstanceId);
        TestInstanceToken retrievedToken = query.getUniqueResult();
        if (retrievedToken != null) {
            deleteToken(retrievedToken.getValue());
        }
    }
}
