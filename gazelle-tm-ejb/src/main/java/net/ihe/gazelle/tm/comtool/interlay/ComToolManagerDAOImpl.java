package net.ihe.gazelle.tm.comtool.interlay;

import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import net.ihe.gazelle.ssov7.gum.client.application.User;
import net.ihe.gazelle.tm.comtool.application.ComToolManagerDAO;
import net.ihe.gazelle.tm.gazelletest.model.instance.MonitorInSessionQuery;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstance;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstanceQuery;
import net.ihe.gazelle.tm.systems.model.System;
import net.ihe.gazelle.tm.systems.model.*;
import net.ihe.gazelle.users.model.Institution;
import org.jboss.seam.annotations.In;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ComToolManagerDAOImpl implements ComToolManagerDAO {

    private final EntityManager entityManager;

    @In(value = "gumUserService")
    private UserService userService;

    public ComToolManagerDAOImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public List<TestingSession> getOpenAndActiveTestingSessions() {
        TestingSessionQuery testingSessionQuery = new TestingSessionQuery(entityManager);
        testingSessionQuery.activeSession().eq(true);
        testingSessionQuery.hiddenSession().eq(false);
        testingSessionQuery.sessionClosed().eq(false);
        testingSessionQuery.orderInGUI().order(true);
        return testingSessionQuery.getListDistinctOrdered();
    }

    @Override
    public List<User> getActiveParticipants(TestingSession testingSession) {
        List<User> resultList = new ArrayList<>();
        List<String> allUsersIds = getAllParticipantIds(testingSession);
        for (String userId : allUsersIds) {
            User user = userService.getUserById(userId);
            if (Boolean.TRUE.equals(user.getActivated())) {
                resultList.add(user);
            }
        }
        return resultList;
    }

    @Override
    public List<User> getActiveAndAcceptedParticipants(TestingSession testingSession) {
        List<User> resultList = new ArrayList<>();
        List<String> allUsers = getAllParticipantIds(testingSession);
        List<Integer> organizationsIds = getAcceptedOrganizationIds(testingSession);
        List<Institution> institutions = new ArrayList<>();
        for (Integer organizationId : organizationsIds) {
            Institution institution = Institution.findInstitutionWithId(organizationId);
            institutions.add(institution);
        }
        for (String userID : allUsers) {
            User user = userService.getUserById(userID);
            for (Institution institution : institutions) {
                if (Objects.equals(user.getOrganizationId(), institution.getKeyword()) && Boolean.TRUE.equals(user.getActivated())) {
                    resultList.add(user);
                }
            }
        }
        return resultList;
    }

    @Override
    public List<User> getActiveMonitors(TestingSession testingSession) {
        MonitorInSessionQuery query = new MonitorInSessionQuery(entityManager);
        query.testingSession().id().eq(testingSession.getId());
        query.isActivated().eq(true);
        List<String> usersIdList = query.userId().getListDistinct();
        List<User> userList = new ArrayList<>();
        for (String userId : usersIdList) {
            User user = userService.getUserById(userId);
            if (Boolean.TRUE.equals(user.getActivated())) {
                userList.add(user);
            }
        }
        return userList;
    }

    @Override
    public List<User> getActiveAdmins(TestingSession testingSession) {
        List<TestingSessionAdmin> testingSessionAdmins = testingSession.getTestingSessionAdmins();
        List<User> adminList = new ArrayList<>();
        for (TestingSessionAdmin sessionAdmin : testingSessionAdmins) {
            User user = userService.getUserById(sessionAdmin.getUserId());
            if (Boolean.TRUE.equals(user.getActivated())) {
                adminList.add(user);
            }
        }
        return adminList;
    }

    @Override
    public List<System> getTestRunSystems(TestInstance testInstance) {
        TestInstanceQuery testInstanceQuery = new TestInstanceQuery(entityManager);
        testInstanceQuery.id().eq(testInstance.getId());
        return testInstanceQuery.testInstanceParticipants().systemInSessionUser().systemInSession().system().getListDistinct();
    }

    // TODO : Change Query cause user is not in gazelle-db.
    private List<String> getAllParticipantIds(TestingSession testingSession) {
        Query participantIdsQuery = entityManager.createNativeQuery("select user_id from tm_connectathon_participant where testing_session_id = ?1");
        participantIdsQuery.setParameter(1, testingSession.getId());
        return participantIdsQuery.getResultList();
    }

    private List<Integer> getAcceptedOrganizationIds(TestingSession testingSession) {
        SystemInSessionQuery systemInSessionQuery = new SystemInSessionQuery(entityManager);
        systemInSessionQuery.testingSession().id().eq(testingSession.getId());
        systemInSessionQuery.acceptedToSession().eq(true);
        return systemInSessionQuery.system().institutionSystems().institution().id().getListDistinct();
    }
}
