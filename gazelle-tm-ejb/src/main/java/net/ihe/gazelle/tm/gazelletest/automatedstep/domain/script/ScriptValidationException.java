package net.ihe.gazelle.tm.gazelletest.automatedstep.domain.script;

public class ScriptValidationException extends RuntimeException {

    public ScriptValidationException() {
    }

    public ScriptValidationException(String message) {
        super(message);
    }

    public ScriptValidationException(String message, Throwable throwable) {
        super(message, throwable);
    }

    public ScriptValidationException(Throwable throwable) {
        super(throwable);
    }
}
