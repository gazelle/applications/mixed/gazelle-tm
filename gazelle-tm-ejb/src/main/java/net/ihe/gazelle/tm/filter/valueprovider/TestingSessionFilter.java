package net.ihe.gazelle.tm.filter.valueprovider;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import net.ihe.gazelle.menu.Authorizations;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import net.ihe.gazelle.ssov7.gum.client.application.Role;
import net.ihe.gazelle.tm.gazelletest.model.instance.MonitorInSessionQuery;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import net.ihe.gazelle.tm.systems.model.TestingSessionEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

public class TestingSessionFilter implements QueryModifier {

   private static final Logger LOG = LoggerFactory.getLogger(TestingSessionFilter.class);
   private static final long serialVersionUID = 5221150633060422866L;
   private final String path;

   private final GazelleIdentity identity;

   public TestingSessionFilter(GazelleIdentity identity, TestingSessionEntity<TestingSession> testingSession) {
      super();
      this.path = testingSession.toString();
      this.identity = identity;
   }

   @Override
   public void modifyQuery(HQLQueryBuilder queryBuilder, Map filterValuesApplied) {
      LOG.trace("modifyQuery");
      if (Authorizations.TM.isGranted() &&
            identity.hasRole(Role.MONITOR) &&
            !identity.hasRole(Role.ADMIN) &&
            !Authorizations.TESTING_SESSION_ADMIN_OF_CURRENT_TESTING_SESSION.isGranted()) {
         MonitorInSessionQuery query = new MonitorInSessionQuery();
         query.userId().eq(identity.getUsername());
         List<Integer> testingSessionIds = query.testingSession().id().getListDistinct();
         queryBuilder.addIn(path + ".id", testingSessionIds);
      }
   }

}
