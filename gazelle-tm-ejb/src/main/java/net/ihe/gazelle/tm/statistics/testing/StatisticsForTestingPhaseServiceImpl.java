package net.ihe.gazelle.tm.statistics.testing;

import net.ihe.gazelle.tm.gazelletest.model.instance.MonitorInSession;
import net.ihe.gazelle.tm.gazelletest.model.instance.Status;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstance;
import net.ihe.gazelle.tm.session.TestingSessionService;
import net.ihe.gazelle.tm.statistics.common.StatisticsCommonService;
import net.ihe.gazelle.tm.systems.model.SystemInSession;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Scope(ScopeType.STATELESS)
@AutoCreate
@Name(value = "statisticsForTestingPhaseService")
public class StatisticsForTestingPhaseServiceImpl implements StatisticsForTestingPhaseService {

    @In(value = "statisticsForTestingPhaseDao")
    private StatisticsForTestingPhaseDao statisticsForTestingPhaseDao;

    @In(value = "statisticsCommonService")
    private StatisticsCommonService statisticsCommonService;

    @In(value = "testingSessionService")
    private TestingSessionService testingSessionService;

    @Override
    public List<SystemInSession> getSystemsForCurrentUser() {
        return statisticsForTestingPhaseDao.getSystemsForCurrentUser(testingSessionService.getUserTestingSession());
    }

    @Override
    public List<TestInstance> getTestInstancesForASystem(SystemInSession system) {
        return statisticsForTestingPhaseDao.getTestInstancesForASystem(system);
    }

    @Override
    public List<TestInstance> getAllTestInstancesForCurrentSession() {
        return statisticsForTestingPhaseDao.getAllTestInstancesForCurrentSession();
    }

    @Override
    public List<TestInstance> getTestInstancesWithVerificationPending() {
        return statisticsForTestingPhaseDao.getTestInstancesWithVerificationPending();
    }

    @Override
    public List<TestInstance> getLeftOverTestInstances() {
        return statisticsForTestingPhaseDao.getLeftOverTestInstances();
    }

    @Override
    public List<TestInstance> getTestInstancesForMonitorInSession(MonitorInSession monitor) {
        return statisticsForTestingPhaseDao.getTestInstancesForMonitorInSession(monitor);
    }

    @Override
    public int getNumberOfTotalTestInstanceForASystemInSession(SystemInSession system) {
        return statisticsForTestingPhaseDao.getNumberOfTotalTestInstanceForASystemInSession(system);
    }

    @Override
    public int getNumberOfTotalTestInstances() {
        return statisticsForTestingPhaseDao.getNumberOfTotalTestInstances();
    }

    @Override
    public int getNumberOfVerificationPendingTestInstances() {
        return statisticsForTestingPhaseDao.getNumberOfVerificationPendingTestInstances();
    }

    @Override
    public int getNumberOfLeftOverTestInstances() {
        List<TestInstance> testInstances = getLeftOverTestInstances();
        int nbLeftOver = 0;
        if (!testInstances.isEmpty()) {
            Date currentDate = new Date();
            for (TestInstance testInstance : testInstances) {
                long elapsedTimeMillis = currentDate.getTime() - testInstance.getLastChanged().getTime();
                long elapsedTimeHours = TimeUnit.HOURS.convert(elapsedTimeMillis, TimeUnit.MILLISECONDS);
                if (elapsedTimeHours > 24) {
                    nbLeftOver++;
                }
            }
        }
        return nbLeftOver;
    }

    @Override
    public int getNumberOfTotalTestInstancesForMonitorInSession(MonitorInSession monitor) {
        return statisticsForTestingPhaseDao.getNumberOfTotalTestInstancesForMonitorInSession(monitor);
    }

    @Override
    public PendingTestInstanceStats getStatisticsForEvaluationPendingTestInstances() {
        List<TestInstance> testInstances = getTestInstancesWithVerificationPending();
        int nbToBeAssigned = 0;
        int nbClaimed = 0;
        int nbCritical = 0;
        int nbPartiallyVerified = 0;
        if (!testInstances.isEmpty()) {

            for (TestInstance testInstance : testInstances) {

                nbToBeAssigned = getNbToBeAssigned(nbToBeAssigned, testInstance);
                nbPartiallyVerified = getNbPartiallyVerified(nbPartiallyVerified, testInstance);
                nbClaimed = getNbClaimed(nbClaimed, testInstance);
                nbCritical = getNbCritical(nbCritical, testInstance);
            }
        }
        return new PendingTestInstanceStats(nbToBeAssigned, nbClaimed, nbCritical, nbPartiallyVerified);
    }

    private int getNbCritical(int nbCritical, TestInstance testInstance) {
        if (testInstance.getLastStatus() == Status.CRITICAL && testInstance.getMonitorInSession() == null) {
            nbCritical++;
        }
        return nbCritical;
    }

    private int getNbClaimed(int nbClaimed, TestInstance testInstance) {
        if ((testInstance.getLastStatus() == Status.CRITICAL || testInstance.getLastStatus() == Status.COMPLETED) && testInstance.getMonitorInSession() != null) {
            nbClaimed++;
        }
        return nbClaimed;
    }

    private int getNbPartiallyVerified(int nbPartiallyVerified, TestInstance testInstance) {
        if (testInstance.getLastStatus() == Status.PARTIALLY_VERIFIED && testInstance.getMonitorInSession() != null) {
            nbPartiallyVerified++;
        }
        return nbPartiallyVerified;
    }

    private int getNbToBeAssigned(int nbToBeAssigned, TestInstance testInstance) {
        if (testInstance.getLastStatus() == Status.COMPLETED && testInstance.getMonitorInSession() == null) {
            nbToBeAssigned++;
        }
        return nbToBeAssigned;
    }

    @Override
    public LeftOverTestInstanceStats getStatisticsLeftOverTestInstances() {
        List<TestInstance> testInstances = getLeftOverTestInstances();
        int nbOneDay = 0;
        int nbOneAndHalfDay = 0;
        int nbTwoDaysAndMore = 0;
        if (!testInstances.isEmpty()) {
            Date currentDate = new Date();
            for (TestInstance testInstance : testInstances) {
                long elapsedTimeMillis = currentDate.getTime() - testInstance.getLastChanged().getTime();
                long elapsedTimeHours = TimeUnit.HOURS.convert(elapsedTimeMillis, TimeUnit.MILLISECONDS);
                if (elapsedTimeHours > 24 && elapsedTimeHours <= 36) {
                    nbOneDay++;
                }
                if (elapsedTimeHours > 36 && elapsedTimeHours <= 48) {
                    nbOneAndHalfDay++;
                }
                if (elapsedTimeHours > 48) {
                    nbTwoDaysAndMore++;
                }
            }
        }

        return new LeftOverTestInstanceStats(nbOneDay, nbOneAndHalfDay, nbTwoDaysAndMore);
    }

    @Override
    public StatisticsCommonService.TestInstanceStats getStatisticsForTestInstances(List<TestInstance> testInstances) {
        return statisticsCommonService.getStatisticsForTestInstances(testInstances);
    }


}
