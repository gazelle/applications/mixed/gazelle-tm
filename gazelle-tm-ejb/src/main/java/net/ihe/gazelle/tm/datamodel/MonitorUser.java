package net.ihe.gazelle.tm.datamodel;

import net.ihe.gazelle.ssov7.gum.client.application.User;
import net.ihe.gazelle.tm.gazelletest.model.instance.MonitorInSession;

public class MonitorUser {

    private User user;
    private MonitorInSession monitorInSession;
    private int testListSize;

    public MonitorUser(User user, MonitorInSession monitorInSession) {
        this.user = user;
        this.monitorInSession = monitorInSession;
        this.testListSize = monitorInSession.getTestList().size();
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public MonitorInSession getMonitorInSession() {
        return monitorInSession;
    }

    public void setMonitorInSession(MonitorInSession monitorInSession) {
        this.monitorInSession = monitorInSession;
    }

    public int getTestListSize() {
        return testListSize;
    }

    public void setTestListSize(int testListSize) {
        this.testListSize = testListSize;
    }
}
