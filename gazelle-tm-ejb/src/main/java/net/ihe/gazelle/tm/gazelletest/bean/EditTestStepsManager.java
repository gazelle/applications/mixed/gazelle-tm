package net.ihe.gazelle.tm.gazelletest.bean;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.common.report.ReportExporterManager;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import net.ihe.gazelle.tf.model.Transaction;
import net.ihe.gazelle.tf.model.WSTransactionUsage;
import net.ihe.gazelle.tm.application.services.TMApplicationConfigurationService;
import net.ihe.gazelle.tm.gazelletest.automatedstep.domain.script.Script;
import net.ihe.gazelle.tm.gazelletest.automatedstep.domain.script.ScriptParsingException;
import net.ihe.gazelle.tm.gazelletest.automatedstep.domain.script.ScriptValidationException;
import net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.script.dto.ScriptDTO;
import net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.script.validation.ScriptValidationService;
import net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.testdesign.service.AutomatedTestStepService;
import net.ihe.gazelle.tm.gazelletest.domain.TestStepDomain;
import net.ihe.gazelle.tm.gazelletest.domain.TestStepType;
import net.ihe.gazelle.tm.gazelletest.model.definition.*;
import net.ihe.gazelle.tm.gazelletest.model.instance.ContextualInformationInstance;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestStepsInstance;
import org.apache.http.entity.ContentType;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Factory;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.richfaces.event.FileUploadEvent;
import org.richfaces.model.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Name("editTestStepsManager")
@GenerateInterface(value = "EditTestStepsManagerLocal")
@Scope(ScopeType.PAGE)
public class EditTestStepsManager implements Serializable, EditTestStepsManagerLocal {

    private static final long serialVersionUID = 645439846131L;

    private static final int DEFAULT_TEST_STEPS_INDEX_INCREMENTATION = 10;

    private static final Logger LOG = LoggerFactory.getLogger(EditTestStepsManager.class);

    private Test editedTestForTestSteps;
    private boolean showTestStepsEditionPanel = false;
    private boolean isCreating = false;
    private TestStepDomain selectedTestStep;
    private List<TestRoles> possibleTestRoles;
    private ContextualInformation selectedContextualInformation;
    private int inputOrOutput = 1;
    private ContextualInformation selectedContextualInformationOld;
    private List<ContextualInformation> listContextualInformationToRemove;
    private String contextualInformationtype;
    private boolean autoResponse = false;
    private String scriptName;
    private String selectedStepType;
    private String uploadErrorMessage;
    private Script script;

    @In
    private GazelleIdentity identity;
    @In(value = "tmApplicationConfigurationService")
    private TMApplicationConfigurationService tmApplicationConfigurationService;
    @In(value = "automatedTestStepService", create = true)
    private AutomatedTestStepService automatedTestStepService;
    @In(value = "scriptValidationService", create = true)
    private ScriptValidationService scriptValidationService;

    public List<String> getPossibleTestStepType() {
        return TestStepType.getTestStepTypes();
    }

    public void updateSelectedTestStepType() {
        resetUpload();
        if (TestStepType.BASIC.equals(TestStepType.fromKey(selectedStepType))) {
            addNewManualTestSteps();
        }
        if (TestStepType.AUTOMATED.equals(TestStepType.fromKey(selectedStepType))) {
            addNewAutomatedTestSteps();
        }
    }

    public boolean isAutomatedStepEnabled() {
        return tmApplicationConfigurationService.isAutomatedStepEnabled();
    }

    public String getSelectedStepType() {
        return selectedStepType;
    }

    public void setSelectedStepType(String selectedStepType) {
        this.selectedStepType = selectedStepType;
    }

    /*
        ___________ AUTOMATION SCRIPT PART ______________
     */

    public void uploadListener(final FileUploadEvent event) {
        final UploadedFile uploadedFile = event.getUploadedFile();
        if (uploadedFile.getData() == null || uploadedFile.getData().length == 0) {
            uploadErrorMessage = "File is empty";
            return;
        }
        if (!ContentType.APPLICATION_JSON.getMimeType().equals(uploadedFile.getContentType())) {
            uploadErrorMessage = "File content is not JSON.";
            return;
        }
        String content = new String(uploadedFile.getData(), StandardCharsets.UTF_8);
        validateScript(content, uploadedFile);
    }

    private void validateScript(String content, UploadedFile uploadedFile) {
        try {
            String error = scriptValidationService.validate(content);
            if (error == null) {
                parseScript(content, uploadedFile);
            } else {
                uploadErrorMessage = error;
            }
        } catch (ScriptValidationException e) {
            LOG.error(e.getMessage());
        }
    }

    private void parseScript(String content, UploadedFile uploadedFile) {
        try {
            setScript(scriptValidationService.parse(content));
            setScriptName(uploadedFile.getName());
            uploadErrorMessage = null;
        } catch (ScriptParsingException e) {
            LOG.error(e.getCause().getMessage());
            uploadErrorMessage = e.getMessage();
        }
    }

    public void downloadScript() {
        try {
            ScriptDTO dto = new ScriptDTO(script);
            ObjectMapper mapper = new ObjectMapper();
            String content = mapper.writeValueAsString(dto);
            ReportExporterManager.exportToFile(content, scriptName);
        } catch (JsonProcessingException e) {
            LOG.error(e.getMessage());
        }
    }

    public Script getScript() {
        return script;
    }

    public void setScript(Script script) {
        this.script = script;
    }

    public String getScriptName() {
        return scriptName;
    }

    public void setScriptName(String scriptName) {
        this.scriptName = scriptName;
    }

    public String getUploadErrorMessage() {
        return uploadErrorMessage;
    }

    public void setUploadErrorMessage(String uploadErrorMessage) {
        this.uploadErrorMessage = uploadErrorMessage;
    }

    public String getScriptNameIfAutomated(TestStepDomain currentStep) {
        if (currentStep.isAutomated()) {
            return currentStep.getTestScriptID();
        } else {
            return "";
        }
    }

    public void resetUpload() {
        this.scriptName = null;
        this.uploadErrorMessage = null;
        this.script = null;
    }

    public boolean isManualTestStep() {
        return !selectedTestStep.isAutomated();
    }

    public boolean isAutomatedTestStep() {
        return selectedTestStep.isAutomated();
    }

    /*
        _____________ ADD NEW TEST STEP _____________
     */

    public void addNewAutomatedTestSteps() {
        LOG.trace("addNewAutomatedTestSteps");
        selectedTestStep = new TestStepDomain();
        selectedTestStep.setType(TestStepType.AUTOMATED);
        initSelectedTestStep();
    }

    public void addNewManualTestSteps() {
        LOG.trace("addNewManualTestSteps");
        selectedTestStep = new TestStepDomain();
        initSelectedTestStep();
    }

    public void addNewTestSteps() {
        LOG.trace("addNewTestSteps");
        isCreating = true;
        selectedTestStep = new TestStepDomain();
        selectedStepType = TestStepType.BASIC.getKey();
        initSelectedTestStep();
    }

    private void initSelectedTestStep() {
        if (editedTestForTestSteps != null) {
            selectedTestStep.setStepIndex(getHighestStepIndex(editedTestForTestSteps.getTestStepsList()) + DEFAULT_TEST_STEPS_INDEX_INCREMENTATION);
            selectedTestStep.setTestParent(editedTestForTestSteps);
            selectedTestStep.setLastChanged(new Date());
            selectedTestStep.setLastModifierId(identity.getUsername());
        }
        autoResponse = false;
        updateTest(selectedTestStep);
        showTestStepsEditionPanel();
    }

    /*
        _____________ CREATE NEW TEST STEP _____________
     */

    public void createTestStep() {
        if (isManualTestStep()) {
            persistSelectedTestSteps();
        }
        if (isAutomatedTestStep()) {
            createAutomatedTestStep();
        }
    }

    public void createAutomatedTestStep() {
        selectedTestStep.setTestScriptID(scriptName);
        if (validateStepIndex() && validateTestStepsDescription()) {
            if (scriptName != null) {
                if (checkAutoResponse()) {
                    return;
                }
                selectedTestStep.setTestParent(editedTestForTestSteps);
                selectedTestStep = automatedTestStepService.createTestStep(selectedTestStep, script);
                FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.INFO, "gazelle.tm.testing.testsDefinition.testStepsCreated");
            } else {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Please add a script to this test step");
                return;
            }

            updateTest(selectedTestStep);
            updateDiagram(editedTestForTestSteps);
            resetUpload();
            selectedTestStep = new TestStepDomain();
            showTestStepsEditionPanel = false;
            isCreating = false;
        }
    }

    public void persistSelectedTestSteps() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("persistSelectedTestSteps");
        }
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        if (selectedTestStep == null) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "TestSteps value is null cannot go further");
            return;
        }
        if (selectedTestStep.getTestParent() == null) {
            selectedTestStep.setTestParent(editedTestForTestSteps);
        }
        if (validateStepIndex() && validateTestStepsDescription()) {
            try {

                if (checkAutoResponse()) {
                    return;
                }

                if ((selectedTestStep.getDescription() == null) || selectedTestStep.getDescription().isEmpty()) {
                    selectedTestStep.setDescription(" ");
                }
                TestSteps testSteps = new TestSteps(selectedTestStep);
                if (selectedTestStep.getId() != null) {
                    testSteps.setId(selectedTestStep.getId());
                }
                TestSteps managedTestStep = TestSteps.mergeTestSteps(testSteps, entityManager);

                if (editedTestForTestSteps.getTestStepsList() == null) {
                    editedTestForTestSteps.setTestStepsList(new ArrayList<TestSteps>());
                }

                editedTestForTestSteps = entityManager.find(Test.class, editedTestForTestSteps.getId());
                entityManager.refresh(editedTestForTestSteps);
                List<TestSteps> lts = editedTestForTestSteps.getTestStepsList();

                boolean updateMode = false;
                if (!lts.contains(managedTestStep)) {
                    lts.add(managedTestStep);
                } else {
                    updateMode = true;
                }
                editedTestForTestSteps.setTestStepsList(lts);
                editedTestForTestSteps = entityManager.merge(editedTestForTestSteps);
                entityManager.flush();
                selectedTestStep = managedTestStep.asDomain();
                if (updateMode) {
                    FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.INFO, "gazelle.tm.testing.testsDefinition.testStepsUpdated");
                } else {
                    FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.INFO, "gazelle.tm.testing.testsDefinition.testStepsCreated");
                }

                updateTest(selectedTestStep);
                updateDiagram(editedTestForTestSteps);

                selectedTestStep = new TestStepDomain();
                showTestStepsEditionPanel = false;
                isCreating = false;
            } catch (Exception e) {
                LOG.error("Failed to persist test steps", e);
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Impossible to persist testSteps : " + e.getMessage());
            }
        } else {
            editedTestForTestSteps = entityManager.merge(editedTestForTestSteps);
            updateTest(selectedTestStep);
        }
    }

    private boolean checkAutoResponse() {
        if (autoResponse) {
            selectedTestStep.setTestRolesResponder(selectedTestStep.getTestRolesInitiator());
            selectedTestStep.setTransaction(null);
            selectedTestStep.setWstransactionUsage(null);
            autoResponse = false;
        } else {
            if (selectedTestStep.getTransaction() == null
                    && !selectedTestStep.getTestRolesInitiator().equals(selectedTestStep.getTestRolesResponder())) {
                FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.ERROR,
                        "gazelle.tm.testing.testsDefinition.transactionNullWarning");
                return true;
            }

            if (selectedTestStep.getTestRolesResponder() == null) {
                FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.ERROR,
                        "gazelle.tm.testing.testsDefinition.testRolesResponderNullWarning");
                return true;
            }

        }
        return false;
    }

    /*
        _____________ EDIT EXISTING TEST STEP FROM BUTTON _____________
     */

    public void updateTestStep() {
        if (isManualTestStep()) {
            persistSelectedTestStepsAndDeleteNonUsedCI();
        }
        if (isAutomatedTestStep() && validateStepIndex() && validateTestStepsDescription()) {
            if (checkAutoResponse()) {
                return;
            }
            if (scriptName != null) {
                selectedTestStep = automatedTestStepService.editTestStep(selectedTestStep, scriptName, script);
                FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.INFO, "gazelle.tm.testing.testsDefinition.testStepsUpdated");
            } else {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Please add a script to this test step");
                return;
            }
            this.deleteNonUsedContextualInformation();
            resetUpload();
            updateTest(selectedTestStep);
            updateDiagram(editedTestForTestSteps);
            selectedTestStep = new TestStepDomain();
            showTestStepsEditionPanel = false;
        }
    }

    public void persistSelectedTestStepsAndDeleteNonUsedCI() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("persistSelectedTestStepsAndDeleteNonUsedCI");
        }
        this.persistSelectedTestSteps();
        this.deleteNonUsedContextualInformation();
    }

    /*
        _____________ EDIT EXISTING TEST STEP FROM FIELDS _____________
     */

    public void persistTestSteps(TestStepDomain inTestSteps) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("persistTestSteps");
        }
        this.selectedTestStep = inTestSteps;
        if (isAutomatedTestStep()) {
            scriptName = inTestSteps.getTestScriptID();
            script = automatedTestStepService.readScript(inTestSteps.getId(), scriptName);
            updateTestStep();
        } else {
            persistSelectedTestSteps();
        }
    }

    /*
        _____________ EDIT INIT EXISTING TEST STEP _____________
     */

    public void editTestStepsAndInitializeVar(TestStepDomain testSteps) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("editTestStepsAndInitializeVar");
        }
        isCreating = false;
        selectedStepType = testSteps.getType().getKey();
        if (testSteps.isAutomated()) {
            scriptName = testSteps.getTestScriptID();
            script = automatedTestStepService.readScript(testSteps.getId(), scriptName);
        }
        this.editTestSteps(testSteps);
        this.listContextualInformationToRemove = new ArrayList<>();
    }

    public void editTestSteps(TestStepDomain inTestSteps) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("editTestSteps");
        }
        selectedTestStep = inTestSteps;
        editTestSteps();
    }

    public void editTestSteps() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("editTestSteps");
        }
        setAutoResponse(selectedTestStep.getTestRolesInitiator().equals(selectedTestStep.getTestRolesResponder()));
        showTestStepsEditionPanel();
    }

    /*
        _____________ DELETE EXISTING TEST STEP _____________
     */

    public void deleteSelectedTestSteps() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("deleteSelectedTestSteps");
        }

        EntityManager entityManager = EntityManagerService.provideEntityManager();
        if (selectedTestStep == null) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "TestSteps value is null cannot go further");
            return;
        }
        TestSteps testSteps = entityManager.find(TestSteps.class, selectedTestStep.getId());
        List<TestStepsInstance> testStepsInstanceList = TestStepsInstance.getTestStepsInstanceByTestSteps(testSteps);

        if (testStepsInstanceList != null && !testStepsInstanceList.isEmpty()) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Impossible to delete TestSteps : selected Test Steps is used in many testSteps instances.");
            return;
        }

        try {
            List<TestSteps> testStepsList = editedTestForTestSteps.getTestStepsList();
            if (testStepsList != null) {
                testStepsList.remove(testSteps);
                editedTestForTestSteps.setTestStepsList(testStepsList);
                editedTestForTestSteps = entityManager.merge(editedTestForTestSteps);
                entityManager.flush();
            }
            if (isAutomatedTestStep()) {
                automatedTestStepService.deleteTestStep(selectedTestStep);
            } else {
                TestSteps.deleteTestStepsWithFind(testSteps);
            }
            FacesMessages.instance().add(StatusMessage.Severity.INFO, "TestSteps deleted");
        } catch (Exception e) {
            LOG.error("Failed to delete teststeps", e);
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Impossible to delete TestSteps : " + e.getMessage());
        }
    }

    /*
        _____________ COPY EXISTING TEST STEP IN A NEW TEST STEP _____________
     */

    public void copySelectedTestStep(TestStepDomain selectedTestSteps) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("copySelectedTestStep");
        }
        selectedTestStep = new TestStepDomain(selectedTestSteps);
        Integer stepToCopyId = selectedTestSteps.getId();
        selectedTestStep.setId(null);
        selectedTestStep.setStepIndex(getHighestStepIndex(editedTestForTestSteps.getTestStepsList()) + DEFAULT_TEST_STEPS_INDEX_INCREMENTATION);
        this.selectedTestStep.setDescription("Copy of Step " + selectedTestSteps.getStepIndex() + " : " + this.selectedTestStep.getDescription());
        if (isAutomatedTestStep()) {
            copyAutomatedTestStep(selectedTestStep, stepToCopyId);
        } else {
            persistSelectedTestSteps();
        }
    }

    public void copyAutomatedTestStep(TestStepDomain automatedTestStep, Integer stepToCopyId) {
        if (validateStepIndex() && validateTestStepsDescription()) {
            scriptName = automatedTestStep.getTestScriptID();
            script = automatedTestStepService.readScript(stepToCopyId, automatedTestStep.getTestScriptID());
            selectedTestStep = automatedTestStepService.copyTestStep(automatedTestStep, stepToCopyId);

            resetUpload();
            updateTest(selectedTestStep);
            updateDiagram(editedTestForTestSteps);
            selectedTestStep = new TestStepDomain();
        }
    }

    public void updateTest(TestStepDomain selectedTestStepDomain) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("updateTest");
        }

        Test test = selectedTestStepDomain.getTestParent();
        test.setLastChanged(selectedTestStepDomain.getLastChanged());
        test.setLastModifierId(selectedTestStepDomain.getLastModifierId());

        EntityManager entityManager = EntityManagerService.provideEntityManager();
        entityManager.merge(test);
        entityManager.flush();
    }

    public void showTestStepsEditionPanel() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("showTestStepsEditionPanel");
        }
        showTestStepsEditionPanel = true;
    }

    public String updateDiagram(Test test) {
        try {
            DiagramGenerator.updateSequenceDiagram(test);
        } catch (Exception e) {
            LOG.error("", e);
        }
        return test.viewFolder() + "/testsDefinition/editTestSteps.seam?id=" + test.getId();
    }

    private int getHighestStepIndex(List<TestSteps> testStepsList) {
        int result = 0;
        if (testStepsList != null) {
            for (TestSteps testSteps : testStepsList) {
                if (testSteps.getStepIndex() > result) {
                    result = testSteps.getStepIndex();
                }
            }
        }
        return result;
    }

    public void deleteNonUsedContextualInformation() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("deleteNonUsedContextualInformation");
        }
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        if (listContextualInformationToRemove != null) {
            for (ContextualInformation ci : listContextualInformationToRemove) {
                if (ci != null && ci.getId() != null) {
                    ci = entityManager.find(ContextualInformation.class, ci.getId());
                    List<TestSteps> lts = TestSteps.getTestStepsFiltered(ci);
                    if (lts != null && !lts.isEmpty()) {
                        return;
                    }
                    entityManager.remove(ci);
                    entityManager.flush();
                }
            }
        }
        listContextualInformationToRemove = new ArrayList<>();
    }

    public boolean getIsCreating() {
        return isCreating;
    }

    public void setCreating(boolean isCreating) {
        this.isCreating = isCreating;
    }

    public ContextualInformation getSelectedContextualInformation() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getSelectedContextualInformation");
        }
        if (selectedContextualInformation == null) {
            selectedContextualInformation = new ContextualInformation();
        }
        return selectedContextualInformation;
    }

    public void setSelectedContextualInformation(ContextualInformation selectedContextualInformation) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("setSelectedContextualInformation");
        }
        this.selectedContextualInformation = selectedContextualInformation;
    }

    public TestStepDomain getSelectedTestSteps() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getSelectedTestSteps");
        }
        return selectedTestStep;
    }

    public void setSelectedTestSteps(TestStepDomain selectedTestSteps) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("setSelectedTestSteps");
        }
        this.selectedTestStep = selectedTestSteps;
    }

    public boolean isAutoResponse() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("isAutoResponse");
        }
        return autoResponse;
    }

    public void setAutoResponse(boolean autoResponse) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("setAutoResponse");
        }
        this.autoResponse = autoResponse;
    }

    public boolean isShowTestStepsEditionPanel() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("isShowTestStepsEditionPanel");
        }
        return showTestStepsEditionPanel;
    }

    public void setShowTestStepsEditionPanel(boolean showTestStepsEditionPanel) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("setShowTestStepsEditionPanel");
        }
        this.showTestStepsEditionPanel = showTestStepsEditionPanel;
    }

    @Factory(value = "editedTestForTestSteps", scope = ScopeType.PAGE)
    public Test getEditedTestForTestSteps() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getEditedTestForTestSteps");
        }
        if (editedTestForTestSteps == null) {
            FacesContext fc = FacesContext.getCurrentInstance();
            String testId = fc.getExternalContext().getRequestParameterMap().get("id");
            if (testId != null) {
                setEditedTestForTestSteps(Integer.parseInt(testId));
            }
        }

        return editedTestForTestSteps;
    }

    private void setEditedTestForTestSteps(int testId) {
        EntityManager em = EntityManagerService.provideEntityManager();
        setEditedTestForTestSteps(em.find(Test.class, testId));
    }

    // Test step edition

    public void setEditedTestForTestSteps(Test editedTest) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("setEditedTestForTestSteps");
        }
        this.editedTestForTestSteps = editedTest;
    }

    public List<TestStepDomain> getListOfTestSteps() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getListOfTestSteps");
        }
        TestStepsQuery query = new TestStepsQuery();
        query.testParent().id().eq(getEditedTestForTestSteps().getId());
        List<TestSteps> res = query.getList();
        Collections.sort(res);
        List<TestStepDomain> domains = new ArrayList<>();
        for (TestSteps step : res) {
            domains.add(step.asDomain());
        }
        return domains;
    }

    public List<TestStepsOption> getPossibleTestStepsOptions() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getPossibleTestStepsOptions");
        }
        TestStepsOptionQuery query = new TestStepsOptionQuery();
        query.getQueryBuilder().addOrder("id", true);
        return query.getList();
    }

    public List<TestRoles> getPossibleInitiatorTestRoles() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getPossibleInitiatorTestRoles");
        }
        if (autoResponse) {
            return getPossibleTestRoles();
        } else {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            Query q = entityManager
                    .createQuery("SELECT distinct tr "
                            + "FROM TestRoles tr, TransactionLink tl, ProfileLink pl "
                            + "JOIN tr.roleInTest.testParticipantsList testParticipantsList "
                            + "WHERE pl.actorIntegrationProfile = testParticipantsList.actorIntegrationProfileOption.actorIntegrationProfile "
                            + "AND tl.fromActor = testParticipantsList.actorIntegrationProfileOption.actorIntegrationProfile.actor "
                            + "AND tl.transaction = pl.transaction " + "AND tr.test = :test");
            q.setParameter("test", getEditedTestForTestSteps());
            List<TestRoles> result = q.getResultList();
            return result;
        }
    }

    private List<TestRoles> getPossibleTestRoles() {
        this.possibleTestRoles = TestRoles.getTestRolesListForATest(getEditedTestForTestSteps());
        return possibleTestRoles;
    }

    public List<Transaction> getPossibleTransactions() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getPossibleTransactions");
        }
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        if (selectedTestStep != null) {
            if (selectedTestStep.getTestRolesInitiator() != null) {

                Query q = entityManager
                        .createQuery("SELECT distinct pl.transaction "
                                + "FROM ProfileLink pl, TestRoles tr "
                                + "JOIN tr.roleInTest.testParticipantsList testParticipantsList "
                                + "WHERE pl.actorIntegrationProfile = testParticipantsList.actorIntegrationProfileOption.actorIntegrationProfile "
                                + "AND tr = :testRoles");
                q.setParameter("testRoles", selectedTestStep.getTestRolesInitiator());
                List<Transaction> result = q.getResultList();
                return result;
            }
        }
        return null;
    }

    public List<WSTransactionUsage> getPossibleWSTransactionUsages() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getPossibleWSTransactionUsages");
        }
        if (this.selectedTestStep != null) {
            if (this.selectedTestStep.getTransaction() != null) {
                return WSTransactionUsage.getWSTransactionUsageFiltered(this.selectedTestStep.getTransaction(), null,
                        null, null);
            }
        }
        return null;
    }

    public List<TestRoles> getPossibleTestRolesResponder() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getPossibleTestRolesResponder");
        }
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        if (selectedTestStep.getTransaction() != null) {
            Query q = entityManager
                    .createQuery("SELECT distinct tr "
                            + "FROM ProfileLink pl, TestRoles tr,TestRoles tr2, TransactionLink tl "
                            + "JOIN tr.roleInTest.testParticipantsList testParticipantsList "
                            + "JOIN tr2.roleInTest.testParticipantsList testParticipantsList2 "
                            + "WHERE pl.actorIntegrationProfile = testParticipantsList.actorIntegrationProfileOption.actorIntegrationProfile "
                            + "AND tl.toActor = testParticipantsList.actorIntegrationProfileOption.actorIntegrationProfile.actor "
                            + "AND tl.fromActor = testParticipantsList2.actorIntegrationProfileOption.actorIntegrationProfile.actor "
                            + "AND tl.transaction = pl.transaction " + "AND pl.transaction = :transaction "
                            + "AND tr2=:testRolesInitiator " + "AND tr.test = :test");
            q.setParameter("testRolesInitiator", selectedTestStep.getTestRolesInitiator());
            q.setParameter("transaction", selectedTestStep.getTransaction());
            q.setParameter("test", editedTestForTestSteps);
            List<TestRoles> result = q.getResultList();
            if (result != null) {
                result.remove(selectedTestStep.getTestRolesInitiator());
                return result;
            }
        }
        return null;
    }

    public List<String> getPossibleMessageType() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getPossibleMessageType");
        }
        if (selectedTestStep.getTransaction() != null) {
            if (selectedTestStep.getTestRolesInitiator() != null) {
                EntityManager em = EntityManagerService.provideEntityManager();
                Query query = em
                        .createQuery("SELECT DISTINCT hmp.messageType "
                                + "FROM Hl7MessageProfile hmp,TransactionLink tl,TestRoles tr JOIN tr.roleInTest.testParticipantsList " +
                                "testParticipants "
                                + "WHERE hmp.transaction=tl.transaction "
                                + "AND testParticipants.actorIntegrationProfileOption.actorIntegrationProfile.actor=hmp.actor "
                                + "AND tl.fromActor=hmp.actor " + "AND tl.transaction=:inTransaction "
                                + "AND tr=:inTestRolesInitiator");
                query.setParameter("inTransaction", selectedTestStep.getTransaction());
                query.setParameter("inTestRolesInitiator", selectedTestStep.getTestRolesInitiator());
                List<String> list = query.getResultList();
                if (!list.isEmpty()) {
                    Collections.sort(list);
                    return list;
                }
            }
        }
        return null;
    }

    public boolean canEditUsage() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("canEditUsage");
        }
        if (this.selectedTestStep != null) {
            if (this.selectedTestStep.getTransaction() != null) {
                List<WSTransactionUsage> lwstr = WSTransactionUsage.getWSTransactionUsageFiltered(
                        this.selectedTestStep.getTransaction(), null, null, null);
                if (lwstr != null) {
                    if (!lwstr.isEmpty()) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public boolean viewTableOfCIInput() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("viewTableOfCIInput");
        }
        return !this.selectedTestStep.getInputContextualInformationList().isEmpty();
    }

    public boolean viewTableOfCIOutput() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("viewTableOfCIOutput");
        }
        return !this.selectedTestStep.getOutputContextualInformationList().isEmpty();
    }

    public void resetTransactionValue() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("resetTransactionValue");
        }
        selectedTestStep.setTransaction(null);
        selectedTestStep.setWstransactionUsage(null);
        resetTestRolesResponderValue();
    }

    public void resetTestRolesResponderValue() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("resetTestRolesResponderValue");
        }
        selectedTestStep.setTestRolesResponder(null);
        selectedTestStep.setMessageType(null);
        selectedTestStep.setWstransactionUsage(null);
    }

    public boolean validateStepIndex() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("validateStepIndex");
        }
        List<TestSteps> testStepsList = editedTestForTestSteps.getTestStepsList();
        if (selectedTestStep.getStepIndex() != null) {
            if (testStepsList != null) {
                for (TestSteps testSteps : testStepsList) {
                    if ((testSteps.getStepIndex().equals(selectedTestStep.getStepIndex()))
                            && (!testSteps.getId().equals(selectedTestStep.getId()))) {
                        FacesMessages.instance().addToControl("stepIndex",
                                "#{messages['gazelle.tm.testing.testsDefinition.stepIndexNotValid']}");

                        return false;
                    }
                }
            }
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "The step index entered was null");
            return false;
        }
        return true;
    }

    public boolean validateTestStepsDescription() {
        if (selectedTestStep.getDescription() != null) {
            if (selectedTestStep.getDescription().length() > TestSteps.MAX_DESCRIPTION_LENGTH) {
                FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.ERROR, "gazelle.validator.length.max",
                        TestSteps.MAX_DESCRIPTION_LENGTH);
                return false;
            }
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "The description entered was null");
            return false;
        }
        return true;

    }

    public void initializeContextualInformation(int i) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("initializeContextualInformation");
        }
        this.selectedContextualInformation = new ContextualInformation();
        this.inputOrOutput = i;
    }

    public void addSelectedCIToTestSteps() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("addSelectedCIToTestSteps");
        }
        if (this.selectedTestStep != null) {
            if (this.selectedContextualInformation != null) {
                if (this.inputOrOutput == 1) {
                    if (this.selectedTestStep.getInputContextualInformationList() == null) {
                        this.selectedTestStep.setInputContextualInformationList(new ArrayList<ContextualInformation>());
                    }
                    if (this.selectedContextualInformationOld != null) {
                        this.selectedTestStep.getInputContextualInformationList().remove(
                                this.selectedContextualInformationOld);
                        this.selectedContextualInformationOld = null;
                    }
                    this.selectedTestStep.getInputContextualInformationList().add(this.selectedContextualInformation);
                } else {
                    if (this.selectedTestStep.getOutputContextualInformationList() == null) {
                        this.selectedTestStep
                                .setOutputContextualInformationList(new ArrayList<ContextualInformation>());
                    }
                    if (this.selectedContextualInformationOld != null) {
                        this.selectedTestStep.getOutputContextualInformationList().remove(
                                this.selectedContextualInformationOld);
                        this.selectedContextualInformationOld = null;
                    }
                    this.selectedTestStep.getOutputContextualInformationList().add(this.selectedContextualInformation);
                }
            }
        }
    }

    public void updateSelectedContextualInformationForInput(ContextualInformation inContextualInformation) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("updateSelectedContextualInformationForInput");
        }
        this.selectedContextualInformation = inContextualInformation;
        this.contextualInformationtype = "input";
    }

    public void updateSelectedContextualInformationForOutput(ContextualInformation inContextualInformation) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("updateSelectedContextualInformationForOutput");
        }
        this.selectedContextualInformation = inContextualInformation;
        this.contextualInformationtype = "output";
    }

    public void deleteSelectedContextualInformation() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("deleteSelectedContextualInformation");
        }
        if (contextualInformationtype.equals("input")) {
            this.deleteSelectedContextualInformationFromInput();
        } else {
            this.deleteSelectedContextualInformationFromOutput();
        }
    }

    public void deleteSelectedContextualInformationFromInput() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("deleteSelectedContextualInformationFromInput");
        }
        if (selectedContextualInformation != null) {
            this.deleteSelectedContextualInformationFromInput(selectedContextualInformation);
        }
    }

    private void deleteSelectedContextualInformationFromInput(ContextualInformation inContextualInformation) {
        if (inContextualInformation != null) {
            List<ContextualInformationInstance> contextualInformationInstanceList = ContextualInformationInstance
                    .getContextualInformationInstanceListForContextualInformation(inContextualInformation);
            if (contextualInformationInstanceList != null) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "The Selected Contextual Information is used");
                return;
            }
            selectedTestStep.getInputContextualInformationList().remove(inContextualInformation);
            if (listContextualInformationToRemove == null) {
                listContextualInformationToRemove = new ArrayList<>();
            }
            listContextualInformationToRemove.add(inContextualInformation);
        }
    }

    public void deleteSelectedContextualInformationFromOutput() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("deleteSelectedContextualInformationFromOutput");
        }
        if (selectedContextualInformation != null) {
            this.deleteSelectedContextualInformationFromOutput(selectedContextualInformation);
        }
    }

    private void deleteSelectedContextualInformationFromOutput(ContextualInformation inContextualInformation) {
        if (inContextualInformation != null) {
            List<ContextualInformationInstance> contextualInformationInstanceList = ContextualInformationInstance
                    .getContextualInformationInstanceListForContextualInformation(inContextualInformation);
            if (contextualInformationInstanceList != null) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "The Selected Contextual Information is used");
                return;
            }
            List<TestSteps> testStepsList = editedTestForTestSteps.getTestStepsList();
            if (testStepsList != null) {
                for (TestSteps testSteps : testStepsList) {
                    if (testSteps.getInputContextualInformationList() != null) {
                        if (testSteps.getInputContextualInformationList().contains(inContextualInformation)) {
                            FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                                    "The Selected Contextual Information is used as input in other steps.");
                            return;
                        }
                    }
                }
            }
            selectedTestStep.getOutputContextualInformationList().remove(inContextualInformation);
            listContextualInformationToRemove.add(inContextualInformation);
        }
    }

    public void updateSelectedContextualInformation(ContextualInformation inContextualInformation, int i) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("updateSelectedContextualInformation");
        }
        this.inputOrOutput = i;
        this.selectedContextualInformation = inContextualInformation;
        this.selectedContextualInformationOld = this.selectedContextualInformation;

    }

    public List<ContextualInformation> getPossibleContextualInformations() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getPossibleContextualInformations");
        }
        if (this.editedTestForTestSteps != null) {
            if (this.selectedTestStep != null) {
                List<TestSteps> lts = new ArrayList<>();
                try {
                    lts = this.editedTestForTestSteps.getTestStepsListAntecedent(new TestSteps(this.selectedTestStep));
                } catch (Exception e) {
                    LOG.error("", e);
                }
                List<ContextualInformation> res = new ArrayList<>();
                for (TestSteps ts : lts) {
                    if (ts.getOutputContextualInformationList() != null) {
                        if (!ts.getOutputContextualInformationList().isEmpty()) {
                            res.addAll(ts.getOutputContextualInformationList());
                        }
                    }
                }
                return res;
            }
        }
        return null;
    }

    public void cancelTestStepsCreation() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("cancelTestStepsCreation");
        }
        showTestStepsEditionPanel = false;
        autoResponse = false;
        selectedTestStep = null;
        selectedStepType = null;
        resetUpload();
    }

    public boolean canEditCIAsInput(ContextualInformation currentCI) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("canEditCIAsInput");
        }
        if (this.selectedTestStep != null) {
            List<ContextualInformation> loutput = this.getPossibleContextualInformations();
            if (loutput.contains(currentCI)) {
                return false;
            }
            return true;
        }
        return false;
    }
}
