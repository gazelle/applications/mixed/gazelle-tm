package net.ihe.gazelle.tm.application.services;

public interface TMApplicationConfigurationService {

    /**
     * Check if datahouse is accessible.
     *
     * @return true if datahouse is accessible, false otherwise
     */
    boolean isDatahouseEnabled();


    /**
     * Check if automated step are activated.
     *
     * @return true if automated step is activated, false otherwise
     */
    boolean isAutomatedStepEnabled();
}
