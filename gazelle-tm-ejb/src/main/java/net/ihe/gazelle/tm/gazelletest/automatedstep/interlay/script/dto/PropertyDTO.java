package net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.script.dto;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonSetter;
import net.ihe.gazelle.tm.gazelletest.automatedstep.domain.script.Property;

import java.io.Serializable;

@JsonPropertyOrder({"key", "value"})
public class PropertyDTO implements Serializable {

    private static final long serialVersionUID = 8997121586590888856L;

    @JsonIgnore
    private Property property;

    public PropertyDTO() {
        this.property = new Property();
    }

    public PropertyDTO(Property property) {
        this.property = property;
    }

    @JsonGetter("key")
    public String getKey() {
        return property.getKey();
    }

    @JsonSetter("key")
    public void setKey(String key) {
        property.setKey(key);
    }

    @JsonGetter("value")
    public String getValue() {
        return property.getValue();
    }

    @JsonSetter("value")
    public void setValue(String value) {
        property.setValue(value);
    }

    @JsonIgnore
    public Property getProperty() {
        return property;
    }

    @JsonIgnore
    public void setProperty(Property property) {
        this.property = property;
    }
}
