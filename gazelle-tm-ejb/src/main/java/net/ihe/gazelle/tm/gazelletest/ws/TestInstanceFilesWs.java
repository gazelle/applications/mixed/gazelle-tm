package net.ihe.gazelle.tm.gazelletest.ws;

import net.ihe.gazelle.tm.gazelletest.action.*;
import org.apache.commons.lang.StringUtils;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.ws.rs.core.Response;
import java.io.IOException;

@Stateless
@Name("testInstanceFilesWs")
public class TestInstanceFilesWs implements TestInstanceFilesWsAPI {

    @In(create = true, value = "testInstanceFilesService")
    private TestInstanceFilesService testInstanceFilesService;

    private static final Logger LOGGER = LoggerFactory.getLogger(TestInstanceFilesWs.class);

    private static final String GAZELLE_TOKEN = "GazelleToken";

    /**
     * Web service to uploads logs result
     *
     * @param authorizationHeader value of the authorizationHeader in the HTTP header with the token value
     * @param testLogs            testLogs in bytes format to save
     */
    @Override
    public Response uploadTestLogs(String authorizationHeader, byte[] testLogs) {
        try {
            String tokenValue = parseTokenValueFromAuthorization(authorizationHeader);
            testInstanceFilesService.saveTestLogsInTestInstance(tokenValue, testLogs);
            LOGGER.info("Logs File Saved");
            return Response.status(Response.Status.CREATED).entity("File saved").build();
        } catch (EmptyFileException e) {
            LOGGER.error(e.getMessage());
            return Response.status(Response.Status.BAD_REQUEST).entity("Invalid File (Null or empty)").build();
        } catch (InvalidTokenException e) {
            LOGGER.error(e.getMessage());
            return Response.status(Response.Status.UNAUTHORIZED).entity(e.getMessage()).build();
        } catch (TestInstanceNotFoundException | TestInstanceClosedException e) {
            LOGGER.error(e.getMessage());
            return Response.status(Response.Status.GONE).entity(e.getMessage()).build();
        } catch (SecurityException | IOException e) {
            LOGGER.error(e.getMessage(),e.getCause());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Unable to save the uploaded file : " + e.getMessage()).build();
        }
    }

    /**
     * Web service methode to upload testReport
     * @param authorizationHeader value of the authorization in the HTTP header with the token value
     * @param testReport          testReport in bytes format to save
     */
    @Override
    public Response uploadTestReport(String authorizationHeader, byte[] testReport) {
        try {
            String tokenValue = parseTokenValueFromAuthorization(authorizationHeader);
            testInstanceFilesService.saveTestReportInTestInstance(tokenValue, testReport);
            LOGGER.info("Report Saved");
            return Response.status(Response.Status.CREATED).entity("File saved").build();
        } catch (EmptyFileException e) {
            LOGGER.error(e.getMessage());
            return Response.status(Response.Status.BAD_REQUEST).entity("Invalid File (Null or empty)").build();
        }  catch (InvalidTestReportException e) {
            LOGGER.error(e.getMessage());
            return Response.status(Response.Status.BAD_REQUEST).entity("Invalid Report : " + e.getMessage()).build();
        }catch (InvalidTokenException e) {
            LOGGER.error(e.getMessage());
            return Response.status(Response.Status.UNAUTHORIZED).entity(e.getMessage()).build();
        } catch (TestInstanceNotFoundException | TestInstanceClosedException e) {
            LOGGER.error(e.getMessage());
            return Response.status(Response.Status.GONE).entity(e.getMessage()).build();
        } catch (SecurityException | IOException e) {
            LOGGER.error(e.getMessage(),e.getCause());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Unable to save the uploaded file : " + e.getMessage()).build();
        }
    }

    protected String parseTokenValueFromAuthorization(String authorizationHeader) throws InvalidTokenException {
        if ((!StringUtils.isBlank(authorizationHeader) && authorizationHeader.contains(GAZELLE_TOKEN))) {
            LOGGER.info("Authorization header is well formed");
            return authorizationHeader.substring(authorizationHeader.lastIndexOf(GAZELLE_TOKEN) + GAZELLE_TOKEN.length()).replace(" ","");
        } else {
            throw new InvalidTokenException("Invalid token either empty or null");
        }
    }

    /**
     * Set Test InstanceFilesService for TU
     *
     * @param testInstanceFilesService
     */
    protected void setTestInstanceFilesService(TestInstanceFilesService testInstanceFilesService) {
        this.testInstanceFilesService = testInstanceFilesService;
    }
}
