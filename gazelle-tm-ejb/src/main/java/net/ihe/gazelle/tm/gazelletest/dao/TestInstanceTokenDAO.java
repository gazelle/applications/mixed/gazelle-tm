package net.ihe.gazelle.tm.gazelletest.dao;

import net.ihe.gazelle.tm.gazelletest.action.TestInstanceTokenCreationException;
import net.ihe.gazelle.tm.gazelletest.action.TestInstanceTokenSavingException;
import net.ihe.gazelle.tm.gazelletest.action.TokenNotFoundException;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstanceToken;

import java.sql.Timestamp;
import java.util.List;

public interface TestInstanceTokenDAO {

    /**
     * Save a token in Database.
     *
     * @param tokenValue     literal value of the token
     * @param username       username linked to the token
     * @param stepInstanceId TestInstance id linked to the token
     * @throws TestInstanceTokenCreationException if one mandatory parameter is missing or invalid
     * @throws TestInstanceTokenSavingException   if the TestInstanceToken already exist for this key (user and TestInstance)
     */
    void createToken(String tokenValue, String username, Integer stepInstanceId, Timestamp timestamp) throws TestInstanceTokenCreationException, TestInstanceTokenSavingException;

    /**
     * Get List of Tokens
     *
     * @return return the Tokens list selected
     */
    List<TestInstanceToken> getAllTokens();

    /**
     * Database request to get TestInstanceToken by Token Value
     *
     * @param tokenValue literal value of the token
     * @return return the TestInstanceToken selected
     * @throws TokenNotFoundException if the token has not been found
     */
    TestInstanceToken getTestInstanceToken(String tokenValue);

    /**
     * Delete expired Token Value
     *
     * @param tokenValue literal value of the token
     */
    void deleteToken(String tokenValue) throws TokenNotFoundException;

    /**
     * Delete token not valid anymore
     *
     * @param testStepInstanceId id of the test step instance linked to this token
     */
    void deleteTokenByTestStepInstance(Integer testStepInstanceId);
}
