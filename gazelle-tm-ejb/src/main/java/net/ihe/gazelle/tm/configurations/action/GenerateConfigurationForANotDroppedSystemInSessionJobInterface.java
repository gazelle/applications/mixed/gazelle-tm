package net.ihe.gazelle.tm.configurations.action;

import net.ihe.gazelle.tm.systems.model.SystemInSession;
import net.ihe.gazelle.tm.systems.model.TestingSession;

import javax.ejb.Local;

@Local
public interface GenerateConfigurationForANotDroppedSystemInSessionJobInterface {

    public void generateConfigurationForANotDroppedSystemInSessionJob(SystemInSession sIs, TestingSession testingSession);
}
