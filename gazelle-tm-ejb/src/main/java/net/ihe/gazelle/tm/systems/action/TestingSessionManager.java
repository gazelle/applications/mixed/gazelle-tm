/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.tm.systems.action;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.menu.Authorizations;
import net.ihe.gazelle.menu.GazelleMenu;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import net.ihe.gazelle.ssov7.gum.client.application.Group;
import net.ihe.gazelle.ssov7.gum.client.application.Role;
import net.ihe.gazelle.ssov7.gum.client.application.User;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import net.ihe.gazelle.ssov7.gum.client.interlay.client.utils.UserSearchParams;
import net.ihe.gazelle.tf.model.Domain;
import net.ihe.gazelle.tf.model.IntegrationProfile;
import net.ihe.gazelle.tm.gazelletest.action.TestInstanceGenerator;
import net.ihe.gazelle.tm.gazelletest.model.definition.TestType;
import net.ihe.gazelle.tm.session.TestingSessionService;
import net.ihe.gazelle.tm.skin.SkinBean;
import net.ihe.gazelle.tm.systems.ProfileInTestingSessionService;
import net.ihe.gazelle.tm.systems.model.*;
import net.ihe.gazelle.users.model.Address;
import net.ihe.gazelle.users.model.Institution;
import net.ihe.gazelle.users.model.Iso3166CountryCode;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.*;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.validation.ConstraintViolation;
import java.io.Serializable;
import java.util.*;

/**
 * <b>Class Description : </b>TestingSessionManager<br>
 * <br>
 * This class manage the TestingSession object. It corresponds to the Business Layer. All operations to implement are done in this class : <li>Add
 * a testingSession</li> <li>Delete a testingSession</li>
 * <li>Show a testingSession</li> <li>Edit a testingSession</li> <li>etc...</li>
 *
 * @class TestingSessionManager.java
 * @package net.ihe.gazelle.tf.action
 * @author Jean-Renan Chatel - / INRIA Rennes IHE development Project
 * @version 1.0 - 2008, April 28
 * @author jlabbe
 * @see > Jchatel@irisa.fr - http://www.ihe-europe.org
 */

/**
 * @author jlabbe
 */

@Name("testingSessionManager")
@Scope(ScopeType.SESSION)
@Synchronized(timeout = 10000)
public class TestingSessionManager implements Serializable, QueryModifier<TestingSession> {
    // ~ Statics variables and Class initializer ///////////////////////////////////////////////////////////////////////

    private static final long serialVersionUID = -1357012045588235656L;
    private static final int UP = 1;

    // ~ Attributes ////////////////////////////////////////////////////////////////////////////////////////////////////
    private static final int DOWN = -1;
    private static final Logger LOG = LoggerFactory.getLogger(TestingSessionManager.class);
    /**
     * entityManager is the interface used to interact with the persistence context.
     */
    @In
    private EntityManager entityManager;

    @In
    private GazelleIdentity identity;

    @In(value = "gumUserService")
    private UserService userService;

    @In(value = "testingSessionService")
    private transient TestingSessionService testingSessionService;

    @In(value = "profileInTestingSessionService")
    private ProfileInTestingSessionService profileInTestingSessionService;
    /**
     * List of all testingSession objects to be managed by this manager bean
     */
    private FilterDataModel<TestingSession> testingSessions;
    private Filter<TestingSession> filter;
    /**
     * Activated Testing Session object
     */
    private TestingSession activatedTestingSession;
    /**
     * Selected Testing Session object
     */
    private TestingSession selectedTestingSession;
    /**
     * Testing Session choosen by a user
     */
    private TestingSession testingSessionChoosen;
    /**
     * Testing Session choosen by a user for a copy
     */
    private TestingSession testingSessionChoosenForCopy;
    /**
     * List of all domains objects associated with a testing session - This object is managed by this manager bean
     */
    private List<Domain> selectedDomains;
    private List<Institution> selectedInstitutions;
    private Domain selectedDomain;
    private Address address;
    private boolean noTestingSessionChoosen = false;
    private boolean displayAll = false;
    private boolean internetTesting = false;
    private SystemInSessionStatus selectedSystemInSessionStatus;
    private List<String> possibleAdminIds;
    private List<String> selectedAdminIds;

    public boolean isInternetTesting() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("isInternetTesting");
        }
        initializeTestingSessionChoosen();
        setInternetTesting(testingSessionChoosen.getInternetTesting());
        return internetTesting;
    }

    public void setInternetTesting(boolean internetTesting) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("setInternetTesting");
        }
        this.internetTesting = internetTesting;
    }

    public boolean enableAllStatusForVendors() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("enableAllStatusForVendors");
        }
        return selectedTestingSession.isSessionWithoutMonitors();
    }

    public boolean getSystemAutoAcceptance() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getSystemAutoAcceptance");
        }
        return selectedTestingSession.isSystemAutoAcceptance();
    }

    public void setSystemAutoAcceptance(boolean systemAutoAcceptance) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("setSystemAutoAcceptance");
        }
        selectedTestingSession.setSystemAutoAcceptance(systemAutoAcceptance);
    }

    public boolean getAllowOneCompanyPlaySeveralRolesInP2PTests() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getAllowOneCompanyPlaySeveralRolesInP2PTests");
        }
        return selectedTestingSession.isAllowOneCompanyPlaySeveralRolesInP2PTests();
    }

    public void setAllowOneCompanyPlaySeveralRolesInP2PTests(boolean allowOneCompanyPlaySeveralRolesInP2PTests) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("setAllowOneCompanyPlaySeveralRolesInP2PTests");
        }
        selectedTestingSession.setAllowOneCompanyPlaySeveralRolesInP2PTests(allowOneCompanyPlaySeveralRolesInP2PTests);
    }

    public boolean getAllowOneCompanyPlaySeveralRolesInGroupTests() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getAllowOneCompanyPlaySeveralRolesInGroupTests");
        }
        return selectedTestingSession.isAllowOneCompanyPlaySeveralRolesInGroupTests();
    }

    public void setAllowOneCompanyPlaySeveralRolesInGroupTests(boolean allowOneCompanyPlaySeveralRolesInGroupTests) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("setAllowOneCompanyPlaySeveralRolesInGroupTests");
        }
        selectedTestingSession.setAllowOneCompanyPlaySeveralRolesInGroupTests(allowOneCompanyPlaySeveralRolesInGroupTests);
    }

    public boolean getAllowAllowParticipantsStartGroupTests() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getAllowAllowParticipantsStartGroupTests");
        }
        return selectedTestingSession.isAllowParticipantsStartGroupTests();
    }

    public void setAllowAllowParticipantsStartGroupTests(boolean allowAllowParticipantsStartGroupTests) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("setAllowAllowParticipantsStartGroupTests");
        }
        selectedTestingSession.setAllowParticipantsStartGroupTests(allowAllowParticipantsStartGroupTests);
    }

    public boolean isDisplayAll() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("isDisplayAll");
        }
        return displayAll;
    }

    public void setDisplayAll(boolean displayHidden) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("setDisplayAll");
        }
        this.displayAll = displayHidden;
    }

    public boolean isNoTestingSessionChoosen() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("isNoTestingSessionChoosen");
        }
        return noTestingSessionChoosen;
    }

    public void setNoTestingSessionChoosen(boolean noTestingSessionChoosen) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("setNoTestingSessionChoosen");
        }
        this.noTestingSessionChoosen = noTestingSessionChoosen;
    }

    public Domain getSelectedDomain() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getSelectedDomain");
        }
        return selectedDomain;
    }

    public void setSelectedDomain(Domain selectedDomain) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("setSelectedDomain");
        }
        this.selectedDomain = selectedDomain;
    }

    public Filter<TestingSession> getFilter() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getFilter");
        }
        if (filter == null) {
            filter = new Filter<TestingSession>(getHQLCriterion());
        }
        return filter;
    }

    private HQLCriterionsForFilter<TestingSession> getHQLCriterion() {
        TestingSessionQuery query = new TestingSessionQuery();
        HQLCriterionsForFilter criterion = query.getHQLCriterionsForFilter();
        criterion.addPath("name", query.name());
        criterion.addQueryModifier(this);
        return criterion;
    }

    /**
     * Set the variable "testingSessionChoosen" to the value of the activate testing session
     */
    public void initializeTestingSessionChoosen() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("initializeTestingSessionChoosen");
        }
        testingSessionChoosen = testingSessionService.getUserTestingSession();
        Contexts.getSessionContext().set("testingSessionChoosen", testingSessionChoosen);

    }

    /**
     * Add a testingSession to the database This operation is allowed for some granted users (check the security.drl)
     *
     * @return String : JSF page to render
     */
    @Restrict("#{s:hasPermission('TestingSessionManager', 'saveTestingSession', null)}")
    public String saveTestingSession() {
        LOG.debug("saveTestingSession");

        if (selectedTestingSession != null) {
            if ((selectedTestingSession.getRegistrationDeadlineDate() == null || selectedTestingSession.getBeginningSession() == null
                    || selectedTestingSession.getEndingSession() == null) && Boolean.FALSE.equals(selectedTestingSession.getContinuousSession())) {
                LOG.error("A non continuous testing session must have a registration deadline, a beginning date and an ending date ");
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "A non continuous testing session must have a registration deadline, a beginning date and an ending date");
                return "";
            }

            if (!selectedTestingSession.getRegistrationDeadlineDate().before(selectedTestingSession.getBeginningSession()) && Boolean.FALSE.equals(selectedTestingSession.getContinuousSession())){
                LOG.error("Registration Date has to be before the beginning of the session");
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Registration Date has to be before the beginning of the session");
                return "";
            }
            if (!selectedTestingSession.getBeginningSession().before(selectedTestingSession.getEndingSession()) && Boolean.FALSE.equals(selectedTestingSession.getContinuousSession())){
                LOG.error("Beginning of session date has to be before the end of the session");
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Beginning of session date has to be before the end of the session");
                return "";
            }

            if (address != null) {
                address = entityManager.merge(address);
                selectedTestingSession.setAddressSession(address);
            }

            if (Boolean.TRUE.equals(selectedTestingSession.getContinuousSession())) {
                selectedTestingSession.setRegistrationDeadlineDate(null);
                selectedTestingSession.setBeginningSession(null);
                selectedTestingSession.setEndingSession(null);
            }

            if (selectedTestingSession.getTargetColor() == null) {
                setSessionColor(SkinBean.DEFAULT_COLOR_HTML);
            }

            if (selectedTestingSession.getId() == null) {
                selectedTestingSession.setNextInvoiceNumber(1);
                // We have to merge before the update TestingSessionAdmin because the update needs a testingSession id to be generated by the database
                selectedTestingSession = TestingSession.mergeTestingSession(selectedTestingSession, entityManager);
                FacesMessages.instance().add(StatusMessage.Severity.INFO, "TestingSession #{testingSession.name} created");
            } else {
                FacesMessages.instance().add(StatusMessage.Severity.INFO, "TestingSession #{testingSession.name} updated");
            }
            // In the case we create a new testing session we now have an id generated, so we can proceed to add the testing session admin
            updateTestingSessionAdmins();
            // We remerge after the update TestingSessionAdmin because its part of the TestingSession table
            selectedTestingSession = TestingSession.mergeTestingSession(selectedTestingSession, entityManager);

            addProfileInTestingSessionForSelectedTestingSession(selectedTestingSession);
            profileInTestingSessionService.setTestabilityRemovedFromSession(selectedTestingSession.getIntegrationProfiles(), selectedTestingSession);

            LOG.warn("{} {} has updated testing session {}", identity.getFirstName(), identity.getLastName(),
                    selectedTestingSession.getName());
        }
        selectedSystemInSessionStatus = null;
        // added to ensure that Gazelle Menu is rebuilt based on changes to
        // selected testing session for user - in relation to ITB efforts.
        GazelleMenu.getInstance().refreshMenu();
        return "/administration/listSessions.seam";
    }

    private void updateTestingSessionAdmins() {
        List<TestingSessionAdmin> admins = new ArrayList<>();
        for (String adminId : selectedAdminIds) {
            admins.add(new TestingSessionAdmin(adminId, selectedTestingSession.getId()));
        }
        selectedTestingSession.setTestingSessionAdmins(admins);
    }

    private void addProfileInTestingSessionForSelectedTestingSession(TestingSession selectedTestingSession) {
        for (Domain domain : selectedTestingSession.getDomains()) {
            for (IntegrationProfile integrationProfile : domain.getIntegrationProfilesForDP()) {
                if (selectedTestingSession.getIntegrationProfiles().contains(integrationProfile)) {
                    profileInTestingSessionService.saveProfileInTestingSession(domain, integrationProfile, selectedTestingSession);
                }
            }
        }
    }

    /**
     * Update the testingSession's informations This operation is allowed for some granted users (check the security.drl)
     *
     * @param d : TestingSession to be updated
     */
    @Restrict("#{s:hasPermission('TestingSessionManager', 'updateTestingSession', null)}")
    public void updateTestingSession(final TestingSession d) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("updateTestingSession");
        }

        TestingSession.mergeTestingSession(d, entityManager);
    }

    /**
     * Save the testingSession's informations : Configuration This operation is allowed for some granted users (check the security.drl)
     */
    @Restrict("#{s:hasPermission('TestingSessionManager', 'updateTestingSession', null)}")
    public void saveConfigurationOverview() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("saveConfigurationOverview");
        }

        TestingSession.mergeTestingSession(testingSessionChoosen, entityManager);
        FacesMessages.instance().add(StatusMessage.Severity.INFO,
                "Configuration for "
                        + testingSessionChoosen.getName()
                        + " has been successfully saved");

    }

    /**
     * Delete the selected testingSession This operation is allowed for some granted users (check the security.drl)
     *
     * @param selectedTestingSession : testingSession to delete
     * @return String : JSF page to render
     */
    @Restrict("#{s:hasPermission('TestingSessionManager', 'deleteTestingSession', null)}")
    public void deleteTestingSession(final TestingSession selectedTestingSession) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("deleteTestingSession");
        }
        TestingSession toDelete = TestingSession.mergeTestingSession(selectedTestingSession, entityManager);

        TestingSession.removeTestingSession(toDelete, entityManager);
    }

    /**
     * Delete the selected testingSession This operation is allowed for some granted users (check the security.drl)
     *
     * @return String : JSF page to render
     */
    @Restrict("#{s:hasPermission('TestingSessionManager', 'deleteTestingSession', null)}")
    public void deleteTestingSession() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("deleteTestingSession");
        }

        deleteTestingSession(selectedTestingSession);
    }

    /**
     * Edit the selected testingSession's informations This operation is allowed for some granted users (check the security.drl)
     *
     * @param tSession : testingSession to edit
     * @return String : JSF page to render
     */
    @Restrict("#{s:hasPermission('TestingSessionManager', 'editTestingSession', null)}")
    public String editTestingSessionActionLink(final TestingSession tSession) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("editTestingSessionActionLink");
        }

        initEditTestingSession(tSession);

        return "/administration/editSession.seam";
    }

    private void initEditTestingSession(TestingSession tSession) {
        selectedTestingSession = entityManager.find(TestingSession.class, tSession.getId());
        address = selectedTestingSession.getAddressSession();
        initSelectedAdmins();
        initPossibleAdmins();
    }

    private void initSelectedAdmins() {
        selectedAdminIds = new ArrayList<>();
        for (TestingSessionAdmin admin : selectedTestingSession.getTestingSessionAdmins()) {
            selectedAdminIds.add(admin.getUserId());
        }
    }

    private void initPossibleAdmins() {
        possibleAdminIds = new ArrayList<>();
        UserSearchParams userSearchParams = new UserSearchParams()
                .setGroup(Group.GRP_TESTING_SESSION_MANAGER)
                .setActivated(true);

        List<User> admins = userService.searchNoLimit(userSearchParams);
        for (User admin : admins) {
            possibleAdminIds.add(admin.getId());
        }
    }

    public void createAddress() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("createAddress");
        }
        if (address == null) {
            address = new Address();
            address.setIso3166CountryCode(new Iso3166CountryCode(entityManager.find(Iso3166CountryCode.class, "-")));
        }
    }

    public void deleteAddress() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("deleteAddress");
        }
        if (address.getId() != null) {
            selectedTestingSession.setAddressSession(null);
            selectedTestingSession = entityManager.merge(selectedTestingSession);
            address = entityManager.find(Address.class, address.getId());
            entityManager.remove(address);
        }
        address = null;
    }

    /**
     * Create a new testingSession. This method is used by a Java client This operation is allowed for some granted users (check the security.drl)
     *
     * @param d : testingSession to create
     */
    @Restrict("#{s:hasPermission('TestingSessionManager', 'createTestingSession', null)}")
    public void createTestingSession(final TestingSession d) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("createTestingSession");
        }

        entityManager.persist(d);
        entityManager.flush();
    }

    /**
     * Get all the testingSessions existing in the database
     *
     * @return List of all existing testingSessions
     */
    public FilterDataModel<TestingSession> getTestingSessions() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getTestingSessions");
        }
        return new FilterDataModel<TestingSession>(getFilter()) {
            @Override
            protected Object getId(TestingSession testingSession) {
                return testingSession.getId();
            }
        };
    }

    @Destroy
    public void destroy() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("destroy");
        }

    }

    /**
     * When Add TestingSession button is clicked, the method called to redirect to editTestingSession page to add new testingSession
     *
     * @return String of web page to display
     */
    @Restrict("#{s:hasPermission('TestingSessionManager', 'addNewTestingSessionButton', null)}")
    public String addNewTestingSessionButton() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("addNewTestingSessionButton");
        }

        selectedTestingSession = new TestingSession();
        selectedAdminIds = new ArrayList<>();
        initPossibleAdmins();

        return "/administration/editSession.seam";
    }

    /**
     * Activate a testing session (pass to 'true' the activate attribute of that selected session, pass to 'false' all the other ones)
     *
     * @return String of web page to display
     */
    @Restrict("#{s:hasPermission('TestingSessionManager', 'activateSession', null)}")
    public void activateSession(TestingSession tSession) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("activateSession");
        }
        // From the administration page, when the administrator clicks on the testing session to activate in the events table (eg. Connectathon
        // NA2009), we activate it and pass to true its 'activate'
        // attribute
        try {

            TestingSession newSessionToActivate = entityManager.find(TestingSession.class, tSession.getId());
            if (newSessionToActivate.getActiveSession()) {
                newSessionToActivate.setActiveSession(false);
            } else {
                newSessionToActivate.setActiveSession(true);
            }
            entityManager.persist(newSessionToActivate);
            entityManager.flush();

            activatedTestingSession = testingSessionService.getUserTestingSession();
        } catch (javax.validation.ConstraintViolationException e) {
            Set<ConstraintViolation<?>> constraintViolations = e.getConstraintViolations();
            for (ConstraintViolation<?> constraintViolation : constraintViolations) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, constraintViolation.getMessage());
            }
        }
    }

    public List<Institution> getSelectedInstitutions() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getSelectedInstitutions");
        }
        return selectedInstitutions;
    }

    public void setSelectedInstitutions(List<Institution> selectedInstitutions) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("setSelectedInstitutions");
        }
        this.selectedInstitutions = selectedInstitutions;
    }

    public TestingSession getActivatedTestingSession() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getActivatedTestingSession");
        }
        return testingSessionService.getUserTestingSession();

    }

    public void setActivatedTestingSession(TestingSession activatedTestingSession) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("setActivatedTestingSession");
        }
        this.activatedTestingSession = activatedTestingSession;
    }

    /**
     * This method checks the registration Deadline (for the registration in a testing session) If the registration deadline date is expired, it
     * returns true, that way it helps to hide a button in the
     * presentation layer, for instance, a vendor won't be able to edit a registered system after the registration deadline.
     *
     * @return boolean: True/False (True if deadline date is expired)
     */
    public boolean isRegistrationDeadlineExpired() {
        LOG.trace("isRegistrationDeadlineExpired");
        return !testingSessionService.getUserTestingSession().isRegistrationOpened() &&
                !identity.hasRole(Role.VENDOR_LATE_REGISTRATION);
    }

    public boolean reuseSystemsForThisSession() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("reuseSystemsForThisSession");
        }
        return false;
    }

    /**
     * This method checks the registration Deadline (for the registration in a testing session) If the registration deadline date is expired, it
     * returns true, that way it helps to hide a button in the
     * presentation layer, for instance, a vendor won't be able to edit a registered system after the registration deadline.
     *
     * @return boolean: True/False (True if deadline date is expired)
     */
    public List<Institution> getInstitutionsForSession() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getInstitutionsForSession");
        }
        return this.getInstitutionsForSession(testingSessionChoosen);
    }

    public List<Institution> getInstitutionsForSession(TestingSession ts) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getInstitutionsForSession");
        }
        List<Institution> returnList = null;
        if (ts != null) {

            returnList = TestingSession.getListOfInstitutionsParticipatingInSession(ts);
        } else {
            returnList = getInstitutionsForActivatedSession();
        }
        Collections.sort(returnList);
        return returnList;
    }

    /**
     * returns the list of institutions owning systems for the selected testing session
     */
    public List<Institution> getInstitutionsForActivatedSession() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getInstitutionsForActivatedSession");
        }

        List<Institution> returnList = null;
        activatedTestingSession = testingSessionService.getUserTestingSession();

        returnList = TestingSession.getListOfInstitutionsRegisterInSession(activatedTestingSession);

        return returnList;
    }

    public List<Domain> getSelectedDomains() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getSelectedDomains");
        }
        return selectedDomains;
    }

    public void setSelectedDomains(List<Domain> selectedDomains) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("setSelectedDomains");
        }
        this.selectedDomains = selectedDomains;
    }

    public TestingSession getSelectedTestingSession() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getSelectedTestingSession");
        }

        if (selectedTestingSession == null) {
            selectedTestingSession = testingSessionService.getUserTestingSession();
        }
        return selectedTestingSession;
    }

    public void setSelectedTestingSession(TestingSession selectedTestingSession) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("setSelectedTestingSession");
        }
        this.selectedTestingSession = selectedTestingSession;
    }

    public TestingSession getTestingSessionChoosen() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getTestingSessionChoosen");
        }
        testingSessionChoosen = (TestingSession) Component.getInstance("testingSessionChoosen");
        return testingSessionChoosen;
    }

    public void setTestingSessionChoosen(TestingSession testingSessionChoosen) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("setTestingSessionChoosen");
        }

        Contexts.getSessionContext().set("testingSessionChoosen", testingSessionChoosen);
        this.testingSessionChoosen = testingSessionChoosen;

    }

    public TestingSession getTestingSessionChoosenForCopy() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getTestingSessionChoosenForCopy");
        }
        return testingSessionChoosenForCopy;
    }

    public void setTestingSessionChoosenForCopy(TestingSession testingSessionChoosenForCopy) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("setTestingSessionChoosenForCopy");
        }
        this.testingSessionChoosenForCopy = testingSessionChoosenForCopy;
    }

    public void removeTestingSessionChoosenForCopy() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("removeTestingSessionChoosenForCopy");
        }
        if (noTestingSessionChoosen) {
            this.testingSessionChoosenForCopy = null;
        }
    }

    public void removenoTestingSessionChoosen() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("removenoTestingSessionChoosen");
        }
        if (testingSessionChoosenForCopy != null) {
            this.noTestingSessionChoosen = false;
        }
    }

    public List<TestingSession> getPossibleTestingSessionsForCompany() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getPossibleTestingSessionsForCompany");
        }
        return TestingSession.getTestingSessionsWhereCompanyWasHere(Institution.getLoggedInInstitution().getName());
    }

    public void enableCriticalStatus() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("enableCriticalStatus");
        }
        selectedTestingSession = changeCriticalStatusForATestingSession(selectedTestingSession, true);
    }

    public void disableCriticalStatus() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("disableCriticalStatus");
        }
        selectedTestingSession = changeCriticalStatusForATestingSession(selectedTestingSession, false);
    }

    private TestingSession changeCriticalStatusForATestingSession(TestingSession inTestinSession,
                                                                  boolean isCriticalTestingSessionEnabled) {
        if (inTestinSession != null) {
            inTestinSession.setIsCriticalStatusEnabled(isCriticalTestingSessionEnabled);
            inTestinSession = TestingSession.mergeTestingSession(inTestinSession, entityManager);
            return inTestinSession;
        }
        return null;
    }

    public List<TableSession> listTableSessionAvailable(TableSession inTable) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("listTableSessionAvailable");
        }
        EntityManager em = EntityManagerService.provideEntityManager();
        Query q = em
                .createQuery("from TableSession ts where ts.id not IN ( SELECT distinct sis.tableSession.id from SystemInSession sis where sis" +
                        ".tableSession.id != null AND sis.testingSession =:inTestingSession )");
        q.setParameter("inTestingSession", testingSessionService.getUserTestingSession());

        List<TableSession> listTableSession = q.getResultList();

        if (inTable != null) {
            listTableSession.add(0, inTable);
        }
        Collections.sort(listTableSession);

        return listTableSession;
    }

    public void generateRandomlyTestInstanceForSelectedInstitutions() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("generateRandomlyTestInstanceForSelectedInstitutions");
        }
        if (selectedInstitutions != null) {
            for (Institution ins : selectedInstitutions) {
                TestInstanceGenerator.generateRandomlyTestInstanceForSelectedInstitution(ins, selectedTestingSession,
                        entityManager);
            }
        }
        FacesMessages.instance().add(StatusMessage.Severity.INFO, "test instances were automatically generated.");
    }

    public SystemInSessionStatus getSelectedSystemInSessionStatus() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getSelectedSystemInSessionStatus");
        }
        return selectedSystemInSessionStatus;
    }

    public void setSelectedSystemInSessionStatus(SystemInSessionStatus selectedSystemInSessionStatus) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("setSelectedSystemInSessionStatus");
        }
        this.selectedSystemInSessionStatus = selectedSystemInSessionStatus;
    }

    public void updateSystemInSessionStatusByTestingSession() {
        if ((selectedTestingSession != null) && (selectedSystemInSessionStatus != null)) {
            Query query = entityManager
                    .createQuery("UPDATE SystemInSession sIs SET sIs.systemInSessionStatus = :selectedSystemInSessionStatus where sIs.testingSession = " +
                            ":selectedTestingSession");
            query.setParameter("selectedSystemInSessionStatus", selectedSystemInSessionStatus);
            query.setParameter("selectedTestingSession", selectedTestingSession);
            int result = query.executeUpdate();
            if (result > 0) {
                FacesMessages.instance().add(StatusMessage.Severity.INFO,
                        "The status of " + result
                                + " systems belonging to the selected testing session was changed to "
                                + selectedSystemInSessionStatus.getKeyword());
            }
        }
    }

    public List<TestingSession> getActiveSessions() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getActiveSessions");
        }
        return TestingSession.GetAllActiveSessions();
    }

    public void addAllIntegrationProfiles() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("addAllIntegrationProfiles");
        }
        if ((selectedTestingSession != null) && (selectedDomain != null)) {
            List<IntegrationProfile> integrationProfilesForDP = selectedDomain.getIntegrationProfilesForDP();
            for (IntegrationProfile integrationProfile : integrationProfilesForDP) {
                if (integrationProfile.isAvailableForTestingSessions()) {
                    if (!selectedTestingSession.getIntegrationProfilesUnsorted().contains(integrationProfile)) {
                        selectedTestingSession.getIntegrationProfilesUnsorted().add(integrationProfile);
                    }
                }
            }
        }
    }

    public void removeAllIntegrationProfilesFromDomain() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("removeAllIntegrationProfilesFromDomain");
        }
        if ((selectedTestingSession != null) && (selectedDomain != null)) {
            List<IntegrationProfile> integrationProfilesForDP = selectedDomain.getIntegrationProfilesForDP();
            for (IntegrationProfile integrationProfile : integrationProfilesForDP) {
                selectedTestingSession.getIntegrationProfilesUnsorted().remove(integrationProfile);
            }
        }
    }

    public void removeAllIntegrationProfiles() {
        LOG.trace("removeAllIntegrationProfiles");
        if (selectedTestingSession != null) {
            selectedTestingSession.getIntegrationProfilesUnsorted().clear();
            LOG.warn("{} {} has remove all Integration Profiles from {}", identity.getFirstName(),
                    identity.getLastName(), selectedTestingSession.getName());
        }
    }

    public List<TestType> getTestTypes() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getTestTypes");
        }
        return TestType.getTestTypesWithoutMESA();
    }

    public String getSessionColor() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getSessionColor");
        }
        String targetColor = SkinBean.DEFAULT_COLOR_HTML;
        if (selectedTestingSession != null) {
            targetColor = selectedTestingSession.getTargetColor();
        }
        return targetColor;
    }

    public void setSessionColor(String sessionColor) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("setSessionColor");
        }
        selectedTestingSession.setTargetColor(sessionColor);
    }

    public List<String> getPossibleAdminIds() {
        return possibleAdminIds;
    }

    public List<String> getSelectedAdminIds() {
        return selectedAdminIds;
    }

    public void setPossibleAdminIds(List<String> possibleAdminIds) {
        this.possibleAdminIds = possibleAdminIds;
    }

    public void setSelectedAdminIds(List<String> selectedAdminIds) {
        this.selectedAdminIds = selectedAdminIds;
    }

    public String getUserLabel(String userId) {
        return userService.getUserDisplayNameWithoutException(userId);
    }

    public List<TestingSession> getTestingSessionWhereUserIsAdmin(User user) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getTestingSessionWhereUserIsAdmin");
        }
        return TestingSession.getTestingSessionWhereUserIsAdmin(user.getId());
    }

    public void markAsDefaultTestingSession(TestingSession testingSession) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("markAsDefaultTestingSession");
        }
        EntityManager em = EntityManagerService.provideEntityManager();
        unSetDefaultTestingSession(em);

        testingSession.setIsDefaultTestingSession(true);
        em.merge(testingSession);
        em.flush();
    }

    /**
     * Remove default testing session mark from previous default testing session
     *
     * @param em entity manager
     */
    private void unSetDefaultTestingSession(EntityManager em) {
        TestingSessionQuery query = new TestingSessionQuery();
        query.isDefaultTestingSession().eq(true);
        List<TestingSession> listDistinct = query.getListDistinct();
        for (TestingSession testingSession : listDistinct) {
            testingSession.setIsDefaultTestingSession(false);
            em.merge(testingSession);
        }
    }

    public Address getAddress() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getAddress");
        }
        return address;
    }

    public void setAddress(Address address) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("setAddress");
        }
        this.address = address;
    }

    public void modifyQuery(HQLQueryBuilder<TestingSession> hqlQueryBuilder, Map<String, Object> map) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("modifyQuery");
        }
        TestingSessionQuery testingSessionQuery = new TestingSessionQuery();
        if (!isDisplayAll()) {
            hqlQueryBuilder.addRestriction(testingSessionQuery.hiddenSession().eqRestriction(false));
        }
    }

    public void moveOrderUp(int testingSessionId) {
        move(testingSessionId, DOWN);
    }

    public void moveOrderDown(int testingSessionId) {
        move(testingSessionId, UP);
    }

    public void move(int testingSessionId, int direction) {
        try {
            TestingSession t = entityManager.find(TestingSession.class, testingSessionId);
            Integer i = t.getOrderInGUI();

            if (!isCurrentGuiOrderIsOverloaded(i)) {
                moveFirstElementFromWantedPosition(direction, i);
            }

            t.setOrderInGUI(i + direction);
            entityManager.merge(t);
        } catch (javax.validation.ConstraintViolationException e) {
            Set<ConstraintViolation<?>> constraintViolations = e.getConstraintViolations();
            for (ConstraintViolation<?> constraintViolation : constraintViolations) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, constraintViolation.getMessage());
            }

        }
    }

    /**
     * We move only the first one
     * <p>
     * The gui order is not unique, so multiple testing sessions can have the same number
     * We want to avoid moving all the testing session at the same time
     *
     * @param position  orderGui
     * @param direction +1: move down , -1 move up
     */
    private void moveFirstElementFromWantedPosition(int direction, Integer position) {
        TestingSessionQuery q = new TestingSessionQuery();
        q.orderInGUI().eq(position + direction);
        List<TestingSession> listDistinct = q.getListDistinct();

        TestingSession testingSession = listDistinct.get(0);
        testingSession.setOrderInGUI(position);
        entityManager.merge(testingSession);
    }

    /**
     * @param position orderGui
     * @return true if there is more than one testing session with the orderGui == to position
     */
    private boolean isCurrentGuiOrderIsOverloaded(int position) {
        TestingSessionQuery q = new TestingSessionQuery();
        q.orderInGUI().eq(position);
        List<TestingSession> TsAtCurrentPosition = q.getListDistinct();
        return TsAtCurrentPosition.size() != 1;
    }

    public boolean isUserAdminOfCurrentTestingSession(TestingSession currentTestingSession) {
        TestingSession testingSession = testingSessionService.getUserTestingSession();
        if (testingSession != null && testingSession.getId().equals(currentTestingSession.getId())) {
            return Authorizations.TESTING_SESSION_ADMIN_OF_CURRENT_TESTING_SESSION.isGranted();
        }
        return false;
    }


    /**
     * get the status of the preference access sample
     *
     * @return boolean
     */
    public boolean isAllowedToAccessToAllSamples() {
        return Authorizations.SAMPLE_SEARCH.isGranted();
    }

    public boolean isOngoingRegistrationSession(TestingSession testingSession) {
        if (testingSession == null) {
            return false;
        }
        if (testingSession.getRegistrationDeadlineDate() == null) {
            return false;
        }
        Date now = new Date();
        return now.compareTo(testingSession.getRegistrationDeadlineDate()) <= 0;
    }

    public boolean isOngoingTestingPhaseSession(TestingSession testingSession) {
        if (testingSession == null) {
            return false;
        }
        if (testingSession.getEndingSession() == null || testingSession.getBeginningSession() == null) {
            return testingSession.getContinuousSession();
        }
        Date now = new Date();
        return now.after(testingSession.getBeginningSession())
                && now.before(testingSession.getEndingSession());
    }
}
