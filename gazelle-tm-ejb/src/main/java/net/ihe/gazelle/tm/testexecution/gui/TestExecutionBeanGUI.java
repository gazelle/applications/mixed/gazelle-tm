package net.ihe.gazelle.tm.testexecution.gui;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import net.ihe.gazelle.tm.gazelletest.model.definition.TestPeerType;
import net.ihe.gazelle.tm.gazelletest.model.definition.TestType;
import net.ihe.gazelle.tm.gazelletest.model.instance.SystemAIPOResultForATestingSession;
import net.ihe.gazelle.tm.session.TestingSessionService;
import net.ihe.gazelle.tm.testexecution.*;
import net.ihe.gazelle.tm.testexecution.sorting.SUTCapabilityReportSortMethod;
import net.ihe.gazelle.tm.testexecution.sorting.SutSortOrder;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.*;
import org.slf4j.LoggerFactory;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.*;

@Name(value = "testExecutionBeanGui")
@AutoCreate
@Scope(ScopeType.PAGE)
public class TestExecutionBeanGUI implements Serializable {

    public static final String REQUIRED = "Required";
    private static final long serialVersionUID = -1982100096859995379L;
    private static final org.slf4j.Logger log = LoggerFactory.getLogger(TestExecutionBeanGUI.class);

    @In(value = "SUTCapabilityReportService")
    private transient SUTCapabilityReportService sutCapabilityReportService;
    private Filter<SystemAIPOResultForATestingSession> filter;
    private transient PartnerReport selectedPartnerReport;
    private TestType selectedTestType;
    private TestPeerType selectedTestPeerType;
    private SUTCapabilityReportSortMethod selectedSort = SUTCapabilityReportSortMethod.SYSTEM;

    private FilterDataModel<SystemAIPOResultForATestingSession> dataModel;
    private final transient Map<SystemAIPOResultForATestingSession, SUTCapabilityReport> cachedSutCapabilityReports = new HashMap<>();
    
    @In(value = "testingSessionService")
    private transient TestingSessionService testingSessionService;

    @Create
    public void init() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExternalContext externalContext = facesContext.getExternalContext();
        filter = sutCapabilityReportService.getFilter(externalContext.getRequestParameterMap(), Collections.singletonList(getQueryModifier()));
        dataModel = sutCapabilityReportService.getDataModel(filter);
    }

    public TestType getSelectedTestType() {
        return selectedTestType;
    }

    public void setSelectedTestType(TestType selectedTestType) {
        this.selectedTestType = selectedTestType;
    }

    public TestPeerType getSelectedTestPeerType() {
        return selectedTestPeerType;
    }

    public void setSelectedTestPeerType(TestPeerType selectedTestPeerType) {
        this.selectedTestPeerType = selectedTestPeerType;
    }

    public SUTCapabilityReport getSutCapabilityReport(SystemAIPOResultForATestingSession systemAIPOResultForATestingSession) {
        SUTCapabilityReport cachedReport = cachedSutCapabilityReports.get(systemAIPOResultForATestingSession);
        if (cachedReport != null)
            return cachedReport;

        SUTCapabilityReport sutCapabilityReport = sutCapabilityReportService.constructSutCapabilityReport(systemAIPOResultForATestingSession);
        cachedSutCapabilityReports.put(systemAIPOResultForATestingSession, sutCapabilityReport);
        return sutCapabilityReport;
    }

    public FilterDataModel<SystemAIPOResultForATestingSession> getDataModel() {
        return dataModel;
    }

    public SUTCapabilityReportSortMethod[] getAllSortMethods() {
        return SUTCapabilityReportSortMethod.values();
    }

    public List<TestReport> getTestReports(SUTCapabilityReport sutCapabilityReport) {
        selectedTestType = (TestType) this.filter.getFilterValues().get("testType");
        selectedTestPeerType = (TestPeerType) this.filter.getFilterValues().get("testPeerType");
        List<TestReport> testReportsFiltered = new ArrayList<>();
        for (TestReport testReport : sutCapabilityReport.getTestReports()) {
            if (!testReport.isInMetatest() && (selectedTestType == null || selectedTestType.getKeyword().equals(testReport.getType())) && (selectedTestPeerType == null || selectedTestPeerType.getKeyword().equals(testReport.getPeerType()))) {
                testReportsFiltered.add(testReport);
            }
        }
        Collections.sort(testReportsFiltered);

        sortMetatests(sutCapabilityReport, testReportsFiltered);

        return testReportsFiltered;
    }

    private void sortMetatests(SUTCapabilityReport sutCapabilityReport, List<TestReport> testReportsFiltered) {
        // This block of code has been added to make sure Metatest are on top of the tests they include, otherwise,
        // the sorting will put them apart

        List<TestReport> childrenTests = new ArrayList<>();
        for (TestReport testReport : sutCapabilityReport.getTestReports()) {
            if (selectedTestType == null) {
                if (testReport.isMetatest()) {
                    childrenTests.addAll(testReport.getEquivalentTestReports());
                    Collections.sort(childrenTests);
                    testReportsFiltered.addAll(testReportsFiltered.indexOf(testReport) + 1, childrenTests);
                }

            } else {
                if (selectedTestType.getKeyword().equals(testReport.getType()) && testReport.isMetatest()) {
                    childrenTests.addAll(testReport.getEquivalentTestReports());
                    Collections.sort(childrenTests);
                    testReportsFiltered.addAll(testReportsFiltered.indexOf(testReport) + 1, childrenTests);
                }
            }
            childrenTests = new ArrayList<>();
        }
    }

    public SUTCapabilityReportSortMethod getSelectedSort() {
        return selectedSort;
    }

    public void setSelectedSort(SUTCapabilityReportSortMethod selectedSort) {
        this.selectedSort = selectedSort;
        filter.modified();
    }

    public PartnerReport getSelectedPartnerReport() {
        return selectedPartnerReport;
    }

    public void setSelectedPartnerReport(PartnerReport selectedPartnerReport) {
        this.selectedPartnerReport = selectedPartnerReport;
    }

    public String initializeOrStartTestRun(TestReport testReport) {
        return "/testing/test/test/StartTestInstance.seam?startTestInstanceSISid=" + testReport.getSystemInSessionId()
                + "&startTestInstanceTRid=" + testReport.getTestRolesId();
    }

    public void clearFilters() {
        filter.clear();
        selectedTestType = null;
    }

    public void refreshPage() {
        filter.modified();
        cachedSutCapabilityReports.clear();
    }

    public boolean showTable(SUTCapabilityReport sutCapabilityReport) {
        return sutCapabilityReport != null
                && sutCapabilityReport.getTestReports() != null
                && !sutCapabilityReport.getTestReports().isEmpty();
    }

    public String showPartnersCount(PartnerReport partnerReport) {
        return partnerReport.getCountTested() + "/" + partnerReport.getCountAvailable();
    }

    public String getBadgeClass(TestReport testReport) {
        if (testReport.getNumberOfTestRunVerified() >= testReport.getTarget()) {
            return "badge gzl-badge-success";
        } else if (testReport.getOptionalityKeyword().equals(REQUIRED)) {
            return "badge gzl-badge-warning";
        } else {
            return "badge";
        }
    }

    public String getVerifiedProgressStyle(SUTCapabilityReport sutCapabilityReport) {
        int totalVerifiedRun = getTotalVerifiedRun(sutCapabilityReport);
        int totalTarget = getTotalTarget(sutCapabilityReport);
        double percentage = 0.0;
        if (totalTarget != 0) {
            percentage = Math.min(100.0, ((double) totalVerifiedRun / (double) totalTarget) * 100);
        }
        return "width:" + percentage + "%";
    }

    public String getTargetProgressLeftStyle(SUTCapabilityReport sutCapabilityReport) {
        int totalVerifiedRun = getTotalVerifiedRun(sutCapabilityReport);
        int totalTarget = getTotalTarget(sutCapabilityReport);
        double percentage = 0.0;
        if (totalTarget != 0) {
            percentage = Math.max(0.0, (((double) totalTarget - (double) totalVerifiedRun) / totalTarget) * 100);
        }
        return "width:" + percentage + "%";
    }

    public int getNumberOfTestLeft(SUTCapabilityReport sutCapabilityReport) {
        return getTotalTarget(sutCapabilityReport) - getTotalVerifiedRun(sutCapabilityReport);
    }

    private int getTotalTarget(SUTCapabilityReport sutCapabilityReport) {
        int totalTarget = 0;
        for (TestReport testReport : sutCapabilityReport.getTestReports()) {
            if (testReport.getOptionalityKeyword().equals(REQUIRED) && !testReport.isInMetatest()) {
                totalTarget += testReport.getTarget();
            }
        }
        return totalTarget;
    }

    public boolean showProgressBar(SUTCapabilityReport sutCapabilityReport) {
        boolean isUnspecified = true;
        if (sutCapabilityReport == null)
            return false;
        if (sutCapabilityReport.getStatus() != null) {
            isUnspecified = sutCapabilityReport.getStatus().isUnspecified();
        }
        return isUnspecified && getTotalTarget(sutCapabilityReport) > 0 && showTable(sutCapabilityReport);
    }

    public int getTotalVerifiedRun(SUTCapabilityReport sutCapabilityReport) {
        int totalVerifiedRun = 0;
        for (TestReport testReport : sutCapabilityReport.getTestReports()) {
            if (!testReport.isInMetatest() && testReport.getOptionalityKeyword().equals(REQUIRED)) {
                totalVerifiedRun += Math.min(testReport.getNumberOfTestRunVerified(), testReport.getTarget());
            }
        }
        return totalVerifiedRun;
    }

    public Filter<SystemAIPOResultForATestingSession> getFilter() {
        return filter;
    }

    public boolean isAbleToStartTestInstance() {
        return !testingSessionService.getUserTestingSession().isClosed();
    }

    public String getRunningPartnersForTestRunReport(TestRunReport testRunReport) {
        StringBuilder runningPartners = new StringBuilder("Running partner(s) :\n");
        for (String runningPartner : testRunReport.getRunningPartners()) {
            runningPartners.append(runningPartner).append("\n");
        }
        return runningPartners.toString();
    }

    private QueryModifier<SystemAIPOResultForATestingSession> getQueryModifier() {
        return new QueryModifier<SystemAIPOResultForATestingSession>() {
            private static final long serialVersionUID = -4132923669515753443L;

            @Override
            public void modifyQuery(HQLQueryBuilder<SystemAIPOResultForATestingSession> queryBuilder, Map<String, Object> filterValuesApplied) {
                for (SutSortOrder sutSortOrder : selectedSort.getSortOrders()) {
                    queryBuilder.addOrder(sutSortOrder.getQueryPath(), sutSortOrder.isAscending());
                }
            }
        };
    }
}
