package net.ihe.gazelle.tm.testexecution;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import net.ihe.gazelle.tm.gazelletest.model.instance.SystemAIPOResultForATestingSession;
import net.ihe.gazelle.tm.systems.model.TestingDepth;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import java.util.List;
import java.util.Map;

@Name(value = "SUTCapabilityReportService")
@AutoCreate
@Scope(ScopeType.STATELESS)
public class SUTCapabilityReportServiceImpl implements SUTCapabilityReportService {
    @In(value = "sutReportServiceDAO")
    private SUTReportServiceDAO sutReportServiceDAO;

    @Override
    public Filter<SystemAIPOResultForATestingSession> getFilter(Map<String, String> requestParameterMap,
                                                                List<QueryModifier<SystemAIPOResultForATestingSession>> queryModifiers) {
        return sutReportServiceDAO.getFilter(requestParameterMap, queryModifiers);
    }

    @Override
    public FilterDataModel<SystemAIPOResultForATestingSession> getDataModel(Filter<SystemAIPOResultForATestingSession> filter) {
        return new FilterDataModel<SystemAIPOResultForATestingSession>(filter) {
            private static final long serialVersionUID = 7614706135649327624L;

            @Override
            protected Object getId(SystemAIPOResultForATestingSession systemAIPOResultForATestingSession) {
                return systemAIPOResultForATestingSession.getId();
            }
        };
    }

    @Override
    public SUTCapabilityReport constructSutCapabilityReport(SystemAIPOResultForATestingSession systemAIPOResultForATestingSession) {
        SUTCapabilityReport sutCapabilityReport = new SUTCapabilityReport();
        sutCapabilityReport.setSutKeyword(systemAIPOResultForATestingSession.getSystemActorProfile().getSystem().getKeyword());
        sutCapabilityReport.setActor(systemAIPOResultForATestingSession.getSystemActorProfile().getActorIntegrationProfileOption().getActorIntegrationProfile().getActor().getName());
        sutCapabilityReport.setOption(systemAIPOResultForATestingSession.getSystemActorProfile().getActorIntegrationProfileOption().getIntegrationProfileOption().getName());
        sutCapabilityReport.setProfile(systemAIPOResultForATestingSession.getSystemActorProfile().getActorIntegrationProfileOption().getActorIntegrationProfile().getIntegrationProfile().getKeyword());
        sutCapabilityReport.setSystemAIPOResultForATestingSessionId(systemAIPOResultForATestingSession.getId());
        sutCapabilityReport.setStatus(SUTCapabilityStatus.getStatusByStatusResults(systemAIPOResultForATestingSession.getStatus()));

        TestingDepth currentTestingDepth = systemAIPOResultForATestingSession.getSystemActorProfile().getTestingDepth();
        if (currentTestingDepth != null) {
            sutCapabilityReport.setTestingDepth(TestingDepthStatus.getStatusByKeyword(currentTestingDepth.getName()));
        }
        sutCapabilityReport.setTestReports(sutReportServiceDAO.getTestReports(sutCapabilityReport));
        return sutCapabilityReport;
    }

    @Override
    public void updateEvaluation(List<SystemAIPOResultForATestingSession> aipoResults) {
        for (SystemAIPOResultForATestingSession aipoResult : aipoResults) {
            sutReportServiceDAO.updateEvaluation(aipoResult);
        }
    }
}
