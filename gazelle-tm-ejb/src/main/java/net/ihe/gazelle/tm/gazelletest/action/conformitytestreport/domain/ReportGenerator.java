package net.ihe.gazelle.tm.gazelletest.action.conformitytestreport.domain;

import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import net.ihe.gazelle.ssov7.gum.client.application.Role;
import net.ihe.gazelle.tf.model.ActorIntegrationProfileOption;
import net.ihe.gazelle.tm.gazelletest.action.conformitytestreport.dao.ConformityTestReportDAO;
import net.ihe.gazelle.tm.gazelletest.action.conformitytestreport.dao.ConformityTestReportDAOImpl;
import net.ihe.gazelle.tm.gazelletest.action.conformitytestreport.exceptions.UnauthorizedException;
import net.ihe.gazelle.tm.gazelletest.action.conformitytestreport.model.*;
import net.ihe.gazelle.tm.gazelletest.bean.SAPResultDetailItem;
import net.ihe.gazelle.tm.gazelletest.model.definition.MetaTest;
import net.ihe.gazelle.tm.gazelletest.model.definition.TestRoles;
import net.ihe.gazelle.tm.gazelletest.model.instance.SystemAIPOResultForATestingSession;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstance;
import net.ihe.gazelle.tm.systems.model.System;
import net.ihe.gazelle.tm.systems.model.SystemInSession;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import net.ihe.gazelle.token.wsclient.InvalidTokenException;
import net.ihe.gazelle.users.model.Institution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class ReportGenerator {

    private static final Logger LOG = LoggerFactory.getLogger(ReportGenerator.class);

    private ConformityTestReportDAO reportDAO;

    public ReportGenerator(GazelleIdentity identity) {
        this.reportDAO = new ConformityTestReportDAOImpl(identity);
    }

    public ReportGenerator(ConformityTestReportDAO reportDAO) {
        this.reportDAO = reportDAO;
    }

    public void checkCompanyKeyWord(String companyKeyword) throws InvalidTokenException {
        for (Institution institution : reportDAO.getInstitutionQueryList()) {
            if (institution.getKeyword().equals(companyKeyword)) {
                return;
            }
        }
        throw new InvalidTokenException("Company unknown for retrieving Organisations");
    }

    public InstitutionListModel exportOrganisations(GazelleIdentity identity) throws InvalidTokenException {
        LOG.info("Executing exportOrganisations(IdentityToken identity)");
        checkCompanyKeyWord(identity.getOrganisationKeyword());
        InstitutionListModel institutionListModel = new InstitutionListModel();
        List<InstitutionModel> institutionModels = new ArrayList<>();
        List<Institution> institutions;
        if (identity.hasRole(Role.ADMIN)) {
            institutions = reportDAO.getInstituionList();
        } else {
            Institution institutionToAdd = reportDAO.findInstituion(identity.getOrganisationKeyword());
            institutions = new ArrayList<>();
            institutions.add(institutionToAdd);
        }
        for (Institution inst : institutions) {
            InstitutionModel im = new InstitutionModel(inst.getId(), inst.getName(), inst.getKeyword());
            institutionModels.add(im);
        }
        institutionListModel.setInstitutions(institutionModels);
        return institutionListModel;
    }

    public TestingSessionListModel exportTestingSessionInfos(GazelleIdentity identity, Boolean isOpen) throws InvalidTokenException {
        LOG.info("Executing exportTestingSessionInfos(IdentityToken identity, isOpen)");
        checkCompanyKeyWord(identity.getOrganisationKeyword());
        TestingSessionListModel testingSessionListModel = new TestingSessionListModel();
        List<TestingSessionModel> sessionModels = new ArrayList<>();
        List<TestingSession> allSessions = parseSessionParameters(identity.getOrganisationKeyword(), identity.hasRole(Role.ADMIN), isOpen);
        if (!allSessions.isEmpty()) {
            for (TestingSession session : allSessions) {
                TestingSessionModel tsm = new TestingSessionModel(session.getId(), session.getDescription(), session.getBeginningSession(), session.getEndingSession(), session.getRegistrationDeadlineDate(), session.getActiveSession(), session.getSessionClosed(), session.getHiddenSession());
                sessionModels.add(tsm);
            }
        } else {
            TestingSessionModel tsm = new TestingSessionModel();
            sessionModels.add(tsm);
        }
        testingSessionListModel.setTestingSession(sessionModels);
        return testingSessionListModel;
    }

    private List<TestingSession> parseSessionParameters(String companyName, boolean isAdmin, Boolean isOpen) {
        LOG.info("Executing parseSessionParameters(companyName, isAdmin, isOpen)");

        List<TestingSession> filteredSessions;
        if (isAdmin) {
            filteredSessions = reportDAO.allSessions();
        } else {
            filteredSessions = reportDAO.allSessionsWhereCompanyWasHere(companyName);
        }

        List<TestingSession> result = new ArrayList<>();

        if (isOpen == null) {
            openOrClosedSession(filteredSessions, result);
        } else if (Boolean.TRUE.equals(isOpen)) {
            openSession(filteredSessions, result);
        } else {
            closedSession(filteredSessions, result);
        }

        return result;
    }

    private void closedSession(List<TestingSession> filteredSessions, List<TestingSession> result) {
        for (TestingSession session : filteredSessions) {
            if (session.isClosed() && Boolean.FALSE.equals(session.getHiddenSession())) {
                result.add(session);
            }
        }
    }

    private void openSession(List<TestingSession> filteredSessions, List<TestingSession> result) {
        for (TestingSession session : filteredSessions) {
            if (!session.isClosed() && Boolean.FALSE.equals(session.getHiddenSession())) {
                result.add(session);
            }
        }
    }

    private void openOrClosedSession(List<TestingSession> filteredSessions, List<TestingSession> result) {
        for (TestingSession session : filteredSessions) {
            if (Boolean.FALSE.equals(session.getHiddenSession())) {
                result.add(session);
            }
        }
    }

    public SystemListModel exportSystemInfos(GazelleIdentity identity, Integer testSessionId, Integer organisationId) throws UnauthorizedException, InvalidTokenException, NullPointerException {

        checkCompanyKeyWord(identity.getOrganisationKeyword());
        TestingSession selectedSession = new TestingSession();
        boolean isValidTestingSession = false;
        for (TestingSession ts : reportDAO.getTestingSessionList()) {
            if (ts.getId().equals(testSessionId)) {
                isValidTestingSession = true;
                selectedSession = ts;
                break;
            }
        }
        if (!isValidTestingSession) {
            throw new UnauthorizedException("Unknown testingSession ID");
        }

        if (!identity.hasRole(Role.ADMIN)) {
            boolean isValidCompanyName = false;
            for (SystemInSession sis : reportDAO.getSystemsInSessionForSession(selectedSession)) {
                if (reportDAO.getCompanyNameOfInstitutionOfSelectedSystemInSession(sis).equals(identity.getOrganisationKeyword())) {
                    isValidCompanyName = true;
                }
            }
            if (!isValidCompanyName) {
                throw new InvalidTokenException("Company unknown for retrieving Systems");
            }
        }
        return getSystemsList(identity.getOrganisationKeyword(), identity.hasRole(Role.ADMIN), testSessionId, organisationId);
    }

    private SystemListModel getSystemsList(String companyName, boolean isAdmin, Integer testSessionId, Integer organisationId) throws InvalidTokenException, NullPointerException {
        LOG.info("Executing getSystemsList(companyName, isAdmin, testSessionId, organisationId)");

        SystemListModel systemListModel = new SystemListModel();
        List<SystemInSession> systemInSessions;
        TestingSession session = reportDAO.getTestingSessionById(testSessionId);
        if (isAdmin) {
            if (organisationId == null) {
                systemInSessions = reportDAO.getSystemsInSessionForSession(session);
            } else {
                Institution institution = reportDAO.findInstitutionById(organisationId);
                if (institution == null) {
                    throw new NullPointerException();
                }
                systemInSessions = reportDAO.getSystemsInSessionForCompanyNameForSession(institution.getName(), session);
            }
        } else {
            if (organisationId == null || reportDAO.findInstitutionById(organisationId).getName().equals(companyName)) {
                systemInSessions = reportDAO.getSystemsInSessionForCompanyNameForSession(companyName, session);
            } else {
                throw new InvalidTokenException("Wrong organisation ID");
            }
        }
        systemListModel.setSystems(generateSystemModelList(systemInSessions));
        return systemListModel;
    }

    private List<SystemModel> generateSystemModelList(List<SystemInSession> systemInSessions) {
        LOG.info("Executing generateSystemModelList(systemInSessions)");

        List<SystemModel> systemModels = new ArrayList<>();
        for (SystemInSession sis : systemInSessions) {
            SystemModel sm;
            if (sis.getSystem() == null) {
                if (sis.getTestingSession() == null) {
                    sm = new SystemModel(null, null, null, null, null, null);
                } else {
                    sm = new SystemModel(null, null, null, sis.getTestingSession().getId(), sis.getTestingSession().getDescription(), sis.getSystem().getUniqueInstitution().getId());
                }
            } else {
                if (sis.getTestingSession() == null) {
                    sm = new SystemModel(sis.getSystem().getId(), sis.getSystem().getName(), sis.getSystem().getKeyword(), null, null, sis.getSystem().getUniqueInstitution().getId());
                } else {
                    sm = new SystemModel(sis.getSystem().getId(), sis.getSystem().getName(), sis.getSystem().getKeyword(), sis.getTestingSession().getId(), sis.getTestingSession().getDescription(), sis.getSystem().getUniqueInstitution().getId());
                }
            }
            systemModels.add(sm);
        }
        return systemModels;
    }

    /**
     * Create a Conformity Test Report when a System is selected and export it as a XML
     *
     * @return String empty character to close the loading page
     */
    public ConformityTestReportModel exportSystemReportAsXML(GazelleIdentity identity, Integer testSessionId, Integer sutId) throws UnauthorizedException, InvalidTokenException {

        TestingSession selectedSession = new TestingSession();
        System selectedSystem = new System();

        boolean isValidTestingSession = false;
        for (TestingSession ts : reportDAO.getTestingSessionList()) {
            if (ts.getId().equals(testSessionId)) {
                isValidTestingSession = true;
                selectedSession = ts;
                break;
            }
        }
        if (!isValidTestingSession) {
            throw new UnauthorizedException("Unknown testingSession ID");
        }

        boolean isValidSystemId = false;
        for (SystemInSession sis : reportDAO.getSystemsInSessionForSession(selectedSession)) {
            if (sis.getSystem().getId().equals(sutId)) {
                isValidSystemId = true;
                selectedSystem = sis.getSystem();
                break;
            }
        }
        if (!isValidSystemId) {
            throw new UnauthorizedException("Unknown sut ID");
        }
        if (!reportDAO.getCompanyNameOfInstitutionOfSelectedSystem(selectedSystem).equals(identity.getOrganisationKeyword()) && Boolean.TRUE.equals(!identity.hasRole(Role.ADMIN))) {
            throw new InvalidTokenException("Not allowed to retrieve these information");
        }

        SystemInSession sis = reportDAO.getSystemInSessionForSession(selectedSystem, selectedSession);

        return getReportForSystem(sis);
    }

    /**
     * Generate test report for a system in a test session
     *
     * @param systemInSession selected SystemInSession
     * @return ConformityTestReportModel
     */
    public ConformityTestReportModel getReportForSystem(SystemInSession systemInSession) {
        LOG.info("Executing getReportForSystem(systemInSession)");

        ConformityTestReportModel systemReportModel = new ConformityTestReportModel();
        systemReportModel.setSystem(systemInSession.getSystem().getId(), systemInSession.getSystem().getName(), systemInSession.getSystem().getKeyword(), systemInSession.getTestingSession().getId(), systemInSession.getTestingSession().getDescription(), systemInSession.getSystem().getUniqueInstitution().getId());
        systemReportModel.setTestingSession(systemInSession.getTestingSession().getId(), systemInSession.getTestingSession().getDescription(), systemInSession.getTestingSession().getBeginningSession(), systemInSession.getTestingSession().getEndingSession(), systemInSession.getTestingSession().getRegistrationDeadlineDate(), systemInSession.getTestingSession().getActiveSession(), systemInSession.getTestingSession().getSessionClosed(), systemInSession.getTestingSession().getHiddenSession());
        systemReportModel.setInstitution(systemInSession.getSystem().getUniqueInstitution().getId(), systemInSession.getSystem().getUniqueInstitution().getName(), systemInSession.getSystem().getUniqueInstitution().getKeyword());

        List<SystemAIPOResultForATestingSession> systemeResult = reportDAO.getResultOfSystem(systemInSession.getSystem(),
                systemInSession.getTestingSession());
        List<AIPOReportModel> listofAIPO = new ArrayList<>();
        for (SystemAIPOResultForATestingSession sysARTS : systemeResult) {
            List<SAPResultDetailItem> aipoDetails = reportDAO.computeDetail(sysARTS);
            AIPOReportModel aipoReportModel = generateAIPOModel(sysARTS.getSystemActorProfile().getActorIntegrationProfileOption(), aipoDetails);
            listofAIPO.add(aipoReportModel);
        }
        systemReportModel.setAipoModelList(listofAIPO);
        return systemReportModel;
    }

    /**
     * Generate the Aipo Structure item of the report
     *
     * @param aipo        selected actor
     * @param aipoDetails List of details of the selected Actor
     * @return Aipo item of report
     */
    private AIPOReportModel generateAIPOModel(ActorIntegrationProfileOption aipo, List<SAPResultDetailItem> aipoDetails) {
        LOG.info("Executing generateAIPOModel(ActorIntegrationProfileOption, List<SAPResultDetailItem>)");

        AIPOReportModel aipoReportModel = new AIPOReportModel();
        DataOfTFObject actor = new DataOfTFObject(aipo.getActorIntegrationProfile().getActor().getKeyword(),
                aipo.getActorIntegrationProfile().getActor().getName());
        aipoReportModel.setActor(actor);
        DataOfTFObject integrationProfile = new DataOfTFObject(aipo.getActorIntegrationProfile().getIntegrationProfile().getKeyword(),
                aipo.getActorIntegrationProfile().getIntegrationProfile().getName());
        aipoReportModel.setActorIntegrationProfile(integrationProfile);
        DataOfTFObject option = new DataOfTFObject(aipo.getIntegrationProfileOption().getKeyword(), aipo.getIntegrationProfileOption().getName());
        aipoReportModel.setActorIntegrationProfileOption(option);
        aipoReportModel.setTestReportList(generateTestReports(aipoDetails));
        return aipoReportModel;
    }

    /**
     * Retrieve and generate report item for an AIPO
     *
     * @param aipoDetails To retrieve data
     * @return Report of the aipo
     */
    private List<AbstractTestReportModel> generateTestReports(List<SAPResultDetailItem> aipoDetails) {
        LOG.info("Executing generateTestReports(List<SAPResultDetailItem>)");

        List<AbstractTestReportModel> testReportModelList = new ArrayList<>();
        int i = 0;
        while (i < aipoDetails.size()) {
            SAPResultDetailItem test = aipoDetails.get(i);
            if (test.isMetaTest()) {
                testReportModelList.add(generateMetaTestReportItem(aipoDetails, i));
                i = i + test.getTestRolesList().size();
            } else {
                testReportModelList.add(generateTestReportItem(test));
            }
            i++;
        }
        return testReportModelList;
    }

    /**
     * Generate Test Report Item from a test
     *
     * @param test AIPO detail
     * @return report element
     */
    private TestReportItem generateTestReportItem(SAPResultDetailItem test) {
        LOG.info("Executing generateTestReportItem(SAPResultDetailItem)");

        List<TestRoles> testRoles = test.getTestRolesList();
        TestReportItem testReport = new TestReportItem(testRoles.get(0).getTest().getKeyword(), testRoles.get(0).getTest().getName(), testRoles.get(0).getTest().getTestType().getKeyword());
        List<TestInstance> testInstanceList = test.getInstancesByAllExecutionStatuses();
        testReport.setTestInstanceReports(reportDAO.generateTestInstanceItem(testInstanceList));
        return testReport;
    }

    /**
     * Generate MetaTestReport, fill it and return it
     *
     * @param aipoDetails List of AIPO details
     * @param i           location in the list of test
     * @return metaTestReport
     */
    private MetaTestReport generateMetaTestReportItem(List<SAPResultDetailItem> aipoDetails, int i) {
        LOG.info("Executing generateMetaTestReportItem(SAPResultDetailItem, int)");

        SAPResultDetailItem test = aipoDetails.get(i);
        MetaTestReport metaTestReport = new MetaTestReport();
        MetaTest metatest = test.getTestRolesList().get(0).getMetaTest();
        metaTestReport.setKeyword(metatest.getKeyword());
        metaTestReport.setName(metatest.getDescription());
        List<TestReportItem> testsOfMetaTestList = new ArrayList<>();

        for (int j = 1; j <= test.getTestRolesList().size(); j++) {
            SAPResultDetailItem testOfMeta = aipoDetails.get(i + j);
            List<TestRoles> testRoles = testOfMeta.getTestRolesList();
            TestReportItem testReport = new TestReportItem();
            testReport.setKeyword(testRoles.get(0).getTest().getKeyword());
            testReport.setName(testRoles.get(0).getTest().getName());
            List<TestInstance> testInstanceList = testOfMeta.getInstancesByAllExecutionStatuses();
            testReport.setTestInstanceReports(reportDAO.generateTestInstanceItem(testInstanceList));
            testsOfMetaTestList.add(testReport);
        }
        metaTestReport.setTestReportItems(testsOfMetaTestList);
        return metaTestReport;
    }

    public void setReportDAO(ConformityTestReportDAO reportDAO) {
        this.reportDAO = reportDAO;
    }
}
