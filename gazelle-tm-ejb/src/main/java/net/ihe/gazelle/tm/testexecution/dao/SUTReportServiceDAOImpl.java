package net.ihe.gazelle.tm.testexecution.dao;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.HQLRestriction;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import net.ihe.gazelle.tf.model.ActorIntegrationProfileOption;
import net.ihe.gazelle.tm.filter.TMCriterions;
import net.ihe.gazelle.tm.filter.modifier.SystemNotTool;
import net.ihe.gazelle.tm.filter.valueprovider.InstitutionFixer;
import net.ihe.gazelle.tm.gazelletest.model.definition.*;
import net.ihe.gazelle.tm.gazelletest.model.instance.*;
import net.ihe.gazelle.tm.session.TestingSessionService;
import net.ihe.gazelle.tm.systems.model.System;
import net.ihe.gazelle.tm.systems.model.*;
import net.ihe.gazelle.tm.testexecution.*;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import javax.persistence.EntityManager;
import java.util.*;

@Name(value = "sutReportServiceDAO")
@AutoCreate
@Scope(ScopeType.STATELESS)
public class SUTReportServiceDAOImpl implements SUTReportServiceDAO, QueryModifier<SystemAIPOResultForATestingSession> {

    private static final long serialVersionUID = -4977820759188246361L;

    @In
    private GazelleIdentity identity;

    private final transient TestingSessionService testingSessionService = (TestingSessionService) Component.getInstance("testingSessionService");

    @Override
    public List<TestReport> getTestReports(SUTCapabilityReport sutCapabilityReport) {

        SystemAIPOResultForATestingSessionQuery systemAIPOResultForATestingSessionQuery = new SystemAIPOResultForATestingSessionQuery();
        systemAIPOResultForATestingSessionQuery.id().eq(sutCapabilityReport.getSystemAIPOResultForATestingSessionId());
        SystemAIPOResultForATestingSession systemAIPOResult = systemAIPOResultForATestingSessionQuery.getUniqueResult();

        List<TestRoles> testRoles = getTestRoles(systemAIPOResult);
        List<TestReport> testReports = new ArrayList<>();

        Map<String, TestReport> metatests = new HashMap<>();

        for (TestRoles testRole : testRoles) {
            if (testRole.getMetaTest() == null) {
                TestReport testReport = constructTestReport(testRole, systemAIPOResult, false);
                testReports.add(testReport);
            } else {
                TestReport testReport = constructTestReport(testRole, systemAIPOResult, true);
                TestReport testReportWithMetaTest = metatests.get(testRole.getMetaTest().getKeyword());
                if (testReportWithMetaTest == null) {
                    testReportWithMetaTest = constructMetaTestReport(testRole);
                    metatests.put(testRole.getMetaTest().getKeyword(), testReportWithMetaTest);
                }
                testReportWithMetaTest.addAllTestRunReport(testReport.getTestRunReports());
                testReportWithMetaTest.addEquivalentTestReports(testReport);
                testReportWithMetaTest.getEquivalentTestReportPartners().add(testReport.getPartnerReports());
            }
        }
        if (!metatests.isEmpty()) {
            for (TestReport metatest : metatests.values()) {
                testReports.add(metatest);
                testReports.addAll(metatest.getEquivalentTestReports());
            }
        }
        return testReports;
    }

    @Override
    public Filter<SystemAIPOResultForATestingSession> getFilter(Map<String, String> requestParameterMap,
                                                                List<QueryModifier<SystemAIPOResultForATestingSession>> queryModifiers) {
        SystemAIPOResultForATestingSessionQuery query = new SystemAIPOResultForATestingSessionQuery();
        HQLCriterionsForFilter<SystemAIPOResultForATestingSession> criteria = query.getHQLCriterionsForFilter();

        TMCriterions.addTestingSession(criteria, "testing_session", query.testSession(), identity);
        TMCriterions.addAIPOCriterions(criteria, query.systemActorProfile().actorIntegrationProfileOption());
        criteria.addPath("institution", query.systemActorProfile().system().institutionSystems().institution(),
                null, InstitutionFixer.INSTANCE);
        criteria.addPath("system", query.systemActorProfile().system());
        criteria.addQueryModifier(new SystemNotTool(query.systemActorProfile().system()));
        criteria.addPath("statusResult", query.status());
        criteria.addPath("testingDepth", query.systemActorProfile().testingDepth());
        criteria.addPath("testType", query.systemActorProfile().aipo().testParticipants().roleInTest()
                .testRoles().test().testType());
        criteria.addPath("testPeerType", query.systemActorProfile().aipo().testParticipants().roleInTest()
                .testRoles().test().testPeerType());

        criteria.addQueryModifier(this);
        if (queryModifiers != null) {
            for (QueryModifier<SystemAIPOResultForATestingSession> queryModifier : queryModifiers) {
                criteria.addQueryModifier(queryModifier);
            }
        }
        return new Filter<>(criteria, requestParameterMap);
    }

    @Override
    public void modifyQuery(HQLQueryBuilder<SystemAIPOResultForATestingSession> hqlQueryBuilder, Map<String, Object> requestParameterMap) {
        SystemAIPOResultForATestingSessionQuery query = new SystemAIPOResultForATestingSessionQuery(hqlQueryBuilder);
        TestingSession testingSession = testingSessionService.getUserTestingSession();

        HQLRestriction enabledRestriction = HQLRestrictions.or(query.enabled().eqRestriction(true), query.enabled().isNullRestriction());
        hqlQueryBuilder.addRestriction(query.systemActorProfile().system().systemsInSession().acceptedToSession().eqRestriction(true));
        hqlQueryBuilder.addRestriction(query.systemActorProfile().system().systemsInSession().testingSession()
                .eqRestriction(testingSession));
        hqlQueryBuilder.addRestriction(query.systemActorProfile().aipo().testParticipants().roleInTest()
                .testRoles().test().testType()
                .inRestriction(testingSession.getTestTypes()));
        hqlQueryBuilder.addRestriction(enabledRestriction);

    }


    private TestRunReport constructTestRunReport(TestInstance testInstance) {
        TestRunReport testRunReport = new TestRunReport();
        testRunReport.setId(testInstance.getId());
        testRunReport.setContainsSameSystemsOrFromSameCompany(testInstance.isContainsSameSystemsOrFromSameCompany());
        if (testInstance.getMonitorInSession() != null && testInstance.getLastStatus() == Status.COMPLETED) {
            testRunReport.setStatus(TestRunStatus.CLAIMED);
        } else {
            testRunReport.setStatus(TestRunStatus.getStatusByKeyword(testInstance.getLastStatus().getKeyword()));
        }
        testRunReport.setRunningPartners(testInstance.getTestInstanceParticipants());
        return testRunReport;
    }

    private TestReport constructMetaTestReport(TestRoles testRolesWithMetaTest) {
        TestReport metaTestReport = new TestReport();
        metaTestReport.setMetatest(true);
        metaTestReport.setInMetatest(false);
        metaTestReport.setTestRunReports(new ArrayList<TestRunReport>());
        metaTestReport.setType(testRolesWithMetaTest.getTest().getTestType().getKeyword());
        metaTestReport.setSummary(testRolesWithMetaTest.getMetaTest().getDescription());
        metaTestReport.setTestCase(testRolesWithMetaTest.getMetaTest().getKeyword());
        metaTestReport.setTarget(testRolesWithMetaTest.getNumberOfTestsToRealize());
        metaTestReport.setDescriptionLink(testRolesWithMetaTest.getMetaTest().getId());
        metaTestReport.setOptionality(testRolesWithMetaTest.getTestOption().getLabelToDisplay());

        return metaTestReport;
    }

    private TestReport constructTestReport(TestRoles testRole, SystemAIPOResultForATestingSession capability, boolean inMetatest) {

        TestReport testReport = new TestReport();
        testReport.setMetatest(false);
        testReport.setTestCase(testRole.getTest().getKeyword());
        testReport.setTarget(testRole.getNumberOfTestsToRealize());
        testReport.setType(testRole.getTest().getTestType().getKeyword());
        testReport.setPeerType(testRole.getTest().getTestPeerType().getKeyword());
        testReport.setOptionality(testRole.getTestOption().getLabelToDisplay());
        testReport.setDescriptionLink(testRole.getTest().getId());
        testReport.setSummary(testRole.getTest().getShortDescription());
        testReport.setTestRolesId(testRole.getId());
        testReport.setInMetatest(inMetatest);

        SystemInSession systemInSession = SystemInSession.getSystemInSessionForSession(capability
                .getSystemActorProfile().getSystem(), capability.getTestSession());
        testReport.setSystemInSessionId(systemInSession.getId());
        List<TestRunReport> testRunReports = new ArrayList<>();

        List<TestInstance> testInstances = getTestRunsForTestForCapability(testRole, capability);
        for (TestInstance testInstance : testInstances) {
            TestRunReport currentTestReport = constructTestRunReport(testInstance);
            testRunReports.add(currentTestReport);
        }

        List<RoleInTest> roleInTests = getRoleInTestsForTestRoles(testRole);
        List<PartnerReport> partnerReports = new ArrayList<>();

        for (RoleInTest roleInTest : roleInTests) {
            List<ActorIntegrationProfileOption> aipoList = new ArrayList<>();
            for (TestParticipants testParticipants : roleInTest.getTestParticipantsList()) {
                if (testParticipants.getTested() != null && testParticipants.getTested()) {
                    aipoList.add(testParticipants.getActorIntegrationProfileOption());
                }
            }

            List<System> availableSystems = getAvailableSystems(capability, aipoList);
            List<String> testedSystems = getTestedSystems(testInstances, roleInTest);

            PartnerReport partnerReport = constructPartnerReport(roleInTest, availableSystems, testedSystems);
            partnerReports.add(partnerReport);
        }
        testReport.setPartnerReports(partnerReports);

        Collections.sort(testRunReports);
        testReport.setTestRunReports(testRunReports);

        return testReport;
    }

    private PartnerReport constructPartnerReport(RoleInTest roleInTest, List<System> availableSystems, List<String> testedSystems) {

        PartnerReport partnerReport = new PartnerReport();
        partnerReport.setRoleKeyword(roleInTest.getKeyword());
        partnerReport.setCountAvailable(availableSystems.size());
        partnerReport.setCountTested(testedSystems.size());

        List<PartnerSystemReport> partnerSystemReports = new ArrayList<>();

        for (System system : availableSystems) {
            PartnerSystemReport partnerSystemReport = constructPartnerSystemReport(testedSystems, system);
            partnerSystemReports.add(partnerSystemReport);
        }

        partnerReport.setPartnerSystemReports(partnerSystemReports);

        return partnerReport;
    }

    private PartnerSystemReport constructPartnerSystemReport(List<String> testedSystems, System system) {
        PartnerSystemReport partnerSystemReport = new PartnerSystemReport();
        partnerSystemReport.setSystemKeyword(system.getKeyword());
        SystemInSession systemInSession = getSystemInSessionBySystem(system);

        if (systemInSession == null || systemInSession.getTableSession() == null) {
            partnerSystemReport.setTable("");
        } else {
            partnerSystemReport.setTable(systemInSession.getTableSession().getTableKeyword());
        }
        partnerSystemReport.setTested(testedSystems.contains(system.getKeyword()));

        return partnerSystemReport;
    }

    private SystemInSession getSystemInSessionBySystem(System system) {

        SystemInSessionQuery systemInSessionQuery = new SystemInSessionQuery();
        systemInSessionQuery.system().id().eq(system.getId());
        systemInSessionQuery.system().keyword().eq(system.getKeyword());

        return systemInSessionQuery.getUniqueResult();
    }

    private List<String> getTestedSystems(List<TestInstance> testInstances, RoleInTest roleInTest) {

        TestInstanceParticipantsQuery testInstanceParticipantsQuery = new TestInstanceParticipantsQuery();
        testInstanceParticipantsQuery.testInstance().in(testInstances);
        testInstanceParticipantsQuery.roleInTest().keyword().eq(roleInTest.getKeyword());

        return testInstanceParticipantsQuery.systemInSessionUser().systemInSession().system().keyword().getListDistinct();
    }

    private List<System> getAvailableSystems(SystemAIPOResultForATestingSession capability, List<ActorIntegrationProfileOption> aipoList) {
        SystemAIPOResultForATestingSessionQuery query = new SystemAIPOResultForATestingSessionQuery();
        query.testSession().id().eq(capability.getTestSession().getId());
        query.systemActorProfile().actorIntegrationProfileOption().in(aipoList);
        query.systemActorProfile().system().systemsInSession().acceptedToSession().eq(true);

        return query.systemActorProfile().system().getListDistinct();
    }

    private List<RoleInTest> getRoleInTestsForTestRoles(TestRoles testRole) {
        RoleInTestQuery roleInTestQuery = new RoleInTestQuery();
        roleInTestQuery.testRoles().test().keyword().eq(testRole.getTest().getKeyword());
        roleInTestQuery.testRoles().id().neq(testRole.getId());
        roleInTestQuery.testRoles().test().testPeerType().neq(TestPeerType.getNO_PEER_TEST());

        return roleInTestQuery.getList();
    }

    private List<TestInstance> getTestRunsForTestForCapability(TestRoles testRole, SystemAIPOResultForATestingSession capability) {
        TestInstanceParticipantsQuery testInstanceParticipantsQuery = new TestInstanceParticipantsQuery();
        testInstanceParticipantsQuery.testInstance().lastStatus().neq(Status.ABORTED);
        testInstanceParticipantsQuery.testInstance().test().eq(testRole.getTest());
        testInstanceParticipantsQuery.systemInSessionUser().systemInSession().system().eq(capability.getSystemActorProfile().getSystem());
        testInstanceParticipantsQuery.testInstance().testingSession().eq(capability.getTestSession());
        testInstanceParticipantsQuery.roleInTest().testParticipantsList().actorIntegrationProfileOption().eq(capability.getSystemActorProfile().getActorIntegrationProfileOption());

        return testInstanceParticipantsQuery.testInstance().getListDistinct();
    }

    private List<TestRoles> getTestRoles(SystemAIPOResultForATestingSession systemAIPOResult) {
        TestRolesQuery trQuery = new TestRolesQuery();
        SystemActorProfiles systemActorProfile = systemAIPOResult.getSystemActorProfile();
        System system = systemActorProfile.getSystem();
        trQuery.roleInTest().testParticipantsList().tested().eq(Boolean.TRUE);
        trQuery.test().testStatus().keyword().eq("ready");
        trQuery.test().testType().in(testingSessionService.getUserTestingSession().getTestTypes());
        trQuery.roleInTest().addFetch();
        trQuery.roleInTest().testParticipantsList().addFetch();
        trQuery.roleInTest().testParticipantsList().actorIntegrationProfileOption().addFetch();
        trQuery.roleInTest().testParticipantsList().aipo().addFetch();

        trQuery.roleInTest().testParticipantsList().aipo().systemActorProfiles().system().eq(system);

        trQuery.roleInTest().testParticipantsList().actorIntegrationProfileOption()
                .eq(systemActorProfile.getActorIntegrationProfileOption());

        trQuery.testOption().labelToDisplay().order(false);
        return trQuery.getList();
    }

    @Override
    public void updateEvaluation(SystemAIPOResultForATestingSession aipoResult) {
        EntityManager em = EntityManagerService.provideEntityManager();
        em.merge(aipoResult);
        em.flush();
    }
}
