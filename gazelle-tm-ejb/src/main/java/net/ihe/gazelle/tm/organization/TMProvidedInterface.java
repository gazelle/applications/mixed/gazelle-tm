package net.ihe.gazelle.tm.organization;


import net.ihe.gazelle.metadata.application.InterfaceBuilder;
import net.ihe.gazelle.metadata.application.ProvidedInterfaceProvider;
import net.ihe.gazelle.metadata.domain.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TMProvidedInterface implements ProvidedInterfaceProvider {
    private final List<Interface> providedInterfaces;

    public TMProvidedInterface() {
        List<AuthzScope> scopes = initAuthzScopes();

        Interface channelInterface = new InterfaceBuilder()
                .setInterfaceName("channel")
                .setAuthzScopes(scopes)
                .setSecured(true)
                .setSecuredMethods(Collections.singletonList(SecuredMethod.M2M))
                .setBindings(Collections.<Binding>singletonList(
                        new RestBinding().setServiceUrl("/rest/organizations")
                )).build();
        providedInterfaces = Collections.singletonList(channelInterface);
    }

    private List<AuthzScope> initAuthzScopes() {
        List<AuthzScope> scopes = new ArrayList<>();
        AuthzScope myOrgaCreate = new AuthzScope();
        myOrgaCreate.setName("my-orga:create");
        myOrgaCreate.setDescription("Create an organization the user will became administrator of.");
        scopes.add(myOrgaCreate);

        AuthzScope myOrgaRead = new AuthzScope();
        myOrgaRead.setName("my-orga:read");
        myOrgaRead.setDescription("View his/her own organisation information.");
        scopes.add(myOrgaRead);

        AuthzScope myOrgaUpdate = new AuthzScope();
        myOrgaUpdate.setName("my-orga:update");
        myOrgaUpdate.setDescription("Edit his/her own organisation information.");
        scopes.add(myOrgaUpdate);

        AuthzScope anyOrgaCreate = new AuthzScope();
        anyOrgaCreate.setName("any-orga:create");
        anyOrgaCreate.setDescription("Create an organization for an other user or without users.");
        scopes.add(anyOrgaCreate);

        AuthzScope anyOrgaReadPublic = new AuthzScope();
        anyOrgaReadPublic.setName("any-orga:read-public");
        anyOrgaReadPublic.setDescription("View public information of any organization.");
        scopes.add(anyOrgaReadPublic);

        AuthzScope anyOrgaReadPrivate = new AuthzScope();
        anyOrgaReadPrivate.setName("any-orga:read-private");
        anyOrgaReadPrivate.setDescription("View all information of any organization.");
        scopes.add(anyOrgaReadPrivate);

        AuthzScope anyOrgaReadUpdate = new AuthzScope();
        anyOrgaReadUpdate.setName("any-orga:update");
        anyOrgaReadUpdate.setDescription("Edit his/her own organisation information.");
        scopes.add(anyOrgaReadUpdate);

        return scopes;
    }


    @Override
    public List<Interface> getProvidedInterfaces() {
        return providedInterfaces;
    }
}
