package net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.instance.dao;

import net.ihe.gazelle.tm.gazelletest.automatedstep.domain.instance.AutomatedTestStepInstanceData;

public interface AutomatedTestStepInstanceFileSystemDAO {

    /**
     * Save an input file for automated test step instance
     * @param testStepsDataId The test step data id
     * @param fileName The name of the file to save
     * @param fileContent The content of data file
     * @throws IllegalArgumentException if testStepsDataId or fileName are null
     * @throws IllegalStateException if error during file creation occurs
     * @throws WriteInputException if error occurs when writing content in created file
     */
    void saveInputDataFile(Integer testStepsDataId, String fileName, String fileContent);

    /**
     * Read an input data file
     * @param inputData The input data to be read
     * @return The content of the input data
     * @throws IllegalArgumentException if testStepsDataId or fileName in inputData are null
     * @throws ReadInputException if error during file reading occurs
     */
    String readInputDataFile(AutomatedTestStepInstanceData inputData);

    /**
     * Delete an input data
     * @param inputData The input data to be deleted
     * @throws IllegalArgumentException if testStepsDataId or fileName in inputData are null
     */
    void deleteInputDataFile(AutomatedTestStepInstanceData inputData);

    class ReadInputException extends RuntimeException {
        private static final long serialVersionUID = 1649530742956245444L;

        public ReadInputException() {
        }

        public ReadInputException(String message) {
            super(message);
        }

        public ReadInputException(String message, Throwable throwable) {
            super(message, throwable);
        }

        public ReadInputException(Throwable throwable) {
            super(throwable);
        }
    }

    class WriteInputException extends RuntimeException {
        private static final long serialVersionUID = 6486439554618060503L;

        public WriteInputException() {
        }

        public WriteInputException(String message) {
            super(message);
        }

        public WriteInputException(String message, Throwable throwable) {
            super(message, throwable);
        }

        public WriteInputException(Throwable throwable) {
            super(throwable);
        }
    }
}
