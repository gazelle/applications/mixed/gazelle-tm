package net.ihe.gazelle.tm.gazelletest.dao;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManagerImpl;
import net.ihe.gazelle.tm.gazelletest.action.TestInstanceNotFoundException;
import net.ihe.gazelle.tm.gazelletest.automatedstep.domain.instance.AutomatedTestStepInstanceData;
import net.ihe.gazelle.tm.gazelletest.model.instance.*;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

@Name("testInstanceDataDAO")
@Scope(ScopeType.EVENT)
@AutoCreate
public class TestInstanceDataDAOImpl implements TestInstanceDataDAO {

    private static final Logger LOG = LoggerFactory.getLogger(TestInstanceDataDAOImpl.class);
    private final ApplicationPreferenceManager applicationPreferenceManager = new ApplicationPreferenceManagerImpl();

    @In
    private EntityManager entityManager;

    @Deprecated
    public TestInstanceDataDAOImpl() {
        // Used Only for seam Injection
    }

    public TestInstanceDataDAOImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }


    /**
     * Save an information of data
     *
     * @param testStepsData Object
     * @return return a TestStepData to merge with TestInstance
     */
    public TestStepsData createTestStepData(TestStepsData testStepsData) {
        if(testStepsData==null){
            throw new IllegalArgumentException("parameter cannot be null");
        }

        testStepsData = entityManager.merge(testStepsData);
        entityManager.flush();
        return testStepsData;
    }

    @Override
    @Transactional
    public TestStepsData createAutomatedTestStepData(String fileName, Integer testStepsInstanceId) {
        TestStepsData testStepsData = new TestStepsData();
        testStepsData.setValue(fileName);
        testStepsData.setDataType(DataType.getDataTypeByKeyword(DataType.AUTOMATION_INPUT_DT));
        testStepsData.setComment("");
        testStepsData.setWithoutFileName(true);
        testStepsData = createTestStepData(testStepsData);
        addTestStepsDataToTestStepsInstance(testStepsData, testStepsInstanceId);
        return testStepsData;
    }

    @Transactional
    private void addTestStepsDataToTestStepsInstance(TestStepsData tsd, Integer testStepsInstanceId) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("addTestStepsDataToTestStepsInstance");
        }
        TestStepsInstanceQuery query = new TestStepsInstanceQuery();
        query.id().eq(testStepsInstanceId);
        TestStepsInstance cleanTestStepsInstance = query.getUniqueResult();
        cleanTestStepsInstance.addTestStepsData(tsd);
        entityManager.merge(cleanTestStepsInstance);
        entityManager.flush();
    }

    @Override
    @Transactional
    public void removeTestStepsDataFromTestStepsInstance(AutomatedTestStepInstanceData inputData) {
        TestStepsInstanceQuery query = new TestStepsInstanceQuery();
        query.id().eq(inputData.getTestStepInstanceId());
        TestStepsInstance cleanTestStepsInstance = query.getUniqueResult();
        TestStepsData tsd = entityManager.find(TestStepsData.class, inputData.getTestStepDataId());
        cleanTestStepsInstance.removeTestStepsDataList(tsd);
        entityManager.merge(cleanTestStepsInstance);
        entityManager.flush();
        entityManager.remove(tsd);
        entityManager.flush();
    }

    /**
     * Get TestStepData By ID
     *
     * @param testStepsDataId
     * @return
     */
    public TestStepsData getTestStepDataById(Integer testStepsDataId) {
        TestStepsDataQuery testStepsDataQuery = new TestStepsDataQuery(entityManager);
        testStepsDataQuery.id().eq(testStepsDataId);
        return testStepsDataQuery.getUniqueResult();
    }


    /**
     * Create File in test steps data
     *
     * @param testStepsDataId Integer
     * @param bytesInput      byte[]
     * @throws IOException
     */
    public void saveFileInTestStepsData(Integer testStepsDataId, byte[] bytesInput) throws IOException {
        if (testStepsDataId == null) {
            throw new IllegalArgumentException("TestStepsData id must be defined.");
        }

        String pathToSaveFile = getPath(testStepsDataId.toString());
        File fileToStore = new File(pathToSaveFile);
        fileToStore.getParentFile().mkdirs();
        ByteArrayInputStream fis = new ByteArrayInputStream(bytesInput);
        if (fileToStore.getParentFile().exists()) {
            FileOutputStream fos;
            if (fileToStore.exists()) {

                if (fileToStore.delete()) {
                    LOG.info("Old version of upload file is deleted");
                } else {
                    LOG.error("Failed to delete fileToStore");
                }
            }
            fos = new FileOutputStream(fileToStore);
            IOUtils.copy(fis, fos);
            fos.close();
            LOG.info("New version of file is saved");
        }

    }


    /**
     * Update TestInstance and TestStepData
     *
     * @param testStepData Object
     * @param instanceId   Integer
     * @throws TestInstanceNotFoundException
     */
    public void updateTestInstanceAndAddTestStepData(TestStepsData testStepData, Integer instanceId, String userId) throws TestInstanceNotFoundException {
        if(testStepData==null || instanceId == null || StringUtils.isBlank(userId)){
            throw new IllegalArgumentException("parameters cannot be null");
        }
        TestInstance testInstance = getTestInstance(instanceId);
        testInstance.removeTestStepsDataList(testStepData);
        testInstance.addTestStepsData(testStepData);
        testInstance.setLastModifierId(userId);
        testInstance.addComment("Test-report file has been upload with fileName " + testStepData.getValue() + " by user : " + userId);
        testInstance.setLastStatus(Status.COMPLETED);
        updateTestInstance(testInstance);
    }


    public void updateTestInstanceAndTestStepDataForLogs(TestStepsData testStepData, Integer instanceId, String userId) throws TestInstanceNotFoundException {
        if(testStepData==null || instanceId == null || StringUtils.isBlank(userId)){
            throw new IllegalArgumentException("parameters cannot be null");
        }
        TestInstance testInstance = getTestInstance(instanceId);
        testInstance.removeTestStepsDataList(testStepData);
        testInstance.addTestStepsData(testStepData);
        testInstance.setLastModifierId(userId);
        testInstance.addComment("Test-logs file has been upload with fileName" + testStepData.getValue() + " by user : " + userId);
        updateTestInstance(testInstance);
    }

    /**
     * Instantiate TestStepData file : Check TestStepData already exist for the token, if there is one it will return the existing, else it will
     * return existing TestStep
     *
     * @param userId String
     * @return TestStepsData
     */
    public TestStepsData instantiateTestStepDataFile(String userId, String fileName) {
        if(StringUtils.isBlank(fileName)|| StringUtils.isBlank(userId)){
            throw new IllegalArgumentException("parameters cannot be null");
        }
        TestStepsData testStepsData = getExistingTestStepDataByUserAndFileName(userId, fileName);

        if (testStepsData == null) {
            testStepsData = new TestStepsData();
            testStepsData.setLastModifierId(userId);
            testStepsData.setLastChanged(new Date());
            testStepsData.setValue(fileName);
            testStepsData.setDataType(DataType.getDataTypeByKeyword(DataType.FILE_DT));
            testStepsData.setComment("");
            testStepsData.setWithoutFileName(true);
        } else {
            testStepsData.setLastModifierId(userId);
            testStepsData.setLastChanged(new Date());
        }
        return testStepsData;
    }

    public TestStepsData getExistingTestStepDataByUserAndFileName(String userId, String fileName) {
        if(StringUtils.isBlank(fileName)|| StringUtils.isBlank(userId)){
            throw new IllegalArgumentException("parameters cannot be null");
        }
        TestStepsDataQuery testStepsDataQuery = new TestStepsDataQuery(entityManager);
        testStepsDataQuery.value().eq(fileName);
        testStepsDataQuery.lastModifierId().eq(userId);
        return testStepsDataQuery.getUniqueResult();
    }

    /**
     * Get Path
     *
     * @param testStepsDataId String
     * @return String Object
     */
    public String getPath(String testStepsDataId) {
        if (StringUtils.isBlank(testStepsDataId)) {
            throw new IllegalArgumentException("TestStepsData id must be defined, cannot create path to store the TestStepsData file.");
        } else {

            String dataPath = applicationPreferenceManager.getGazelleDataPath();
            String fileStepsPath = applicationPreferenceManager.getStringValue("file_steps_path");

            String filePathForThisFileInstance = dataPath + java.io.File.separatorChar + fileStepsPath
                    + java.io.File.separatorChar + testStepsDataId;
            return filePathForThisFileInstance;
        }
    }


    /**
     * Update Test Instance
     *
     * @param testInstance Object
     * @return Test instance
     */
    public TestInstance updateTestInstance(TestInstance testInstance) {
        if(testInstance == null ){
            throw new IllegalArgumentException("parameters cannot be null");
        }
        testInstance = entityManager.merge(testInstance);
        entityManager.flush();
        return testInstance;
    }

    /**
     * Fet The TestInstance with an id
     *
     * @param testInstanceId Object
     * @return TestInstance
     * @throws TestInstanceNotFoundException
     */
    public TestInstance getTestInstance(Integer testInstanceId) throws TestInstanceNotFoundException {
        if (testInstanceId == null) {
            throw new TestInstanceNotFoundException("Invalid testInstanceId (null) ");
        }
        TestInstance testInstance = entityManager.find(TestInstance.class, testInstanceId);
        if (testInstance != null) {
            return testInstance;
        } else {
            throw new TestInstanceNotFoundException("No test instance has been found for id : " + testInstanceId);
        }
    }
}
