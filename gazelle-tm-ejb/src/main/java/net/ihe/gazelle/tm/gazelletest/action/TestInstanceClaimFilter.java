package net.ihe.gazelle.tm.gazelletest.action;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TestInstanceClaimFilter implements Filter {

    private static final String REQUEST_PARAM_KEY = "javax.faces.source";
    private static final String REGEX_VALUE = "globalform:completedTestInstanceDataTable:(\\d+):claimTestSupport";
    private static final String HEADER_NAME = "testInstanceId";

    @Override
    public void init(FilterConfig config) {
        // Initialize global variables if necessary.
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        if (httpServletRequest != null
                && httpServletRequest.getParameterMap() != null
                && httpServletRequest.getParameterMap().get(REQUEST_PARAM_KEY) instanceof String[]
                && ((String[]) httpServletRequest.getParameterMap().get(REQUEST_PARAM_KEY)).length > 0) {
            String source = ((String[]) httpServletRequest.getParameterMap().get(REQUEST_PARAM_KEY))[0];
            if (source != null && source.matches(REGEX_VALUE)) {
                String testId = extractNumber(source);
                ServletContext context = httpServletRequest.getSession().getServletContext();
                if (context != null && context.getAttribute(testId) != null) {
                    context.removeAttribute(testId);
                    httpServletResponse.setHeader(HEADER_NAME, testId);
                    httpServletResponse.sendError(404);
                }
            }
        }
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {
        // Cleanup global variables if necessary.
    }

    public static String extractNumber(String input) {
        Pattern pattern = Pattern.compile(":(\\d+):");
        Matcher matcher = pattern.matcher(input);
        if (matcher.find()) {
            return matcher.group(1);
        }
        return null;
    }

}