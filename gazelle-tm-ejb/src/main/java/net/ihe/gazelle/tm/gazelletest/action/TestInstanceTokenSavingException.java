package net.ihe.gazelle.tm.gazelletest.action;

import java.io.Serializable;

public class TestInstanceTokenSavingException extends Exception implements Serializable {
    public TestInstanceTokenSavingException() {
    }

    public TestInstanceTokenSavingException(String s) {
        super(s);
    }

    public TestInstanceTokenSavingException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public TestInstanceTokenSavingException(Throwable throwable) {
        super(throwable);
}
}

