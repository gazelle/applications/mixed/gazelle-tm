package net.ihe.gazelle.tm.testexecution;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManagerImpl;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstanceParticipants;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class TestRunReport implements Comparable<TestRunReport> {
    private Integer id;
    private TestRunStatus status;
    private boolean containsSameSystemsOrFromSameCompany;
    private List<String> runningPartners;
    private final ApplicationPreferenceManager applicationPreferenceManager = new ApplicationPreferenceManagerImpl();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TestRunStatus getStatus() {
        return status;
    }

    public void setStatus(TestRunStatus status) {
        this.status = status;
    }

    public String getLocation() {
        return applicationPreferenceManager.getApplicationUrl() + "testInstance.seam?id=" + getId();
    }

    public boolean isContainsSameSystemsOrFromSameCompany() {
        return containsSameSystemsOrFromSameCompany;
    }

    public void setContainsSameSystemsOrFromSameCompany(boolean containsSameSystemsOrFromSameCompany) {
        this.containsSameSystemsOrFromSameCompany = containsSameSystemsOrFromSameCompany;
    }

    public List<String> getRunningPartners() {
        return runningPartners;
    }

    public void setRunningPartners(List<TestInstanceParticipants> runningPartners) {
        List<String> runningPartnersResults =new ArrayList<>();
        for (TestInstanceParticipants testInstanceParticipants: runningPartners) {
            runningPartnersResults.add(testInstanceParticipants.getSystemInSessionUser().getSystemInSession().getSystem().getKeyword());
        }
        this.runningPartners=runningPartnersResults;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TestRunReport that = (TestRunReport) o;
        return Objects.equals(id, that.id) && status == that.status;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, status);
    }


    @Override
    public int compareTo(TestRunReport o) {
        if (o.getStatus() != null) {
            return this.getStatus().getRank() - o.getStatus().getRank();
        } else {
            return 1;
        }
    }
}
