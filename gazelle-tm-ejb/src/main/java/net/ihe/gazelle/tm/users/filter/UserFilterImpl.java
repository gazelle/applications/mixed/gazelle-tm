package net.ihe.gazelle.tm.users.filter;

import net.ihe.gazelle.common.filter.NoHQLFilter;
import net.ihe.gazelle.common.filter.NoHQLQueryBuilder;
import net.ihe.gazelle.common.filter.UrlFilter;
import net.ihe.gazelle.common.filter.list.NoHQLDataModel;
import net.ihe.gazelle.common.filter.util.MapNotifier;
import net.ihe.gazelle.common.filter.util.MapNotifierListener;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import net.ihe.gazelle.ssov7.gum.client.application.Role;
import net.ihe.gazelle.ssov7.gum.client.application.User;
import net.ihe.gazelle.users.model.Institution;
import org.jboss.seam.core.ResourceBundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.model.SelectItem;
import java.util.*;

public class UserFilterImpl implements MapNotifierListener, NoHQLFilter<User> {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserFilterImpl.class);
    public static final String SEARCH_FILTER = "search";
    public static final String INSTITUTION_FILTER = "organizationId";
    public static final String ROLE_FILTER = "roles";
    public static final String ACTIVATED_FILTER = "activated";
    private final UrlFilter urlFilter;
    private final List<String> filterIds;
    private final Map<String, List<SelectItem>> possibleValues;
    private final Map<String, Boolean> display;
    private final GazelleIdentity identity;
    private NoHQLDataModel<User> dataModel;
    private MapNotifier<String, String> filterValues;

    public UserFilterImpl(Map<String, String> requestParameterMap,
                          GazelleIdentity identity) {
        this.identity = identity;
        possibleValues = new WeakHashMap<>();
        display = new HashMap<>();

        filterIds = new ArrayList<>(Arrays.asList(SEARCH_FILTER, INSTITUTION_FILTER, ROLE_FILTER, ACTIVATED_FILTER));

        urlFilter = new UrlFilter();
        if (requestParameterMap != null && !requestParameterMap.isEmpty()) {
            filterValues = urlFilter.initFromUrl(requestParameterMap);
        } else {
            initFilters();
        }
        filterValues.addListener(this);
    }

    public void addListener(MapNotifierListener listener) {
        filterValues.addListener(listener);
    }

    private static String getOrganizationName(String organizationId) {
        Institution institution = Institution.findInstitutionWithKeyword(organizationId);
        return institution != null ? institution.getName() : organizationId;
    }

    public void initFilters() {
        filterValues = new MapNotifier<>(new HashMap<String, String>());
        for (String filterId : filterIds) {
            filterValues.put(filterId, null);
        }
    }

    @Override
    public String format(String selectedValue) {
        return selectedValue;
    }

    @Override
    public NoHQLDataModel<User> getDataModel() {
        return dataModel;
    }

    @Override
    public void setDataModel(NoHQLDataModel<User> dataModel) {
        this.dataModel = dataModel;
    }

    @Override
    public void resetField(String fieldId) {
        filterValues.put(fieldId, null);
    }

    @Override
    public void clear() {
        filterValues.clear();
    }

    @Override
    public Map<String, String> getFilterValues() {
        return filterValues;
    }

    @Override
    public List<SelectItem> getPossibleValues(String filterId) {
        if (!possibleValues.containsKey(filterId)) {
            List<SelectItem> selectItems = new ArrayList<>();
            final String showAll = ResourceBundle.instance().getString("gazelle.common.button.ShowAll");
            selectItems.add(new SelectItem(null, showAll));
            switch (filterId) {
                case INSTITUTION_FILTER:
                    NoHQLQueryBuilder<User> orgQueryBuilder = dataModel.newQueryBuilder();
                    appendFilters(orgQueryBuilder, INSTITUTION_FILTER);
                    Map<String, Long> resultOrg = orgQueryBuilder.getPossibleCountValues(INSTITUTION_FILTER);
                    for (Map.Entry<String, Long> entry : resultOrg.entrySet()) {
                        selectItems.add(new SelectItem(entry.getKey(), getOrganizationName(entry.getKey()) + " ("
                                + entry.getValue() + ")"));
                    }
                    break;
                case ACTIVATED_FILTER:
                    NoHQLQueryBuilder<User> activatedQueryBuilder = dataModel.newQueryBuilder();
                    appendFilters(activatedQueryBuilder, ACTIVATED_FILTER);
                    Map<String, Long> resultActivated = activatedQueryBuilder.getPossibleCountValues(ACTIVATED_FILTER);
                    for (Map.Entry<String, Long> entry : resultActivated.entrySet()) {
                        selectItems.add(new SelectItem(entry.getKey(), (entry.getKey()) + " ("
                                + entry.getValue() + ")"));
                    }
                    break;
                case ROLE_FILTER:
                    NoHQLQueryBuilder<User> rolesQueryBuilder = dataModel.newQueryBuilder();
                    appendFilters(rolesQueryBuilder, ROLE_FILTER);
                    Map<String, Long> resultRoles = rolesQueryBuilder.getPossibleCountValues(ROLE_FILTER);
                    for (Map.Entry<String, Long> entry : resultRoles.entrySet()) {
                        selectItems.add(new SelectItem(entry.getKey(), (entry.getKey()) + " ("
                                + entry.getValue() + ")"));
                    }
                    break;
                default:
            }
            sortSelectItems(selectItems, showAll);
            possibleValues.put(filterId, selectItems);
        }
        return possibleValues.get(filterId);
    }

    private void sortSelectItems(List<SelectItem> selectItems, final String showAll) {
        Collections.sort(selectItems, new Comparator<SelectItem>() {
            @Override
            public int compare(SelectItem s1, SelectItem s2) {
                // The showAll item needs to stay in first position in the select one menu
                if (s1.getLabel().equals(showAll)) {
                    return -1;
                }
                if (s2.getLabel().equals(showAll)) {
                    return 1;
                }
                return s1.getLabel().trim().compareToIgnoreCase(s2.getLabel().trim());
            }
        });
    }

    @Override
    public void setDisplay(String filterId) {
        display.put(filterId, true);
    }

    @Override
    public boolean isDisplay(String filterId) {
        return display.get(filterId) != null && display.get(filterId);
    }

    @Override
    public String getUrlParameters() {
        return urlFilter.getUrlParameters(filterValues);
    }

    @Override
    public void setUrlParameters(String urlParameters) {
        // Not implemented.
    }

    @Override
    public void appendFilters(NoHQLQueryBuilder<User> queryBuilder, String excludeCriterion) {
        if (!SEARCH_FILTER.equals(excludeCriterion)) {
            queryBuilder.addEq(SEARCH_FILTER, filterValues.get(SEARCH_FILTER));
        }
        if (!ROLE_FILTER.equals(excludeCriterion)) {
            queryBuilder.addEq(ROLE_FILTER, filterValues.get(ROLE_FILTER));
        }
        if (!ACTIVATED_FILTER.equals(excludeCriterion)) {
            queryBuilder.addEq(ACTIVATED_FILTER, filterValues.get(ACTIVATED_FILTER));
        }
        if (!INSTITUTION_FILTER.equals(excludeCriterion)) {
            if (!identity.hasRole(Role.ADMIN) && !identity.hasRole(Role.PROJECT_MANAGER)) {
                queryBuilder.addEq(INSTITUTION_FILTER, identity.getOrganisationKeyword());
            } else {
                queryBuilder.addEq(INSTITUTION_FILTER, filterValues.get(INSTITUTION_FILTER));
            }
        }
    }

    @Override
    public void appendFilters(NoHQLQueryBuilder<User> queryBuilder) {
        appendFilters(queryBuilder, null);
    }

    @Override
    public void modified() {
        possibleValues.clear();
        if (dataModel != null) {
            dataModel.resetCache();
        }

        // If one value of the map is empty, remove it from the map
        List<String> keysToRemove = new ArrayList<>();
        for (Map.Entry<String, String> entry : filterValues.entrySet()) {
            if (entry.getValue() == null || entry.getValue().isEmpty()) {
                keysToRemove.add(entry.getKey());
            }
        }
        for (String key : keysToRemove) {
            filterValues.remove(key);
        }
    }
}
