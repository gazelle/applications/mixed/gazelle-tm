package net.ihe.gazelle.tm.financial.action;

import net.ihe.gazelle.tm.financial.model.Invoice;
import net.ihe.gazelle.tm.systems.model.SystemInSession;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import net.ihe.gazelle.tm.users.model.ConnectathonParticipant;
import net.ihe.gazelle.users.model.Iso3166CountryCode;
import net.ihe.gazelle.util.Pair;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author abderrazek boufahja
 */
public class FinancialCalcWithVatStatusRecalculation extends FinancialCalc {

    /**
     * Calculate VAT amount (depending on billing country-address)
     * <p/>
     * We have 4 cases for the computation of the VAT, checking with the Billing Country (in billing address) : 1. You are from Belgium and then it
     * is always 21% VAT 2. You are from a country not in
     * the European Community. You will NOT have to pay the 21 % of Belgium VAT 3. You are from a country within the European Community AND you
     * have a valid VAT number (see EU Taxation and Customs
     * Union for more information) then you will not have to pay the Belgium VAT. 4. You are from a country within the European Community BUT you
     * do NOT have a valid VAT number. Then you will be
     * charged for the Belgium VAT amount (21%). 5. None VAT number is entered, then VAT amount reaches 21%
     *
     * @param invoice : Invoice to update
     * @return Invoice : invoice with new VAT amount
     */
    public Invoice calculateVatAmountForCompany(Invoice invoice, EntityManager em) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Invoice calculateVatAmountForCompany");
        }
        if ((invoice == null) || (invoice.getInstitution() == null) || (invoice.getInstitution().getId() == null)) {
            return invoice;
        }
        List<Iso3166CountryCode> liso = new ArrayList<Iso3166CountryCode>();
        if (invoice.getTestingSession() != null) {
            liso = invoice.getTestingSession().getListVATCountry(em);
        }
        Pair<Boolean, BigDecimal> resvat = calculateVatAmount(invoice.getTestingSession(), invoice.getFeesAmount(), invoice
                        .getFeesDiscount(), invoice.getVatNumber(), liso,
                invoice.getVatCountry());
        if (resvat != null) {
            if (resvat.getObject2() != null) {
                invoice.setVatAmount(resvat.getObject2());
            } else {
                invoice.setVatAmount(BigDecimal.ZERO);
            }
            invoice.setVatDue(resvat.getObject1());
        }
        return invoice;
    }

    /**
     * Calculate VAT amount (depending on billing country-address)
     * <p/>
     * We have 4 cases for the computation of the VAT, checking with the Billing Country (in billing address) : 1. You are from Belgium and then it
     * is always 21% VAT 2. You are from a country not in
     * the European Community. You will NOT have to pay the 21 % of Belgium VAT 3. You are from a country within the European Community AND you
     * have a valid VAT number (see EU Taxation and Customs
     * Union for more information) then you will not have to pay the Belgium VAT. 4. You are from a country within the European Community BUT you
     * do NOT have a valid VAT number. Then you will be
     * charged for the Belgium VAT amount (21%). 5. None VAT number is entered, then VAT amount reaches 21%
     *
     * @param invoice : Invoice to update
     * @return Invoice : invoice with new VAT amount
     */
    public Pair<Boolean, BigDecimal> calculateVatAmount(TestingSession ts, BigDecimal feesAmount, BigDecimal feesDiscount, String vatNumber,
                                                               List<Iso3166CountryCode> liso,
                                                               Iso3166CountryCode iso) {
        Boolean vatDue = null;
        BigDecimal vatAmount = null;
        if (iso != null) {
            // Case 1
            if (liso.contains(iso)) {
                BigDecimal feesWithDiscount = feesAmount.add(feesDiscount.negate());
                vatAmount = feesWithDiscount.multiply(ts.getVatPercent()).setScale(2);
                vatDue = true;
            }
            // Case 2
            else if ((iso.getEc() == null) || (!iso.getEc())) {
                vatAmount = BigDecimal.ZERO;
                vatDue = false;

            }
            // Case 3 and 4
            else {
                // TODO Important Note : For now we don't check the VAT number validity !!! so case 3 and 4 are the same.
                if ((vatNumber != null) && (!vatNumber.equals(""))) {
                    vatAmount = BigDecimal.ZERO;
                    vatDue = false;
                }
                // Case 5
                else {
                    BigDecimal feesWithoutDiscount = feesAmount.add(feesDiscount.negate());
                    vatAmount = feesWithoutDiscount.multiply(ts.getVatPercent()).setScale(2);
                    vatDue = true;
                }
            }
        }
        if ((vatDue != null) && (vatAmount != null)) {
            return new Pair<Boolean, BigDecimal>(vatDue, vatAmount);
        } else {
            return new Pair<Boolean, BigDecimal>(false, BigDecimal.ZERO);
        }
    }

    public Invoice recalculateFees(Invoice invoice, EntityManager em) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Invoice recalculateFees");
        }
        if ((invoice != null) && (invoice.getId() != null)) {
            invoice.setFeesAmount(calculateTotalFee(invoice.getTestingSession(), invoice.getNumberSystem(), invoice
                    .getNumberParticipant()));
            invoice.setNumberExtraParticipant(invoice.calculateNumberExtraParticipant());
            invoice = calculateVatAmountForCompany(invoice, em);
            invoice = Invoice.mergeInvoice(invoice, em);
        }
        return invoice;
    }

    public Invoice reloadInvoice(Invoice invoice, EntityManager entityManager) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Invoice reloadInvoice");
        }
        if ((invoice != null) && (invoice.getId() != null)) {
            List<SystemInSession> lsis = SystemInSession.getSystemsInSessionForCompanyForSession(entityManager, invoice.getInstitution(), invoice
                    .getTestingSession());
            List<ConnectathonParticipant> lcp = ConnectathonParticipant.filterConnectathonParticipant(entityManager, invoice.getInstitution(),
                    invoice.getTestingSession());
            invoice.setNumberSystem(lsis.size());
            invoice.setNumberParticipant(lcp.size());
            invoice.setFeesAmount(calculateTotalFee(invoice.getTestingSession(), lsis.size(), lcp.size()));
            invoice.setNumberExtraParticipant(invoice.calculateNumberExtraParticipant());
            invoice = calculateVatAmountForCompany(invoice, entityManager);
            invoice = Invoice.mergeInvoice(invoice, entityManager);
        }
        return invoice;
    }

}
