package net.ihe.gazelle.tm.gazelletest.action.conformitytestreport.model;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * The aim of this class is to store required information in order to produce testReport
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class DataOfTFObject {

    @XmlElement(name = "keyword")
    private String keyword;

    @XmlElement(name = "name")
    private String name;

    @XmlElement(name = "testType")
    private String testType;

    /**
     * Getter for Name Value
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Setter for Name value
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Getter for Keyword value
     *
     * @return keyword value
     */
    public String getKeyword() {
        return keyword;
    }

    /**
     * Setter for Keyword value
     *
     * @param keyword
     */
    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getTestType() {
        return testType;
    }

    public void setTestType(String testType) {
        this.testType = testType;
    }

    public DataOfTFObject(String keyword, String name, String testType) {
        this.keyword = keyword;
        this.name = name;
        this.testType = testType;
    }

    /**
     * Constructor
     *
     * @param keyword
     * @param name
     */
    public DataOfTFObject(String keyword, String name) {
        this.keyword = keyword;
        this.name = name;
    }

    public DataOfTFObject() {
    }

}

