package net.ihe.gazelle.tm.session.bean;

import net.ihe.gazelle.menu.GazelleMenu;
import net.ihe.gazelle.tm.session.TestingSessionService;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import javax.faces.context.FacesContext;

@Name("testSessionBean")
@Scope(ScopeType.EVENT)
@AutoCreate
public class TestSessionBean {

    @In(value = "testingSessionService", create = true)
    TestingSessionService testingSessionService;

    public TestingSession getSelectedTestingSession() {
        return testingSessionService.getUserTestingSession();
    }

    public void setSelectedTestingSession(TestingSession selectedTestingSession) {

        testingSessionService.setCurrentTestingSession(selectedTestingSession);
        //need to refresh the Menu once testing Session change is successful
        GazelleMenu.getInstance().refreshMenu();

        // Invalidate bean to refresh the  aipo list when session is changed.
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("aipoSelector", null);
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("reportManagerBean", null);
    }


}
