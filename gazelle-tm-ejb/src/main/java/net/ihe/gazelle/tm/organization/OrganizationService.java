package net.ihe.gazelle.tm.organization;

import net.ihe.gazelle.ssov7.gum.client.interlay.ws.OrganizationResource;
import net.ihe.gazelle.users.model.DelegatedOrganization;
import net.ihe.gazelle.users.model.Institution;

import java.util.List;
import java.util.NoSuchElementException;

public interface OrganizationService {

    /**
     * <b>Should Check the following:</b>
     * <ul>
     *  <li>Organization has billing contact</li>
     *  <li>Organization has at least one of each:
     *      <ul>
     *          <li>Marketing Contact</li>
     *          <li>Financial Contact</li>
     *          <li>Technical Contact</li>
     *      </ul>
     *  </li>
     * </ul>
     * @param institution id of the organisation
     * @return boolean
     */
    boolean isOrganizationRegistrationCompleted(Institution institution);

    /**
     * Check if organization is delegated.
     * @param institution the Organization to check.
     * @return true if the organization is delegated, false otherwise.
     */
    boolean isOrganizationDelegated(Institution institution);

    /**
     * Temporary method for GUM step 2 to retrieve organizations by search in TM
     * @return List(Institution)
     */
    List<Institution> searchForOrganizations(String organizationName, String externalId, String idpId, Boolean delegated);

    /**
     * Get the current organization
     * @return the current organization
     */
    Institution getCurrentOrganization();

    /**
     * Get the organization with the given id (keyword)
     * @param organizationId the id of the organization
     * @return the organization
     */
    Institution getOrganizationById(String organizationId);

    /**
     * Get the organization with the given name
     * @param organizationName the name of the organization
     * @return the organization or null if not found
     */
    Institution findOrganizationByName(String organizationName);

    /**
     * get a delegated organization by its keyword
     * @param keyword the keyword of the organization
     * @return the delegated organization
     */
    DelegatedOrganization getDelegatedOrganizationByKeyword(String keyword);

    /**
     * Get an OrganizationResource which take in account le delegated attributes or not
     * @param institution the institution to transform in resource
     * @return the organization resource
     */
    OrganizationResource getOrganizationresource(Institution institution);

    /**
     * Temporary method for GUM step 2 to create an organization in TM from GUM
     * @param organization the Organization to create
     */
    void createOrganization(OrganizationResource organization);

    /**
     * Update the name of the organization
     *
     * @param keyword     the keyword of the institution to patch.
     * @param institution the institution attributes to update.
     * @return the updated organization
     * @throws IllegalArgumentException if organizationId or name is null
     * @throws NoSuchElementException   if the organization does not exist
     */
    Institution updateOrganization(String keyword, Institution institution);

    /**
     * Is there at least one organization boolean.
     *
     * @return true if one or more Organization exist, false otherwise
     */
    boolean isThereAtLeastOneOrganization();
}
