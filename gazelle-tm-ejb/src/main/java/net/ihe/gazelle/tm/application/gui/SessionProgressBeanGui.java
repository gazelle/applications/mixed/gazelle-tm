package net.ihe.gazelle.tm.application.gui;


import net.ihe.gazelle.ssov7.gum.client.application.Role;
import net.ihe.gazelle.tm.application.action.ApplicationManager;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import net.ihe.gazelle.tm.organization.OrganizationService;
import net.ihe.gazelle.tm.session.TestingSessionService;
import net.ihe.gazelle.tm.systems.SystemUnderTestService;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import net.ihe.gazelle.users.model.Institution;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.intercept.PostActivate;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.LocaleSelector;
import org.jboss.seam.international.StatusMessage;

import java.io.Serializable;
import java.text.SimpleDateFormat;

@Name("sessionProgressBeanGui")
@Scope(ScopeType.PAGE)
public class SessionProgressBeanGui implements Serializable {

    private static final long serialVersionUID = 8944205384609418398L;

    @In(create = true)
    private ApplicationManager applicationManager;

    public static final String DATE_PATTERN = "d MMMMM yyyy";

    private static final String UNDEFINED = "UNDEFINED";

    @In(value = "testingSessionService", create = true)
    private transient TestingSessionService testingSessionService;

    @In(value = "systemUnderTestService", create = true)
    private transient SystemUnderTestService systemUnderTestService;

    @In(value = "organizationService", create = true)
    private transient OrganizationService organizationService;

    @In
    private GazelleIdentity identity;

    private TestingSession currentTestingSession;

    private boolean isThereAcceptedSystem;

    private boolean isRegistrationOpen;

    private boolean isTestingSessionRunning;

    private boolean isTestingSessionBeforeStart;

    private boolean isTestingSessionAfterEnd;

    private boolean isContinuousSession;

    private enum ProgressStyleClass {
        IN_PROGRESS("gzl-progress-in-progress"),
        COMPLETED("gzl-progress-completed"),
        NOT_COMPLETED("gzl-progress-not-completed"),
        DEFAULT("gzl-progress-default");

        private final String value;

        ProgressStyleClass(String value) {
            this.value = value;
        }
    }

    /////////////////// init ///////////////////
    @Create
    public void init() {
        try {
            this.currentTestingSession = testingSessionService.getUserTestingSession();
            Institution currentInstitution = organizationService.getCurrentOrganization();
            if (currentTestingSession != null && currentInstitution != null) {
                this.isThereAcceptedSystem = systemUnderTestService
                        .isThereAcceptedSystem(currentTestingSession, currentInstitution);
                this.isRegistrationOpen = testingSessionService.isRegistrationOpen(currentTestingSession);
                this.isTestingSessionRunning = testingSessionService.isTestingSessionRunning(currentTestingSession);
                this.isTestingSessionBeforeStart = testingSessionService.isBeforeSessionStart(currentTestingSession);
                this.isTestingSessionAfterEnd = testingSessionService.isAfterSessionEnd(currentTestingSession);
                this.isContinuousSession = testingSessionService.isContinuousSession(currentTestingSession);
            } else {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "No selected session or organisation");
            }
        } catch (IllegalArgumentException e) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "An error has occurred during initialization");
        }
    }

    @PostActivate
    void postActivate() {
        // Reload non-serializable fields in case of bean passivation.
        testingSessionService = (TestingSessionService) Component.getInstance("testingSessionService");
        systemUnderTestService = (SystemUnderTestService) Component.getInstance("systemUnderTestService");
        organizationService = (OrganizationService) Component.getInstance("organizationService");
    }

    /////////////////// Data Retrievers ///////////////////

    public String getRegistrationStyleClass() {
        if (isRegistrationInProgress()) {
            return ProgressStyleClass.IN_PROGRESS.value;
        }
        if (isRegistrationOverNotCompleted()) {
            return ProgressStyleClass.NOT_COMPLETED.value;
        }
        if (isRegistrationCompleted()) {
            return ProgressStyleClass.COMPLETED.value;
        }
        return ProgressStyleClass.DEFAULT.value;
    }

    public String getPreparationStyleClass() {
        if (isPreparationInProgress()) {
            return ProgressStyleClass.IN_PROGRESS.value;
        }
        if (isPreparationCompleted()) {
            return ProgressStyleClass.COMPLETED.value;
        }
        return ProgressStyleClass.DEFAULT.value;
    }

    public String getTestingStyleClass() {
        if (isTestingInProgress()) {
            return ProgressStyleClass.IN_PROGRESS.value;
        }
        if (isTestingCompleted()) {
            return ProgressStyleClass.COMPLETED.value;
        }
        return ProgressStyleClass.DEFAULT.value;
    }

    public String getEvaluationStyleClass() {
        if (isEvaluationInProgress()) {
            return ProgressStyleClass.IN_PROGRESS.value;
        }
        return ProgressStyleClass.DEFAULT.value;
    }

    public String getFormattedRegistrationDeadline() {
        if (currentTestingSession.getRegistrationDeadlineDate() != null) {
            return new SimpleDateFormat(DATE_PATTERN, LocaleSelector.instance().getLocale())
                  .format(currentTestingSession.getRegistrationDeadlineDate());
        }
        return UNDEFINED;
    }

    public String getFormattedSessionBeginning() {
        if (currentTestingSession.getBeginningSession() != null) {
            return new SimpleDateFormat(DATE_PATTERN, LocaleSelector.instance().getLocale())
                  .format(currentTestingSession.getBeginningSession());
        }
        return UNDEFINED;
    }

    public String getFormattedSessionEnd() {
        if (currentTestingSession.getEndingSession() != null) {
            return new SimpleDateFormat(DATE_PATTERN, LocaleSelector.instance().getLocale())
                  .format(currentTestingSession.getEndingSession());
        }
        return UNDEFINED;
    }

    /////////////////// Checkers ///////////////////

    public boolean checkRole() {
        return identity.hasRole(Role.VENDOR) ||
                identity.hasRole(Role.VENDOR_ADMIN) ||
                identity.hasRole(Role.TESTING_SESSION_ADMIN) ||
                identity.hasRole(Role.ADMIN) ||
                identity.hasRole(Role.MONITOR);
    }

    public boolean showProgressBar() {
        return checkRole() && !testingSessionService.isContinuousSession(currentTestingSession) && applicationManager.isTestManagement();
    }

    public boolean showSessionsCards() {
        return checkRole();
    }

    public boolean isRegistrationInProgress() {
        if (identity.hasRole(Role.ADMIN) || identity.hasRole(Role.TESTING_SESSION_ADMIN)) {
            return !isContinuousSession &&
                    isRegistrationOpen;
        }
        return !isContinuousSession &&
                isRegistrationOpen && !isThereAcceptedSystem;
    }

    public boolean isRegistrationOverNotCompleted() {
        if (identity.hasRole(Role.ADMIN) || identity.hasRole(Role.TESTING_SESSION_ADMIN)) {
            return false;
        }
        return !isContinuousSession &&
                !isRegistrationOpen && !isThereAcceptedSystem;
    }

    public boolean isRegistrationCompleted() {
        if (identity.hasRole(Role.ADMIN) || identity.hasRole(Role.TESTING_SESSION_ADMIN)) {
            return !isContinuousSession;
        }
        return !isContinuousSession && isThereAcceptedSystem;
    }

    public boolean isPreparationInProgress() {
        if (identity.hasRole(Role.ADMIN) || identity.hasRole(Role.TESTING_SESSION_ADMIN)) {
            return !isContinuousSession && isTestingSessionBeforeStart && !isRegistrationOpen;
        }
        return !isContinuousSession &&
                isThereAcceptedSystem && isTestingSessionBeforeStart;
    }

    public boolean isPreparationCompleted() {
        return isTestingInProgress() || isTestingCompleted();
    }

    public boolean isTestingInProgress() {
        if (identity.hasRole(Role.ADMIN)
                || identity.hasRole(Role.TESTING_SESSION_ADMIN)
                || identity.hasRole(Role.MONITOR)) {
            return !isContinuousSession && isTestingSessionRunning;
        }
        return !isContinuousSession &&
                isTestingSessionRunning && isThereAcceptedSystem;
    }

    public boolean isTestingCompleted() {
        return isEvaluationInProgress() || isEvaluationCompleted();
    }

    public boolean isEvaluationInProgress() {
        if (identity.hasRole(Role.ADMIN) || identity.hasRole(Role.TESTING_SESSION_ADMIN)) {
            return !isContinuousSession && isTestingSessionAfterEnd;
        }

        return !isContinuousSession &&
                isThereAcceptedSystem && isTestingSessionAfterEnd;
    }

    public boolean isEvaluationCompleted() {
        //To be defined
        return false;
    }


    public boolean showNoPhaseForContinuousSession() {
        return isContinuousSession;
    }

    public boolean showRegistrationPhase() {
        if (identity.hasRole(Role.ADMIN)
                || identity.hasRole(Role.TESTING_SESSION_ADMIN)
                || identity.hasRole(Role.MONITOR)) {
            return isRegistrationOpen && !isContinuousSession;
        }
        return !isContinuousSession && !isThereAcceptedSystem;
    }

    public boolean showPreparationPhase() {
        if (identity.hasRole(Role.ADMIN)
                || identity.hasRole(Role.TESTING_SESSION_ADMIN)
                || identity.hasRole(Role.MONITOR)) {
            //to be determined
            return !isRegistrationOpen && !isContinuousSession && isTestingSessionBeforeStart;
        }
        return !isContinuousSession &&
                isThereAcceptedSystem && isTestingSessionBeforeStart;
    }

    public boolean showTestingPhase() {
        return isTestingSessionRunning;
    }

    public boolean showEvaluationPhase() {
        if (identity.isLoggedIn() && identity.hasRole(Role.MONITOR) &&
              testingSessionService.isTestingSessionRunning(testingSessionService.getUserTestingSession())) {
            return false;
        }
        return isTestingSessionAfterEnd && !isContinuousSession;
    }

}
