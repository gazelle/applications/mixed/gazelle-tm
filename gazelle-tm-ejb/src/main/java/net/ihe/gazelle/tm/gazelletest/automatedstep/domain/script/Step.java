package net.ihe.gazelle.tm.gazelletest.automatedstep.domain.script;

import java.util.ArrayList;
import java.util.List;

public class Step {

    private String id;
    private String type;
    private List<Property> properties;

    public Step() {
        // empty for serialization
    }

    public Step(String id, String type, List<Property> properties) {
        this.id = id;
        this.type = type;
        this.properties = new ArrayList<>(properties);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Property> getProperties() {
        return new ArrayList<>(properties);
    }

    public void setProperties(List<Property> properties) {
        this.properties = new ArrayList<>(properties);
    }
}
