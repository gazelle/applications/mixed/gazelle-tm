package net.ihe.gazelle.tm.gazelletest.testingSessionScope;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.tm.gazelletest.model.instance.SystemAIPOResultForATestingSession;
import net.ihe.gazelle.tm.systems.model.ProfileInTestingSession;
import net.ihe.gazelle.tm.systems.model.Testability;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import net.ihe.gazelle.tm.testexecution.SUTCapabilityStatus;

import java.util.List;
import java.util.Map;

public interface TestingSessionScopeService {
    Filter<ProfileInTestingSession> getFilter(Map<String, String> requestParameterMap);

    List<ProfileCoverage> getProfileCoverageForProfileScope(ProfileScope profileScope);

    List<ProfileScope> getProfileScopes(Filter<ProfileInTestingSession> filter);

    String getFormattedLastChangedDateForProfileScope(ProfileScope profileScope);

    int getMaxProfileCoverageForProfileScope(ProfileScope profileScope);

    int getCurrentProfileCoverageForProfileScope(ProfileScope profileScope);

    void updateProfileInTestingSession(ProfileInTestingSession profileInTestingSession);

    List<SystemAIPOResultForATestingSession> getSystemAIPOResultByTestability(Testability testability, TestingSession testingSession);

    void autoEvaluate(TestingSession session, Testability testability, SUTCapabilityStatus evaluationStatus);
}
