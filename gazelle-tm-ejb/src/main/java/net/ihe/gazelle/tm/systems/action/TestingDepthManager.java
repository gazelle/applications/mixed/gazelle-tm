/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.tm.systems.action;

import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import net.ihe.gazelle.ssov7.gum.client.application.Role;
import net.ihe.gazelle.tf.model.*;
import net.ihe.gazelle.tm.systems.model.System;
import net.ihe.gazelle.tm.systems.model.*;
import net.ihe.gazelle.tm.utils.systems.IHEImplementationForSystem;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.*;
import org.jboss.seam.international.StatusMessages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * <b>Class Description : </b>TestingDepthManager<br>
 * <br>
 * This class manage the TestingDepth object. It corresponds to the Business Layer. All operations to implement are done in this class : <li>Add a
 * testingDepth</li> <li>Edit a testingDepth</li> <li>
 * Delete a testingDepth</li> <li>etc...</li>
 *
 * @author Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @version 1.0 - 2008, July 10
 * @class TestingDepthManager.java
 * @package net.ihe.gazelle.tm.systems.action
 * @see > Jchatel@irisa.fr - http://www.ihe-europe.org
 */

@Scope(ScopeType.PAGE)
@Name("testingDepthManager")
@GenerateInterface("TestingDepthManagerLocal")
public class TestingDepthManager implements Serializable, TestingDepthManagerLocal {
    private static final long serialVersionUID = -450911336361283760L;
    private static final Logger LOG = LoggerFactory.getLogger(TestingDepthManager.class);

    /**
     * entityManager is the interface used to interact with the persistence context.
     */
    @In
    private EntityManager entityManager;
    @In
    private GazelleIdentity identity;

    /**
     * SelectedSystem object managed my this manager bean and injected in JSF
     */
    private SystemInSession selectedSystemInSession;

    /**
     * TestingDepth objects - List of possible values managed my this manager bean and injected in JSF
     */
    private List<TestingDepth> possibleTestingDepths;

    @Create
    @Override
    public void init() {
        final String systemInSessionId = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("id");
        if (systemInSessionId != null) {
            try {
                Integer id = Integer.parseInt(systemInSessionId);
                selectedSystemInSession = entityManager.find(SystemInSession.class, id);
            } catch (NumberFormatException e) {
                LOG.error("failed to find system in session with id = " + systemInSessionId);
            }
        }
    }

    /**
     * Set and persist the selected testingDepth after a user action (click on TestingDepth listBox) This operation is allowed for some granted
     * testingDepths (check the security.drl)
     */
    @Override
    public void setTestingDepthFromListbox(final IHEImplementationForSystem iheImplementation) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("setTestingDepthFromListbox");
        }
        setTestingDepth(iheImplementation, false);
    }

    @Override
    public void setTestingDepthFromListbox(final IHEImplementationForSystem iheImplementation, SimulatorInSession simulatorInSession) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("setTestingDepthFromListbox");
        }
        setTestingDepth(iheImplementation, false, simulatorInSession);
    }

    @Override
    public void setWantedTestingDepthFromListbox(final IHEImplementationForSystem iheImplementation) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("setWantedTestingDepthFromListbox");
        }
        setTestingDepth(iheImplementation, true);
    }

    @Override
    public void setTestingDepthFromListboxSAP(SystemActorProfiles systemActorProfile) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("setTestingDepthFromListboxSAP");
        }
        changeTestingDepth(false, systemActorProfile.getSystem(), systemActorProfile.getActorIntegrationProfileOption()
                .getActorIntegrationProfile().getIntegrationProfile(), systemActorProfile
                .getActorIntegrationProfileOption().getActorIntegrationProfile().getActor(), systemActorProfile
                .getActorIntegrationProfileOption().getIntegrationProfileOption(), systemActorProfile.getTestingDepth());
    }

    @Override
    public void setTestingDepth(final IHEImplementationForSystem iheImplementation, boolean wanted, SimulatorInSession simulatorInSession) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("setTestingDepth");
        }
        System system = null;
        if (selectedSystemInSession != null) {
            system = selectedSystemInSession.getSystem();
        }
        if (system == null) {
            system = simulatorInSession.getSystem();
        }
        if (iheImplementation != null) {
            IntegrationProfile integrationProfile = iheImplementation.getIntegrationProfile();
            Actor actor = iheImplementation.getActor();
            IntegrationProfileOption integrationProfileOption = iheImplementation.getIntegrationProfileOption();

            TestingDepth testingDepthToPersist;
            if (wanted) {
                testingDepthToPersist = iheImplementation.getWantedTestingDepth();
            } else {
                testingDepthToPersist = iheImplementation.getTestingDepth();
            }

            if (testingDepthToPersist != null) {
                testingDepthToPersist = entityManager.find(TestingDepth.class, testingDepthToPersist.getId());
            }
            changeTestingDepth(wanted, system, integrationProfile, actor, integrationProfileOption, testingDepthToPersist);
        }
    }

    @Override
    public void setTestingDepth(final IHEImplementationForSystem iheImplementation, boolean wanted) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("setTestingDepth");
        }
        if (iheImplementation != null) {
            System system = selectedSystemInSession.getSystem();
            IntegrationProfile integrationProfile = iheImplementation.getIntegrationProfile();
            Actor actor = iheImplementation.getActor();
            IntegrationProfileOption integrationProfileOption = iheImplementation.getIntegrationProfileOption();

            TestingDepth testingDepthToPersist;
            if (wanted) {
                testingDepthToPersist = iheImplementation.getWantedTestingDepth();
            } else {
                testingDepthToPersist = iheImplementation.getTestingDepth();
            }
            if (testingDepthToPersist != null) {
                testingDepthToPersist = entityManager.find(TestingDepth.class, testingDepthToPersist.getId());
            }
            changeTestingDepth(wanted, system, integrationProfile, actor, integrationProfileOption, testingDepthToPersist);
        }
    }

    @Override
    public void changeTestingDepth(boolean wanted, System system, IntegrationProfile integrationProfile, Actor actor,
                                  IntegrationProfileOption integrationProfileOption, TestingDepth testingDepthToPersist) {
        SystemActorProfilesQuery query = new SystemActorProfilesQuery();
        query.system().id().eq(system.getId());
        query.actorIntegrationProfileOption().actorIntegrationProfile().integrationProfile().id().eq(integrationProfile.getId());
        query.actorIntegrationProfileOption().actorIntegrationProfile().actor().id().eq(actor.getId());
        query.actorIntegrationProfileOption().integrationProfileOption().id().eq(integrationProfileOption.getId());

        List<SystemActorProfiles> listSystemActorProfiles = query.getList();
        if (listSystemActorProfiles.size() >= 1) {
            for (SystemActorProfiles systemActorProfiles : listSystemActorProfiles) {
                if (wanted) {
                    systemActorProfiles.setWantedTestingDepth(testingDepthToPersist);
                } else {
                    systemActorProfiles.setTestingDepth(testingDepthToPersist);
                    if (testingDepthToPersist == null) {
                        systemActorProfiles.setTestingDepthReviewed(false);
                    } else {
                        systemActorProfiles.setTestingDepthReviewed(true);
                    }
                }
                entityManager.merge(systemActorProfiles);
            }
        } else {
            StatusMessages.instance().add("Internal error : Cannot persist system type");
            LOG.error("No SystemActorProfiles to upadte...");
        }
    }

    /**
     * Find all testingDepths existing in the database
     *
     * @return List<TestingDepth> : list of the possible TestingDepth
     */
    @Override
    public List<TestingDepth> getPossibleTestingDepths() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getPossibleTestingDepths");
        }
        possibleTestingDepths = entityManager.createQuery("from TestingDepth row").getResultList();
        return possibleTestingDepths;
    }

    /**
     * Destroy the Manager bean when the session is over.
     */
    @Override
    @Destroy

    public void destroy() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("destroy");
        }
    }

    @Override
    public boolean canSetTestingDepth(IHEImplementationForSystem iheImplementationForSystem) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("canSetTestingDepth");
        }
        if (iheImplementationForSystem == null) {
            return false;
        }
        if (!identity.hasRole(Role.ADMIN)) {
            return false;
        }
        ActorIntegrationProfileOption aipo = getAIPO(iheImplementationForSystem);
        if ((aipo != null) && aipo.getMaybeSupportive()) {
            return true;
        }
        return false;
    }

    @Override
    public boolean couldSetWantedTestingDepth(IHEImplementationForSystem iheImplementationForSystem) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("couldSetWantedTestingDepth");
        }
        if (iheImplementationForSystem == null) {
            return false;
        }
        ActorIntegrationProfileOption aipo = getAIPO(iheImplementationForSystem);
        if ((aipo != null) && aipo.getMaybeSupportive()) {
            return true;
        }
        return false;
    }

    @Override
    public boolean canSetWantedTestingDepth(IHEImplementationForSystem iheImplementationForSystem,
                                           net.ihe.gazelle.tm.systems.model.System system) {
        if ((iheImplementationForSystem == null) || (system == null)) {
            return false;
        }
        if (identity.hasRole(Role.ADMIN)) {
            return true;
        }
        Set<InstitutionSystem> institutionSystems = system.getInstitutionSystems();
        boolean found = false;
        if (institutionSystems != null) {
            for (InstitutionSystem institutionSystem : institutionSystems) {
                if (institutionSystem.getInstitution().getKeyword().equals(identity.getOrganisationKeyword())) {
                    found = true;
                }
            }
        }
        if (!found) {
            return false;
        }
        ActorIntegrationProfileOption uniqueResult = getAIPO(iheImplementationForSystem);
        if ((uniqueResult != null) && uniqueResult.getMaybeSupportive()) {
            SystemActorProfiles sap = getSystemActorProfiles(uniqueResult, system);
            if (sap != null) {
                if ((sap.getTestingDepthReviewed() == null) || !sap.getTestingDepthReviewed()) {
                    return true;
                }
            }
            return false;
        } else {
            return false;
        }
    }

    @Override
    public SystemActorProfiles getSystemActorProfiles(IHEImplementationForSystem iheImplementationForSystem,
                                                      net.ihe.gazelle.tm.systems.model.System system) {
        return getSystemActorProfiles(getAIPO(iheImplementationForSystem), system);
    }

    @Override
    public SystemActorProfiles getSystemActorProfiles(ActorIntegrationProfileOption aipo,
                                                      net.ihe.gazelle.tm.systems.model.System system) {
        if (aipo == null) {
            return null;
        }
        SystemActorProfilesQuery sapQuery = new SystemActorProfilesQuery();
        sapQuery.actorIntegrationProfileOption().eq(aipo);
        sapQuery.system().eq(system);
        SystemActorProfiles sap = sapQuery.getUniqueResult();
        return sap;
    }

    @Override
    public ActorIntegrationProfileOption getAIPO(IHEImplementationForSystem iheImplementationForSystem) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getAIPO");
        }
        if (iheImplementationForSystem == null) {
            return null;
        }
        ActorIntegrationProfileOptionQuery query = new ActorIntegrationProfileOptionQuery();
        query.actorIntegrationProfile().actor().eq(iheImplementationForSystem.getActor());
        query.actorIntegrationProfile().integrationProfile().eq(iheImplementationForSystem.getIntegrationProfile());
        query.integrationProfileOption().eq(iheImplementationForSystem.getIntegrationProfileOption());
        ActorIntegrationProfileOption uniqueResult = query.getUniqueResult();
        return uniqueResult;
    }

    @Override
    public void setTestingDepthReviewed(IHEImplementationForSystem iheImplementationForSystem,
                                       net.ihe.gazelle.tm.systems.model.System system) {
        SystemActorProfiles sap = getSystemActorProfiles(iheImplementationForSystem, system);
        if (sap != null) {
            sap.setTestingDepthReviewed(iheImplementationForSystem.isTestingDepthReviewed());
            setTestingDepthReviewedSAP(sap);
        }
    }

    @Override
    public void setTestingDepthReviewedSAP(SystemActorProfiles systemActorProfiles) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("setTestingDepthReviewedSAP");
        }
        EntityManagerService.provideEntityManager().merge(systemActorProfiles);
        EntityManagerService.provideEntityManager().flush();
    }

    @Override
    public void initWantedTestingDepth(SystemInSession sis, IHEImplementationForSystem iheImplementationForSystem) {
        selectedSystemInSession = sis;
        initWantedTestingDepth(iheImplementationForSystem);
    }

    @Override
    public void initWantedTestingDepth(IHEImplementationForSystem iheImplementationForSystem) {
        System system = selectedSystemInSession.getSystem();
        IntegrationProfile integrationProfile = iheImplementationForSystem.getIntegrationProfile();
        Actor actor = iheImplementationForSystem.getActor();
        IntegrationProfileOption integrationProfileOption = iheImplementationForSystem.getIntegrationProfileOption();

        SystemActorProfilesQuery query = new SystemActorProfilesQuery();
        query.system().id().eq(system.getId());
        query.actorIntegrationProfileOption().actorIntegrationProfile().integrationProfile().id()
                .eq(integrationProfile.getId());
        query.actorIntegrationProfileOption().actorIntegrationProfile().actor().id().eq(actor.getId());

        query.actorIntegrationProfileOption().integrationProfileOption().id().eq(integrationProfileOption.getId());

        List<SystemActorProfiles> listSystemActorProfiles = query.getList();
        for (SystemActorProfiles systemActorProfiles : listSystemActorProfiles) {
            TestingDepth res = systemActorProfiles.getWantedTestingDepth();
            if (res != null) {
            } else {
                TestingDepthQuery q = new TestingDepthQuery();
                q.name().eq("Thorough");
                TestingDepth testingDepth = q.getUniqueResult();
                if (testingDepth != null) {
                    systemActorProfiles.setWantedTestingDepth(testingDepth);
                    systemActorProfiles = entityManager.merge(systemActorProfiles);
                }
            }
        }
        if (iheImplementationForSystem.getWantedTestingDepth() == null) {
            TestingDepthQuery q = new TestingDepthQuery();
            q.name().eq("Thorough");
            TestingDepth testingDepth = q.getUniqueResult();
            if (testingDepth != null) {
                iheImplementationForSystem.setWantedTestingDepth(testingDepth);
            }
        }
        entityManager.flush();
    }
}
