package net.ihe.gazelle.tm.messages;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.messaging.MessagePropertyChanged;
import net.ihe.gazelle.ssov7.gum.client.application.User;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import net.ihe.gazelle.ssov7.gum.client.interlay.client.utils.UserSearchParams;
import net.ihe.gazelle.tm.gazelletest.model.instance.Status;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstance;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import org.kohsuke.MetaInfServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

@MetaInfServices(MessageSourceFromPropertyChange.class)
public class TestInstanceStatusMessageSource extends MessageSourceUsersDB<TestInstance> implements
        MessageSourceFromPropertyChange<TestInstance, Status> {

    private static final Logger LOG = LoggerFactory.getLogger(TestInstanceStatusMessageSource.class);
    private final Collection<String> pathsToUser = new ArrayList<>();

    public TestInstanceStatusMessageSource() {
        super();
        pathsToUser.add("listTestInstanceEvent.userId");
    }

    public TestInstanceStatusMessageSource(UserService userService) {
        super(userService);
        pathsToUser.add("listTestInstanceEvent.userId");
    }

    @Override
    public List<User> getUsers(String userId, TestInstance sourceObject, String[] messageParameters) {
        List<User> superUsers = super.getUsers(userId, sourceObject, messageParameters);
        Set<User> users = superUsers != null
                ? new HashSet<>(superUsers)
                : new HashSet<User>();
        HQLQueryBuilder<TestInstance> queryBuilder = new HQLQueryBuilder<>(getSourceClass());
        queryBuilder.addEq("id", getId(sourceObject, messageParameters));

        List<?> orgaIds = queryBuilder.getListDistinct("testInstanceParticipants.systemInSessionUser.systemInSession.system.institutionSystems.institution.keyword");
        for (Object orgaId : orgaIds) {
            if (orgaId instanceof String)
                users.addAll(userService.searchNoLimit(new UserSearchParams().setOrganizationId((String) orgaId)));
        }

        if (userId != null) {
            users.remove(userService.getUserById(userId));
        }
        return new ArrayList<>(users);
    }

    @Override
    public Collection<String> getPathsToUsername() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getPathsToUsername");
        }
        return pathsToUser;
    }

    @Override
    public Class<TestInstance> getSourceClass() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getSourceClass");
        }
        return TestInstance.class;
    }

    @Override
    public String getLink(TestInstance instance, String[] messageParameters) {
        return "testInstance.seam?id=" + instance.getId();
    }

    @Override
    public Integer getId(TestInstance sourceObject, String[] messageParameters) {
        return sourceObject.getId();
    }

    @Override
    public String getImage(TestInstance sourceObject, String[] messageParameters) {
        return "gzl-icon-" + messageParameters[3].replace(' ', '-');
    }

    @Override
    public TestingSession getTestingSession(TestInstance sourceObject, String[] messageParameters) {
        return sourceObject.getTestingSession();
    }

    @Override
    public String getType(TestInstance instance, String[] messageParameters) {
        return "gazelle.message.testinstance.status";
    }

    @Override
    public Class<TestInstance> getPropertyObjectClass() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getPropertyObjectClass");
        }
        return TestInstance.class;
    }

    @Override
    public String getPropertyName() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getPropertyName");
        }
        return "lastStatus";
    }

    @Override
    public void receivePropertyMessage(MessagePropertyChanged<TestInstance, Status> messagePropertyChanged) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("receivePropertyMessage");
        }
        if (messagePropertyChanged.getObject().getId() != null) {
            String lastDisplay = "None";
            if (messagePropertyChanged.getOldValue() != null) {
                lastDisplay = messagePropertyChanged.getOldValue().getLabelToDisplay();
            }
            String newDisplay = "None";
            String newKeyword = "None";
            if (messagePropertyChanged.getNewValue() != null) {
                newDisplay = messagePropertyChanged.getNewValue().getLabelToDisplay();
                newKeyword = messagePropertyChanged.getNewValue().getKeyword();
            }
            if (!lastDisplay.equals(newDisplay)) {
                NotificationService.dispatchNotification(this, messagePropertyChanged.getObject(), newKeyword, newDisplay, lastDisplay, messagePropertyChanged
                        .getObject()
                        .getId().toString());
            }
        }
    }

}
