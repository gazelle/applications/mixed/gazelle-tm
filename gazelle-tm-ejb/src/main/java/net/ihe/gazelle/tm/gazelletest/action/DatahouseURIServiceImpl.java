package net.ihe.gazelle.tm.gazelletest.action;

import net.ihe.gazelle.preferences.PreferenceProvider;
import net.ihe.gazelle.proxy.ws.ChannelType;
import net.ihe.gazelle.proxy.ws.Configuration;
import net.ihe.gazelle.services.GenericServiceLoader;
import net.ihe.gazelle.tf.model.Actor;
import net.ihe.gazelle.tf.model.NetworkCommunicationType;
import net.ihe.gazelle.tf.model.Standard;
import net.ihe.gazelle.tm.gazelletest.model.definition.RoleInTest;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstance;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestStepsInstance;
import net.ihe.gazelle.tm.systems.model.SystemInSession;
import org.apache.http.client.utils.URIBuilder;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

@Scope(ScopeType.EVENT)
@AutoCreate
@Name("datahouseURIService")
public class DatahouseURIServiceImpl implements DatahouseURIService {
    private static final String CAPTURED_DATE = "capture_date";
    private static final String SENDER_HOSTNAME = "sender_hostname";
    private static final String PROXY_PORT = "proxy_port";
    private static final String RECEIVER_HOSTNAME = "receiver_hostname";
    private static final String CHANNEL_TYPE = "channel_type";
    private final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
    private static final Logger LOG = LoggerFactory.getLogger(DatahouseURIServiceImpl.class);
    private static final String DATE_INTERVAL_SEPARATOR = "_";

    @In
    DatahouseURIServiceDAO datahouseURIServiceDAO;

    public DatahouseURIServiceImpl() {
    }

    public DatahouseURIServiceImpl(DatahouseURIServiceDAO datahouseURIServiceDAO) {
        this.datahouseURIServiceDAO = datahouseURIServiceDAO;
    }

    @Override
    public String createDatahouseUri(TestStepsInstance testStepsInstance) {
        if (testStepsInstance == null)
            throw new IllegalArgumentException("testStepsInstance is null");
        String datahouseBaseUrl = getDatahouseBaseUrl();
        try {
            URIBuilder uriBuilder = new URIBuilder(datahouseBaseUrl);
            TestInstance testInstance = getTestInstance(testStepsInstance);
            if (testInstance == null)
                return uriBuilder.build().toString();

            RoleInTest initiatorRole = testStepsInstance.getTestSteps().getTestRolesInitiator().getRoleInTest();
            SystemInSession systemInSessionInitiator = testStepsInstance.getSystemInSessionInitiator();

            List<Configuration> initiatorConfigurations = ProxyManager.getConfigurationsForTestStepsInstance(testInstance, initiatorRole, systemInSessionInitiator);

            RoleInTest responderRole = testStepsInstance.getTestSteps().getTestRolesResponder().getRoleInTest();
            SystemInSession systemInSessionResponder = testStepsInstance.getSystemInSessionResponder();
            List<Configuration> responderConfigurations = getResponderConfigurations(testStepsInstance, testInstance, responderRole, systemInSessionResponder);

            setProxyPortParam(responderConfigurations, uriBuilder);
            setCapturedDateParam(testStepsInstance, uriBuilder);
            setSenderHostnameParam(initiatorConfigurations, uriBuilder);
            setReceiverHostnameParam(responderConfigurations, uriBuilder);
            setChannelType(responderConfigurations, uriBuilder);
            //setChannelType
            return uriBuilder.build().toString();
        } catch (URISyntaxException e) {
            throw new IllegalStateException("Error during creation of Datahouse UI URI", e);
        }

    }

    private TestInstance getTestInstance(TestStepsInstance testStepsInstance) {
        if (testStepsInstance.getTestInstance() != null)
            return testStepsInstance.getTestInstance();
        else {
            FacesContext fc = FacesContext.getCurrentInstance();
            String testId = fc.getExternalContext().getRequestParameterMap().get("id");
            if (testId != null)
                return TestInstance.getTestInstanceById(Integer.valueOf(testId));
            else
                return null;
        }
    }

    private void setChannelType(List<Configuration> responderConfigurations, URIBuilder uriBuilder) {
        String channelType = getChannelType(responderConfigurations);
        if (!channelType.isEmpty())
            uriBuilder.addParameter(CHANNEL_TYPE, channelType);
    }

    private String getChannelType(List<Configuration> responderConfigurations) {
        if (!responderConfigurations.isEmpty()) {
            String channelType = responderConfigurations.get(0).getType().getValue();
            for (Configuration responderConfiguration : responderConfigurations) {
                if (!responderConfiguration.getType().getValue().equals(channelType))
                    return "";
            }
            return channelType;
        }
        return "";
    }

    private void setReceiverHostnameParam(List<Configuration> responderConfigurations, URIBuilder uriBuilder) {
        String receiverHostname = getHostname(responderConfigurations);
        if (!receiverHostname.isEmpty()) {
            uriBuilder.setParameter(RECEIVER_HOSTNAME, receiverHostname);
        }
    }

    private void setProxyPortParam(List<Configuration> responderConfigurations, URIBuilder uriBuilder) {
        int proxyPort = getProxyPort(responderConfigurations);
        if (proxyPort != -1)
            uriBuilder.setParameter(PROXY_PORT, String.valueOf(proxyPort));
    }

    private void setSenderHostnameParam(List<Configuration> initiatorConfigurations, URIBuilder uriBuilder) {
        String senderHostname = getHostname(initiatorConfigurations);
        if (!senderHostname.isEmpty())
            uriBuilder.setParameter(SENDER_HOSTNAME, senderHostname);
    }

    private void setCapturedDateParam(TestStepsInstance testStepsInstance, URIBuilder uriBuilder) throws URISyntaxException {
        String interval = getCaptureInterval(testStepsInstance);
        if (!DATE_INTERVAL_SEPARATOR.equals(interval)) {
            String dateParam = "[" + interval + "]";
            uriBuilder.setParameter(CAPTURED_DATE, dateParam).build();
        }
    }

    public String getDatahouseBaseUrl() {
        PreferenceProvider preferenceProvider = GenericServiceLoader.getService(PreferenceProvider.class);
        String datahouseBaseUrl = preferenceProvider.getString("gazelle_datahouse_ui_base_url");
        if (datahouseBaseUrl == null || datahouseBaseUrl.isEmpty())
            throw new IllegalStateException("gazelle_datahouse_ui_base_url is null or empty");
        return datahouseBaseUrl;
    }

    private String getCaptureInterval(TestStepsInstance testStepsInstance) {
        String interval = "";
        if (testStepsInstance.getStartDate() != null) {
            interval += dateFormat.format(testStepsInstance.getStartDate());
        }
        interval += DATE_INTERVAL_SEPARATOR;
        if (testStepsInstance.getEndDate() != null)
            interval += dateFormat.format(testStepsInstance.getEndDate());
        return interval;
    }


    private int getProxyPort(List<Configuration> configurations) {
        if (!configurations.isEmpty()) {
            int proxyPort = configurations.get(0).getProxyPort();
            for (Configuration configuration : configurations) {
                if (proxyPort != configuration.getProxyPort())
                    return -1;
            }
            return proxyPort;
        }
        return -1;
    }

    private List<Configuration> getResponderConfigurations(TestStepsInstance testStepsInstance, TestInstance testInstance, RoleInTest roleInTest, SystemInSession systemInSession) {
        try {
            Standard standard = datahouseURIServiceDAO.getStandard(testStepsInstance);
            Actor actor = datahouseURIServiceDAO.getActor(testStepsInstance, roleInTest, systemInSession);
            if (standard==null || actor == null){
                String missingParam = standard == null ? "Standard" : "Actor";
                LOG.warn( "{} is null, no configuration will be returned", missingParam);
                return new ArrayList<>();
            }

            List<Configuration> configurations = ProxyManager.getApprovedResponderConfigurationsForTestStepsInstance(testInstance, roleInTest, systemInSession, testStepsInstance.getTestSteps().getTransaction());
            List<Configuration> filteredConfigurations = new ArrayList<>();
            //Usage of Boolean.TRUE.equals() for NPE check
            boolean secured = Boolean.TRUE.equals(testStepsInstance.getTestSteps().getSecured());

            for (Configuration configuration : configurations) {
                if (configuration.getSecuredChannel() == secured
                        && isNetworkCommunicationTypeValid(configuration, standard)
                        && isActorValid(configuration, actor)) {
                    filteredConfigurations.add(configuration);
                }
            }
            return filteredConfigurations;
        } catch (Exception e) {
            LOG.error("Error while getting responder configurations", e);
            return new ArrayList<>();
        }

    }

    private boolean isActorValid(Configuration configuration, Actor actor) {
        return configuration.getName().equalsIgnoreCase(actor.getKeyword());
    }

    private boolean isNetworkCommunicationTypeValid(Configuration configuration, Standard standard) {
        if (standard.getNetworkCommunicationType().equals(NetworkCommunicationType.WEBSERVICE)
                && configuration.getType().equals(ChannelType.HTTP))
            return true;
        if (standard.getNetworkCommunicationType().equals(NetworkCommunicationType.HL7v2)
                && configuration.getType().equals(ChannelType.HL7v2))
            return true;
        return configuration.getType().getValue().equalsIgnoreCase(standard.getNetworkCommunicationType().getLabel());
    }

    private String getHostname(List<Configuration> configurations) {
        if (!configurations.isEmpty()) {
            String hostname = configurations.get(0).getHostname();
            for (Configuration initiatorConfiguration : configurations) {
                if (!hostname.equals(initiatorConfiguration.getHostname())) {
                    return "";
                }
            }
            return hostname;
        } else {
            return "";
        }
    }

}
