package net.ihe.gazelle.tm.application.gui;

import net.ihe.gazelle.tm.application.action.ApplicationManager;
import net.ihe.gazelle.tm.application.model.Section;
import net.ihe.gazelle.tm.application.services.SectionService;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.bean.ManagedBean;
import javax.persistence.Enumerated;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

@Name("homeEditorBean")
@Scope(ScopeType.EVENT)
@ManagedBean
public class HomeEditorBean implements Serializable{

    private static final String ANNOUNCEMENT = "Announcement";
    private static final String DOCUMENTATION = "Documentation";
    private static final String TOOL_INDEX = "ToolIndex";

    protected static final Logger LOG = LoggerFactory.getLogger(HomeEditorBean.class);
    private static final long serialVersionUID = -3879454075382990572L;

    @In(create = true)
    private SectionService sectionService;

    @In(create = true)
    private ApplicationManager applicationManager;

    private String selectedContent;


    private Section selectedSection;

    @Enumerated
    public homeSection homeSection;

    public enum homeSection {
        ANNOUNCEMENT(HomeEditorBean.ANNOUNCEMENT, 1),
        DOCUMENTATION(HomeEditorBean.DOCUMENTATION, 2),
        TOOL_INDEX(HomeEditorBean.TOOL_INDEX,3);

        private final String keyword;
        private final int rank;

        private homeSection(String keyword, int rank) {
            this.keyword = keyword;
            this.rank = rank;
        }

        public String getKeyword() {
            return this.keyword;
        }

        public int getRank() {
            return this.rank;
        }
    }

    public List<homeSection> getHomeSections(){
        return Arrays.asList(homeSection.values());
    }

    public void setHomeSection(HomeEditorBean.homeSection homeSection) {
        this.homeSection = homeSection;
        switch (homeSection.keyword) {
            case ANNOUNCEMENT:
                setSelectedSection(getAnnouncementSection());
                break;
            case DOCUMENTATION:
                setSelectedSection(getDocumentationSection());
                break;
            case TOOL_INDEX:
                setSelectedSection(getToolIndexSection());
                break;
            default:
                LOG.error("No section selected");
        }
    }

    /**
     * Save the selected section with the new content in the database
     */
    public void save(String content){
        if(selectedSection == null){
            LOG.warn("Unselected section");
            setSelectedSection(getAnnouncementSection());
        }
        savePreviousSection();
        selectedSection.setContent(content);
        sectionService.updateSection(selectedSection);
        FacesMessages.instance().add(StatusMessage.Severity.INFO, "Section saved");
    }

    ///////////////////////////////////////////////////////////
    //////////////////Getters and Setters//////////////////////
    ///////////////////////////////////////////////////////////

    /**
     * if the selected contend is null, it will set the selected Section and content as Announcement Section and content
     * @return the String corresponding to the selected content
     */
    public String getSelectedContent() {
        if (this.selectedContent == null){
            setSelectedSection(getAnnouncementSection());
            setSelectedContent(getSelectedSection().getContent());
        }
        return this.selectedContent;
    }

    public void setSelectedContent(String selectedContent) {
        this.selectedContent = selectedContent;
    }

    public Section getSelectedSection() {
        return this.selectedSection;
    }

    private void setSelectedSection(Section selectedSection) {
        this.selectedSection = selectedSection;
        setSelectedContent(selectedSection.getContent());
    }

    /**
     * Save the content of the selected section in the database in the backup Section of the table if changed.
     * If the backup section isn't present in the database, it will create it.
     */
    private void savePreviousSection(){
        String backupSectionName = "previous" + this.selectedSection.getName();
        Section backupSection= sectionService.getSectionByName(backupSectionName);
        if(backupSection==null) {
            backupSection = new Section();
            backupSection.setName(backupSectionName);
        }
        backupSection.setContent(selectedSection.getContent());
        sectionService.updateSection(backupSection);
        setSelectedContent(backupSection.getContent());
    }

    public void restorePreviousContent(){
        String previousSectionName = "previous" + selectedSection.getName();
        Section previousSection = sectionService.getSectionByName(previousSectionName);
        if(previousSection != null) {
            String content = previousSection.getContent();
            save(content);
        }
    }

    /**
     * @return Documentation section if the application is TestManagement else null
     */
    public Section getDocumentationSection() {
        Section section = null;
        if (Boolean.TRUE.equals(applicationManager.isTestManagement())) {
            section = sectionService.getSectionByName(DOCUMENTATION);
        }
        return section;
    }

    /**
     * @return Tool Index section if the application is TestManagement else null
     */
    public Section getToolIndexSection() {
        Section section = null;
        if(Boolean.TRUE.equals(applicationManager.isTestManagement())) {
            section = sectionService.getSectionByName(TOOL_INDEX);
        }
        return section;
    }

    /**
     * @return Announcement section if the application is TestManagement else null
     */
    private Section getAnnouncementSection() {
        Section section = null;
        if (Boolean.TRUE.equals(applicationManager.isTestManagement())) {
            section = sectionService.getSectionByName(ANNOUNCEMENT);
        }
        return section;
    }

    public homeSection getHomeSection(){
        return this.homeSection;
    }

    public List<Section> getSectionValue(){
        List<Section> sections = null;
        if (applicationManager.isTestManagement()){
            sections = sectionService.getSections();
        }
        return sections;
    }

}
