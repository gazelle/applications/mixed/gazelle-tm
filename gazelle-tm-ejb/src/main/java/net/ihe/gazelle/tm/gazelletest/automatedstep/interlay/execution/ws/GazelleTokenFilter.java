package net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.execution.ws;

import net.ihe.gazelle.tm.gazelletest.action.InvalidTokenException;
import org.apache.http.HttpHeaders;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response;
import java.io.IOException;

public class GazelleTokenFilter implements Filter {

    public static final String GAZELLE_TOKEN_KEY_PREFIX = "GazelleToken ";
    public static final String CONST_GAZELLE_TOKEN = "_const_gazelle_token_";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // nothing to init
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        if (!(request instanceof HttpServletRequest)) {
            throw new ServletException("This filter can only process HttpServletRequest requests");
        } else {
            final HttpServletRequest httpRequest = (HttpServletRequest) request;
            final HttpServletResponse httpResponse = (HttpServletResponse) response;
            try {
                String authnHeader = httpRequest.getHeader(HttpHeaders.AUTHORIZATION);
                if (authnHeader != null && authnHeader.startsWith(GAZELLE_TOKEN_KEY_PREFIX)) {
                    String token = parseToken(authnHeader);
                    httpRequest.setAttribute(CONST_GAZELLE_TOKEN, token);
                } else {
                    httpRequest.setAttribute(CONST_GAZELLE_TOKEN, null);
                }
            } catch (InvalidTokenException e) {
                httpResponse.sendError(Response.Status.UNAUTHORIZED.getStatusCode(), e.getMessage());
            } catch (Exception e) {
                httpResponse.sendError(Response.Status.UNAUTHORIZED.getStatusCode());
            }
            chain.doFilter(httpRequest, httpResponse);
        }
    }

    @Override
    public void destroy() {
        // Nothing to do here
    }

    private String parseToken(String token) {
        return token.replace(GAZELLE_TOKEN_KEY_PREFIX, "");
    }
}
