package net.ihe.gazelle.tm.gazelletest.action.conformitytestreport.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.NONE)
public class AbstractTestReportModel {

    @XmlElement(name = "testInfo")
    private DataOfTFObject testKeywordName;

    /**
     * Getter of Keyword of the test
     *
     * @return String keyword
     */
    public String getKeyword() {
        return this.testKeywordName.getKeyword();
    }

    /**
     * Setter of keyword
     *
     * @param keyword keyword String
     */
    public void setKeyword(String keyword) {
        this.testKeywordName.setKeyword(keyword);
    }

    /**
     * Getter of the Name of the test
     *
     * @return String name
     */
    public String getName() {
        return this.testKeywordName.getName();
    }

    /**
     * Setter of the test name
     *
     * @param name of the test
     */
    public void setName(String name) {
        this.testKeywordName.setName(name);
    }

    public AbstractTestReportModel() {
        testKeywordName = new DataOfTFObject();
    }

    public AbstractTestReportModel(String keyword, String name) {
        testKeywordName = new DataOfTFObject(keyword, name);
    }

    public AbstractTestReportModel(String keyword, String name, String testType) {
        testKeywordName = new DataOfTFObject(keyword, name, testType);
    }


}
