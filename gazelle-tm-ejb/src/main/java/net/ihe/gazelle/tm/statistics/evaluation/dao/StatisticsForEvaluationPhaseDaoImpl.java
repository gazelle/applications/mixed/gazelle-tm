package net.ihe.gazelle.tm.statistics.evaluation.dao;

import net.ihe.gazelle.tf.model.ActorIntegrationProfileOption;
import net.ihe.gazelle.tf.model.ActorIntegrationProfileOptionAttributes;
import net.ihe.gazelle.tf.model.Domain;
import net.ihe.gazelle.tm.gazelletest.model.instance.SystemAIPOResultForATestingSession;
import net.ihe.gazelle.tm.gazelletest.model.instance.SystemAIPOResultForATestingSessionQuery;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstance;
import net.ihe.gazelle.tm.statistics.common.StatisticsCommonService;
import net.ihe.gazelle.tm.statistics.evaluation.StatisticsForEvaluationPhaseDao;
import net.ihe.gazelle.tm.systems.model.SystemInSession;
import net.ihe.gazelle.tm.systems.model.SystemInSessionQuery;
import net.ihe.gazelle.tm.systems.model.SystemInSessionRegistrationStatus;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import net.ihe.gazelle.users.model.Institution;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import java.util.List;

@Scope(ScopeType.STATELESS)
@AutoCreate
@Name(value = "statisticsForEvaluationPhaseDao")
public class StatisticsForEvaluationPhaseDaoImpl implements StatisticsForEvaluationPhaseDao {

    @In(value = "statisticsCommonService")
    StatisticsCommonService statisticsCommonService;

    @Override
    public List<SystemInSession> getSystemsForCurrentUser(TestingSession selectedTestingSession) {
        SystemInSessionQuery systemInSessionQuery = getSystemInSessionQuery(selectedTestingSession);
        systemInSessionQuery.system().institutionSystems().institution().eq(Institution.getLoggedInInstitution());

        return systemInSessionQuery.getListDistinct();

    }

    private SystemInSessionQuery getSystemInSessionQuery(TestingSession selectedTestingSession) {
        SystemInSessionQuery systemInSessionQuery = new SystemInSessionQuery();
        systemInSessionQuery.acceptedToSession().eq(true);
        systemInSessionQuery.system().isTool().eq(false);
        systemInSessionQuery.testingSession().eq(selectedTestingSession);
        return systemInSessionQuery;
    }

    @Override
    public List<SystemAIPOResultForATestingSession> getCapabilitiesForASystem(SystemInSession system) {
        SystemAIPOResultForATestingSessionQuery capabilityQuery = new SystemAIPOResultForATestingSessionQuery();
        capabilityQuery.systemActorProfile().system().id().eq(system.getSystem().getId());
        capabilityQuery.systemActorProfile().system().keyword().order(true, true);
        capabilityQuery.systemActorProfile().system().isTool().eq(false);
        return capabilityQuery.getList();
    }

    @Override
    public List<TestInstance> getAllTestInstancesForCurrentSession() {
        return statisticsCommonService.getTestInstanceParticipantsQuery().testInstance().getListDistinct();
    }

    @Override
    public List<SystemAIPOResultForATestingSession> getAllCapabilitiesForADomain(Domain domain, TestingSession selectedTestingSession) {
        SystemAIPOResultForATestingSessionQuery capabilityQuery = getSystemAIPOResultForATestingSessionQueryForADomain(domain, selectedTestingSession);
        return capabilityQuery.getListDistinct();
    }

    private SystemAIPOResultForATestingSessionQuery getSystemAIPOResultForATestingSessionQueryForADomain(Domain domain, TestingSession selectedTestingSession) {
        SystemAIPOResultForATestingSessionQuery capabilityQuery = new SystemAIPOResultForATestingSessionQuery();
        ActorIntegrationProfileOptionAttributes<ActorIntegrationProfileOption> aipoAttributes = capabilityQuery.systemActorProfile().actorIntegrationProfileOption();
        capabilityQuery.addRestriction(aipoAttributes.actorIntegrationProfile().integrationProfile().domainsForDP().id().eqRestriction(domain.getId()));
        capabilityQuery.testSession().eq(selectedTestingSession);
        capabilityQuery.systemActorProfile().system().systemsInSession().registrationStatus().neq(SystemInSessionRegistrationStatus.DROPPED);
        capabilityQuery.systemActorProfile().system().isTool().neq(true);
        capabilityQuery.systemActorProfile().system().systemsInSession().acceptedToSession().eq(true);
        return capabilityQuery;
    }

    @Override
    public int getNbTotalTestInstancesForCurrentSession() {
        return statisticsCommonService.getTestInstanceParticipantsQuery().testInstance().getCountDistinctOnPath();
    }

    @Override
    public int getNbTotalCapabilitiesForADomain(Domain domain, TestingSession selectedTestingSession) {
        SystemAIPOResultForATestingSessionQuery capabilityQuery = getSystemAIPOResultForATestingSessionQueryForADomain(domain, selectedTestingSession);
        return capabilityQuery.getCountDistinctOnPath();
    }


}
