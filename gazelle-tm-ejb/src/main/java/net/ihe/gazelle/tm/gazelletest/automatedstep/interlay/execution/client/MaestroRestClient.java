package net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.execution.client;

import net.ihe.gazelle.tm.gazelletest.automatedstep.domain.script.MaestroRequest;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestStepsInstance;

public interface MaestroRestClient {

    /**
     * Client to call ITB
     * @param script The JSON script to be sent
     * @param stepInstance The test step instance that launched the call
     * @throws StepRunClientException
     */
    void runAutomatedTestStep(MaestroRequest script, TestStepsInstance stepInstance) throws StepRunClientException;
}
