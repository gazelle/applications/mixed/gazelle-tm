package net.ihe.gazelle.tm.gazelletest.action;

import org.xml.sax.SAXException;

import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.IOException;
import java.io.StringReader;

/**
 * Utility class for XSD validation Unit tests of External Tool Test Report
 */
public class TestReportXSDValidator {

   private final Validator validator;

   public TestReportXSDValidator(final String TEST_REPORT_XSD_LOCATION) throws SAXException {
      SchemaFactory sfactory = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
      Schema schema = sfactory.newSchema(
              new StreamSource[]{getResourceAsStreamSource("test-report/xml.xsd"),
                      getResourceAsStreamSource(TEST_REPORT_XSD_LOCATION)}
      );
      validator = schema.newValidator();
   }

   public XSDErrorHandler validateResource(String fileContent) throws IOException, SAXException {
      XSDErrorHandler errorHandler = new XSDErrorHandler();
      validator.setErrorHandler(errorHandler);
      validator.validate(new StreamSource(new StringReader(fileContent)));
      return errorHandler;
   }
   public XSDErrorHandler validateResource(StreamSource streamSource) throws IOException, SAXException {
      XSDErrorHandler errorHandler = new XSDErrorHandler();
      validator.setErrorHandler(errorHandler);
      validator.validate(streamSource);
      return errorHandler;
   }

   private StreamSource getResourceAsStreamSource(String resourcePath) {
      return new StreamSource(TestReportXSDValidator.class.getClassLoader().getResourceAsStream(resourcePath));
   }

}
