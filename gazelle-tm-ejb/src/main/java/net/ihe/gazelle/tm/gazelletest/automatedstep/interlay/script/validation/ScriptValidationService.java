package net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.script.validation;

import net.ihe.gazelle.tm.gazelletest.automatedstep.domain.script.Script;

public interface ScriptValidationService {

    /**
     * Validate an automation script using a json schéma
     * @param jsonString the content of the script
     * @return the list of error messages if any
     */
    String validate(String jsonString);

    /**
     * Parse the automation script into a domain object representation
     * @param jsonString the content of the script
     * @return the script representation
     */
    Script parse(String jsonString);
}
