package net.ihe.gazelle.tm.gazelletest.testingSessionScope;

import net.ihe.gazelle.tm.systems.model.ProfileInTestingSession;

import java.util.List;

public class ProfileScope {
    private ProfileInTestingSession profileInTestingSession;
    private List<ProfileCoverage> profileCoverages;

    public ProfileInTestingSession getProfileInTestingSession() {
        return profileInTestingSession;
    }

    public void setProfileInTestingSession(ProfileInTestingSession profileInTestingSession) {
        this.profileInTestingSession = profileInTestingSession;
    }

    public List<ProfileCoverage> getActorCoverages() {
        return profileCoverages;
    }

    public void setActorCoverages(List<ProfileCoverage> profileCoverages) {
        this.profileCoverages = profileCoverages;
    }
}