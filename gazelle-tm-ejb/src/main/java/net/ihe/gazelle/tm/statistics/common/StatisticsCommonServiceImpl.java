package net.ihe.gazelle.tm.statistics.common;

import net.ihe.gazelle.tm.gazelletest.model.instance.Status;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstance;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstanceParticipantsQuery;
import net.ihe.gazelle.tm.session.TestingSessionService;
import net.ihe.gazelle.tm.systems.model.*;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import java.util.List;

@Scope(ScopeType.STATELESS)
@AutoCreate
@Name(value = "statisticsCommonService")
public class StatisticsCommonServiceImpl implements StatisticsCommonService {

    @In(value = "statisticsCommonDao")
    StatisticsCommonDao statisticsCommonDao;

    @In(value = "testingSessionService")
    TestingSessionService testingSessionService;

    @Override
    public TestInstanceParticipantsQuery getTestInstanceParticipantsQuery() {
        return statisticsCommonDao.getTestInstanceParticipantsQuery(testingSessionService.getUserTestingSession());
    }

    @Override
    public SystemInSessionQuery getSystemInSessionQuery() {
        return statisticsCommonDao.getSystemInSessionQuery(testingSessionService.getUserTestingSession());
    }

    @Override
    public ProfileInTestingSessionStats getProfileInTestingSessionStatsForCurrentSession() {
        List<ProfileInTestingSession> profileInTestingSessions = statisticsCommonDao.getAllProfileInTestingSessionForCurrentSession(testingSessionService.getUserTestingSession());

        int nbTestable = 0;
        int nbDropped = 0;
        int nbFewPartners = 0;
        int nbDecisionPending = 0;

        for (ProfileInTestingSession profileInTestingSession : profileInTestingSessions){
            if (profileInTestingSession.getTestability()== Testability.TESTABLE){
                nbTestable++;
            }
            if (profileInTestingSession.getTestability()==Testability.DROPPED){
                nbDropped++;
            }
            if (profileInTestingSession.getTestability()==Testability.FEW_PARTNERS){
                nbFewPartners++;
            }
            if (profileInTestingSession.getTestability() == Testability.DECISION_PENDING){
                nbDecisionPending++;
            }
        }

        return new ProfileInTestingSessionStats(nbTestable,nbDropped,nbFewPartners,nbDecisionPending);
    }


    @Override
    public int getNumberOfSessionParticipants() {
        return statisticsCommonDao.getNumberOfSessionParticipants(testingSessionService.getUserTestingSession());
    }

    @Override
    public int getNumberOfRegisteredInstitutionWithAttendee() {
        return statisticsCommonDao.getNumberOfRegisteredInstitutionWithAttendee(testingSessionService.getUserTestingSession());
    }

    @Override
    public int getNumberOfRegisteredInstitutionWithSystem() {
        return statisticsCommonDao.getNumberOfRegisteredInstitutionWithSystem(testingSessionService.getUserTestingSession());
    }

    @Override
    public int getNumberOfRegisteredSystemInSession() {
        return statisticsCommonDao.getNumberOfRegisteredSystemInSession(testingSessionService.getUserTestingSession());
    }

    @Override
    public int getNumberOfIntegrationProfilesForCurrentSession() {
        return statisticsCommonDao.getNumberOfIntegrationProfilesForCurrentSession(testingSessionService.getUserTestingSession());
    }

    @Override
    public int getNumberOfDomainsForCurrentSession() {
        return statisticsCommonDao.getNumberOfDomainForCurrentSession(testingSessionService.getUserTestingSession());
    }

    @Override
    public TestInstanceStats getStatisticsForTestInstances(List<TestInstance> testInstances) {
        int nbVerified = 0;
        int nbFailed = 0;
        int nbInProgress = 0;
        int nbToBeVerified = 0;
        int nbPartiallyVerified = 0;
        int nbCritical = 0;
        if (!testInstances.isEmpty()) {


            for (TestInstance testInstance : testInstances) {
                nbVerified = getNbVerified(nbVerified, testInstance);
                nbFailed = getNbFailed(nbFailed, testInstance);
                nbInProgress = getNbInProgress(nbInProgress, testInstance);
                nbToBeVerified = getNbToBeVerified(nbToBeVerified, testInstance);
                nbPartiallyVerified = getNbPartiallyVerified(nbPartiallyVerified, testInstance);
                nbCritical = getNbCritical(nbCritical, testInstance);
            }

        }
        return new TestInstanceStats(nbVerified, nbFailed, nbInProgress, nbToBeVerified, nbPartiallyVerified, nbCritical);
    }

    private int getNbCritical(int nbCritical, TestInstance testInstance) {
        if (testInstance.getLastStatus() == Status.CRITICAL) {
            nbCritical++;
        }
        return nbCritical;
    }

    private int getNbPartiallyVerified(int nbPartiallyVerified, TestInstance testInstance) {
        if (testInstance.getLastStatus() == Status.PARTIALLY_VERIFIED) {
            nbPartiallyVerified++;
        }
        return nbPartiallyVerified;
    }

    private int getNbToBeVerified(int nbToBeVerified, TestInstance testInstance) {
        if (testInstance.getLastStatus() == Status.COMPLETED) {
            nbToBeVerified++;
        }
        return nbToBeVerified;
    }

    private int getNbInProgress(int nbInProgress, TestInstance testInstance) {
        if (testInstance.getLastStatus() == Status.STARTED || testInstance.getLastStatus() == Status.PAUSED) {
            nbInProgress++;
        }
        return nbInProgress;
    }

    private int getNbFailed(int nbFailed, TestInstance testInstance) {
        if (testInstance.getLastStatus() == Status.FAILED || testInstance.getLastStatus() == Status.COMPLETED_ERRORS) {
            nbFailed++;
        }
        return nbFailed;
    }

    private int getNbVerified(int nbVerified, TestInstance testInstance) {
        if (testInstance.getLastStatus() == Status.VERIFIED || testInstance.getLastStatus() == Status.SELF_VERIFIED) {
            nbVerified++;
        }
        return nbVerified;
    }


}
