package net.ihe.gazelle.tm.testexecution;

import net.ihe.gazelle.tm.gazelletest.model.instance.StatusResults;

public enum SUTCapabilityStatus {

    UNSPECIFIED("", "gazelle.tm.testing.results.unspecified", "", 1),
    AT_RISK("atrisk", "gazelle.tm.testing.results.resultStatusAtRisk", "gzl-label gzl-label-orange", 2),
    DID_NOT_COMPLETE("failed", "gazelle.tm.testing.results.resultStatusFailed", "gzl-label gzl-label-red", 3),
    OPTION_NOT_GRADED("nograding", "gazelle.tm.testing.results.noGrading", "gzl-label gzl-label-grey", 4),
    NO_PEER("nopeer", "gazelle.tm.testing.results.noPeer", "gzl-label gzl-label-grey", 5),
    WITHDRAWN("withdrawn", "gazelle.tm.testing.results.resultStatusWithDrawn", "gzl-label gzl-label-grey", 6),
    PASSED("passed", "net.ihe.gazelle.tm.Passed", "gzl-label gzl-label-green", 10);
    private String labelToDisplay;
    private String cssStyle;
    private String keyword;
    private int rank;

    SUTCapabilityStatus(String keyword, String labelToDisplay, String cssStyle, int rank) {
        this.keyword = keyword;
        this.labelToDisplay = labelToDisplay;
        this.cssStyle = cssStyle;
        this.rank = rank;
    }

    public String getLabelToDisplay() {
        return labelToDisplay;
    }

    public static SUTCapabilityStatus getStatusByStatusResults(StatusResults statusResults) {
        if (statusResults != null) {
            SUTCapabilityStatus[] sutCapabilityStatuses = values();
            for (int i = 0; i < sutCapabilityStatuses.length; i++) {
                if (sutCapabilityStatuses[i].getKeyword().equals(statusResults.getKeyword())) {
                    return sutCapabilityStatuses[i];
                }
            }
        }
        return UNSPECIFIED;
    }

    public String getStatusStyleClass() {
        return cssStyle;
    }

    public String getKeyword() {
        return this.keyword;
    }

    public int getRank() {
        return rank;
    }

    public boolean isUnspecified() {
        return this.keyword.equals(UNSPECIFIED.getKeyword());
    }

}
