package net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.script.dto;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonSetter;
import net.ihe.gazelle.tm.gazelletest.automatedstep.domain.script.InputDefinition;
import net.ihe.gazelle.tm.gazelletest.automatedstep.domain.script.Script;
import net.ihe.gazelle.tm.gazelletest.automatedstep.domain.script.Step;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@JsonPropertyOrder({"name", "inputs", "steps"})
public class ScriptDTO implements Serializable {

    private static final long serialVersionUID = -265774528156078850L;

    @JsonIgnore
    private Script script;

    public ScriptDTO() {
        this.script = new Script();
    }

    public ScriptDTO(Script script) {
        this.script = script;
    }

    @JsonGetter("name")
    public String getName() {
        return script.getName();
    }

    @JsonSetter("name")
    public void setName(String name) {
        script.setName(name);
    }

    @JsonGetter("inputs")
    public List<InputDefinitionDTO> getInputs() {
        List<InputDefinitionDTO> inputs = new ArrayList<>();
        for (InputDefinition inputDefinition : script.getInputs()) {
            inputs.add(new InputDefinitionDTO(inputDefinition));
        }
        return inputs;
    }

    @JsonSetter("inputs")
    public void setInputs(List<InputDefinitionDTO> inputDefinitionDTOList) {
        List<InputDefinition> inputDefinitions = new ArrayList<>();
        for (InputDefinitionDTO inputDefinitionDTO : inputDefinitionDTOList) {
            inputDefinitions.add(inputDefinitionDTO.getInput());
        }
        script.setInputs(inputDefinitions);
    }

    @JsonGetter("steps")
    public List<StepDTO> getSteps() {
        List<StepDTO> steps = new ArrayList<>();
        for (Step step : script.getSteps()) {
            steps.add(new StepDTO(step));
        }
        return steps;
    }

    @JsonSetter("steps")
    public void setSteps(List<StepDTO> stepDTOList) {
        List<Step> steps = new ArrayList<>();
        for (StepDTO stepDTO : stepDTOList) {
            steps.add(stepDTO.getStep());
        }
        script.setSteps(steps);
    }

    @JsonIgnore
    public Script getScript() {
        return script;
    }

    @JsonIgnore
    public void setScript(Script script) {
        this.script = script;
    }
}
