package net.ihe.gazelle.tm.organization;

import net.ihe.gazelle.ssov7.authn.interlay.adapter.GazelleIdentityImpl;
import net.ihe.gazelle.ssov7.gum.client.application.Role;
import net.ihe.gazelle.ssov7.m2m.server.domain.M2MAuthorization;

public class M2MOrganizationAuthorization {


    public static final M2MAuthorization ANY_ORGA_CREATE = new M2MAuthorization() {
        @Override
        public boolean isGranted(Object... scope) {
            GazelleIdentityImpl instance = GazelleIdentityImpl.instance();
            return instance.isLoggedIn() && instance.hasRole(Role.ADMIN);
        }
    };

    public static final M2MAuthorization ANY_ORGA_READ_PRIVATE = new M2MAuthorization() {
        @Override
        public boolean isGranted(Object... scope) {
            GazelleIdentityImpl instance = GazelleIdentityImpl.instance();
            return instance.isLoggedIn() && instance.hasRole(Role.ADMIN);
        }
    };

    public static final M2MAuthorization ANY_ORGA_READ_PUBLIC = new M2MAuthorization() {
        @Override
        public boolean isGranted(Object... scope) {
            GazelleIdentityImpl instance = GazelleIdentityImpl.instance();
            return instance.isLoggedIn() && instance.hasRole(Role.ADMIN);
        }
    };

    public static final M2MAuthorization ANY_ORGA_UPDATE = new M2MAuthorization() {
        @Override
        public boolean isGranted(Object... scope) {
            GazelleIdentityImpl instance = GazelleIdentityImpl.instance();
            return instance.isLoggedIn() && instance.hasRole(Role.ADMIN);
        }
    };

    public static final M2MAuthorization MY_ORGA_READ = new M2MAuthorization() {
        @Override
        public boolean isGranted(Object... scope) {
            GazelleIdentityImpl instance = GazelleIdentityImpl.instance();
            return instance.isLoggedIn() && instance.hasRole(Role.ADMIN);
        }
    };

    public static final M2MAuthorization MY_ORGA_UPDATE = new M2MAuthorization() {
        @Override
        public boolean isGranted(Object... scope) {
            GazelleIdentityImpl instance = GazelleIdentityImpl.instance();
            return instance.isLoggedIn() && instance.hasRole(Role.ADMIN);
        }
    };

    private M2MOrganizationAuthorization() {
        // Hide constructor, this class is exclusively composed of static members.
    }
}
