package net.ihe.gazelle.tm.testexecution;

import java.util.List;
import java.util.Objects;

public class SUTCapabilityReport {
    private String id;
    private int systemAIPOResultForATestingSessionId;
    private String sutKeyword;
    private String profile;
    private String actor;
    private String option;
    private SUTCapabilityStatus status;
    private TestingDepthStatus testingDepth;
    private List<TestReport> testReports;

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getActor() {
        return actor;
    }

    public void setActor(String actor) {
        this.actor = actor;
    }

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }

    public SUTCapabilityStatus getStatus() {
        return status;
    }

    public void setStatus(SUTCapabilityStatus status) {
        this.status = status;
    }

    public TestingDepthStatus getTestingDepth() {
        return testingDepth;
    }

    public void setTestingDepth(TestingDepthStatus testingDepth) {
        this.testingDepth = testingDepth;
    }

    public List<TestReport> getTestReports() {
        return testReports;
    }

    public void setTestReports(List<TestReport> testReports) {
        this.testReports = testReports;
    }

    public String getSutKeyword() {
        return sutKeyword;
    }

    public void setSutKeyword(String sutKeyword) {
        this.sutKeyword = sutKeyword;
    }

    public int getSystemAIPOResultForATestingSessionId() {
        return systemAIPOResultForATestingSessionId;
    }

    public void setSystemAIPOResultForATestingSessionId(int systemAIPOResultForATestingSessionId) {
        this.systemAIPOResultForATestingSessionId = systemAIPOResultForATestingSessionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SUTCapabilityReport that = (SUTCapabilityReport) o;
        return Objects.equals(sutKeyword, that.sutKeyword)
                && Objects.equals(profile, that.profile)
                && Objects.equals(actor, that.actor)
                && Objects.equals(option, that.option);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sutKeyword, profile, actor, option);
    }

    public String getId() {
        if (this.id == null){
            this.id = Integer.toString(hashCode());
        }
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
