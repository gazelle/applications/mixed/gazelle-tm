package net.ihe.gazelle.tm.filter.valueprovider;

import net.ihe.gazelle.hql.criterion.ValueProvider;
import net.ihe.gazelle.menu.Authorizations;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import net.ihe.gazelle.ssov7.gum.client.application.Role;
import net.ihe.gazelle.users.model.Institution;
import org.jboss.seam.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class InstitutionFixer implements ValueProvider {

   public static final InstitutionFixer INSTANCE = new InstitutionFixer();
   private static final Logger LOG = LoggerFactory.getLogger(InstitutionFixer.class);
   private static final long serialVersionUID = 4889171421799551325L;

   @Override
   public Object getValue() {
      LOG.trace("getValue");
      GazelleIdentity identity = (GazelleIdentity) Component.getInstance("org.jboss.seam.security.identity");
      if (identity.isLoggedIn()) {
         if (identity.hasRole(Role.ADMIN) ||
               identity.hasRole(Role.MONITOR) ||
               identity.hasRole(Role.ACCOUNTING) ||
               Authorizations.TESTING_SESSION_ADMIN_OF_CURRENT_TESTING_SESSION.isGranted()) {
            return null;
         }
         return Institution.findInstitutionWithKeyword(
               identity.getOrganisationKeyword()
         );
      }
      return null;
   }
}
