package net.ihe.gazelle.tm.gazelletest.action;

import java.io.Serializable;

public class EmptyFileException extends Exception implements Serializable {
    public EmptyFileException() {
    }

    public EmptyFileException(String s) {
        super(s);
    }

    public EmptyFileException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public EmptyFileException(Throwable throwable) {
        super(throwable);
    }
}
