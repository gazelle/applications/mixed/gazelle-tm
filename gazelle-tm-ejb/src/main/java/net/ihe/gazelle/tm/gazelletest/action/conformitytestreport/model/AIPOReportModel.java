package net.ihe.gazelle.tm.gazelletest.action.conformitytestreport.model;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "aipo")
@XmlAccessorType(XmlAccessType.NONE)
public class AIPOReportModel {

    @XmlElement(name = "integrationProfile")
    private DataOfTFObject actorIntegrationProfile;

    @XmlElement(name = "actor")
    private DataOfTFObject actor;

    @XmlElement(name = "option")
    private DataOfTFObject actorIntegrationProfileOption;

    @XmlElementWrapper(name = "testReports")
    @XmlElements({@XmlElement(name = "metaTest", type = MetaTestReport.class), @XmlElement(name = "test", type = TestReportItem.class)})
    private List<AbstractTestReportModel> testReportList = new ArrayList<>();

    /**
     * Getter of the profile
     *
     * @return DataOfTFObject
     */
    public DataOfTFObject getActorIntegrationProfile() {
        return actorIntegrationProfile;
    }

    /**
     * Setter of the profile
     *
     * @param actorIntegrationProfile actor
     */
    public void setActorIntegrationProfile(DataOfTFObject actorIntegrationProfile) {
        this.actorIntegrationProfile = actorIntegrationProfile;
    }

    /**
     * Getter of Actor
     *
     * @return DataOfTFObject
     */
    public DataOfTFObject getActor() {
        return actor;
    }

    /**
     * Setter for actor
     *
     * @param actor infos
     */
    public void setActor(DataOfTFObject actor) {
        this.actor = actor;
    }

    /**
     * Getter of Option
     *
     * @return DataOfTFObject
     */
    public DataOfTFObject getActorIntegrationProfileOption() {
        return actorIntegrationProfileOption;
    }

    /**
     * Setter of option
     *
     * @param actorIntegrationProfileOption options
     */
    public void setActorIntegrationProfileOption(DataOfTFObject actorIntegrationProfileOption) {
        this.actorIntegrationProfileOption = actorIntegrationProfileOption;
    }

    /**
     * Getter of the list of test-reports
     *
     * @return List
     */
    public List<AbstractTestReportModel> getTestReportList() {
        return new ArrayList<>(testReportList);
    }

    /**
     * Setter of the list of test reports
     *
     * @param testReportList reports
     */
    public void setTestReportList(List<AbstractTestReportModel> testReportList) {
        this.testReportList = new ArrayList<>(testReportList);
    }

}

