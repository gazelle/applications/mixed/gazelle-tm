package net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.execution.service;

public class NotRunningException extends RuntimeException {

    public NotRunningException() {
    }

    public NotRunningException(String message) {
        super(message);
    }

    public NotRunningException(String message, Throwable throwable) {
        super(message, throwable);
    }

    public NotRunningException(Throwable throwable) {
        super(throwable);
    }
}
