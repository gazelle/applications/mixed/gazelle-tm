package net.ihe.gazelle.tm.gazelletest.action.conformitytestreport.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@XmlRootElement(name = "testInstance")
@XmlAccessorType(XmlAccessType.NONE)
public class TestInstanceReport {

    @XmlElement(name = "instanceKeyword")
    private String keyword;

    @XmlElement(name = "id")
    private String testInstanceId;

    @XmlElement(name = "monitorName")
    private String monitorName;

    @XmlElement(name = "permalink")
    private String permalink;

    @XmlElement(name = "result")
    private String result;

    @XmlElement(name = "startDate")
    private Date startDate;

    @XmlElement(name = "lastModifiedDate")
    private Date lastModifiedDate;

    /**
     * Constructor
     */
    public TestInstanceReport() {
    }

    public TestInstanceReport(String keyword, String testInstanceId, String monitorName, String result, Date startDate, Date lastModifiedDate) {
        setKeyword(keyword);
        setTestInstanceId(testInstanceId);
        setMonitorName(monitorName);
        setResult(result);
        setStartDate(startDate);
        setLastModifiedDate(lastModifiedDate);
    }


    /**
     * Getter of tet keyword
     *
     * @return String
     */
    public String getKeyword() {
        return keyword;
    }

    /**
     * Setter of test keyword
     *
     * @param keyword of instance
     */
    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    /**
     * Getter instance Id
     *
     * @return String
     */
    public String getTestInstanceId() {
        return testInstanceId;
    }

    /**
     * Setter of the Id of the test instance
     *
     * @param testInstanceId of the instance
     */
    public void setTestInstanceId(String testInstanceId) {
        this.testInstanceId = testInstanceId;
    }

    /**
     * Getter Monitor Name
     *
     * @return String
     */
    public String getMonitorName() {
        return monitorName;
    }

    /**
     * Setter of monitor name
     *
     * @param monitorName of the instance
     */
    public void setMonitorName(String monitorName) {
        this.monitorName = monitorName;
    }

    /**
     * Getter of result
     *
     * @return result value
     */
    public String getResult() {
        return result;
    }

    /**
     * Setter of Result Value
     *
     * @param result String
     */
    public void setResult(String result) {
        this.result = result;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public void setPermalink(String permalink) {
        this.permalink = permalink;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }
}
