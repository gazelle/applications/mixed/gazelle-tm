package net.ihe.gazelle.tm.organization;

import net.ihe.gazelle.ssov7.gum.client.interlay.ws.OrganizationResource;
import net.ihe.gazelle.tm.organization.exception.OrganizationCreationException;
import net.ihe.gazelle.tm.organization.exception.OrganizationServiceDAOException;
import net.ihe.gazelle.tm.organization.exception.OrganizationServiceException;
import net.ihe.gazelle.users.model.*;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

@Name("organizationService")
@AutoCreate
@Scope(ScopeType.STATELESS)
public class OrganizationServiceImpl implements OrganizationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(OrganizationServiceImpl.class);
    private static final String UNKNOWN = "unknown";

    @In(value = "organizationServiceDAO", create = true)
    private OrganizationServiceDAO organizationServiceDAO;

    public OrganizationServiceImpl() {
    }

    public OrganizationServiceImpl(OrganizationServiceDAO organizationServiceDAO) {
        this.organizationServiceDAO = organizationServiceDAO;
    }

    @Override
    public boolean isOrganizationRegistrationCompleted(Institution institution) {
        LOGGER.trace("isOrganizationRegistrationCompleted");
        if (institution == null || institution.getContacts() == null) {
            throw new IllegalArgumentException("Institution must be defined with the following elements: contacts");
        }
        List<PersonFunction> requiredRoles = organizationServiceDAO.getRequiredRolesToCompleteRegistration();
        return !institution.getContacts().isEmpty()
                && organizationServiceDAO.hasOrganisationBillingContact(institution)
                && areRequiredRolesExist(institution.getContacts(), requiredRoles);
    }

    @Override
    public boolean isOrganizationDelegated(Institution institution) {
        LOGGER.trace("isOrganizationDelegated");
        if (institution == null) {
            throw new IllegalArgumentException("Institution must not be null.");
        }
        try {
            return Objects.equals(organizationServiceDAO.getDelegatedOrganizationById(institution.getId()).getKeyword(), institution.getKeyword());
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    @Override
    public List<Institution> searchForOrganizations(String organizationName, String externalId, String idpId, Boolean delegated) {
        LOGGER.trace("searchForOrganizations");
        if (organizationName != null && externalId == null && idpId == null && delegated == null) {
            return searchForOrganizationsByName(organizationName);
        } else {
            return searchForOrganizationByExternalParams(externalId, idpId, delegated);
        }
    }

    private List<Institution> searchForOrganizationsByName(String organizationName) {
        List<Institution> institutions = new ArrayList<>();
        Institution institution = organizationServiceDAO.findOrganisationByName(organizationName);
        if (institution != null)
            institutions.add(institution);

        return institutions;
    }

    private List<Institution> searchForOrganizationByExternalParams(String externalId, String idpId, Boolean delegated) {
        List<Institution> allInstitution = organizationServiceDAO.getAllInstitution();
        List<Institution> filteredInstitutions = new LinkedList<>();

        for (Institution institution : allInstitution) {
            // If organization is missing keyword we skip it
            if (institution.getKeyword() == null || institution.getKeyword().equals("NULL")) {
                continue;
            }

            if (institutionMatchSearch(institution, externalId, idpId, delegated)) {
                filteredInstitutions.add(institution);
            }
        }
        return filteredInstitutions;
    }

    private boolean institutionMatchSearch(Institution institution, String externalId, String idpId, Boolean delegated) {
        if (isDelegated(institution)) {
            DelegatedOrganization delegatedInstitution = (DelegatedOrganization) institution;
            if (delegated == null || delegated) {
                return (externalId == null && idpId == null) ||
                        (externalId == null && delegatedInstitution.getIdpId().equals(idpId)) ||
                        (idpId == null && delegatedInstitution.getExternalId().equals(externalId)) ||
                        (delegatedInstitution.getExternalId().equals(externalId) && delegatedInstitution.getIdpId().equals(idpId));
            }
        } else {
            if (delegated == null || !delegated)
                return externalId == null && idpId == null;
        }
        return false;
    }

    @Override
    public Institution getCurrentOrganization() {
        LOGGER.trace("getCurrentOrganization");
        return organizationServiceDAO.getCurrentOrganization();
    }

    @Override
    public Institution getOrganizationById(String organizationId) {
        LOGGER.trace("getOrganizationById");
        if (organizationId == null)
            throw new IllegalArgumentException("organizationId is null");
        return organizationServiceDAO.getInstitutionWithKeyword(organizationId);
    }

    @Override
    public Institution findOrganizationByName(String name) {
        LOGGER.trace("getOrganizationByName");
        return organizationServiceDAO.findOrganisationByName(name);
    }

    @Override
    public DelegatedOrganization getDelegatedOrganizationByKeyword(String keyword) {
        LOGGER.trace("isDelegatedOrganizationExist");
        if (keyword == null)
            throw new IllegalArgumentException("The keyword shall not be null");
        return organizationServiceDAO.getDelegatedOrganizationByKeyword(keyword);
    }

    @Override
    public OrganizationResource getOrganizationresource(Institution institution) {
        if (institution == null) {
            throw new IllegalArgumentException("Organization is null");
        }
        if (isDelegated(institution)) {
            DelegatedOrganization delegatedOrganization = (DelegatedOrganization) institution;
            return new OrganizationResource(delegatedOrganization.getKeyword(), delegatedOrganization.getName(), delegatedOrganization.getUrl(), delegatedOrganization.getExternalId(), delegatedOrganization.getIdpId());
        }
        return new OrganizationResource(institution.getKeyword(), institution.getName(), institution.getUrl());
    }

    @Override
    public void createOrganization(OrganizationResource organizationResource) {
        if (organizationResource.getId() == null || organizationResource.getName() == null || organizationResource.getUrl() == null) {
            throw new IllegalArgumentException("Some of the Organization fields are null.");
        }
        checkIfOrganizationAlreadyExistWithKeyword(organizationResource);
        if (organizationResource.isDelegated()) {
            createDelegatedOrganization(organizationResource);
        } else {
            createLocalOrganization(organizationResource);
        }
    }


    private void createLocalOrganization(OrganizationResource organizationResource) {
        LOGGER.trace("createLocalOrganization");
        String addressString = "default address";

        // If the institution does not exist, we create it
        Institution selectedInstitution = new Institution(organizationResource.getName());

        // Mock Address and address list
        Address address = new Address();
        address.setAddress(addressString);
        address.setAddressLine2(addressString);
        List<Address> addresses = new ArrayList<>();
        addresses.add(address);

        selectedInstitution.setMailingAddress(address);
        selectedInstitution.setAddresses(addresses);
        selectedInstitution.setKeyword(organizationResource.getId());
        selectedInstitution.setActivated(true);
        selectedInstitution.setUrl(organizationResource.getUrl());
        try {
            organizationServiceDAO.createInstitution(selectedInstitution, address);
        } catch (OrganizationServiceDAOException e) {
            throw new OrganizationServiceException(e.getMessage());
        }
    }

    private void checkIfOrganizationAlreadyExistWithKeyword(OrganizationResource organizationResource) {
        if (organizationServiceDAO.isOrganizationExisting(organizationResource.getId())) {
            throw new OrganizationCreationException(String.format("The organization with keyword %s, already exists.", organizationResource.getId()));
        }
    }


    private void createDelegatedOrganization(OrganizationResource organizationResource) {
        LOGGER.trace("createDelegatedOrganization");
        // If the delegated organization does not exist, we create it.
        DelegatedOrganization delegatedOrganization = new DelegatedOrganization(organizationResource.getName(), organizationResource.getExternalId(), organizationResource.getId(), "https://ihe-europe.net/", organizationResource.getIdpId());
        delegatedOrganization.setActivated(true);
        Institution tmOrganization;

        // Persist organization in TM first.
        Address address = new Address();
        address.setAddress(UNKNOWN);
        address.setAddressLine2(UNKNOWN);
        address.setState(UNKNOWN);
        address.setCity(UNKNOWN);
        organizationServiceDAO.createInstitution(delegatedOrganization, address);
        tmOrganization = Institution.findInstitutionWithKeyword(delegatedOrganization.getKeyword());
        if (tmOrganization == null) {
            throw new NoSuchElementException(String.format("Can't find organization with keyword %s", organizationResource.getId()));
        }
        // Persist delegated organization.
        delegatedOrganization.setId(tmOrganization.getId());
        try {
            organizationServiceDAO.createDelegatedOrganization(delegatedOrganization);
        } catch (OrganizationServiceDAOException e) {
            throw new OrganizationServiceException(e.getMessage());
        }
    }

    @Override
    public Institution updateOrganization(String keyword, Institution institution) {
        if (keyword == null || institution == null)
            throw new IllegalArgumentException("Organization " + (keyword == null ? "keyword" : "organization resource") + " must not be null ");

        Institution organizationToUpdate = organizationServiceDAO.getInstitutionWithKeyword(keyword);
        Institution updatedInstitution = null;
        try {
            if (institution.getName() != null && !organizationToUpdate.getName().equals(institution.getName())) {
                updatedInstitution = organizationServiceDAO.updateOrganizationName(organizationToUpdate, institution.getName());
                organizationToUpdate = updatedInstitution;
            }
            if (isDelegated(institution) && isLocal(organizationToUpdate)) {
                institution.setId(organizationToUpdate.getId());
                updatedInstitution = organizationServiceDAO.migrateLocalOrganizationToDelegated(institution);
            }
            return updatedInstitution == null ? organizationToUpdate : updatedInstitution;
        } catch (OrganizationServiceDAOException e) {
            throw new OrganizationServiceException(e.getMessage());
        }
    }

    @Override
    public boolean isThereAtLeastOneOrganization() {
        return organizationServiceDAO.isThereAtLeastOneOrganization();
    }

    private boolean isLocal(Institution organizationToUpdate) {
        return !isDelegated(organizationToUpdate);
    }

    private boolean isDelegated(Institution institution) {
        return institution instanceof DelegatedOrganization;
    }

    private boolean areRequiredRolesExist(Set<Person> contacts, List<PersonFunction> roles) {
        LOGGER.trace("areRequiredRolesExist");
        Set<PersonFunction> allRoles = new HashSet<>();
        for (Person contact : contacts) {
            allRoles.addAll(contact.getPersonFunction());
        }
        for (PersonFunction role : roles) {
            if (!allRoles.contains(role)) {
                return false;
            }
        }
        return true;
    }
}