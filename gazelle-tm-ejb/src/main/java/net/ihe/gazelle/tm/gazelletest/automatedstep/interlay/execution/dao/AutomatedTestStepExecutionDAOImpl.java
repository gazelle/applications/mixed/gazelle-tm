package net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.execution.dao;

import net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.execution.service.AutomatedTestInstanceStatusMessageSource;
import net.ihe.gazelle.tm.gazelletest.domain.AutomatedTestStepExecution;
import net.ihe.gazelle.tm.gazelletest.domain.AutomatedTestStepReportFile;
import net.ihe.gazelle.tm.gazelletest.model.instance.*;
import net.ihe.gazelle.tm.messages.NotificationService;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

@Name("automatedTestStepExecutionDAO")
@Scope(ScopeType.EVENT)
@AutoCreate
public class AutomatedTestStepExecutionDAOImpl implements AutomatedTestStepExecutionDAO {

    @In
    private EntityManager entityManager;

    @Override
    public void createExecution(AutomatedTestStepExecution execution) {
        AutomatedTestStepExecutionEntity entity = new AutomatedTestStepExecutionEntity(execution);
        entity = entityManager.merge(entity);
        TestStepsInstanceQuery testStepsInstanceQuery = new TestStepsInstanceQuery();
        testStepsInstanceQuery.id().eq(execution.getTestStepsInstanceId());
        TestStepsInstance testStepsInstance = testStepsInstanceQuery.getUniqueResult();
        testStepsInstance.setExecutionEntity(entity);
        entityManager.merge(testStepsInstance);
        entityManager.flush();
    }

    @Override
    public void updateExecution(AutomatedTestStepExecution execution) {
        AutomatedTestStepExecutionEntityQuery query = new AutomatedTestStepExecutionEntityQuery();
        query.testStepsInstanceId().eq(execution.getTestStepsInstanceId());
        AutomatedTestStepExecutionEntity entity = query.getUniqueResult();
        if (entity != null) {
            if (execution.getStatus() != null) {
                entity.setStatus(execution.getStatus());
            }
            if (execution.getUserId() != null) {
                entity.setUserId(execution.getUserId());
            }
            if (execution.getDate() != null) {
                entity.setDate(execution.getDate());
            }
            if (execution.getJsonReport() != null) {
                entity.setReport(execution.getJsonReport());
            }
            if (execution.getPdfFiles() != null) {
                clearOldReportFiles(entity);
                List<AutomatedTestStepReportFileEntity> reportFileEntityList = new ArrayList<>();
                for (AutomatedTestStepReportFile reportFile : execution.getPdfFiles()) {
                    AutomatedTestStepReportFileEntity reportFileEntity = new AutomatedTestStepReportFileEntity(reportFile);
                    reportFileEntity.setExecution(entity);
                    entityManager.persist(reportFileEntity);
                    reportFileEntityList.add(reportFileEntity);
                }
                entity.setFiles(reportFileEntityList);
            }
            entityManager.merge(entity);
            entityManager.flush();
        }
    }

    private void clearOldReportFiles(AutomatedTestStepExecutionEntity entity) {
        if (entity.getFiles() != null && !entity.getFiles().isEmpty()) {
            for (AutomatedTestStepReportFileEntity reportEntity : entity.getFiles()) {
                entityManager.remove(reportEntity);
            }
            entity.getFiles().clear();
        }
    }

    @Override
    public TestStepsInstance updateTestStepInstanceStatus(Integer testStepInstanceId, TestStepsInstanceStatus status) {
        TestStepsInstanceQuery query = new TestStepsInstanceQuery();
        query.id().eq(testStepInstanceId);
        TestStepsInstance stepInstance = query.getUniqueResult();
        stepInstance.setTestStepsInstanceStatus(status);
        stepInstance = entityManager.merge(stepInstance);
        entityManager.flush();
        return stepInstance;
    }

    @Override
    public void sendNotification(String userId, AutomatedTestStepExecution execution, TestStepsInstance testStepsInstance) {
        TestInstance testInstance = testStepsInstance.getTestInstance();
        NotificationService.dispatchNotification(
                new AutomatedTestInstanceStatusMessageSource(),
                userId,
                testStepsInstance.getTestInstance(),
                execution.getStatus().getLabel(),
                testInstance.getId().toString(),
                testStepsInstance.getTestSteps().getStepIndex().toString()
        );
    }

    @Override
    public AutomatedTestStepExecution getExecution(Integer testStepInstanceId) {
        AutomatedTestStepExecutionEntityQuery query = new AutomatedTestStepExecutionEntityQuery();
        query.testStepsInstanceId().eq(testStepInstanceId);
        AutomatedTestStepExecutionEntity entity = query.getUniqueResult();
        return entity != null ?  entity.asDomain() : null;
    }
}
