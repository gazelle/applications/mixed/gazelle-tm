package net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.execution.dao;

import net.ihe.gazelle.tm.gazelletest.domain.AutomatedTestStepExecution;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestStepsInstance;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestStepsInstanceStatus;

public interface AutomatedTestStepExecutionDAO {

    /**
     * Create a new execution on database
     * @param execution the execution to create
     */
    void createExecution(AutomatedTestStepExecution execution);

    /**
     * Update an existing execution in database
     * @param execution the value to update (all non-null fields)
     */
    void updateExecution(AutomatedTestStepExecution execution);

    /**
     * Update the test step instance status depending on the received execution status
     * @param testStepInstanceId the id of the test step instance
     * @param status the test step instance status to put
     * @return the update test step instance
     */
    TestStepsInstance updateTestStepInstanceStatus(Integer testStepInstanceId, TestStepsInstanceStatus status);

    /**
     * Send notification message to all concerned users
     * @param userId the id of the user that send this notification
     * @param execution received execution result and report
     * @param testStepsInstance the id of the test step instance
     */
    void sendNotification(String userId, AutomatedTestStepExecution execution, TestStepsInstance testStepsInstance);

    /**
     * Get an execution from the is of the test step instance linked to it
     * @param testStepInstanceId The id of the test step instance linked to the execution
     * @return The execution filtered with those criteria
     */
    AutomatedTestStepExecution getExecution(Integer testStepInstanceId);
}
