package net.ihe.gazelle.tm.financial.action.services;

import net.ihe.gazelle.tm.financial.FinancialSummary;
import net.ihe.gazelle.tm.financial.FinancialSummaryOneSystem;
import net.ihe.gazelle.tm.financial.action.FinancialCalcWithVatStatusRecalculation;
import net.ihe.gazelle.tm.financial.model.Invoice;
import net.ihe.gazelle.tm.systems.model.SystemInSession;
import net.ihe.gazelle.tm.systems.model.SystemInSessionRegistrationStatus;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import net.ihe.gazelle.tm.users.model.ConnectathonParticipant;
import net.ihe.gazelle.users.model.Institution;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


@Name(value = "feesService")
@Scope(ScopeType.STATELESS)
public class FeesServiceImpl implements FeesService{

    @Override
    public FinancialSummary getFees(TestingSession testingSession, Institution institution) {
        if(testingSession == null){
            return null;
        }
        if(institution == null){
            return null;
        }
        List<ConnectathonParticipant> participants = ConnectathonParticipant.filterConnectathonParticipant(null,
                institution, testingSession);
        List<SystemInSession> systemsInSession = SystemInSession.getSystemsInSessionForCompanyForSession(null, institution,
                testingSession);
        BigDecimal systemFee;
        List<FinancialSummaryOneSystem> financialSummariesOneSystem = new ArrayList<>();
        boolean first = true;
        for (SystemInSession systemInSession : systemsInSession) {
            if (!SystemInSessionRegistrationStatus.DROPPED.name().equals(systemInSession.getRegistrationStatus().name())){
                if (first) {
                    systemFee = testingSession.getFeeFirstSystem();
                } else {
                    systemFee = testingSession.getFeeAdditionalSystem();
                }
                financialSummariesOneSystem.add(getFinancialSummaryOneSystem(systemInSession, systemFee));
                first = false;
            }
        }
        BigDecimal participantFee = testingSession.getFeeParticipant();
        Invoice invoice = Invoice.getInvoiceForAnInstitutionAndATestingSession(null, institution, testingSession);
        if(invoice == null){
            return null;
        }
        FinancialSummary financialSummary = getInitialFinancialSummaryFromSIS(financialSummariesOneSystem,
                participants, participantFee, testingSession, invoice);
        FinancialCalcWithVatStatusRecalculation financialCalc = new FinancialCalcWithVatStatusRecalculation();
        invoice = financialCalc.calculateVatAmountForCompany(invoice,null);
        financialSummary.setInvoice(invoice);
        return financialSummary;
    }

    @Override
    public FinancialSummary getFees(Integer sessionId, Integer organizationId) {
        TestingSession testingSession = TestingSession.getSessionById(sessionId);
        Institution institution = Institution.findInstitutionWithId(organizationId);
        return getFees(testingSession, institution);

    }

    @Override
    public BigDecimal getTotalToPay(FinancialSummary financialSummary) {
        if(financialSummary == null || financialSummary.getInvoice() == null){
            return new BigDecimal(0);
        }
        TestingSession testingSession;
        if (financialSummary.getTestingSession() == null){
            if(financialSummary.getInvoice().getTestingSession() == null){
                return new BigDecimal(0);
            }
            else{
                testingSession = financialSummary.getInvoice().getTestingSession();
            }
        }
        else{
            testingSession = financialSummary.getTestingSession();
        }
        return testingSession.getFeeFirstSystem()
                .add(testingSession.getFeeAdditionalSystem()
                        .multiply(BigDecimal.valueOf(financialSummary.getFinancialSummaryOneSystems().size()-1)))
                .add(testingSession.getFeeParticipant()
                        .multiply(BigDecimal.valueOf(financialSummary.getNumberAdditionalParticipant(testingSession))))
                .add(financialSummary.getInvoice().getFeesDiscount().negate())
                .add(financialSummary.getInvoice().getVatAmount());
    }

    ////// Private methods //////

    private  FinancialSummaryOneSystem getFinancialSummaryOneSystem(SystemInSession systemInSession,
                                                                          BigDecimal systemFee) {
        FinancialSummaryOneSystem fsstmp = new FinancialSummaryOneSystem();
        if (!SystemInSessionRegistrationStatus.DROPPED.name().equals(systemInSession.getRegistrationStatus().name())){
            fsstmp.setSystemInSession(systemInSession);
        }
        if (!systemInSession.getSystem().getIsTool()) {
            fsstmp.setSystemFee(systemFee);
        }
        return fsstmp;
    }

    private  FinancialSummary getInitialFinancialSummaryFromSIS(List<FinancialSummaryOneSystem> financialSummariesOneSystem, List<ConnectathonParticipant> participants,
                                                                      BigDecimal participantFee,TestingSession activatedTestingSession, Invoice invoice) {
        FinancialSummary financialSummary = new FinancialSummary();
        financialSummary.setFinancialSummaryOneSystems(financialSummariesOneSystem);
        financialSummary.setFee(calculateTotalFee(financialSummariesOneSystem, participants
                ,participantFee, activatedTestingSession));
        financialSummary.setTestingSession(activatedTestingSession);
        financialSummary.setNumberParticipant(participants.size());
        financialSummary.setFeeVAT(invoice.getVatAmount());
        if (!invoice.getFeesDiscount().equals(new BigDecimal("0.00"))) {
            financialSummary.setFeeDiscount(invoice.getFeesDiscount());
            financialSummary.setIsDiscount(true);
            financialSummary.setReasonForDiscount(invoice.getReasonsForDiscount());
        }
        financialSummary.setTotalFeePaid(invoice.getFeesPaid());
        financialSummary.setTotalFeeDue(invoice.getFeesDue().setScale(2));
        financialSummary.setFeeVAT(invoice.getVatAmount());
        return financialSummary;
    }


    public static BigDecimal calculateTotalFee(List<FinancialSummaryOneSystem> financialSummariesOneSystem, List<ConnectathonParticipant> participants, BigDecimal participantFee, TestingSession activatedTestingSession) {
        BigDecimal res = new BigDecimal("0.00");
        for (FinancialSummaryOneSystem financialSummaryOneSystem : financialSummariesOneSystem) {
            if (!financialSummaryOneSystem.getSystemInSession().getSystem().getIsTool()) {
                res = res.add(financialSummaryOneSystem.getSystemFee());
            }
        }
        if (participants != null) {
            int numb = participants.size() - (financialSummariesOneSystem.size() * activatedTestingSession.getNbParticipantsIncludedInSystemFees());
            if (numb > 0) {
                res = res.add(participantFee.multiply(new BigDecimal(numb)));
            }
        }
        return res;
    }


}
