package net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.script.dto;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonSetter;
import net.ihe.gazelle.tm.gazelletest.automatedstep.domain.script.InputContent;
import net.ihe.gazelle.tm.gazelletest.automatedstep.domain.script.MaestroRequest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@JsonPropertyOrder({"test", "inputs"})
public class MaestroRequestDTO implements Serializable {

    private static final long serialVersionUID = 9175326331124582435L;

    @JsonIgnore
    private MaestroRequest maestroRequest;

    public MaestroRequestDTO() {
        this.maestroRequest = new MaestroRequest();
    }

    public MaestroRequestDTO(MaestroRequest maestroRequest) {
        this.maestroRequest = maestroRequest;
    }

    @JsonGetter("clientCallback")
    public String getClientCallback() {
        return maestroRequest.getClientCallback();
    }

    @JsonSetter("clientCallback")
    public void setClientCallback(String clientCallback) {
        maestroRequest.setClientCallback(clientCallback);
    }

    @JsonGetter("callbackAuthorization")
    public String getCallbackAuthorization() {
        return maestroRequest.getCallbackAuthorization();
    }

    @JsonSetter("callbackAuthorization")
    public void setCallbackAuthorization(String callbackAuthorization) {
        maestroRequest.setCallbackAuthorization(callbackAuthorization);
    }

    @JsonGetter("test")
    public ScriptDTO getScript() {
        return new ScriptDTO(maestroRequest.getScript());
    }

    @JsonSetter("test")
    public void setScript(ScriptDTO scriptDTO) {
        maestroRequest.setScript(scriptDTO.getScript());
    }

    @JsonGetter("inputs")
    public List<InputContentDTO> getInputs() {
        List<InputContentDTO> inputContentDTOList = new ArrayList<>();
        for (InputContent input : maestroRequest.getInputs()) {
            inputContentDTOList.add(new InputContentDTO(input));
        }
        return inputContentDTOList;
    }

    @JsonSetter("inputs")
    public void setInputs(List<InputContentDTO> inputContentDTOList) {
        List<InputContent> inputs = new ArrayList<>();
        for (InputContentDTO inputContentDTO : inputContentDTOList) {
            inputs.add(inputContentDTO.getInput());
        }
        maestroRequest.setInputs(inputs);
    }

    @JsonIgnore
    public MaestroRequest getMaestroRequest() {
        return maestroRequest;
    }

    @JsonIgnore
    public void setMaestroRequest(MaestroRequest maestroRequest) {
        this.maestroRequest = maestroRequest;
    }
}

