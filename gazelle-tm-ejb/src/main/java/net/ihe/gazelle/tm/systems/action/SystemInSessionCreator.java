package net.ihe.gazelle.tm.systems.action;

import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import net.ihe.gazelle.ssov7.gum.client.application.User;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import net.ihe.gazelle.ssov7.gum.client.interlay.client.utils.UserSearchParams;
import net.ihe.gazelle.tm.application.action.ApplicationManager;
import net.ihe.gazelle.tm.systems.model.System;
import net.ihe.gazelle.tm.systems.model.SystemType;
import net.ihe.gazelle.users.model.Institution;
import org.jboss.seam.annotations.*;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.international.StatusMessages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static org.jboss.seam.ScopeType.PAGE;

/**
 * Created by gthomazon on 05/07/17.
 */
@Name("systemInSessionCreator")
@Scope(PAGE)
@Synchronized(timeout = 10000)
@GenerateInterface("SystemInSessionCreatorLocal")
public class SystemInSessionCreator extends AbstractSystemInSessionEditor
      implements Serializable, SystemInSessionCreatorLocal {



   private static final Logger LOG = LoggerFactory.getLogger(SystemInSessionCreator.class);
   private static final long serialVersionUID = -566180111619017150L;
   protected SystemType sysType;
   protected String sysversion;
   protected String syskeywordSuffix;
   protected String sysname;
   protected String sysOwnerId;

   @In
   GazelleIdentity identity;
   @In
   private EntityManager entityManager;

   @In(value = "gumUserService")
   private UserService userService;

   private SystemInSessionBuilder sisBuilder;

   public SystemInSessionCreator() {
   }

   public SystemInSessionCreator(SystemInSessionBuilder sisBuilder) {
      this.sisBuilder = sisBuilder;
   }

   public SystemType getSysType() {
      return sysType;
   }

   public void setSysType(SystemType sysType) {
      this.sysType = sysType;
   }

   public String getSysversion() {
      return sysversion;
   }

   public void setSysversion(String sysversion) {
      this.sysversion = sysversion;
   }

   public String getSyskeywordSuffix() {
      return syskeywordSuffix;
   }

   public void setSyskeywordSuffix(String syskeywordSuffix) {
      this.syskeywordSuffix = syskeywordSuffix;
   }

   public String getSysname() {
      return sysname;
   }

   public void setSysname(String sysname) {
      this.sysname = sysname;
   }

   public Institution getInstitutionForCreation() {
      return institutionForCreation;
   }

   public void setInstitutionForCreation(Institution institutionForCreation) {
      this.institutionForCreation = institutionForCreation;
   }

   public String getsysOwnerId() {
      return sysOwnerId;
   }

   public void setsysOwnerId(String sysOwnerId) {
      this.sysOwnerId = sysOwnerId;
   }

   @Override
   @Create
   public void initCreation() {
      if (LOG.isDebugEnabled()) {
         LOG.debug("initCreation");
      }
      sysOwnerId = identity.getUsername();
      institutionForCreation = Institution.getLoggedInInstitution();
   }

   /**
    * Add (create) a system to the database This operation is allowed for some granted users (check the security.drl)
    */
   @Override
   @Restrict("#{s:hasPermission('SystemInSessionCreator', 'addSystemForPR', null)}")
   public void addSystemForPR(SystemInSessionBuilder sisBuilder) {
      if (LOG.isDebugEnabled()) {
         LOG.debug("addSystemForPR");
      }
      System sys = entityManager.find(System.class, sisBuilder.getSystemInSession().getSystem().getId());
      sisBuilder.addSystemForPR(sys);
   }

   /**
    * Add (create) a system to the database This operation is allowed for some granted users (check the security.drl)
    */
   private void addSystemForTM(SystemInSessionBuilder sisBuilder) throws SystemActionException {
      if (LOG.isDebugEnabled()) {
         LOG.debug("addSystemForTM");
      }
      sisBuilder.addSystemForTM(identity);
   }

   /**
    * Add (create) a system to the database This operation is allowed for some granted users (check the security.drl)
    */
   @Override
   @Restrict("#{s:hasPermission('SystemInSessionCreator', 'addSystem', null)}")
   public String addSystem() {
      if (LOG.isDebugEnabled()) {
         LOG.debug("addSystem");
      }

      sisBuilder = new SystemInSessionBuilder(sysType, sysversion, syskeywordSuffix, sysname, institutionForCreation,
              userService.getUserById(sysOwnerId));
      try {
         addSystemForTM(sisBuilder);
         if (ApplicationManager.instance().isProductRegistry()) {
            addSystemForPR(sisBuilder);
         }
         StatusMessages.instance()
                 .addFromResourceBundle(StatusMessage.Severity.INFO, "gazelle.systems.system.faces" +
                         ".SystemSuccessfullyCreated", sysname);

         setDefaultTab("editSystemSummaryTab");
         return "/systems/system/editSystemInSession.seam?id=" + sisBuilder.getSystemInSession().getId();
      } catch (SystemActionException e) {
         StatusMessages.instance().addFromResourceBundle(StatusMessage.Severity.ERROR, e.getMessage());
      }
      return null;
   }

   /**
    * Generate the system keyword, using the selected system type and the institution keyword eg. PACS_AGFA_
    */
   @Override
   @Restrict("#{s:hasPermission('SystemInSessionCreator', 'generateSystemKeyword', null)}")
   public void generateSystemKeyword() {
      if (LOG.isDebugEnabled()) {
         LOG.debug("generateSystemKeyword");
      }
      try {
         setSyskeywordSuffix(SystemInSessionBuilder.generateSystemKeywordSuffix(institutionForCreation, sysType));
      } catch (SystemActionException e) {
         StatusMessages.instance().add(StatusMessage.Severity.ERROR, e.getMessage());
      }
   }

   @Override
   public String getSystemInSessionKeywordBase() {
      return SystemInSessionBuilder.getSystemInSessionKeywordBase(sysType, institutionForCreation);
   }

   public List<User> listUsersForCompany(Institution institution) {
      if (institution != null) {
         UserSearchParams userSearchParams = new UserSearchParams()
                 .setOrganizationId(institution.getKeyword()).setActivated(true);
         return userService.searchNoLimit(userSearchParams);
      } else {
         return new ArrayList<>();
      }
   }


   public List<User> listPossibleOwners() {
      if(identity.hasRole("admin_role") || identity.hasRole("testing_session_admin_role")) {
         return listUsersForCompany(this.institutionForCreation);
      }
      else if (identity.hasRole("vendor_admin_role")) {
         Institution institution = Institution.getLoggedInInstitution();
         return listUsersForCompany(institution);
      }
     else {
          return new ArrayList<>();
     }
   }

   public List<String> listPossibleOwnerIds() {
      List<String> userIds = new ArrayList<>();
      for (User user : listPossibleOwners()) {
         userIds.add(user.getId());
      }
      return userIds;
   }

   public String getDisplayNameFromId(String userId) {
      return userService.getUserDisplayNameWithoutException(userId);
   }
}