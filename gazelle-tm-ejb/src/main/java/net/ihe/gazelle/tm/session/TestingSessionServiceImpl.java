package net.ihe.gazelle.tm.session;

import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import net.ihe.gazelle.tm.gazelletest.model.definition.TestType;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import net.ihe.gazelle.tm.systems.model.TestingSessionAdmin;
import net.ihe.gazelle.users.UserContext;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import javax.faces.model.SelectItem;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Name("testingSessionService")
@AutoCreate
@Scope(ScopeType.EVENT)
public class TestingSessionServiceImpl implements TestingSessionService {

    private static final String TESTING_SESSION_MUST_BE_DEFINED = "TestingSession must be defined";

    @In(value = "testingSessionServiceDAO", create = true)
    private TestingSessionServiceDAO testingSessionServiceDAO;

    @In(value = "gumUserService")
    private UserService userService;

    @In(value = "userContext")
    private UserContext userContext;

    public TestingSessionServiceImpl() {
        //For seam
    }

    public TestingSessionServiceImpl(TestingSessionServiceDAO testingSessionServiceDAO, UserService userService, UserContext userContext) {
        this.testingSessionServiceDAO = testingSessionServiceDAO;
        this.userService = userService;
        this.userContext = userContext;
    }

    @Override
    public TestingSession getUserTestingSession() {
        Integer testingSessionId = userContext.getSelectedTestingSessionId();

        boolean testingSessionExisting = testingSessionId != null && testingSessionServiceDAO.isTestingSessionExisting(testingSessionId);
        if (!testingSessionExisting) {
            TestingSession defaultTestingSession = testingSessionServiceDAO.getDefaultTestingSession();
            userContext.setSelectedTestingSessionId(defaultTestingSession.getId());
            return defaultTestingSession;
        }

        return testingSessionServiceDAO.getTestingSessionById(testingSessionId);
    }

    @Override
    public void setCurrentTestingSession(TestingSession testingSession) {
        //FIXME Contexts.getSessionContext().set("selectedTestingSession", refreshedTestingSession);
        //FIXME Contexts.getSessionContext().set("selectedUser", userService.getUserById(identity.getUsername()));


        userContext.setSelectedTestingSessionId(testingSession == null ? null : testingSession.getId());
    }

    @Override
    public boolean isTestingSessionRunning(TestingSession testingSession) {
        if (testingSession == null) {
            return false;
        }
        if (testingSession.getBeginningSession() == null
                || testingSession.getEndingSession() == null) {
            return testingSession.getContinuousSession();
        }
        Date now = new Date();
        return (now.after(testingSession.getBeginningSession()) &&
                now.before(testingSession.getEndingSession()));
    }

    @Override
    public boolean isBeforeSessionStart(TestingSession testingSession) {
        if (testingSession == null) {
            throw new IllegalArgumentException(TESTING_SESSION_MUST_BE_DEFINED);
        }
        if (testingSession.getBeginningSession() == null) {
            return testingSession.getContinuousSession();
        }
        Date now = new Date();
        return now.before(testingSession.getBeginningSession());
    }

    @Override
    public boolean isAfterSessionEnd(TestingSession testingSession) {
        if (testingSession == null) {
            throw new IllegalArgumentException(TESTING_SESSION_MUST_BE_DEFINED);
        }
        if (testingSession.getEndingSession() == null) {
            return testingSession.getContinuousSession();
        }
        Date now = new Date();
        return now.after(testingSession.getEndingSession());
    }

    @Override
    public boolean isRegistrationOpen(TestingSession testingSession) {
        if (testingSession == null) {
            throw new IllegalArgumentException(TESTING_SESSION_MUST_BE_DEFINED);
        }
        return testingSession.isRegistrationOpened();
    }


    @Override
    public boolean isAttendeesRegistrationEnabled(TestingSession testingSession) {
        if (testingSession == null) {
            throw new IllegalArgumentException(TESTING_SESSION_MUST_BE_DEFINED);
        }
        return testingSession.getAllowParticipantRegistration();
    }

    @Override
    public boolean isTestingSessionOpen(TestingSession testingSession) {
        if (testingSession == null) {
            throw new IllegalArgumentException(TESTING_SESSION_MUST_BE_DEFINED);
        }
        return !testingSession.isClosed();
    }

    @Override
    public List<String> getTestingSessionManagers(TestingSession testingSession) {
        if (testingSession == null) {
            throw new IllegalArgumentException(TESTING_SESSION_MUST_BE_DEFINED);
        }
        List<TestingSessionAdmin> testingSessionAdmins = testingSession.getTestingSessionAdmins();
        List<String> adminIds = new ArrayList<>();
        for (TestingSessionAdmin sessionAdmin : testingSessionAdmins) {
            adminIds.add(userService.getUserDisplayNameWithoutException(sessionAdmin.getUserId()));
        }
        return adminIds;
    }


    @Override
    public boolean isContinuousSession(TestingSession testingSession) {
        return Boolean.TRUE.equals(testingSession.getContinuousSession());
    }

    @Override
    public boolean isPrecatTestsEnabled(TestingSession testingSession) {
        return testingSession.isEnablePrecatTests();
    }

    @Override
    public List<TestType> getListTestType() {
        return getUserTestingSession().getTestTypes();
    }

    @Override
    public List<SelectItem> getListTestTypeAsSelectItems() {
        List<TestType> list = getUserTestingSession().getTestTypes();
        List<SelectItem> result = new ArrayList<>();
        String label = org.jboss.seam.core.ResourceBundle.instance().getString("gazelle.tests.testInstance.allOptions");
        result.add(new SelectItem("All", label));
        for (TestType testType : list) {
            result.add(new SelectItem(testType.getKeyword(), testType.getLabelToDisplay()));
        }
        return result;
    }

}
