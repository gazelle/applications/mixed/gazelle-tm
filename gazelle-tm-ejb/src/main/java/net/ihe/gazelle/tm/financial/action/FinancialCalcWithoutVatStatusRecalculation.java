package net.ihe.gazelle.tm.financial.action;

import net.ihe.gazelle.tm.financial.model.Invoice;
import net.ihe.gazelle.tm.systems.model.TestingSession;

import java.math.BigDecimal;

public class FinancialCalcWithoutVatStatusRecalculation extends FinancialCalc {

    /**
     * Calculate VAT amount (not depending on billing country-address) only if VAT is due
     * depending on the TestingSession VAT percent
     *
     * @param invoice : Invoice to update
     * @return Invoice : invoice with new VAT amount
     */
    public Invoice calculateVatAmountForCompany(Invoice invoice) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Invoice calculateVatAmountForCompanyWithoutChangingVatIsDue");
        }
        if ((invoice == null) || (invoice.getInstitution() == null) || (invoice.getInstitution().getId() == null)) {
            return invoice;
        }
        BigDecimal vatAmount = calculateVatAmount(
                invoice.getTestingSession(),
                invoice.getFeesAmount(),
                invoice.getFeesDiscount(),
                invoice.getVatDue());
        invoice.setVatAmount(vatAmount);
        return invoice;
    }

    /**
     * Calculate VAT amount (not depending on billing country-address) only if VAT is due
     * depending on the TestingSession VAT percent
     *
     * @param ts : TestingSession for recover VAT percent
     * @param feesAmount : BigDecimal for fees amount excl. VAT
     * @param feesDiscount : BigDecimal for fees discount on fees amount
     * @param vatDue : Boolean if VAT is due or not
     * @return vatAmount : the new VAT amount
     */
    public BigDecimal calculateVatAmount(TestingSession ts, BigDecimal feesAmount,
                                                BigDecimal feesDiscount, Boolean vatDue) {
        if(vatDue == null || !vatDue) {
            return BigDecimal.ZERO;
        }

        BigDecimal feesWithDiscount = feesAmount.add(feesDiscount.negate());
        BigDecimal vatAmount = feesWithDiscount.multiply(ts.getVatPercent()).setScale(2);
        return vatAmount;
    }
}
