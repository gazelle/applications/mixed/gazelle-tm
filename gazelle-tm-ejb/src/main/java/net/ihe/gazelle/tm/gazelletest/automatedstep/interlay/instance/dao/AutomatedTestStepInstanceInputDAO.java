package net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.instance.dao;

import net.ihe.gazelle.tm.gazelletest.domain.AutomatedTestStepInputDomain;
import net.ihe.gazelle.tm.gazelletest.model.instance.AutomatedTestStepInstanceInput;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstance;

import java.util.List;

public interface AutomatedTestStepInstanceInputDAO {

    /**
     * Create an automated test step instance input
     * @param inputDefinition The definition of the input instance to be saved
     * @param testStepsInstanceId The id of the test step instance linked to this input
     * @return The instance input created
     */
    AutomatedTestStepInstanceInput createAutomatedTestStepInstanceInput(AutomatedTestStepInputDomain inputDefinition, Integer testStepsInstanceId);

    /**
     * Get a list of instance input linked to a test step instance
     * @param testStepsInstanceId The id of the test step instance
     * @return The result list of instance input
     */
    List<AutomatedTestStepInstanceInput> getInstanceInputWithoutData(Integer testStepsInstanceId);

    /**
     * Get all the instance input for a whole test instance
     * @param testInstance the test instance
     * @return the list of all instance input
     */
    List<AutomatedTestStepInstanceInput> getInstanceInputForTestInstance(TestInstance testInstance);

    /**
     * Update an existing automated test step instance input to set the test step data id
     * @param automatedTestStepInputId The id of the test step input definition
     * @param automatedTestStepDataId The id of the input data
     */
    void setTestStepDataIdToInstanceInput(Integer automatedTestStepInputId, Integer automatedTestStepDataId);
}
