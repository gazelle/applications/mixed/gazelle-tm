package net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.execution.ws;

import net.ihe.gazelle.ssov7.authn.interlay.Authenticator;
import net.ihe.gazelle.ssov7.authn.interlay.AuthenticatorFactory;
import net.ihe.gazelle.tm.gazelletest.action.TestInstanceTokenService;
import net.ihe.gazelle.tm.gazelletest.dao.TestInstanceTokenDAOImpl;
import org.jboss.seam.Component;

import javax.persistence.EntityManager;

public class GazelleTokenAuthenticatorFactory implements AuthenticatorFactory {

    @Override
    public Authenticator createAuthenticator() {
        EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
        return new GazelleTokenAuthenticator(new TestInstanceTokenService(new TestInstanceTokenDAOImpl(entityManager)));
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
