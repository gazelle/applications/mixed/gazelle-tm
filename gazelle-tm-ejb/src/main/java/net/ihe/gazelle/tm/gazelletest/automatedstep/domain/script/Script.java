package net.ihe.gazelle.tm.gazelletest.automatedstep.domain.script;

import java.util.ArrayList;
import java.util.List;

public class Script {

    private String name;
    private List<InputDefinition> inputs;
    private List<Step> steps;

    public Script() {
        // empty for serialization
    }

    public Script(String name, List<InputDefinition> inputs, List<Step> steps) {
        this.name = name;
        this.inputs = new ArrayList<>(inputs);
        this.steps = new ArrayList<>(steps);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<InputDefinition> getInputs() {
        return new ArrayList<>(inputs);
    }

    public void setInputs(List<InputDefinition> inputs) {
        this.inputs = new ArrayList<>(inputs);
    }

    public List<Step> getSteps() {
        return new ArrayList<>(steps);
    }

    public void setSteps(List<Step> steps) {
        this.steps = new ArrayList<>(steps);
    }
}
