package net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.execution.service;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.ssov7.gum.client.application.User;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstance;
import net.ihe.gazelle.tm.messages.MessageSourceUsersDB;
import net.ihe.gazelle.tm.systems.model.TestingSession;

import java.util.*;

public class AutomatedTestInstanceStatusMessageSource extends MessageSourceUsersDB<TestInstance> {

    private final Collection<String> pathsToUser = new ArrayList<>();

    public AutomatedTestInstanceStatusMessageSource() {
        super();
        pathsToUser.add("monitorInSession.userId");
        pathsToUser.add("testStepsInstanceList.executionEntity.userId");
    }

    public AutomatedTestInstanceStatusMessageSource(UserService userService) {
        super(userService);
        pathsToUser.add("monitorInSession.userId");
        pathsToUser.add("testStepsInstanceList.executionEntity.userId");
    }

    @Override
    public List<User> getUsers(String userId, TestInstance sourceObject, String[] messageParameters) {
        EntityManagerService.provideEntityManager();

        Collection<String> pathsToUser = getPathsToUsername();
        Set<String> userIdList = new HashSet<>();
        for (String pathToUser : pathsToUser) {
            HQLQueryBuilder<TestInstance> queryBuilder = new HQLQueryBuilder<>(getSourceClass());
            queryBuilder.addEq("id", getId(sourceObject, messageParameters));

            List<?> values = queryBuilder.getListDistinct(pathToUser);
            for (Object value : values) {
                if (value instanceof String) {
                    userIdList.add((String) value);
                }
            }
        }

        List<User> users = new ArrayList<>();
        for (String id : userIdList) {
            users.add(userService.getUserById(id));
        }
        return users;
    }

    @Override
    public Integer getId(TestInstance sourceObject, String[] messageParameters) {
        return sourceObject.getId();
    }

    @Override
    public Collection<String> getPathsToUsername() {
        return pathsToUser;
    }

    @Override
    public Class<TestInstance> getSourceClass() {
        return TestInstance.class;
    }

    @Override
    public String getType(TestInstance instance, String[] messageParameters) {
        return "gazelle.message.testinstance.automatedStepRun";
    }

    @Override
    public String getLink(TestInstance entity, String[] messageParameters) {
        return "testInstance.seam?id=" + entity.getId();
    }

    @Override
    public String getImage(TestInstance sourceObject, String[] messageParameters) {
        return "gzl-icon-gears";
    }

    @Override
    public TestingSession getTestingSession(TestInstance sourceObject, String[] messageParameters) {
        return sourceObject.getTestingSession();
    }
}
