package net.ihe.gazelle.tm.gazelletest.action;

import java.io.Serializable;

public class InvalidTestReportException extends Exception implements Serializable {

    public InvalidTestReportException() {
    }

    public InvalidTestReportException(String s) {
        super(s);
    }

    public InvalidTestReportException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public InvalidTestReportException(Throwable throwable) {
        super(throwable);
    }

}