package net.ihe.gazelle.tm.organization.dao;

import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.tm.organization.OrganizationServiceDAO;
import net.ihe.gazelle.tm.organization.exception.OrganizationServiceDAOException;
import net.ihe.gazelle.users.model.*;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

@Name("organizationServiceDAO")
@Scope(ScopeType.STATELESS)
public class OrganizationServiceDAOImpl implements OrganizationServiceDAO {

    private static final Logger LOG = LoggerFactory.getLogger(OrganizationServiceDAOImpl.class);
    @In
    private EntityManager entityManager;

    @Override
    public List<PersonFunction> getRequiredRolesToCompleteRegistration() {
        LOG.trace("getRequiredRolesToCompleteRegistration");
        PersonFunction billing = PersonFunction.getBillingFunction(null);
        PersonFunction marketing = PersonFunction.getMarketingFunction(null);
        PersonFunction technical = PersonFunction.getTechnicalFunction(null);
        return Arrays.asList(billing, marketing, technical);
    }

    @Override
    public boolean hasOrganisationBillingContact(Institution institution) {
        LOG.trace("hasOrganisationBillingContact");
        return Person.companyAlreadyContainsBillingContact(institution);
    }

    @Override
    public Institution getCurrentOrganization() {
        LOG.trace("getCurrentOrganization");
        return Institution.getLoggedInInstitution();
    }

    @Override
    public List<Institution> getAllInstitution() {
        LOG.trace("getAllInstitution");
        return Institution.listAllInstitutions();
    }

    @Override
    public Institution getInstitutionWithKeyword(String keyword) {
        LOG.trace("getInstitutionWithKeyword");
        if (keyword == null)
            throw new IllegalArgumentException("The keyword of the organization is null");
        Institution institution = Institution.findInstitutionWithKeyword(keyword);
        if (institution == null)
            throw new NoSuchElementException(String.format("Organization with keyword %s not found", keyword));
        return institution;
    }

    @Override
    public Institution getInstitutionById(Integer id) {
        Institution institution = Institution.findInstitutionWithId(id);
        if (institution == null) {
            throw new NoSuchElementException(String.format("Organization with id %s not found", id));
        }
        return institution;
    }

    @Override
    public Institution findOrganisationByName(String name) {
        LOG.trace("findOrganisationByName");
        return Institution.findInstitutionWithName(name);
    }

    @Override
    public DelegatedOrganization getDelegatedOrganizationById(Integer organizationId) {
        LOG.trace("findDelegatedOrganizationById");
        if (organizationId == null)
            throw new IllegalArgumentException("Organization organization id is null");
        DelegatedOrganization delegatedOrganization = entityManager.find(DelegatedOrganization.class, organizationId);
        if (delegatedOrganization == null)
            throw new NoSuchElementException(String.format("Could not find delegated organization with organization_id: %s", organizationId));
        return delegatedOrganization;
    }

    @Override
    public DelegatedOrganization getDelegatedOrganizationByKeyword(String keyword) {
        LOG.trace("isDelegatedOrganizationExist");
        Query query = entityManager.createQuery("SELECT d FROM DelegatedOrganization d WHERE keyword = :keyword", DelegatedOrganization.class);
        query.setParameter("keyword", keyword);
        try {
            return (DelegatedOrganization) query.getSingleResult();
        } catch (NoResultException e) {
            throw new NoSuchElementException(String.format("Could not find delegated organization with keyword: %s", keyword));
        }
    }

    @Override
    public void createInstitution(Institution institution, Address address) {
        LOG.trace("createInstitution");
        try {
            InstitutionTypeQuery query = new InstitutionTypeQuery();
            query.addRestriction(HQLRestrictions.eq("type", "Company"));
            InstitutionType institutionType = query.getUniqueResult();
            institution.setInstitutionType(institutionType);
            entityManager.persist(institutionType);

            entityManager.persist(address);
            entityManager.persist(institution);
            entityManager.flush();
            LOG.info("Institution fully created");
        } catch (PersistenceException e) {
            throw new OrganizationServiceDAOException(e.getMessage());
        }
    }

    @Override
    public void createDelegatedOrganization(DelegatedOrganization delegatedOrganization) {
        LOG.trace("createDelegatedOrganization");
        try {
            entityManager.persist(delegatedOrganization);
            entityManager.flush();
        } catch (PersistenceException e) {
            throw new OrganizationServiceDAOException(e.getMessage());
        }
    }

    @Override
    public boolean isOrganizationExisting(String keyword) {
        return Institution.findInstitutionWithKeyword(keyword) != null;
    }

    @Override
    public Institution updateOrganizationName(Institution institution, String name) {
        institution.setName(name);
        try {
            entityManager.persist(institution);
            entityManager.flush();
            LOG.trace("Institution {} updated", institution.getKeyword());
            return getInstitutionWithKeyword(institution.getKeyword());
        } catch (PersistenceException e) {
            String message = String.format("Could not update name of delegated organization with id: %s", institution.getKeyword());
            throw new OrganizationServiceDAOException(message, e);
        }
    }

    @Override
    public Institution migrateLocalOrganizationToDelegated(Institution institution) {
        LOG.trace("createDelegatedOrganization");
        try {
            Query query = entityManager.createNativeQuery("INSERT INTO usr_delegated_organization " +
                    "(organization_id, external_id, idp_id) VALUES (:institutionId, :externalId, :idpId)");
            query.setParameter("institutionId", institution.getId());
            query.setParameter("externalId", ((DelegatedOrganization) institution).getExternalId());
            query.setParameter("idpId", ((DelegatedOrganization) institution).getIdpId());
            query.executeUpdate();
        } catch (PersistenceException e) {
            throw new OrganizationServiceDAOException(e.getMessage());
        }
        return getInstitutionById(institution.getId());
    }

    @Override
    public boolean isThereAtLeastOneOrganization() {
        InstitutionQuery institutionQuery = new InstitutionQuery();
        return institutionQuery.getCount() >= 1;
    }
}