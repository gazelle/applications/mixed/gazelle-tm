package net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.instance.dao;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManagerImpl;
import net.ihe.gazelle.tm.gazelletest.automatedstep.domain.instance.AutomatedTestStepInstanceData;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

@Name("automatedTestStepInstanceFileSystemDAO")
@Scope(ScopeType.STATELESS)
@AutoCreate
public class AutomatedTestStepInstanceFileSystemDAOImpl implements AutomatedTestStepInstanceFileSystemDAO {

    private static final Logger LOG = LoggerFactory.getLogger(AutomatedTestStepInstanceFileSystemDAOImpl.class);
    private final ApplicationPreferenceManager applicationPreferenceManager = new ApplicationPreferenceManagerImpl();

    @Override
    public void saveInputDataFile(Integer testStepsDataId, String fileName, String fileContent) {
        String pathToSaveFile = getFullPath(testStepsDataId, fileName);
        File fileToStore = new File(pathToSaveFile);
        fileToStore.getParentFile().mkdirs();
        if (fileToStore.getParentFile().exists()) {
            try {
                if (fileToStore.exists()) {
                    Files.delete(fileToStore.toPath());
                }
                try (FileOutputStream fos = new FileOutputStream(fileToStore)) {
                    fos.write(fileContent.getBytes(StandardCharsets.UTF_8));
                }
            } catch (IOException e) {
                throw new WriteInputException("Could not save uploaded input : " + e.getMessage());
            }
        } else {
            throw new IllegalStateException(fileToStore.getParentFile() + " does not exists.");
        }
    }

    @Override
    public String readInputDataFile(AutomatedTestStepInstanceData inputData) {
        String pathToGetFile = getFullPath(inputData.getTestStepDataId(), inputData.getFileName());
        try {
            byte [] bytes = Files.readAllBytes(Paths.get(pathToGetFile));
            return new String(bytes, StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new ReadInputException("Failed reading input " + pathToGetFile, e);
        }
    }

    @Override
    public void deleteInputDataFile(AutomatedTestStepInstanceData inputData) {
        String pathToDeleteFile = getFullPath(inputData.getTestStepDataId(), inputData.getFileName());
        try {
            File fileToDelete = new File(pathToDeleteFile);
            if (fileToDelete.exists()) {
                Files.delete(Paths.get(pathToDeleteFile));
            }
        } catch (IOException e) {
            LOG.error("Could not delete input " + pathToDeleteFile, e);
        }
    }

    private String getFullPath(Integer testStepDataId, String fileName) {
        return getPath() + generateFileName(testStepDataId, fileName);
    }

    private String getPath() {
        String dataPath = applicationPreferenceManager.getGazelleDataPath();
        String scriptPath = applicationPreferenceManager.getStringValue("file_steps_path");
        return dataPath + java.io.File.separatorChar + scriptPath + java.io.File.separatorChar;
    }

    private String generateFileName(Integer testStepDataId, String fileName) {
        if (testStepDataId == null || fileName == null || fileName.isEmpty()) {
            throw new IllegalArgumentException("Needed parameters for the test step instance input name generation are null");
        }
        return testStepDataId + "_" + fileName;
    }
}
