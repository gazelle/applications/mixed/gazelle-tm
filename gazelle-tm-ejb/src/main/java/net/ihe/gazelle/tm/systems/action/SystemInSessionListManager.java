package net.ihe.gazelle.tm.systems.action;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.list.GazelleListDataModel;
import net.ihe.gazelle.common.filter.list.NoHQLDataModel;
import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.common.log.ExceptionLogging;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.menu.Authorizations;
import net.ihe.gazelle.objects.model.ObjectInstance;
import net.ihe.gazelle.objects.model.ObjectInstanceFile;
import net.ihe.gazelle.ssov7.gum.client.application.Role;
import net.ihe.gazelle.tf.model.IntegrationProfile;
import net.ihe.gazelle.tm.application.action.ApplicationManager;
import net.ihe.gazelle.tm.gazelletest.action.SystemInSessionOverview;
import net.ihe.gazelle.tm.gazelletest.model.instance.SystemInSessionUser;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestStepsInstance;
import net.ihe.gazelle.tm.session.TestingSessionService;
import net.ihe.gazelle.tm.systems.SystemSummary;
import net.ihe.gazelle.tm.systems.SystemUnderTestService;
import net.ihe.gazelle.tm.systems.model.InstitutionSystem;
import net.ihe.gazelle.tm.systems.model.ProfileInTestingSessionQuery;
import net.ihe.gazelle.tm.systems.model.System;
import net.ihe.gazelle.tm.systems.model.SystemInSession;
import net.ihe.gazelle.tm.systems.model.SystemInSessionQuery;
import net.ihe.gazelle.tm.systems.model.SystemInSessionRegistrationStatus;
import net.ihe.gazelle.tm.systems.model.SystemInSessionStatus;
import net.ihe.gazelle.tm.systems.model.TableSession;
import net.ihe.gazelle.tm.systems.model.Testability;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import net.ihe.gazelle.users.model.Institution;
import net.ihe.gazelle.users.model.InstitutionQuery;
import net.ihe.gazelle.util.Pair;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.async.QuartzDispatcher;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.international.StatusMessages;
import org.jboss.seam.security.Identity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.model.DataModel;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.jboss.seam.ScopeType.PAGE;

@Name("systemInSessionListManager")
@Scope(PAGE)
@GenerateInterface("SystemInSessionListManagerLocal")
public class SystemInSessionListManager extends AbstractSystemInSessionEditor implements Serializable, SystemInSessionListManagerLocal {

    private static final Logger LOG = LoggerFactory.getLogger(SystemInSessionListManager.class);
    private static final long serialVersionUID = -9119983737419317076L;

    @In
    private EntityManager entityManager;

    private Institution selectedInstitution;
    private SystemInSessionRegistrationStatus selectedRegistrationStatus;
    private List<SystemInSession> systemsInSession;
    private SystemInSession selectedSystemInSession;
    private SystemInSessionModifier sisModifier;
    private System systemForInstitutions;
    private Filter<Institution> filterInstitution;
    private boolean canDeleteSIS = false;
    private String messageProblemOnDelete;

    @In(create = true)
    private ApplicationManager applicationManager;

    private List<SystemSummary> registeredSystemSummary;
    private SelectedMenuValue selectedHasMissingDependencies;

    public enum SelectedMenuValue {
        PLEASE_SELECT("gazelle.common.PleaseSelect"),
        YES("gazelle.common.Yes"),
        NO("gazelle.common.No");
        private String label;
        private SelectedMenuValue(String label){
            this.label=label;
        }
        public String getLabel(){
            return this.label;
        }
    }
    public void setSelectedHasMissingDependencies(SelectedMenuValue value){
        this.selectedHasMissingDependencies=value;
    }
    public SelectedMenuValue getSelectedHasMissingDependencies(){
        return selectedHasMissingDependencies;
    }

    public SelectedMenuValue[] getAllSelectedMenuValue(){
        return SelectedMenuValue.values();
    }

    public SelectedMenuValue getSelectedIsAccepted() {
        return selectedIsAccepted;
    }

    public void setSelectedIsAccepted(SelectedMenuValue selectedIsAccepted) {
        this.selectedIsAccepted = selectedIsAccepted;
    }

    private SelectedMenuValue selectedIsAccepted;

    private TestingSession currentTestingSession;
    @In(value="systemUnderTestService", create = true)
    private SystemUnderTestService systemUnderTestService;
    @In(value = "testingSessionService",create = true)
    private TestingSessionService testingSessionService;

    public List<SystemInSessionRegistrationStatus> getStatus(){ return Arrays.asList(SystemInSessionRegistrationStatus.values()); }

    @Create
    public void init() {
        try {
            this.currentTestingSession = testingSessionService.getUserTestingSession();
            this.registeredSystemSummary = systemUnderTestService.getRegisteredSystemsOfSession(currentTestingSession);
        } catch (IllegalArgumentException e){
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "An error has occurred during initialization");
        }
    }

    public Institution getSelectedInstitution() {
        return selectedInstitution;
    }
    public void setSelectedInstitution(Institution selectedInstitution) {
        this.selectedInstitution = selectedInstitution;
    }

    public SystemInSessionRegistrationStatus getSelectedRegistrationStatus() {
        return selectedRegistrationStatus;
    }

    public void setSelectedRegistrationStatus(SystemInSessionRegistrationStatus selectedRegistrationStatus) {
        this.selectedRegistrationStatus = selectedRegistrationStatus;
    }

    public SystemInSession getSelectedSystemInSession() {
        return selectedSystemInSession;
    }

    public void setSelectedSystemInSession(SystemInSession selectedSystemInSession) {
        this.selectedSystemInSession = selectedSystemInSession;
    }

    @Override
    public System getSystemForInstitutions() {
        LOG.debug("getSystemForInstitutions");
        return systemForInstitutions;
    }

    @Override
    public void setSystemForInstitutions(System systemForInstitutions) {
        LOG.debug("setSystemForInstitutions");
        this.systemForInstitutions = systemForInstitutions;
        filterInstitution = null;
    }

    @Override
    public Filter<Institution> getFilterInstitution() {
        LOG.debug("getFilterInstitution");
        if (filterInstitution == null) {
            InstitutionQuery query = new InstitutionQuery();
            HQLCriterionsForFilter<Institution> hqlCriterionsForFilter = query.getHQLCriterionsForFilter();
            hqlCriterionsForFilter.addPath("institution", query);
            filterInstitution = new Filter<Institution>(hqlCriterionsForFilter) {
                private static final long serialVersionUID = -8300279869803615591L;

                @Override
                public boolean isCountEnabled() {
                    if (LOG.isDebugEnabled()) {
                        LOG.debug("isCountEnabled");
                    }
                    return false;
                }
            };
        }
        return filterInstitution;
    }

    @Override
    public List<SystemInSession> getSystemsInSession() {
        LOG.debug("getSystemsInSession");
        List<SystemInSession> ssIs = new ArrayList<SystemInSession>();

        if (selectedInstitution != null) {
            for (SystemInSession sis : systemsInSession) {
                String institutionKeyword = displayInstitutionKeywordForSystem(sis.getSystem());
                if (institutionKeyword.equals(selectedInstitution.getKeyword())) {
                    if (selectedRegistrationStatus != null) {
                        if (sis.getRegistrationStatus().equals(selectedRegistrationStatus)) {
                            ssIs.add(sis);
                        }
                    } else {
                        ssIs.add(sis);
                    }
                }
            }
            return ssIs;
        }
        if (selectedRegistrationStatus != null) {
            for (SystemInSession sis : systemsInSession) {
                if (sis.getRegistrationStatus().equals(selectedRegistrationStatus)) {
                    ssIs.add(sis);
                }
            }
            return ssIs;
        }
        return systemsInSession;
    }

    @Override
    public void setSystemsInSession(List<SystemInSession> systemsInSession) {
        LOG.debug("setSystemsInSession");
        this.systemsInSession = systemsInSession;
    }

    @Override
    public boolean isCanDeleteSIS() {
        LOG.debug("isCanDeleteSIS");
        return canDeleteSIS;
    }

    @Override
    public void setCanDeleteSIS(boolean canDeleteSIS) {
        LOG.debug("setCanDeleteSIS");
        this.canDeleteSIS = canDeleteSIS;
    }

    @Override
    public String getMessageProblemOnDelete() {
        LOG.debug("getMessageProblemOnDelete");
        return messageProblemOnDelete;
    }

    @Override
    public void setMessageProblemOnDelete(String messageProblemOnDelete) {
        LOG.debug("setMessageProblemOnDelete");
        this.messageProblemOnDelete = messageProblemOnDelete;
    }

    @Override
    public String getSystemInSessionKeywordBase() {
        return null;
    }

    @Override
    @Restrict("#{s:hasPermission('SystemInSessionListManager', 'getSystemsInSessionListDependingInstitution', null)}")
    public void getSystemsInSessionListDependingInstitution() {
        LOG.debug("getSystemsInSessionListDependingInstitution");
        EntityManager em = EntityManagerService.provideEntityManager();
        if (Identity.instance().hasRole(Role.ADMIN)
                || Identity.instance().hasRole(Role.VENDOR_ADMIN)
                || Identity.instance().hasRole(Role.TESTING_SESSION_ADMIN)
                || Identity.instance().hasRole(Role.VENDOR)) {

            TestingSession testingSession = testingSessionService.getUserTestingSession();
            if (testingSession == null) {
                LOG.error(NO_ACTIVE_SESSION);
                StatusMessages.instance().addFromResourceBundle(StatusMessage.Severity.ERROR, "gazelle.systems.error.noActivatedTestingSession");
            }

            Institution institution;
            if (Identity.instance().hasRole(Role.ADMIN)
                    || Identity.instance().hasRole(Role.TESTING_SESSION_ADMIN)) {
                institution = null;
            } else {
                institution = Institution.getLoggedInInstitution();
            }
            systemsInSession = SystemInSession.getSystemsInSessionForCompanyForSession(em, institution, testingSession);
        }
    }

    public List<SystemInSessionRegistrationStatus> getListOfRegistrationStatus(TestingSession ts) {
        LOG.debug("getListOfRegistrationStatus");
        SystemInSessionQuery q = new SystemInSessionQuery();
        q.testingSession().id().eq(ts.getId());
        return q.registrationStatus().getListDistinct();
    }

    public NoHQLDataModel<SystemInSession> getAllSystemsInSession() {
        final List<SystemInSession> res = getSystemsInSession();
        return new GazelleListDataModel<SystemInSession>(res);
    }

    @Override
    public String displayInstitutionKeywordForSystem(System inSystem) {
        LOG.debug("displayInstitutionKeywordForSystem");
        List<Institution> listInst = System.getInstitutionsForASystem(inSystem);
        StringBuffer s = new StringBuffer();
        int size = listInst.size();
        for (int i = 0; i < size; i++) {
            s.append(listInst.get(i).getKeyword());
            if ((size > 1) && (i == (size - 1))) {
                s.append("/");
            }
        }
        return s.toString();
    }

    public List<SystemInSessionRegistrationStatus> getPossibleRegistrationStatus(SystemInSession sys) {
        LOG.debug("getPossibleRegistrationStatus");
        return sys.getPossibleRegistrationStatus();
    }


    public void updateSystemInSession(SystemInSession currentSystemInSession) {
        LOG.debug("updateSystemInSession");
        entityManager.merge(currentSystemInSession);
        entityManager.flush();
    }

    /**
     * This method indicates for a system if the logged in user may delete this system. It returns a flag, if true, then 'delete' button is
     * displayed. Delete button is displayed for admins and
     * system's owner.
     *
     * @param currentSystem : system (containing Integration Statement informations) used to determinate status
     * @return Boolean : true if we display the delete button
     */
    @Override
    public boolean isDeleteSystemButtonRendered(System currentSystem) {
        LOG.debug("isDeleteSystemButtonRendered");
        Boolean returnedFlag = false;
        if (Identity.instance().hasRole(Role.ADMIN) || Authorizations.TESTING_SESSION_ADMIN_OF_CURRENT_TESTING_SESSION.isGranted()
                || Identity.instance().hasRole(Role.VENDOR_ADMIN) ||
                Identity.instance().getCredentials().getUsername().equals(currentSystem.getOwnerUserId())) {
            returnedFlag = true;
        }
        return returnedFlag;
    }

    @Override
    @Restrict("#{s:hasPermission('SystemInSessionListManager', 'deleteSystemInSession', null)}")
    public void deleteSystemInSession() {
        LOG.debug("deleteSystemInSession");
        try {
            List<SystemInSession> listSiS = SystemInSession.findSystemInSessionsWithSystem(selectedSystemInSession
                    .getSystem());
            if (listSiS.size() >= 1) {
                SystemInSession.deleteSelectedSystemInSessionWithDependencies(selectedSystemInSession);
                FacesMessages.instance().add(StatusMessage.Severity.INFO,
                        "The selected system (" + selectedSystemInSession.getSystem().getKeyword()
                                + ") was deleted from the testing session :"
                                + selectedSystemInSession.getTestingSession().getName());
            }

            // It's just for PR
            if (ApplicationManager.instance().isProductRegistry()) {
                DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
                Date date = new Date();
                System sys = entityManager.find(System.class, selectedSystemInSession.getSystem().getId());
                String version = sys.getVersion() + "_Deleted_" + dateFormat.format(date);
                sys.setVersion(version);
                entityManager.merge(sys);
                entityManager.flush();
            }

            selectedSystemInSession = null;

            // Calculate new amounts for invoice - Fees and VAT changes when a system is deleted
            sisModifier = new SystemInSessionModifier(selectedInstitution, selectedSystemInSession);
            sisModifier.calculateFinancialSummaryDTO();
        } catch (Exception ex) {
            net.ihe.gazelle.common.log.ExceptionLogging.logException(ex, LOG);
            StatusMessages.instance().addFromResourceBundle(
                    "gazelle.systems.error.SystemCanNotBeDeleted");
        }
    }

    @Override
    @Restrict("#{s:hasPermission('SystemInSessionListManager', 'deleteSystemInAllSession', null)}")
    public void deleteSystemInAllSession() {
        LOG.debug("deleteSystemInAllSession");
        try {
            SystemInSession sis = selectedSystemInSession;
            deleteSystemInSession();
            FacesMessages.instance().add(StatusMessage.Severity.INFO, sis.getSystem().getKeyword() + "-" + sis.getSystem().getName()
                    + " has been removed from all testing session");
            SystemInSessionOverview sso = new SystemInSessionOverview();
            sso.refreshFilter();
            selectedSystemInSession = null;
        } catch (Exception e) {
            ExceptionLogging.logException(e, LOG);
            StatusMessages.instance().addFromResourceBundle(StatusMessage.Severity.ERROR, "gazelle.systems.error.SystemCanNotBeDeleted");
        }
    }

    @Override
    @Restrict("#{s:hasPermission('SystemInSessionListManager', 'createSystemSessionForSystem', null)}")
    public void createSystemSessionForSystem(System s) {
        LOG.debug("createSystemSessionForSystem");
        TableSession tableSession = TableSession.getTableSessionByKeyword(TableSession.getDEFAULT_TABLE_SESSION_STRING());
        SystemInSessionStatus systemInSessionStatus = SystemInSessionStatus.getSTATUS_NOT_HERE_YET();
        SystemInSession newSysInSess = new SystemInSession(tableSession, s, currentTestingSession, null, systemInSessionStatus);
        // The system status need to be set to IN_PROGRESS
        newSysInSess.setRegistrationStatus(SystemInSessionRegistrationStatus.IN_PROGRESS);
        // The copied system is set to false
        newSysInSess.setIsCopy(false);
        // The system shall not be accepted to the connectathon yet.
        newSysInSess.setAcceptedToSession(false);
        newSysInSess = entityManager.merge(newSysInSess);
        entityManager.flush();

        FacesMessages.instance().add(StatusMessage.Severity.INFO, "New system in session : " + newSysInSess.getLabel() + " has been added in "
                + currentTestingSession.getName());
    }

    @Override
    @Restrict("#{s:hasPermission('SystemInSessionListManager', 'updateSelectedSystemInSession', null)}")
    public void updateSelectedSystemInSession(SystemInSession currentSIS) {
        LOG.debug("updateSelectedSystemInSession");
        this.selectedSystemInSession = currentSIS;
        Pair<Boolean, String> pbs = this.canDeleteSystemInSession(selectedSystemInSession);
        this.canDeleteSIS = pbs.getObject1().booleanValue();
        this.messageProblemOnDelete = pbs.getObject2();
    }

    @Override
    public Pair<Boolean, String> canDeleteSystemInSession(SystemInSession inSystemInSession) {
        LOG.debug("canDeleteSystemInSession");
        boolean res = true;
        if (inSystemInSession != null) {
            if (inSystemInSession.getId() != null) {
                int lsisucount = SystemInSessionUser.getCountSystemInSessionUserFiltered(inSystemInSession, null);
                if (lsisucount > 0) {
                    return new Pair<Boolean, String>(false, "The system is used on some tests.");
                }
                int noi = ObjectInstance.getNumberObjectInstanceFiltered(null, inSystemInSession, null, null);
                if (noi > 0) {
                    return new Pair<Boolean, String>(false,
                            "The system has upload some samples on the testing session.");
                }
                int noif = ObjectInstanceFile.getCountObjectInstanceFileFiltered(null, null, inSystemInSession, null,
                        null);
                if (noif > 0) {
                    return new Pair<Boolean, String>(false, "The system has upload some sampleson the testing session.");
                }
                int ntsi = TestStepsInstance.getCountTestStepsInstanceBySystemInSession(inSystemInSession);
                if (ntsi > 0) {
                    return new Pair<Boolean, String>(false, "The system is used on some tests.");
                }
            }
        }
        return new Pair<Boolean, String>(res, null);
    }

    @Override
    @Restrict("#{s:hasPermission('SystemInSessionListManager', 'addInstitutionToSystem', null)}")
    public void addInstitutionToSystem() {
        LOG.debug("addInstitutionToSystem");
        if (systemForInstitutions != null) {
            Institution institution = (Institution) getFilterInstitution().getRealFilterValue("institution");
            if (institution != null) {
                InstitutionSystem institutionSystem = new InstitutionSystem();
                institutionSystem.setInstitution(institution);
                institutionSystem.setSystem(systemForInstitutions);
                entityManager.persist(institutionSystem);
                entityManager.flush();
            }
        }
    }

    public DataModel<SystemSummary> getRegisteredSystems(){
        List<SystemSummary> res = registeredSystemSummary;

        if (selectedInstitution!=null) {
            res = systemUnderTestService.getRegisteredSystems(currentTestingSession,selectedInstitution);
        }

        if(selectedRegistrationStatus!=null){
            List<SystemSummary> tmp = new ArrayList<SystemSummary>();
            for(SystemSummary sysSum : res){
                if(sysSum.getRegistrationStatus().equals(selectedRegistrationStatus)){
                    tmp.add(sysSum);
                }
            }
            res=tmp;
        }
        if(selectedIsAccepted!=null && (selectedIsAccepted != SelectedMenuValue.PLEASE_SELECT)){
            List<SystemSummary> tmp = new ArrayList<SystemSummary>();
            for(SystemSummary sysSum : res){
                if(sysSum.isAccepted() == (selectedIsAccepted==SelectedMenuValue.YES)){
                    tmp.add(sysSum);
                }
            }
            res=tmp;
        }
        if(selectedHasMissingDependencies!=null && (selectedHasMissingDependencies != SelectedMenuValue.PLEASE_SELECT)){
            List<SystemSummary> tmp = new ArrayList<SystemSummary>();
            for(SystemSummary sysSum : res){
                if(sysSum.hasMissingDependencies() != (selectedHasMissingDependencies== SelectedMenuValue.YES)){
                    tmp.add(sysSum);
                }
            }
            res=tmp;
        }

        final List<SystemSummary> finalRes = res;
        return new GazelleListDataModel<SystemSummary>(finalRes);
    }

    public void acceptAllListedSystems(boolean accepted) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("acceptListedSystems");
        }
//        List<SystemInSession> res = getSystemsInSession();
//        return new NoHQLDataModel<>(new ListQueryBuilderFactory<>(res));
        NoHQLDataModel<SystemSummary> dataModel = (NoHQLDataModel<SystemSummary>) getRegisteredSystems();
//        List<SystemSummary> list = dataModel.getListElements();
        List<Integer> testingSessionId = new ArrayList<Integer>();
        for(SystemSummary systemSummary : dataModel){
            if (!testingSessionId.contains(systemSummary.getSystemInSession().getTestingSession().getId())) {
                testingSessionId.add(systemSummary.getSystemInSession().getTestingSession().getId());
            }
            systemSummary.getSystemInSession().setAcceptedToSession(accepted);
            systemSummary.setAccepted(systemSummary.getSystemInSession().getAcceptedToSession());
            EntityManagerService.provideEntityManager().merge(systemSummary.getSystemInSession());
            EntityManagerService.provideEntityManager().flush();
        }

        QuartzDispatcher.instance().scheduleAsynchronousEvent("updateConnectathonResultsList", testingSessionId);
    }

    public Testability getTestabilityForProfile(IntegrationProfile profile){
        ProfileInTestingSessionQuery query = new ProfileInTestingSessionQuery();
        query.integrationProfile().id().eq(profile.getId());
        query.testingSession().id().eq(testingSessionService.getUserTestingSession().getId());
        try{
            return query.getUniqueResult().getTestability();
        }catch (Exception e){
            return null;
        }
    }

    public boolean showTestabilityColumn(IntegrationProfile profile){
        return (getTestabilityForProfile(profile) != null && applicationManager.isTestManagement());
    }

}
