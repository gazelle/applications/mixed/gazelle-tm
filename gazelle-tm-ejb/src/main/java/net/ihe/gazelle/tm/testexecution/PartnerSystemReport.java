package net.ihe.gazelle.tm.testexecution;

import java.util.Objects;

public class PartnerSystemReport {

    private String systemKeyword;
    private String table;
    private boolean isTested;

    public String getSystemKeyword() {
        return systemKeyword;
    }

    public void setSystemKeyword(String systemKeyword) {
        this.systemKeyword = systemKeyword;
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public boolean isTested() {
        return isTested;
    }

    public void setTested(boolean tested) {
        isTested = tested;
    }

    public String getTestedStyleClass(){
        if(isTested){
            return "gzl-icon-checked";
        }else {
            return "gzl-icon-times";
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PartnerSystemReport that = (PartnerSystemReport) o;
        return isTested == that.isTested && Objects.equals(systemKeyword, that.systemKeyword) && Objects.equals(table, that.table);
    }

    @Override
    public int hashCode() {
        return Objects.hash(systemKeyword, table, isTested);
    }
}
