package net.ihe.gazelle.tm.statistics.testing;

import net.ihe.gazelle.tm.gazelletest.model.instance.MonitorInSession;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstance;
import net.ihe.gazelle.tm.statistics.common.StatisticsCommonService;
import net.ihe.gazelle.tm.systems.model.SystemInSession;

import java.util.List;

public interface StatisticsForTestingPhaseService {
    List<SystemInSession> getSystemsForCurrentUser();

    List<TestInstance> getTestInstancesForASystem(SystemInSession system);

    List<TestInstance> getAllTestInstancesForCurrentSession();

    List<TestInstance> getTestInstancesWithVerificationPending();

    List<TestInstance> getLeftOverTestInstances();

    List<TestInstance> getTestInstancesForMonitorInSession(MonitorInSession monitor);

    int getNumberOfTotalTestInstanceForASystemInSession(SystemInSession system);

    int getNumberOfTotalTestInstancesForMonitorInSession(MonitorInSession monitor);

    int getNumberOfTotalTestInstances();

    int getNumberOfVerificationPendingTestInstances();

    int getNumberOfLeftOverTestInstances();

    PendingTestInstanceStats getStatisticsForEvaluationPendingTestInstances();

    LeftOverTestInstanceStats getStatisticsLeftOverTestInstances();

    StatisticsCommonService.TestInstanceStats getStatisticsForTestInstances(List<TestInstance> testInstances);

    class PendingTestInstanceStats {
        private final int nbOfToBeAssigned;
        private final int nbOfClaimed;
        private final int nbOfCritical;
        private final int nbOfPartiallyVerified;

        public PendingTestInstanceStats(int nbOfToBeVerified, int nbOfClaimed, int nbOfCritical, int nbOfPartiallyVerified) {
            this.nbOfToBeAssigned = nbOfToBeVerified;
            this.nbOfClaimed = nbOfClaimed;
            this.nbOfCritical = nbOfCritical;
            this.nbOfPartiallyVerified = nbOfPartiallyVerified;
        }

        public int getNbOfToBeAssigned() {
            return nbOfToBeAssigned;
        }

        public int getNbOfClaimed() {
            return nbOfClaimed;
        }

        public int getNbOfCritical() {
            return nbOfCritical;
        }

        public int getNbOfPartiallyVerified() {
            return nbOfPartiallyVerified;
        }
    }

    class LeftOverTestInstanceStats {
        private final int nbOfAtLeastOneDay;
        private final int nbOfAtLeastOneAndHalfDay;
        private final int nbOfMoreThanTwoDays;

        public LeftOverTestInstanceStats(int nbOfAtLeastOneDay, int nbOfAtLeastOneAndHalfDay, int nbOfMoreThanTwoDays) {
            this.nbOfAtLeastOneDay = nbOfAtLeastOneDay;
            this.nbOfAtLeastOneAndHalfDay = nbOfAtLeastOneAndHalfDay;
            this.nbOfMoreThanTwoDays = nbOfMoreThanTwoDays;
        }

        public int getNbOfAtLeastOneDay() {
            return nbOfAtLeastOneDay;
        }

        public int getNbOfAtLeastOneAndHalfDay() {
            return nbOfAtLeastOneAndHalfDay;
        }

        public int getNbOfMoreThanTwoDays() {
            return nbOfMoreThanTwoDays;
        }
    }

}
