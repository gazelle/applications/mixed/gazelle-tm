package net.ihe.gazelle.tm.application.gui.summary;


import net.ihe.gazelle.ssov7.gum.client.application.Role;
import net.ihe.gazelle.tm.financial.FinancialSummary;
import net.ihe.gazelle.tm.financial.action.services.FeesService;
import net.ihe.gazelle.tm.financial.model.Invoice;
import net.ihe.gazelle.tm.session.TestingSessionService;
import net.ihe.gazelle.tm.systems.SystemSummary;
import net.ihe.gazelle.tm.systems.SystemUnderTestService;
import net.ihe.gazelle.tm.systems.model.SystemInSessionRegistrationStatus;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import net.ihe.gazelle.tm.organization.OrganizationService;
import net.ihe.gazelle.users.model.Institution;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.security.Identity;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Name("SUTSummaryBeanGui")
@Scope(ScopeType.PAGE)
public class SUTSummaryBeanGui implements Serializable {

    private static final long serialVersionUID = 249728544993852994L;

    @In(value = "systemUnderTestService", create = true)
    private SystemUnderTestService systemUnderTestService;

    @In(value = "feesService", create = true)
    private FeesService feesService;

    @In(value = "organizationService", create = true)
    private OrganizationService organizationService;

    @In(value = "testingSessionService",create = true)
    private TestingSessionService testingSessionService;

    private TestingSession currentTestingSession;

    private Institution currentInstitution;

    private List<SystemSummary> systemsSummary;

    private FinancialSummary financialSummary;

    private Invoice invoice;

    private boolean registrationOpen;

    private boolean thereAreAcceptedSystems;

    /////////////////// init ///////////////////

    @Create
    public void init(){
        try{
            this.currentTestingSession = testingSessionService.getUserTestingSession();
            this.currentInstitution = organizationService.getCurrentOrganization();
            if(currentTestingSession != null && currentInstitution != null){
                this.registrationOpen = testingSessionService.isRegistrationOpen(currentTestingSession);
                this.systemsSummary = systemUnderTestService.getRegisteredSystems(currentTestingSession, currentInstitution);
                this.financialSummary = feesService.getFees(currentTestingSession, currentInstitution);
                if(financialSummary != null){
                    this.invoice = this.financialSummary.getInvoice();
                }
                this.thereAreAcceptedSystems = systemUnderTestService.isThereAcceptedSystem(currentTestingSession, currentInstitution);
            }
            else {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "No selected session or organization");
            }
        }
        catch (IllegalArgumentException e){
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "An error has occurred during initialization");
        }
    }


    /////////////////// Getters & Setters ///////////////////


    /////////////////// Data Retrievers ///////////////////

    public int getNumberOfRegisteredSUTs(){
        return countByStatus(systemsSummary, Arrays.asList(SystemInSessionRegistrationStatus.IN_PROGRESS, SystemInSessionRegistrationStatus.COMPLETED));
    }

    public int getNumberOfInProgressSUTs(){
        return countByStatus(systemsSummary, Collections.singletonList(SystemInSessionRegistrationStatus.IN_PROGRESS));
    }

    public int getNumberOfCompletedSUTs(){
        return countByStatus(systemsSummary, Collections.singletonList(SystemInSessionRegistrationStatus.COMPLETED));
    }

    public int getNumberOfDropped(){
        return countByStatus(systemsSummary, Collections.singletonList(SystemInSessionRegistrationStatus.DROPPED));
    }

    public int getNumberOfSystemsWithMissingDependencies(){
        int count = 0;
        for(SystemSummary systemSummary : systemsSummary){
            if(systemSummary.hasMissingDependencies() &&
                    !systemSummary.getRegistrationStatus().equals(SystemInSessionRegistrationStatus.DROPPED)){
                count++;
            }
        }
        return count;
    }


    /////////////////// Checkers ///////////////////

    public boolean checkRole(){
        return Identity.instance().hasRole(Role.VENDOR) ||
                Identity.instance().hasRole(Role.VENDOR_ADMIN);
    }

    public boolean showSUTSummary(){
        return checkRole();
    }

    public boolean organizationInRegistrationPhase(){
        return registrationOpen &&
                (!thereAreAcceptedSystems ||
                        (invoice != null && currentTestingSession.isContractRequired()
                                && !invoice.getContractReceived()));
    }

    public boolean showSUTLateAlert(){
        return !registrationOpen && !this.thereAreAcceptedSystems;
    }



    public boolean showAddImportSUTButton(){
        return registrationOpen || Identity.instance().hasRole(Role.VENDOR_LATE_REGISTRATION);
    }


    /////////////////// Links ///////////////////

    public String manageSUTsButton(){
        return "/registration/registration.seam";
    }

    public String addSUTButton(){
        return "/systems/system/createSystemInSession.seam";
    }

    public String importSUTButton(){
        return "/systems/system/copySystemInSession.seam";
    }

    /////////////////// Private Methods ///////////////////
    private int countByStatus(List<SystemSummary> systemsSummary,List<SystemInSessionRegistrationStatus> status){
        int count = 0;
        for(SystemSummary systemSummary : systemsSummary){
            if(status.contains(systemSummary.getRegistrationStatus())){
                count++;
            }
        }
        return count;
    }
}
