package net.ihe.gazelle.tm.organization.exception;

public class OrganizationCreationException extends RuntimeException {

    public OrganizationCreationException() {
    }

    public OrganizationCreationException(String message) {
        super(message);
    }

    public OrganizationCreationException(String message, Throwable throwable) {
        super(message, throwable);
    }

    public OrganizationCreationException(Throwable throwable) {
        super(throwable);
    }
}
