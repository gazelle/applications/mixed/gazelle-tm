package net.ihe.gazelle.tm.gazelletest.action.conformitytestreport.dao;

import net.ihe.gazelle.tm.gazelletest.model.instance.Status;

import java.util.HashMap;
import java.util.Map;

public class TestInstanceStatusMapper {

    private static final Map<Status, String> map = new HashMap<>();

    static {
        for (Status status : Status.values()) {
            map.put(status, status.name());
        }
        map.put(Status.STARTED, "RUNNING");
        map.put(Status.COMPLETED, "TO_BE_VERIFIED");
    }

    public String asString(Status status) {
        return map.get(status);
    }
}
