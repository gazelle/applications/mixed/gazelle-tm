package net.ihe.gazelle.tm.application.gui.summary;

import flex.messaging.io.PageableRowSet;
import net.ihe.gazelle.menu.Pages;
import net.ihe.gazelle.ssov7.gum.client.application.Role;
import net.ihe.gazelle.tm.session.AttendeeService;
import net.ihe.gazelle.tm.session.TestingSessionService;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import net.ihe.gazelle.tm.organization.OrganizationService;
import net.ihe.gazelle.users.model.Institution;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.security.Identity;

import java.io.Serializable;

@Name("teamSummaryBeanGui")
@Scope(ScopeType.PAGE)
public class TeamSummaryBeanGui implements Serializable {


    private static final long serialVersionUID = 7246565195586549871L;
    private TestingSession currentTestingSession;

    @In(value = "testingSessionService",create = true)
    private TestingSessionService testingSessionService;


    @In(value = "attendeeService",create = true)
    private AttendeeService attendeeService;

    @In(value = "organizationService", create = true)
    private OrganizationService organizationService;


    private Institution currentInstitution;


    /////////////////// init ///////////////////

    @Create
    public void init(){
        currentTestingSession = testingSessionService.getUserTestingSession();
        this.currentInstitution = organizationService.getCurrentOrganization();

    }

    /////////////////// Checkers ///////////////////

    public boolean showTeamSummary(){
        return Identity.instance().isLoggedIn();
    }

    public boolean isRegistrationAllowed(){
        return testingSessionService.isAttendeesRegistrationEnabled(currentTestingSession);
    }

    public boolean canInviteMembers(){
        return Identity.instance().hasRole(Role.VENDOR_ADMIN);
    }

    /////////////////// Data Retrievers ///////////////////

    public long getNumberOfActiveMembers(){
        return attendeeService.getNumberOfActiveMembers(currentInstitution);
    }

    public long getNumberOfPendingMembers(){
        return attendeeService.getNumberOfPendingMembers(currentInstitution);
    }

    public long getNumberOfRegisteredMembers(){
        return attendeeService.getNumberOfRegisteredAttendees(currentTestingSession, currentInstitution);
    }


    /////////////////// Links ///////////////////

    public String inviteTeamMembersButton(){
        return Pages.ADMIN_MANAGE_USERS.getMenuLink();
    }

    public String manageAttendeesButton(){
        return "/users/connectathon/listParticipants.seam";
    }

}
