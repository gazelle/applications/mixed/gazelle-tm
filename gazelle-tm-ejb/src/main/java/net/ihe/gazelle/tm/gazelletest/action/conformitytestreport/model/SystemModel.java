package net.ihe.gazelle.tm.gazelletest.action.conformitytestreport.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "system")
@XmlAccessorType(XmlAccessType.FIELD)
public class SystemModel {

    @XmlElement(name = "id")
    private Integer idSystem;

    @XmlElement(name = "name")
    private String name;

    @XmlElement(name = "keyword")
    private String keyword;

    @XmlElement(name = "idTestingSession")
    private Integer idTestingSession;

    @XmlElement(name = "testingSessionDescription")
    private String description;

    @XmlElement(name = "organisationId")
    private Integer organisationId;

    public SystemModel() {}

    public SystemModel(Integer idSystem, String name, String keyword, Integer idTestingSession, String description, Integer organisationId) {
        this.idSystem = idSystem;
        this.name = name;
        this.keyword = keyword;
        this.idTestingSession = idTestingSession;
        this.description = description;
        this.organisationId = organisationId;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getIdSystem() {
        return idSystem;
    }

    public void setIdSystem(Integer idSystem) {
        this.idSystem = idSystem;
    }

    public Integer getIdTestingSession() {
        return idTestingSession;
    }

    public void setIdTestingSession(Integer idTestingSession) {
        this.idTestingSession = idTestingSession;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getOrganisationId() {
        return organisationId;
    }

    public void setOrganisationId(Integer organisationId) {
        this.organisationId = organisationId;
    }
}
