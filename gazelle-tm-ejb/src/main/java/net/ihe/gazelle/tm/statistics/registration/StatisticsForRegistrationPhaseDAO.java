package net.ihe.gazelle.tm.statistics.registration;


import net.ihe.gazelle.tm.systems.model.SystemInSession;

import java.util.List;

public interface StatisticsForRegistrationPhaseDAO {


    List<SystemInSession> getAllSystemForCurrentTestingSession();

}
