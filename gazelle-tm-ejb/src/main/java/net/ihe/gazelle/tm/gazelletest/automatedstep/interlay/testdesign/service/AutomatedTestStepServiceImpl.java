package net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.testdesign.service;

import net.ihe.gazelle.tm.gazelletest.automatedstep.domain.script.InputDefinition;
import net.ihe.gazelle.tm.gazelletest.automatedstep.domain.script.Script;
import net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.script.dao.TestStepScriptDAO;
import net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.script.validation.ScriptValidationService;
import net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.testdesign.dao.AutomatedTestStepDAO;
import net.ihe.gazelle.tm.gazelletest.domain.AutomatedTestStepInputDomain;
import net.ihe.gazelle.tm.gazelletest.domain.TestStepDomain;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@Name("automatedTestStepService")
@Scope(ScopeType.STATELESS)
@AutoCreate
public class AutomatedTestStepServiceImpl implements AutomatedTestStepService {

    @In(value = "testStepScriptDAO")
    private TestStepScriptDAO testStepScriptDAO;
    @In(value = "automatedTestStepDAO")
    private AutomatedTestStepDAO automatedTestStepDAO;
    @In(value = "scriptValidationService")
    private ScriptValidationService scriptValidationService;

    public AutomatedTestStepServiceImpl() {
        // empty for injection
    }

    public AutomatedTestStepServiceImpl(TestStepScriptDAO testStepScriptDAO, AutomatedTestStepDAO automatedTestStepDAO, ScriptValidationService scriptValidationService) {
        this.testStepScriptDAO = testStepScriptDAO;
        this.automatedTestStepDAO = automatedTestStepDAO;
        this.scriptValidationService = scriptValidationService;
    }

    @Override
    public TestStepDomain createTestStep(TestStepDomain testStep, Script script) {
        for (InputDefinition inputDefinition : script.getInputs()) {
            testStep.addInput(new AutomatedTestStepInputDomain(inputDefinition.getKey(), inputDefinition.getLabel(), inputDefinition.getFormat(), inputDefinition.getType(), inputDefinition.getRequired()));
        }
        TestStepDomain testSteps = automatedTestStepDAO.createTestStep(testStep);
        testStepScriptDAO.saveScript(testSteps, script);
        return testSteps;
    }

    @Override
    public TestStepDomain editTestStep(TestStepDomain testStep, String newScriptName, Script script) {
        TestStepDomain editedTestStep = new TestStepDomain(testStep);
        editedTestStep.setTestScriptID(newScriptName);
        TestStepDomain testSteps = automatedTestStepDAO.editTestStep(editedTestStep, script.getInputs());
        testStepScriptDAO.deleteScript(testStep);
        testStepScriptDAO.saveScript(editedTestStep, script);
        return testSteps;
    }

    @Override
    public TestStepDomain copyTestStep(TestStepDomain automatedStepCopy, Integer stepToCopyId) {
        Script scriptToCopy = readScript(stepToCopyId, automatedStepCopy.getTestScriptID());
        TestStepDomain createdTestStep = automatedTestStepDAO.createTestStep(automatedStepCopy);
        testStepScriptDAO.saveScript(createdTestStep, scriptToCopy);
        return createdTestStep;
    }

    @Override
    public void deleteTestStep(TestStepDomain testStep) {
        automatedTestStepDAO.deleteTestStep(testStep);
        testStepScriptDAO.deleteScript(testStep);
    }

    @Override
    public Script readScript(Integer stepId, String scriptName) {
        String content = testStepScriptDAO.readScript(stepId, scriptName);
        return scriptValidationService.parse(content);
    }
}
