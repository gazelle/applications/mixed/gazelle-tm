package net.ihe.gazelle.tm.systems.action;

import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import net.ihe.gazelle.ssov7.gum.client.application.Role;
import net.ihe.gazelle.ssov7.gum.client.application.User;
import net.ihe.gazelle.tm.application.action.ApplicationManager;
import net.ihe.gazelle.tm.session.TestingSessionService;
import net.ihe.gazelle.tm.systems.model.System;
import net.ihe.gazelle.tm.systems.model.*;
import net.ihe.gazelle.users.model.Institution;
import org.jboss.seam.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by gthomazon on 05/07/17.
 */
public class SystemInSessionBuilder extends AbstractSystemInSessionBuilder {

    private static final Logger LOG = LoggerFactory.getLogger(SystemInSessionBuilder.class);
    private TestingSessionService testingSessionService = (TestingSessionService) Component.getInstance("testingSessionService");

    public SystemInSessionBuilder() {
    }

    public SystemInSessionBuilder(SystemType sysType, String sysversion, String syskeywordSuffix, String sysname, Institution
            institutionForCreation, User sysOwnerUser) {
        super(sysType, sysversion, syskeywordSuffix, sysname, institutionForCreation, sysOwnerUser);
    }

    public SystemInSessionBuilder(SystemType sysType, String sysversion, String syskeywordSuffix, String sysname, Institution
            institutionForCreation, User sysOwnerUser, SystemInSession systemInSession) {
        super(sysType, sysversion, syskeywordSuffix, sysname, institutionForCreation, sysOwnerUser, systemInSession);
    }

    private Institution getInstitutionOfCurrentUser() {
        return Institution.getLoggedInInstitution();
    }

    private void setDefaultRegistrationStatus() {
        systemInSession.setRegistrationStatus(SystemInSessionRegistrationStatus.IN_PROGRESS);
    }

    /**
     * Add (create/update) a system to the database This operation is allowed for some granted users (check the security.drl)
     * @param identity
     */
    public void addSystemForTM(GazelleIdentity identity) throws SystemActionException {
        if (LOG.isDebugEnabled()) {
            LOG.debug("addSystemForTM");
        }
        System newSystem = initSystemWithFormValues();
        createNewSystemInSession(identity, newSystem);
        calculateFinancialSummaryDTO();
    }

    public System initSystemWithFormValues() {
        generateSystemForSystemInSession(systemInSession);
        System newSystem = systemInSession.getSystem();
        newSystem.setName(sysname);
        newSystem.setSystemType(sysType);
        newSystem.setVersion(sysversion);
        newSystem.setKeywordSuffix(syskeywordSuffix);
        newSystem.setOwnerUserId(sysOwnerUser.getId());
        return newSystem;
    }

    private void createNewSystemInSession(GazelleIdentity identity, System newSystem) throws SystemActionException {
        if (institutionForCreation != null) {
            newSystem.setInstitutionForCreation(institutionForCreation);
        }
        if (isSystemKeywordValid(identity)) {
            buildRealKeyword();

            newSystem = entityManager.merge(newSystem);
            InstitutionSystem institutionSystem = new InstitutionSystem(newSystem, institutionForCreation);
            institutionSystem = entityManager.merge(institutionSystem);

            TableSession tableSession = TableSession.getTableSessionByKeyword(TableSession
                    .getDEFAULT_TABLE_SESSION_STRING());
            SystemInSessionStatus systemInSessionStatus = SystemInSessionStatus.getSTATUS_NOT_HERE_YET();
            TestingSession selectedTestingSession = testingSessionService.getUserTestingSession();
            systemInSession = new SystemInSession(tableSession, newSystem,
                    selectedTestingSession, null, systemInSessionStatus);
            if (selectedTestingSession.isSystemAutoAcceptance()) {
                systemInSession.setAcceptedToSession(true);
            }
            Set<InstitutionSystem> is = new HashSet<InstitutionSystem>();
            is.add(institutionSystem);
            newSystem.setInstitutionSystems(is);

            setDefaultRegistrationStatus();

            systemInSession.setIsCopy(false);

            systemInSession = entityManager.merge(systemInSession);
            try {
                entityManager.flush();
            } catch (ConstraintViolationException e){
                LOG.error("Error saving system : ", e);
                for(ConstraintViolation constraintViolation: e.getConstraintViolations()) {
                    throw new SystemActionException("Error on field " + constraintViolation.getPropertyPath() + " with message : " + constraintViolation.getMessage());
                }
            }
        }
    }

    private void generateSystemForSystemInSession(SystemInSession sis) {
        System system = new System();

        User systemOwner = new User();
        if (sysOwnerUser.hasRole(Role.VENDOR_ADMIN) || sysOwnerUser.hasRole(Role.VENDOR)) {
            systemOwner = sysOwnerUser;
        }

        if (ApplicationManager.instance().isProductRegistry()) {
            SystemTypeQuery query = new SystemTypeQuery();
            SystemType systemType = query.getUniqueResult();
            system.setSystemType(systemType);

            system.setIntegrationStatementDate(new Date());
        } else {
            system.setSystemType(null);
        }
        system.setOwnerUserId(systemOwner.getId());

        sis.setSystem(system);
    }
}
