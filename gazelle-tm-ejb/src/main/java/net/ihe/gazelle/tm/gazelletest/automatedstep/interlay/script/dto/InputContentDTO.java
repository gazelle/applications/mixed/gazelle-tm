package net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.script.dto;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonSetter;
import net.ihe.gazelle.tm.gazelletest.automatedstep.domain.script.InputContent;

import java.io.Serializable;

@JsonPropertyOrder({"key", "value"})
public class InputContentDTO implements Serializable {

    private static final long serialVersionUID = -1704792943820271139L;

    @JsonIgnore
    private InputContent input;

    public InputContentDTO() {
        this.input = new InputContent();
    }

    public InputContentDTO(InputContent input) {
        this.input = input;
    }

    @JsonGetter("key")
    public String getKey() {
        return input.getKey();
    }

    @JsonSetter("key")
    public void setKey(String key) {
        input.setKey(key);
    }

    @JsonGetter("value")
    public String getValue() {
        return input.getValue();
    }

    @JsonSetter("value")
    public void setValue(String value) {
        input.setValue(value);
    }

    @JsonIgnore
    public InputContent getInput() {
        return input;
    }

    @JsonIgnore
    public void setInput(InputContent input) {
        this.input = input;
    }
}
