package net.ihe.gazelle.tm.gazelletest.action;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.common.filter.NoHQLFilter;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import net.ihe.gazelle.ssov7.gum.client.application.Group;
import net.ihe.gazelle.ssov7.gum.client.application.Role;
import net.ihe.gazelle.ssov7.gum.client.application.User;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import net.ihe.gazelle.ssov7.gum.client.interlay.client.UserRestQueryBuilder;
import net.ihe.gazelle.ssov7.gum.client.interlay.client.utils.UserSearchParams;
import net.ihe.gazelle.tm.datamodel.MonitorUser;
import net.ihe.gazelle.tm.filter.TMCriterions;
import net.ihe.gazelle.tm.filter.modifier.TestMatchingTestingSession;
import net.ihe.gazelle.tm.gazelletest.model.definition.Test;
import net.ihe.gazelle.tm.gazelletest.model.definition.TestEntity;
import net.ihe.gazelle.tm.gazelletest.model.definition.TestQuery;
import net.ihe.gazelle.tm.gazelletest.model.instance.MonitorInSession;
import net.ihe.gazelle.tm.gazelletest.model.instance.MonitorInSessionQuery;
import net.ihe.gazelle.tm.users.filter.UserFilterImpl;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Remove;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.*;

@Name("monitorSearchFilter")
@Scope(ScopeType.PAGE)
public class MonitorSearchFilter implements Serializable, QueryModifier<MonitorInSession> {

    private static final long serialVersionUID = -782240547894724760L;
    private static final Logger LOG = LoggerFactory.getLogger(MonitorSearchFilter.class);

    private FilterDataModel<MonitorInSession> monitors;
    private List<Test> monitorTests;
    private MonitorInSession displayedMonitor;
    private Filter<Test> testFilter;
    private MonitorInSession selectedMonitor;
    private transient UserFilterImpl userFilter;
    @In
    private GazelleIdentity identity;
    @In(value = "gumUserService")
    private transient UserService userService;

    public MonitorInSession getSelectedMonitor() {
        return selectedMonitor;
    }

    public void setSelectedMonitor(MonitorInSession selectedMonitor) {
        this.selectedMonitor = selectedMonitor;
    }

    @Create
    public void create() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("create");
        }
        FacesContext fc = FacesContext.getCurrentInstance();
        Map<String, String> requestParameterMap = fc.getExternalContext().getRequestParameterMap();

        HQLCriterionsForFilter<MonitorInSession> criterions = getCriterions();
        criterions.addQueryModifier(this);
        Filter<MonitorInSession> filter = new Filter<>(criterions, requestParameterMap);
        monitors = new FilterDataModel<MonitorInSession>(filter) {
            private static final long serialVersionUID = 1L;

            @Override
            protected Object getId(MonitorInSession t) {
                // TODO Auto-generated method stub
                return t.getId();
            }
        };

        userFilter = new UserFilterImpl(requestParameterMap, identity);
        userFilter.addListener(filter);
    }

    public FilterDataModel<MonitorInSession> getElements() {
        return monitors;
    }

    private List<String> idsOf(List<User> users) {
        List<String> userIds = new ArrayList<>();
        for (User user : users) {
            userIds.add(user.getId());
        }
        return userIds;
    }

    private HQLCriterionsForFilter<MonitorInSession> getCriterions() {
        MonitorInSessionQuery query = new MonitorInSessionQuery();
        HQLCriterionsForFilter<MonitorInSession> result = query.getHQLCriterionsForFilter();

        TestEntity<Test> testEntity = query.monitorTests().test();
        TMCriterions.addTestingSession(result, "testing_session", query.testingSession(), identity);
        TMCriterions.addAIPOCriterionsUsingTest(result, testEntity, "testing_session");

        result.addPath("user_id", query.userId());
        result.addPath("test", testEntity);
        result.addPath("active", query.isActivated(), true);
        result.addQueryModifierForCriterion("test", new TestMatchingTestingSession(testEntity, "testing_session"));

        return result;
    }

    @Remove
    @Destroy
    public void destroy() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("destroy");
        }

    }

    public void clear() {
        monitors.getFilter().clear();
        userFilter.clear();
    }

    public FilterDataModel<MonitorInSession> getMonitors() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getMonitors");
        }
        return monitors;
    }

    public MonitorUser getUserFromMonitor(MonitorInSession monitorInSession) {
        try {
            User user = userService.getUserById(monitorInSession.getUserId());
            return new MonitorUser(user, monitorInSession);
        } catch (NoSuchElementException e) {
            return null;
        }
    }

    public MonitorUser getUserDisplayed() {
        if (displayedMonitor != null) {
            return getUserFromMonitor(displayedMonitor);
        } else {
            return new MonitorUser(new User(), new MonitorInSession());
        }
    }

    public void getMonitorInSessionById() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getMonitorInSessionById");
        }
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String monitorId = params.get("id");
        if ((monitorId != null) && !monitorId.isEmpty()) {
            try {
                Integer id = Integer.parseInt(monitorId);
                EntityManager em = EntityManagerService.provideEntityManager();
                displayedMonitor = em.find(MonitorInSession.class, id);
            } catch (NumberFormatException e) {
                LOG.error("monitorId is empty or not well-formed: " + monitorId);
                displayedMonitor = null;
            }
        } else {
            displayedMonitor = null;
        }
    }

    public String getUrlParameters() {
        if (userFilter != null && userFilter.getFilterValues().containsKey(UserRestQueryBuilder.SEARCH) &&
                userFilter.getFilterValues().get(UserRestQueryBuilder.SEARCH) != null &&
                !userFilter.getFilterValues().get(UserRestQueryBuilder.SEARCH).isEmpty()) {
            return monitors.getFilter().getUrlParameters() + "&" + userFilter.getUrlParameters();
        } else {
            return monitors.getFilter().getUrlParameters();
        }
    }

    public void setUrlParameters(String urlParameters) {
        // don't need to write here just to prevent EL exception from jsf
    }

    public MonitorInSession getDisplayedMonitor() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getDisplayedMonitor");
        }
        return displayedMonitor;
    }

    public void showTestsForMonitor(MonitorUser monitor) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("showTestsForMonitor");
        }
        setSelectedMonitor(monitor.getMonitorInSession());
        monitorTests = new ArrayList<>(monitor.getMonitorInSession().getTestList());
    }

    public List<Test> getTestsDisplay() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getTestsDisplay");
        }
        return monitorTests;
    }

    public FilterDataModel<Test> listTestsDisplay() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("listTestsDisplay");
        }
        return new FilterDataModel<Test>(getFilter()) {
            @Override
            protected Object getId(Test test) {
                return test.getId();
            }
        };
    }

    public NoHQLFilter<User> getUserFilter() {
        LOG.trace("getFilter");
        if (userFilter == null) {
            FacesContext fc = FacesContext.getCurrentInstance();
            Map<String, String> requestParameterMap = new HashMap<>(fc.getExternalContext().getRequestParameterMap());
            userFilter = new UserFilterImpl(requestParameterMap, identity);
        }
        return userFilter;
    }

    public Filter<Test> getFilter() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getFilter");
        }
        if (testFilter == null) {
            TestQuery q = new TestQuery();
            HQLCriterionsForFilter<Test> result = q.getHQLCriterionsForFilter();
            result.addQueryModifier(new QueryModifier<Test>() {
                @Override
                public void modifyQuery(HQLQueryBuilder<Test> hqlQueryBuilder, Map<String, Object> map) {
                    if (selectedMonitor != null) {
                        TestQuery q = new TestQuery();
                        hqlQueryBuilder.addRestriction(q.monitorsInSession().id().eqRestriction(selectedMonitor.getId()));
                    }
                }
            });
            this.testFilter = new Filter<>(result);
        }
        return this.testFilter;
    }

    @Override
    public void modifyQuery(HQLQueryBuilder<MonitorInSession> hqlQueryBuilder, Map<String, Object> map) {
        String search = userFilter.getFilterValues().get(UserRestQueryBuilder.SEARCH);
        if (search != null && !search.isEmpty()) {
            UserService service = (UserService) Component.getInstance("gumUserService");
            UserSearchParams userSearchParams = new UserSearchParams()
                .setSearch(search).setGroup(Group.GRP_MONITOR).setActivated(true);
            List<User> users = service.searchNoLimit(userSearchParams);
            hqlQueryBuilder.addIn("userId", idsOf(users));
        }
    }
}
