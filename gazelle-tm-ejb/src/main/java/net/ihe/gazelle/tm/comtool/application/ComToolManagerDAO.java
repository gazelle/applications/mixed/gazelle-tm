package net.ihe.gazelle.tm.comtool.application;

import net.ihe.gazelle.ssov7.gum.client.application.User;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstance;
import net.ihe.gazelle.tm.systems.model.System;
import net.ihe.gazelle.tm.systems.model.TestingSession;

import java.util.List;

public interface ComToolManagerDAO {

   List<TestingSession> getOpenAndActiveTestingSessions();

   List<User> getActiveParticipants(TestingSession testingSession);

   List<User> getActiveAndAcceptedParticipants(TestingSession testingSession);

   List<User> getActiveMonitors(TestingSession testingSession);

   List<User> getActiveAdmins(TestingSession testingSession);

   List<System> getTestRunSystems(TestInstance testInstance);

}
