package net.ihe.gazelle.tm.gazelletest.action;



import java.io.Serializable;

/**
 * Expected exceptions occuring during a test using an external tool when the used token is invalid.
 *
 * @author nbt
 */

public class InvalidTokenException extends RuntimeException implements Serializable {

    private static final long serialVersionUID = 2868216697949737627L;

    public InvalidTokenException() {
    }

    public InvalidTokenException(String s) {
        super(s);
    }

    public InvalidTokenException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public InvalidTokenException(Throwable throwable) {
        super(throwable);
    }
}
