package net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.testdesign.dao;

import net.ihe.gazelle.tm.gazelletest.automatedstep.domain.script.InputDefinition;
import net.ihe.gazelle.tm.gazelletest.domain.TestStepDomain;

import java.util.List;

public interface AutomatedTestStepDAO {

    /**
     * Create an automated test step for test definition and create its input definition
     * @param testStep the domain object representing the test step to be persisted
     * @return the domain object updated with the auto incremented id after creation
     */
    TestStepDomain createTestStep(TestStepDomain testStep);

    /**
     * Edit an existing automated test step, it will delete and recreate the input definition
     * @param testStep the domain object representing the test step to be edited
     * @param inputDefinitions the list of computed input from the json script uploaded by test designer
     * @return the domain object updated after edition
     */
    TestStepDomain editTestStep(TestStepDomain testStep, List<InputDefinition> inputDefinitions);

    /**
     * Delete an existing automated test step
     * @param testStep the domain object representing the test step to be deleted
     */
    void deleteTestStep(TestStepDomain testStep);
}
