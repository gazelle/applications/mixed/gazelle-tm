package net.ihe.gazelle.tm.gazelletest.action.conformitytestreport.dao;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManagerImpl;
import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import net.ihe.gazelle.tm.filter.TMCriterions;
import net.ihe.gazelle.tm.filter.modifier.SystemNotTool;
import net.ihe.gazelle.tm.filter.valueprovider.InstitutionFixer;
import net.ihe.gazelle.tm.gazelletest.action.ConnectathonResultDataModel;
import net.ihe.gazelle.tm.gazelletest.action.conformitytestreport.model.TestInstanceReport;
import net.ihe.gazelle.tm.gazelletest.bean.*;
import net.ihe.gazelle.tm.gazelletest.model.definition.MetaTest;
import net.ihe.gazelle.tm.gazelletest.model.definition.TestRoles;
import net.ihe.gazelle.tm.gazelletest.model.definition.TestRolesQuery;
import net.ihe.gazelle.tm.gazelletest.model.definition.TestType;
import net.ihe.gazelle.tm.gazelletest.model.instance.*;
import net.ihe.gazelle.tm.session.TestingSessionService;
import net.ihe.gazelle.tm.systems.model.System;
import net.ihe.gazelle.tm.systems.model.*;
import net.ihe.gazelle.users.model.Institution;
import net.ihe.gazelle.users.model.InstitutionQuery;
import org.jboss.seam.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.util.*;

public class ConformityTestReportDAOImpl implements ConformityTestReportDAO {

    private static final Logger LOG = LoggerFactory.getLogger(ConformityTestReportDAOImpl.class);
    private static final String READY = "ready";

    private final ConnectathonResultDataModel datamodel;

    private ApplicationPreferenceManager applicationPreferenceManager;

    private TestInstanceStatusMapper statusMapper = new TestInstanceStatusMapper();

    private TestingSessionService testingSessionService = (TestingSessionService) Component.getInstance("testingSessionService");

    public ConformityTestReportDAOImpl(GazelleIdentity identity) {
        this.datamodel = new ConnectathonResultDataModel(createFilter(identity));
        this.applicationPreferenceManager = new ApplicationPreferenceManagerImpl();
    }

    private Filter<SystemAIPOResultForATestingSession> createFilter(GazelleIdentity identity) {
        SystemAIPOResultForATestingSessionQuery query = new SystemAIPOResultForATestingSessionQuery();
        HQLCriterionsForFilter<SystemAIPOResultForATestingSession> hqlCriterions = query.getHQLCriterionsForFilter();

        TMCriterions.addTestingSession(hqlCriterions, "testSession", query.testSession(), identity);
        TMCriterions.addAIPOCriterions(hqlCriterions, query.systemActorProfile().actorIntegrationProfileOption());
        hqlCriterions.addPath("institution", query.systemActorProfile().system().institutionSystems().institution(),
                null, InstitutionFixer.INSTANCE);
        hqlCriterions.addPath("system", query.systemActorProfile().system());
        hqlCriterions.addQueryModifier(new SystemNotTool(query.systemActorProfile().system()));
        hqlCriterions.addPath("statusResult", query.status());
        hqlCriterions.addPath("testingDepth", query.systemActorProfile().testingDepth());
        hqlCriterions.addPath("testTypeReal", query.systemActorProfile().aipo().testParticipants().roleInTest()
                .testRoles().test().testType());
        return new Filter<>(hqlCriterions);
    }

    /**
     * AGB: change from private to public to use the method in reporting4TestingSession.xhtml
     *
     * @param systemAIPOResult SystemAIPOResultForATestingSession
     * @return List of SAPResultDetail
     */
    @Override
    public List<SAPResultDetailItem> computeDetail(SystemAIPOResultForATestingSession systemAIPOResult,
                                                   boolean isInteroperability) {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        return computeDetail(systemAIPOResult, entityManager, isInteroperability);
    }

    @Override
    public List<SAPResultDetailItem> computeDetail(SystemAIPOResultForATestingSession systemAIPOResult) {
        return computeDetail(systemAIPOResult, false);
    }

    private void sapResultDetailListIteration1(List<SAPResultDetailItem> result) {
        for (SAPResultDetailItem detailItem : result) {
            if (detailItem instanceof SAPResultDetailTestRoleItem) {
                SAPResultDetailTestRoleItem sapResultDetailTestRoleItem = (SAPResultDetailTestRoleItem) detailItem;
                MetaTest metaTest = sapResultDetailTestRoleItem.getTestRoles().getMetaTest();
                if (metaTest != null) {
                    sapResultDetailListIteration2(result, sapResultDetailTestRoleItem, metaTest);
                }
            }
        }
    }

    private void sapResultDetailListIteration2(List<SAPResultDetailItem> result, SAPResultDetailTestRoleItem sapResultDetailTestRoleItem, MetaTest metaTest) {
        for (SAPResultDetailItem detailItem2 : result) {
            if (detailItem2 instanceof SAPResultDetailMetaTestItem) {
                SAPResultDetailMetaTestItem sapResultDetailMetaTestItem = (SAPResultDetailMetaTestItem) detailItem2;
                MetaTest metaTest2 = sapResultDetailMetaTestItem.getMetaTest();
                if (metaTest.getId().equals(metaTest2.getId())) {
                    sapResultDetailTestRoleItem.setResultMetaTest(sapResultDetailMetaTestItem);
                }
            }
        }
    }

    private List<SAPResultDetailItem> computeDetail(SystemAIPOResultForATestingSession systemAIPOResult,
                                                   EntityManager entityManager, boolean isInteroperability) {
        LOG.info("Executing computeDetail(SystemAIPOResultForATestingSession, entityManager, isInteroperability)");

        List<SAPResultDetailItem> result = new ArrayList<>();

        SystemActorProfiles systemActorProfile = systemAIPOResult.getSystemActorProfile();
        System system = systemActorProfile.getSystem();

        List<TestRoles> rolesWithTests = getTestRolesWithTests(systemAIPOResult, systemActorProfile, system,
                isInteroperability);

        for (TestRoles testRoles : rolesWithTests) {
            SAPResultDetailTestRoleItem detailTR = new SAPResultDetailTestRoleItem(testRoles, systemAIPOResult);
            result.add(detailTR);
        }

        List<TestRoles> rolesWithMetaTests = getTestRolesWithMetaTests(systemAIPOResult, systemActorProfile, system,
                isInteroperability);

        for (TestRoles testRoles : rolesWithMetaTests) {
            SAPResultDetailTestRoleItem detailTR = new SAPResultDetailTestRoleItem(testRoles, systemAIPOResult);
            result.add(detailTR);
            SAPResultDetailMetaTestItem.addAndCreateIfNeeded(detailTR, result);
        }

        sapResultDetailListIteration1(result);

        List<TestRoles> roles = getTestRoles(systemAIPOResult, systemActorProfile, system, isInteroperability);

        Set<Integer> roleInTestIds = new HashSet<>();
        for (TestRoles testRoles : roles) {
            roleInTestIds.add(testRoles.getRoleInTest().getId());
        }

        TestInstanceParticipantsQuery tipQuery = new TestInstanceParticipantsQuery();
        List<TestType> testTypes = new ArrayList<>(systemAIPOResult.getTestSession().getTestTypes());

        if (isInteroperability && testingSessionService.getUserTestingSession().isTestingInteroperability()) {
            tipQuery.testInstance().test().testType().id().eq(TestType.getTYPE_INTEROPERABILITY().getId());
        } else {
            testTypes.remove(TestType.getTYPE_INTEROPERABILITY());
            tipQuery.testInstance().test().testType().in(testTypes);
        }

        tipQuery.roleInTest().testParticipantsList().actorIntegrationProfileOption()
                .eq(systemActorProfile.getActorIntegrationProfileOption());
        tipQuery.systemInSessionUser().systemInSession().system().id().eq(system.getId());
        tipQuery.systemInSessionUser().systemInSession().testingSession().eq(systemAIPOResult.getTestSession());
        tipQuery.roleInTest().id().in(roleInTestIds);
        List<TestInstanceParticipants> tip = tipQuery.getList();

        for (TestInstanceParticipants testInstanceParticipants : tip) {
            for (SAPResultDetailItem detailItem : result) {
                detailItem.tryAddTestInstance(testInstanceParticipants.getTestInstance());
            }
        }

        TestingSession activatedTestingSession = systemAIPOResult.getTestSession();

        for (SAPResultDetailItem detailItem : result) {

            AIPOSystemPartners partners = detailItem.getPartners();
            List<TestRoles> testRolesList = detailItem.getTestRolesList();
            PartnersStatisticsBuilder.get(activatedTestingSession).addPartners(entityManager, partners,
                    systemActorProfile, testRolesList);
        }

        Collections.sort(result);

        return result;
    }

    private List<TestRoles> getTestRolesWithMetaTests(SystemAIPOResultForATestingSession systemAIPOResult,
                                                      SystemActorProfiles systemActorProfile, System system, boolean isInteroperability) {
        LOG.info("Executing getTestRolesWithMetaTests(SystemAIPOResultForATestingSession, SystemActorProfiles, System, isInteroperability)");

        TestRolesQuery trQuery = new TestRolesQuery();

        trQuery.roleInTest().testParticipantsList().tested().eq(Boolean.TRUE);
        List<TestType> testTypes = new ArrayList<>(systemAIPOResult.getTestSession().getTestTypes());

        if (isInteroperability && testingSessionService.getUserTestingSession().isTestingInteroperability()) {
            trQuery.test().testType().id().eq(TestType.getTYPE_INTEROPERABILITY().getId());
        } else {
            testTypes.remove(TestType.getTYPE_INTEROPERABILITY());
            trQuery.test().testType().in(testTypes);
        }
        trQuery.test().testStatus().keyword().eq(READY);

        Integer selectedTestPeerTypeId = datamodel.getSelectedTestPeerTypeId();
        if ((selectedTestPeerTypeId != null) && (selectedTestPeerTypeId != -1)) {
            trQuery.test().testPeerType().id().eq(selectedTestPeerTypeId);
        }
        trQuery.metaTest().isNotEmpty();
        trQuery.roleInTest().addFetch();
        trQuery.roleInTest().testParticipantsList().addFetch();
        trQuery.roleInTest().testParticipantsList().actorIntegrationProfileOption().addFetch();
        trQuery.roleInTest().testParticipantsList().aipo().addFetch();

        trQuery.roleInTest().testParticipantsList().aipo().systemActorProfiles().system().eq(system);

        trQuery.roleInTest().testParticipantsList().actorIntegrationProfileOption()
                .eq(systemActorProfile.getActorIntegrationProfileOption());

        trQuery.test().keyword().order(true);

        return trQuery.getList();
    }

    private List<TestRoles> getTestRolesWithTests(SystemAIPOResultForATestingSession systemAIPOResult,
                                                  SystemActorProfiles systemActorProfile, System system, boolean isInteroperability) {
        LOG.info("Executing getTestRolesWithTests(SystemAIPOResultForATestingSession, SystemActorProfiles, System, isInteroperability)");

        TestRolesQuery trQuery = new TestRolesQuery();

        trQuery.roleInTest().testParticipantsList().tested().eq(Boolean.TRUE);
        List<TestType> testTypes = new ArrayList<>(systemAIPOResult.getTestSession().getTestTypes());

        if (isInteroperability && testingSessionService.getUserTestingSession().isTestingInteroperability()) {
            trQuery.test().testType().id().eq(TestType.getTYPE_INTEROPERABILITY().getId());
        } else {
            testTypes.remove(TestType.getTYPE_INTEROPERABILITY());
            trQuery.test().testType().in(testTypes);
        }

        trQuery.test().testStatus().keyword().eq(READY);

        Integer selectedTestPeerTypeId = datamodel.getSelectedTestPeerTypeId();
        if ((selectedTestPeerTypeId != null) && (selectedTestPeerTypeId != -1)) {
            trQuery.test().testPeerType().id().eq(selectedTestPeerTypeId);
        }
        trQuery.metaTest().isEmpty();
        trQuery.roleInTest().addFetch();
        trQuery.roleInTest().testParticipantsList().addFetch();
        trQuery.roleInTest().testParticipantsList().actorIntegrationProfileOption().addFetch();
        trQuery.roleInTest().testParticipantsList().aipo().addFetch();

        trQuery.roleInTest().testParticipantsList().aipo().systemActorProfiles().system().eq(system);

        trQuery.roleInTest().testParticipantsList().actorIntegrationProfileOption()
                .eq(systemActorProfile.getActorIntegrationProfileOption());

        trQuery.test().keyword().order(true);

        return trQuery.getList();
    }

    private List<TestRoles> getTestRoles(SystemAIPOResultForATestingSession systemAIPOResult,
                                         SystemActorProfiles systemActorProfile, System system, boolean isInteroperability) {
        LOG.info("Executing getTestRoles(SystemAIPOResultForATestingSession, SystemActorProfiles, System, isInteroperability)");

        TestRolesQuery trQuery = new TestRolesQuery();

        trQuery.roleInTest().testParticipantsList().tested().eq(Boolean.TRUE);
        List<TestType> testTypes = new ArrayList<>(systemAIPOResult.getTestSession().getTestTypes());

        trQuery.test().testStatus().keyword().eq(READY);

        if (isInteroperability && testingSessionService.getUserTestingSession().isTestingInteroperability()) {
            trQuery.test().testType().id().eq(TestType.getTYPE_INTEROPERABILITY().getId());
        } else {
            testTypes.remove(TestType.getTYPE_INTEROPERABILITY());
            trQuery.test().testType().in(testTypes);
        }

        Integer selectedTestPeerTypeId = datamodel.getSelectedTestPeerTypeId();
        if ((selectedTestPeerTypeId != null) && (selectedTestPeerTypeId != -1)) {
            trQuery.test().testPeerType().id().eq(selectedTestPeerTypeId);
        }

        trQuery.roleInTest().addFetch();
        trQuery.roleInTest().testParticipantsList().addFetch();
        trQuery.roleInTest().testParticipantsList().actorIntegrationProfileOption().addFetch();
        trQuery.roleInTest().testParticipantsList().aipo().addFetch();

        trQuery.roleInTest().testParticipantsList().aipo().systemActorProfiles().system().eq(system);

        trQuery.roleInTest().testParticipantsList().actorIntegrationProfileOption()
                .eq(systemActorProfile.getActorIntegrationProfileOption());

        trQuery.test().keyword().order(true);

        return trQuery.getList();
    }

    /**
     * Retrieve and make the report for a selected test
     *
     * @param tiList List of TestInstance
     * @return report of a test
     */
    public List<TestInstanceReport> generateTestInstanceItem(List<TestInstance> tiList) {
        LOG.info("Executing generateTestInstanceItem(List<TestInstance>)");

        List<TestInstanceReport> testInstanceReportList = new ArrayList<>();
        for (TestInstance testInstance : tiList) {
            TestInstanceReport testInstanceReport = new TestInstanceReport();
            if (testInstance.getMonitorInSession() != null) {
                testInstanceReport.setMonitorName(testInstance.getMonitorInSession().getUserId());
            } else {
                testInstanceReport.setMonitorName("undefined");
            }
            if (testInstance.getLastStatus() != null) {
                testInstanceReport.setResult(statusMapper.asString(testInstance.getLastStatus()));
            }
            testInstanceReport.setTestInstanceId(testInstance.getId().toString());
            testInstanceReport.setPermalink(applicationPreferenceManager.getApplicationUrl() + "testInstance.seam?id=" + testInstance.getId().toString());
            testInstanceReport.setStartDate(testInstance.getStartDate());
            testInstanceReport.setLastModifiedDate(testInstance.getLastChanged());
            testInstanceReport.setTestInstanceId(testInstance.getId().toString());
            testInstanceReportList.add(testInstanceReport);
        }
        return testInstanceReportList;
    }

    /**
     * Retrieve All information to fill the Conformity Test Report for a selected System in a Testing Session
     *
     * @param system selected System
     * @param testingSession selected TestingSession
     * @return List of SystemAIPOResult
     */
    @Override
    public List<SystemAIPOResultForATestingSession> getResultOfSystem(System system, TestingSession testingSession) {
        SystemAIPOResultForATestingSessionQuery query = new SystemAIPOResultForATestingSessionQuery();
        query.testSession().eq(testingSession);
        query.systemActorProfile().system().eq(system);
        query.addRestriction(HQLRestrictions.or(query.enabled().eqRestriction(true), query.enabled()
                .isNullRestriction()));
        return query.getList();
    }

    public void setApplicationPreferenceManager(ApplicationPreferenceManager applicationPreferenceManager) {
        this.applicationPreferenceManager = applicationPreferenceManager;
    }

    @Override
    public List<Institution> getInstitutionQueryList() {
        InstitutionQuery iq = new InstitutionQuery();
        return iq.getList();
    }

    @Override
    public List<Institution> getInstituionList() {
        return Institution.listAllInstitutions();
    }

    @Override
    public Institution findInstituion(String name) {
        return Institution.findInstitutionWithKeyword(name);
    }

    @Override
    public Institution findInstitutionById(Integer id) {
        return Institution.findInstitutionWithId(id);
    }

    @Override
    public List<SystemInSession> getSystemsInSessionForSession(TestingSession session) {
        return SystemInSession.getSystemsInSessionForSession(session);
    }

    @Override
    public List<SystemInSession> getSystemsInSessionForCompanyNameForSession(String name, TestingSession session) {
        return SystemInSession.getSystemsInSessionForCompanyNameForSession(name, session);
    }

    @Override
    public SystemInSession getSystemInSessionForSession(System inSystem, TestingSession inTestingSession) {
        return SystemInSession.getSystemInSessionForSession(inSystem, inTestingSession);
    }

    @Override
    public String getCompanyNameOfInstitutionOfSelectedSystemInSession(SystemInSession sis) {
        return sis.getSystem().getUniqueInstitution().getName();
    }

    @Override
    public String getCompanyNameOfInstitutionOfSelectedSystem(System system) {
        return system.getUniqueInstitution().getName();
    }

    @Override
    public List<TestingSession> allSessions() {
        return TestingSession.getAllSessions();
    }

    @Override
    public List<TestingSession> getTestingSessionList() {
        TestingSessionQuery sessionQuery = new TestingSessionQuery();
        return sessionQuery.getList();
    }

    @Override
    public List<TestingSession> allSessionsWhereCompanyWasHere(String name) {
        return TestingSession.getTestingSessionsWhereCompanyWasHere(name);
    }

    @Override
    public TestingSession getTestingSessionById(Integer id) {
        return TestingSession.getSessionById(id);
    }
}
