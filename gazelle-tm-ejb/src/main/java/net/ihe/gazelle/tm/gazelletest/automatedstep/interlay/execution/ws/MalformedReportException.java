package net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.execution.ws;

public class MalformedReportException extends RuntimeException {

    public MalformedReportException() {
    }

    public MalformedReportException(String message) {
        super(message);
    }

    public MalformedReportException(String message, Throwable throwable) {
        super(message, throwable);
    }

    public MalformedReportException(Throwable throwable) {
        super(throwable);
    }
}
