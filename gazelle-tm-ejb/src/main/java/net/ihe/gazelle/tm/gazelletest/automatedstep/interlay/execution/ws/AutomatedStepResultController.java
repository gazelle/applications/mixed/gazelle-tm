package net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.execution.ws;

import javax.ejb.Local;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Local
@Path("/testruns")
@Produces(MediaType.APPLICATION_JSON)
public interface AutomatedStepResultController {

    @POST
    @Path("{testInstanceId}/steps/{testStepsInstanceId}")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    Response receiveReport(@PathParam("testInstanceId") Integer testInstanceId,
                           @PathParam("testStepsInstanceId") Integer testStepInstanceId,
                           @Context HttpServletRequest request);
}
