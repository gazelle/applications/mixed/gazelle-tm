package net.ihe.gazelle.tm.messages;

import net.ihe.gazelle.common.filter.HibernateDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.tm.messages.model.Message;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;

public class MessageDataModel extends HibernateDataModel<Message> {
   private static final long serialVersionUID = -670702686562978993L;
   private final String username;

   public MessageDataModel(EntityManager entityManager, String username) {
      super(Message.class);
      setEntityManager(entityManager);
      this.username = username;
   }

   @Override
   protected void appendFilters(FacesContext context, HQLQueryBuilder<Message> queryBuilder) {
      super.appendFilters(context, queryBuilder);
      queryBuilder.addEq("users.userId", username);
   }

   @Override
   protected Object getId(Message message) {
      return message.getId();
   }
}
