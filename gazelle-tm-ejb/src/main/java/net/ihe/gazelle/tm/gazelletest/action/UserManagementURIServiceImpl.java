package net.ihe.gazelle.tm.gazelletest.action;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@Scope(ScopeType.EVENT)
@AutoCreate
@Name(value = "userManagementURIService")
public class UserManagementURIServiceImpl implements UserManagementURIService {

    private final String baseGumUsersUri;

    public UserManagementURIServiceImpl() {
        baseGumUsersUri = System.getenv("GUM_USERS_URL");
    }

    @Override
    public String getUserManagementURI() {
        assertBaseUriIsSet();
        return baseGumUsersUri;
    }

    @Override
    public String getUserManagementURIFilteredByOrganizationId(String organizationId) {
        assertBaseUriIsSet();
        return baseGumUsersUri + "?organizationId=" + organizationId;
    }

    private void assertBaseUriIsSet() {
        if (baseGumUsersUri == null)
            throw new IllegalStateException("GUM_USERS_URL is not set");
    }
}
