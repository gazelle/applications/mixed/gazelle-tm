package net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.testdesign.dao;

import net.ihe.gazelle.tm.gazelletest.automatedstep.domain.script.InputDefinition;
import net.ihe.gazelle.tm.gazelletest.domain.AutomatedTestStepInputDomain;
import net.ihe.gazelle.tm.gazelletest.domain.TestStepDomain;
import net.ihe.gazelle.tm.gazelletest.model.definition.*;
import net.ihe.gazelle.tm.tee.model.TmTestStepMessageProfile;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

@Name("automatedTestStepDAO")
@Scope(ScopeType.EVENT)
@AutoCreate
public class AutomatedTestStepDAOImpl implements AutomatedTestStepDAO {

    @In
    private EntityManager entityManager;

    @Override
    public TestStepDomain createTestStep(TestStepDomain testStep) {
        AutomatedTestStepEntity entity = new AutomatedTestStepEntity(testStep);
        List<AutomatedTestStepInput> inputEntityList = new ArrayList<>();
        for (AutomatedTestStepInputDomain inputDomain : testStep.getInputs()) {
            AutomatedTestStepInput inputEntity = new AutomatedTestStepInput(inputDomain, entity);
            inputEntityList.add(inputEntity);
        }
        entity.setInputs(inputEntityList);
        entityManager.persist(entity);
        for (AutomatedTestStepInput inputEntity : inputEntityList) {
            entityManager.persist(inputEntity);
        }
        entityManager.flush();

        AutomatedTestStepEntity managedTestStep = entityManager.find(AutomatedTestStepEntity.class, entity.getId());
        Test editedTestForTestSteps = testStep.getTestParent();
        if (editedTestForTestSteps.getTestStepsList() == null) {
            editedTestForTestSteps.setTestStepsList(new ArrayList<TestSteps>());
        }
        editedTestForTestSteps = entityManager.find(Test.class, editedTestForTestSteps.getId());
        entityManager.refresh(editedTestForTestSteps);
        List<TestSteps> lts = editedTestForTestSteps.getTestStepsList();
        if (!lts.contains(managedTestStep)) {
            lts.add(managedTestStep);
        }
        editedTestForTestSteps.setTestStepsList(lts);
        editedTestForTestSteps = entityManager.merge(editedTestForTestSteps);
        entityManager.flush();
        TestStepDomain createdTestStep = entity.asDomain();
        createdTestStep.setTestParent(editedTestForTestSteps);
        return createdTestStep;
    }

    @Override
    public TestStepDomain editTestStep(TestStepDomain testStep, List<InputDefinition> inputDefinitions) {
        AutomatedTestStepEntity entity = new AutomatedTestStepEntity(testStep);
        entity.setId(testStep.getId());
        List<AutomatedTestStepInput> inputEntityList = new ArrayList<>();
        for (InputDefinition inputDefinition : inputDefinitions) {
            AutomatedTestStepInput inputEntity = new AutomatedTestStepInput(inputDefinition.getKey(), inputDefinition.getLabel(), inputDefinition.getFormat(), inputDefinition.getType(), inputDefinition.getRequired(), entity);
            inputEntityList.add(inputEntity);
        }
        deleteStepLinkedInputs(testStep.getId());
        TestStepDomain testSteps = entityManager.merge(entity).asDomain();
        for (AutomatedTestStepInput inputEntity : inputEntityList) {
            entityManager.persist(inputEntity);
        }
        entityManager.flush();
        return testSteps;
    }

    @Override
    public void deleteTestStep(TestStepDomain testStep) {
        AutomatedTestStepEntity entity = entityManager.find(AutomatedTestStepEntity.class, testStep.getId());
        entity.setId(testStep.getId());
        for (AutomatedTestStepInput input : getInputs(entity.getId())) {
            entityManager.remove(input);
        }
        if (entity.getTmTestStepMessageProfiles() != null) {
            for (TmTestStepMessageProfile profile : entity.getTmTestStepMessageProfiles()) {
                TmTestStepMessageProfile.deleteTmTestStepMessageProfile(entityManager, profile);
            }
        }
        entityManager.flush();
        entityManager.remove(entityManager.contains(entity) ? entity : entityManager.merge(entity));
        entityManager.flush();
        for (ContextualInformation ci : entity.getInputContextualInformationList()) {
            ContextualInformation.deleteContextualInformation(entityManager, ci);
        }
        for (ContextualInformation ci : entity.getOutputContextualInformationList()) {
            ContextualInformation.deleteContextualInformation(entityManager, ci);
        }
    }

    private List<AutomatedTestStepInput> getInputs(Integer testStepId) {
        String query = "SELECT input FROM AutomatedTestStepInput input WHERE input.automatedTestStep.id = :testStepId";
        return entityManager.createQuery(query, AutomatedTestStepInput.class)
                .setParameter("testStepId", testStepId)
                .getResultList();
    }

    private void deleteStepLinkedInputs(Integer testStepId) {
        String query = "DELETE FROM AutomatedTestStepInput input WHERE input.automatedTestStep.id = :testStepId";
        entityManager.createQuery(query)
                .setParameter("testStepId", testStepId)
                .executeUpdate();
    }
}
