package net.ihe.gazelle.tm.messages;

import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import net.ihe.gazelle.ssov7.gum.client.application.User;

import net.ihe.gazelle.tm.systems.model.TestingSession;
import org.jboss.seam.Component;

import java.util.ArrayList;
import java.util.List;

public class SimpleMessageSource implements MessageSource<User> {

    public static final SimpleMessageSource INSTANCE = new SimpleMessageSource();

    private UserService userService;

    private SimpleMessageSource() {
        super();
        this.userService = (UserService) Component.getInstance("gumUserService");
    }

    @Override
    public String getLink(User instance, String[] messageParameters) {
        if ((messageParameters != null) && (messageParameters.length > 2)) {
            return messageParameters[2];
        } else {
            return "";
        }
    }

    @Override
    public String getImage(User sourceObject, String[] messageParameters) {
        return "gzl-icon-comment-o";
    }

    @Override
    public TestingSession getTestingSession(User sourceObject, String[] messageParameters) {
        return null;
    }

    @Override
    public String getType(User instance, String[] messageParameters) {
        return "gazelle.message.simple";
    }

    @Override
    public List<User> getUsers(String username, User sourceObject, String[] messageParameters) {
        String[] usersIds = messageParameters[0].split(" ");
        List<User> users = new ArrayList<>();
        for (String userId : usersIds) {
            User user = userService.getUserById(userId);
            users.add(user);
        }
        return users;
    }

}
