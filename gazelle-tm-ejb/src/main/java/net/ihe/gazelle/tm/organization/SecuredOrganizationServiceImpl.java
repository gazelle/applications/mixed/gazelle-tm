package net.ihe.gazelle.tm.organization;

import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import net.ihe.gazelle.ssov7.authn.interlay.adapter.GazelleIdentityImpl;
import net.ihe.gazelle.ssov7.gum.client.interlay.ws.OrganizationResource;
import net.ihe.gazelle.tm.organization.exception.OrganizationServiceException;
import net.ihe.gazelle.users.model.DelegatedOrganization;
import net.ihe.gazelle.users.model.Institution;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import java.util.List;

@Name("securedOrganizationService")
@Scope(ScopeType.STATELESS)
public class SecuredOrganizationServiceImpl implements OrganizationService {

    @In(value = "organizationService", create = true)
    OrganizationService organizationService;
    GazelleIdentity gazelleIdentity = GazelleIdentityImpl.instance();

    @Override
    public boolean isOrganizationRegistrationCompleted(Institution institution) {
        return organizationService.isOrganizationRegistrationCompleted(institution);
    }

    @Override
    public boolean isOrganizationDelegated(Institution institution) {
        return organizationService.isOrganizationDelegated(institution);
    }

    @Override
    public List<Institution> searchForOrganizations(String organizationName, String externalId, String idpId, Boolean delegated) {
        return organizationService.searchForOrganizations(organizationName, externalId, idpId, delegated);
    }

    @Override
    public Institution getCurrentOrganization() {
        return organizationService.getCurrentOrganization();
    }

    @Override
    public Institution getOrganizationById(String id) {
        return organizationService.getOrganizationById(id);
    }

    @Override
    public Institution findOrganizationByName(String name) {
        return organizationService.findOrganizationByName(name);
    }

    @Override
    public DelegatedOrganization getDelegatedOrganizationByKeyword(String keyword) {
        return organizationService.getDelegatedOrganizationByKeyword(keyword);
    }

    @Override
    public OrganizationResource getOrganizationresource(Institution institution) {
        return organizationService.getOrganizationresource(institution);
    }

    @Override
    public void createOrganization(OrganizationResource organization) {
        if (gazelleIdentity.isLoggedIn() && gazelleIdentity.hasPermission("any-orga", "create"))
             organizationService.createOrganization(organization);
        else
            throw new OrganizationServiceException("Current Identity is unauthorized to create an organization");
    }

    @Override
    public Institution updateOrganization(String keyword, Institution institution) {
        if (gazelleIdentity.isLoggedIn() && gazelleIdentity.hasPermission("any-orga", "update"))
            return organizationService.updateOrganization(keyword, institution);
        else
            throw new OrganizationServiceException("Current Identity is unauthorized to update an organization");
    }

    @Override
    public boolean isThereAtLeastOneOrganization() {
        return organizationService.isThereAtLeastOneOrganization();
    }

}