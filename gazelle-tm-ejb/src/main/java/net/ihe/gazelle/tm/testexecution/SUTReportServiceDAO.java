package net.ihe.gazelle.tm.testexecution;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import net.ihe.gazelle.tm.gazelletest.model.instance.SystemAIPOResultForATestingSession;

import java.util.List;
import java.util.Map;

public interface SUTReportServiceDAO {
    List<TestReport> getTestReports(SUTCapabilityReport sutCapabilityReport);
    Filter<SystemAIPOResultForATestingSession> getFilter(Map<String, String> requestParameterMap,
                                                         List<QueryModifier<SystemAIPOResultForATestingSession>> queryModifiers);

    void updateEvaluation(SystemAIPOResultForATestingSession aipoResult);
}
