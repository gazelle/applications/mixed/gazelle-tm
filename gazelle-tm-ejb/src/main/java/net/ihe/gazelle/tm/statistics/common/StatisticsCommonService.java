package net.ihe.gazelle.tm.statistics.common;

import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstance;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstanceParticipantsQuery;
import net.ihe.gazelle.tm.systems.model.SystemInSessionQuery;

import java.util.List;


public interface StatisticsCommonService {
    TestInstanceParticipantsQuery getTestInstanceParticipantsQuery();

    SystemInSessionQuery getSystemInSessionQuery();

    ProfileInTestingSessionStats getProfileInTestingSessionStatsForCurrentSession();

    int getNumberOfSessionParticipants();

    int getNumberOfRegisteredInstitutionWithAttendee();

    int getNumberOfRegisteredInstitutionWithSystem();

    int getNumberOfRegisteredSystemInSession();

    int getNumberOfIntegrationProfilesForCurrentSession();

    int getNumberOfDomainsForCurrentSession();

    TestInstanceStats getStatisticsForTestInstances(List<TestInstance> testInstances);


    class TestInstanceStats {
        private final int nbOfVerified;
        private final int nbOfFailed;
        private final int nbOfInProgress;
        private final int nbOfTobVerified;
        private final int nbOfPartiallyVerified;
        private final int nbOfCritical;

        public TestInstanceStats(int nbOfVerified, int nbOfFailed, int nbOfInProgress, int nbOfTobVerified, int nbOfPartiallyVerified, int nbOfCritical) {
            this.nbOfVerified = nbOfVerified;
            this.nbOfFailed = nbOfFailed;
            this.nbOfInProgress = nbOfInProgress;
            this.nbOfTobVerified = nbOfTobVerified;
            this.nbOfPartiallyVerified = nbOfPartiallyVerified;
            this.nbOfCritical = nbOfCritical;
        }

        public int getNbOfTobVerified() {
            return nbOfTobVerified;
        }

        public int getNbOfPartiallyVerified() {
            return nbOfPartiallyVerified;
        }

        public int getNbOfVerified() {
            return nbOfVerified;
        }

        public int getNbOfFailed() {
            return nbOfFailed;
        }

        public int getNbOfInProgress() {
            return nbOfInProgress;
        }

        public int getNbOfCritical() {
            return nbOfCritical;
        }


        public boolean isEmpty() {
            return nbOfVerified == 0
                    && nbOfFailed == 0
                    && nbOfInProgress == 0
                    && nbOfTobVerified == 0
                    && nbOfPartiallyVerified == 0
                    && nbOfCritical == 0;
        }
    }

    class ProfileInTestingSessionStats {
        private final int nbTestable;
        private final int nbDropped;
        private final int nbFewPartners;
        private final int nbDecisionPending;

        public ProfileInTestingSessionStats(int nbTestable, int nbDropped, int nbFewPartners, int nbDecisionPending) {
            this.nbTestable = nbTestable;
            this.nbDropped = nbDropped;
            this.nbFewPartners = nbFewPartners;
            this.nbDecisionPending = nbDecisionPending;
        }

        public int getNbTestable() {
            return nbTestable;
        }

        public int getNbDropped() {
            return nbDropped;
        }

        public int getNbFewPartners() {
            return nbFewPartners;
        }

        public int getNbDecisionPending() {
            return nbDecisionPending;
        }

        public boolean isEmpty() {
            return nbTestable == 0 && nbDropped == 0 && nbFewPartners == 0 && nbDecisionPending == 0;
        }
    }
}
