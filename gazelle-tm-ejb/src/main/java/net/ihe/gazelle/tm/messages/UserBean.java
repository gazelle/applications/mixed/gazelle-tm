package net.ihe.gazelle.tm.messages;

import net.ihe.gazelle.ssov7.gum.client.application.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserBean {

    private static final Logger LOG = LoggerFactory.getLogger(UserBean.class);

    private String username;
    private String firstName;
    private String lastName;

    public UserBean() {
        super();
    }

    public UserBean(User user) {
        this.username = user.getId();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
    }

    public String getUsername() {
        LOG.debug("getUsername");
        return username;
    }

    public void setUsername(String username) {
        LOG.debug("setUsername");
        this.username = username;
    }

    public String getFirstName() {
        LOG.debug("getFirstName");
        return firstName;
    }

    public void setFirstName(String firstName) {
        LOG.debug("setFirstName");
        this.firstName = firstName;
    }

    public String getLastName() {
        LOG.debug("getLastName");
        return lastName;
    }

    public void setLastName(String lastName) {
        LOG.debug("setLastName");
        this.lastName = lastName;
    }

    @Override
    public int hashCode() {
        LOG.debug("hashCode");
        final int prime = 31;
        int result = 1;
        result = (prime * result) + ((firstName == null) ? 0 : firstName.hashCode());
        result = (prime * result) + ((lastName == null) ? 0 : lastName.hashCode());
        result = (prime * result) + ((username == null) ? 0 : username.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        LOG.debug("equals");
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        UserBean other = (UserBean) obj;
        if (firstName == null) {
            if (other.firstName != null) {
                return false;
            }
        } else if (!firstName.equals(other.firstName)) {
            return false;
        }
        if (lastName == null) {
            if (other.lastName != null) {
                return false;
            }
        } else if (!lastName.equals(other.lastName)) {
            return false;
        }
        if (username == null) {
            if (other.username != null) {
                return false;
            }
        } else if (!username.equals(other.username)) {
            return false;
        }
        return true;
    }

}
