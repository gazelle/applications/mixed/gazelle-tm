package net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.script.dao;

import net.ihe.gazelle.tm.gazelletest.automatedstep.domain.script.Script;
import net.ihe.gazelle.tm.gazelletest.domain.TestStepDomain;

public interface TestStepScriptDAO {

    /**
     * Save an automation script for a test step
     * @param testStep the test step linked to the script to save
     * @param script the script to be saved
     */
    void saveScript(TestStepDomain testStep, Script script);

    /**
     * Read a script from the test step id and script name
     * @param stepId the id of the test step
     * @param scriptName the name of the script
     * @return The content of the script
     */
    String readScript(Integer stepId, String scriptName);

    /**
     * Delete an automation script for a test step
     * @param testStep the test step linked to the script to delete
     */
    void deleteScript(TestStepDomain testStep);
}
