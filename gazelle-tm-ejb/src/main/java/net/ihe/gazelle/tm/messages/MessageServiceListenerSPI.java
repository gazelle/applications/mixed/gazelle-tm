package net.ihe.gazelle.tm.messages;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.tm.application.action.ApplicationManager;
import net.ihe.gazelle.tm.messages.model.Message;
import org.jboss.seam.Component;
import org.kohsuke.MetaInfServices;

import javax.persistence.EntityManager;

@MetaInfServices(NotificationListener.class)
public class MessageServiceListenerSPI implements NotificationListener {

   @Override
   public void sendNotification(Message message) {
      new MessageService(
            (ApplicationPreferenceManager) Component.getInstance("applicationPreferenceManager"),
            (ApplicationManager) Component.getInstance("applicationManager"),
            (EntityManager) Component.getInstance("entityManager")
      ).sendNotification(message);
   }

}
