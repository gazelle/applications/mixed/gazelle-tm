package net.ihe.gazelle.tm.gazelletest.action.conformitytestreport.model;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "metaTest")
@XmlAccessorType(XmlAccessType.FIELD)
public class MetaTestReport extends AbstractTestReportModel {

    @XmlElementWrapper(name = "tests")
    @XmlElement(name = "test")
    private List<TestReportItem> testReportItems;


    public MetaTestReport() {
        this.testReportItems = new ArrayList<>();
    }


    /**
     * Getter of the list of Test Reports
     *
     * @return List of test report
     */
    public List<TestReportItem> getTestReportItems() {
        if (testReportItems == null) {
            return new ArrayList<>();
        }
        return new ArrayList<>(testReportItems);
    }

    /**
     * Setter of testReports
     *
     * @param testReportItems list of test reports
     */
    public void setTestReportItems(List<TestReportItem> testReportItems) {
        if (testReportItems != null) {
            this.testReportItems = new ArrayList<>(testReportItems);
        } else {
            this.testReportItems = new ArrayList<>();
        }
    }

    public MetaTestReport(String keyword, String name) {
        super(keyword, name);
    }
}
