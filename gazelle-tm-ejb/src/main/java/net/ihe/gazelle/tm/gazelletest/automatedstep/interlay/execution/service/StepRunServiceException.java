package net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.execution.service;

public class StepRunServiceException extends Exception {

    public StepRunServiceException() {
    }

    public StepRunServiceException(String message) {
        super(message);
    }

    public StepRunServiceException(String message, Throwable throwable) {
        super(message, throwable);
    }

    public StepRunServiceException(Throwable throwable) {
        super(throwable);
    }
}
