package net.ihe.gazelle.tm.testexecution;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import net.ihe.gazelle.tm.gazelletest.model.instance.SystemAIPOResultForATestingSession;

import java.util.List;
import java.util.Map;

public interface SUTCapabilityReportService {

    Filter<SystemAIPOResultForATestingSession> getFilter(Map<String, String> requestParameterMap,
                                                         List<QueryModifier<SystemAIPOResultForATestingSession>> queryModifiers);

    FilterDataModel<SystemAIPOResultForATestingSession> getDataModel(Filter<SystemAIPOResultForATestingSession> filter);

    void updateEvaluation(List<SystemAIPOResultForATestingSession> aipoResults);

    SUTCapabilityReport constructSutCapabilityReport(SystemAIPOResultForATestingSession systemAIPOResultForATestingSession);
}
