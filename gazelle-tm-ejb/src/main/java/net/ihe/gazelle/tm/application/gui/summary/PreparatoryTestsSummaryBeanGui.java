package net.ihe.gazelle.tm.application.gui.summary;

import net.ihe.gazelle.menu.Pages;
import net.ihe.gazelle.tm.organization.OrganizationService;
import net.ihe.gazelle.tm.session.TestingSessionService;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;

import java.io.Serializable;


@Name("preparatoryTestsSummaryBeanGui")
@Scope(ScopeType.PAGE)
public class PreparatoryTestsSummaryBeanGui implements Serializable {

    private static final long serialVersionUID = -4714862816916313584L;

    @In(value = "testingSessionService",create = true)
    private TestingSessionService testingSessionService;

    @In(value = "organizationService", create = true)
    private OrganizationService organizationService;

    private TestingSession currentTestingSession;


    /////////////////// init ///////////////////

    @Create
    public void init(){
        try {
            this.currentTestingSession = testingSessionService.getUserTestingSession();
        }
        catch (IllegalArgumentException e){
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "An error has occurred during initialization");
        }
    }

    /////////////////// Checker ///////////////////

    public boolean isPrecatTestsEnabled() {
        return testingSessionService.isPrecatTestsEnabled(currentTestingSession);
    }

    /////////////////// Links ///////////////////

    public String listPreparatoryTestsButton(){
        return Pages.TEST_EXECUTION.getMenuLink();
    }

}
