package net.ihe.gazelle.tm.tee.util;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

/**
 * Contains utility methods for handling XML
 *
 * @author tnabeel
 */
public class XMLUtil {

    private static final Logger LOG = LoggerFactory.getLogger(XMLUtil.class);

    /**
     * Take a file content as parameter and return a document
     * @param xmlContents file content as a byte array
     * @return a document
     */
    public static Document loadDocument(byte[] xmlContents) {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory
                    .newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(new ByteArrayInputStream(
                    xmlContents));
            return document;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Take a file name as parameter and return a document
     * @param fileName file name as string
     * @return a document
     */
    public static Document loadDocument(String fileName) {
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fileName);
            doc.getDocumentElement().normalize();
            return doc;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Pretty prints a file content to a string
     * @param xmlContents file content as an array byte
     * @return the content prettyfied in a string
     */
    public static String getPrettyContents(byte[] xmlContents) {
        try {
            return getPrettyContents(loadDocument(xmlContents));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


    /**
     * Pretty prints a file to a string
     * @param xmlContents file content as a string
     * @return the content prettified in a string
     */
    public static String getPrettyContents(String xmlContents) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("String getPrettyContents");
        }
        try {
            return getPrettyContents(xmlContents.getBytes(StandardCharsets.UTF_8.name()));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Pretty prints a document to a string
     * @param a document
     * @return the content prettyfied in a string
     */
    public static String getPrettyContents(Document document) throws IOException {
        if (LOG.isDebugEnabled()) {
            LOG.debug("String getPrettyContents");
        }
        return getPrettyContents(new DOMSource(document));
    }


    /**
     * Pretty prints a source to a string
     * @param a source
     * @return the source prettyfied in a strin
     */
    public static String getPrettyContents(Source source) throws IOException {
        if (LOG.isDebugEnabled()) {
            LOG.debug("String getPrettyContents");
        }
        TransformerFactory tfactory = TransformerFactory.newInstance();
        Transformer serializer;
        try {
            serializer = tfactory.newTransformer();
            //Setup indenting to "pretty print"
            serializer.setOutputProperty(OutputKeys.INDENT, "yes");
            serializer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
            serializer.setOutputProperty(OutputKeys.METHOD, "xml");
            serializer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            serializer.setOutputProperty("{http://xml.apache.org/xslt}indent-number", "1");

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            serializer.transform(source, new StreamResult(bos));
            bos.close();
            return new String(bos.toByteArray(), StandardCharsets.UTF_8.name());
        } catch (TransformerException e) {
            throw new RuntimeException("getNodeContents failed", e);
        }
    }

    /**
     * Get the node value of a node given its name
     * @param document document
     * @param nodeName node name
     * @return the value of the node in a string
     */
    public static String getNodeValueFromDocument(Document document, String nodeName) {
        if (document == null || StringUtils.isBlank(nodeName)){
            throw new IllegalArgumentException("Invalid parameter document or Node is null");
        }
        NodeList nList = document.getElementsByTagName(nodeName);
        String nodeValue = null;
        if (nList.getLength() > 0) {
            Node nNode = nList.item(0);
            Element element = (Element) nNode;
            nodeValue = element.getTextContent();
        }
        return nodeValue;
    }

    /**
     * Get all the attributes of a node given its name
     * @param document document
     * @param nodeName node name
     * @return in a map the associated attribute names and attribute values
     */
    public static Map<String, String> listAllAttributesOfNodeFromDocument(Document document, String nodeName) {
        if (document == null || StringUtils.isBlank(nodeName)){
            throw new IllegalArgumentException("Invalid parameter document or Node is null");
        }
        NodeList nList = document.getElementsByTagName(nodeName);
        Map<String, String> attributesAndValue = new HashMap<>();
        for (int i = 0; i < nList.getLength(); i++) {
            Node nNode = nList.item(i);
            Element element = (Element) nNode;

            // get a map containing the attributes of this node
            NamedNodeMap attributes = element.getAttributes();

            // get the number of nodes in this map
            int numAttrs = attributes.getLength();

            for (int j = 0; j < numAttrs; j++) {
                Attr attr = (Attr) attributes.item(j);

                String attrName = attr.getNodeName();
                String attrValue = attr.getNodeValue();

                attributesAndValue.put(attrName, attrValue);

            }
        }
        return attributesAndValue;
    }

}
