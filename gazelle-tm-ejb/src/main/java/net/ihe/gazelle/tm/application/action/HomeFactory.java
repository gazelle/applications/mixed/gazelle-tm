package net.ihe.gazelle.tm.application.action;

import net.ihe.gazelle.tm.application.dao.SectionDAOImpl;
import net.ihe.gazelle.tm.application.services.SectionDAO;
import net.ihe.gazelle.tm.application.services.SectionService;
import net.ihe.gazelle.tm.application.services.SectionServiceImpl;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.*;

@Name("homeFactory")
@Scope(ScopeType.EVENT)
@AutoCreate
public class HomeFactory {

    @In("sectionDAO")
    SectionDAO sectionDAO;

    @Factory(value = "sectionService", autoCreate = true, scope = ScopeType.STATELESS)
    public SectionService getSectionService(){
        return new SectionServiceImpl(sectionDAO);
    }
}
