package net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.script.dao;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.tm.gazelletest.automatedstep.domain.script.Script;
import net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.script.dto.ScriptDTO;
import net.ihe.gazelle.tm.gazelletest.domain.TestStepDomain;
import org.apache.commons.io.IOUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

@Name("testStepScriptDAO")
@Scope(ScopeType.STATELESS)
@AutoCreate
public class TestStepScriptDAOImpl implements TestStepScriptDAO {

    private static final Logger LOG = LoggerFactory.getLogger(TestStepScriptDAOImpl.class);

    @In(value = "applicationPreferenceManager")
    private ApplicationPreferenceManager applicationPreferenceManager;

    @Override
    public void saveScript(TestStepDomain testStep, Script script) {
        ScriptDTO scriptDTO = new ScriptDTO(script);
        String pathToSaveFile = getFullPath(testStep.getId(), testStep.getTestScriptID());
        File fileToStore = new File(pathToSaveFile);
        fileToStore.getParentFile().mkdirs();
        if (fileToStore.getParentFile().exists()) {
            try {
                ObjectMapper mapper = new ObjectMapper();
                String content = mapper.writeValueAsString(scriptDTO);
                ByteArrayInputStream fis = new ByteArrayInputStream(content.getBytes(StandardCharsets.UTF_8));
                String fileName = getScriptNameFromTestStep(testStep.getId(), testStep.getTestScriptID());
                if (fileName != null) {
                    Files.delete(Paths.get(pathToSaveFile));
                }
                try (FileOutputStream fos = new FileOutputStream(fileToStore)) {
                    IOUtils.copy(fis, fos);
                }
            } catch (IOException e) {
                LOG.error("Error saving script " + testStep.getTestScriptID(), e);
            }
        } else {
            throw new IllegalStateException(fileToStore.getParentFile().getAbsolutePath() + " does not exists.");
        }
    }

    @Override
    public String readScript(Integer stepId, String scriptName) {
        try {
            String pathToGetFile = getFullPath(stepId, scriptName);
            byte [] bytes = Files.readAllBytes(Paths.get(pathToGetFile));
            return new String(bytes, StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new IllegalStateException("Failed reading script " + scriptName, e);
        }
    }

    @Override
    public void deleteScript(TestStepDomain testStep) {
        try {
            String pathToDeleteFile = getFullPath(testStep.getId(), testStep.getTestScriptID());
            File fileToDelete = new File(pathToDeleteFile);
            if (fileToDelete.exists()) {
                Files.delete(Paths.get(pathToDeleteFile));
            }
        } catch (IOException e) {
            LOG.error("Could not delete script " + testStep.getTestScriptID(), e);
        }
    }

    private String getFullPath(Integer stepId, String scriptName) {
        return getPath() + generateFileName(stepId, scriptName);
    }

    private String getPath() {
        String dataPath = applicationPreferenceManager.getGazelleDataPath();
        String scriptPath = applicationPreferenceManager.getStringValue("test_step_script_path");
        return dataPath + java.io.File.separatorChar + scriptPath + java.io.File.separatorChar;
    }

    private String generateFileName(Integer testStepId, String testScriptId) {
        if (testStepId == null || testScriptId == null || testScriptId.isEmpty()) {
            throw new IllegalArgumentException("Needed parameters for the testStep script name generation are null");
        }
        return testStepId + "_" + testScriptId;
    }

    public String getScriptNameFromTestStep(Integer testStepId, String testScriptId) {
        String directoryPath = getPath();
        File directory = new File(directoryPath);
        if (directory.exists() && directory.isDirectory()) {
            String[] filesList = directory.list();
            if (filesList != null) {
                for (String fileName : filesList) {
                    if (fileName.startsWith(testStepId + "_")) {
                        return generateFileName(testStepId, testScriptId);
                    }
                }
            }
        }
        return null;
    }
}
