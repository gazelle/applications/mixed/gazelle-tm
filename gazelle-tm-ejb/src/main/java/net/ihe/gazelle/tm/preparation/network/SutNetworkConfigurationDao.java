package net.ihe.gazelle.tm.preparation.network;

import net.ihe.gazelle.tm.systems.model.SystemInSession;
import net.ihe.gazelle.tm.systems.model.TestingSession;

import java.util.List;

public interface SutNetworkConfigurationDao {
    /**
     * Gets number of network configurations approved for a system id.
     *
     * @param systemInSessionId the system in session id
     * @return the number of network configurations approved for a system id
     */
    int getNumberOfNetworkConfigurationsApprovedForASystemId(int systemInSessionId);

    /**
     * Gets number of network configurations not approved for a system id.
     *
     * @param systemInSessionId the system in session id
     * @return the number of network configurations not approved for a system id
     */
    int getNumberOfNetworkConfigurationsNotApprovedForASystemId(int systemInSessionId);

    /**
     * Gets number of network configurations hosts.
     *
     * @param selectedTestingSession the selected testing session
     * @return the number of network configurations hosts
     */
    int getNumberOfNetworkConfigurationsHosts(TestingSession selectedTestingSession);

    /**
     * Gets number of network configurations hosts no ip.
     *
     * @param selectedTestingSession the selected testing session
     * @return the number of network configurations hosts no ip
     */
    int getNumberOfNetworkConfigurationsHostsNoIp(TestingSession selectedTestingSession);

    /**
     * Gets list system in session of curren user.
     *
     * @param selectedTestingSession the selected testing session
     * @return the list system in session of curren user
     */
    List<SystemInSession> getListSystemInSessionOfCurrenUser(TestingSession selectedTestingSession);
}
