package net.ihe.gazelle.tm.gazelletest.utils;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.ss.usermodel.CellStyle;

public class HSSFCellUtils {

    public static HSSFCell getCell(HSSFSheet worksheet, CellStyle style, int irow, int icol) {
        HSSFRow row = worksheet.getRow(irow);
        if (row == null) {
            row = worksheet.createRow(irow);
            row.setRowStyle(style);
        }
        HSSFCell cell = row.getCell(icol);
        if (cell == null) {
            cell = row.createCell(icol);
        }
        return cell;
    }
}
