package net.ihe.gazelle.tm.gazelletest.testingSessionScope;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.tf.model.Actor;
import net.ihe.gazelle.tf.model.IntegrationProfile;
import net.ihe.gazelle.tf.model.IntegrationProfileOption;
import net.ihe.gazelle.tm.gazelletest.model.instance.SystemAIPOResultForATestingSession;
import net.ihe.gazelle.tm.systems.model.ProfileInTestingSession;
import net.ihe.gazelle.tm.systems.model.SystemInSession;
import net.ihe.gazelle.tm.systems.model.Testability;
import net.ihe.gazelle.tm.systems.model.TestingSession;

import java.util.List;
import java.util.Map;

public interface TestingSessionScopeDao {
    Filter<ProfileInTestingSession> getFilter(Map<String, String> requestParameterMap);

    List<SystemInSession> getSystemsInSessionByActorAndProfile(IntegrationProfile integrationProfile, Actor actor, TestingSession testingSession);

    List<IntegrationProfileOption> getProfileOptionsBySAIP(SystemInSession systemInSession, Actor actor, IntegrationProfile integrationProfile );

    void updateProfileInTestingSession(ProfileInTestingSession profileInTestingSession);

    List<SystemAIPOResultForATestingSession> getSystemAIPOResultByTestability(Testability testability, TestingSession testingSession);
}
