package net.ihe.gazelle.tm.systems.dao;

import net.ihe.gazelle.tf.model.ActorIntegrationProfileOption;
import net.ihe.gazelle.tm.systems.SystemUnderTestServiceDAO;
import net.ihe.gazelle.tm.systems.model.System;
import net.ihe.gazelle.tm.systems.model.SystemActorProfiles;
import net.ihe.gazelle.tm.systems.model.SystemInSession;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import net.ihe.gazelle.users.model.Institution;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;


@Name(value = "systemUnderTestServiceDAO")
@Scope(ScopeType.STATELESS)
public class SystemUnderTestServiceDAOImpl implements SystemUnderTestServiceDAO {

    @In
    private EntityManager entityManager;

    @Override
    public List<SystemInSession> getSystemsOfOrganizationInSession(TestingSession testingSession, Institution institution) {
        if(testingSession == null || institution == null){
            throw new IllegalArgumentException("Testing Session and Institution must be defined");
        }
        return SystemInSession.getSystemsInSessionForCompanyForSession(null,institution,testingSession);

    }
    @Override
    public List<SystemInSession> getSystemsOfOrganizationInSessionOfSession(TestingSession testingSession) {
        if(testingSession== null){
            throw new IllegalArgumentException("Testing Session must be defined");
        }
        return SystemInSession.getSystemsInSessionForSession(testingSession);
    }
    @Override
    public List<ActorIntegrationProfileOption> getActorsIntegrationProfiles(System system) {
        return SystemActorProfiles
                .getListOfActorIntegrationProfileOptions(system, null);
    }


    public long getNumberOfAcceptedSystemsInSession(TestingSession testingSession, Institution institution){
        String queryString = "select count(s) from SystemInSession s join s.system.institutionSystems sint where " +
                "s.testingSession.id = :sessionId and sint.institution.id = :institutionId and s.acceptedToSession = true";
        TypedQuery<Long> query = entityManager.createQuery(queryString, Long.class);
        query.setParameter("sessionId",testingSession.getId());
        query.setParameter("institutionId",institution.getId());
        return query.getSingleResult();
    }

    @Override
    public long getNumberOfRegisteredSystemsInSession(TestingSession testingSession, Institution institution) {
        String queryString = "select count(s) from SystemInSession s join s.system.institutionSystems sint where" +
                               " s.testingSession.id = :sessionId and sint.institution.id = :institutionId";
        TypedQuery<Long> query = entityManager.createQuery(queryString, Long.class);
        query.setParameter("sessionId",testingSession.getId());
        query.setParameter("institutionId",institution.getId());
        return query.getSingleResult();
    }
}
