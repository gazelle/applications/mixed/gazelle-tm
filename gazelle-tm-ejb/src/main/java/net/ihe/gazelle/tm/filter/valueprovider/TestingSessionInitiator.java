package net.ihe.gazelle.tm.filter.valueprovider;

import net.ihe.gazelle.hql.criterion.ValueProvider;
import net.ihe.gazelle.tm.session.TestingSessionService;
import org.jboss.seam.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestingSessionInitiator implements ValueProvider {

    public static final TestingSessionInitiator INSTANCE = new TestingSessionInitiator();
    private static final Logger LOG = LoggerFactory.getLogger(TestingSessionInitiator.class);
    private static final long serialVersionUID = -8047544369456796713L;
    private final transient TestingSessionService testingSessionService;

    public TestingSessionInitiator() {
        testingSessionService = (TestingSessionService) Component.getInstance("testingSessionService");
    }

    @Override
    public Object getValue() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getValue");
        }
        return testingSessionService.getUserTestingSession();
    }

}
