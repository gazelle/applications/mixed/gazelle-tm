package net.ihe.gazelle.tm.messages;

import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstanceParticipants;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;

/**
 * used on the comments of preCAT testInstances
 *
 * @author aboufahj
 */
public class TestInstanceParticipantCommentMessageSource extends MessageSourceUsersDB<TestInstanceParticipants> {

    public static final TestInstanceParticipantCommentMessageSource INSTANCE = new TestInstanceParticipantCommentMessageSource();

    private static final Logger LOG = LoggerFactory.getLogger(TestInstanceParticipantCommentMessageSource.class);

    private final Collection<String> pathsToUser = new ArrayList<>();

    private TestInstanceParticipantCommentMessageSource() {
        super();
        pathsToUser.add("systemInSessionUser.user.username");
    }

    public TestInstanceParticipantCommentMessageSource(UserService userService) {
        super(userService);
        pathsToUser.add("systemInSessionUser.user.username");
    }

    @Override
    public Collection<String> getPathsToUsername() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getPathsToUsername");
        }
        return pathsToUser;
    }

    @Override
    public Class<TestInstanceParticipants> getSourceClass() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getSourceClass");
        }
        return TestInstanceParticipants.class;
    }

    @Override
    public String getLink(TestInstanceParticipants instance, String[] messageParameters) {
        return "/mesaTestInstance.seam?id=" + instance.getId();
    }

    @Override
    public Integer getId(TestInstanceParticipants sourceObject, String[] messageParameters) {
        return sourceObject.getId();
    }

    @Override
    public String getImage(TestInstanceParticipants sourceObject, String[] messageParameters) {
        return "gzl-icon-comment-o";
    }

    @Override
    public TestingSession getTestingSession(TestInstanceParticipants sourceObject, String[] messageParameters) {
        return sourceObject.getTestInstance().getTestingSession();
    }

    @Override
    public String getType(TestInstanceParticipants instance, String[] messageParameters) {
        return "gazelle.message.testinstance.precomment";
    }
}
