package net.ihe.gazelle.tm.preparation.network;

import net.ihe.gazelle.menu.Pages;
import net.ihe.gazelle.users.model.Institution;

public class SutNetworkConfigurationSummary {

    private String sutName;
    private int numberApproved;
    private int numberNotApproved;
    private int systemId;
    private int testingSessionId;

    public String getSutName() {
        return sutName;
    }

    public void setSutName(String sutName) {
        this.sutName = sutName;
    }

    public int getNumberApproved() {
        return numberApproved;
    }

    public void setNumberApproved(int numberApproved) {
        this.numberApproved = numberApproved;
    }

    public int getNumberNotApproved() {
        return numberNotApproved;
    }

    public void setNumberNotApproved(int numberNotApproved) {
        this.numberNotApproved = numberNotApproved;
    }

    public int getSystemId() {
        return systemId;
    }

    public void setSystemId(int systemId) {
        this.systemId = systemId;
    }

    public int getTestingSessionId() {
        return testingSessionId;
    }

    public void setTestingSessionId(int testingSessionId) {
        this.testingSessionId = testingSessionId;
    }

    public String getNetworkInterfaceLink() {
        return Pages.CONFIG_ALL.getMenuLink() + "?" + "system=" + systemId + "&testSession=" + testingSessionId
                + "&institution=" + Institution.getLoggedInInstitution().getId();
    }
}
