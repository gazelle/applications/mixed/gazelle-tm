package net.ihe.gazelle.tm.gazelletest.action.conformitytestreport.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "organization")
@XmlAccessorType(XmlAccessType.FIELD)
public class InstitutionModel {

    @XmlElement(name = "id")
    private Integer idOrganization;

    @XmlElement(name = "name")
    private String name;

    @XmlElement(name = "keyword")
    private String keyword;

    public InstitutionModel() {
    }

    public InstitutionModel(Integer idOrganization, String name, String keyword) {
        this.idOrganization = idOrganization;
        this.name = name;
        this.keyword = keyword;
    }

    public Integer getIdOrganization() {
        return idOrganization;
    }

    public void setIdOrganization(Integer idSystem) {
        this.idOrganization = idSystem;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }
}
