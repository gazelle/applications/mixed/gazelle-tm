package net.ihe.gazelle.tm.gazelletest.action;

import net.ihe.gazelle.tm.gazelletest.dao.TestInstanceTokenDAO;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstanceToken;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestStepsInstance;
import org.apache.commons.lang.StringUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Name("testInstanceTokenService")
@Scope(ScopeType.STATELESS)
@AutoCreate
public class TestInstanceTokenService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TestInstanceTokenService.class);

    @In(value = "testInstanceTokenDAO")
    private TestInstanceTokenDAO testInstanceTokenDAO;

    public TestInstanceTokenService() {
        //empty for injection
    }

    public TestInstanceTokenService(TestInstanceTokenDAO testInstanceTokenDAO) {
        this.testInstanceTokenDAO = testInstanceTokenDAO;
    }

    private String generateToken(String userId, TestStepsInstance stepInstance, Timestamp expirationDate) {
        String generatedToken = UUID.randomUUID().toString();
        try {
            testInstanceTokenDAO.createToken(generatedToken, userId, stepInstance.getId(), expirationDate);
            return generatedToken;
        } catch (TestInstanceTokenCreationException | TestInstanceTokenSavingException exception) {
           throw new IllegalStateException("Unable to create test token", exception);
        }
    }

    /**
     * Generate a token using an amount of minutes validity (used for sub 1 hour expiration)
     * @param userId id of the user generating the token
     * @param stepInstance id of the test step instance linked to the token
     * @param minutes number of minutes before expiration
     * @return
     */
    public String generateTokenWithMinutesValidity(String userId, TestStepsInstance stepInstance, int minutes) {
        Timestamp expirationDate = new Timestamp(System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(minutes));
        return generateToken(userId, stepInstance, expirationDate);
    }

    /**
     * Generate a token using an amount of minutes validity (used for more than 1 hour expiration)
     * @param userId id of the user generating the token
     * @param stepInstance id of the test step instance linked to the token
     * @param hours number of hours before expiration
     * @return
     */
    public String generateTokenWithHoursValidity(String userId, TestStepsInstance stepInstance, int hours) {
        Timestamp expirationDate = new Timestamp(System.currentTimeMillis() + TimeUnit.HOURS.toMillis(hours));
        return generateToken(userId, stepInstance, expirationDate);
    }

    /**
     * Retrieve The TestInstanceToken By Token Value
     *
     * @param tokenValue String uuid token
     * @return Valid TestInstance matching to the unique TokenValue
     * @throws InvalidTokenException  If the TokenValidity is expired
     * @throws TokenNotFoundException If the token Value doesn't match any TestInstanceToken
     */
    public TestInstanceToken checkAndGetTestInstanceTokenByTokenValue(String tokenValue) throws InvalidTokenException, TokenNotFoundException {
        if (StringUtils.isBlank(tokenValue)) {
            LOGGER.error("Invalid token either empty or null");
            throw new InvalidTokenException("Invalid token either empty or null");
        }
        TestInstanceToken testInstanceToken = testInstanceTokenDAO.getTestInstanceToken(tokenValue);
        testInstanceTokenDAO.deleteToken(testInstanceToken.getValue());
        if (!isTokenValid(testInstanceToken)) {
            LOGGER.error("Invalid expired token");
            throw new InvalidTokenException("Invalid Token : Expired");
        }
        LOGGER.info("Token value is OK");
        return testInstanceToken;
    }

    /**
     * Check the validity of the token
     *
     * @param testInstanceToken The retrieved token
     * @return boolean according to the token validity
     */
    public boolean isTokenValid(TestInstanceToken testInstanceToken) throws TokenNotFoundException {
        Timestamp currentTime = new Timestamp(System.currentTimeMillis());
        Timestamp expirationTime = new Timestamp(testInstanceToken.getExpirationDateTime().getTime());
        return currentTime.before(expirationTime);
    }

    /**
     * Delete all expired tokens in the database
     *
     * @throws TokenNotFoundException
     */
    public void deleteExpiredTokens() throws TokenNotFoundException {
        Timestamp currentTime = new Timestamp(System.currentTimeMillis());
        List<TestInstanceToken> tokens = testInstanceTokenDAO.getAllTokens();
        for (TestInstanceToken token : tokens) {
            if (currentTime.after(token.getExpirationDateTime())) {
                testInstanceTokenDAO.deleteToken(token.getValue());
            }
        }
    }

    public TestInstanceToken getTokenByValue(String tokenValue) {
        return testInstanceTokenDAO.getTestInstanceToken(tokenValue);
    }

    public void deleteTokenByTestStepInstance(Integer testStepInstanceId) {
        testInstanceTokenDAO.deleteTokenByTestStepInstance(testStepInstanceId);
    }

    protected void setTestInstanceTokenDAO(TestInstanceTokenDAO testInstanceTokenDAO) {
        this.testInstanceTokenDAO = testInstanceTokenDAO;
    }
}
