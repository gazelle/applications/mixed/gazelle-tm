package net.ihe.gazelle.tm.messages;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.common.util.GazelleCookie;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import net.ihe.gazelle.tm.application.action.ApplicationManager;
import net.ihe.gazelle.tm.messages.model.Message;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.util.Date;

/**
 * Manage notification send to the GUI
 */
@Name("messageManager")
@Scope(ScopeType.EVENT)
public class MessageManager {
   private static final long serialVersionUID = -495519156451804999L;
   private static final String LAST_READ_MESSAGE = "LAST_READ_MESSAGE";

   private static final Logger LOG = LoggerFactory.getLogger(MessageManager.class);
   @In
   private ApplicationPreferenceManager applicationPreferenceManager;
   @In
   private ApplicationManager applicationManager;
   @In
   private GazelleIdentity identity;
   @In
   private EntityManager entityManager;
   @In
   private MessageFormatter messageFormatter;

   private MessageService messageService;

   @Create
   public void init() {
      LOG.trace("init");
      messageService = new MessageService(applicationPreferenceManager, applicationManager, entityManager);
   }

   public boolean areMessagesEnabled() {
      LOG.trace("areMessagesEnabled");
      return messageService.areMessagesEnabled();
   }

   public boolean isMessageFeatureGranted() {
      LOG.trace("isMessageFeatureGranted");
      return messageService.isMessageFeatureGranted(identity);
   }

   public int getUnreadCount() {
      LOG.trace("getUnreadCount");
      Date lastReadDate = getCookieDate(LAST_READ_MESSAGE);
      return messageService.getUnreadCount(identity, lastReadDate);
   }

   public boolean hasUnread() {
      LOG.trace("hasUnread");
      return getUnreadCount() > 0;
   }

   public MessageDataModel getMessages() {
      LOG.trace("getMessages");
      setCookieDate(new Date(), LAST_READ_MESSAGE);
      return messageService.getMessages(identity);
   }

   /**
    * Format the type to be readable by humans
    *
    * @param message
    *
    * @return
    */
   public String getFormattedType(Message message) {
      LOG.trace("getFormattedType");
      return messageFormatter.getFormattedType(message);
   }

   /**
    * Format the message in a phrase readable by humans
    *
    * @param message
    *
    * @return
    */
   public String getFormattedMessage(Message message) {
      LOG.trace("getFormattedMessage");
      return messageFormatter.getFormattedMessage(message);
   }

   public String getFormattedLink(Message message) {
      String link = message.getLink();
      if (link != null && link.toLowerCase().startsWith("http")) {
         return link;
      } else {
         return PreferenceService.getString("application_url") + link;
      }
   }

   private Date getCookieDate(String cookieName) {
      String stringDate = GazelleCookie.getCookie(cookieName);
      if (stringDate != null && !stringDate.trim().isEmpty()) {
         return new Date(Long.parseLong(stringDate));
      } else {
         Date newDate = new Date();
         setCookieDate(newDate, cookieName);
         return newDate;
      }
   }

   private void setCookieDate(Date date, String cookieName) {
      String ms = Long.toString(date.getTime());
      GazelleCookie.setCookie(cookieName, ms);
   }

}
