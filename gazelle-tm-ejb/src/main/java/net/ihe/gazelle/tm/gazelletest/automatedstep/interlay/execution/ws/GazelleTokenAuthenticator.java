package net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.execution.ws;

import net.ihe.gazelle.ssov7.authn.interlay.Authenticator;
import net.ihe.gazelle.ssov7.authn.interlay.AuthenticatorResource;
import net.ihe.gazelle.ssov7.gum.client.application.Role;
import net.ihe.gazelle.tm.gazelletest.action.TestInstanceTokenService;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstanceToken;
import org.jboss.seam.web.ServletContexts;
import org.jboss.seam.web.Session;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class GazelleTokenAuthenticator implements Authenticator {

    private final TestInstanceTokenService service;

    public GazelleTokenAuthenticator(TestInstanceTokenService service) {
        this.service = service;
    }

    @Override
    public AuthenticatorResource authenticate() {
        String tokenValue = getTestInstanceToken();
        AuthenticatorResource authenticatorResource = new AuthenticatorResource();
        if (tokenValue != null) {
            TestInstanceToken token = service.checkAndGetTestInstanceTokenByTokenValue(tokenValue);
            authenticatorResource = testInstanceTokenToIdentityToken(token);
            authenticatorResource.setLoggedIn(true);
            return authenticatorResource;
        }
        authenticatorResource.setLoggedIn(false);
        return authenticatorResource;
    }

    @Override
    public boolean isReadyToLogin() {
        return getTestInstanceToken() != null;
    }

    @Override
    public String getLogoutType() {
        return "noLogout";
    }

    @Override
    public void logout() {
        Session.instance().invalidate();
    }

    private AuthenticatorResource testInstanceTokenToIdentityToken(TestInstanceToken token) {
        AuthenticatorResource authenticatorResource = new AuthenticatorResource();
        authenticatorResource.setPrincipal(new GazelleTokenPrincipal(token));
        authenticatorResource.setUsername(token.getUserId());
        authenticatorResource.setFirstName("GazelleToken");
        authenticatorResource.setLastName("Gazelle");
        authenticatorResource.setEmail("not provided");
        authenticatorResource.setOrganizationKeyword("not needed");
        authenticatorResource.setRoles(Role.USER);
        return authenticatorResource;
    }

    private String getTestInstanceToken() {
        HttpServletRequest httpServletRequest = getHttpServletRequest();
        if (httpServletRequest != null) {
            HttpSession session = httpServletRequest.getSession();
            if (session != null) {
                return (String) httpServletRequest.getAttribute(GazelleTokenFilter.CONST_GAZELLE_TOKEN);
            }
        }
        return null;
    }

    private HttpServletRequest getHttpServletRequest() {
        ServletContexts contexts = ServletContexts.getInstance();
        HttpServletRequest request = null;
        if (contexts != null)
            request = contexts.getRequest();
        return request;
    }
}
