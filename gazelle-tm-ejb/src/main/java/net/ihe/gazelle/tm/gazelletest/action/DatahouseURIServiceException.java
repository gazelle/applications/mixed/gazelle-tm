package net.ihe.gazelle.tm.gazelletest.action;

public class DatahouseURIServiceException extends RuntimeException {
    public DatahouseURIServiceException() {
        super();
    }

    public DatahouseURIServiceException(String message) {
        super(message);
    }

    public DatahouseURIServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public DatahouseURIServiceException(Throwable cause) {
        super(cause);
    }
}
