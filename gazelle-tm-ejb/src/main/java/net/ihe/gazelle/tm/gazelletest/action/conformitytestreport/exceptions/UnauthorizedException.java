package net.ihe.gazelle.tm.gazelletest.action.conformitytestreport.exceptions;

public class UnauthorizedException extends Exception {
    public UnauthorizedException(String s) {
        super(s);
    }
}
