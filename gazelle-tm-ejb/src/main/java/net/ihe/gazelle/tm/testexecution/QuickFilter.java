package net.ihe.gazelle.tm.testexecution;

import org.apache.commons.codec.binary.Base64;

import javax.servlet.http.Cookie;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class QuickFilter implements Serializable {

    private static final long serialVersionUID = 1328006984471570852L;
    private final Cookie cookie;
    private final String displayName;
    private boolean defaultFilter;
    private final String url;

    private static final String UTF_8 = StandardCharsets.UTF_8.name();

    public QuickFilter(Cookie cookie, String cookieBaseName, String defaultCookieName) {
        this.cookie = cookie;
        this.displayName = getName(cookieBaseName);
        this.defaultFilter = isDefault(defaultCookieName);
        this.url = decodeUrl(cookie);
    }

    public Cookie getCookie() {
        return cookie;
    }

    public String getDisplayName() {
        return displayName;
    }

    public boolean isDefaultFilter() {
        return defaultFilter;
    }

    public void setDefaultFilter(boolean defaultFilter) {
        this.defaultFilter = defaultFilter;
    }

    public String getUrl() {
        return url;
    }

    public Map<String, String> getParameters() {
        Map<String, String> parameters = new HashMap<>();
        String[] params = url.split("\\?")[1].split("&");
        for (String param : params) {
            String name = param.split("=")[0];
            String value = param.split("=")[1];
            parameters.put(name, value);
        }
        return parameters;
    }

    private String getName(String cookieBaseName) {
        if (cookie.getName().contains(cookieBaseName)) {
            return cookie.getName().substring(cookieBaseName.length() + 3);
        } else {
            return "None";
        }
    }

    private String decodeUrl(Cookie cookie) {
        try {
            return new String(Base64.decodeBase64(cookie.getValue()), UTF_8);
        } catch (UnsupportedEncodingException e) {
            throw new IllegalStateException(e);
        }
    }

    private boolean isDefault(String defaultCookieName) {
        if (defaultCookieName == null) {
            return false;
        }
        return defaultCookieName.equals(getDisplayName());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        QuickFilter that = (QuickFilter) o;
        return defaultFilter == that.defaultFilter
                && Objects.equals(cookie, that.cookie)
                && Objects.equals(displayName, that.displayName)
                && Objects.equals(url, that.url);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cookie, displayName, defaultFilter, url);
    }
}
