package net.ihe.gazelle.tm.application.services;

import net.ihe.gazelle.tm.application.model.Section;

import java.util.List;

public interface SectionService {

    Section getSection(Integer sectionId);

    Section getSectionByName(String sectionName);

    List<Section> getSections();

    List<Section> getRenderedSections();

    void removeSection(Section section);

    void updateSection(Section section);
}
