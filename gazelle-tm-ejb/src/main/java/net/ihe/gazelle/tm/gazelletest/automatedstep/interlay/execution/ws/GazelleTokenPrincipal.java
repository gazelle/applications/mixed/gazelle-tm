package net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.execution.ws;

import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstanceToken;

import java.io.Serializable;
import java.security.Principal;
import java.util.Objects;

public class GazelleTokenPrincipal implements Principal, Serializable {

    private static final long serialVersionUID = 2779635497784009582L;

    private final TestInstanceToken token;

    public GazelleTokenPrincipal(TestInstanceToken token) {
        this.token = token;
    }

    @Override
    public String getName() {
        return token.getValue();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GazelleTokenPrincipal)) return false;
        GazelleTokenPrincipal that = (GazelleTokenPrincipal) o;
        return Objects.equals(token, that.token);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(token);
    }
}
