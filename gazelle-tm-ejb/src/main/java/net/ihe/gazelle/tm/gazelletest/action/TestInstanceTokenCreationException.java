package net.ihe.gazelle.tm.gazelletest.action;

import java.io.Serializable;

public class TestInstanceTokenCreationException extends Exception implements Serializable {
    public TestInstanceTokenCreationException() {
    }

    public TestInstanceTokenCreationException(String s) {
        super(s);
    }

    public TestInstanceTokenCreationException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public TestInstanceTokenCreationException(Throwable throwable) {
        super(throwable);
    }

}
