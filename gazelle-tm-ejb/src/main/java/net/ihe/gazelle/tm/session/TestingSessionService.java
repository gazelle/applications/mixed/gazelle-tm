package net.ihe.gazelle.tm.session;

import net.ihe.gazelle.tm.gazelletest.model.definition.TestType;
import net.ihe.gazelle.tm.systems.model.TestingSession;

import javax.faces.model.SelectItem;
import java.util.List;


public interface TestingSessionService {

    /**
     * Get the current testing session of the logged user
     * If the service cannot find a session, it will use the default one.
     *
     * @return the current TestingSession of the logged user.
     */
    TestingSession getUserTestingSession();

    /**
     * Set the current TestingSession of the logged user.
     *
     * @param testingSession the current session
     */
    void setCurrentTestingSession(TestingSession testingSession);

    /**
     * @param testingSession testing session object
     * @return boolean
     * @throws IllegalArgumentException if testing session is null
     */

    boolean isTestingSessionRunning(TestingSession testingSession);

    /**
     * @param testingSession testing session object
     * @return boolean
     * @throws IllegalArgumentException if testing session is null
     */

    boolean isRegistrationOpen(TestingSession testingSession);


    /**
     * @param testingSession testing session object
     * @return boolean
     * @throws IllegalArgumentException if testing session is null
     */
    boolean isBeforeSessionStart(TestingSession testingSession);

    /**
     * @param testingSession testing session object
     * @return boolean
     * @throws IllegalArgumentException if testing session is null
     */
    boolean isAfterSessionEnd(TestingSession testingSession);


    /**
     * @param testingSession testing session object
     * @return boolean
     * @throws IllegalArgumentException if testing session is null
     */
    List<String> getTestingSessionManagers(TestingSession testingSession);

    /**
     * @param testingSession testing session object
     * @return boolean
     * @throws IllegalArgumentException if testing session is null
     */

    boolean isAttendeesRegistrationEnabled(TestingSession testingSession);


    /**
     * @param testingSession testing session object
     * @return boolean
     * @throws IllegalArgumentException if testing session is null
     */
    boolean isTestingSessionOpen(TestingSession testingSession);

    boolean isContinuousSession(TestingSession testingSession);

    boolean isPrecatTestsEnabled(TestingSession testingSession);

    List<TestType> getListTestType();

    /**
     * @return Return the list of TestType as SelectItems
     */
    List<SelectItem> getListTestTypeAsSelectItems();

}
