package net.ihe.gazelle.tm.gazelletest.testingSessionScope.gui;

import net.ihe.gazelle.tm.systems.model.Testability;
import org.jboss.seam.annotations.Name;

import java.util.HashMap;
import java.util.Map;

@Name("testabilityTreatmentBean")
public class TestabilityTreatmentBean {

    private static final Map<Testability, String> testabilityStyle = new HashMap<>();
    static {
        testabilityStyle.put(Testability.TESTABLE, "gzl-label gzl-label-green");
        testabilityStyle.put(Testability.FEW_PARTNERS, "gzl-label gzl-label-orange");
        testabilityStyle.put(Testability.DROPPED, "gzl-label gzl-label-red");
        testabilityStyle.put(Testability.REMOVED_FROM_SESSION, "gzl-label gzl-label-red");
        testabilityStyle.put(Testability.DECISION_PENDING,"gzl-label gzl-label-blue");
    }

    public String getTestabilityStyle(Testability testability){
        if(testability != null) {
            String style = testabilityStyle.get(testability);
            if (style != null) {
                return style;
            }
        }
        return "gzl-label gzl-label-blue";
    }

    public String getTestabilityLabel(Testability testability) {
        if(testability != null) {
            return testability.getLabelToDisplay();
        } else {
            return Testability.DECISION_PENDING.getLabelToDisplay();
        }
    }

}
