package net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.bean;

import net.ihe.gazelle.common.report.ReportExporterManager;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import net.ihe.gazelle.tm.gazelletest.automatedstep.domain.instance.AutomatedTestStepInstanceData;
import net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.execution.service.AutomatedTestStepRunService;
import net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.execution.service.StepRunServiceException;
import net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.instance.service.AutomatedTestStepInstanceInputService;
import net.ihe.gazelle.tm.gazelletest.domain.AutomatedTestStepExecution;
import net.ihe.gazelle.tm.gazelletest.domain.AutomatedTestStepExecutionStatus;
import net.ihe.gazelle.tm.gazelletest.domain.AutomatedTestStepReportFile;
import net.ihe.gazelle.tm.gazelletest.model.instance.AutomatedTestStepInstanceInput;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstance;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestStepsInstance;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Transactional;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Name("automatedTestInstanceInputManager")
@Scope(ScopeType.PAGE)
public class AutomatedTestInstanceInputManager {

    @In(value = "automatedTestStepInstanceInputService", create = true)
    private AutomatedTestStepInstanceInputService inputDataService;
    @In(value = "automatedTestStepRunService", create = true)
    private AutomatedTestStepRunService runService;
    @In
    private GazelleIdentity identity;
    @In(value = "gumUserService")
    private UserService userService;


    private TestInstance selectedTestInstance;
    private final Map<TestStepsInstance, Boolean> inputsFulfillment = new HashMap<>();
    // Map<testInstanceId, AutomatedTestStepExecution>
    private final Map<Integer, AutomatedTestStepExecution> statusMap = new HashMap<>();
    private final Map<AutomatedTestStepInstanceData, Boolean> outDatedData = new HashMap<>();

    private void init() {
        if (selectedTestInstance != null) {
            for (TestStepsInstance stepInstance : selectedTestInstance.getTestStepsInstanceList()) {
                if (stepInstance.getTestSteps().asDomain().isAutomated()) {
                    boolean isFullFilled = isTestStepInstanceReady(stepInstance);
                    inputsFulfillment.put(stepInstance, isFullFilled);
                }
                statusMap.put(stepInstance.getId(), runService.getExecution(stepInstance.getId()));
                outDatedData.putAll(runService.getUpToDateStatusForAllInstanceInput(stepInstance));
            }
        }
    }

    @Transactional
    public List<AutomatedTestStepInstanceData> getInputDataFromTestStepInstance(TestStepsInstance stepInstance) {
        if (stepInstance != null) {
            this.selectedTestInstance = stepInstance.getTestInstance();
            init();
        }
        return inputDataService.getInputDataFromTestStepInstance(stepInstance);
    }

    public boolean isTestStepInstanceReady(TestStepsInstance stepInstance) {
        List<AutomatedTestStepInstanceInput> instanceInputList = inputDataService.getInstanceInputForTestStepInstance(stepInstance);
        for (AutomatedTestStepInstanceInput instanceInput : instanceInputList) {
            if (instanceInput.getRequired() && instanceInput.getTestStepDataId() == null) {
                return false;
            }
        }
        return true;
    }

    public boolean isInputFullFilled(TestStepsInstance stepInstance) {
        if (stepInstance != null) {
            this.selectedTestInstance = stepInstance.getTestInstance();
            init();
            if (inputsFulfillment.get(stepInstance) != null) {
                return inputsFulfillment.get(stepInstance);
            }
        }
        return false;
    }

    public int getNumberOfInputForTestStep(TestStepsInstance stepInstance) {
        if (stepInstance != null
                && stepInstance.getTestSteps() != null
                && stepInstance.getTestSteps().asDomain().getInputs() != null) {
            return stepInstance.getTestSteps().asDomain().getInputs().size();
        }
        return 1;
    }

    public boolean displayCurrentFineUpload(AutomatedTestStepInstanceData inputData) {
        return inputData.getFileName() == null;
    }

    public String getInputFileName(AutomatedTestStepInstanceData inputData) {
        return inputData.getFileName();
    }

    public void downloadInputFile(AutomatedTestStepInstanceData inputData) {
        String currentFileContent = inputDataService.readInputData(inputData);
        ReportExporterManager.exportToFile(currentFileContent, inputData.getFileName());
    }

    @Transactional
    public void deleteInputFile(AutomatedTestStepInstanceData inputData) {
        inputDataService.deleteInputData(inputData);
    }

    public String getUserAndDate(AutomatedTestStepInstanceData inputData) {
        return userService.getUserDisplayNameWithoutException(inputData.getUserId()) + "  -  " + inputData.getLastUpdate();
    }

    @Transactional
    public void runStep(TestStepsInstance stepInstance) {
        try {
            runService.run(identity, stepInstance);
        } catch (StepRunServiceException e) {
            FacesMessages.instance().add(StatusMessage.Severity.WARN, e.getMessage());
        }
    }

    @Transactional
    public void cancelRun(TestStepsInstance stepInstance) {
        runService.cancelExecution(stepInstance.getId());
    }

    public boolean warnModifiedInputs(TestStepsInstance stepInstance) {
        return statusMatchAnyOf(stepInstance, AutomatedTestStepExecutionStatus.DONE_PASSED, AutomatedTestStepExecutionStatus.DONE_FAILED)
                && runService.isRunOutDated(stepInstance.getId());
    }

    public boolean isOutDated(AutomatedTestStepInstanceData inputData, TestStepsInstance stepInstance) {
        if (!outDatedData.containsKey(inputData)) {
            return false;
        }
        return inputsFulfillment.get(stepInstance) && outDatedData.get(inputData);
    }

    public boolean isRunnable(TestStepsInstance stepInstance) {
        AutomatedTestStepExecution execution = statusMap.get(stepInstance.getId());
        return (execution == null || isCompleted(stepInstance)) && isInputFullFilled(stepInstance);
    }

    public boolean isRunning(TestStepsInstance stepInstance) {
        return statusMatchAnyOf(stepInstance, AutomatedTestStepExecutionStatus.RUNNING);
    }

    public boolean isNotReadyToRun(TestStepsInstance stepInstance) {
        return !isRunnable(stepInstance) && !isRunning(stepInstance);
    }

    public boolean isCompleted(TestStepsInstance stepInstance) {
        return statusMatchAnyOf(stepInstance,
                AutomatedTestStepExecutionStatus.DONE_PASSED,
                AutomatedTestStepExecutionStatus.DONE_FAILED,
                AutomatedTestStepExecutionStatus.TIMED_OUT,
                AutomatedTestStepExecutionStatus.DONE_UNDEFINED
        );
    }

    public boolean isPassed(TestStepsInstance stepInstance) {
        return statusMatchAnyOf(stepInstance, AutomatedTestStepExecutionStatus.DONE_PASSED);
    }

    public boolean isFailed(TestStepsInstance stepInstance) {
        return statusMatchAnyOf(stepInstance, AutomatedTestStepExecutionStatus.DONE_FAILED);
    }

    public boolean isDoneWithError(TestStepsInstance stepInstance) {
        return statusMatchAnyOf(stepInstance,
                AutomatedTestStepExecutionStatus.TIMED_OUT
        );
    }

    private boolean statusMatchAnyOf(TestStepsInstance stepInstance, AutomatedTestStepExecutionStatus... status) {
        AutomatedTestStepExecution execution = statusMap.get(stepInstance.getId());
        if (execution != null) {
            for (AutomatedTestStepExecutionStatus statusToMatch : status) {
                if (statusToMatch.equals(execution.getStatus())) {
                    return true;
                }
            }
        }
        return false;
    }

    public String getExecutionDate(TestStepsInstance stepInstance) {
        AutomatedTestStepExecution execution = statusMap.get(stepInstance.getId());
        SimpleDateFormat desiredFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        return desiredFormat.format(execution.getDate());
    }

    public void getExecutionReport(TestStepsInstance stepInstance) {
        AutomatedTestStepExecution execution = statusMap.get(stepInstance.getId());
        ReportExporterManager.exportToFile(execution.getJsonReport(), stepInstance.getId() + "_report.json");
    }

    public void viewPDFReport(TestStepsInstance stepInstance) {
        AutomatedTestStepExecution execution = statusMap.get(stepInstance.getId());
        if (execution != null
                && execution.getPdfFiles() != null
                && !execution.getPdfFiles().isEmpty()) {
            AutomatedTestStepReportFile reportFile = execution.getPdfFiles().get(0);
            ReportExporterManager.exportToFile("pdf", reportFile.getReport(), reportFile.getReportName());
        }
    }

    public String getExecutionStatus(TestStepsInstance stepInstance) {
        AutomatedTestStepExecution execution = statusMap.get(stepInstance.getId());
        return execution.getStatus().getLabel();
    }
}
