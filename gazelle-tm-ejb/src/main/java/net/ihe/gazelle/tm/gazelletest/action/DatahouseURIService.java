package net.ihe.gazelle.tm.gazelletest.action;

import net.ihe.gazelle.tm.gazelletest.model.instance.TestStepsInstance;

public interface DatahouseURIService {

    /**
     * Get the pre-filtered datahouse-ui for the TestStepsInstance
     * @param testStepsInstance the TestStepsInstance that will be used to fill query parameters
     * @return a String of the datahouse-ui URI with query parameters filled
     * @see TestStepsInstance
     * @throws IllegalArgumentException if an error during the creation of URI
     */
    String createDatahouseUri(TestStepsInstance testStepsInstance);

    /**
     * Gets datahouse base url.
     * @return the datahouse base url
     * @throws IllegalStateException if datahouse base url is not found or empty
     */
    String getDatahouseBaseUrl();

}
