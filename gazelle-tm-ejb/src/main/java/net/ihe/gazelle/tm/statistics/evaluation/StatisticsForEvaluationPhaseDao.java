package net.ihe.gazelle.tm.statistics.evaluation;

import net.ihe.gazelle.tf.model.Domain;
import net.ihe.gazelle.tm.gazelletest.model.instance.SystemAIPOResultForATestingSession;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstance;
import net.ihe.gazelle.tm.systems.model.SystemInSession;
import net.ihe.gazelle.tm.systems.model.TestingSession;

import java.util.List;

public interface StatisticsForEvaluationPhaseDao {

    List<SystemInSession> getSystemsForCurrentUser(TestingSession selectedTestingSession);

    List<SystemAIPOResultForATestingSession> getCapabilitiesForASystem(SystemInSession system);

    List<TestInstance> getAllTestInstancesForCurrentSession();

    List<SystemAIPOResultForATestingSession> getAllCapabilitiesForADomain(Domain domain, TestingSession selectedTestingSession);

    int getNbTotalTestInstancesForCurrentSession();

    int getNbTotalCapabilitiesForADomain(Domain domain, TestingSession selectedTestingSession);
}
