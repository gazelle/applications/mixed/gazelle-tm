package net.ihe.gazelle.tm.systems;

import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import net.ihe.gazelle.ssov7.gum.client.application.User;
import net.ihe.gazelle.tf.model.ActorIntegrationProfileOption;
import net.ihe.gazelle.tf.model.constraints.AipoRule;
import net.ihe.gazelle.tf.model.constraints.AipoRuleQuery;
import net.ihe.gazelle.tm.systems.model.*;
import net.ihe.gazelle.tm.systems.model.System;

import net.ihe.gazelle.users.model.Institution;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@Name(value = "systemUnderTestService")
@Scope(ScopeType.STATELESS)
public class SystemUnderTestServiceImpl implements SystemUnderTestService {

    @In(value = "systemUnderTestServiceDAO", create = true)
    private SystemUnderTestServiceDAO systemUnderTestServiceDAO;

    @In(value = "gumUserService")
    private UserService userService;


    @Override
    public List<SystemSummary> getRegisteredSystems(TestingSession testingSession, Institution institution) {

        List<SystemInSession> systemsInSession = systemUnderTestServiceDAO.getSystemsOfOrganizationInSession(testingSession, institution);
        if (systemsInSession == null) {
            throw new NoSuchElementException("No systems found for requested Testing Session and Institution");
        }
        List<SystemSummary> systemsSummary = new ArrayList<>();
        for (SystemInSession systemInSession : systemsInSession) {
            systemsSummary.add(constructSystemSummary(systemInSession));
        }
        return systemsSummary;
    }

    @Override
    public List<SystemSummary> getRegisteredSystemsOfSession(TestingSession testingSession) {

        List<SystemInSession> systemsInSession = systemUnderTestServiceDAO.getSystemsOfOrganizationInSessionOfSession(testingSession);
        if (systemsInSession == null) {
            throw new NoSuchElementException("No systems found for requested Testing Session and Institution");
        }
        List<SystemSummary> systemsSummary = new ArrayList<>();
        for (SystemInSession systemInSession : systemsInSession) {
            systemsSummary.add(constructSystemSummary(systemInSession));
        }
        return systemsSummary;
    }

    @Override
    public boolean hasSystemMissingDependency(System system) {
        AipoRuleQuery aipoRuleQuery = new AipoRuleQuery();
        List<AipoRule> rules = aipoRuleQuery.getList();
        return this.isMissingAIPO(system, rules);
    }

    @Override
    public boolean isThereAcceptedSystem(TestingSession testingSession, Institution institution) {
        long numberOfAcceptedSystems = systemUnderTestServiceDAO.getNumberOfAcceptedSystemsInSession(testingSession, institution);
        return numberOfAcceptedSystems > 0;
    }

    @Override
    public boolean isThereRegisteredSystems(TestingSession testingSession, Institution institution) {
        long numberOfRegisteredSystems = systemUnderTestServiceDAO.getNumberOfRegisteredSystemsInSession(testingSession, institution);
        return numberOfRegisteredSystems > 0;
    }

    @Override
    public long getNumberOfAcceptedSystems(TestingSession testingSession, Institution institution) {
        return systemUnderTestServiceDAO.getNumberOfAcceptedSystemsInSession(testingSession, institution);
    }

    private boolean isMissingAIPO(System system, List<AipoRule> rules) {
        List<AipoRule> invalidRules = new ArrayList<AipoRule>();
        List<ActorIntegrationProfileOption> AIPO = systemUnderTestServiceDAO.getActorsIntegrationProfiles(system);
        if (AIPO == null || AIPO.isEmpty()) {
            return true;
        }
        for (AipoRule rule : rules) {
            if (!rule.validate(AIPO)) {
                invalidRules.add(rule);
            }
        }
        return !invalidRules.isEmpty();
    }

    private SystemSummary constructSystemSummary(SystemInSession systemInSession) {
        SystemSummary systemSummary = new SystemSummary();
        systemSummary.setSUTKeyword(systemInSession.getSystem().getKeyword());
        systemSummary.setSUTName(systemInSession.getSystem().getName());
        systemSummary.setSUTType(systemInSession.getSystem().getSystemType().getSystemTypeKeyword());
        systemSummary.setVersion(systemInSession.getSystem().getVersion());

        systemSummary.setRegistrationStatus(systemInSession.getRegistrationStatus());
        systemSummary.setMissingDependencies(this.hasSystemMissingDependency(systemInSession.getSystem()));
        systemSummary.setAccepted(systemInSession.getAcceptedToSession());
        systemSummary.setSystem(systemInSession.getSystem());
        systemSummary.setTool(systemInSession.getSystem().getIsTool());
        systemSummary.setSystemInSession(systemInSession);

        String userId = systemInSession.getSystem().getOwnerUserId();
        try {
            User user = userService.getUserById(userId);
            systemSummary.setOwner(user.getFirstName() + " " + user.getLastName());
            systemSummary.setOwnerMail(user.getEmail());
        } catch (NoSuchElementException e) {
            systemSummary.setOwner(userId);
            systemSummary.setOwnerMail("");
        }
        return systemSummary;
    }
}
