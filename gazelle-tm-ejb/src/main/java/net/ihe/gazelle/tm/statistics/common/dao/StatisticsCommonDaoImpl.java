package net.ihe.gazelle.tm.statistics.common.dao;

import net.ihe.gazelle.tm.gazelletest.model.instance.Status;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstanceParticipantsQuery;
import net.ihe.gazelle.tm.statistics.common.StatisticsCommonDao;
import net.ihe.gazelle.tm.systems.model.*;
import net.ihe.gazelle.tm.users.model.ConnectathonParticipantQuery;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import java.util.List;

@Scope(ScopeType.STATELESS)
@AutoCreate
@Name(value = "statisticsCommonDao")
public class StatisticsCommonDaoImpl implements StatisticsCommonDao {

    @Override
    public TestInstanceParticipantsQuery getTestInstanceParticipantsQuery(TestingSession selectedTestingSession) {
        TestInstanceParticipantsQuery testInstanceParticipantsQuery = new TestInstanceParticipantsQuery();
        testInstanceParticipantsQuery.testInstance().testingSession().eq(selectedTestingSession);
        testInstanceParticipantsQuery.testInstance().lastStatus().neq(Status.ABORTED);
        testInstanceParticipantsQuery.systemInSessionUser().systemInSession().system().isTool().eq(false);
        testInstanceParticipantsQuery.testInstance().test().testType().keyword().neq("preparatory");
        return testInstanceParticipantsQuery;
    }

    @Override
    public int getNumberOfSessionParticipants(TestingSession selectedTestingSession) {
        ConnectathonParticipantQuery connectathonParticipantQuery = new ConnectathonParticipantQuery();
        connectathonParticipantQuery.testingSession().id().eq(selectedTestingSession.getId());

        return connectathonParticipantQuery.getCountDistinctOnPath();
    }

    @Override
    public int getNumberOfRegisteredInstitutionWithAttendee(TestingSession selectedTestingSession) {

        ConnectathonParticipantQuery connectathonParticipantQuery = new ConnectathonParticipantQuery();
        connectathonParticipantQuery.testingSession().id().eq(selectedTestingSession.getId());
        connectathonParticipantQuery.institution().activated().eq(true);

        return connectathonParticipantQuery.institution().getCountDistinctOnPath();
    }

    @Override
    public int getNumberOfRegisteredInstitutionWithSystem(TestingSession selectedTestingSession) {
        SystemInSessionQuery systemInSessionQuery = new SystemInSessionQuery();
        systemInSessionQuery.testingSession().id().eq(selectedTestingSession.getId());
        systemInSessionQuery.system().institutionSystems().institution().activated().eq(true);
        return systemInSessionQuery.system().institutionSystems().institution().getCountDistinctOnPath();
    }

    @Override
    public int getNumberOfRegisteredSystemInSession(TestingSession selectedTestingSession) {
        SystemInSessionQuery systemInSessionQuery = getSystemInSessionQuery(selectedTestingSession);
        return systemInSessionQuery.getCountDistinctOnPath();
    }

    @Override
    public int getNumberOfDomainForCurrentSession(TestingSession selectedTestingSession) {
        TestingSessionQuery testingSessionQuery = new TestingSessionQuery();
        testingSessionQuery.id().eq(selectedTestingSession.getId());
        return testingSessionQuery.integrationProfiles().domainsProfile().domain().getCountDistinctOnPath();
    }

    @Override
    public int getNumberOfIntegrationProfilesForCurrentSession(TestingSession testingSession) {
        TestingSessionQuery testingSessionQuery = new TestingSessionQuery();
        testingSessionQuery.id().eq(testingSession.getId());
        return testingSessionQuery.integrationProfiles().id().getCountDistinctOnPath();
    }

    public SystemInSessionQuery getSystemInSessionQuery(TestingSession selectedTestingSession) {
        SystemInSessionQuery systemInSessionQuery = new SystemInSessionQuery();
        systemInSessionQuery.testingSession().id().eq(selectedTestingSession.getId());
        return systemInSessionQuery;
    }

    @Override
    public List<ProfileInTestingSession> getAllProfileInTestingSessionForCurrentSession(TestingSession selectedTestingSession) {
        ProfileInTestingSessionQuery profileInTestingSessionQuery = new ProfileInTestingSessionQuery();
        profileInTestingSessionQuery.testingSession().id().eq(selectedTestingSession.getId());
        return profileInTestingSessionQuery.getListDistinct();
    }
}
