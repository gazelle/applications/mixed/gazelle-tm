package net.ihe.gazelle.tm.systems.dao;

import net.ihe.gazelle.tf.model.Domain;
import net.ihe.gazelle.tf.model.IntegrationProfile;
import net.ihe.gazelle.tm.systems.ProfileInTestingSessionDAO;
import net.ihe.gazelle.tm.systems.model.ProfileInTestingSession;
import net.ihe.gazelle.tm.systems.model.ProfileInTestingSessionQuery;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import javax.persistence.EntityManager;
import java.util.List;

@Name(value = "profileInTestingSessionDAO")
@AutoCreate
@Scope(ScopeType.STATELESS)
public class ProfileInTestingSessionDAOImpl implements ProfileInTestingSessionDAO {

    @In
    EntityManager entityManager;

    @Override
    public List<ProfileInTestingSession> getAllProfileInTestingSessionsForCurrentSession(TestingSession selectedTestingSession) {
        ProfileInTestingSessionQuery profileInTestingSessionQuery = new ProfileInTestingSessionQuery();
        profileInTestingSessionQuery.testingSession().id().eq(selectedTestingSession.getId());
        return profileInTestingSessionQuery.getListDistinct();
    }

    @Override
    public boolean isProfileInTestingSessionAlreadyCreated(Domain domain, IntegrationProfile integrationProfile,TestingSession selectedTestingSession) {
        ProfileInTestingSessionQuery profileInTestingSessionQuery = new ProfileInTestingSessionQuery();
        profileInTestingSessionQuery.domain().id().eq(domain.getId());
        profileInTestingSessionQuery.testingSession().id().eq(selectedTestingSession.getId());
        profileInTestingSessionQuery.integrationProfile().id().eq(integrationProfile.getId());

        return profileInTestingSessionQuery.getUniqueResult()==null;
    }

    @Override
    public void saveProfileInTestingSession(ProfileInTestingSession profileInTestingSession) {
        entityManager.persist(profileInTestingSession);
    }

    @Override
    public void updateProfileInTestingSession(ProfileInTestingSession profileInTestingSession) {
        entityManager.merge(profileInTestingSession);
    }
}
