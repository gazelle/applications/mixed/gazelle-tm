package net.ihe.gazelle.tm.systems;

import net.ihe.gazelle.tm.systems.model.System;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import net.ihe.gazelle.users.model.Institution;

import java.util.List;

public interface SystemUnderTestService {


    List<SystemSummary> getRegisteredSystems(TestingSession testingSession, Institution institution);
    List<SystemSummary> getRegisteredSystemsOfSession(TestingSession testingSession);
    boolean hasSystemMissingDependency(System system);

    boolean isThereAcceptedSystem(TestingSession testingSession, Institution institution);

    boolean isThereRegisteredSystems(TestingSession testingSession, Institution institution);

    long getNumberOfAcceptedSystems(TestingSession testingSession, Institution institution);


}
