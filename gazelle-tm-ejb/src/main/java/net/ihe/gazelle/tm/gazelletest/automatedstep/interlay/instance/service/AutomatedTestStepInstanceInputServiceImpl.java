package net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.instance.service;

import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import net.ihe.gazelle.tm.gazelletest.action.TestStepDataNotFoundException;
import net.ihe.gazelle.tm.gazelletest.automatedstep.domain.instance.AutomatedTestStepInstanceData;
import net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.instance.dao.AutomatedTestStepInstanceFileSystemDAO;
import net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.instance.dao.AutomatedTestStepInstanceInputDAO;
import net.ihe.gazelle.tm.gazelletest.dao.TestInstanceDataDAO;
import net.ihe.gazelle.tm.gazelletest.domain.AutomatedTestStepInputDomain;
import net.ihe.gazelle.tm.gazelletest.model.instance.AutomatedTestStepInstanceInput;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstance;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestStepsData;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestStepsInstance;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Name("automatedTestStepInstanceInputService")
@Scope(ScopeType.STATELESS)
@AutoCreate
public class AutomatedTestStepInstanceInputServiceImpl implements AutomatedTestStepInstanceInputService {

    @In(value = "gumUserService")
    private UserService userService;
    @In(value = "testInstanceDataDAO")
    private TestInstanceDataDAO testInstanceDataDAO;
    @In(value = "automatedTestStepInstanceInputDAO")
    private AutomatedTestStepInstanceInputDAO automatedTestStepInstanceInputDAO;
    @In(value = "automatedTestStepInstanceFileSystemDAO")
    private AutomatedTestStepInstanceFileSystemDAO automatedTestStepInstanceFileSystemDAO;

    public AutomatedTestStepInstanceInputServiceImpl() {
        // empty for injection
    }

    public AutomatedTestStepInstanceInputServiceImpl(TestInstanceDataDAO testInstanceDataDAO, AutomatedTestStepInstanceInputDAO automatedTestStepInstanceInputDAO, AutomatedTestStepInstanceFileSystemDAO automatedTestStepInstanceFileSystemDAO) {
        this.testInstanceDataDAO = testInstanceDataDAO;
        this.automatedTestStepInstanceInputDAO = automatedTestStepInstanceInputDAO;
        this.automatedTestStepInstanceFileSystemDAO = automatedTestStepInstanceFileSystemDAO;
    }


    @Override
    public AutomatedTestStepInstanceInput createAutomatedTestStepInstanceInput(AutomatedTestStepInputDomain inputDefinition, Integer testStepsInstanceId) {
        return automatedTestStepInstanceInputDAO.createAutomatedTestStepInstanceInput(inputDefinition, testStepsInstanceId);
    }

    @Override
    public void saveInputData(Integer automatedTestStepInstanceInputId, Integer testStepsInstanceId, String fileName, String fileContent) {
        Integer testStepsDataId = testInstanceDataDAO.createAutomatedTestStepData(fileName, testStepsInstanceId).getId();
        automatedTestStepInstanceFileSystemDAO.saveInputDataFile(testStepsDataId, fileName, fileContent);
        automatedTestStepInstanceInputDAO.setTestStepDataIdToInstanceInput(automatedTestStepInstanceInputId, testStepsDataId);
    }

    @Override
    public String readInputData(AutomatedTestStepInstanceData inputData) {
        return automatedTestStepInstanceFileSystemDAO.readInputDataFile(inputData);
    }

    @Override
    public void deleteInputData(AutomatedTestStepInstanceData inputData) {
        automatedTestStepInstanceFileSystemDAO.deleteInputDataFile(inputData);
        testInstanceDataDAO.removeTestStepsDataFromTestStepsInstance(inputData);
        automatedTestStepInstanceInputDAO.setTestStepDataIdToInstanceInput(inputData.getTestStepInstanceInputId(), null);
    }

    @Override
    public List<AutomatedTestStepInstanceData> getInputDataFromTestStepInstance(TestStepsInstance stepInstance) {
        List<AutomatedTestStepInstanceData> inputDataList = new ArrayList<>();
        if (stepInstance != null) {
            for (AutomatedTestStepInstanceInput instanceInput : automatedTestStepInstanceInputDAO.getInstanceInputWithoutData(stepInstance.getId())) {
                AutomatedTestStepInstanceData inputData = new AutomatedTestStepInstanceData(instanceInput.getId(), stepInstance.getId(), instanceInput.getKey(), instanceInput.getLabel(), instanceInput.getDescription(), instanceInput.getRequired());
                try {
                    TestStepsData data = testInstanceDataDAO.getTestStepDataById(instanceInput.getTestStepDataId());
                    if (data != null) {
                        inputData.setFileName(data.getValue());
                        inputData.setTestStepDataId(data.getId());
                        inputData.setUserId(data.getLastModifierId());
                        SimpleDateFormat desiredFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
                        inputData.setLastUpdate(desiredFormat.format(data.getLastChanged()));
                    }
                } catch (TestStepDataNotFoundException e) {
                    // do nothing
                }
                inputDataList.add(inputData);
            }
        }
        return inputDataList;
    }

    @Override
    public Date getLatestUploadDate(Integer testStepInstanceId) {
        List<AutomatedTestStepInstanceInput> inputs = automatedTestStepInstanceInputDAO.getInstanceInputWithoutData(testStepInstanceId);
        Date latestUploadDate = null;
        for (AutomatedTestStepInstanceInput input : inputs) {
            if (latestUploadDate == null || input.getLastChanged().after(latestUploadDate)) {
                latestUploadDate = input.getLastChanged();
            }
        }
        return latestUploadDate;
    }

    @Override
    public List<AutomatedTestStepInstanceInput> getInstanceInputForTestInstance(TestInstance testInstance) {
        return automatedTestStepInstanceInputDAO.getInstanceInputForTestInstance(testInstance);
    }

    @Override
    public List<AutomatedTestStepInstanceInput> getInstanceInputForTestStepInstance(TestStepsInstance testStepsInstance) {
        return automatedTestStepInstanceInputDAO.getInstanceInputWithoutData(testStepsInstance.getId());
    }
}
