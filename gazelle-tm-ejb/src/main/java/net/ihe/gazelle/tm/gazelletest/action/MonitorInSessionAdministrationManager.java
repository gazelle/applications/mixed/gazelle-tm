package net.ihe.gazelle.tm.gazelletest.action;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManagerImpl;
import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.list.GazelleListDataModel;
import net.ihe.gazelle.common.filter.list.NoHQLDataModel;
import net.ihe.gazelle.common.report.ReportExporterManager;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import net.ihe.gazelle.ssov7.gum.client.application.Group;
import net.ihe.gazelle.ssov7.gum.client.application.Role;
import net.ihe.gazelle.ssov7.gum.client.application.User;
import net.ihe.gazelle.ssov7.gum.client.application.service.SortOrder;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import net.ihe.gazelle.ssov7.gum.client.interlay.client.utils.UserSearchParams;
import net.ihe.gazelle.tf.model.Actor;
import net.ihe.gazelle.tf.model.ActorIntegrationProfileOption;
import net.ihe.gazelle.tf.model.Domain;
import net.ihe.gazelle.tf.model.IntegrationProfile;
import net.ihe.gazelle.tm.datamodel.MonitorUser;
import net.ihe.gazelle.tm.gazelletest.model.definition.*;
import net.ihe.gazelle.tm.gazelletest.model.instance.MonitorInSession;
import net.ihe.gazelle.tm.gazelletest.model.instance.MonitorInSessionQuery;
import net.ihe.gazelle.tm.session.TestingSessionService;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import net.ihe.gazelle.users.model.Institution;
import net.sf.jasperreports.engine.JRException;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.*;


@Name("monitorInSessionAdministrationManager")
@Scope(ScopeType.PAGE)
public class MonitorInSessionAdministrationManager implements Serializable, QueryModifier<MonitorInSession> {

   public static final String SEARCH_BY_DOMAIN_LABEL_TO_DISPLAY = "gazelle.common.button.searchByDomain";
   public static final String SEARCH_BY_PROFILE_LABEL_TO_DISPLAY = "gazelle.common.button.searchByIntegrationProfile";
   public static final String SEARCH_BY_ACTOR_LABEL_TO_DISPLAY = "gazelle.common.button.searchByActor";
   private static final long serialVersionUID = 1L;
   private static final Logger LOG = LoggerFactory.getLogger(MonitorInSessionAdministrationManager.class);
   private final ApplicationPreferenceManager applicationPreferenceManager = new ApplicationPreferenceManagerImpl();
   private MonitorInSession selectedMonitorInSession;
   private Domain selectedDomain;
   private IntegrationProfile selectedIntegrationProfile;
   private Actor selectedActor;
   private String selectedCriterion;
   private List<String> selectedMonitors = new ArrayList<>();
   private List<Test> availableTests;
   private List<Test> selectedTests;
   private Filter<MonitorInSession> filter;
   private boolean showMonitorInSessionListPanel = true;
   private boolean showAddMonitorInSessionPanel = false;
   private boolean showEditMonitorInSessionPanel = false;
   private boolean showViewMonitorInSessionPanel = false;
   private boolean showAddTestsPanel = false;
   private List<Test> testsWithNoMonitors;
   private Institution selectedInstitution;
   @In
   private GazelleIdentity identity;
   @In(value = "gumUserService")
   private transient UserService userService;
   @In(value = "testingSessionService")
   private transient TestingSessionService testingSessionService;

   public Institution getSelectedInstitution() {
      if (LOG.isDebugEnabled()) {
         LOG.debug("getSelectedInstitution");
      }
      return selectedInstitution;
   }

   public void setSelectedInstitution(Institution selectedInstitution) {
      if (LOG.isDebugEnabled()) {
         LOG.debug("setSelectedInstitution");
      }
      this.selectedInstitution = selectedInstitution;
   }

   public NoHQLDataModel<MonitorInSession> getFoundMonitorInSessionList() {
      if (LOG.isDebugEnabled()) {
         LOG.debug("getFoundMonitorInSessionList");
      }
      final List<MonitorInSession> monitorInSessions = getMonitorInSessions();
      return new GazelleListDataModel<>(monitorInSessions);
   }

   public NoHQLDataModel<MonitorUser> getMonitorUserDataModel() {
      List<MonitorInSession> monitorInSessions = getMonitorInSessions();
      if (monitorInSessions == null) {
         monitorInSessions = new ArrayList<>();
      }
      final List<MonitorUser> monitorUsers = new ArrayList<>();
      for (MonitorInSession monitorInSession : monitorInSessions) {
         try {
            monitorUsers.add(new MonitorUser(userService.getUserById(monitorInSession.getUserId()), monitorInSession));
         }catch (NoSuchElementException e){
            logUserNotFoundWarn(monitorInSession);
         }
      }
      return new GazelleListDataModel<>(monitorUsers);
   }

   private void logUserNotFoundWarn(MonitorInSession monitorInSession) {
      LOG.warn("User with id {} not found", monitorInSession.getUserId());
   }

   private List<MonitorInSession> getMonitorInSessions() {
      return MonitorInSession.getAllActivatedMonitorsForATestingSession(testingSessionService.getUserTestingSession());
   }

   public Filter<MonitorInSession> getFilter() {
      if (LOG.isDebugEnabled()) {
         LOG.debug("getFilter");
      }
      if (filter == null) {
         filter = new Filter<>(getHQLCriterions());
      }
      return filter;
   }

   private HQLCriterionsForFilter<MonitorInSession> getHQLCriterions() {
      MonitorInSessionQuery query = new MonitorInSessionQuery();
      HQLCriterionsForFilter<MonitorInSession> criterionsForFilter = query.getHQLCriterionsForFilter();
      criterionsForFilter.addQueryModifier(this);
      return criterionsForFilter;
   }

   public List<String> getPossibleSearchCriterionList() {
      if (LOG.isDebugEnabled()) {
         LOG.debug("getPossibleSearchCriterionList");
      }
      List<String> possibleSearchCriterionList = new ArrayList<>();
      possibleSearchCriterionList.add(SEARCH_BY_DOMAIN_LABEL_TO_DISPLAY);
      possibleSearchCriterionList.add(SEARCH_BY_PROFILE_LABEL_TO_DISPLAY);
      possibleSearchCriterionList.add(SEARCH_BY_ACTOR_LABEL_TO_DISPLAY);
      return possibleSearchCriterionList;
   }

   public String getSEARCH_BY_DOMAIN_LABEL_TO_DISPLAY() {
      if (LOG.isDebugEnabled()) {
         LOG.debug("getSEARCH_BY_DOMAIN_LABEL_TO_DISPLAY");
      }
      return SEARCH_BY_DOMAIN_LABEL_TO_DISPLAY;
   }

   public String getSEARCH_BY_PROFILE_LABEL_TO_DISPLAY() {
      if (LOG.isDebugEnabled()) {
         LOG.debug("getSEARCH_BY_PROFILE_LABEL_TO_DISPLAY");
      }
      return SEARCH_BY_PROFILE_LABEL_TO_DISPLAY;
   }

   public String getSEARCH_BY_ACTOR_LABEL_TO_DISPLAY() {
      if (LOG.isDebugEnabled()) {
         LOG.debug("getSEARCH_BY_ACTOR_LABEL_TO_DISPLAY");
      }
      return SEARCH_BY_ACTOR_LABEL_TO_DISPLAY;
   }

   public String getSelectedCriterion() {
      if (LOG.isDebugEnabled()) {
         LOG.debug("getSelectedCriterion");
      }
      return selectedCriterion;
   }

   public void setSelectedCriterion(String selectedCriterion) {
      if (LOG.isDebugEnabled()) {
         LOG.debug("setSelectedCriterion");
      }
      this.selectedCriterion = selectedCriterion;
   }

   public List<Test> getSelectedTests() {
      if (LOG.isDebugEnabled()) {
         LOG.debug("getSelectedTests");
      }
      return selectedTests;
   }

   public void setSelectedTests(List<Test> selectedTests) {
      if (LOG.isDebugEnabled()) {
         LOG.debug("setSelectedTests");
      }
      this.selectedTests = selectedTests;
   }

   public List<Test> getAvailableTests() {
      if (LOG.isDebugEnabled()) {
         LOG.debug("getAvailableTests");
      }
      return availableTests;
   }

   public void setAvailableTests(List<Test> availableTests) {
      if (LOG.isDebugEnabled()) {
         LOG.debug("setAvailableTests");
      }
      this.availableTests = availableTests;
   }

   public Domain getSelectedDomain() {
      if (LOG.isDebugEnabled()) {
         LOG.debug("getSelectedDomain");
      }
      return selectedDomain;
   }

   public void setSelectedDomain(Domain selectedDomain) {
      if (LOG.isDebugEnabled()) {
         LOG.debug("setSelectedDomain");
      }
      this.selectedDomain = selectedDomain;
   }

   public IntegrationProfile getSelectedIntegrationProfile() {
      if (LOG.isDebugEnabled()) {
         LOG.debug("getSelectedIntegrationProfile");
      }
      return selectedIntegrationProfile;
   }

   public void setSelectedIntegrationProfile(IntegrationProfile selectedIntegrationProfile) {
      if (LOG.isDebugEnabled()) {
         LOG.debug("setSelectedIntegrationProfile");
      }
      this.selectedIntegrationProfile = selectedIntegrationProfile;
   }

   public Actor getSelectedActor() {
      if (LOG.isDebugEnabled()) {
         LOG.debug("getSelectedActor");
      }
      return selectedActor;
   }

   public void setSelectedActor(Actor selectedActor) {
      if (LOG.isDebugEnabled()) {
         LOG.debug("setSelectedActor");
      }
      this.selectedActor = selectedActor;
   }

   public boolean isShowMonitorInSessionListPanel() {
      if (LOG.isDebugEnabled()) {
         LOG.debug("isShowMonitorInSessionListPanel");
      }
      return showMonitorInSessionListPanel;
   }

   public void setShowMonitorInSessionListPanel(boolean showMonitorInSessionListPanel) {
      if (LOG.isDebugEnabled()) {
         LOG.debug("setShowMonitorInSessionListPanel");
      }
      this.showMonitorInSessionListPanel = showMonitorInSessionListPanel;
   }

   public boolean isShowEditMonitorInSessionPanel() {
      if (LOG.isDebugEnabled()) {
         LOG.debug("isShowEditMonitorInSessionPanel");
      }
      return showEditMonitorInSessionPanel;
   }

   public void setShowEditMonitorInSessionPanel(boolean showEditMonitorInSessionPanel) {
      if (LOG.isDebugEnabled()) {
         LOG.debug("setShowEditMonitorInSessionPanel");
      }
      this.showEditMonitorInSessionPanel = showEditMonitorInSessionPanel;
   }

   public boolean isShowAddMonitorInSessionPanel() {
      if (LOG.isDebugEnabled()) {
         LOG.debug("isShowAddMonitorInSessionPanel");
      }
      return showAddMonitorInSessionPanel;
   }

   public void setShowAddMonitorInSessionPanel(boolean showAddMonitorInSessionPanel) {
      if (LOG.isDebugEnabled()) {
         LOG.debug("setShowAddMonitorInSessionPanel");
      }
      this.showAddMonitorInSessionPanel = showAddMonitorInSessionPanel;
   }

   public MonitorInSession getSelectedMonitorInSession() {
      if (LOG.isDebugEnabled()) {
         LOG.debug("getSelectedMonitorInSession");
      }
      return selectedMonitorInSession;
   }

   public void setSelectedMonitorInSession(MonitorInSession selectedMonitorInSession) {
      if (LOG.isDebugEnabled()) {
         LOG.debug("setSelectedMonitorInSession");
      }
      this.selectedMonitorInSession = selectedMonitorInSession;
   }

   public boolean isShowViewMonitorInSessionPanel() {
      if (LOG.isDebugEnabled()) {
         LOG.debug("isShowViewMonitorInSessionPanel");
      }
      return showViewMonitorInSessionPanel;
   }

   public void setShowViewMonitorInSessionPanel(boolean showViewMonitorInSessionPanel) {
      if (LOG.isDebugEnabled()) {
         LOG.debug("setShowViewMonitorInSessionPanel");
      }
      this.showViewMonitorInSessionPanel = showViewMonitorInSessionPanel;
   }

   public boolean isShowAddTestsPanel() {
      if (LOG.isDebugEnabled()) {
         LOG.debug("isShowAddTestsPanel");
      }
      return showAddTestsPanel;
   }

   public void setShowAddTestsPanel(boolean showAddTestsPanel) {
      if (LOG.isDebugEnabled()) {
         LOG.debug("setShowAddTestsPanel");
      }
      this.showAddTestsPanel = showAddTestsPanel;
   }

   public List<Test> getTestsWithNoMonitors() {
      if (LOG.isDebugEnabled()) {
         LOG.debug("getTestsWithNoMonitors");
      }
      return testsWithNoMonitors;
   }

   public void setTestsWithNoMonitors(List<Test> testsWithNoMonitors) {
      if (LOG.isDebugEnabled()) {
         LOG.debug("setTestsWithNoMonitors");
      }
      this.testsWithNoMonitors = testsWithNoMonitors;
   }

   private void resetSearchValues() {
      selectedCriterion = null;
      selectedActor = null;
      selectedDomain = null;
      selectedIntegrationProfile = null;
      selectedTests = new ArrayList<>();
      availableTests = new ArrayList<>();
   }

   public List<String> getSelectedMonitors() {
      if (LOG.isDebugEnabled()) {
         LOG.debug("getSelectedMonitors");
      }
      return selectedMonitors;
   }

   public void setSelectedMonitors(List<String> selectedMonitors) {
      if (LOG.isDebugEnabled()) {
         LOG.debug("setSelectedMonitors");
      }
      this.selectedMonitors = selectedMonitors;
   }

   public User getSelectedUserByMonitorId() {
      if (selectedMonitorInSession != null) {
         return userService.getUserById(selectedMonitorInSession.getUserId());
      } else {
         return new User();
      }
   }

   public List<SelectItem> getPossibleMonitorsSelectItems() {
      List<SelectItem> selectItems = new ArrayList<>();
      for(User user : getPossibleMonitors().values()) {
         selectItems.add(new SelectItem(user.getId(), user.getFirstNameAndLastName()));
      }
      return selectItems;
   }

   private Map<String, User> getPossibleMonitors() {
      if (LOG.isDebugEnabled()) {
         LOG.debug("getMonitorsList");
      }
      Map<String, User> monitorsMap = new LinkedHashMap<>();
      List<User> monitors;
      UserSearchParams userSearchParams = new UserSearchParams()
              .setGroup(Group.GRP_MONITOR)
              .setActivated(true)
              .setSortBy("firstName").setSortOrder(SortOrder.ASC);

      if (this.selectedInstitution != null) {
         userSearchParams.setOrganizationId(this.selectedInstitution.getKeyword());
         monitors = userService.searchNoLimit(userSearchParams);
      } else {
         monitors = userService.searchNoLimit(userSearchParams);
      }
      if (monitors != null) {
         Collections.sort(monitors);
         for (User user : monitors) {
            monitorsMap.put(user.getId(), user);
         }
      }
      List<MonitorInSession> monitorInSessionList = MonitorInSession.getAllActivatedMonitorsForATestingSession(
              testingSessionService.getUserTestingSession());
      if (monitorInSessionList != null) {
         for (MonitorInSession monitorInSession : monitorInSessionList) {
            monitorsMap.remove(monitorInSession.getUserId());
         }

      }
      return monitorsMap;
   }

   public void initMonitorInSessionManagement() {
      if (LOG.isDebugEnabled()) {
         LOG.debug("initMonitorInSessionManagement");
      }
      availableTests = new ArrayList<>();
      selectedTests = new ArrayList<>();
      hideAddMonitorInSessionPanel();
      updateMonitorInSessionListStatus();
   }

   public void showEditMonitorInSessionPanel() {
      if (LOG.isDebugEnabled()) {
         LOG.debug("showEditMonitorInSessionPanel");
      }
      this.setShowAddMonitorInSessionPanel(false);
      this.setShowEditMonitorInSessionPanel(true);
      this.setShowMonitorInSessionListPanel(false);
      this.setShowViewMonitorInSessionPanel(false);
   }

   public void hideEditMonitorInSessionPanel() {
      if (LOG.isDebugEnabled()) {
         LOG.debug("hideEditMonitorInSessionPanel");
      }
      hideAddMonitorInSessionPanel();
   }

   public void showAddMonitorInSessionPanel() {
      if (LOG.isDebugEnabled()) {
         LOG.debug("showAddMonitorInSessionPanel");
      }
      this.setShowAddMonitorInSessionPanel(true);
      this.setShowEditMonitorInSessionPanel(false);
      this.setShowMonitorInSessionListPanel(true);
      this.setShowViewMonitorInSessionPanel(false);
   }

   public void hideAddMonitorInSessionPanel() {
      if (LOG.isDebugEnabled()) {
         LOG.debug("hideAddMonitorInSessionPanel");
      }
      this.setShowAddMonitorInSessionPanel(false);
      this.setShowEditMonitorInSessionPanel(false);
      this.setShowMonitorInSessionListPanel(true);
      this.setShowViewMonitorInSessionPanel(false);
   }

   public void showViewMonitorInSessionPanel() {
      if (LOG.isDebugEnabled()) {
         LOG.debug("showViewMonitorInSessionPanel");
      }
      this.setShowAddMonitorInSessionPanel(false);
      this.setShowEditMonitorInSessionPanel(false);
      this.setShowMonitorInSessionListPanel(false);
      this.setShowViewMonitorInSessionPanel(true);
   }

   public void showAddTestsPanel() {
      if (LOG.isDebugEnabled()) {
         LOG.debug("showAddTestsPanel");
      }
      this.setShowAddTestsPanel(true);
      resetSearchValues();
   }

   private void updateMonitorInSessionListStatus() {
      List<MonitorInSession> monitorInSessionList = MonitorInSession
            .getAllActivatedMonitorsForATestingSession(testingSessionService.getUserTestingSession());
      if (monitorInSessionList != null) {
         EntityManager entityManager = EntityManagerService.provideEntityManager();
         for (MonitorInSession monitorInSession : monitorInSessionList) {
            try {
               // if the user is no more monitor, the related monitorInSession must be deactivated
               User user = userService.getUserById(monitorInSession.getUserId());
               if (!user.hasRole(Role.MONITOR)) {
                  monitorInSession.setActivated(false);
                  entityManager.merge(monitorInSession);
               }
            }catch (NoSuchElementException e){
               logUserNotFoundWarn(monitorInSession);
            }


         }
         entityManager.flush();
      }
   }

   public void addMonitorsToActivatedTestingSession() {
      if (LOG.isDebugEnabled()) {
         LOG.debug("addMonitorsToActivatedTestingSession");
      }
      EntityManager entityManager = EntityManagerService.provideEntityManager();
      for (String userId : selectedMonitors) {
         MonitorInSession monitorInSession;
         TestingSession currentTestingSession = testingSessionService.getUserTestingSession();
         List<MonitorInSession> disactivatedMonitorInSessionList = MonitorInSession
               .getDisactivatedMonitorInSessionForATestingSessionByUser(
                       currentTestingSession, userId);
         if ((disactivatedMonitorInSessionList != null) && (disactivatedMonitorInSessionList.size() == 1)) {
            monitorInSession = disactivatedMonitorInSessionList.get(0);
            monitorInSession.setActivated(true);
         } else {
            List<Test> testList = new ArrayList<>();
            monitorInSession = new MonitorInSession(userId, testList, true, currentTestingSession);
         }
         entityManager.merge(monitorInSession);
      }
      entityManager.flush();
      selectedMonitors = new ArrayList<>();
      hideAddMonitorInSessionPanel();
   }

   public void editMonitorInSession(MonitorInSession inMonitorInSession) {
      if (LOG.isDebugEnabled()) {
         LOG.debug("editMonitorInSession");
      }
      this.selectedMonitorInSession = inMonitorInSession;
      editSelectedMonitorInSession();
   }

   public void editSelectedMonitorInSession() {
      if (LOG.isDebugEnabled()) {
         LOG.debug("editSelectedMonitorInSession");
      }
      showEditMonitorInSessionPanel();
      selectedCriterion = null;
      selectedDomain = null;
      selectedActor = null;
      selectedIntegrationProfile = null;
      setShowAddTestsPanel((selectedMonitorInSession.getTestList() == null)
            || (selectedMonitorInSession.getTestList().size() == 0));
      selectedTests = new ArrayList<>();
      availableTests = new ArrayList<>();
   }

   public void deleteMonitorInSession(MonitorInSession inMonitorInSession) {
      if (LOG.isDebugEnabled()) {
         LOG.debug("deleteMonitorInSession");
      }

      this.selectedMonitorInSession = inMonitorInSession;
   }

   public void deleteSelectedMonitorInSession() {
      if (LOG.isDebugEnabled()) {
         LOG.debug("deleteSelectedMonitorInSession");
      }
      EntityManager entityManager = EntityManagerService.provideEntityManager();
      MonitorInSession monitorToDelete = entityManager.find(MonitorInSession.class, selectedMonitorInSession.getId());
      entityManager.remove(monitorToDelete);
      entityManager.flush();
      selectedMonitorInSession = null;
      hideEditMonitorInSessionPanel();
   }

   private List<Test> getAvailableTestsForSelectedCriteria() {
      List<Test> result = null;
      List<TestType> testTypes;

      // Reload testing session to avoid LazyLoading issues.
      TestingSession testingSession = TestingSession.getSessionById(selectedMonitorInSession.getTestingSession().getId());
      if (!testingSession.isTestingInteroperability()) {
         testTypes = TestType.getTestTypesWithoutMESAandITB();
      } else {
         testTypes = TestType.getTestTypesWithoutMESA();
      }

      if (selectedCriterion != null) {
         if (selectedCriterion.equals(SEARCH_BY_DOMAIN_LABEL_TO_DISPLAY) && (selectedDomain != null)) {
            result = TestRoles.getTestFiltered(testTypes, TestStatus.getSTATUS_READY(),
                  null, null, null, selectedDomain, null, null, null, true, null);

         } else {
            if (selectedCriterion.equals(SEARCH_BY_PROFILE_LABEL_TO_DISPLAY)
                  && (selectedIntegrationProfile != null)) {
               result = TestRoles.getTestFiltered(testTypes,
                     TestStatus.getSTATUS_READY(), null, null, null, null, selectedIntegrationProfile, null,
                     null, true, null);
            } else {
               if (selectedCriterion.equals(SEARCH_BY_ACTOR_LABEL_TO_DISPLAY) && (selectedActor != null)) {
                  result = TestRoles.getTestFiltered(testTypes,
                        TestStatus.getSTATUS_READY(), null, null, null, null, null, selectedActor, null, true,
                        null);
               } else {
                  availableTests = new ArrayList<>();
               }
            }
         }
      }
      if (result != null) {
         Collections.sort(result);
      }
      return result;
   }

   private List<Test> getTestsForSelectedCriteriaForSelectedMonitorInSession() {
      List<Test> result;
      // Reload testingSession to avoid LazyLoadingException.
      TestingSession testingSession = TestingSession.getSessionById(selectedMonitorInSession.getTestingSession().getId());
      List<TestType> testTypes = testingSession.getTestTypes();
      if (selectedCriterion.equals(SEARCH_BY_DOMAIN_LABEL_TO_DISPLAY) && (selectedDomain != null)) {
         result = MonitorInSession.getTestsByMonitorByDomainByIPByActor(selectedMonitorInSession, testTypes,
               selectedDomain, null, null, null, true);
      } else {
         if (selectedCriterion.equals(SEARCH_BY_PROFILE_LABEL_TO_DISPLAY) && (selectedIntegrationProfile != null)) {
            result = MonitorInSession.getTestsByMonitorByDomainByIPByActor(selectedMonitorInSession, testTypes,
                  null, selectedIntegrationProfile, null, null, true);
         } else {
            if (selectedCriterion.equals(SEARCH_BY_ACTOR_LABEL_TO_DISPLAY) && (selectedActor != null)) {
               result = MonitorInSession.getTestsByMonitorByDomainByIPByActor(selectedMonitorInSession, testTypes,
                     null, null, selectedActor, null, true);
            } else {
               result = new ArrayList<>();
            }
         }
      }
      if (result != null) {
         Collections.sort(result);
      }
      return result;
   }

   public void findTestsForSelectedCriteria() {
      if (LOG.isDebugEnabled()) {
         LOG.debug("findTestsForSelectedCriteria");
      }
      availableTests = getAvailableTestsForSelectedCriteria();
      selectedTests = getTestsForSelectedCriteriaForSelectedMonitorInSession();
      List<Test> affectedTests = selectedMonitorInSession.getTestList();
      if (availableTests != null) {
         availableTests.removeAll(affectedTests);
      }
      if (selectedTests == null) {
         selectedTests = new ArrayList<>();
      }
      if (availableTests == null) {
         availableTests = new ArrayList<>();
      }
   }

   public List<Actor> getActorsForIntegrationProfile() {
      if (LOG.isDebugEnabled()) {
         LOG.debug("getActorsForIntegrationProfile");
      }
      if (selectedIntegrationProfile != null) {
         return ActorIntegrationProfileOption
               .getActorsFromAIPOFiltered(null, selectedIntegrationProfile, null, null);

      }
      return new ArrayList<>();
   }

   public void addSelectedTestsToMonitor() {
      if (LOG.isDebugEnabled()) {
         LOG.debug("addSelectedTestsToMonitor");
      }
      if (selectedMonitorInSession != null) {
         if (selectedTests != null) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            List<Test> testListToRemove = getTestsForSelectedCriteriaForSelectedMonitorInSession();
            List<Test> monitorTestList = selectedMonitorInSession.getTestList();
            if (monitorTestList != null) {
               if (testListToRemove != null) {
                  monitorTestList.removeAll(testListToRemove);
               }

            } else {
               monitorTestList = new ArrayList<>();
            }
            monitorTestList.addAll(selectedTests);
            selectedMonitorInSession.setTestList(monitorTestList);
            selectedMonitorInSession = entityManager.merge(selectedMonitorInSession);
            entityManager.flush();
            FacesMessages.instance().add(StatusMessage.Severity.INFO, "Selected tests are added to current monitor.");
         }
      }
   }

   public void viewMonitorInSession(MonitorInSession inMonitorInSession) {
      if (LOG.isDebugEnabled()) {
         LOG.debug("viewMonitorInSession");
      }
      setSelectedMonitorInSession(inMonitorInSession);
      showViewMonitorInSessionPanel();
   }

   public void removeTestFromMonitorList(Test inTest) {
      if (LOG.isDebugEnabled()) {
         LOG.debug("removeTestFromMonitorList");
      }
      if ((selectedMonitorInSession != null) && (inTest != null)) {
         List<Test> testList = selectedMonitorInSession.getTestList();
         if (testList != null) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            testList.remove(inTest);
            selectedMonitorInSession.setTestList(testList);
            selectedMonitorInSession = entityManager.merge(selectedMonitorInSession);
         }
      }
   }

   public void downloadSelectedTestsAsPdf() {
      if (LOG.isDebugEnabled()) {
         LOG.debug("downloadSelectedTestsAsPdf");
      }

      try {
         if (selectedMonitorInSession != null) {
            User monitorUser = userService.getUserById(selectedMonitorInSession.getUserId());
            String monitorName = monitorUser.getFirstName() + " " + monitorUser.getLastName();
            String monitorLogin = monitorUser.getEmail();
            String institution = monitorUser.getOrganizationId();

            if (!selectedMonitorInSession.getTestList().isEmpty()) {
               String selectedTestList = "";
               List<Integer> selectedTestsId = new ArrayList<>();
               for (Test test : selectedMonitorInSession.getTestList()) {
                  selectedTestsId.add(test.getId());
                  if (!selectedTestList.isEmpty()) {
                     selectedTestList = selectedTestList + "," + test.getId();
                  } else {
                     selectedTestList = Integer.toString(test.getId());
                  }
               }

               Map<String, Object> params = new HashMap<>();
               params.put("selectedIP", "");
               params.put("selectedActor", "");
               params.put("selectedInstitution", institution);
               params.put("selectedObjectName", monitorName);
               params.put("selectedObjectKeyword", monitorLogin);
               params.put("testList", selectedTestList);
               // get the application url to use it as parameter for the report
               String applicationurl = applicationPreferenceManager.getApplicationUrl();
               params.put("applicationurl", applicationurl);

               // get the testDescription Language to use it as parameter for the report
               List<Integer> res = new ArrayList<>();
               EntityManager em = EntityManagerService.provideEntityManager();
               for (Integer testId : selectedTestsId) {
                  Test test = em.find(Test.class, testId);
                  List<TestDescription> descriptionList = test.getTestDescription();
                  for (TestDescription testDescription : descriptionList) {
                     res.add(testDescription.getGazelleLanguage().getId());
                  }
                  if (!res.isEmpty()) {
                     //TODO GZL-4659
                     params.put("testDescriptionLanguageId", res.get(0));
                  }
               }
               ReportExporterManager.exportToPDF("gazelleMultipleTestReport", monitorLogin.replace(" ", "_")
                     + ".pdf", params);

            } else {
               LOG.error("no test selected");
            }
         } else {
            LOG.error("no monitor in session selected");
         }
      } catch (JRException e) {
         LOG.error(e.getMessage(), e);
         FacesMessages.instance().add(StatusMessage.Severity.ERROR, e.getMessage());
      }

   }

   public void downloadSelectedTestsAsPdf(MonitorInSession inMonitorInSession) {
      if (LOG.isDebugEnabled()) {
         LOG.debug("downloadSelectedTestsAsPdf");
      }
      setSelectedMonitorInSession(inMonitorInSession);
      downloadSelectedTestsAsPdf();
   }

   @Destroy
   public void destroy() {
      if (LOG.isDebugEnabled()) {
         LOG.debug("destroy");
      }

   }

   public boolean isConnectedUserMonitorForSelectedSession() {
      LOG.trace("isConnectedUserMonitorForSelectedSession");
      return MonitorInSession.isMonitorForSelectedSession(identity.getUsername(),
              testingSessionService.getUserTestingSession());
   }

   @Override
   public void modifyQuery(HQLQueryBuilder<MonitorInSession> hqlQueryBuilder, Map<String, Object> map) {
      if (LOG.isDebugEnabled()) {
         LOG.debug("modifyQuery");
      }
      MonitorInSessionQuery query = new MonitorInSessionQuery();
      hqlQueryBuilder.addRestriction(query.testingSession().eqRestriction(testingSessionService.getUserTestingSession()));
      hqlQueryBuilder.addRestriction(query.isActivated().eqRestriction(true));
   }

   public List<Institution> getInstitutionListForAllMonitorsInSession() {
      UserSearchParams userSearchParams = new UserSearchParams().setGroup(Role.MONITOR).setActivated(true);
      List<User> allMonitors = userService.searchNoLimit(userSearchParams);
      List<MonitorInSession> monitorsInSessionListUsed = getFoundMonitorInSessionList().getAllItems(FacesContext.getCurrentInstance());

      List<Institution> res = new ArrayList<>();

      for (MonitorInSession monitorUsed : monitorsInSessionListUsed) {
         allMonitors.remove(monitorUsed.getUserId());
      }

      for (User user : allMonitors) {
         res.add(Institution.findInstitutionWithKeyword(user.getOrganizationId()));
      }
      res = removeDuplicateInstitution(res);

      Collections.sort(res);
      return res;
   }

   public List<Institution> removeDuplicateInstitution(List<Institution> l) {
      // add elements to al, including duplicates
      Set<Institution> hs = new HashSet<>();
      hs.addAll(l);
      l.clear();
      l.addAll(hs);
      return l;
   }
}