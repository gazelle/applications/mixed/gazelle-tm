package net.ihe.gazelle.tm.systems;

import net.ihe.gazelle.tf.model.ActorIntegrationProfileOption;
import net.ihe.gazelle.tm.systems.model.System;
import net.ihe.gazelle.tm.systems.model.SystemInSession;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import net.ihe.gazelle.users.model.Institution;

import java.util.List;

public interface SystemUnderTestServiceDAO {

    List<SystemInSession> getSystemsOfOrganizationInSession(TestingSession testingSession, Institution institution);

    List<SystemInSession> getSystemsOfOrganizationInSessionOfSession(TestingSession testingSession);

    List<ActorIntegrationProfileOption> getActorsIntegrationProfiles(System system);

    long getNumberOfRegisteredSystemsInSession(TestingSession testingSession, Institution institution);

    long getNumberOfAcceptedSystemsInSession(TestingSession testingSession, Institution institution);

}
