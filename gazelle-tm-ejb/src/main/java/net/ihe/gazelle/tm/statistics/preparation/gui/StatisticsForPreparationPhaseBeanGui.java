package net.ihe.gazelle.tm.statistics.preparation.gui;

import net.ihe.gazelle.menu.Pages;
import net.ihe.gazelle.tm.gazelletest.model.definition.TestType;
import net.ihe.gazelle.tm.session.TestingSessionService;
import net.ihe.gazelle.tm.statistics.common.StatisticsCommonService;
import net.ihe.gazelle.tm.statistics.preparation.StatisticsForPreparationPhaseService;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Scope(ScopeType.EVENT)
@AutoCreate
@Name("statisticsForPreparationPhaseBeanGui")
public class StatisticsForPreparationPhaseBeanGui {

    private static final String WIDTH_STRING = "width:";
    @In(value = "statisticsForPreparationPhaseService")
    StatisticsForPreparationPhaseService statisticsForPreparationPhaseService;

    @In(value = "testingSessionService")
    private TestingSessionService testingSessionService;

    public int getNumberOfSessionParticipants() {
        return statisticsForPreparationPhaseService.getNumberOfSessionParticipants();
    }

    public int getNumberOfRegisteredInstitutionWithAttendee() {
        return statisticsForPreparationPhaseService.getNumberOfRegisteredInstitutionWithAttendee();
    }
    public int getNumberOfRegisteredInstitutionWithSystem() {
        return statisticsForPreparationPhaseService.getNumberOfRegisteredInstitutionWithSystem();
    }
    public int getNumberOfRegisteredSystemInSession() {
        return statisticsForPreparationPhaseService.getNumberOfRegisteredSystemInSession();
    }

    public int getNumberOfAcceptedSystems() {
        return statisticsForPreparationPhaseService.getNumberOfAcceptedSystemInSession();
    }

    public int getNumberOfNotAcceptedSystems() {
        return statisticsForPreparationPhaseService.getNumberOfNotAcceptedSystemInSession();
    }

    public int getNumberOfActivatedMonitorInSession() {
        return statisticsForPreparationPhaseService.getNumberOfActivatedMonitorInSession();
    }

    public int getNumberOfApprovedSupportiveRequests() {
        return statisticsForPreparationPhaseService.getNumberOfAcceptedSupportiveRequest();
    }

    public int getNumberOfNotYetApprovedSupportiveRequests() {
        return statisticsForPreparationPhaseService.getNumberOfNotYetAcceptedSupportiveRequest();
    }

    public int getNumberOfSUTNetworkConfigurations() {
        return statisticsForPreparationPhaseService.getNumberOfSUTNetworkConfiguration();
    }

    public int getNumberOfAcceptedSUTNetworkConfigurations(){
        return statisticsForPreparationPhaseService.getNumberOfSUTNetworkConfigurationAccepted();
    }

    public int getNumberOfNotAcceptedSutNetworkConfigurations(){
        return statisticsForPreparationPhaseService.getNumberOfSUTNetworkConfigurationNotAccepted();
    }

    public int getNumberOfGeneratedHosts(){
        return statisticsForPreparationPhaseService.getNumberOfGeneratedHosts();
    }

    public int getNumberOfGeneratedHostsWithIP(){
        return statisticsForPreparationPhaseService.getNumberOfGeneratedHostsWithIP();
    }

    public int getNumberOfGeneratedHostsWithoutIP(){
        return statisticsForPreparationPhaseService.getNumberOfGeneratedHostsWithoutIP();
    }

    public int getNumberOfPreparatoryTests(){
        return statisticsForPreparationPhaseService.getNumberOfPreparatoryTests();
    }

    public List<Integer> getStatisticsOfPreparatoryTests(){
        StatisticsCommonService.TestInstanceStats testInstanceStats =statisticsForPreparationPhaseService.getPreparatoryTestStatistics();
        int nbInProgress = testInstanceStats.getNbOfInProgress()+testInstanceStats.getNbOfPartiallyVerified()+ testInstanceStats.getNbOfTobVerified() + testInstanceStats.getNbOfCritical();

        return new ArrayList<>(Arrays.asList(testInstanceStats.getNbOfVerified(),testInstanceStats.getNbOfFailed(), nbInProgress));
    }
    public String getAcceptedSystemsStyle() {
        double nbAcceptedSystems = statisticsForPreparationPhaseService.getNumberOfAcceptedSystemInSession();
        double nbTotalSystems = nbAcceptedSystems + statisticsForPreparationPhaseService.getNumberOfNotAcceptedSystemInSession();
        double percentage = Math.round((nbAcceptedSystems / nbTotalSystems) * 100.0);
        return WIDTH_STRING + percentage + "%";
    }

    public String getNotAcceptedSystemsStyle() {
        double nbNotAcceptedSystems = statisticsForPreparationPhaseService.getNumberOfNotAcceptedSystemInSession();
        double nbTotalSystems = statisticsForPreparationPhaseService.getNumberOfAcceptedSystemInSession() + nbNotAcceptedSystems;
        double percentage = Math.round((nbNotAcceptedSystems / nbTotalSystems) * 100.0);
        return WIDTH_STRING + percentage + "%";
    }

    public String getApprovedSupportiveRequestsStyle() {
        double nbApprovedRequests = statisticsForPreparationPhaseService.getNumberOfAcceptedSupportiveRequest();
        double nbTotalRequests = nbApprovedRequests + statisticsForPreparationPhaseService.getNumberOfNotYetAcceptedSupportiveRequest();
        double percentage = Math.round((nbApprovedRequests / nbTotalRequests) * 100.0);

        return WIDTH_STRING + percentage + "%";
    }

    public String getNotYetApprovedSupportiveRequestsStyle() {
        double nbNotYetApprovedRequests = statisticsForPreparationPhaseService.getNumberOfNotYetAcceptedSupportiveRequest();
        double nbTotalRequests = nbNotYetApprovedRequests + statisticsForPreparationPhaseService.getNumberOfAcceptedSupportiveRequest();
        double percentage = Math.round((nbNotYetApprovedRequests / nbTotalRequests) * 100.0);

        return WIDTH_STRING + percentage + "%";
    }

    public String getAcceptedSUTNetworkConfigurationStyle() {
        double nbAcceptedConfigurations = statisticsForPreparationPhaseService.getNumberOfSUTNetworkConfigurationAccepted();
        double nbTotalConfigurations= nbAcceptedConfigurations+statisticsForPreparationPhaseService.getNumberOfSUTNetworkConfigurationNotAccepted();
        double percentage = Math.round((nbAcceptedConfigurations/nbTotalConfigurations)*100.0);

        return WIDTH_STRING + percentage + "%";
    }
    public String getNotAcceptedSUTNetworkConfigurationStyle() {
        double nbNotAcceptedConfigurations = statisticsForPreparationPhaseService.getNumberOfSUTNetworkConfigurationNotAccepted();
        double nbTotalConfigurations= nbNotAcceptedConfigurations+statisticsForPreparationPhaseService.getNumberOfSUTNetworkConfigurationAccepted();
        double percentage = Math.round((nbNotAcceptedConfigurations/nbTotalConfigurations)*100.0);

        return WIDTH_STRING + percentage + "%";
    }

    public String getNumberOfGeneratedHostsWithIPStyle(){
        double nbHostsWithIP=statisticsForPreparationPhaseService.getNumberOfGeneratedHostsWithIP();
        double nbTotalHosts = nbHostsWithIP + statisticsForPreparationPhaseService.getNumberOfGeneratedHostsWithoutIP();
        double percentage= Math.round((nbHostsWithIP/nbTotalHosts)*100.0);

        return WIDTH_STRING+percentage+"%";
    }

    public String getNumberOfGeneratedHostsWithoutIPStyle(){
        double nbHostsWithoutIP=statisticsForPreparationPhaseService.getNumberOfGeneratedHostsWithoutIP();
        double nbTotalHosts = nbHostsWithoutIP + statisticsForPreparationPhaseService.getNumberOfGeneratedHostsWithIP();
        double percentage= Math.round((nbHostsWithoutIP/nbTotalHosts)*100.0);

        return WIDTH_STRING+percentage+"%";
    }

    public List<String> getColorsForPreparatoryStatsChart(){
        return Arrays.asList(
                "\"#5cb85c\"",
                "\"red\"",
                "\"#4ca8de\"");
    }
    public String getManageAttendeesLink() {
        return Pages.REGISTRATION_PARTICIPANTS.getMenuLink();
    }

    public String getManageSUTsLink() {
        return Pages.REGISTRATION_SYSTEMS.getMenuLink();
    }
    public String getReviewSupportiveRequestsLink() {
        return Pages.REGISTRATION_REVIEW_SYSTEMS.getMenuLink();
    }

    public String getManageMonitorsLink() {
        return Pages.ADMIN_MANAGE_MONITORS.getMenuLink();
    }

    public String getProxyAndNetworkDetailsLink(){
        return Pages.CONFIG_NETWORK_OVERVIEW.getMenuLink();
    }

    public String getAllTestRunsPreparatoryFiltered(){
        return Pages.CAT_INSTANCES.getMenuLink()+ "?testTypeReal="+TestType.getTypeByKeyword("preparatory").getId();
    }

    public boolean isProxyUseEnabled(){
        return testingSessionService.getUserTestingSession().getIsProxyUseEnabled();
    }
}
