package net.ihe.gazelle.tm.gazelletest.action.conformitytestreport.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@XmlRootElement(name = "testingSession")
@XmlAccessorType(XmlAccessType.FIELD)
public class TestingSessionModel {

    @XmlElement(name = "id")
    private Integer idSession;

    @XmlElement(name = "description")
    private String description;

    @XmlElement(name = "startDate")
    private Date beginningSession;

    @XmlElement(name = "endDate")
    private Date endingSession;

    @XmlElement(name = "registrationDeadlineDate")
    private Date registrationDeadlineDate;

    @XmlElement(name = "isActive")
    private Boolean activeSession;

    @XmlElement(name = "isOpen")
    private Boolean openSession;

    @XmlElement(name = "isHidden")
    private Boolean hiddenSession;

    public TestingSessionModel() {
    }

    public TestingSessionModel(Integer idSession, String description, Date beginningSession, Date endingSession, Date registrationDeadlineDate, Boolean activeSession, Boolean openSession, Boolean hiddenSession) {
        this.idSession = idSession;
        this.description = description;
        this.beginningSession = beginningSession;
        this.endingSession = endingSession;
        this.registrationDeadlineDate = registrationDeadlineDate;
        this.activeSession = activeSession;
        this.openSession = !openSession;
        this.hiddenSession = hiddenSession;
    }

    public Integer getIdSession() {
        return idSession;
    }

    public void setIdSession(Integer idSession) {
        this.idSession = idSession;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getBeginningSession() {
        return beginningSession;
    }

    public void setBeginningSession(Date beginningSession) {
        this.beginningSession = beginningSession;
    }

    public Date getEndingSession() {
        return endingSession;
    }

    public void setEndingSession(Date endingSession) {
        this.endingSession = endingSession;
    }

    public Date getRegistrationDeadlineDate() {
        return registrationDeadlineDate;
    }

    public void setRegistrationDeadlineDate(Date registrationDeadlineDate) {
        this.registrationDeadlineDate = registrationDeadlineDate;
    }

    public Boolean getActiveSession() {
        return activeSession;
    }

    public void setActiveSession(Boolean activeSession) {
        this.activeSession = activeSession;
    }

    public Boolean getOpenSession() {
        return openSession;
    }

    public void setOpenSession(Boolean openSession) {
        this.openSession = openSession;
    }

    public Boolean getHiddenSession() {
        return hiddenSession;
    }

    public void setHiddenSession(Boolean hiddenSession) {
        this.hiddenSession = hiddenSession;
    }
}
