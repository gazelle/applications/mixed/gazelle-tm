package net.ihe.gazelle.tm.statistics.evaluation;

import net.ihe.gazelle.tf.model.Domain;
import net.ihe.gazelle.tm.gazelletest.model.instance.SystemAIPOResultForATestingSession;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstance;
import net.ihe.gazelle.tm.statistics.common.StatisticsCommonService;
import net.ihe.gazelle.tm.systems.model.SystemInSession;

import java.util.List;

public interface StatisticsForEvaluationPhaseService {
    List<SystemInSession> getSystemsForCurrentUser();

    List<TestInstance> getAllTestInstancesForCurrentSession();

    StatisticsCommonService.TestInstanceStats getStatisticsForTestInstances(List<TestInstance> testInstances);

    List<SystemAIPOResultForATestingSession> getCapabilitiesForASystem(SystemInSession system);

    CapabilityStats getCapabilitiesStatisticsForASystemInSession(SystemInSession systemInSession);

    CapabilityStats getCapabilitiesStatisticsForADomain(Domain domain);

    int getNbTotalTestInstancesForCurrentSession();

    int getNbTotalCapabilitiesForADomain(Domain domain);

    class CapabilityStats {
        private final int nbOfPassed;
        private final int nbOfDidNotComplete;
        private final int nbOfAtRisk;
        private final int nbOfNotEvaluated;
        private final int nbOfEvaluationPending;
        private final int nbOfNoPeer;

        public CapabilityStats(int nbOfPassed, int nbOfDidNotComplete, int nbOfAtRisk, int nbOfNotEvaluated, int nbOfEvaluationPending, int nbOfNoPeer) {
            this.nbOfPassed = nbOfPassed;
            this.nbOfDidNotComplete = nbOfDidNotComplete;
            this.nbOfAtRisk = nbOfAtRisk;
            this.nbOfNotEvaluated = nbOfNotEvaluated;
            this.nbOfEvaluationPending = nbOfEvaluationPending;
            this.nbOfNoPeer = nbOfNoPeer;
        }

        public int getNbOfPassed() {
            return nbOfPassed;
        }

        public int getNbOfDidNotComplete() {
            return nbOfDidNotComplete;
        }

        public int getNbOfAtRisk() {
            return nbOfAtRisk;
        }

        public int getNbOfNotEvaluated() {
            return nbOfNotEvaluated;
        }

        public int getNbOfEvaluationPending() {
            return nbOfEvaluationPending;
        }

        public int getNbOfNoPeer() {
            return nbOfNoPeer;
        }

        public boolean isEmpty() {
            return nbOfPassed == 0
                    && nbOfDidNotComplete == 0
                    && nbOfAtRisk == 0
                    && nbOfNotEvaluated == 0
                    && nbOfEvaluationPending == 0
                    && nbOfNoPeer == 0;
        }
    }
}
