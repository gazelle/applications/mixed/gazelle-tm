package net.ihe.gazelle.tm.messages;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManagerImpl;
import net.ihe.gazelle.messaging.MessagePropertyChanged;
import net.ihe.gazelle.messaging.MessagingProvider;
import net.ihe.gazelle.services.GenericServiceLoader;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import net.ihe.gazelle.ssov7.authn.interlay.adapter.GazelleIdentityImpl;
import net.ihe.gazelle.ssov7.gum.client.application.User;
import net.ihe.gazelle.tm.messages.model.Message;
import net.ihe.gazelle.tm.messages.model.MessageParameter;
import net.ihe.gazelle.tm.messages.model.MessageUser;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.kohsuke.MetaInfServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.*;

/**
 * NotificationService process, dispatch and format notification
 */
@Name("notificationService")
@Scope(ScopeType.EVENT)
@MetaInfServices(MessagingProvider.class)
public class NotificationService implements Serializable, MessagingProvider {

    private static final Logger LOG = LoggerFactory.getLogger(MessageManager.class);
    private static final ApplicationPreferenceManager applicationPreferenceManager = new ApplicationPreferenceManagerImpl();

    /**
     * Dispatch the notification to all notification listeners
     *
     * @param messageSource
     * @param sourceObject
     * @param messageParameters
     * @param <T>
     */
    public static <T> void dispatchNotification(MessageSource<T> messageSource, T sourceObject, String... messageParameters) {
        GazelleIdentity identity = GazelleIdentityImpl.instance();
        dispatchNotification(messageSource, identity.getUsername(), sourceObject, messageParameters);
    }

    public static <T> void dispatchNotification(MessageSource<T> messageSource, String userId, T sourceObject, String... messageParameters) {
        List<User> concernedUsers = NotificationService.getConcernedUsers(messageSource, sourceObject, userId, messageParameters);
        Message oMessage = NotificationService.createMessage(messageSource, sourceObject, userId, concernedUsers, messageParameters);

        List<NotificationListener> notificationListeners = GenericServiceLoader.getServices(NotificationListener.class);
        for (NotificationListener notificationListener : notificationListeners) {
            notificationListener.sendNotification(oMessage);
        }
    }

    /**
     * Create a Message processable by notification listeners
     *
     * @param messageSource     nature of the notification (eg. Test instance status change)
     * @param sourceObject      source of the notification (eg. Test Instance)
     * @param username      author of the message (user that triggered the notification)
     * @param concernedUsers    list of user to send the notification to
     * @param messageParameters additional information about the message
     * @param <T>
     * @return
     */
    protected static <T> Message createMessage(MessageSource<T> messageSource, T sourceObject, String username, List<User> concernedUsers, String... messageParameters) {
        Message oMessage = new Message();
        oMessage.setDate(new Date());
        oMessage.setLink(messageSource.getLink(sourceObject, messageParameters));
        oMessage.setType(messageSource.getType(sourceObject, messageParameters));
        oMessage.setImage(messageSource.getImage(sourceObject, messageParameters));
        oMessage.setTestingSession(messageSource.getTestingSession(sourceObject, messageParameters));

        if (username != null) {
            oMessage.setAuthor(username);
        } else {
            oMessage.setAuthor("Gazelle");
        }

        List<MessageParameter> oMessageParameters = new ArrayList<>();
        for (String messageParameter : messageParameters) {
            MessageParameter oMessageParameter = new MessageParameter();
            oMessageParameter.setMessageParameter(messageParameter);
            oMessageParameters.add(oMessageParameter);
        }
        oMessage.setMessageParameters(oMessageParameters);
        Set<MessageUser> concernedUsersIdList = new HashSet<>();
        for (User user : concernedUsers) {
            concernedUsersIdList.add(new MessageUser(user.getId()));
        }
        oMessage.setUsers(concernedUsersIdList);

        return oMessage;
    }

    /**
     * Get the list of users to send the notification to
     *
     * @param messageSource
     * @param sourceObject
     * @param username      author of the notification, will not be in the list
     * @param messageParameters
     * @param <T>
     * @return
     */
    private static <T> List<User> getConcernedUsers(MessageSource<T> messageSource, T sourceObject, String username, String... messageParameters) {
        return messageSource.getUsers(username, sourceObject, messageParameters);
    }

    @Override
    public void receiveMessage(Object message) {
        LOG.trace("receiveMessage");
        if (message instanceof MessagePropertyChanged) {
            List<MessageSourceFromPropertyChange> providers = GenericServiceLoader
                    .getServices(MessageSourceFromPropertyChange.class);
            MessagePropertyChanged messagePropertyChanged = (MessagePropertyChanged) message;
            for (MessageSourceFromPropertyChange messageSource : providers) {
                if (messageSource.getPropertyObjectClass()
                        .isAssignableFrom(messagePropertyChanged.getObject().getClass())) {
                    if (messageSource.getPropertyName().equals(messagePropertyChanged.getPropertyName())) {
                        messageSource.receivePropertyMessage(messagePropertyChanged);
                    }
                }
            }
        }
    }
}
