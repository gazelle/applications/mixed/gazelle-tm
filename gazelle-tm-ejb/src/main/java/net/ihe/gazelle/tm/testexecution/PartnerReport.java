package net.ihe.gazelle.tm.testexecution;



import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class PartnerReport {
    private String roleKeyword;
    private int countAvailable;
    private int countTested;
    private List<PartnerSystemReport> partnerSystemReports= new ArrayList<>();

    public String getRoleKeyword() {
        return roleKeyword;
    }

    public void setRoleKeyword(String roleKeyword) {
        this.roleKeyword = roleKeyword;
    }

    public int getCountAvailable() {
        return countAvailable;
    }

    public void setCountAvailable(int countAvailable) {
        this.countAvailable = countAvailable;
    }

    public int getCountTested() {
        return countTested;
    }

    public void setCountTested(int countTested) {
        this.countTested = countTested;
    }

    public List<PartnerSystemReport> getPartnerSystemReports() {
        return partnerSystemReports;
    }

    public void setPartnerSystemReports(List<PartnerSystemReport> partnerSystemReports) {
        this.partnerSystemReports = partnerSystemReports;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PartnerReport that = (PartnerReport) o;
        return countAvailable == that.countAvailable && countTested == that.countTested && Objects.equals(roleKeyword, that.roleKeyword) && Objects.equals(partnerSystemReports, that.partnerSystemReports);
    }

    @Override
    public int hashCode() {
        return Objects.hash(roleKeyword, countAvailable, countTested, partnerSystemReports);
    }
}
