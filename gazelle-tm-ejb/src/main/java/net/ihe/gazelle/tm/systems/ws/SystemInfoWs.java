package net.ihe.gazelle.tm.systems.ws;

import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import net.ihe.gazelle.ssov7.gum.client.application.User;
import net.ihe.gazelle.tf.ws.data.InstitutionWrapper;
import net.ihe.gazelle.tm.systems.model.InstitutionSystem;
import net.ihe.gazelle.tm.systems.model.SystemInSession;
import net.ihe.gazelle.tm.systems.model.SystemInSessionQuery;
import net.ihe.gazelle.tm.systems.model.SystemInSessionRegistrationStatus;

import net.ihe.gazelle.tm.ws.data.SystemInSessionWrapper;
import net.ihe.gazelle.tm.ws.data.SystemsInSessionWrapper;
import net.ihe.gazelle.tm.ws.data.TestingSessionWrapper;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import java.util.ArrayList;
import java.util.List;

@Stateless
@Name("systemInfoWs")
public class SystemInfoWs implements SystemInfoWsApi {

    private static final Logger LOG = LoggerFactory.getLogger(SystemInfoWs.class);

    @In(value = "gumUserService")
    private UserService userService;

    @Override
    public SystemsInSessionWrapper getSystemsForTestingSessionsForUser(String userId, String apiKey) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getSystemsForTestingSessionsForUser");
        }
        SystemsInSessionWrapper wrapper = new SystemsInSessionWrapper();
        User user;
        if (userId.isEmpty()) {
            return wrapper;
        } else {
            user = userService.getUserById(userId);
            if (user == null) {
                LOG.error(userId + " is not recorded");
                return wrapper;
            }
        }
        SystemInSessionQuery query = new SystemInSessionQuery();
        query.registrationStatus().eq(SystemInSessionRegistrationStatus.COMPLETED);
        query.testingSession().sessionClosed().eq(false);
        query.testingSession().activeSession().eq(true);
        query.system().institutionSystems().institution().keyword().eq(user.getOrganizationId());
        query.system().systemActorProfiles().actorIntegrationProfileOption().actorIntegrationProfile().integrationProfile().keyword().eq("ATNA");
        List<SystemInSession> systems = query.getList();

        for (SystemInSession sis : systems) {

            List<InstitutionWrapper> institutions = new ArrayList<InstitutionWrapper>();
            for (InstitutionSystem sysInstitution : sis.getSystem().getInstitutionSystems()) {
                institutions.add(new InstitutionWrapper(sysInstitution.getInstitution().getName(), sysInstitution.getInstitution().getKeyword()));
            }
            SystemInSessionWrapper single = new SystemInSessionWrapper(new TestingSessionWrapper(sis.getTestingSession().getId(), sis.getTestingSession().getName()), institutions, sis.getSystem().getKeyword());
            wrapper.addSystemInSessionWrapper(single);
        }
        return wrapper;
    }

    @Override
    public String getInstitutionKeywordForUser(String userId, String apiKey) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getInstitutionKeywordForUser");
        }
        User user = userService.getUserById(userId);
        return user.getOrganizationId();
    }
}
