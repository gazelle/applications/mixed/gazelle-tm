package net.ihe.gazelle.tm.gazelletest.testingSessionScope;

import net.ihe.gazelle.tm.gazelletest.testingSessionScope.comparators.DomainComparator;
import net.ihe.gazelle.tm.gazelletest.testingSessionScope.comparators.DroppedFirstComparator;
import net.ihe.gazelle.tm.gazelletest.testingSessionScope.comparators.IntegrationProfileComparator;
import net.ihe.gazelle.tm.gazelletest.testingSessionScope.comparators.TestableFirstComparator;

import java.util.Comparator;

public enum ProfileScopeSortMethod {
    PROFILE("gazelle.testmanagement.object.Profile", new IntegrationProfileComparator()),
    DOMAIN("gazelle.tf.Domain",new DomainComparator()),
    TESTABLE_FIRST("net.ihe.gazelle.tm.TestableFirst",new TestableFirstComparator()),
    DROPPED_FIRST("net.ihe.gazelle.tm.DroppedFirst", new DroppedFirstComparator());

    private String labelToDisplay;
    private Comparator<ProfileScope> comparator;

    ProfileScopeSortMethod(String labelToDisplay, Comparator<ProfileScope> comparator) {
        this.labelToDisplay = labelToDisplay;
        this.comparator = comparator;
    }

    public String getLabelToDisplay() {
        return labelToDisplay;
    }

    public Comparator<ProfileScope> getComparator() {
        return comparator;
    }
}
