package net.ihe.gazelle.tm.application.gui.summary;


import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import java.io.Serializable;

@Name("continuousSessionSummaryBeanGui")
@Scope(ScopeType.PAGE)
public class ContinuousSessionSummaryBeanGui implements Serializable {

    private static final long serialVersionUID = -6454103195947230901L;

    /////////////////// Links ///////////////////

    public String listPreparatoryTestsButton(){
        return "/testing/test/cat.seam";
    }

    public String sampleExchangeButton(){
        return "/objects/system_object.seam";
    }


}
