package net.ihe.gazelle.tm.session;

import net.ihe.gazelle.tm.systems.model.TestingSession;


public interface TestingSessionServiceDAO {


    /**
     * Gets default testing session.
     *
     * @return the default {@link TestingSession}
     */
    TestingSession getDefaultTestingSession();

    /**
     * Check if testing session exist.
     *
     * @param testingSessionId the {@link TestingSession} id
     * @return true if it exists, otherwise false
     */
    boolean isTestingSessionExisting(Integer testingSessionId);

    /**
     * Gets testing session by id.
     *
     * @param testingSessionId the testing session id
     * @return the {@link TestingSession}
     */
    TestingSession getTestingSessionById(Integer testingSessionId);
}
