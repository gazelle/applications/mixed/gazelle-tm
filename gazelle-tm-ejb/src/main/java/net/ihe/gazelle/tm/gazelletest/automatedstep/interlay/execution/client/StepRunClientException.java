package net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.execution.client;

public class StepRunClientException extends Exception {

    public StepRunClientException() {
    }

    public StepRunClientException(String message) {
        super(message);
    }

    public StepRunClientException(String message, Throwable throwable) {
        super(message, throwable);
    }

    public StepRunClientException(Throwable throwable) {
        super(throwable);
    }
}
