package net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.script.dto;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonSetter;
import net.ihe.gazelle.tm.gazelletest.automatedstep.domain.script.InputDefinition;

import java.io.Serializable;

@JsonPropertyOrder({"key", "label", "format", "type", "required"})
public class InputDefinitionDTO implements Serializable {

    private static final long serialVersionUID = 3175980723869766083L;

    @JsonIgnore
    private InputDefinition input;

    public InputDefinitionDTO() {
        this.input = new InputDefinition();
    }

    public InputDefinitionDTO(InputDefinition input) {
        this.input = input;
    }

    @JsonGetter("key")
    public String getKey() {
        return input.getKey();
    }

    @JsonSetter("key")
    public void setKey(String key) {
        input.setKey(key);
    }

    @JsonGetter("label")
    public String getLabel() {
        return input.getLabel();
    }

    @JsonSetter("label")
    public void setLabel(String label) {
        input.setLabel(label);
    }

    @JsonGetter("format")
    public String getFormat() {
        return input.getFormat();
    }

    @JsonSetter("format")
    public void setFormat(String format) {
        input.setFormat(format);
    }

    @JsonGetter("type")
    public String getType() {
        return input.getType();
    }

    @JsonSetter("type")
    public void setType(String type) {
        input.setType(type);
    }

    @JsonGetter("required")
    public boolean getRequired() {
        return input.getRequired();
    }

    @JsonSetter("required")
    public void setRequired(boolean required) {
        input.setRequired(required);
    }

    @JsonIgnore
    public InputDefinition getInput() {
        return input;
    }

    @JsonIgnore
    public void setInput(InputDefinition inputDefinition) {
        this.input = inputDefinition;
    }
}
