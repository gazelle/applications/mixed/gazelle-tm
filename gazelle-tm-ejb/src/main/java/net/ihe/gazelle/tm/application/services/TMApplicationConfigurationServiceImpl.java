package net.ihe.gazelle.tm.application.services;

import net.ihe.gazelle.preferences.PreferenceService;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@Scope(ScopeType.EVENT)
@AutoCreate
@Name("tmApplicationConfigurationService")
public class TMApplicationConfigurationServiceImpl implements TMApplicationConfigurationService {

    @Override
    public boolean isDatahouseEnabled() {
        return PreferenceService.getBoolean("datahouse_enabled");
    }

    @Override
    public boolean isAutomatedStepEnabled() {
        return PreferenceService.getBoolean("automated_step_enabled");
    }
}
