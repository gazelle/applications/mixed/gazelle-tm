package net.ihe.gazelle.tm.gazelletest.action;

import net.ihe.gazelle.tf.model.*;
import net.ihe.gazelle.tm.gazelletest.model.definition.RoleInTest;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstance;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstanceParticipants;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestStepsInstance;
import net.ihe.gazelle.tm.systems.model.SystemInSession;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@AutoCreate
@Name("datahouseURIServiceDAO")
@Scope(ScopeType.EVENT)
public class DatahouseURIServiceDAOImpl implements DatahouseURIServiceDAO {


    @Override
    public Standard getStandard(TestStepsInstance testStepsInstance) {
        StandardQuery query = new StandardQuery();
        Transaction transaction = testStepsInstance.getTestSteps().getTransaction();
        query.transactions().id().eq(transaction.getId());
        query.networkCommunicationType().isNotNull();
        query.networkCommunicationType().neq(NetworkCommunicationType.NONE);
        return query.getUniqueResult();
    }

    @Override
    public Actor getActor(TestStepsInstance testStepsInstance, RoleInTest roleInTest, SystemInSession systemInSession) {
        TestInstanceParticipants testInstanceParticipants = TestInstanceParticipants.getTestInstanceParticipantsByTestInstanceBySiSByRoleInTest(
                TestInstance.getTestInstanceByTestStepsInstance(testStepsInstance),
                systemInSession,
                roleInTest);
        if (testInstanceParticipants != null)
            return testInstanceParticipants.getActorIntegrationProfileOption().getActorIntegrationProfile().getActor();
        else {
            return null;
        }
    }
}
