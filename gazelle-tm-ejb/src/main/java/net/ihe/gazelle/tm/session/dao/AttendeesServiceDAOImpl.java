package net.ihe.gazelle.tm.session.dao;

import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import net.ihe.gazelle.ssov7.gum.client.interlay.client.utils.UserSearchParams;
import net.ihe.gazelle.tm.session.AttendeeServiceDAO;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import net.ihe.gazelle.tm.users.model.ConnectathonParticipant;
import net.ihe.gazelle.users.model.Institution;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.NoSuchElementException;


@Name("attendeesServiceDAO")
@Scope(ScopeType.STATELESS)
public class AttendeesServiceDAOImpl implements AttendeeServiceDAO {

    @In
    private EntityManager entityManager;

    @In("gumUserService")
    private UserService userService;

    @Override
    public List<ConnectathonParticipant> getRegisteredAttendees(TestingSession session, Institution institution) {
        return ConnectathonParticipant.filterConnectathonParticipant(null, institution, session);
    }

    @Override
    public List<ConnectathonParticipant> getRegisteredAttendees(Integer sessionId, Integer organizationId) {
        TestingSession testingSession = TestingSession.getSessionById(sessionId);
        if (testingSession == null) {
            throw new NoSuchElementException("Element Testing Session with id: "+sessionId+" not found");
        }
        Institution institution = Institution.findInstitutionWithId(organizationId);
        if (institution == null) {
            throw new NoSuchElementException("Element Institution with id: "+organizationId+" not found");
        }
        return getRegisteredAttendees(testingSession, institution);
    }

    @Override
    public long getNumberOfRegisteredAttendees(TestingSession session, Institution institution) {
        String queryString = "select count(p) from ConnectathonParticipant p where p.testingSession.id = :sessionId" +
                " and p.institution.id = :institutionId";
        TypedQuery<Long> query = entityManager.createQuery(queryString, Long.class);
        query.setParameter("sessionId",session.getId());
        query.setParameter("institutionId", institution.getId());
        return query.getSingleResult();
    }

    @Override
    public long getNumberOfActiveMembers(Institution institution) {
        UserSearchParams userSearchParams = new UserSearchParams()
                .setOrganizationId(institution.getKeyword())
                .setActivated(true)
                .setOffset(1);

        return userService.searchAndFilter(userSearchParams).getCount();
    }

    @Override
    public long getNumberOfPendingMembers(Institution institution) {
        // FIXME This is wrong, it will return all deactivated users of the organization.
        //  Pending users are users that are deactivated AND have an activation code not null.
        //  The API does not support that query yet.
        UserSearchParams userSearchParams = new UserSearchParams()
            .setOrganizationId(institution.getKeyword())
            .setActivated(false)
            .setOffset(1);
        return userService.searchAndFilter(userSearchParams).getCount();
    }
}
