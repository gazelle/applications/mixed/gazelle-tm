package net.ihe.gazelle.tm.statistics.preparation;

import net.ihe.gazelle.tm.statistics.common.StatisticsCommonService;

public interface StatisticsForPreparationPhaseService {

    int getNumberOfSessionParticipants();

    int getNumberOfRegisteredInstitutionWithAttendee();

    int getNumberOfRegisteredInstitutionWithSystem();

    int getNumberOfRegisteredSystemInSession();

    int getNumberOfActivatedMonitorInSession();

    int getNumberOfAcceptedSystemInSession();

    int getNumberOfNotAcceptedSystemInSession();

    int getNumberOfAcceptedSupportiveRequest();

    int getNumberOfNotYetAcceptedSupportiveRequest();

    int getNumberOfSUTNetworkConfiguration();

    int getNumberOfSUTNetworkConfigurationAccepted();

    int getNumberOfSUTNetworkConfigurationNotAccepted();

    int getNumberOfGeneratedHosts();

    int getNumberOfGeneratedHostsWithIP();

    int getNumberOfGeneratedHostsWithoutIP();

    int getNumberOfPreparatoryTests();

    StatisticsCommonService.TestInstanceStats getPreparatoryTestStatistics();

}
