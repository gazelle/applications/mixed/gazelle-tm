package net.ihe.gazelle.tm.comtool.interlay;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManagerImpl;
import net.ihe.gazelle.communicationgateway.client.ProvisioningClient;
import net.ihe.gazelle.communicationgateway.client.TestRunClient;
import net.ihe.gazelle.communicationgateway.dto.OrganizationDTO;
import net.ihe.gazelle.communicationgateway.dto.OrganizationRoleDTO;
import net.ihe.gazelle.communicationgateway.dto.OrganizationUserDTO;
import net.ihe.gazelle.communicationgateway.dto.ProfileDTO;
import net.ihe.gazelle.communicationgateway.dto.SystemDTO;
import net.ihe.gazelle.communicationgateway.dto.TestDTO;
import net.ihe.gazelle.communicationgateway.dto.TestRunDTO;
import net.ihe.gazelle.communicationgateway.dto.TestRunRoleDTO;
import net.ihe.gazelle.communicationgateway.dto.TestingSessionDTO;
import net.ihe.gazelle.communicationgateway.dto.TestingSessionRoleDTO;
import net.ihe.gazelle.communicationgateway.dto.TestingSessionUserDTO;
import net.ihe.gazelle.communicationgateway.dto.UserDTO;
import net.ihe.gazelle.communicationgateway.dto.payload.ArchiveTestRunChannel;
import net.ihe.gazelle.communicationgateway.dto.payload.CreateTestRunChannel;
import net.ihe.gazelle.communicationgateway.dto.payload.JoinTestRunChannel;
import net.ihe.gazelle.communicationgateway.dto.payload.LeaveTestRunChannel;
import net.ihe.gazelle.communicationgateway.dto.payload.TestRunInvitationLink;
import net.ihe.gazelle.communicationgateway.dto.payload.TestRunMessage;
import net.ihe.gazelle.communicationgateway.dto.payload.UnarchiveTestRunChannel;
import net.ihe.gazelle.jaxrs.json.client.JaxRsJsonClientFactory;
import net.ihe.gazelle.ssov7.gum.client.application.Role;
import net.ihe.gazelle.ssov7.gum.client.application.User;
import net.ihe.gazelle.tf.model.IntegrationProfile;
import net.ihe.gazelle.tm.comtool.application.ComToolRestClient;
import net.ihe.gazelle.tm.gazelletest.model.definition.Test;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstance;
import net.ihe.gazelle.tm.systems.model.InstitutionSystem;
import net.ihe.gazelle.tm.systems.model.System;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import net.ihe.gazelle.users.model.Institution;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

@Name("comToolRestClient")
@Scope(ScopeType.APPLICATION)
@AutoCreate
public class ComToolRestClientImpl implements ComToolRestClient {
   private static final Logger LOG = LoggerFactory.getLogger(ComToolRestClientImpl.class);

   @In(value = "jaxRsJsonClientFactory", create = true, required = true)
   JaxRsJsonClientFactory jaxRsClientFactory;

   private final ApplicationPreferenceManager applicationPreferenceManager = new ApplicationPreferenceManagerImpl();

   protected ProvisioningClient getProvisioningClient() {
      LOG.debug("getProvisioningClient");
      return jaxRsClientFactory.getClient(ProvisioningClient.class, getGctUrl());
   }

   protected TestRunClient getTestRunClient() {
      LOG.debug("getTestRunClient");
      return jaxRsClientFactory.getClient(TestRunClient.class, getGctUrl());
   }

   @Override
   public String getGctUrl() {
      LOG.debug("getGctUrl");
      return applicationPreferenceManager.getStringValue("gct_url");
   }

   @Override
   public void updateTestingSessionUsersAndChannels(TestingSessionUpdate testingSessionUpdate) {
      LOG.debug("updateTestingSessionsUsersAndChannels");
      updateTestingSessionsUsersAndChannels(Collections.singletonList(testingSessionUpdate));
   }

   @Override
   public void updateTestingSessionsUsersAndChannels(List<TestingSessionUpdate> testingSessionUpdateList) {
      LOG.debug("updateTestingSessionsUsersAndChannels");
      List<TestingSessionDTO> testingSessionsDTO = new ArrayList<>();

      for (TestingSessionUpdate testingSessionUpdate : testingSessionUpdateList) {
         TestingSessionDTO testingSessionDTO = asTestingSessionDTO(testingSessionUpdate.getTestingSession());
         // set users, institutions and profiles for user/channel creation/update
         testingSessionDTO.setUsers(asUsersDTO(
               testingSessionUpdate.getUsers(),
               testingSessionUpdate.getMonitors(),
               testingSessionUpdate.getAdmins()
         ));
         testingSessionDTO.setOrganizations(asOrganisationsDTO(testingSessionUpdate.getInstitutions()));
         testingSessionDTO.setProfiles(asProfilesDTO(testingSessionUpdate.getIntegrationProfiles()));
         testingSessionsDTO.add(testingSessionDTO);
      }
      getProvisioningClient().updateTestingSessions(testingSessionsDTO);
   }

   @Override
   public void createTestRunChannel(TestInstance testInstance, List<System> systems, User creator) {
      LOG.debug("createTestRunChannel");
      TestingSessionDTO testingSessionDTO = asTestingSessionDTO(testInstance.getTestingSession());
      TestRunDTO testRunDTO = asTestRunDTO(testInstance);
      // add systems in test run, for channel notifications
      testRunDTO.setSystems(asSystemsDTO(systems));
      UserDTO userDTO = asUserDTO(creator);
      CreateTestRunChannel createChannel = new CreateTestRunChannel(
            testingSessionDTO,
            testRunDTO,
            userDTO
      );
      getTestRunClient().createTestRunChannel(createChannel);
   }

   @Override
   public String getInvitationLink(TestInstance testInstance) {
      LOG.debug("getInvitationLink");
      TestRunInvitationLink testRunInvitationLink = new TestRunInvitationLink(
            asTestingSessionDTO(testInstance.getTestingSession()),
            asTestRunDTO(testInstance)
      );
      return getTestRunClient().getInvitationLink(testRunInvitationLink);
   }

   @Override
   public void archiveTestRunChannel(TestInstance testInstance, String testStatus,
                                     String lastTestStatus) {
      LOG.debug("archiveTestRunChannel");
      ArchiveTestRunChannel archiveChannel = new ArchiveTestRunChannel(
            asTestingSessionDTO(testInstance.getTestingSession()),
            asTestRunDTO(testInstance),
            testStatus,
            lastTestStatus);
      getTestRunClient().archiveTestRunChannel(archiveChannel);
   }

   @Override
   public void unarchiveTestRunChannel(TestInstance testInstance, String testStatus,
                                       String lastTestStatus) {
      LOG.debug("unarchiveTestRunChannel");
      UnarchiveTestRunChannel unarchiveChannel = new UnarchiveTestRunChannel(
            asTestingSessionDTO(testInstance.getTestingSession()),
            asTestRunDTO(testInstance),
            testStatus,
            lastTestStatus);
      getTestRunClient().unarchiveTestRun(unarchiveChannel);
   }

   @Override
   public void sendTestRunMessage(TestInstance testInstance, String testMsg) {
      LOG.debug("sendTestRunMessage");
      TestRunMessage testRunMessage = new TestRunMessage(
            asTestingSessionDTO(testInstance.getTestingSession()),
            asTestRunDTO(testInstance),
            testMsg);
      getTestRunClient().sendTestRunMessage(testRunMessage);
   }

   @Override
   public void addMonitorToTestRunChannel(TestInstance testInstance, User user) {
      getTestRunClient().joinTestRunChannel(new JoinTestRunChannel(
            asTestingSessionDTO(testInstance.getTestingSession()),
            asTestRunDTO(testInstance),
            asUserDTO(user),
            TestRunRoleDTO.ADMIN
      ));
   }

   @Override
   public void removeUserFromTestRunChannel(TestInstance testInstance, User user) {
      getTestRunClient().leaveTestRunChannel(new LeaveTestRunChannel(
            asTestingSessionDTO(testInstance.getTestingSession()),
            asTestRunDTO(testInstance),
            asUserDTO(user)
      ));
   }

   private Set<TestingSessionUserDTO> asUsersDTO(List<User> users, List<User> monitors, List<User> admins) {
      LOG.debug("asUsersDTO");
      Map<String, TestingSessionUserDTO> allUsers = new TreeMap<>();
      for (User user : users) {
         TestingSessionUserDTO testingSessionUserDTO = asTestingSessionUserDTO(user);
         allUsers.put(user.getId(), testingSessionUserDTO);
      }
      addUsersWithRole(monitors, allUsers, TestingSessionRoleDTO.MONITOR);
      addUsersWithRole(admins, allUsers, TestingSessionRoleDTO.TESTING_SESSION_ADMIN);
      return new HashSet<>(allUsers.values());
   }

   private void addUsersWithRole(List<User> users, Map<String, TestingSessionUserDTO> allUsers,
                                 TestingSessionRoleDTO role) {
      LOG.debug("addUsersWithRole");
      for (User user : users) {
         TestingSessionUserDTO testingSessionUserDTO = allUsers.get(user.getId());
         if (testingSessionUserDTO == null) {
            testingSessionUserDTO = asTestingSessionUserDTO(user);
            allUsers.put(user.getId(), testingSessionUserDTO);
         }
         testingSessionUserDTO.getRoles().add(role);
      }
   }

   private TestingSessionUserDTO asTestingSessionUserDTO(User user) {
      LOG.debug("asTestingSessionUserDTO");
      Set<TestingSessionRoleDTO> roles = new HashSet<>();
      return new TestingSessionUserDTO(asUserDTO(user), roles);
   }

   private UserDTO asUserDTO(User user) {
      LOG.debug("asUserDTO");
      return new UserDTO(
              null,
            user.getId(),
            user.getFirstName(),
            user.getLastName(),
            user.getEmail()
      );
   }

   private Set<ProfileDTO> asProfilesDTO(List<IntegrationProfile> integrationProfiles) {
      LOG.debug("asProfilesDTO");
      Set<ProfileDTO> profilesDTO = new HashSet<>();
      for (IntegrationProfile integrationProfile : integrationProfiles) {
         profilesDTO.add(asProfileDTO(integrationProfile));
      }
      return profilesDTO;
   }

   private ProfileDTO asProfileDTO(IntegrationProfile integrationProfile) {
      LOG.debug("asProfileDTO");
      return new ProfileDTO(
            String.valueOf(integrationProfile.getId()),
            integrationProfile.getKeyword(),
            integrationProfile.getName()
      );
   }

   private Set<OrganizationDTO> asOrganisationsDTO(List<InstitutionUsers> institutionsUsers) {
      LOG.debug("asOrganisationsDTO");
      Set<OrganizationDTO> organizationDTOs = new HashSet<>();
      for (InstitutionUsers institutionUsers : institutionsUsers) {
         OrganizationDTO organizationDTO = asOrganisationDTO(institutionUsers.getInstitution());
         Set<OrganizationUserDTO> users = new HashSet<>();
         for (User user : institutionUsers.getUsers()) {
            UserDTO userDTO = asUserDTO(user);
            Set<OrganizationRoleDTO> roles = new HashSet<>();
            OrganizationUserDTO organizationUserDTO = new OrganizationUserDTO(userDTO, roles);
            if (user.hasRole(Role.VENDOR_ADMIN)) {
               roles.add(OrganizationRoleDTO.VENDOR_ADMIN);
            }
            users.add(organizationUserDTO);
         }
         organizationDTO.setUsers(users);
         organizationDTOs.add(organizationDTO);
      }
      return organizationDTOs;
   }

   private OrganizationDTO asOrganisationDTO(Institution institution) {
      LOG.debug("asOrganisationDTO");
      return new OrganizationDTO(
            String.valueOf(institution.getId()),
            institution.getKeyword(),
            institution.getName(),
            null
      );
   }

   private TestingSessionDTO asTestingSessionDTO(TestingSession testingSession) {
      LOG.debug("asTestingSessionDTO");
      return new TestingSessionDTO(
            String.valueOf(testingSession.getId()),
            // FIXME may use a better keyword for testing session (zone, year, ...)
            testingSession.getDescription(),
            testingSession.getDescription(),
            null, null, null
      );
   }

   private TestRunDTO asTestRunDTO(TestInstance testInstance) {
      LOG.debug("asTestRunDTO");
      return new TestRunDTO(
            String.valueOf(testInstance.getId()),
            asTestDTO(testInstance.getTest()),
            null
      );
   }

   private TestDTO asTestDTO(Test test) {
      LOG.debug("asTestDTO");
      return new TestDTO(
            String.valueOf(test.getId()),
            test.getKeyword(),
            test.getName()
      );
   }

   private Set<SystemDTO> asSystemsDTO(List<System> systems) {
      LOG.debug("asSystemsDTO");
      Set<SystemDTO> systemsDTO = new HashSet<>();
      for (System system : systems) {
         systemsDTO.add(asSystemDTO(system));
      }
      return systemsDTO;
   }

   private SystemDTO asSystemDTO(System system) {
      LOG.debug("asSystemDTO");
      Set<OrganizationDTO> organizationsDTO = new HashSet<>();
      for (InstitutionSystem institutionSystem : system.getInstitutionSystems()) {
         organizationsDTO.add(asOrganisationDTO(institutionSystem.getInstitution()));
      }
      return new SystemDTO(
            String.valueOf(system.getId()),
            system.getKeyword(),
            system.getName(),
            organizationsDTO
      );
   }

}
