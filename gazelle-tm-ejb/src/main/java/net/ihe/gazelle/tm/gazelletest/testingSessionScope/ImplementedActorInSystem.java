package net.ihe.gazelle.tm.gazelletest.testingSessionScope;

import net.ihe.gazelle.tm.systems.model.SystemInSessionRegistrationStatus;

import java.util.List;
import java.util.Objects;

public class ImplementedActorInSystem {
    private String organizationName;
    private String systemKeyword;
    private List<String> profileOptions;
    private String testingDepth;
    private SystemInSessionRegistrationStatus registrationStatus;
    private boolean acceptedToSession;

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public String getSystemKeyword() {
        return systemKeyword;
    }

    public void setSystemKeyword(String systemKeyword) {
        this.systemKeyword = systemKeyword;
    }

    public List<String> getProfileOptions() {
        return profileOptions;
    }

    public void setProfileOptions(List<String> profileOptions) {
        this.profileOptions = profileOptions;
    }

    public String getTestingDepth() {
        return testingDepth;
    }

    public void setTestingDepth(String testingDepth) {
        this.testingDepth = testingDepth;
    }

    public SystemInSessionRegistrationStatus getRegistrationStatus() {
        return registrationStatus;
    }

    public void setRegistrationStatus(SystemInSessionRegistrationStatus registrationStatus) {
        this.registrationStatus = registrationStatus;
    }

    public boolean isAcceptedToSession() {
        return acceptedToSession;
    }

    public void setAcceptedToSession(boolean acceptedToSession) {
        this.acceptedToSession = acceptedToSession;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ImplementedActorInSystem that = (ImplementedActorInSystem) o;
        return acceptedToSession == that.acceptedToSession && Objects.equals(organizationName, that.organizationName) && Objects.equals(systemKeyword, that.systemKeyword) && Objects.equals(profileOptions, that.profileOptions) && Objects.equals(testingDepth, that.testingDepth) && registrationStatus == that.registrationStatus;
    }

    @Override
    public int hashCode() {
        return Objects.hash(organizationName, systemKeyword, profileOptions, testingDepth, registrationStatus, acceptedToSession);
    }
}
