package net.ihe.gazelle.tm.registration.gui;


import net.ihe.gazelle.menu.Pages;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import net.ihe.gazelle.ssov7.gum.client.application.Role;
import net.ihe.gazelle.tm.financial.FinancialSummary;
import net.ihe.gazelle.tm.financial.action.services.FeesService;
import net.ihe.gazelle.tm.financial.model.Invoice;
import net.ihe.gazelle.tm.organization.OrganizationService;
import net.ihe.gazelle.tm.session.AttendeeService;
import net.ihe.gazelle.tm.session.TestingSessionService;
import net.ihe.gazelle.tm.systems.SystemSummary;
import net.ihe.gazelle.tm.systems.SystemUnderTestService;
import net.ihe.gazelle.tm.systems.model.SystemInSession;
import net.ihe.gazelle.tm.systems.model.SystemInSessionRegistrationStatus;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import net.ihe.gazelle.tm.systems.model.TestingSessionAdmin;
import net.ihe.gazelle.tm.users.model.ConnectathonParticipant;
import net.ihe.gazelle.users.model.Institution;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.international.StatusMessages;
import org.jboss.seam.security.Identity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

@Name("registrationBeanGui")
@Scope(ScopeType.PAGE)
public class RegistrationBeanGui implements Serializable {

    private static final long serialVersionUID = 7264651567129327875L;

    @In(value = "testingSessionService", create = true)
    private TestingSessionService testingSessionService;

    @In(value = "organizationService", create = true)
    private OrganizationService organizationService;

    @In(value = "systemUnderTestService", create = true)
    private SystemUnderTestService systemUnderTestService;

    @In(value = "attendeeService", create = true)
    private AttendeeService attendeeService;

    @In(value = "feesService", create = true)
    private FeesService feesService;

    @In(value = "gumUserService")
    private UserService userService;

    @In
    private GazelleIdentity identity;

    private TestingSession selectedTestingSession;

    private Institution selectedInstitution;

    private FinancialSummary financialSummary;

    private Invoice invoice;

    private String currency = "USD";

    private boolean showPopupSwitch = true;

    private final SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

    private static final String PENDING = "Pending";

    private static final String UNDEFINED = "UNDEFINED";
    private Boolean registrationOpen;

    private SystemInSession selectedSystemInSession;

    @Create
    public void init() {
        this.selectedTestingSession = testingSessionService.getUserTestingSession();
        this.registrationOpen = testingSessionService.isRegistrationOpen(selectedTestingSession);
        this.selectedInstitution = organizationService.getCurrentOrganization();
        if (identity.isLoggedIn() && selectedTestingSession != null && selectedInstitution != null) {
            if (selectedTestingSession.isContractRequired()) {
                this.financialSummary = feesService.getFees(selectedTestingSession, selectedInstitution);
                if (financialSummary != null && financialSummary.getInvoice() != null) {
                    this.invoice = financialSummary.getInvoice();
                }
                if (selectedTestingSession.getCurrency() != null) {
                    this.currency = selectedTestingSession.getCurrency().getKeyword();
                }
            }
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "No selected session or organization");
        }


    }

    /////////////////// Getters & Setters ///////////////////

    public TestingSession getSelectedTestingSession() {
        return selectedTestingSession;
    }

    public void setSelectedTestingSession(TestingSession selectedTestingSession) {
        this.selectedTestingSession = selectedTestingSession;
    }

    public FinancialSummary getFinancialSummary() {
        return financialSummary;
    }

    public Boolean getShowPopupSwitch() {
        return showPopupSwitch;
    }

    public void setShowPopupSwitch(boolean state) {
        showPopupSwitch = state;
    }

    public Invoice getInvoice() {
        return invoice;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }

    public SystemInSession getSelectedSystemInSession() {
        return selectedSystemInSession;
    }

    public void setSelectedSystemInSession(SystemInSession systemInSession) {
        this.selectedSystemInSession = systemInSession;
    }

    public void deleteSystemInSession() {
        try {
            List<SystemInSession> listSiS = SystemInSession.findSystemInSessionsWithSystem(selectedSystemInSession.getSystem());
            if (listSiS.size() >= 1) {
                SystemInSession.deleteSelectedSystemInSessionWithDependencies(selectedSystemInSession);
                FacesMessages.instance().add(StatusMessage.Severity.INFO, "The selected system (" + selectedSystemInSession.getSystem().getKeyword() + ") was deleted from the testing session :" + selectedSystemInSession.getTestingSession().getDescription());
            }

            selectedSystemInSession = null;


        } catch (Exception ex) {
            StatusMessages.instance().addFromResourceBundle("gazelle.systems.error.SystemCanNotBeDeleted");
        }
    }


    /////////////////// Data Retrievers ///////////////////

    public String getRegistrationDeadline() {
        if (selectedTestingSession.getRegistrationDeadlineDate() == null) {
            return UNDEFINED;
        }
        return formatter.format(selectedTestingSession.getRegistrationDeadlineDate());
    }

    public List<String> getCurrentSessionAdmins() {
        if (selectedTestingSession.getTestingSessionAdmins() != null && !selectedTestingSession.getTestingSessionAdmins().isEmpty()) {
            List<TestingSessionAdmin> testingSessionAdmins =
                    selectedTestingSession.getTestingSessionAdmins();
            List<String> adminNames = new ArrayList<>();
            for (TestingSessionAdmin sessionAdmin : testingSessionAdmins) {
                adminNames.add(userService.getUserDisplayNameWithoutException(sessionAdmin.getUserId()));
            }
            return adminNames;
        } else {
            return testingSessionService.getTestingSessionManagers(selectedTestingSession);
        }
    }

    public List<SystemSummary> getSystemsUnderTest() {
        return systemUnderTestService.getRegisteredSystems(selectedTestingSession, selectedInstitution);
    }

    public String getSystemOwner(SystemInSession systemInSession) {
        String userId = systemInSession.getSystem().getOwnerUserId();
        return userService.getUserById(userId).getFirstName() + " " + userService.getUserById(userId).getLastName();
    }

    public String getOwnerMail(SystemInSession systemInSession) {
        String userId = systemInSession.getSystem().getOwnerUserId();
        return userService.getUserById(userId).getEmail();
    }

    public List<ConnectathonParticipant> getAttendees() {
        return attendeeService.getRegisteredAttendees(selectedTestingSession, selectedInstitution);

    }

    public String getSUTStatusLabel(SystemSummary systemSummary) {
        if (systemSummary.getRegistrationStatus().equals(SystemInSessionRegistrationStatus.IN_PROGRESS)) {
            return "label label-info";
        }
        if (systemSummary.getRegistrationStatus().equals(SystemInSessionRegistrationStatus.COMPLETED)) {
            return "label label-success";
        }
        return "label label-danger";
    }


    // Fees infos

    public String getParticipationPrice() {
        return selectedTestingSession.getFeeParticipant() + " " + currency;
    }


    public String getMainSUTFees() {
        return selectedTestingSession.getFeeFirstSystem() + " " + currency;
    }

    public String getAdditionalSUTFee() {
        return selectedTestingSession.getFeeAdditionalSystem() + " " + currency;
    }

    public String getAttendeesFees() {
        return selectedTestingSession.getFeeParticipant() + " " + currency;
    }

    public int getNumberOfAdditionalSUTs() {
        if (financialSummary == null || financialSummary.getFinancialSummaryOneSystems() == null) {
            return 0;
        }
        return getFinancialSummary().getFinancialSummaryOneSystems().size() - 1;
    }

    public int getNumberOfIncludedAttendees() {
        return selectedTestingSession.getNbParticipantsIncludedInSystemFees();
    }

    public int getTotalNumberOfIncludedAttendees() {
        if (financialSummary == null || financialSummary.getFinancialSummaryOneSystems() == null) {
            return 0;
        }
        return getNumberOfIncludedAttendees() * financialSummary.getFinancialSummaryOneSystems().size();
    }

    public int getNumberOfAdditionalAttendees() {
        if (financialSummary == null) {
            return 0;
        }
        return financialSummary.getNumberAdditionalParticipant(selectedTestingSession);
    }


    public BigDecimal getTotalAdditionalSUTs() {
        if (selectedTestingSession.getFeeAdditionalSystem() == null) {
            return new BigDecimal(0);
        }
        return selectedTestingSession.getFeeAdditionalSystem().multiply(BigDecimal.valueOf(getNumberOfAdditionalSUTs()));
    }

    public String getTotalAdditionalSUTsCurrency() {
        return getTotalAdditionalSUTs() + " " + currency;
    }

    public BigDecimal getTotalAdditionalAttendees() {
        if (selectedTestingSession.getFeeParticipant() == null) {
            return new BigDecimal(0);
        }
        return selectedTestingSession.getFeeParticipant().multiply(BigDecimal.valueOf(getNumberOfAdditionalAttendees()));
    }

    public String getTotalAdditionalAttendeesCurrency() {
        return getTotalAdditionalAttendees() + " " + currency;
    }

    public BigDecimal getTotalDiscount() {
        if (invoice == null || invoice.getFeesDiscount() == null) {
            return new BigDecimal(0);
        }
        return invoice.getFeesDiscount().negate();
    }

    public String getTotalDiscountCurrency() {
        return getTotalDiscount() + " " + currency;
    }

    public BigDecimal getVATAmount() {
        if (invoice == null || invoice.getVatAmount() == null) {
            return new BigDecimal(0);
        }
        return invoice.getVatAmount();
    }

    public String getVATPercentage() {
        if (selectedTestingSession.getVatPercent() == null) {
            return "0%";
        }
        return selectedTestingSession.getVatPercent().multiply(BigDecimal.valueOf(100)) + "%";
    }

    public String getVATAmountCurrency() {
        return getVATAmount() + " " + currency;
    }

    public String getTotal() {
        if (financialSummary == null) {
            return "An error has occurred";
        }
        return feesService.getTotalToPay(financialSummary) + " " + currency;
    }


    // Contracts infos

    public String getGeneratedOn() {
        if (invoice == null || invoice.getInvoiceGenerationDate() == null) {
            return PENDING;
        }
        return formatter.format(invoice.getInvoiceGenerationDate());
    }

    public String getReceivedOn() {
        if (invoice == null || invoice.getContractReceivedDate() == null) {
            return PENDING;
        }
        return formatter.format(invoice.getContractReceivedDate());
    }

    public String getPONumber() {
        if (invoice == null || invoice.getPurchaseOrder() == null || "".equals(invoice.getPurchaseOrder())) {
            return PENDING;
        }
        return invoice.getPurchaseOrder();
    }

    public String getSentOn() {
        if (invoice == null || invoice.getInvoiceSentDate() == null) {
            return PENDING;
        }
        return formatter.format(invoice.getInvoiceSentDate());
    }

    public String getInstructions() {
        if (invoice == null || invoice.getSpecialInstructions() == null || "".equals(invoice.getSpecialInstructions())) {
            return PENDING;
        }
        return invoice.getSpecialInstructions();
    }


    /////////////////// Checkers ///////////////////

    public boolean isVendorAdmin() {
        return Identity.instance().hasRole(Role.VENDOR_ADMIN);
    }

    public boolean renderRegistrationAlert() {
        return !selectedTestingSession.getContinuousSession() && selectedTestingSession.getRegistrationDeadlineDate() != null;
    }

    public boolean isVendorLateRegistration() {
        return Identity.instance().hasRole(Role.VENDOR_ADMIN);
    }

    public boolean renderAlertWarnForLateRegistration() {
        return !selectedTestingSession.getContinuousSession()
                && selectedTestingSession.getRegistrationDeadlineDate() != null
                && Identity.instance().hasRole(Role.VENDOR_LATE_REGISTRATION);
    }

    public boolean isRegistrationOpen() {
        return testingSessionService.isRegistrationOpen(selectedTestingSession);
    }

    public boolean isOrganizationRegistrationCompleted() {
        return organizationService.isOrganizationRegistrationCompleted(selectedInstitution);
    }


    public boolean isAttendeesRegistrationEnabled() {
        return testingSessionService.isAttendeesRegistrationEnabled(selectedTestingSession);
    }

    public boolean isThereSystemsWithMissingDependency() {
        List<SystemSummary> systemsSummary = systemUnderTestService.getRegisteredSystems(selectedTestingSession, selectedInstitution);
        boolean found = false;
        for (SystemSummary systemSummary : systemsSummary) {
            if (systemSummary.hasMissingDependencies()) {
                found = true;
            }
        }
        return found;
    }

    public boolean isContractRequired() {
        return selectedTestingSession.isContractRequired();
    }

    public boolean isAttendeesSectionDisplayed() {
        return isAttendeesRegistrationEnabled();
    }

    public boolean hasAttendeesRegistered() {
        return (getAttendees().size() > 0);
    }

    public boolean isManageAttendeeButtonDisplayed() {
        return (isAttendeesRegistrationEnabled() && identity.hasRole(Role.VENDOR_ADMIN) && isTestingSessionOpened());
    }

    public boolean checkContractRole() {
        return identity.hasRole(Role.VENDOR) || identity.hasRole(Role.VENDOR_ADMIN);
    }

    public boolean isContractReceived() {
        if (invoice == null || invoice.getContractReceived() == null) {
            return false;
        }
        return invoice.getContractReceived();
    }

    public boolean shouldShowPopup() {
        return selectedTestingSession != null && selectedTestingSession.getIsDefaultTestingSession() != null && !selectedTestingSession.getIsDefaultTestingSession() && showPopupSwitch;
    }

    public boolean systemHasAtLeastOneSUT() {
        return financialSummary.getFinancialSummaryOneSystems().size() > 0;
    }

    public boolean showManageContractButton() {
        return Identity.instance().isLoggedIn() && Identity.instance().hasRole(Role.VENDOR_ADMIN) && systemHasAtLeastOneSUT();
    }

    public String getManageAttendeeLink() {
        return Pages.REGISTRATION_PARTICIPANTS.getLink();
    }

    public boolean showAddImportOrNewSUTButton() {
        return registrationOpen || (Identity.instance().hasRole(Role.VENDOR_LATE_REGISTRATION) && isTestingSessionOpened());
    }

    public boolean canDeleteSUT(SystemSummary systemSummary) {
        return registrationOpen && Identity.instance().hasRole(Role.VENDOR_ADMIN) && !systemSummary.isAccepted();
    }

    public boolean isTestingSessionOpened() {return  !selectedTestingSession.getSessionClosed(); };

    /////////////////// Links ///////////////////

    public String manageSUTsButton() {
        return "/systems/system/listSystemsInSession.seam";
    }

    public String manageAttendeesButtons() {
        return "/users/connectathon/listParticipants.seam";
    }

    public String manageContractButton() {
        return "/financial/financialSummary.seam";
    }

    public String addNewSystemActionButton() {
        return "/systems/system/createSystemInSession.seam";
    }

    public String copySystemActionButton() {
        return "/systems/system/copySystemInSession.seam";
    }

    public String viewSystemInSession(SystemInSession inSystemSession) {
        return "/systems/system/showSystemInSession.seam?id=" + inSystemSession.getId();
    }

    public String editSystemInSessionSummaryActionRedirect(SystemInSession sis) {
        return "/systems/system/editSystemInSession.seam?id=" + sis.getId();
    }

}
