package net.ihe.gazelle.tf.action.converter;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.objects.model.*;
import net.ihe.gazelle.tf.model.ActorIntegrationProfileOption;
import net.ihe.gazelle.tf.model.ActorIntegrationProfileOptionQuery;
import net.ihe.gazelle.tm.gazelletest.model.reversed.AIPO;

import javax.persistence.EntityManager;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.List;

/**
 * Converts Sample Types to XML String and back.
 */
public class SamplesTypeToXmlConverter {

    /**
     * Transform a list of ObjectTypes to a XML String.
     *
     * @param objectTypes list of ObjectTypes.
     * @return an XML representation of input ObjectTypes.
     * @throws JAXBException if the marshalling cannot be performed
     */
    public String samplesTypeToXml(List<ObjectType> objectTypes) throws JAXBException {

        ObjectTypeIE objectTypeIE = this.listToIE(objectTypes);

        JAXBContext jaxbContext = JAXBContext.newInstance(ObjectTypeIE.class, ObjectType.class, ObjectTypeStatus.class, ObjectAttribute.class,
                ObjectCreator.class, ObjectReader.class, ObjectFile.class, ActorIntegrationProfileOption.class,
                ObjectFileType.class, AIPO.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        jaxbMarshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
        StringWriter sw = new StringWriter();
        jaxbMarshaller.marshal(objectTypeIE, sw);
        return sw.toString();
    }

    /**
     * Get the Import/Export structure from a list of ObjectTypes.
     *
     * @param objectTypes list of ObjectTypes.
     * @return the corresponding Import/Export structure.
     */
    private ObjectTypeIE listToIE(List<ObjectType> objectTypes) {
        return new ObjectTypeIE(objectTypes);
    }

    /**
     * Unmarshall the ObjectTypeIE structure from an XML String.
     * Also look for duplicates or missing TF elements.
     *
     * @param xmlString XML representation of the structure.
     * @return the ObjectTypeIE Structure.
     * @throws JAXBException if the unmarshalling cannot be performed.
     */
    public ObjectTypeIE extractAndCheckObjectTypes(String xmlString) throws JAXBException {
        EntityManager entityManager = EntityManagerService.provideEntityManager();

        ObjectTypeIE objectTypeIE = xmlToSamplesType(xmlString);
        objectTypeIE = searchForDuplicate(objectTypeIE, entityManager);
        return checkSamplesType(objectTypeIE, entityManager);
    }

    /**
     * Unmarshall the ObjectTypeIE structure from an XML String.
     *
     * @param xmlString XML representation of the structure.
     * @return the ObjectTypeIE Structure.
     * @throws JAXBException if the unmarshalling cannot be performed.
     */
    public ObjectTypeIE xmlToSamplesType(String xmlString) throws JAXBException {

        JAXBContext jaxbContext = JAXBContext.newInstance(ObjectTypeIE.class, ObjectType.class, ObjectTypeStatus.class, ObjectAttribute.class,
                ObjectCreator.class, ObjectReader.class, ObjectFile.class, ActorIntegrationProfileOption.class,
                ObjectFileType.class, AIPO.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

        Reader reader = new InputStreamReader(new ByteArrayInputStream
                (xmlString.getBytes(StandardCharsets.UTF_8)), StandardCharsets.UTF_8);
        return (ObjectTypeIE) unmarshaller.unmarshal(reader);
    }

    /**
     * Check for Unknown TF elements.
     *
     * @param objectTypeIE  list of ObjectTypes to check.
     * @param entityManager EntityManager used to search in DB.
     * @return the updated ObjectTypeIE structure.
     */
    private ObjectTypeIE checkSamplesType(ObjectTypeIE objectTypeIE, EntityManager entityManager) {

        List<ObjectType> objectTypes = objectTypeIE.getObjectTypes();
        boolean configOk;
        Iterator iterator = objectTypes.iterator();
        while (iterator.hasNext()) {
            ObjectType objectType = ((ObjectType) iterator.next());

            configOk = checkObjectTypeStatus(objectTypeIE, objectType)
                    && checkObjectFileTypes(objectTypeIE, objectType, entityManager)
                    && checkAIPOFromObjectCreator(objectTypeIE, objectType, entityManager)
                    && checkAIPOFromObjectReader(objectTypeIE, objectType, entityManager);

            if (!configOk) {
                iterator.remove();
                objectTypeIE.addIgnoredObjectType(objectType);
            }
        }
        objectTypeIE.setObjectTypes(objectTypes);
        return objectTypeIE;
    }

    /**
     * Check Object Type Status.
     *
     * @param objectTypeIE {@link ObjectTypeIE}
     * @param objectType   Object Type to check.
     * @return false if the status is unknown, true otherwise.
     */
    private boolean checkObjectTypeStatus(ObjectTypeIE objectTypeIE, ObjectType objectType) {
        boolean configOk = true;
        if (objectType.getObjectTypeStatus() != null && !isValidStatus(objectType.getObjectTypeStatus())) {
            configOk = false;
            objectTypeIE.addUnknownStatus(objectType.getObjectTypeStatus().getKeyword());
        } else if (objectType.getObjectTypeStatus() != null) {
            objectType.setObjectTypeStatus(ObjectTypeStatus.getStatusByKeyword(objectType.getObjectTypeStatus().getKeyword()));
        }
        return configOk;
    }

    /**
     * Check Object File Types.
     *
     * @param objectTypeIE  {@link ObjectTypeIE}
     * @param objectType    Object Type to check.
     * @param entityManager EntityManager used to search in DB.
     * @return false if File Type is null or unknown.
     */
    private boolean checkObjectFileTypes(ObjectTypeIE objectTypeIE, ObjectType objectType, EntityManager entityManager) {
        boolean configOk = true;
        if (objectType.getObjectFiles() != null && !objectType.getObjectFiles().isEmpty()) {
            for (ObjectFile objectFile : objectType.getObjectFiles()) {
                if (objectFile.getType() != null) {
                    ObjectFileTypeQuery objectFileTypeQuery = new ObjectFileTypeQuery(entityManager);
                    objectFileTypeQuery.keyword().eq(objectFile.getType().getKeyword());
                    ObjectFileType dbObjetFileType = objectFileTypeQuery.getUniqueResult();
                    if (dbObjetFileType != null) {
                        objectFile.setType(dbObjetFileType);
                    } else {
                        configOk = false;
                        if (objectTypeIE.getUnknownObjectFileTypes() == null || !objectTypeIE.getUnknownObjectFileTypes().contains(objectFile.getType().getKeyword())) {
                            objectTypeIE.addUnknownObjectFileType(objectFile.getType().getKeyword());
                        }
                    }
                } else {
                    configOk = false;
                }
            }
        }
        return configOk;
    }

    /**
     * Check AIPO from Object Reader.
     *
     * @param objectTypeIE  {@link ObjectTypeIE}
     * @param objectType    object type to check.
     * @param entityManager EntityManager used to search in DB.
     * @return false if AIPO is null or unknown.
     */
    private boolean checkAIPOFromObjectReader(ObjectTypeIE objectTypeIE, ObjectType objectType, EntityManager entityManager) {
        boolean configOk = true;
        if (objectType.getObjectReaders() != null && !objectType.getObjectReaders().isEmpty()) {
            for (ObjectReader objectReader : objectType.getObjectReaders()) {
                if (objectReader.getAIPO() != null) {
                    ActorIntegrationProfileOption dbActorIntegrationProfileOption =
                            existingActorIntegrationProfileOption(objectReader.getAIPO(), entityManager);
                    if (dbActorIntegrationProfileOption != null) {
                        objectReader.setAIPO(dbActorIntegrationProfileOption);
                        AIPO dbAIPO = entityManager.find(AIPO.class, dbActorIntegrationProfileOption.getId());
                        if (dbAIPO != null) {
                            objectReader.setReversedAipo(dbAIPO);
                            dbAIPO.getObjectReaders().add(objectReader);
                        } else {
                            configOk = false;
                        }
                    } else {
                        configOk = false;
                        if (objectTypeIE.getUnknownAipo() == null || !objectTypeIE.getUnknownAipo().contains(objectReader.getAIPO().toStringShort())) {
                            objectTypeIE.addUnknownAipo(objectReader.getAIPO().toStringShort());
                        }
                    }
                }
            }
        }
        return configOk;
    }

    /**
     * Check AIPO from Object Reader.
     *
     * @param objectTypeIE  {@link ObjectTypeIE}
     * @param objectType    object type to check.
     * @param entityManager EntityManager used to search in DB.
     * @return false if AIPO is null or unknown.
     */
    private boolean checkAIPOFromObjectCreator(ObjectTypeIE objectTypeIE, ObjectType objectType, EntityManager entityManager) {
        boolean configOk = true;
        if (objectType.getObjectCreators() != null && !objectType.getObjectCreators().isEmpty()) {
            for (ObjectCreator objectCreator : objectType.getObjectCreators()) {
                if (objectCreator.getAIPO() != null) {
                    ActorIntegrationProfileOption dbActorIntegrationProfileOption =
                            existingActorIntegrationProfileOption(objectCreator.getAIPO(), entityManager);
                    if (dbActorIntegrationProfileOption != null) {
                        objectCreator.setAIPO(dbActorIntegrationProfileOption);
                        AIPO dbAIPO = entityManager.find(AIPO.class, dbActorIntegrationProfileOption.getId());
                        if (dbAIPO != null) {
                            objectCreator.setReversedAipo(dbAIPO);
                            dbAIPO.getObjectCreators().add(objectCreator);
                        } else {
                            configOk = false;
                        }
                    } else {
                        configOk = false;
                        objectTypeIE.addUnknownAipo(objectCreator.getAIPO().toStringShort());
                    }
                }
            }
        }
        return configOk;
    }

    /**
     * Check if the status of the ObjectType is valid.
     *
     * @param objectTypeStatus status to check.
     * @return true if the status is valid, false otherwise.
     */
    public boolean isValidStatus(ObjectTypeStatus objectTypeStatus) {
        return ObjectTypeStatus.getSTATUS_READY().getKeyword().equals(objectTypeStatus.getKeyword()) ||
                ObjectTypeStatus.getSTATUS_TOBECOMPLETED().getKeyword().equals(objectTypeStatus.getKeyword()) ||
                ObjectTypeStatus.getSTATUS_STORAGESUBSTITUTE().getKeyword().equals(objectTypeStatus.getKeyword()) ||
                ObjectTypeStatus.getSTATUS_DEPRECATED().getKeyword().equals(objectTypeStatus.getKeyword());
    }

    /**
     * Checks an AIPO does exist in DB.
     *
     * @param aipo          AIPO to look for.
     * @param entityManager EntityManager used to perform the Search.
     * @return the value of the AIPO
     */
    private ActorIntegrationProfileOption existingActorIntegrationProfileOption(ActorIntegrationProfileOption aipo, EntityManager entityManager) {
        ActorIntegrationProfileOptionQuery aipoQuery = new ActorIntegrationProfileOptionQuery(entityManager);
        if (aipo.getIntegrationProfileOption() != null) {
            aipoQuery.integrationProfileOption().keyword().eq(aipo.getIntegrationProfileOption().getKeyword());
        } else {
            aipoQuery.integrationProfileOption().eq(null);
        }
        if (aipo.getActorIntegrationProfile().getActor() != null) {
            aipoQuery.actorIntegrationProfile().actor().keyword().eq(aipo.getActorIntegrationProfile().getActor().getKeyword());
        } else {
            aipoQuery.actorIntegrationProfile().actor().eq(null);
        }
        if (aipo.getActorIntegrationProfile().getIntegrationProfile() != null) {
            aipoQuery.actorIntegrationProfile().integrationProfile().keyword().eq(aipo.getActorIntegrationProfile().getIntegrationProfile().getKeyword());
        } else {
            aipoQuery.actorIntegrationProfile().integrationProfile().eq(null);
        }
        ActorIntegrationProfileOption aipoFromDB = aipoQuery.getUniqueResult();
        return aipoFromDB;
    }

    /**
     * Look for duplicate Object Type in DB.
     *
     * @param objectTypeIE  list of Object Types.
     * @param entityManager EntityManager used to search DB.
     * @return the updated ObjectTypeIE structure.
     */
    private ObjectTypeIE searchForDuplicate(ObjectTypeIE objectTypeIE, EntityManager entityManager) {
        List<ObjectType> objectTypes = objectTypeIE.getObjectTypes();
        Iterator iterator = objectTypes.iterator();
        while (iterator.hasNext()) {
            ObjectType objectType = (ObjectType) iterator.next();

            ObjectTypeQuery objectTypeQuery = new ObjectTypeQuery(entityManager);
            objectTypeQuery.keyword().eq(objectType.getKeyword());
            ObjectType dbObjectType = objectTypeQuery.getUniqueResult();
            if (dbObjectType != null) {
                iterator.remove();
                objectTypeIE.addDuplicatedObjectType(objectType);
            }
        }

        objectTypeIE.setObjectTypes(objectTypes);
        return objectTypeIE;
    }

}
