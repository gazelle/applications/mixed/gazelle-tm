package net.ihe.gazelle.tf.action.converter;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.tf.model.*;
import net.ihe.gazelle.tf.model.export.Hl7MessageProfileIE;

import javax.persistence.EntityManager;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.List;

public class Hl7MessageProfileToXmlConverter {

    public String hl7MessageProfilesToXml(List<Hl7MessageProfile> hl7MessageProfiles) throws JAXBException {

        Hl7MessageProfileIE hl7MessageProfileIE = new Hl7MessageProfileIE(hl7MessageProfiles);

        JAXBContext jaxbContext = JAXBContext.newInstance(Hl7MessageProfileIE.class, Hl7MessageProfile.class, Domain.class,
                Actor.class, Transaction.class, AffinityDomain.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        StringWriter sw = new StringWriter();
        jaxbMarshaller.marshal(hl7MessageProfileIE, sw);
        return sw.toString();
    }

    public Hl7MessageProfileIE extractAndCheckMessageProfiles (String xmlString) throws JAXBException{
        Hl7MessageProfileIE hl7MessageProfileIE = xmlToMessageProfiles(xmlString);

        EntityManager entityManager = EntityManagerService.provideEntityManager();
        hl7MessageProfileIE = searchForDuplicate(hl7MessageProfileIE, entityManager);
        hl7MessageProfileIE = checkModelElements(hl7MessageProfileIE, entityManager);
        return hl7MessageProfileIE;
    }

    private Hl7MessageProfileIE xmlToMessageProfiles(String xmlString) throws JAXBException {

        JAXBContext jaxbContext = JAXBContext.newInstance(Hl7MessageProfileIE.class, Hl7MessageProfile.class, Domain.class,
                Actor.class, Transaction.class, AffinityDomain.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

        Reader reader = new InputStreamReader(new ByteArrayInputStream
                (xmlString.getBytes(StandardCharsets.UTF_8)), StandardCharsets.UTF_8);
        return (Hl7MessageProfileIE) unmarshaller.unmarshal(reader);
    }

    private Hl7MessageProfileIE searchForDuplicate(Hl7MessageProfileIE hl7MessageProfileIE, EntityManager entityManager){

        Hl7MessageProfileQuery query = new Hl7MessageProfileQuery(entityManager);
        List<Hl7MessageProfile> profilesInBase = query.getList();
        boolean duplicate;
        boolean ignored;

        List<Hl7MessageProfile> profilesToImport = hl7MessageProfileIE.getMessageProfiles();
        Iterator iterator = profilesToImport.iterator();
        while (iterator.hasNext()) {
            Hl7MessageProfile hl7MessageProfile = ((Hl7MessageProfile)iterator.next());
            duplicate = false;
            ignored = false;
            for (Hl7MessageProfile profileInBase : profilesInBase){
                if (hl7MessageProfile.getProfileOid() != null){
                    if (hl7MessageProfile.getProfileOid().equals(profileInBase.getProfileOid())){
                        duplicate = true;
                    }
                } else {
                    ignored = true;
                }
            }
            if (duplicate){
                iterator.remove();
                hl7MessageProfileIE.addDuplicatedProfile(hl7MessageProfile);
            } else if (ignored){
                iterator.remove();
                hl7MessageProfileIE.addIgnoredProfile(hl7MessageProfile);
            }
        }
        hl7MessageProfileIE.setMessageProfiles(profilesToImport);
        return hl7MessageProfileIE;
    }

    private Hl7MessageProfileIE checkModelElements(Hl7MessageProfileIE hl7MessageProfileIE, EntityManager entityManager){

        List<Hl7MessageProfile> profilesToImport = hl7MessageProfileIE.getMessageProfiles();
        Iterator iterator = profilesToImport.iterator();
        boolean ignored;

        while (iterator.hasNext()) {
            Hl7MessageProfile hl7MessageProfile = ((Hl7MessageProfile)iterator.next());
            ignored = false;

            if (hl7MessageProfile.getDomain() != null){
                DomainQuery query = new DomainQuery(entityManager);
                query.keyword().eq(hl7MessageProfile.getDomain().getKeyword());
                Domain domain = query.getUniqueResult();
                if (domain == null){
                    ignored = true;
                    hl7MessageProfileIE.addUnknownDomain(hl7MessageProfile.getDomain().getKeyword());
                } else {
                    hl7MessageProfile.setDomain(domain);
                }
            }

            if (hl7MessageProfile.getActor() != null){
                ActorQuery query = new ActorQuery(entityManager);
                query.keyword().eq(hl7MessageProfile.getActor().getKeyword());
                Actor actor = query.getUniqueResult();
                if (actor == null){
                    ignored = true;
                    hl7MessageProfileIE.addUnknownActor(hl7MessageProfile.getActor().getKeyword());
                } else {
                    hl7MessageProfile.setActor(actor);
                }
            }

            if (hl7MessageProfile.getTransaction() != null){
                TransactionQuery query = new TransactionQuery(entityManager);
                query.keyword().eq(hl7MessageProfile.getTransaction().getKeyword());
                Transaction transaction = query.getUniqueResult();
                if (transaction == null){
                    ignored = true;
                    hl7MessageProfileIE.addUnknownTransaction(hl7MessageProfile.getTransaction().getKeyword());
                } else {
                    hl7MessageProfile.setTransaction(transaction);
                }
            }


            if (ignored){
                iterator.remove();
                hl7MessageProfileIE.addIgnoredProfile(hl7MessageProfile);
            }
        }
        hl7MessageProfileIE.setMessageProfiles(profilesToImport);
        return hl7MessageProfileIE;
    }


}
