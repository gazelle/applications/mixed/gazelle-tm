package net.ihe.gazelle.tf.action;

import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.objects.model.*;
import net.ihe.gazelle.tf.model.*;
import net.ihe.gazelle.tf.model.auditMessage.AuditMessage;
import net.ihe.gazelle.tf.model.auditMessage.export.AuditMessageIE;
import net.ihe.gazelle.tf.model.constraints.AipoRule;
import net.ihe.gazelle.tf.model.constraints.export.AipoRulesIE;
import net.ihe.gazelle.tf.model.export.DocumentIE;
import net.ihe.gazelle.tf.model.export.Hl7MessageProfileIE;
import net.ihe.gazelle.tf.model.export.StandardIE;
import net.ihe.gazelle.tm.configurations.converter.ConfigurationTypeMappedWithAIPOToXmlConverter;
import net.ihe.gazelle.tm.configurations.model.*;
import net.ihe.gazelle.tm.configurations.model.export.ConfigurationTypeMappedWithAIPOIE;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Persistence manager for the import of TF Model elements.
 */
public class TFPersistenceManager {

    /**
     * Persist TF Rules from a list.
     *
     * @param aipoRulesIE list of TF Rules to persist.
     * @return the number of added TF Rules.
     */
    public int addTFRules(AipoRulesIE aipoRulesIE) {

        int nbrAddedRules = 0;

        EntityManager em = EntityManagerService.provideEntityManager();
        if (aipoRulesIE != null && aipoRulesIE.getAipoRules() != null) {
            for (AipoRule aipoRule : aipoRulesIE.getAipoRules()) {
                em.merge(aipoRule);
                nbrAddedRules++;
            }
            em.flush();
        }

        return nbrAddedRules;
    }

    /**
     * Persist Standards from a list.
     *
     * @param standardIE list of standards to persist.
     * @return the nomber of persisted standards.
     */
    public int addStandards(StandardIE standardIE) {

        int nbrAddedStandards = 0;

        EntityManager em = EntityManagerService.provideEntityManager();
        if (standardIE != null && standardIE.getStandards() != null) {
            for (Standard standard : standardIE.getStandards()) {
                Standard mergedStandard = em.merge(standard);
                for (Transaction transaction : mergedStandard.getTransactions()) {
                    List<Standard> standardList = HibernateHelper.getLazyValue(transaction, "standards", transaction.getStandards());
                    standardList.add(mergedStandard);
                    transaction.setStandards(standardList);
                    em.merge(transaction);
                }
                nbrAddedStandards++;
            }
            em.flush();
        }
        return nbrAddedStandards;
    }

    /**
     * Persist Documents from a list.
     *
     * @param documentIE list of documents to persist.
     * @return the number of persisted documents.
     */
    public int addDocuments(DocumentIE documentIE) {

        int nbrAddedDocuments = 0;

        EntityManager em = EntityManagerService.provideEntityManager();
        if (documentIE != null) {
            if (documentIE.getDocuments() != null) {
                for (Document document : documentIE.getDocuments()) {
                    mergeDocument(document, em);
                    nbrAddedDocuments++;
                }
            }
            if (documentIE.getDuplicatedDocuments() != null) {
                for (Document document : documentIE.getDuplicatedDocuments()) {
                    mergeDocument(document, em);
                    nbrAddedDocuments++;
                }
            }
            em.flush();
        }
        return nbrAddedDocuments;
    }

    /**
     * Merge an existing documents.
     *
     * @param document      document to merge.
     * @param entityManager EntityManager used to perform the action.
     */
    private void mergeDocument(Document document, EntityManager entityManager) {
        List<DocumentSection> sections = document.getDocumentSection();
        document.setDocumentSection(null);
        document = entityManager.merge(document);
        if (sections != null && !sections.isEmpty()) {
            for (DocumentSection documentSection : sections) {
                documentSection.setDocument(document);
                entityManager.merge(documentSection);
            }
        }
        DocumentSectionQuery query = new DocumentSectionQuery();
        query.document().document_md5_hash_code().eq(document.getDocument_md5_hash_code());
        document.setDocumentSection(query.getList());
    }

    /**
     * Add configurations from a list.
     *
     * @param configurationsIE list of configurations to add.
     * @return the number of added configurations.
     */
    public int addConfigurations(ConfigurationTypeMappedWithAIPOIE configurationsIE) {

        int nbrAddedConfiguration = 0;
        ConfigurationTypeMappedWithAIPOToXmlConverter converter = new ConfigurationTypeMappedWithAIPOToXmlConverter();
        EntityManager em = EntityManagerService.provideEntityManager();

        if (configurationsIE != null && configurationsIE.getConfigurationTypeMappedWithAIPOS() != null) {
            for (ConfigurationTypeMappedWithAIPO configuration : configurationsIE.getConfigurationTypeMappedWithAIPOS()) {
                ConfigurationTypeMappedWithAIPO configurationToPersist = configuration;
                ActorIntegrationProfileOption aipo = converter.existingAipo(configuration.getActorIntegrationProfileOption(), em);
                if (aipo != null) {
                    ConfigurationTypeMappedWithAIPOQuery query = new ConfigurationTypeMappedWithAIPOQuery(em);
                    query.actorIntegrationProfileOption().id().eq(aipo.getId());
                    ConfigurationTypeMappedWithAIPO configurationInDB = query.getUniqueResult();
                    if (configurationInDB != null) {
                        configurationToPersist = configurationInDB;
                    } else {
                        configurationToPersist.setActorIntegrationProfileOption(aipo);
                    }
                }

                List<ConfigurationTypeWithPortsWSTypeAndSopClass> configurationTypes = new ArrayList<>();
                for (ConfigurationTypeWithPortsWSTypeAndSopClass configurationType : configuration.getListOfConfigurationTypes()) {
                    configurationType.setWebServiceType(null);

                    configurationType.setConfigurationType(getConfigurationTypeToPersist(configurationType.getConfigurationType(), em));
                    configurationType.setWsTRansactionUsage(getWSTransactinUsageToPersist(configurationType.getWsTRansactionUsage(), em));
                    configurationType.setSopClass(getSopClassToPersist(configurationType.getSopClass(), em));
                    configurationType.setTransportLayer(getTransportLayerToPersist(configurationType.getTransportLayer(), em));
                    configurationTypes.add(em.merge(configurationType));
                }

                configurationToPersist.setListOfConfigurationTypes(configurationTypes);

                em.merge(configurationToPersist);
                nbrAddedConfiguration++;
            }
            em.flush();
        }

        return nbrAddedConfiguration;
    }

    /**
     * Get ConfigurationType to persist.
     * If it does not already exist in Database, it is created.
     *
     * @param configurationType ConfigurationType to look for in Database.
     * @param entityManager     EntityManager used to perform the action.
     * @return the ConfigurationType to persist.
     */
    private ConfigurationType getConfigurationTypeToPersist(ConfigurationType configurationType, EntityManager entityManager) {
        if (configurationType != null) {
            ConfigurationTypeQuery configurationTypeQuery = new ConfigurationTypeQuery(entityManager);
            configurationTypeQuery.className().eq(configurationType.getClassName());
            ConfigurationType configurationTypeFromDB = configurationTypeQuery.getUniqueResult();
            if (configurationTypeFromDB != null) {
                return configurationTypeFromDB;
            }
        }
        return entityManager.merge(configurationType);
    }

    /**
     * Get WSTransactionUsage to persist.
     * If it does not already exist in Database, it is created.
     *
     * @param wsTransactionUsage WSTransactionUsage to look for in Database.
     * @param entityManager      EntityManager used to perform the action.
     * @return the WSTransactionUsage to persist.
     */
    private WSTransactionUsage getWSTransactinUsageToPersist(WSTransactionUsage wsTransactionUsage, EntityManager entityManager) {

        if (wsTransactionUsage != null) {
            WSTransactionUsageQuery query = new WSTransactionUsageQuery(entityManager);
            query.usage().eq(wsTransactionUsage.getUsage());
            WSTransactionUsage wsTransactionUsageFromDB = query.getUniqueResult();

            if (wsTransactionUsageFromDB != null) {
                return wsTransactionUsageFromDB;
            }

            if (wsTransactionUsage.getTransaction() != null) {
                TransactionQuery transactionQuery = new TransactionQuery(entityManager);
                transactionQuery.keyword().eq(wsTransactionUsage.getTransaction().getKeyword());
                wsTransactionUsage.setTransaction(transactionQuery.getUniqueResult());
            }
            return entityManager.merge(wsTransactionUsage);
        }
        return null;
    }

    /**
     * Get SopClass to persist.
     * If it does not already exist in Database, it is created.
     *
     * @param sopClass      SopClass to look for in Database.
     * @param entityManager EntityManager used to perform the action.
     * @return the SopClass to persist.
     */
    private SopClass getSopClassToPersist(SopClass sopClass, EntityManager entityManager) {

        if (sopClass != null) {
            SopClassQuery query = new SopClassQuery(entityManager);
            query.keyword().eq(sopClass.getKeyword());
            SopClass sopClassFromDB = query.getUniqueResult();
            if (sopClassFromDB != null) {
                return sopClassFromDB;
            }
            return entityManager.merge(sopClass);
        }
        return null;
    }

    /**
     * Get TransportLayer to persist.
     * If it does not already exist in Database, it is created.
     *
     * @param transportLayer TransportLayer to look for in Database.
     * @param entityManager  EntityManager used to perform the action.
     * @return the TransportLayer to persist.
     */
    private TransportLayer getTransportLayerToPersist(TransportLayer transportLayer, EntityManager entityManager) {

        if (transportLayer != null) {
            TransportLayerQuery query = new TransportLayerQuery(entityManager);
            query.keyword().eq(transportLayer.getKeyword());
            TransportLayer transportLayerFromDB = query.getUniqueResult();
            if (transportLayerFromDB != null) {
                return transportLayerFromDB;
            }
            return entityManager.merge(transportLayer);
        }
        return null;
    }

    /**
     * Persist Audit Messages from a list.
     *
     * @param auditMessageIE list of Audit Messages to persist.
     * @return the number of persisted Audit Messages.
     */
    public int addAuditMessages(AuditMessageIE auditMessageIE) {
        int nbrAddedAuditMessage = 0;

        EntityManager em = EntityManagerService.provideEntityManager();
        if (auditMessageIE != null && auditMessageIE.getAuditMessages() != null) {
            for (AuditMessage auditMessage : auditMessageIE.getAuditMessages()) {
                em.merge(auditMessage);
                nbrAddedAuditMessage++;
            }
            em.flush();
        }
        return nbrAddedAuditMessage;
    }

    /**
     * Persist HL7 Messages Profiles from a list.
     *
     * @param hl7MessageProfileIE list of HL7 Message Profiles to persist.
     * @return the number of persisted HL7 Message Profiles.
     */
    public int addHl7LMessageProfiles(Hl7MessageProfileIE hl7MessageProfileIE) {

        int nbrAddedProfiles = 0;

        EntityManager em = EntityManagerService.provideEntityManager();
        if (hl7MessageProfileIE != null) {
            if (hl7MessageProfileIE.getMessageProfiles() != null) {
                for (Hl7MessageProfile messageProfile : hl7MessageProfileIE.getMessageProfiles()) {
                    mergeMessageProfile(messageProfile, em);
                    nbrAddedProfiles++;
                }
            }
            em.flush();
        }
        return nbrAddedProfiles;
    }

    /**
     * Merge an existing HL7 Message Profile.
     *
     * @param hl7MessageProfile profile to merge.
     * @param entityManager     EntityManager used to perform the action.
     */
    private void mergeMessageProfile(Hl7MessageProfile hl7MessageProfile, EntityManager entityManager) {

        if (hl7MessageProfile != null) {
            if (hl7MessageProfile.getAffinityDomains() != null) {
                List<AffinityDomain> mergedAffinityDomains = new ArrayList<>();
                for (AffinityDomain affinityDomain : hl7MessageProfile.getAffinityDomains()) {
                    AffinityDomainQuery query = new AffinityDomainQuery(entityManager);
                    query.keyword().eq(affinityDomain.getKeyword());
                    AffinityDomain affinityDomainFromBase = query.getUniqueResult();
                    if (affinityDomainFromBase != null) {
                        mergedAffinityDomains.add(affinityDomainFromBase);
                    } else {
                        mergedAffinityDomains.add(entityManager.merge(affinityDomain));
                    }
                }
                hl7MessageProfile.setAffinityDomains(mergedAffinityDomains);
            }
            entityManager.merge(hl7MessageProfile);
        }
    }

    /**
     * Persist Object Types from a list.
     *
     * @param objectTypeIE list of Object Types to persist.
     * @return the number of persisted Object Types.
     */
    public int addSamplesType(ObjectTypeIE objectTypeIE) {

        int nbrAddedConfiguration = 0;
        EntityManager em = EntityManagerService.provideEntityManager();

        if (objectTypeIE != null && objectTypeIE.getObjectTypes() != null) {
            for (ObjectType objectType : objectTypeIE.getObjectTypes()) {

                Set<ObjectFile> objectFilesToPersist = objectType.getObjectFiles();
                objectType.setObjectFiles(null);

                Set<ObjectAttribute> objectAttributesToPersist = objectType.getObjectAttributes();
                objectType.setObjectAttributes(null);

                Set<ObjectReader> objectReadersToPersist = objectType.getObjectReaders();
                objectType.setObjectReaders(null);

                Set<ObjectCreator> objectCreatorsToPersist = objectType.getObjectCreators();
                objectType.setObjectCreators(null);

                objectType = em.merge(objectType);


                if (objectFilesToPersist != null && !objectFilesToPersist.isEmpty()) {
                    Set<ObjectFile> persistedObjectFiles = new HashSet<>();
                    for (ObjectFile objectFile : objectFilesToPersist) {
                        objectFile.setObject(objectType);
                        persistedObjectFiles.add(em.merge(objectFile));
                    }
                    objectType.setObjectFiles(persistedObjectFiles);
                }

                if (objectCreatorsToPersist != null && !objectCreatorsToPersist.isEmpty()) {
                    Set<ObjectCreator> persistedObjectCreators = new HashSet<>();
                    for (ObjectCreator objectCreator : objectCreatorsToPersist) {
                        objectCreator.setObject(objectType);
                        persistedObjectCreators.add(em.merge(objectCreator));
                    }
                    objectType.setObjectCreators(persistedObjectCreators);
                }

                if (objectReadersToPersist != null && !objectReadersToPersist.isEmpty()) {
                    Set<ObjectReader> persistedObjectReaders = new HashSet<>();
                    for (ObjectReader objectReader : objectReadersToPersist) {
                        objectReader.setObject(objectType);
                        persistedObjectReaders.add(em.merge(objectReader));
                    }
                    objectType.setObjectReaders(persistedObjectReaders);
                }

                if (objectAttributesToPersist != null && !objectAttributesToPersist.isEmpty()) {
                    Set<ObjectAttribute> persistedObjectAttributes = new HashSet<>();
                    for (ObjectAttribute objectAttribute : objectAttributesToPersist) {
                        objectAttribute.setObject(objectType);
                        objectAttribute = em.merge(objectAttribute);
                        persistedObjectAttributes.add(objectAttribute);
                    }
                    objectType.setObjectAttributes(persistedObjectAttributes);
                }
                nbrAddedConfiguration++;
            }
            em.flush();
        }
        return nbrAddedConfiguration;
    }

    /**
     * Persist OID RooT Definitions from a list.
     *
     * @param oidRootDefinitionIE list of OID Root Definition to persist.
     * @return the number of persisted OID Root Definition.
     */
    public int addOIDRootDefinitions(OIDRootDefinitionIE oidRootDefinitionIE) {

        int nbrAddedOIDRootDefinition = 0;
        EntityManager em = EntityManagerService.provideEntityManager();

        if (oidRootDefinitionIE != null && oidRootDefinitionIE.getOidRootDefinitions() != null) {
            for (OIDRootDefinition oidRootDefinition : oidRootDefinitionIE.getOidRootDefinitions()) {

                List<OIDRequirement> oidRequirements = oidRootDefinition.getListOIDRequirementsNotPersisted();
                oidRootDefinition.setOidRequirements(null);
                oidRootDefinition = em.merge(oidRootDefinition);

                for (OIDRequirement oidRequirement : oidRequirements) {
                    oidRequirement.setOidRootDefinition(oidRootDefinition);
                    em.merge(oidRequirement);
                }
                nbrAddedOIDRootDefinition++;
            }
            em.flush();
        }
        return nbrAddedOIDRootDefinition;
    }

    /**
     * Persist OID Requirements from a list.
     *
     * @param oidRootDefinitionIE list of OID Requirements to persist.
     * @return the number of persisted OID Requirements.
     */
    public int addOIDRequirements(OIDRootDefinitionIE oidRootDefinitionIE) {

        int nbrAddedOIDRequirements = 0;
        EntityManager em = EntityManagerService.provideEntityManager();

        if (oidRootDefinitionIE != null && oidRootDefinitionIE.getNewRequirements() != null) {
            for (OIDRequirement oidRequirement : oidRootDefinitionIE.getNewRequirements()) {
                OIDRequirementQuery query = new OIDRequirementQuery(em);
                query.label().eq(oidRequirement.getLabel());
                query.prefix().eq(oidRequirement.getPrefix());
                OIDRequirement dbOidRequirement = query.getUniqueResult();
                dbOidRequirement.setActorIntegrationProfileOptionList(oidRequirement.getActorIntegrationProfileOptionList());
                em.merge(dbOidRequirement);
                nbrAddedOIDRequirements++;
            }
            em.flush();
        }
        return nbrAddedOIDRequirements;
    }
}
