package net.ihe.gazelle.tf.action.converter;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.tf.model.Actor;
import net.ihe.gazelle.tf.model.IntegrationProfile;
import net.ihe.gazelle.tf.model.IntegrationProfileOption;
import net.ihe.gazelle.tf.model.constraints.*;
import net.ihe.gazelle.tf.model.constraints.export.AipoRulesIE;

import javax.persistence.EntityManager;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.List;

public class TFRuleToXmlConverter {

    public String rulesToXml(List<AipoRule> aipoRules) throws JAXBException {

        AipoRulesIE aipoRulesIE = this.listToIE(aipoRules);

        JAXBContext jaxbContext = JAXBContext.newInstance(AipoRulesIE.class, AipoRule.class,
                AipoList.class, AipoSingle.class, AipoCriterion.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        StringWriter sw = new StringWriter();
        jaxbMarshaller.marshal(aipoRulesIE, sw);
        return sw.toString();
    }

    private AipoRulesIE listToIE(List<AipoRule> aipoRules){
        return new AipoRulesIE(aipoRules);
    }

    public AipoRulesIE extractAndCheckRules(String xmlString) throws JAXBException{
        AipoRulesIE aipoRulesIE = xmlToTFRules(xmlString);
        aipoRulesIE = checkIgnoredRules(aipoRulesIE);
        aipoRulesIE = searchForDuplicate(aipoRulesIE);
        return aipoRulesIE;
    }

    public AipoRulesIE xmlToTFRules(String xmlString) throws JAXBException {

        JAXBContext jaxbContext = JAXBContext.newInstance(AipoRulesIE.class, AipoRule.class,
                AipoList.class, AipoSingle.class, AipoCriterion.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

        Reader reader = new InputStreamReader(new ByteArrayInputStream
                (xmlString.getBytes(StandardCharsets.UTF_8)), StandardCharsets.UTF_8);
        return (AipoRulesIE) unmarshaller.unmarshal(reader);
    }

    private AipoRulesIE checkIgnoredRules(AipoRulesIE aipoRulesIE){
        List<AipoRule> aipoRules = aipoRulesIE.getAipoRules();
        Iterator iterator = aipoRules.iterator();
        while (iterator.hasNext()) {
            AipoRule aipoRule = ((AipoRule)iterator.next());
            String stringRule = aipoRule.toString();
            aipoRule = getModelElementsFromKeyword(aipoRulesIE, aipoRule);
            if (aipoRule == null){
                iterator.remove();
                aipoRulesIE.addIgnoredRule(stringRule);
            }
        }
        aipoRulesIE.setAipoRules(aipoRules);
        return aipoRulesIE;
    }

    private AipoRule getModelElementsFromKeyword(AipoRulesIE aipoRulesIE, AipoRule aipoRule){
        aipoRule.setCause(checkAipoCriterion(aipoRulesIE, aipoRule.getCause()));
        aipoRule.setConsequence(checkAipoCriterion(aipoRulesIE, aipoRule.getConsequence()));
        if (!isImportable(aipoRule)){
            return null;
        }
        return aipoRule;
    }

    private AipoCriterion checkAipoCriterion(AipoRulesIE aipoRulesIE, AipoCriterion aipoCriterion){
        if (aipoCriterion instanceof AipoList){
            return checkAipoList(aipoRulesIE, (AipoList) aipoCriterion);
        } else if (aipoCriterion instanceof  AipoSingle) {
            return checkAipoSingle(aipoRulesIE, (AipoSingle) aipoCriterion);
        }
        return null;
    }

    private AipoCriterion checkAipoList(AipoRulesIE aipoRulesIE, AipoList aipoList){
        List<AipoCriterion> aipoCriteria = aipoList.getAipoCriterions();
        Iterator iterator = aipoCriteria.iterator();
        while (iterator.hasNext()) {
            AipoCriterion aipoCriterion = ((AipoCriterion)iterator.next());
            aipoCriterion = checkAipoCriterion(aipoRulesIE, aipoCriterion);
            if (aipoCriterion == null){
                iterator.remove();
            }
        }
        if (aipoCriteria.isEmpty()){
            return null;
        } else if (aipoCriteria.size() == 1){
            return aipoCriteria.get(0);
        }
        aipoList.setAipoCriterions(aipoCriteria);
        return aipoList;
    }

    private AipoCriterion checkAipoSingle(AipoRulesIE aipoRulesIE, AipoSingle aipoSingle){
        String actorKeyword = aipoSingle.getActorKeyword();
        String integrationProfileKeyword = aipoSingle.getIntegrationProfileKeyword();
        String optionKeyword = aipoSingle.getOptionKeyword();

        Actor actor = null;
        IntegrationProfile integrationProfile = null;
        IntegrationProfileOption option = null;

        if (actorKeyword != null) {
            actor = Actor.findActorWithKeyword(actorKeyword);
            if (actor == null){
                aipoRulesIE.addUnknownActor(actorKeyword);
            }
        }
        if (integrationProfileKeyword != null){
            integrationProfile = IntegrationProfile.findIntegrationProfileWithKeyword(integrationProfileKeyword);
            if (integrationProfile == null){
                aipoRulesIE.addUnknownIntegrationProfile(integrationProfileKeyword);
            }
        }
        if (optionKeyword != null){
            option = IntegrationProfileOption.findIntegrationProfileOptionWithKeyword(optionKeyword);
            if (option == null){
                aipoRulesIE.addUnknownOption(optionKeyword);
            }
        }

        if ((actorKeyword != null && actor == null)
                || (integrationProfileKeyword != null && integrationProfile == null)
                || (optionKeyword != null && option == null)){
            return null;
        }

        aipoSingle.setActor(actor);
        aipoSingle.setIntegrationProfile(integrationProfile);
        aipoSingle.setOption(option);
        return aipoSingle;
    }

    private boolean isImportable(AipoRule aipoRule){
        return (aipoRule.getCause() != null && aipoRule.getConsequence() != null);
    }

    private AipoRulesIE searchForDuplicate(AipoRulesIE aipoRulesIE){
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        AipoRuleQuery aipoRuleQuery = new AipoRuleQuery(entityManager);
        List <AipoRule> rulesFromBase = aipoRuleQuery.getList();
        boolean duplicate;

        List<AipoRule> aipoRules = aipoRulesIE.getAipoRules();
        Iterator iterator = aipoRules.iterator();
        while (iterator.hasNext()) {
            AipoRule aipoRule = ((AipoRule)iterator.next());
            duplicate = false;
            for (AipoRule ruleFromBase : rulesFromBase){
                if (match(aipoRule, ruleFromBase)){
                    duplicate = true;
                }
            }
            if (duplicate){
                iterator.remove();
                aipoRulesIE.addDuplicatedRules(aipoRule);
            }
        }
        aipoRulesIE.setAipoRules(aipoRules);
        return aipoRulesIE;
    }

    private boolean match(AipoRule aipoRule1, AipoRule aipoRule2){
        return (match(aipoRule1.getCause(), aipoRule2.getCause()) && match(aipoRule1.getConsequence(), aipoRule2.getConsequence()));
    }

    private boolean match(AipoCriterion aipoCriterion1, AipoCriterion aipoCriterion2){
        if (aipoCriterion1.getClass() != aipoCriterion2.getClass()){
            return false;
        } else if (aipoCriterion1 instanceof AipoSingle){
            return match((AipoSingle) aipoCriterion1, (AipoSingle) aipoCriterion2);
        } else if (aipoCriterion1 instanceof AipoList){
            return match((AipoList) aipoCriterion1, (AipoList) aipoCriterion2);
        } else{
            return false;
        }
    }

    private boolean match(AipoSingle aipoSingle1, AipoSingle aipoSingle2){
        return (areKeywordsEquals(aipoSingle1.getActorKeyword(),aipoSingle2.getActorKeyword())
                && (areKeywordsEquals(aipoSingle1.getIntegrationProfileKeyword(), aipoSingle2.getIntegrationProfileKeyword()))
                && (areKeywordsEquals(aipoSingle1.getOptionKeyword(), aipoSingle2.getOptionKeyword())));
    }

    private boolean areKeywordsEquals(String keyword1, String keyword2){
        if (keyword1 != null){
            return keyword1.equals(keyword2);
        } else {
            return keyword2 == null;
        }
    }

    private boolean match(AipoList aipoList1, AipoList aipoList2){
        boolean match;
        if (aipoList1.getAipoCriterions().size() != aipoList2.getAipoCriterions().size()){
            return false;
        } else {
            for (AipoCriterion aipoCriterionFrom1 : aipoList1.getAipoCriterions()){
                match = false;
                for(AipoCriterion aipoCriterionFrom2 : aipoList2.getAipoCriterions()){
                    if (match(aipoCriterionFrom1, aipoCriterionFrom2)) {
                        match = true;
                    }
                }
                if (!match){
                    return false;
                }
            }
            return true;
        }
    }
}
