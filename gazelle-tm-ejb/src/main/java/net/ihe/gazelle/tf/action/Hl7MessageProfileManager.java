package net.ihe.gazelle.tf.action;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.tf.action.converter.Hl7MessageProfileToXmlConverter;
import net.ihe.gazelle.tf.model.AffinityDomain;
import net.ihe.gazelle.tf.model.Hl7MessageProfile;
import net.ihe.gazelle.tf.model.Hl7MessageProfileQuery;
import net.ihe.gazelle.tf.model.export.Hl7MessageProfileIE;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.core.ResourceBundle;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.richfaces.event.FileUploadEvent;
import org.richfaces.model.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Remove;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.xml.bind.JAXBException;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;


@Name("tfHl7MessageProfileManager")
@Scope(ScopeType.SESSION)
@GenerateInterface("Hl7MessageProfileManagerLocal")
public class Hl7MessageProfileManager implements Serializable, Hl7MessageProfileManagerLocal {

    private static final long serialVersionUID = 450911179351283760L;
    private static final Logger LOG = LoggerFactory.getLogger(Hl7MessageProfileManager.class);

    private Hl7MessageProfile selectedProfile;
    private boolean displayProfile;
    private boolean displayAffinityDomainList;
    private AffinityDomain newAffinityDomain;
    private Filter<Hl7MessageProfile> filter;
    private FilterDataModel<Hl7MessageProfile> allProfiles;

    private String gazelleHL7ValidatorURL = null;
    
    private Hl7MessageProfileIE hl7MessageProfileToImport;
    private String uploadedFileContent;
    private String description;
    private boolean reviewBeforeSaving = true;

    @Override
    public FilterDataModel<Hl7MessageProfile> getAllProfiles() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getAllProfiles");
        }
        if (allProfiles == null){
            allProfiles =  new FilterDataModel<Hl7MessageProfile>(getFilter()) {
                @Override
                protected Object getId(Hl7MessageProfile hl7MessageProfile) {
                    return hl7MessageProfile.getId();
                }
            };
        }
        return allProfiles;
    }

    public void setAllProfiles(FilterDataModel<Hl7MessageProfile> allProfiles){
        this.allProfiles = allProfiles;
    }

    public Filter<Hl7MessageProfile> getFilter() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getFilter");
        }
        if (filter == null) {
            filter = new Filter<Hl7MessageProfile>(getHQLCriterionsForFilter());
        }
        return filter;
    }

    private HQLCriterionsForFilter<Hl7MessageProfile> getHQLCriterionsForFilter() {
        Hl7MessageProfileQuery query = new Hl7MessageProfileQuery();
        HQLCriterionsForFilter<Hl7MessageProfile> criterionsForFilter = query.getHQLCriterionsForFilter();
        criterionsForFilter.addPath("actor", query.actor());
        criterionsForFilter.addPath("transaction", query.transaction());
        criterionsForFilter.addPath("domain", query.domain());
        criterionsForFilter.addPath("afDomain", query.affinityDomains().labelToDisplay());
        criterionsForFilter.addPath("version", query.hl7Version());
        criterionsForFilter.addPath("type", query.messageType());
        return criterionsForFilter;
    }

    @Override
    public boolean getDisplayProfile() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getDisplayProfile");
        }
        return displayProfile;
    }

    @Override
    public void setDisplayProfile(boolean displayProfile) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("setDisplayProfile");
        }
        this.displayProfile = displayProfile;
    }

    @Override
    public Hl7MessageProfile getSelectedProfile() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getSelectedProfile");
        }
        return selectedProfile;
    }

    @Override
    public void setSelectedProfile(Hl7MessageProfile selectedProfile) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("setSelectedProfile");
        }
        this.selectedProfile = selectedProfile;
    }

    @Override
    public boolean isDisplayAffinityDomainList() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("isDisplayAffinityDomainList");
        }
        return displayAffinityDomainList;
    }

    @Override
    public void setDisplayAffinityDomainList(boolean displayAffinityDomainList) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("setDisplayAffinityDomainList");
        }
        this.displayAffinityDomainList = displayAffinityDomainList;
    }

    @Override
    public AffinityDomain getNewAffinityDomain() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getNewAffinityDomain");
        }
        return newAffinityDomain;
    }

    @Override
    public void setNewAffinityDomain(AffinityDomain newAffinityDomain) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("setNewAffinityDomain");
        }
        this.newAffinityDomain = newAffinityDomain;
    }

    /***********************************************************************/

    @Override
    @Remove
    @Destroy
    public void destroy() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("destroy");
        }

    }

    @Override
    public void editProfile(Hl7MessageProfile profile) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("editProfile");
        }
        selectedProfile = profile;
        displayProfile = true;
        displayAffinityDomainList = false;
    }

    @Override
    public void saveSelectedProfile() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("saveSelectedProfile");
        }
        Hl7MessageProfile.addMessageProfileReference(selectedProfile);
        displayProfile = false;
        selectedProfile = null;
    }

    @Override
    public void cancelDisplay() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("cancelDisplay");
        }
        displayProfile = false;
        selectedProfile = null;
    }

    @Override
    public List<SelectItem> getAffinityDomainList() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getAffinityDomainList");
        }
        List<AffinityDomain> affinityDomains = AffinityDomain.getAllAffinityDomains();
        List<SelectItem> items = null;
        if (affinityDomains != null) {
            items = new ArrayList<SelectItem>();
            items.add(new SelectItem(null, ResourceBundle.instance().getString("gazelle.common.PleaseSelect")));
            for (AffinityDomain ad : affinityDomains) {
                if (!selectedProfile.getAffinityDomains().contains(ad)) {
                    items.add(new SelectItem(ad, ad.getLabelToDisplay()));
                }
            }
        }
        newAffinityDomain = null;
        return items;
    }

    @Override
    public void removeAffinityDomain(AffinityDomain inAffinityDomain) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("removeAffinityDomain");
        }
        if ((selectedProfile.getAffinityDomains() != null)
                && selectedProfile.getAffinityDomains().contains(inAffinityDomain)) {
            selectedProfile.getAffinityDomains().remove(inAffinityDomain);
        }
    }

    @Override
    public void addAffinityDomain() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("addAffinityDomain");
        }
        if (selectedProfile.getAffinityDomains() != null) {
            selectedProfile.getAffinityDomains().add(newAffinityDomain);
        } else {
            List<AffinityDomain> aDomains = new ArrayList<AffinityDomain>();
            aDomains.add(newAffinityDomain);
            selectedProfile.setAffinityDomains(aDomains);
        }
        displayAffinityDomainList = false;
    }

    public String getGazelleHL7ValidatorURL() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getGazelleHL7ValidatorURL");
        }
        if (gazelleHL7ValidatorURL == null) {
            gazelleHL7ValidatorURL = PreferenceService.getString("gazelle_hl7_validator_url");
        }
        return gazelleHL7ValidatorURL;
    }

    public void exportHl7MessageProfilesAsXml(){
        try {
            TFModelExporter.exportHl7MessageProfiles(getAllProfiles().getAllItems(FacesContext.getCurrentInstance()));
        } catch (JAXBException e){
            LOG.error("Error exporting TF Rules as xml : ", e);
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Error converting TF Rules to xml");
        }
    }

    public Hl7MessageProfileIE getHl7MessageProfileToImport() {
        return hl7MessageProfileToImport;
    }

    public void setHl7MessageProfileToImport(Hl7MessageProfileIE hl7MessageProfileToImport) {
        this.hl7MessageProfileToImport = hl7MessageProfileToImport;
    }

    public String getUploadedFileContent() {
        return uploadedFileContent;
    }

    public void setUploadedFileContent(String uploadedFileContent) {
        this.uploadedFileContent = uploadedFileContent;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isReviewBeforeSaving() {
        return reviewBeforeSaving;
    }

    public void setReviewBeforeSaving(boolean reviewBeforeSaving) {
        this.reviewBeforeSaving = reviewBeforeSaving;
    }

    public void uploadListener(final FileUploadEvent event) {
        final UploadedFile uploadedFile = event.getUploadedFile();
        if (uploadedFile.getData() != null && uploadedFile.getData().length > 0) {
            setUploadedFileContent(new String(uploadedFile.getData(), StandardCharsets.UTF_8));
            setDescription(uploadedFile.getName());
        } else {
            FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.ERROR, "File is empty");
        }
    }

    public void resetImport(){
        resetUpload();
        this.hl7MessageProfileToImport = null;
    }

    public void resetUpload(){
        this.uploadedFileContent = null;
        this.description = null;
    }

    public void importMessageProfilesFromXml() {

        Hl7MessageProfileToXmlConverter hl7MessageProfileToXmlConverter = new Hl7MessageProfileToXmlConverter();
        try {
            setHl7MessageProfileToImport(hl7MessageProfileToXmlConverter.extractAndCheckMessageProfiles(getUploadedFileContent()));
            if (!this.reviewBeforeSaving){
                persistImportedMessageProfiles();
            }
        } catch (JAXBException e){
            LOG.error("Error Importing Standards : ", e);
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Error importing Standards with message :\n\"" +
                    e.getMessage() + "\" Please check your file.");
        }
    }

    public void persistImportedMessageProfiles(){

        TFPersistenceManager tfPersistenceManager = new TFPersistenceManager();
        int numberOfImportedRules = tfPersistenceManager.addHl7LMessageProfiles(hl7MessageProfileToImport);
        hl7MessageProfileToImport.setImported(true);
        FacesMessages.instance().add(StatusMessage.Severity.INFO, "Successfully imported " +
                numberOfImportedRules + " HL7 Message Profiles !");
    }
}