/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.objects.admin;

import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.common.tree.GazelleTreeNodeImpl;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.objects.model.*;
import net.ihe.gazelle.tf.action.TFModelExporter;
import net.ihe.gazelle.tf.action.TFPersistenceManager;
import net.ihe.gazelle.tf.action.converter.SamplesTypeToXmlConverter;
import net.ihe.gazelle.tf.model.*;
import net.ihe.gazelle.tm.datamodel.ObjectTypeDataModel;
import net.ihe.gazelle.tm.session.TestingSessionService;
import net.ihe.gazelle.tm.systems.model.SystemInSession;
import net.ihe.gazelle.tm.systems.model.SystemInSessionRegistrationStatus;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Synchronized;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.richfaces.event.FileUploadEvent;
import org.richfaces.model.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.xml.bind.JAXBException;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

/**
 * @author Abderrazek Boufahja > INRIA Rennes IHE development Project
 */

@Name("objectTypeManager")
@Scope(ScopeType.SESSION)
@Synchronized(timeout = 10000)
@GenerateInterface(value = "ObjectTypeManagerLocal")
public class ObjectTypeManager extends ObjectFileTypeManager implements ObjectTypeManagerLocal, Serializable {

    private static final long serialVersionUID = 9096149345318123456L;

    // ~ Logger ///////////////////////////////////////////////////////////////////////////

    private static final Logger LOG = LoggerFactory.getLogger(ObjectTypeManager.class);

    // ~ Attribute ///////////////////////////////////////////////////////////////////////

    private ObjectType selectedObjectType;

    private IntegrationProfile selectedIntegrationProfileForCreator;

    private Actor selectedActorForCreator;

    private IntegrationProfileOption selectedIntegrationProfileOptionForCreator;

    private String creatorDescription;

    private IntegrationProfile selectedIntegrationProfileForReader;

    private Actor selectedActorForReader;

    private IntegrationProfileOption selectedIntegrationProfileOptionForReader;

    private String readerDescription;

    private ObjectFileType selectedFileType;

    private String selectedFileDescription;

    private String createdAttributeType;

    private String createdAttributeDescription;

    private ObjectCreator selectedCreator;

    private ObjectReader selectedReader;

    private ObjectFile selectedObjectFile;

    private ObjectAttribute selectedObjectAttribute;

    private int selectedFileMinimum;

    private int selectedFileMaximum;

    private Domain selectedDomainForCreators;

    private Domain selectedDomainForReaders;

    private ObjectTypeStatus selectedObjectTypeStatus;

    private ObjectTypeDataModel foundObjectTypes;

    private String uploadedFileContent;
    private String description;
    private ObjectTypeIE objectTypeToImport;
    private boolean reviewBeforeSaving = true;

    @In(value = "testingSessionService")
    private transient TestingSessionService testingSessionService;

    // ~ getter && setter ////////////////////////////////////////////////////////////////

    /**
     * {@inheritDoc}
     */
    @Override
    public ObjectTypeDataModel getFoundObjectTypes() {
        LOG.debug("getFoundObjectTypes");
        return foundObjectTypes;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setFoundObjectTypes(ObjectTypeDataModel foundObjectTypes) {
        LOG.debug("setFoundObjectTypes");
        this.foundObjectTypes = foundObjectTypes;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ObjectTypeStatus getSelectedObjectTypeStatus() {
        LOG.debug("getSelectedObjectTypeStatus");
        return selectedObjectTypeStatus;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setSelectedObjectTypeStatus(ObjectTypeStatus selectedObjectTypeStatus) {
        LOG.debug("setSelectedObjectTypeStatus");
        this.selectedObjectTypeStatus = selectedObjectTypeStatus;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Domain getSelectedDomainForCreators() {
        LOG.debug("getSelectedDomainForCreators");
        return selectedDomainForCreators;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setSelectedDomainForCreators(Domain selectedDomainForCreators) {
        LOG.debug("setSelectedDomainForCreators");
        this.selectedDomainForCreators = selectedDomainForCreators;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Domain getSelectedDomainForReaders() {
        LOG.debug("getSelectedDomainForReaders");
        return selectedDomainForReaders;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setSelectedDomainForReaders(Domain selectedDomainForReaders) {
        LOG.debug("setSelectedDomainForReaders");
        this.selectedDomainForReaders = selectedDomainForReaders;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getSelectedFileMinimum() {
        LOG.debug("getSelectedFileMinimum");
        return selectedFileMinimum;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setSelectedFileMinimum(int selectedFileMinimum) {
        LOG.debug("setSelectedFileMinimum");
        this.selectedFileMinimum = selectedFileMinimum;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getSelectedFileMaximum() {
        LOG.debug("getSelectedFileMaximum");
        return selectedFileMaximum;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setSelectedFileMaximum(int selectedFileMaximum) {
        LOG.debug("setSelectedFileMaximum");
        this.selectedFileMaximum = selectedFileMaximum;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ObjectType getSelectedObjectType() {
        LOG.debug("getSelectedObjectType");
        return selectedObjectType;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setSelectedObjectType(ObjectType selectedObjectType) {
        LOG.debug("setSelectedObjectType");
        this.selectedObjectType = selectedObjectType;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IntegrationProfile getSelectedIntegrationProfileForCreator() {
        LOG.debug("getSelectedIntegrationProfileForCreator");
        return selectedIntegrationProfileForCreator;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setSelectedIntegrationProfileForCreator(IntegrationProfile selectedIntegrationProfileForCreator) {
        LOG.debug("setSelectedIntegrationProfileForCreator");
        this.selectedIntegrationProfileForCreator = selectedIntegrationProfileForCreator;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Actor getSelectedActorForCreator() {
        LOG.debug("getSelectedActorForCreator");
        return selectedActorForCreator;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setSelectedActorForCreator(Actor selectedActorForCreator) {
        LOG.debug("setSelectedActorForCreator");
        this.selectedActorForCreator = selectedActorForCreator;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IntegrationProfileOption getSelectedIntegrationProfileOptionForCreator() {
        LOG.debug("getSelectedIntegrationProfileOptionForCreator");
        return selectedIntegrationProfileOptionForCreator;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setSelectedIntegrationProfileOptionForCreator(
            IntegrationProfileOption selectedIntegrationProfileOptionForCreator) {
        this.selectedIntegrationProfileOptionForCreator = selectedIntegrationProfileOptionForCreator;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCreatorDescription() {
        LOG.debug("getCreatorDescription");
        return creatorDescription;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCreatorDescription(String creatorDescription) {
        LOG.debug("setCreatorDescription");
        this.creatorDescription = creatorDescription;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IntegrationProfile getSelectedIntegrationProfileForReader() {
        LOG.debug("getSelectedIntegrationProfileForReader");
        return selectedIntegrationProfileForReader;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setSelectedIntegrationProfileForReader(IntegrationProfile selectedIntegrationProfileForReader) {
        LOG.debug("setSelectedIntegrationProfileForReader");
        this.selectedIntegrationProfileForReader = selectedIntegrationProfileForReader;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Actor getSelectedActorForReader() {
        LOG.debug("getSelectedActorForReader");
        return selectedActorForReader;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setSelectedActorForReader(Actor selectedActorForReader) {
        LOG.debug("setSelectedActorForReader");
        this.selectedActorForReader = selectedActorForReader;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IntegrationProfileOption getSelectedIntegrationProfileOptionForReader() {
        LOG.debug("getSelectedIntegrationProfileOptionForReader");
        return selectedIntegrationProfileOptionForReader;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setSelectedIntegrationProfileOptionForReader(
            IntegrationProfileOption selectedIntegrationProfileOptionForReader) {
        this.selectedIntegrationProfileOptionForReader = selectedIntegrationProfileOptionForReader;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getReaderDescription() {
        LOG.debug("getReaderDescription");
        return readerDescription;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setReaderDescription(String readerDescription) {
        LOG.debug("setReaderDescription");
        this.readerDescription = readerDescription;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ObjectFileType getSelectedFileType() {
        LOG.debug("getSelectedFileType");
        return selectedFileType;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setSelectedFileType(ObjectFileType selectedFileType) {
        LOG.debug("setSelectedFileType");
        this.selectedFileType = selectedFileType;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getSelectedFileDescription() {
        LOG.debug("getSelectedFileDescription");
        return selectedFileDescription;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setSelectedFileDescription(String selectedFileDescription) {
        LOG.debug("setSelectedFileDescription");
        this.selectedFileDescription = selectedFileDescription;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCreatedAttributeType() {
        LOG.debug("getCreatedAttributeType");
        return createdAttributeType;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCreatedAttributeType(String createdAttributeType) {
        LOG.debug("setCreatedAttributeType");
        this.createdAttributeType = createdAttributeType;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCreatedAttributeDescription() {
        LOG.debug("getCreatedAttributeDescription");
        return createdAttributeDescription;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCreatedAttributeDescription(String createdAttributeDescription) {
        LOG.debug("setCreatedAttributeDescription");
        this.createdAttributeDescription = createdAttributeDescription;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ObjectCreator getSelectedCreator() {
        LOG.debug("getSelectedCreator");
        return selectedCreator;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setSelectedCreator(ObjectCreator selectedCreator) {
        LOG.debug("setSelectedCreator");
        this.selectedCreator = selectedCreator;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ObjectReader getSelectedReader() {
        LOG.debug("getSelectedReader");
        return selectedReader;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setSelectedReader(ObjectReader selectedReader) {
        LOG.debug("setSelectedReader");
        this.selectedReader = selectedReader;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ObjectFile getSelectedObjectFile() {
        LOG.debug("getSelectedObjectFile");
        return selectedObjectFile;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setSelectedObjectFile(ObjectFile selectedObjectFile) {
        LOG.debug("setSelectedObjectFile");
        this.selectedObjectFile = selectedObjectFile;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ObjectAttribute getSelectedObjectAttribute() {
        LOG.debug("getSelectedObjectAttribute");
        return selectedObjectAttribute;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setSelectedObjectAttribute(ObjectAttribute selectedObjectAttribute) {
        LOG.debug("setSelectedObjectAttribute");
        this.selectedObjectAttribute = selectedObjectAttribute;
    }

    /**
     * Getter for the uploadedFileContent property.
     *
     * @return the value of the property
     */
    public String getUploadedFileContent() {
        return uploadedFileContent;
    }

    /**
     * Setter for the uploadedFileContent property.
     *
     * @param uploadedFileContent value to set to the property.
     */
    public void setUploadedFileContent(String uploadedFileContent) {
        this.uploadedFileContent = uploadedFileContent;
    }

    /**
     * Getter for the description property.
     *
     * @return the value of the property.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Setter for the description property.
     *
     * @param description value to set to the property.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Getter for the objectToImport property.
     *
     * @return the value of the property.
     */
    public ObjectTypeIE getObjectTypeToImport() {
        return objectTypeToImport;
    }

    /**
     * Setter for the objectTypeToImport property.
     *
     * @param objectTypeToImport value to set to the property.
     */
    public void setObjectTypeToImport(ObjectTypeIE objectTypeToImport) {
        this.objectTypeToImport = objectTypeToImport;
    }

    /**
     * Getter for the reviewBeforeSaving property.
     *
     * @return the value of the property.
     */
    public boolean isReviewBeforeSaving() {
        return reviewBeforeSaving;
    }

    /**
     * Setter for the reviewBeforeSaving property.
     *
     * @param reviewBeforeSaving value to set to the property.
     */
    public void setReviewBeforeSaving(boolean reviewBeforeSaving) {
        this.reviewBeforeSaving = reviewBeforeSaving;
    }

    // ~ methods /////////////////////////////////////////////////////////////////////////

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ObjectType> getListOfAllObjectTypeForSelectedStatus() {
        LOG.debug("getListOfAllObjectTypeForSelectedStatus");
        if (this.selectedObjectTypeStatus != null) {
            return ObjectType.getObjectTypeFiltered(null, this.selectedObjectTypeStatus, null, null);
        } else {
            return ObjectType.getAllObjectType();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ObjectType> getListOfAllObjectTypeReady() {
        LOG.debug("getListOfAllObjectTypeReady");
        List<ObjectType> res = ObjectType.getObjectTypeFiltered("ready", null, null, null);
        Collections.sort(res);
        return res;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Restrict("#{s:hasPermission('ObjectTypeManager', 'getListOfObjectCreatorForSelectedObjectType', null)}")
    public List<ObjectCreator> getListOfObjectCreatorForSelectedObjectType() {
        LOG.debug("getListOfObjectCreatorForSelectedObjectType");
        if (selectedObjectType != null) {
            if (selectedObjectType.getId() != null) {
                return ObjectCreator.getObjectCreatorFiltered(null, selectedObjectType);
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Restrict("#{s:hasPermission('ObjectTypeManager', 'getListOfObjectReaderForSelectedObjectType', null)}")
    public List<ObjectReader> getListOfObjectReaderForSelectedObjectType() {
        LOG.debug("getListOfObjectReaderForSelectedObjectType");
        if (selectedObjectType != null) {
            if (selectedObjectType.getId() != null) {
                return ObjectReader.getObjectReaderFiltered(null, selectedObjectType);
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Restrict("#{s:hasPermission('ObjectTypeManager', 'getListOfObjectFileForSelectedObjectTypeForCreator', null)}")
    public List<ObjectFile> getListOfObjectFileForSelectedObjectTypeForCreator() {
        LOG.debug("getListOfObjectFileForSelectedObjectTypeForCreator");
        if (selectedObjectType != null) {
            if (selectedObjectType.getId() != null) {
                return ObjectFile.getObjectFileFiltered(selectedObjectType, "creator");
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Restrict("#{s:hasPermission('ObjectTypeManager', 'getListOfObjectFileForSelectedObjectTypeForReaders', null)}")
    public List<ObjectFile> getListOfObjectFileForSelectedObjectTypeForReaders() {
        LOG.debug("getListOfObjectFileForSelectedObjectTypeForReaders");
        if (selectedObjectType != null) {
            if (selectedObjectType.getId() != null) {
                return ObjectFile.getObjectFileFiltered(selectedObjectType, "reader");
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Restrict("#{s:hasPermission('ObjectTypeManager', 'getListOfObjectAttributeForSelectedObjectType', null)}")
    public List<ObjectAttribute> getListOfObjectAttributeForSelectedObjectType() {
        LOG.debug("getListOfObjectAttributeForSelectedObjectType");
        if (selectedObjectType != null) {
            if (selectedObjectType.getId() != null) {
                return ObjectAttribute.getObjectAttributeFiltered(selectedObjectType);
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<IntegrationProfile> getPossibleIntegrationProfilesForCreator() {
        LOG.debug("getPossibleIntegrationProfilesForCreator");
        if (this.selectedDomainForCreators != null) {
            return this.getPossibleIntegrationProfiles(this.selectedDomainForCreators);
        }
        return IntegrationProfile.listAllIntegrationProfiles();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<IntegrationProfile> getPossibleIntegrationProfilesForReader() {
        LOG.debug("getPossibleIntegrationProfilesForReader");
        if (this.selectedDomainForReaders != null) {
            return this.getPossibleIntegrationProfiles(this.selectedDomainForReaders);
        }
        return IntegrationProfile.listAllIntegrationProfiles();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Actor> getPossibleActorsForCreator() {
        LOG.debug("getPossibleActorsForCreator");
        List<Actor> actorL = null;
        if (this.selectedIntegrationProfileForCreator != null) {
            List<ActorIntegrationProfileOption> listOfTestParticipants = ActorIntegrationProfileOption
                    .getActorIntegrationProfileOptionFiltered(null, this.selectedIntegrationProfileForCreator, null,
                            null);
            actorL = getActorsFromTestParticipants(listOfTestParticipants);
        }
        return actorL;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Actor> getPossibleActorsForReader() {
        LOG.debug("getPossibleActorsForReader");
        List<Actor> actorL = null;
        if (this.selectedIntegrationProfileForReader != null) {
            List<ActorIntegrationProfileOption> listOfTestParticipants = ActorIntegrationProfileOption
                    .getActorIntegrationProfileOptionFiltered(null, this.selectedIntegrationProfileForReader, null,
                            null);
            actorL = getActorsFromTestParticipants(listOfTestParticipants);
        }
        return actorL;
    }

    /**
     * Get the list of all actor references by test participants in list.
     *
     * @param listOfTestParticipants list of test participants
     * @return the list of referenced actor.
     */
    private List<Actor> getActorsFromTestParticipants(List<ActorIntegrationProfileOption> listOfTestParticipants) {
        HashSet<Actor> setOfActor = new HashSet<>();
        if (listOfTestParticipants == null) {
            return null;
        }
        for (ActorIntegrationProfileOption tp : listOfTestParticipants) {
            setOfActor.add(tp.getActorIntegrationProfile().getActor());
        }

        List<Actor> actorL = new ArrayList<>(setOfActor);
        Collections.sort(actorL);
        return actorL;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<IntegrationProfileOption> getPossibleOptionsForCreator() {
        LOG.debug("getPossibleOptionsForCreator");
        List<IntegrationProfileOption> ipOL = null;
        if ((this.selectedIntegrationProfileForCreator != null) && (this.selectedActorForCreator != null)) {
            List<ActorIntegrationProfileOption> listOfTestParticipants = ActorIntegrationProfileOption
                    .getActorIntegrationProfileOptionFiltered(null, this.selectedIntegrationProfileForCreator, null,
                            this.selectedActorForCreator);
            ipOL = getOptionsFromTestParticipants(listOfTestParticipants);
        }
        return ipOL;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<IntegrationProfileOption> getPossibleOptionsForReader() {
        LOG.debug("getPossibleOptionsForReader");
        List<IntegrationProfileOption> ipOL = null;
        if ((this.selectedIntegrationProfileForReader != null) && (this.selectedActorForReader != null)) {
            List<ActorIntegrationProfileOption> listOfTestParticipants = ActorIntegrationProfileOption
                    .getActorIntegrationProfileOptionFiltered(null, this.selectedIntegrationProfileForReader, null,
                            this.selectedActorForReader);
            ipOL = getOptionsFromTestParticipants(listOfTestParticipants);
        }
        return ipOL;
    }

    /**
     * Get the list of all options references by test participants in list.
     *
     * @param listOfTestParticipants list of test participants
     * @return the list of referenced options.
     */
    private List<IntegrationProfileOption> getOptionsFromTestParticipants(List<ActorIntegrationProfileOption> listOfTestParticipants) {
        HashSet<IntegrationProfileOption> setOfOption = new HashSet<>();
        if (listOfTestParticipants == null) {
            return null;
        }
        for (ActorIntegrationProfileOption tp : listOfTestParticipants) {
            setOfOption.add(tp.getIntegrationProfileOption());
        }

        List<IntegrationProfileOption> ipOL = new ArrayList<>(setOfOption);
        Collections.sort(ipOL);
        return ipOL;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Restrict("#{s:hasPermission('ObjectTypeManager', 'addNewCreatorOfcurrentObjectType', null)}")
    public void addNewCreatorOfcurrentObjectType() {
        LOG.debug("addNewCreatorOfcurrentObjectType");
        if ((this.selectedIntegrationProfileForCreator != null) && (this.selectedActorForCreator != null)
                && (this.selectedIntegrationProfileOptionForCreator != null)) {
            if (creatorDescription == null) {
                creatorDescription = "";
            }
            ActorIntegrationProfileOption AIPO = ActorIntegrationProfileOption.getActorIntegrationProfileOption(
                    this.selectedActorForCreator, this.selectedIntegrationProfileForCreator,
                    this.selectedIntegrationProfileOptionForCreator);
            ObjectCreator objectCreator = new ObjectCreator(creatorDescription, AIPO, this.selectedObjectType);
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            entityManager.merge(objectCreator);
            entityManager.flush();
            FacesMessages.instance().add(StatusMessage.Severity.INFO, "#{messages['gazelle.testmanagement.object.objectcreatorsaved']}");
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "#{messages['gazelle.testmanagement.object.youmustchoosepao']}");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void resetSelectionValuesForCreator(int i) {
        LOG.debug("resetSelectionValuesForCreator");
        if (i == 1) {
            selectedIntegrationProfileForCreator = null;
            selectedActorForCreator = null;
            selectedIntegrationProfileOptionForCreator = null;
        }
        if (i == 2) {
            selectedActorForCreator = null;
            selectedIntegrationProfileOptionForCreator = null;
        }
        if (i == 3) {
            selectedIntegrationProfileOptionForCreator = null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Restrict("#{s:hasPermission('ObjectTypeManager', 'addNewReaderOfcurrentObjectType', null)}")
    public void addNewReaderOfcurrentObjectType() {
        LOG.debug("addNewReaderOfcurrentObjectType");
        if ((this.selectedIntegrationProfileForReader != null) && (this.selectedActorForReader != null)
                && (this.selectedIntegrationProfileOptionForReader != null)) {
            if (this.readerDescription == null) {
                this.readerDescription = "";
            }
            ActorIntegrationProfileOption AIPO = ActorIntegrationProfileOption.getActorIntegrationProfileOption(
                    this.selectedActorForReader, this.selectedIntegrationProfileForReader,
                    this.selectedIntegrationProfileOptionForReader);
            ObjectReader objectReader = new ObjectReader(this.readerDescription, AIPO, this.selectedObjectType);
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            entityManager.merge(objectReader);
            entityManager.flush();

            FacesMessages.instance().add(StatusMessage.Severity.INFO, "#{messages['gazelle.testmanagement.object.newobjreadersaved']}");
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "#{messages['gazelle.testmanagement.object.youmustchoosepao']}");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void resetSelectionValuesForReader(int i) {
        LOG.debug("resetSelectionValuesForReader");
        if (i == 1) {
            selectedIntegrationProfileForReader = null;
            selectedActorForReader = null;
            selectedIntegrationProfileOptionForReader = null;
        }
        if (i == 2) {
            selectedActorForReader = null;
            selectedIntegrationProfileOptionForReader = null;
        }
        if (i == 3) {
            selectedIntegrationProfileOptionForReader = null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Restrict("#{s:hasPermission('ObjectTypeManager', 'getPossibleFileTypes', null)}")
    public List<ObjectFileType> getPossibleFileTypes() {
        LOG.debug("getPossibleFileTypes");
        if (selectedObjectType != null) {
            if (selectedObjectType.getId() != null) {
                return ObjectFileType.getListOfAllObjectFileType();
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Restrict("#{s:hasPermission('ObjectTypeManager', 'addNewObjectFileTypeToObjectType', null)}")
    public void addNewObjectFileTypeToObjectType() {
        LOG.debug("addNewObjectFileTypeToObjectType");
        if (selectedFileType != null) {
            if (selectedObjectType.getId() != null) {
                if (selectedFileMaximum < selectedFileMinimum) {
                    FacesMessages.instance().add(StatusMessage.Severity.ERROR, "#{messages['gazelle.testmanagement.object.maxvaluebiggermin']}");
                } else {
                    if (selectedFileDescription == null) {
                        selectedFileDescription = "";
                    }
                    ObjectFile objectFile = new ObjectFile(selectedObjectType, selectedFileDescription,
                            selectedFileType, selectedFileMinimum, selectedFileMaximum, null);
                    EntityManager entityManager = EntityManagerService.provideEntityManager();
                    entityManager.merge(objectFile);
                    entityManager.flush();
                    resetSelectionValuesForFileType();
                    FacesMessages.instance().add(StatusMessage.Severity.INFO, "#{messages['gazelle.testmanagement.object.newobjfilesaved']}");
                }
            }
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "#{messages['gazelle.testmanagement.object.probincreationobjfile']}");
        }
    }

    /**
     * Reset selection values for file type.
     */
    private void resetSelectionValuesForFileType() {
        selectedFileType = null;
        selectedFileDescription = null;
        selectedFileMinimum = 1;
        selectedFileMaximum = 1;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Restrict("#{s:hasPermission('ObjectTypeManager', 'addNewObjectAttributeToObjectType', null)}")
    public void addNewObjectAttributeToObjectType() {
        LOG.debug("addNewObjectAttributeToObjectType");
        if (this.createdAttributeType != null) {
            if (this.createdAttributeDescription == null) {
                this.createdAttributeDescription = "";
            }
            ObjectAttribute objectAttribute = new ObjectAttribute(this.selectedObjectType, this.createdAttributeType,
                    this.createdAttributeDescription);
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            entityManager.merge(objectAttribute);
            entityManager.flush();
            resetSelectionValuesForAttributeType();
            FacesMessages.instance().add(StatusMessage.Severity.INFO, "#{messages['gazelle.testmanagement.object.newobjattrsaved']}");
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "#{messages['gazelle.testmanagement.object.problemchooseattrtype']}");
        }
    }

    /**
     * Reset selection values for attribute type.
     */
    private void resetSelectionValuesForAttributeType() {
        this.createdAttributeType = null;
        this.createdAttributeDescription = null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Restrict("#{s:hasPermission('ObjectTypeManager', 'updateSelectedObjectType', null)}")
    public void updateSelectedObjectType() {
        LOG.debug("updateSelectedObjectType");
        if (selectedObjectType != null) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            selectedObjectType = entityManager.merge(selectedObjectType);
            entityManager.flush();
            FacesMessages.instance().add(StatusMessage.Severity.INFO, "#{messages['gazelle.testmanagement.object.selectedobjtypeupdated']}");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Restrict("#{s:hasPermission('ObjectTypeManager', 'getNumberOfObjectInstanceByObjectType', null)}")
    public String getNumberOfObjectInstanceByObjectTypeForCurrentTestingSession(ObjectType objectType) {
        LOG.debug("getNumberOfObjectInstanceByObjectTypeForCurrentTestingSession");
        String result = "";
        TestingSession testingSession = testingSessionService.getUserTestingSession();
        List<ObjectInstance> listObjectInstance = ObjectInstance.getObjectInstanceFiltered(objectType, null,
                testingSession);
        if (listObjectInstance != null) {
            result = String.valueOf(listObjectInstance.size());
            return result;
        } else {
            return "None";
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Restrict("#{s:hasPermission('ObjectTypeManager', 'getNumberOfObjectInstanceByObjectType', null)}")
    public String getNumberOfObjectInstanceByObjectType(ObjectType objectType) {
        LOG.debug("getNumberOfObjectInstanceByObjectType");
        String result = "";
        List<ObjectInstance> listObjectInstance = ObjectInstance.getObjectInstanceFiltered(objectType, null, null);
        if (listObjectInstance != null) {
            result = String.valueOf(listObjectInstance.size());
            return result;
        } else {
            return "None";
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<SystemInSession> getListSystemInSessionOfAIPO(ActorIntegrationProfileOption AIPO) {
        LOG.debug("getListSystemInSessionOfAIPO");
        TestingSession testingSession = testingSessionService.getUserTestingSession();
        EntityManager em = EntityManagerService.provideEntityManager();
        return SystemInSession.getSystemInSessionFiltered(em, null, testingSession, null, null, null, null, null, AIPO,
                null, null, null, null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public GazelleTreeNodeImpl<Object> getTreeNode() {
        LOG.debug("getTreeNode");
        GazelleTreeNodeImpl<Object> rootNode = new GazelleTreeNodeImpl<Object>();
        List<ObjectCreator> listOC = this.getListOfObjectCreatorForSelectedObjectType();
        int i = 0;
        int j;
        for (ObjectCreator objectCreator : listOC) {
            GazelleTreeNodeImpl<Object> AIPOTreeNode = new GazelleTreeNodeImpl<Object>();
            AIPOTreeNode.setData(objectCreator.getAIPO().getId());
            List<SystemInSession> listSIS = this.getListSystemInSessionOfAIPO(objectCreator.getAIPO());
            j = 0;
            for (SystemInSession SIS : listSIS) {
                GazelleTreeNodeImpl<Object> AIPOTreeNodeChild = new GazelleTreeNodeImpl<Object>();
                AIPOTreeNodeChild.setData(SIS);
                AIPOTreeNode.addChild(j, AIPOTreeNodeChild);
                j++;
            }
            rootNode.addChild(i, AIPOTreeNode);
            i++;
        }
        return rootNode;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Restrict("#{s:hasPermission('ObjectTypeManager', 'getNumberOfObjectInstanceBySISForSelectedObjectType', null)}")
    public String getNumberOfObjectInstanceBySISForSelectedObjectType(SystemInSession SIS) {
        LOG.debug("getNumberOfObjectInstanceBySISForSelectedObjectType");
        String result = "";
        TestingSession testingSession = testingSessionService.getUserTestingSession();
        List<ObjectInstance> listObjectInstance = ObjectInstance.getObjectInstanceFiltered(selectedObjectType, SIS,
                testingSession);
        int nbre = 0;
        if (listObjectInstance != null) {
            nbre = listObjectInstance.size();
            result = String.valueOf(nbre);
            return result;
        } else {
            return "0";
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Restrict("#{s:hasPermission('ObjectTypeManager', 'getListSISofSelectedObjectTypeForCreation', null)}")
    public List<SystemInSession> getListSISofSelectedObjectTypeForCreation() {
        LOG.debug("getListSISofSelectedObjectTypeForCreation");
        List<SystemInSession> result = new ArrayList<SystemInSession>();
        if (this.selectedObjectType != null) {
            List<ObjectCreator> listOC = this.getListOfObjectCreatorForSelectedObjectType();
            for (ObjectCreator objectCreator : listOC) {
                List<SystemInSession> listSIS = this.getListSystemInSessionOfAIPO(objectCreator.getAIPO());
                for (SystemInSession SIS : listSIS) {
                    if (!result.contains(SIS)
                            && (SIS.getRegistrationStatus() == null || !SIS.getRegistrationStatus().equals(
                            SystemInSessionRegistrationStatus.DROPPED))) {
                        result.add(SIS);
                    }
                }
            }
            Collections.sort(result);
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Restrict("#{s:hasPermission('ObjectTypeManager', 'getTableOfSIS', null)}")
    public String getTableOfSIS(SystemInSession systemInSession) {
        LOG.debug("getTableOfSIS");
        String result = "";
        if (systemInSession != null) {
            if (systemInSession.getTableSession() != null) {
                result = systemInSession.getTableSession().getTableKeyword();
            }
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Restrict("#{s:hasPermission('ObjectTypeManager', 'getListOfAIPOCreatorImplementedBySISForSelectedObjectType', null)}")
    public List<ActorIntegrationProfileOption> getListOfAIPOCreatorImplementedBySISForSelectedObjectType(
            SystemInSession systemInSession) {
        List<ActorIntegrationProfileOption> result = new ArrayList<ActorIntegrationProfileOption>();
        List<ObjectCreator> listOC = this.getListOfObjectCreatorForSelectedObjectType();
        for (ObjectCreator objectCreator : listOC) {
            List<SystemInSession> listSIS = this.getListSystemInSessionOfAIPO(objectCreator.getAIPO());
            if (listSIS.contains(systemInSession)) {
                result.add(objectCreator.getAIPO());
            }
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Restrict("#{s:hasPermission('ObjectTypeManager', 'getListSISofSelectedObjectTypeForReading', null)}")
    public List<SystemInSession> getListSISofSelectedObjectTypeForReading() {
        LOG.debug("getListSISofSelectedObjectTypeForReading");
        List<SystemInSession> result = new ArrayList<SystemInSession>();
        if (this.selectedObjectType != null) {
            List<ObjectReader> listOR = this.getListOfObjectReaderForSelectedObjectType();
            for (ObjectReader objectReader : listOR) {
                List<SystemInSession> listSIS = this.getListSystemInSessionOfAIPO(objectReader.getAIPO());
                for (SystemInSession SIS : listSIS) {
                    if (!result.contains(SIS)
                            && (SIS.getRegistrationStatus() == null || !SIS.getRegistrationStatus().equals(
                            SystemInSessionRegistrationStatus.DROPPED))) {
                        result.add(SIS);
                    }
                }
            }
        }
        Collections.sort(result);
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Restrict("#{s:hasPermission('ObjectTypeManager', 'getListOfAIPOReaderImplementedBySISForSelectedObjectType', null)}")
    public List<ActorIntegrationProfileOption> getListOfAIPOReaderImplementedBySISForSelectedObjectType(
            SystemInSession systemInSession) {
        List<ActorIntegrationProfileOption> result = new ArrayList<ActorIntegrationProfileOption>();
        List<ObjectReader> listOR = this.getListOfObjectReaderForSelectedObjectType();
        for (ObjectReader objectReader : listOR) {
            List<SystemInSession> listSIS = this.getListSystemInSessionOfAIPO(objectReader.getAIPO());
            if (listSIS.contains(systemInSession)) {
                result.add(objectReader.getAIPO());
            }
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void inisializeSelectedObjectType() {
        LOG.debug("inisializeSelectedObjectType");
        ObjectTypeStatus inObjectTypeStatus = ObjectTypeStatus.getSTATUS_READY();
        this.selectedObjectType = new ObjectType("", "", "", "", inObjectTypeStatus);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Restrict("#{s:hasPermission('ObjectTypeManager', 'mergeSelectedObjectType', null)}")
    public void mergeSelectedObjectType() {
        LOG.debug("mergeSelectedObjectType");
        if (selectedObjectType != null) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            selectedObjectType = entityManager.merge(selectedObjectType);
            entityManager.flush();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Restrict("#{s:hasPermission('ObjectTypeManager', 'mergeObjectType', null)}")
    public void mergeObjectType(ObjectType inObjectType) {
        LOG.debug("mergeObjectType");
        if (inObjectType != null) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            entityManager.merge(inObjectType);
            entityManager.flush();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ObjectTypeStatus> getPossibleObjectTypeStatus() {
        LOG.debug("getPossibleObjectTypeStatus");
        return ObjectTypeStatus.getListStatus();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Restrict("#{s:hasPermission('ObjectTypeManager', 'updateSelectedCreatorOfcurrentObjectType', null)}")
    public void updateSelectedCreatorOfcurrentObjectType() {
        LOG.debug("updateSelectedCreatorOfcurrentObjectType");
        if ((selectedCreator != null) && (selectedObjectType != null)) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            selectedCreator = entityManager.merge(selectedCreator);
            entityManager.flush();
            FacesMessages.instance().add(StatusMessage.Severity.INFO, "#{messages['gazelle.testmanagement.object.objcreatorupdated']}");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Restrict("#{s:hasPermission('ObjectTypeManager', 'updateSelectedReaderOfcurrentObjectType', null)}")
    public void updateSelectedReaderOfcurrentObjectType() {
        LOG.debug("updateSelectedReaderOfcurrentObjectType");
        if ((selectedReader != null) && (selectedObjectType != null)) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            selectedReader = entityManager.merge(selectedReader);
            entityManager.flush();
            FacesMessages.instance().add(StatusMessage.Severity.INFO, "#{messages['gazelle.testmanagement.object.objreaderupdatd']}");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Restrict("#{s:hasPermission('ObjectTypeManager', 'mergeObjectFileTypeOfObjectType', null)}")
    public void mergeObjectFileTypeOfObjectType() {
        LOG.debug("mergeObjectFileTypeOfObjectType");
        if ((selectedObjectFile != null) && (selectedObjectType != null)) {
            if (selectedObjectFile.getType() != null) {
                if (selectedObjectFile.getDescription().length() > 255) {
                    FacesMessages.instance().add(StatusMessage.Severity.ERROR, "#{messages['gazelle.testmanagement.object.probdescfilesolong']}");
                } else if (selectedObjectFile.getMax() >= selectedObjectFile.getMin()) {
                    EntityManager entityManager = EntityManagerService.provideEntityManager();
                    selectedObjectFile.setObject(this.selectedObjectType);
                    selectedObjectFile = entityManager.merge(selectedObjectFile);
                    entityManager.flush();
                    FacesMessages.instance().add(StatusMessage.Severity.INFO, "#{messages['gazelle.testmanagement.object.objfilesaved']}");
                } else {
                    FacesMessages.instance().add(StatusMessage.Severity.ERROR, "#{messages['gazelle.testmanagement.object.maxvaluebiggermin']}");
                }
            } else {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "#{messages['gazelle.testmanagement.object.typeoffilenotspecified']}");
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Restrict("#{s:hasPermission('ObjectTypeManager', 'mergeObjectAttributeOfObjectType', null)}")
    public void mergeObjectAttributeOfObjectType() {
        LOG.debug("mergeObjectAttributeOfObjectType");
        if ((selectedObjectAttribute != null) && (selectedObjectType != null)) {
            if (selectedObjectAttribute.getKeyword().equals("")) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "#{messages['gazelle.testmanagement.object.youmustspecifykeyword']}");
            } else {
                EntityManager entityManager = EntityManagerService.provideEntityManager();
                selectedObjectAttribute.setObject(this.selectedObjectType);
                selectedObjectAttribute = entityManager.merge(selectedObjectAttribute);
                entityManager.flush();
                FacesMessages.instance().add(StatusMessage.Severity.INFO, "#{messages['gazelle.testmanagement.object.attributesaved']}");
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initializeSelectedObjectFile() {
        LOG.debug("initializeSelectedObjectFile");
        this.selectedObjectFile = new ObjectFile();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Restrict("#{s:hasPermission('ObjectTypeManager', 'getListObjectInstanceForSelectedObjectType', null)}")
    public List<ObjectInstance> getListObjectInstanceForSelectedObjectType() {
        LOG.debug("getListObjectInstanceForSelectedObjectType");
        if (this.selectedObjectType == null) {
            return null;
        }
        if (this.selectedObjectType.getId() == null) {
            return null;
        }
        if (this.selectedObjectType.getId() == 0) {
            return null;
        }
        return this.getListObjectInstanceForObjectType(this.selectedObjectType);
    }

    /**
     * {@inheritDoc}
     */
    @Restrict("#{s:hasPermission('ObjectTypeManager', 'getListObjectInstanceForObjectType', null)}")
    private List<ObjectInstance> getListObjectInstanceForObjectType(ObjectType objectType) {
        List<ObjectInstance> listObjectInstance = new ArrayList<ObjectInstance>();
        if (objectType != null) {
            if (objectType.getId() != 0) {
                listObjectInstance = ObjectInstance.getObjectInstanceFiltered(objectType, null, null);
            }
        }
        return listObjectInstance;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initializeSelectedObjectAttribute() {
        LOG.debug("initializeSelectedObjectAttribute");
        this.selectedObjectAttribute = new ObjectAttribute();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ObjectInstanceFile> getListObjectInstanceFileForSelectedObjectFile() {
        LOG.debug("getListObjectInstanceFileForSelectedObjectFile");
        List<ObjectInstanceFile> result = new ArrayList<ObjectInstanceFile>();
        if (this.selectedObjectFile == null) {
            return result;
        }
        if (this.selectedObjectFile.getId() == null) {
            return result;
        }
        if (this.selectedObjectFile.getId() == 0) {
            return result;
        }

        try {
            result = ObjectInstanceFile.getObjectInstanceFileFiltered(null, selectedObjectFile, null, null, null);
        } catch (Exception e) {
            e.getStackTrace();
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Restrict("#{s:hasPermission('ObjectTypeManager', 'viewListOIFOnDeleting', null)}")
    public boolean viewListOIFOnDeleting() {
        LOG.debug("viewListOIFOnDeleting");
        List<ObjectInstanceFile> listOIF;
        try {
            listOIF = this.getListObjectInstanceFileForSelectedObjectFile();
        } catch (Exception e) {
            return false;
        }
        if (listOIF.size() > 0) {
            return true;
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Restrict("#{s:hasPermission('ObjectTypeManager', 'editObjectType', null)}")
    public String editObjectType(ObjectType inObjectType) {
        LOG.debug("editObjectType");
        this.setSelectedObjectType(inObjectType);
        return "/objects/editObjectType.xhtml";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getNumberOfObjectInstanceToReadBySIS(SystemInSession inSystemInSession) {
        LOG.debug("getNumberOfObjectInstanceToReadBySIS");
        int result = 0;
        TestingSession testingSession = testingSessionService.getUserTestingSession();
        List<ObjectType> list_OT_OnR = this.getListOfObjectTypeForCurrentSessionOnReading(inSystemInSession);
        List<ObjectInstance> list_OI_tmp;
        for (ObjectType inObjectType : list_OT_OnR) {
            list_OI_tmp = ObjectInstance.getObjectInstanceFiltered(inObjectType, null, testingSession);
            for (ObjectInstance inObjectInstance : list_OI_tmp) {
                if (inObjectInstance.isCompleted()) {
                    result++;
                }
            }
        }
        return String.valueOf(result);
    }

    /**
     * Get list of object type for current session.
     *
     * @param inSystemInSession System in session
     * @return the list of corresponding {@link ObjectType}
     */
    private List<ObjectType> getListOfObjectTypeForCurrentSessionOnReading(SystemInSession inSystemInSession) {
        List<ObjectType> listOT = new ArrayList<ObjectType>();
        if (inSystemInSession != null) {
            listOT = ObjectType.getObjectTypeFiltered(null, null, null, inSystemInSession.getSystem());
        }
        return listOT;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getNumberOfCompletedObjectInstanceBySISForSelectedObjectType(SystemInSession inSystemInSession) {
        LOG.debug("getNumberOfCompletedObjectInstanceBySISForSelectedObjectType");
        if (selectedObjectType != null) {
            return this.getNumberOfCompletedObjectInstanceBySISForSelectedObjectType(inSystemInSession,
                    selectedObjectType);
        }
        return "0";
    }

    /**
     * Get the number of completed object instance for a system per object type.
     *
     * @param inSystemInSession system in session
     * @param inObjectType      object type
     * @return the literal representation of the number.
     */
    private String getNumberOfCompletedObjectInstanceBySISForSelectedObjectType(SystemInSession inSystemInSession,
                                                                                ObjectType inObjectType) {
        int result = 0;
        List<ObjectInstance> listObjectInstance = ObjectInstance.getObjectInstanceFiltered(null, inSystemInSession,
                null);
        if (listObjectInstance != null) {
            for (ObjectInstance objectInstance : listObjectInstance) {
                if ((objectInstance.getObject().equals(inObjectType)) && (objectInstance.isCompleted())) {
                    result++;
                }
            }
        }
        return String.valueOf(result);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Domain> getPossibleDomains() {
        LOG.debug("getPossibleDomains");
        return Domain.getPossibleDomains();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<IntegrationProfile> getPossibleIntegrationProfiles(Domain inDomain) {
        LOG.debug("getPossibleIntegrationProfiles");
        return IntegrationProfile.getPossibleIntegrationProfiles(inDomain);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void getListOfObjectTypeByDataModel() {
        LOG.debug("getListOfObjectTypeByDataModel");
        foundObjectTypes = new ObjectTypeDataModel(selectedObjectTypeStatus);
    }

    // ~delete methods ////////////////////////////////////////////////////////////////

    /**
     * {@inheritDoc}
     */
    @Override
    @Restrict("#{s:hasPermission('ObjectTypeManager', 'deleteSelectedObjectTypeFromDataBaseForSelectedObjectType', null)}")
    public void deleteSelectedObjectTypeFromDataBaseForSelectedObjectType() {
        LOG.debug("deleteSelectedObjectTypeFromDataBaseForSelectedObjectType");
        ObjectType.deleteObjectType(this.selectedObjectType);
        FacesMessages.instance().add(StatusMessage.Severity.INFO, "#{messages['gazelle.testmanagement.object.objtypeselecteddeleted']}");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Restrict("#{s:hasPermission('ObjectTypeManager', 'deleteSelectedCreatorFromDataBase', null)}")
    public void deleteSelectedCreatorFromDataBase() {
        LOG.debug("deleteSelectedCreatorFromDataBase");
        ObjectCreator.deleteObjectCreator(this.selectedCreator);
        FacesMessages.instance().add(StatusMessage.Severity.INFO, "#{messages['gazelle.testmanagement.object.typeofcreatorselecteddeleted']}");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Restrict("#{s:hasPermission('ObjectTypeManager', 'deleteSelectedReaderFromDataBase', null)}")
    public void deleteSelectedReaderFromDataBase() {
        LOG.debug("deleteSelectedReaderFromDataBase");
        ObjectReader.deleteObjectReader(this.selectedReader);
        FacesMessages.instance().add(StatusMessage.Severity.INFO, "#{messages['gazelle.testmanagement.object.typeofreaderspecifieddeleted']}");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Restrict("#{s:hasPermission('ObjectTypeManager', 'deleteSelectedObjectFileFromDataBase', null)}")
    public void deleteSelectedObjectFileFromDataBase() {
        LOG.debug("deleteSelectedObjectFileFromDataBase");
        ObjectFile.deleteObjectFile(this.selectedObjectFile);
        FacesMessages.instance().add(StatusMessage.Severity.INFO, "#{messages['gazelle.testmanagement.object.typeoffilespecifieddeleted']}");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Restrict("#{s:hasPermission('ObjectTypeManager', 'deleteSelectedObjectAttributeFromDataBase', null)}")
    public void deleteSelectedObjectAttributeFromDataBase() {
        LOG.debug("deleteSelectedObjectAttributeFromDataBase");
        ObjectAttribute.deleteObjectAttribute(this.selectedObjectAttribute);
        FacesMessages.instance().add(StatusMessage.Severity.INFO, "#{messages['gazelle.testmanagement.object.attrselecteddeleted']}");
    }

    // Import/export methods ////////////////////////////////////////////////////////////

    /**
     * Listener for file upload event. Will update file description and content properties.
     *
     * @param event file upload event.
     */
    public void uploadListener(final FileUploadEvent event) {
        final UploadedFile uploadedFile = event.getUploadedFile();
        if (uploadedFile.getData() != null && uploadedFile.getData().length > 0) {
            setUploadedFileContent(new String(uploadedFile.getData(), StandardCharsets.UTF_8));
            setDescription(uploadedFile.getName());
        } else {
            FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.ERROR, "File is empty");
        }
    }

    /**
     * Reset the upload.
     */
    public void resetUpload() {
        this.uploadedFileContent = null;
        this.description = null;
    }

    /**
     * Reset the import.
     */
    public void resetImport() {
        resetUpload();
        this.objectTypeToImport = null;
    }

    /**
     * Export sample types as XML.
     */
    public void exportSamplesTypeAsXml() {
        try {
            TFModelExporter.exportObjectType(getFoundObjectTypes().getAllItems(FacesContext.getCurrentInstance()));
        } catch (JAXBException e) {
            LOG.error("Error exporting Samples' Types as xml : ", e);
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Error converting Samples' Types to xml");
        }
    }

    /**
     * Import samples from XML.
     */
    public void importSamplesFromXml() {
        SamplesTypeToXmlConverter samplesTypeToXmlConverter = new SamplesTypeToXmlConverter();
        try {
            setObjectTypeToImport(samplesTypeToXmlConverter.extractAndCheckObjectTypes(getUploadedFileContent()));
            if (!this.reviewBeforeSaving) {
                persistImportedSamples();
            }
        } catch (JAXBException e) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Error importing configurations with message :\n\"" +
                    e.getMessage() + "\" Please check your file.");
        }
    }

    /**
     * Persist Imported Rules
     */
    public void persistImportedSamples() {
        TFPersistenceManager tfPersistenceManager = new TFPersistenceManager();
        int numberOfImportedRules = tfPersistenceManager.addSamplesType(objectTypeToImport);

        objectTypeToImport.setImported(true);
        FacesMessages.instance().add(StatusMessage.Severity.INFO, "Successfully imported " +
                numberOfImportedRules + " Samples' Type !");
        getListOfObjectTypeByDataModel();
    }
}
