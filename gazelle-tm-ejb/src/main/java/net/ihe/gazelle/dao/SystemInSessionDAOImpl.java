package net.ihe.gazelle.dao;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManagerImpl;
import net.ihe.gazelle.common.log.ExceptionLogging;
import net.ihe.gazelle.common.model.PathLinkingADocument;
import net.ihe.gazelle.common.util.File;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import net.ihe.gazelle.ssov7.gum.client.interlay.factory.UserServiceFactory;
import net.ihe.gazelle.tm.session.TestingSessionService;
import net.ihe.gazelle.tm.systems.model.System;
import net.ihe.gazelle.tm.systems.model.SystemInSession;
import net.ihe.gazelle.tm.systems.model.SystemInSessionQuery;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import org.apache.commons.io.IOUtils;
import org.jboss.seam.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by gthomazon on 17/07/17.
 */
public class SystemInSessionDAOImpl implements SystemInSessionDAO {

    private static final Logger LOG = LoggerFactory.getLogger(SystemInSessionDAOImpl.class);
    private final ApplicationPreferenceManager applicationPreferenceManager = new ApplicationPreferenceManagerImpl();

    @Override
    public String getHl7ConformanceStatementsPath() {
        return applicationPreferenceManager.getGazelleHL7ConformanceStatementsPath();
    }

    @Override
    public String getDicomConformanceStatementsPath() {
        return applicationPreferenceManager.getGazelleDicomConformanceStatementsPath();
    }


    /**
     * This method retrieves HL7 Conformance Statements for a selected system. This is called when to get that objects/files before editing/viewing
     * a system
     */
    @Override
    public ArrayList<File> retrieveHL7ConformanceStatementForSelectedSystemToViewOrEdit(SystemInSession systemInSession) {
        LOG.debug("retrieveHL7ConformanceStatementForSelectedSystemToViewOrEdit");
        return findDocumentsFilesForSelectedSystem(applicationPreferenceManager.getGazelleHL7ConformanceStatementsPath(), systemInSession.getSystem(), systemInSession.getSystem()
                .getPathsToHL7Documents());
    }

    /**
     * This method retrieves HL7 Conformance Statements for a selected system. This is called when to get that objects/files before editing/viewing
     * a system
     */
    @Override
    public ArrayList<File> retrieveDicomConformanceStatementForSelectedSystemToViewOrEdit(SystemInSession
                                                                                                  systemInSession) {
        LOG.debug("retrieveDicomConformanceStatementForSelectedSystemToViewOrEdit");
        return findDocumentsFilesForSelectedSystem(applicationPreferenceManager.getGazelleDicomConformanceStatementsPath(), systemInSession.getSystem(), systemInSession.getSystem()
                .getPathsToDicomDocuments());

    }

    private ArrayList<File> findDocumentsFilesForSelectedSystem(String fileServerPath, System system, List<PathLinkingADocument>
            systemPathsToFileDocuments) {
        ArrayList<File> documentsFilesForSelectedSystem = new ArrayList<>();
        if (systemPathsToFileDocuments != null) {
            for (int iElements = 0; iElements < systemPathsToFileDocuments.size(); iElements++) {
                String pathname = fileServerPath + java.io.File.separatorChar
                        + system.getId() + java.io.File.separatorChar
                        + systemPathsToFileDocuments.get(iElements).getPath();
                java.io.File f = new java.io.File(pathname);
                try {
                    File file = new File(pathname, IOUtils.toByteArray(new FileInputStream
                            (f)));
                    documentsFilesForSelectedSystem.add(file);
                } catch (FileNotFoundException e) {
                    ExceptionLogging.logException(e, LOG);
                } catch (IOException e) {
                    ExceptionLogging.logException(e, LOG);
                }
            }
        }
        return documentsFilesForSelectedSystem;
    }

    @Override
    public boolean isSystemInSessionRelatedToUser(SystemInSession systemInSession, String username) {
        List<SystemInSession> listSis = getSystemInSessionRelatedToUser(username,
                getTestingSessionService().getUserTestingSession());
        return listSis.contains(systemInSession);
    }

    @Override
    public List<SystemInSession> getSystemInSessionRelatedToUser(String username, TestingSession ts) {
        if (username == null) {
            return new ArrayList<>();
        }
        SystemInSessionQuery q = new SystemInSessionQuery();
        UserService userService = UserServiceFactory.getUserService();
        q.system().institutionSystems().institution().keyword()
                .eq(userService.getUserById(username).getOrganizationId());
        if (ts != null) {
            q.testingSession().eq(ts);
        }
        return q.getList();
    }

    private static TestingSessionService getTestingSessionService() {
        return (TestingSessionService) Component.getInstance("testingSessionService");
    }
}
