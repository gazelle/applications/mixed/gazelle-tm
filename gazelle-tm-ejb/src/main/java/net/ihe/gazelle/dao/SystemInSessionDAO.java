package net.ihe.gazelle.dao;

import net.ihe.gazelle.common.util.File;
import net.ihe.gazelle.tm.systems.model.SystemInSession;
import net.ihe.gazelle.tm.systems.model.TestingSession;

import java.util.ArrayList;
import java.util.List;

public interface SystemInSessionDAO {
    String getHl7ConformanceStatementsPath();

    String getDicomConformanceStatementsPath();

    ArrayList<File> retrieveHL7ConformanceStatementForSelectedSystemToViewOrEdit(SystemInSession systemInSession);

    ArrayList<File> retrieveDicomConformanceStatementForSelectedSystemToViewOrEdit(SystemInSession systemInSession);

    boolean isSystemInSessionRelatedToUser(SystemInSession systemInSession, String username);

    List<SystemInSession> getSystemInSessionRelatedToUser(String username, TestingSession ts);
}
