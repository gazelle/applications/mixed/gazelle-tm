package net.ihe.gazelle.IO;

import net.ihe.gazelle.common.fineuploader.FineuploaderListener;
import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import net.ihe.gazelle.tm.datamodel.TestDataModel;
import net.ihe.gazelle.tm.gazelletest.model.definition.Test;
import net.ihe.gazelle.tm.gazelletest.model.definition.TestQuery;
import org.apache.commons.io.IOUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Transactional;
import org.jboss.seam.async.QuartzDispatcher;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBException;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Bean than manages Tests import and export.
 */
@Name("testIOManager")
@Scope(ScopeType.CONVERSATION)
@GenerateInterface("TestIOManagerLocal")
public class TestIOManager implements TestIOManagerLocal, Serializable, FineuploaderListener {

    private static final long serialVersionUID = 2740330132656079658L;
    private static final Logger LOG = LoggerFactory.getLogger(TestIOManager.class);
    private String filePath = "";
    private boolean deleteImportedFile = false;
    private String fileName;

    @In(value = "gumUserService")
    private UserService userService;

    @In(create = true, value = "TestImporterJob")
    private TestImporterJob testImporterJob;

    @In
    private GazelleIdentity identity;

    /**
     * Export a single test as XML.
     *
     * @param currentTest test to export.
     * @throws IOException if the export is not successful
     */
    public void export(Test currentTest) throws IOException {
        LOG.debug("export");
        TestsExporter testExporter = new TestsExporter();
        try {
            fileName = currentTest.getKeyword() + "_" + getDate() + ".xml";
            TestQuery testQuery = new TestQuery();
            testQuery.id().eq(currentTest.getId());
            Test test = testQuery.getUniqueResult();
            testExporter.exportTest(test);
            redirectExport(testExporter);
        } catch (JAXBException e) {
            LOG.error("Error marshalling test to xml !", e);
        }
    }

    /**
     * Export all tests filtered in Datatable.
     *
     * @param tests tests to export
     * @throws IOException if the export isn't successful
     */
    public void exportFilteredTests(TestDataModel tests) throws IOException {
        LOG.debug("exportFilteredTests");
        TestsExporter testExporter = new TestsExporter();
        List<Test> testList = tests.getAllItems(FacesContext.getCurrentInstance());
        try {
            testExporter.exportTests(testList);
            redirectExport(testExporter);
        } catch (JAXBException e) {
            LOG.error("Error marshalling test to xml !", e);
        }
    }

    /**
     * Redirect the export of tests.
     *
     * @param testsExporter test exporter.
     * @throws IOException if the export cannot be performed.
     */
    private void redirectExport(TestsExporter testsExporter) throws IOException {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExternalContext externalContext = facesContext.getExternalContext();

        HttpServletResponse response = (HttpServletResponse) externalContext.getResponse();
        ServletOutputStream servletOutputStream = response.getOutputStream();
        response.setContentType("application/xml");
        response.setHeader("Content-Disposition", "attachment; filename=\"" + getFileName() + "\"");

        testsExporter.write(servletOutputStream);
        servletOutputStream.flush();
        servletOutputStream.close();
        facesContext.responseComplete();
    }

    /**
     * Getter for the fileName property.
     *
     * @return the value of the property.
     */
    private String getFileName() {
        if (fileName == null) {
            provideFileName();
        }
        return fileName;
    }

    /**
     * Get the current date as a String.
     *
     * @return literal representation of current date.
     */
    private String getDate() {
        Date date = new Date();
        SimpleDateFormat formater = new SimpleDateFormat("yyyyMMdd-HH:mm:ss");
        return formater.format(date.getTime());
    }

    /**
     * Create a file Name for the XML test.
     */
    private void provideFileName() {
        fileName = "Tests_" + getDate() + ".xml";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public void uploadedFile(File uploadFile, String filename, String id, String param) {
        LOG.debug("uploadedFile");
        if ("testsToUpload".equals(id)) {
            File tmpFile = new File("");
            try {
                tmpFile = createTmpFileFromFile(uploadFile);
            } catch (IOException e) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, e.getMessage());
                return;
            }
            testImporterJob.importXmlTests(tmpFile.getAbsolutePath(), true, userService.getUserById(identity.getUsername()));
            if (uploadFile.delete()) {
                LOG.info("tmpFile deleted");
            } else {
                LOG.error("Failed to delete tmpFile");
            }
        }
    }

    /**
     * Perform the import.
     */
    public void importAction() {
        LOG.debug("importAction");
        if (!"".equals(getFilePath())) {
            QuartzDispatcher.instance().scheduleAsynchronousEvent("importXmlTests", getFilePath(),
                    isDeleteImportedFile(), userService.getUserById(identity.getUsername()));
        }
    }

    /**
     * Getter for the filePath property.
     *
     * @return the value of the property.
     */
    public String getFilePath() {
        LOG.debug("getFilePath");
        return filePath;
    }

    /**
     * Setter for the filePath property.
     *
     * @param filePath the value to set to the property.
     */
    public void setFilePath(String filePath) {
        LOG.debug("setFilePath");
        this.filePath = filePath;
    }

    /**
     * Getter for the deleteImportedFile property.
     *
     * @return the value of the property.
     */
    public boolean isDeleteImportedFile() {
        LOG.debug("isDeleteImportedFile");
        return deleteImportedFile;
    }

    /**
     * Setter for the deleteImportedFile property.
     *
     * @param deleteImportedFile value to set to the property.
     */
    public void setDeleteImportedFile(boolean deleteImportedFile) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("setDeleteImportedFile");
        }
        this.deleteImportedFile = deleteImportedFile;
    }

    /**
     * Create a temporary file from an existing file.
     *
     * @param file file to copy in a temporary file.
     * @return the temporary file.
     */
    private File createTmpFileFromFile(final File file) throws IOException {
        FileOutputStream fos = null;
        File tempFile = null;
        try {
            tempFile = File.createTempFile(file.getName(), ".tmp");
            fos = new FileOutputStream(tempFile.getAbsolutePath());
            fos.write(IOUtils.toByteArray(new FileInputStream(file)));
            fos.close();
        } catch (final IOException e) {
            LOG.error("Error creating temp file : ", e);
            IOException tempFileException = new IOException("Error creating temp file : ", e);
            throw tempFileException;
        } finally {
            IOUtils.closeQuietly(fos);
        }
        return tempFile;
    }
}
