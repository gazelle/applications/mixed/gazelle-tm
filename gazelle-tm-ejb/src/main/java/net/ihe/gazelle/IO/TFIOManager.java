package net.ihe.gazelle.IO;


import net.ihe.gazelle.common.fineuploader.FineuploaderListener;
import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.tf.model.IntegrationProfile;
import net.ihe.gazelle.tf.model.IntegrationProfileQuery;
import net.ihe.gazelle.tm.gazelletest.model.definition.Test;
import net.ihe.gazelle.tm.gazelletest.model.definition.TestQuery;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import javax.persistence.EntityManager;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

//TODO Finish this implementation
@Name("tfIOManager")
@Scope(ScopeType.CONVERSATION)
@GenerateInterface("TfIOManagerLocal")
public class TFIOManager implements TfIOManagerLocal, Serializable, FineuploaderListener {

    @In
    private EntityManager entityManager;

    private List<IntegrationProfile> integrationProfiles;
    private List<IntegrationProfile> selectedIntegrationProfiles;

    /**
     * Retrieve the complete list of integration profiles registered in the tool.
     */
    @Create
    public void listIntegrationProfiles() {
        IntegrationProfileQuery query = new IntegrationProfileQuery(entityManager);
        this.integrationProfiles = query.getList();
    }

    /**
     * Getter for the integrationProfiles property
     *
     * @return the value of the property.
     */
    public List<IntegrationProfile> getIntegrationProfiles() {
        return integrationProfiles;
    }

    /**
     * Setter for the integrationProfiles property
     *
     * @param integrationProfiles value to set to the property
     */
    public void setIntegrationProfiles(List<IntegrationProfile> integrationProfiles) {
        if (integrationProfiles != null) {
            this.integrationProfiles = new ArrayList<>(integrationProfiles);
        } else {
            this.integrationProfiles = null;
        }
    }

    /**
     * Getter for the selectedIntegrationProfiles property
     *
     * @return the value of the property.
     */
    public List<IntegrationProfile> getSelectedIntegrationProfiles() {
        return selectedIntegrationProfiles;
    }

    /**
     * Setter for the selectedIntegrationProfiles property.
     *
     * @param selectedIntegrationProfiles value to set to the property.
     */
    public void setSelectedIntegrationProfiles(List<IntegrationProfile> selectedIntegrationProfiles) {
        if (selectedIntegrationProfiles != null) {
            this.selectedIntegrationProfiles = new ArrayList<>(selectedIntegrationProfiles);
        } else {
            this.selectedIntegrationProfiles = null;
        }
    }

    /**
     * Get the list of Tests corresponding to selected Integration Profiles.
     *
     * @return a list of filtered tests.
     */
    public List<Test> getFilteredTests() {
        TestQuery query = new TestQuery(entityManager);
        query.testRoles().roleInTest().testParticipantsList().actorIntegrationProfileOption()
                .actorIntegrationProfile().integrationProfile().in(selectedIntegrationProfiles);
        return query.getList();
    }

    /**
     * Reset the list of selected profiles.
     */
    public void resetProfiles() {
        this.selectedIntegrationProfiles = new ArrayList<>();
    }

    /**
     * Download the Technical Framework for selected Integration Profiles
     */
    public void downloadTF() {
        //TODO
    }

    /**
     * Listener of an uploaded file.
     *
     * @param uploadFile file that is uploaded
     * @param filename   name of the file
     * @param id         id of the upload
     * @param param      additional param
     * @throws IOException if the upload cannot be performed.
     */
    @Override
    public void uploadedFile(File uploadFile, String filename, String id, String param) throws IOException {
        // Keep this ?
    }
}
