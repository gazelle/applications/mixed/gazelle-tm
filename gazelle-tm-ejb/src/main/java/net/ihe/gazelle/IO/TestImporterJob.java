package net.ihe.gazelle.IO;

import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.ssov7.gum.client.application.User;
import net.ihe.gazelle.tm.gazelletest.model.definition.Test;
import net.ihe.gazelle.tm.messages.NotificationService;
import net.ihe.gazelle.tm.messages.SimpleMessageSource;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.*;
import org.jboss.seam.annotations.async.Asynchronous;
import org.jboss.seam.async.QuartzDispatcher;
import org.jboss.seam.async.TimerSchedule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.FilenameFilter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Name("TestImporterJob")
@AutoCreate
@Scope(ScopeType.APPLICATION)
@GenerateInterface("TestImporterJobLocal")
public class TestImporterJob implements TestImporterJobLocal, Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(TestImporterJob.class);

    /**
     * Import tests for an XML file.
     * Imports are done in a background job, to avoid transaction timeout, and return quickly to the user.
     *
     * @param filePath           path to the XML file.
     * @param deleteImportedFile boolean value, delete file if true.
     * @param initUser           User that initiated the import.
     */
    @Override
    @Observer("importXmlTests")
    @Transactional
    @Asynchronous
    public void importXmlTests(String filePath, boolean deleteImportedFile, User initUser) {
        LOG.debug("importXmlTests");
        File testsToImportXml = new File(filePath);

        if (testsToImportXml.isFile()) {
            TestXmlPackager testPackage = unmarshallXml(testsToImportXml);
            if (testPackage == null) {
                NotificationService.dispatchNotification(SimpleMessageSource.INSTANCE, initUser, initUser.getId(),
                        "Error importing test : could not unmarshall uploaded file !", "/testing/testsDefinition/testsList.seam");
                return;
            }
            List<Test> tests = testPackage.getTests();
            QuartzDispatcher.instance().scheduleAsynchronousEvent("importTest", tests, tests.size(), 1,
                    deleteImportedFile, testsToImportXml, initUser);

        } else if (testsToImportXml.isDirectory()) {

            GenericExtFilter filter = new GenericExtFilter("xml");
            File[] files = testsToImportXml.listFiles(filter);
            if (files != null) {
                List<File> listOfFiles = new ArrayList<File>(Arrays.asList(files));
                QuartzDispatcher.instance().scheduleAsynchronousEvent("importTestFromFile", listOfFiles,
                        listOfFiles.size(), 1, deleteImportedFile, initUser);
            }
        }
    }

    /**
     * A new call to saveOrMergeFromFile is issued for each test to import.
     * This is done in order to avoid transaction timeout.
     * Each method call has it's own transaction
     * A test import can take time and when importing a lot of tests it could take more than the allowed transaction time.
     *
     * @param xmlFiles           list of XML files to import
     * @param nbFiles            number of files to import
     * @param counter            counter of imported files
     * @param deleteImportedFile boolean value, delete file if true.
     * @param initUser           User that initiated the import.
     */
    @Override
    @Observer("importTestFromFile")
    @Transactional
    @Asynchronous
    public void saveOrMergeFromFile(List<File> xmlFiles, int nbFiles, int counter, boolean deleteImportedFile, User initUser) {
        LOG.warn("saveOrMergeFromFile");
        if (xmlFiles.size() > 0) {
            File testsToImportXml = xmlFiles.remove(0);
            counter++;
            TestXmlPackager testPackage = unmarshallXml(testsToImportXml);
            if (testPackage == null) {
                NotificationService.dispatchNotification(SimpleMessageSource.INSTANCE, initUser, initUser.getId(),
                        "Error importing test from file " + testsToImportXml.getName() + " : could not unmarshall file !", "/testing" +
                                "/testsDefinition/testsList.seam");
            } else {
                List<Test> tests = testPackage.getTests();
                QuartzDispatcher.instance().scheduleAsynchronousEvent("importTest", tests, tests.size(), 1,
                        deleteImportedFile, testsToImportXml, initUser);
            }
            if (deleteImportedFile) {
                if (testsToImportXml.delete()) {
                    LOG.info("testsToImportXml deleted");
                } else {
                    LOG.error("Failed to delete testsToImportXml");
                }
            }
            QuartzDispatcher.instance().scheduleAsynchronousEvent("importTestFromFile", xmlFiles, nbFiles, counter,
                    deleteImportedFile, initUser);
        }
    }

    /**
     * A new call to saveOrMerge is issued for each test to import.
     * This is done in order to avoid transaction timeout.
     * Each method call has it's own transaction
     * A test import can take time and when importing a lot of tests it could take more than the allowed transaction time.
     *
     * @param tests              list of test to import.
     * @param nbTests            number of tests to import.
     * @param counter            counter of tests imported.
     * @param deleteImportedFile boolean value, delete file if true.
     * @param xmlFile            imported XML file.
     * @param initUser           User that initiated the import.
     */
    @Override
    @Observer("importTest")
    @Transactional
    @Asynchronous
    public void saveOrMerge(List<Test> tests, int nbTests, int counter, boolean deleteImportedFile, File xmlFile, User initUser) {
        LOG.debug("saveOrMerge");
        if (tests.size() > 0) {
            Test test = tests.get(0);
            test.setLastModifierId(initUser.getId());
            try {
                test.saveOrMerge(EntityManagerService.provideEntityManager(), counter, nbTests);
            } catch (Exception e) {
                QuartzDispatcher.instance().scheduleAsynchronousEvent("errorOnTestImport", e, test, initUser);
            }
            tests.remove(0);
            counter++;
            QuartzDispatcher.instance().scheduleTimedEvent("importTest", new TimerSchedule(new Long(2000)), tests, nbTests, counter,
                    deleteImportedFile, xmlFile, initUser);
        } else {
            if (deleteImportedFile) {
                if (xmlFile.delete()) {
                    LOG.info("xmlFile deleted");
                } else {
                    LOG.error("Failed to delete xmlFile");
                }
            }
            NotificationService.dispatchNotification(SimpleMessageSource.INSTANCE, initUser, initUser.getId(),
                    "The import of your tests is over", "/testing/testsDefinition/testsList.seam");
        }
    }

    /**
     * Handle errors on test import. Logs the error and send a notification to the user that triggered the import.
     *
     * @param e        exception to handle
     * @param test     test that was imported
     * @param initUser user that triggered the import
     */
    @Override
    @Observer("errorOnTestImport")
    @Transactional
    @Asynchronous
    public void errorOnTestImport(Exception e, Test test, User initUser) {
        LOG.error("Error importing test " + test.getKeyword() + " : " + e.getMessage(), e);
        NotificationService.dispatchNotification(SimpleMessageSource.INSTANCE, initUser, initUser.getId(),
                "Error importing test " + test.getKeyword() + " : " + e.getMessage(), "/testing/testsDefinition/testsList.seam");
    }

    /**
     * Unmarshall XML from a file content to a Test package.
     *
     * @param testsToImportXml file to unmarshall.
     * @return the {@link TestXmlPackager}.
     */
    private TestXmlPackager unmarshallXml(File testsToImportXml) {
        TestXmlPackager testPackage = null;
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(TestXmlPackager.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            testPackage = (TestXmlPackager) jaxbUnmarshaller.unmarshal(testsToImportXml);
        } catch (JAXBException e) {
            LOG.error("Failed to import tests");
            LOG.error("JAXBContext cannot unmarshal " + testsToImportXml.getAbsolutePath());
            LOG.error("JAXBContext cannot unmarshal " + e.getCause());
        }
        return testPackage;
    }

    /**
     * Filter on file name. This filter check the extension of the files.
     */
    private static class GenericExtFilter implements FilenameFilter {

        private String ext;

        /**
         * Default constructor for the class.
         *
         * @param ext value to set to the ext property.
         */
        GenericExtFilter(String ext) {
            this.ext = ext;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public boolean accept(File dir, String name) {
            LOG.debug("accept");
            return (name.endsWith(ext));
        }
    }
}
