package db.migration;

import net.ihe.gazelle.tm.systems.model.TestingSession;
import org.flywaydb.core.api.migration.jdbc.BaseJdbcMigration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;

public class V8_0_0_3__Init_first_testing_session extends BaseJdbcMigration {

   private static final Logger LOG = LoggerFactory.getLogger(V8_0_0_3__Init_first_testing_session.class);
   public static final String INSERT_NEW_TESTING_SESSION_QUERY = "INSERT INTO tm_testing_session " +
           "(id, active, allow_one_company_play_several_role_in_group_tests, allow_one_company_play_several_role_in_p2p_tests,allow_participants_start_group_tests,contact_email,contact_firstname,contact_lastname,continuous_session,required_contract,default_testing_session,order_in_gui,hidden,hide_advanced_sample_search_to_vendors,session_without_monitors,systemautoacceptance,name,conformity_test_report_enabled,display_certificates_menu,is_critical_status_enabled,is_proxy_use_enabled)" +
           " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";

   @Override
   public void migrate(Connection connection) throws Exception {
      // Check if a testing session needs to be created
      if (isTestingSessionCreationNeeded(connection)) {
         // Create a testing session
         createTestingSession(connection);
      }
   }

   /**
    * Check if a testing session needs to be created
    * @param connection The connection to the database
    * @return true if a testing session needs to be created, false otherwise
    * @throws SQLException if an error occurs
    */
   private static boolean isTestingSessionCreationNeeded(Connection connection) throws SQLException {
      try(Statement statement = connection.createStatement()) {
         try(ResultSet resultSet = statement.executeQuery("SELECT COUNT(*) FROM public.tm_testing_session;")) {
            if(resultSet.next())
               return resultSet.getInt(1) == 0;
            else
               throw new SQLException("Unable to retrieve the number of testing sessions");
         }
      }
   }

   /**
    * Create a testing session
    * @param connection The connection to the database
    * @throws SQLException if an error occurs
    */
   private static void createTestingSession(Connection connection) throws SQLException {
      TestingSession testingSession = generateDefaultTestingSession(connection);

      try(PreparedStatement statement = connection.prepareStatement(INSERT_NEW_TESTING_SESSION_QUERY)) {
         statement.setInt(1,1);
         statement.setBoolean(2,testingSession.getActiveSession());
         statement.setBoolean(3,testingSession.isAllowOneCompanyPlaySeveralRolesInGroupTests());
         statement.setBoolean(4,testingSession.isAllowOneCompanyPlaySeveralRolesInP2PTests());
         statement.setBoolean(5,testingSession.isAllowParticipantsStartGroupTests());
         statement.setString(6,testingSession.getContactEmail());
         statement.setString(7,testingSession.getContactFirstname());
         statement.setString(8,testingSession.getContactLastname());
         statement.setBoolean(9,testingSession.getContinuousSession());
         statement.setBoolean(10,testingSession.isContractRequired());
         statement.setBoolean(11,testingSession.getIsDefaultTestingSession());
         statement.setInt(12,testingSession.getOrderInGUI());
         statement.setBoolean(13,testingSession.getHiddenSession());
         statement.setBoolean(14,testingSession.isHideAdvancedSampleSearchToVendors());
         statement.setBoolean(15,testingSession.isSessionWithoutMonitors());
         statement.setBoolean(16,testingSession.isSystemAutoAcceptance());
         statement.setString(17,testingSession.getName());
         statement.setBoolean(18,testingSession.getConformityTestReportEnabled());
         statement.setBoolean(19,testingSession.isDisplayCertificatesMenu());
         statement.setBoolean(20,testingSession.getIsCriticalStatusEnabled());
         statement.setBoolean(21,testingSession.getIsProxyUseEnabled());
         int resultSet = statement.executeUpdate();
            if (resultSet == 0)
               throw new SQLException("No rows affected");
      }
   }

   /**
    * Generate a default testing session object
    * @param connection The connection to the database
    * @return A default testing session object
    * @throws SQLException if the preferences cannot be retrieved
    */
   private static TestingSession generateDefaultTestingSession(Connection connection) throws SQLException {
      TestingSession testingSession = new TestingSession();

      testingSession.setActiveSession(true);
      testingSession.setAllowParticipantsStartGroupTests(true);
      testingSession.setAllowOneCompanyPlaySeveralRolesInP2PTests(true);
      testingSession.setAllowOneCompanyPlaySeveralRolesInGroupTests(true);
      testingSession.setContractRequired(false);
      testingSession.setContinuousSession(true);
      testingSession.setIsDefaultTestingSession(true);
      testingSession.setHiddenSession(false);
      testingSession.setOrderInGUI(1);
      testingSession.setHideAdvancedSampleSearchToVendors(false);
      testingSession.setSessionWithoutMonitors(true);
      testingSession.setSystemAutoAcceptance(true);
      testingSession.setDisplayCertificatesMenu(true);
      testingSession.setIsCriticalStatusEnabled(false);
      testingSession.setName("Default testing session");
      testingSession.setIsProxyUseEnabled(false);

      // Retrieve preferences to set some default values
      Map<String,String> preferences = retrieveRequiredApplicationPreferences();
      testingSession.setConformityTestReportEnabled(false);
      testingSession.setContactEmail(preferences.get("application_admin_email"));
      testingSession.setContactFirstname(preferences.get("application_admin_firstname"));
      testingSession.setContactLastname(preferences.get("application_admin_lastname"));

      return testingSession;
   }

   /**
    * Retrieve all application preferences
    * @return A map of all application preferences
    */
   private static Map<String,String> retrieveRequiredApplicationPreferences() {
      Map<String,String> preferences = new HashMap<>();
      //TODO Catch directly environment variables instead of pref table
      String adminName = System.getenv("application_admin_name");
      if (adminName != null && adminName.split(" ").length == 1) {
         preferences.put("application_admin_firstname","admin");
         preferences.put("application_admin_lastname",adminName);
      } else {
         preferences.put("application_admin_firstname","admin");
         preferences.put("application_admin_lastname","admin");
      };
      preferences.put("time_zone",System.getenv("time_zone") != null ? System.getenv("time_zone") : "Europe/Paris");
      preferences.put("application_admin_email",System.getenv("application_admin_email") != null ? System.getenv("application_admin_email") : "admin@gazelle.com");

      return preferences;
   }
}