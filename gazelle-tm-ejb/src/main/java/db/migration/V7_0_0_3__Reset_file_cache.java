package db.migration;

import org.apache.commons.io.FileUtils;
import org.flywaydb.core.api.migration.jdbc.BaseJdbcMigration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;

public class V7_0_0_3__Reset_file_cache extends BaseJdbcMigration {

   private static final Logger LOG = LoggerFactory.getLogger(V7_0_0_3__Reset_file_cache.class);

   @Override
   public void migrate(Connection connection) {
      // User photo ids have changed, so the file-system cache /opt/gazelle/fileCache must be re-built.
      try {
         File folder = new File(getFileCachePath());
         FileUtils.cleanDirectory(folder);
         LOG.warn("cache folder cleaned");
      } catch (IOException | IllegalArgumentException e) {
         LOG.warn("Cannot clear file cache", e);
      }
   }

   private static String getFileCachePath() {
      // Retrieve preference value from environment variable
      String gazelleHomePath = System.getenv("gazelle_home_path");

      // Default value if environment variable is not set
      if (gazelleHomePath == null) gazelleHomePath = "/opt/gazelle";

      return gazelleHomePath + File.separatorChar + "fileCache" + File.separatorChar;
   }
}