<?xml version="1.0" encoding="UTF-8"?>
<!--
 Copyright 2021 IHE International (http://www.ihe.net)

 Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and limitations under the License.
-->
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified" targetNamespace="http://testreport.gazelle.ihe.net/"
           xmlns="http://testreport.gazelle.ihe.net/">
    <xs:annotation>
        <xs:documentation>This Schema documents the test report used by an external tool (simulator or conformance tool) to report a test suite
            execution to Gazelle Test Bed.
        </xs:documentation>
    </xs:annotation>

    <xs:import namespace="http://www.w3.org/XML/1998/namespace" schemaLocation="xml.xsd"/>

    <xs:element name="testReport">
        <xs:annotation>
            <xs:documentation>The root element of a Test report. It must contain the identification of the service or tool that has executed the test
                suite ; the identification of the subject of the test suite (aka. the system under test); The general result of this report with some
                statistics ; and it may contain a detailed result section that list all tests. Each report must be identified uniquely with a UUID
                (version 4 RFC 4122, randomly generated) and must have a date and time of creation. In the case the report is modified later, the date
                and time should be updated accordingly.
            </xs:documentation>
        </xs:annotation>
        <xs:complexType>
            <xs:sequence>
                <xs:element name="testService" type="testServiceType"/>
                <xs:element name="systemUnderTest" type="systemUnderTestType"/>
                <xs:element name="resultOverview" type="resultOverviewType"/>
                <xs:element name="detailedResult" type="detailedResultType" minOccurs="0"/>
            </xs:sequence>
            <xs:attribute name="uuid" type="uuidType" use="required"/>
            <xs:attribute name="dateTime" type="xs:dateTime" use="required"/>
        </xs:complexType>
    </xs:element>

    <xs:complexType name="identificationType">
        <xs:annotation>
            <xs:documentation>Identification of an entity or a software module based on a name and a version. If the version is not known, it should
                be explicited using 'unknown' as value.
            </xs:documentation>
        </xs:annotation>
        <xs:simpleContent>
            <xs:extension base="xs:string">
                <xs:attribute name="version" type="xs:string" use="required"/>
            </xs:extension>
        </xs:simpleContent>
    </xs:complexType>

    <xs:complexType name="testServiceType">
        <xs:annotation>
            <xs:documentation>Identification of the service or tool that has executed the test suite and/or authored this test report. A disclaimer is
                required to inform the recipient of the report about licencing consideration, eventual liabilities or result interpretation.
            </xs:documentation>
        </xs:annotation>
        <xs:sequence>
            <xs:element name="serviceIdentification" type="identificationType"/>
            <xs:element name="disclaimer" type="textType"/>
        </xs:sequence>
    </xs:complexType>

    <xs:complexType name="systemUnderTestType">
        <xs:annotation>
            <xs:documentation>Identification of the subject of the test suite (aka. the system under test). Some hostnames and addresses can be added.
            </xs:documentation>
        </xs:annotation>
        <xs:sequence>
            <xs:element name="systemIdentification" type="identificationType"/>
            <xs:element name="macAddress" type="macAddressType" minOccurs="0" maxOccurs="unbounded"/>
            <xs:element name="ipAddress" type="ipAddressType" minOccurs="0" maxOccurs="unbounded"/>
            <xs:element name="hostName" type="xs:string" minOccurs="0" maxOccurs="unbounded"/>
        </xs:sequence>
    </xs:complexType>

    <xs:complexType name="resultOverviewType">
        <xs:annotation>
            <xs:documentation>The overview of a test report is the general result of the complete execution. It must contain the name of the test
                suite (set of tests) that have been executed, the general result of this test suite and the numbers of tests that have been run with
                the sum of their statuses. The result overview may contain some notes about the system under test or the test suite execution. If the
                test-service is a web application and is archiving results, the overview may contain an URL that point to this very-same test suite
                results.
            </xs:documentation>
        </xs:annotation>
        <xs:sequence>
            <xs:element name="testSuiteName" type="xs:string"/>
            <xs:element name="status" type="testSuiteResultType"/>
            <xs:element name="numberOfTests" type="numberOfTestsType"/>
            <xs:element name="note" type="textType" minOccurs="0" maxOccurs="unbounded"/>
            <xs:element name="urlToTestSuiteResult" type="xs:anyURI" minOccurs="0"/>
        </xs:sequence>
    </xs:complexType>

    <xs:complexType name="detailedResultType">
        <xs:annotation>
            <xs:documentation>This section list all test instances that have been run and their results. If this section is included, the test
                instance list must be exhaustive and include all the tests counted in resultOverview/numberOfTests.
            </xs:documentation>
        </xs:annotation>
        <xs:sequence>
            <xs:element name="testInstance" type="testInstanceType" maxOccurs="unbounded"/>
        </xs:sequence>
    </xs:complexType>

    <xs:complexType name="testInstanceType">
        <xs:annotation>
            <xs:documentation>This represents a test instance report. It must contains an id, a date and time of execution, the information of the
                instantiated test case and the result status. It may contain the information of a specific simulator or tool used. Some notes
                related to the execution can be added to either contextualize the execution, include comments of operators or to report some warnings.
                Finally if the test-service is a web application and is archiving results, the test instance section may contain an URL that
                point to this very-same test instance result.
            </xs:documentation>
        </xs:annotation>
        <xs:sequence>
            <xs:element name="test" type="testInfoType"/>
            <xs:element name="simulatorIdentification" type="identificationType" minOccurs="0"/>
            <xs:element name="status" type="testInstanceResultType"/>
            <xs:element name="note" type="textType" minOccurs="0" maxOccurs="unbounded"/>
            <xs:element name="urlToTestInstance" type="xs:anyURI" minOccurs="0"/>
        </xs:sequence>
        <xs:attribute name="instanceId" type="xs:string"/>
        <xs:attribute name="dateTime" type="xs:dateTime"/>
    </xs:complexType>

    <xs:complexType name="testInfoType">
        <xs:annotation>
            <xs:documentation>Information about a test case that have been instantiated for this run. It must include the identification of the test
                and could include a short description of its goals. The version of the test case should be included if known.
            </xs:documentation>
        </xs:annotation>
        <xs:sequence>
            <xs:element name="description" type="textType" minOccurs="0"/>
        </xs:sequence>
        <xs:attribute name="name" type="xs:string" use="required"/>
        <xs:attribute name="version" type="xs:string"/>
    </xs:complexType>

    <xs:simpleType name="uuidType">
        <xs:annotation>
            <xs:documentation>UUID Type. All UUID in this report should be randomly generated according to UUID version 4 RFC 4122.</xs:documentation>
        </xs:annotation>
        <xs:restriction base="xs:string">
            <xs:pattern value="[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}"/>
        </xs:restriction>
    </xs:simpleType>

    <xs:simpleType name="macAddressType">
        <xs:annotation>
            <xs:documentation>MAC Address</xs:documentation>
        </xs:annotation>
        <xs:restriction base="xs:string">
            <xs:pattern value="([0-9A-F]{2}[:-]){5}([0-9A-F]{2})"/>
        </xs:restriction>
    </xs:simpleType>

    <xs:simpleType name="ipAddressType">
        <xs:annotation>
            <xs:documentation>
                IPv4 or IPv6 Address.
            </xs:documentation>
        </xs:annotation>
        <xs:restriction base="xs:string">
            <xs:pattern
                    value="(((1?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])\.){3}(1?[0-9]?[0-9]|2[0-4][0-9]|25[0-5]))|(([0-9a-fA-F]{1,4}:){7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]+|::(ffff(:0{1,4})?:)?((25[0-5]|(2[0-4]|1?[0-9])?[0-9])\.){3}(25[0-5]|(2[0-4]|1?[0-9])?[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1?[0-9])?[0-9])\.){3}(25[0-5]|(2[0-4]|1?[0-9])?[0-9]))"/>
        </xs:restriction>
    </xs:simpleType>

    <xs:simpleType name="testSuiteResultType">
        <xs:annotation>
            <xs:documentation>General result of a test suite execution that depends on the test instance results. It should follow the following
                rules: By default and if empty, then the test suite is UNDEFINED. If one of the test instances is UNDEFINED, then the test suite
                result is UNDEFINED. If one test instance is FAILED and without other test instance UNDEFINED, then the test suite result is FAILED.
                If all test instances are PASSED, then the test suite is PASSED. Test instance with the status SKIPPED are ignored in the test suite
                result computation.
            </xs:documentation>
        </xs:annotation>
        <xs:restriction base="xs:string">
            <xs:enumeration value="PASSED"/>
            <xs:enumeration value="FAILED"/>
            <xs:enumeration value="UNDEFINED"/>
        </xs:restriction>
    </xs:simpleType>

    <xs:simpleType name="testInstanceResultType">
        <xs:annotation>
            <xs:documentation>Test instance result. Result assignement should follow those rules: If a test has not been run because the context make
                it irrelevant, then the result is SKIPPED. If the test has been run or started, but the test process has been blocked, interrupted or
                if the logs cannot determine whether the SUT is fulfilling the test assertions, then the result is UNDEFINED. If the test has been run
                and the logs shows that the SUT is NOT fulfilling the test assertions, then the result is FAILED. If the test has been run and the
                logs shows that the SUT is fulfilling the test assertions, with or without warnings, then the result is PASSED. Also, If the test
                instance return warnings, the service should consider reporting the warnings as notes in the test instance element.
            </xs:documentation>
        </xs:annotation>
        <xs:restriction base="xs:string">
            <xs:enumeration value="PASSED"/>
            <xs:enumeration value="FAILED"/>
            <xs:enumeration value="SKIPPED"/>
            <xs:enumeration value="UNDEFINED"/>
        </xs:restriction>
    </xs:simpleType>

    <xs:complexType name="numberOfTestsType">
        <xs:annotation>
            <xs:documentation>Hold test instances number and statistics about the statuses. The 'total' attribute is the total number of tests that
                should have been run for this test suite. The 'run' attribute is the total test instances that have been run or started during the
                execution of this test suite. The 'passed', 'failed', 'undefined' and 'skipped' attributes are the total test instances with the
                respective results 'PASSED', 'FAILED', 'UNDEFINED' and 'SKIPPED'. The following constraints should be followed :
                total = run + skipped.
                run = passed + failed + undefined.
            </xs:documentation>
        </xs:annotation>
        <xs:attribute name="total" type="xs:nonNegativeInteger" use="required"/>
        <xs:attribute name="run" type="xs:nonNegativeInteger" use="required"/>
        <xs:attribute name="passed" type="xs:nonNegativeInteger" use="required"/>
        <xs:attribute name="failed" type="xs:nonNegativeInteger" use="required"/>
        <xs:attribute name="undefined" type="xs:nonNegativeInteger" use="required"/>
        <xs:attribute name="skipped" type="xs:nonNegativeInteger" use="required"/>
    </xs:complexType>

    <xs:complexType name="textType">
        <xs:annotation>
            <xs:documentation>Simple text type to write comments and notes using natural language</xs:documentation>
        </xs:annotation>
        <xs:simpleContent>
            <xs:extension base="xs:string">
                <xs:attribute  ref="xml:lang"/>
            </xs:extension>
        </xs:simpleContent>
    </xs:complexType>

</xs:schema>