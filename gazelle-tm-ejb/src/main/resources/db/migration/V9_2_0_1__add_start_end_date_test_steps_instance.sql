ALTER TABLE tm_test_steps_instance add column if not exists start_date timestamp with time zone;
ALTER TABLE tm_test_steps_instance add column if not exists end_date timestamp with time zone;

INSERT INTO cmn_application_preference (id, class_name, preference_name, preference_value, description) VALUES (nextval('cmn_application_preference_id_seq'), 'java.lang.String', 'gazelle_datahouse_ui_base_url', 'http://localhost:3000/datahouse-ui', 'the base URL of datahouse-ui');
