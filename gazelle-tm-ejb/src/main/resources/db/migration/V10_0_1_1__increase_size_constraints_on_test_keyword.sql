ALTER TABLE public.tm_test ALTER COLUMN keyword TYPE VARCHAR(255);
ALTER TABLE public.tm_test_aud ALTER COLUMN keyword TYPE VARCHAR(255);
ALTER TABLE public.tm_meta_test ALTER COLUMN keyword TYPE VARCHAR(255);
ALTER TABLE public.tm_meta_test_aud ALTER COLUMN keyword TYPE VARCHAR(255);