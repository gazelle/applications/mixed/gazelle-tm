-- Remove the installation_done preference
DELETE FROM public.cmn_application_preference where preference_name = 'installation_done';
-- Insert the gum_front_url preference
INSERT INTO  public.cmn_application_preference (id, class_name, preference_name, preference_value, description) VALUES (nextval('cmn_application_preference_id_seq'), 'java.lang.String', 'gum_front_url', 'http://localhost:3000/gum-ui', 'The url of the Gazelle User Management front application. Used for installation process.');