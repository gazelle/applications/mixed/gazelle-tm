INSERT INTO cmn_application_preference (class_name, preference_name, preference_value, description, id)
VALUES ('java.lang.Boolean',
        'ip_login',
        'false',
        'Enable/disable login via IP filtering mechanism (do not use in production). ip_login_admin regexp must be defined if set to true.',
        nextval('cmn_application_preference_id_seq')) ON CONFLICT (preference_name) DO NOTHING;

INSERT INTO cmn_application_preference (class_name, preference_name, preference_value, description, id)
VALUES ('java.lang.String',
        'ip_login_admin',
        '.*',
        'Regular expression to filter IP addresses for which users will be identified as admin users. ip_login must be set to true to be effective.',
        nextval('cmn_application_preference_id_seq')) ON CONFLICT (preference_name) DO NOTHING;

-- Disable blocked user
UPDATE usr_users SET activated = false WHERE blocked = true;

-- Remove columns linked the block user feature
ALTER TABLE usr_users DROP COLUMN IF EXISTS blocked;
ALTER TABLE usr_users DROP COLUMN IF EXISTS counter_failed_login_attempts;
DROP INDEX IF EXISTS usr_users_blocked_idx;

-- Remove foreign-key of preferences table towards user table.
ALTER TABLE tm_user_preferences ADD COLUMN username character varying(255);
UPDATE tm_user_preferences SET username = (SELECT username FROM usr_users WHERE usr_users.id = tm_user_preferences.user_id);
ALTER TABLE tm_user_preferences ALTER COLUMN username SET NOT NULL ;
ALTER TABLE tm_user_preferences ADD CONSTRAINT uk_username UNIQUE (username);

ALTER TABLE tm_user_preferences DROP CONSTRAINT IF EXISTS uk_6e83kvu2r93s4hafcs6gcwmah;
ALTER TABLE tm_user_preferences DROP CONSTRAINT IF EXISTS fk_6e83kvu2r93s4hafcs6gcwmah;
ALTER TABLE tm_user_preferences DROP CONSTRAINT IF EXISTS fkebf6c58a96e9f6ac;
ALTER TABLE tm_user_preferences DROP COLUMN  user_id;

-- Fix for update sql for version 6.10.4 by adding 'preparatory' test type
INSERT INTO tm_test_type (id, last_changed, last_modifier_id, description, keyword, label_to_display)
VALUES (nextval('tm_test_type_id_seq'),
        NULL,
        NULL,
        'Preparatory tests to execute before the event starts',
        'preparatory',
        'preparatory') ON CONFLICT (keyword) DO NOTHING;
