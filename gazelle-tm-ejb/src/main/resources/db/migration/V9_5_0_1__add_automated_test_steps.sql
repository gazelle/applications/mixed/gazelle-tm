INSERT INTO cmn_application_preference (id, class_name, preference_name, preference_value, description) VALUES (nextval('cmn_application_preference_id_seq'), 'java.lang.String', 'test_step_script_path', 'testScript', 'Path where to store the uploaded test step scripts for automated test steps.');
INSERT INTO cmn_application_preference (id, class_name, preference_name, preference_value, description) VALUES (nextval('cmn_application_preference_id_seq'), 'java.lang.Boolean', 'automated_step_enabled', 'false', 'Boolean variable to enable or not automated test step usage.');
INSERT INTO cmn_application_preference (id, class_name, preference_name, preference_value, description) VALUES (nextval('cmn_application_preference_id_seq'), 'java.lang.String', 'maestro_url', 'http://localhost:8080/gateway', 'URL to reach maestro application.');
INSERT INTO tm_data_type (id, last_changed, last_modifier_id, description, keyword) VALUES (nextval('tm_data_type_id_seq'), NULL, NULL, 'input for automated test step instance', 'automationInput');

ALTER TABLE tm_test DROP COLUMN cttintegrated;
ALTER TABLE tm_test_instance_token RENAME COLUMN username TO userId;
ALTER TABLE tm_test_instance_token RENAME COLUMN creation_date_time TO expiration_date_time;
ALTER TABLE tm_test_instance_token RENAME COLUMN test_instance_identifier TO test_step_instance_id;
ALTER TABLE if EXISTS tm_test_instance_token
    ADD CONSTRAINT FK_tm_test_instance_token FOREIGN KEY (test_step_instance_id) REFERENCES tm_test_steps_instance;

ALTER TABLE tm_test_steps_instance ADD COLUMN execution_id int;

CREATE TABLE tm_automated_test_step
(
    test_script_id character varying(255),
    test_steps_id int NOT NULL,
    primary key (test_steps_id)
);

ALTER TABLE if EXISTS tm_automated_test_step
    ADD CONSTRAINT FK12rdgn0aw2sqzefqerfco1hr083 FOREIGN KEY (test_steps_id) REFERENCES tm_test_steps;

ALTER TABLE public.tm_automated_test_step OWNER TO gazelle;

CREATE TABLE tm_automated_test_step_aud
(
    test_script_id character varying(255),
    test_steps_id int NOT NULL,
    rev integer NOT NULL,
    revtype smallint
);

ALTER TABLE ONLY public.tm_automated_test_step_aud
    ADD CONSTRAINT tm_automated_test_step_aud_pkey PRIMARY KEY (test_steps_id, rev);

ALTER TABLE if EXISTS ONLY public.tm_automated_test_step_aud
    ADD CONSTRAINT fk_15r95d6g5mox0g2xyokwfisgr FOREIGN KEY (rev) REFERENCES public.revinfo(rev);

ALTER TABLE public.tm_automated_test_step_aud OWNER TO gazelle;

CREATE TABLE tm_automated_test_step_input
(
    id int NOT NULL,
    key character varying(255),
    label character varying(255),
    description character varying(255),
    type character varying(255),
    required boolean,
    test_steps_id integer NOT NULL,
    primary key (id)
);

ALTER TABLE if EXISTS ONLY public.tm_automated_test_step_input
    ADD CONSTRAINT fk_n0kvbowvki1af3a6ku0lq2ixq FOREIGN KEY (test_steps_id) REFERENCES public.tm_automated_test_step(test_steps_id);

ALTER TABLE public.tm_automated_test_step_input OWNER TO gazelle;

CREATE SEQUENCE public.tm_automated_test_step_input_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE public.tm_automated_test_step_input_id_seq OWNER TO gazelle;

CREATE TABLE tm_automated_test_step_input_aud
(
    id int NOT NULL,
    key character varying(255),
    label character varying(255),
    description character varying(255),
    type character varying(255),
    required boolean,
    test_steps_id integer,
    rev integer NOT NULL,
    revtype smallint
);

ALTER TABLE ONLY public.tm_automated_test_step_input_aud
    ADD CONSTRAINT tm_automated_test_step_input_aud_pkey PRIMARY KEY (id, rev);

ALTER TABLE if EXISTS ONLY public.tm_automated_test_step_input_aud
    ADD CONSTRAINT fk_15095d6g5mox0g2xyokwfisgr FOREIGN KEY (rev) REFERENCES public.revinfo(rev);

ALTER TABLE public.tm_automated_test_step_input_aud OWNER TO gazelle;

CREATE TABLE tm_automated_test_step_instance_input
(
    id int NOT NULL,
    last_changed timestamp with time zone,
    last_modifier_id character varying(255),
    test_steps_instance_id integer NOT NULL,
    test_step_data_id integer,
    key character varying(255),
    label character varying(255),
    description character varying(255),
    type character varying(255),
    required boolean,
    primary key (id)
);

ALTER TABLE if EXISTS ONLY public.tm_automated_test_step_instance_input
    ADD CONSTRAINT fk_n0kvbowvki1af3atgyhtu0lq2ixq FOREIGN KEY (test_steps_instance_id) REFERENCES public.tm_test_steps_instance(id);

ALTER TABLE public.tm_automated_test_step_instance_input OWNER TO gazelle;

CREATE SEQUENCE public.tm_automated_test_step_instance_input_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE public.tm_automated_test_step_instance_input_id_seq OWNER TO gazelle;

CREATE TABLE tm_automated_test_step_instance_input_aud
(
    id int NOT NULL,
    last_changed timestamp with time zone,
    last_modifier_id character varying(255),
    test_steps_instance_id integer NOT NULL,
    test_step_data_id integer,
    key character varying(255),
    label character varying(255),
    description character varying(255),
    type character varying(255),
    required boolean,
    rev integer NOT NULL,
    revtype smallint
);

ALTER TABLE ONLY public.tm_automated_test_step_instance_input_aud
    ADD CONSTRAINT tm_automated_test_step_instance_input_aud_pkey PRIMARY KEY (id, rev);

ALTER TABLE if EXISTS ONLY public.tm_automated_test_step_instance_input_aud
    ADD CONSTRAINT fk_15095d6g5mox0g2xyokwfisgr FOREIGN KEY (rev) REFERENCES public.revinfo(rev);

CREATE TABLE tm_automated_test_step_execution
(
    id int NOT NULL,
    test_steps_instance_id int NOT NULL UNIQUE,
    user_id character varying(255),
    status character varying(255),
    date timestamp without time zone,
    report text,
    primary key (id)
);

ALTER TABLE if EXISTS ONLY public.tm_automated_test_step_execution
    ADD CONSTRAINT fk_n0kvbowvki1afrftgtgyhtu0lq2ixq FOREIGN KEY (test_steps_instance_id) REFERENCES public.tm_test_steps_instance(id);

ALTER TABLE public.tm_automated_test_step_execution OWNER TO gazelle;

CREATE SEQUENCE public.tm_automated_test_step_execution_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE public.tm_automated_test_step_execution_id_seq OWNER TO gazelle;

CREATE TABLE tm_automated_test_step_execution_aud
(
    id int NOT NULL,
    test_steps_instance_id int NOT NULL,
    user_id character varying(255),
    status character varying(255),
    date timestamp without time zone,
    report text,
    rev integer NOT NULL,
    revtype smallint
);

ALTER TABLE ONLY public.tm_automated_test_step_execution_aud
    ADD CONSTRAINT tm_automated_test_step_execution_aud_pkey PRIMARY KEY (id, rev);

ALTER TABLE if EXISTS ONLY public.tm_automated_test_step_execution_aud
    ADD CONSTRAINT tm_automated_test_step_execution_aud_fkey FOREIGN KEY (rev) REFERENCES public.revinfo(rev);

CREATE TABLE tm_automated_test_step_report_file
(
    id int NOT NULL,
    report_name character varying(255) NOT NULL,
    pdf_report BYTEA NOT NULL,
    execution_id int NOT NULL
);

ALTER TABLE public.tm_automated_test_step_report_file OWNER TO gazelle;

CREATE SEQUENCE public.tm_automated_test_step_report_file_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE public.tm_automated_test_step_report_file_id_seq OWNER TO gazelle;

CREATE TABLE tm_automated_test_step_report_file_aud
(
    id int NOT NULL,
    report_name character varying(255),
    pdf_report BYTEA,
    execution_id int,
    rev integer NOT NULL,
    revtype smallint
);

ALTER TABLE ONLY public.tm_automated_test_step_report_file_aud
    ADD CONSTRAINT tm_automated_test_step_report_file_aud_pkey PRIMARY KEY (id, rev);

ALTER TABLE if EXISTS ONLY public.tm_automated_test_step_report_file_aud
    ADD CONSTRAINT tm_automated_test_step_report_file_aud_fkey FOREIGN KEY (rev) REFERENCES public.revinfo(rev);