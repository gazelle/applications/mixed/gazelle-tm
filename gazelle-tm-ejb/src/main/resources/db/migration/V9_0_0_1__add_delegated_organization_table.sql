create table usr_delegated_organization
(
    external_id    varchar(255),
    idp_id         varchar(255),
    organization_id int not null,
    primary key (organization_id)
);

alter table if exists usr_delegated_organization
    add constraint FK1n0aw2s9ebayp18mco1hr083
        foreign key (organization_id)
            references usr_institution;

ALTER TABLE usr_institution ALTER COLUMN keyword TYPE varchar(32);