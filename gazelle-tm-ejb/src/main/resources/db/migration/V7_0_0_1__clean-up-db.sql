
-- Clean up old and unused preferences.
DELETE FROM cmn_application_preference WHERE preference_name = 'active_bpel_server_url';
DELETE FROM cmn_application_preference WHERE preference_name = 'active-bpel_server_url';
DELETE FROM cmn_application_preference WHERE preference_name = 'application_build_time';
DELETE FROM cmn_application_preference WHERE preference_name = 'application_profile';
DELETE FROM cmn_application_preference WHERE preference_name = 'application_database_initialization_flag';
DELETE FROM cmn_application_preference WHERE preference_name = 'application_db_drop_and_import';
DELETE FROM cmn_application_preference WHERE preference_name = 'application_drools_loaded_at_startup';
DELETE FROM cmn_application_preference WHERE preference_name = 'certificates_generated_by_host';
DELETE FROM cmn_application_preference WHERE preference_name = 'content-security-policy';
DELETE FROM cmn_application_preference WHERE preference_name = 'content-security-policy-report-only';
DELETE FROM cmn_application_preference WHERE preference_name = 'gazelle_master_model_application';
DELETE FROM cmn_application_preference WHERE preference_name = 'key_length_encryption';
DELETE FROM cmn_application_preference WHERE preference_name = 'photo_base_url';
DELETE FROM cmn_application_preference WHERE preference_name = 'photo_basedir';
DELETE FROM cmn_application_preference WHERE preference_name = 'service_schematron_validator';
DELETE FROM cmn_application_preference WHERE preference_name = 'service_xdsevs';

-- Clean up duplicated preferences with bad typo (should not be all lower case).
DELETE FROM cmn_application_preference WHERE preference_name = 'cache-control';
DELETE FROM cmn_application_preference WHERE preference_name = 'strict-transport-security';
DELETE FROM cmn_application_preference WHERE preference_name = 'x-frame-options';

-- Fix potential problem of column type for description column in tm_test_instance table.
ALTER TABLE tm_test_instance RENAME COLUMN description TO description_to_delete;
ALTER TABLE tm_test_instance ADD COLUMN description text;
UPDATE tm_test_instance SET description = cast(description_to_delete as text);
ALTER TABLE tm_test_instance DROP COLUMN description_to_delete;

-- Fix for potential missing constraint keyword for test type
ALTER TABLE ONLY public.tm_test_type DROP CONSTRAINT IF EXISTS uk_6q0ah7pcfjbqk2hr0a3aoa2t6;
ALTER TABLE ONLY public.tm_test_type ADD CONSTRAINT uk_6q0ah7pcfjbqk2hr0a3aoa2t6 UNIQUE (keyword);
