ALTER TABLE tm_status_results add column if not exists rank integer;
ALTER TABLE tm_object_type_status add column if not exists  rank integer;
ALTER TABLE tm_system_in_session_status add column if not exists  rank integer;

UPDATE tm_status_results set rank = 7 where keyword = 'atrisk' ;
UPDATE tm_status_results set rank = 6 where keyword = 'failed' ;
UPDATE tm_status_results set rank = 5 where keyword = 'nograding' ;
UPDATE tm_status_results set rank = 4 where keyword = 'nopeer' ;
UPDATE tm_status_results set rank = 3 where keyword = 'withdrawn' ;
UPDATE tm_status_results set rank = 2 where keyword = 'passed' ;
