package net.ihe.gazelle.objects.action;

import net.ihe.gazelle.objects.model.ObjectAttribute;
import net.ihe.gazelle.objects.model.ObjectInstance;
import net.ihe.gazelle.objects.model.ObjectInstanceAttribute;
import net.ihe.gazelle.objects.model.ObjectType;
import net.ihe.gazelle.tm.systems.model.SystemInSession;
import net.ihe.gazelle.users.model.Institution;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class ObjectManagerTest {

    @Spy
    private final ObjectManager objectManager = new ObjectManager();

    @Test
    public void compareObjectInstanceWithSearchParameterWithNullObjectInstance() {
        assert objectManager.compareObjectInstanceWithSearchParameter(null);
    }

    @Test
    public void objectInstanceContainsSearchParameterInName() {
        objectManager.setSearchParameter("searchParameter");
        ObjectInstance objectInstance = new ObjectInstance();
        objectInstance.setName("searchParameter");
        ObjectType objectType = new ObjectType();
        objectType.setDescription("description");
        objectInstance.setObject(objectType);
        assert objectManager.compareObjectInstanceWithSearchParameter(objectInstance);
    }

    @Test
    public void objectInstanceContainsSearchParameterInDescription() {
        objectManager.setSearchParameter("searchParameter");
        ObjectInstance objectInstance = new ObjectInstance();
        objectInstance.setName("name");
        ObjectType objectType = new ObjectType();
        objectType.setDescription("searchParameter");
        objectInstance.setObject(objectType);
        assert objectManager.compareObjectInstanceWithSearchParameter(objectInstance);
    }

    @Test
    public void objectInstanceNotContainsSearchParameterInDescription() {
        objectManager.setSearchParameter("searchParameter");
        ObjectInstance objectInstance = new ObjectInstance();
        objectInstance.setName("name");
        ObjectType objectType = new ObjectType();
        objectType.setDescription("description");
        objectInstance.setObject(objectType);
        Institution institution = new Institution();
        institution.setName("test");
        institution.setKeyword("test");
        Mockito.doReturn(institution).when(objectManager).getInstitutionInfo(objectInstance);
        assertFalse(objectManager.compareObjectInstanceWithSearchParameter(objectInstance));
    }

    @Test
    public void objectInstanceNotContainsSearchParameterInAttributeKeyword() {
        objectManager.setSearchParameter("searchParameter");
        ObjectInstance objectInstance = new ObjectInstance();
        objectInstance.setName("name");
        ObjectType objectType = new ObjectType();
        objectType.setDescription("description");
        objectInstance.setObject(objectType);

        Set<ObjectInstanceAttribute> objectInstanceAttributes = new HashSet<>();
        ObjectInstanceAttribute objectInstanceAttribute = new ObjectInstanceAttribute();
        objectInstanceAttribute.setAttribute(new ObjectAttribute(new ObjectType(), "searchParameterKeyword","description"));
        objectInstanceAttributes.add(objectInstanceAttribute);
        objectInstance.setObjectInstanceAttributes(objectInstanceAttributes);
        assert objectManager.compareObjectInstanceWithSearchParameter(objectInstance);
    }

    @Test
    public void verifyObjectTypeForRenderingTreeSampleTestWithNullObjectType() {
        assert objectManager.verifyObjectTypeForRenderingTreeSample(null);
    }

    @Test
    public void verifyObjectTypeForRenderingTreeSampleTestWithMatchingSearchParameter() {
        objectManager.setSearchParameter("searchParameter");
        ObjectType objectTypeTop = new ObjectType();

        ObjectInstance objectInstance = new ObjectInstance();
        objectInstance.setName("searchParameter");
        ObjectType objectTypebottom = new ObjectType();
        objectTypebottom.setDescription("description");
        objectInstance.setObject(objectTypebottom);

        Set<ObjectInstance> objectInstanceSet = new HashSet<>();
        objectInstanceSet.add(objectInstance);
        objectTypeTop.setObjectInstances(objectInstanceSet);
        assert objectManager.verifyObjectTypeForRenderingTreeSample(objectTypeTop);
    }

    @Test
    public void verifyObjectTypeForRenderingTreeSampleTestWithoutMatchingSearchParameter() {
        objectManager.setSearchParameter("searchParameter");
        ObjectType objectTypeTop = new ObjectType();

        ObjectInstance objectInstance = new ObjectInstance();
        objectInstance.setName("name");
        ObjectType objectTypebottom = new ObjectType();
        objectTypebottom.setDescription("description");
        objectInstance.setObject(objectTypebottom);

        Set<ObjectInstance> objectInstanceSet = new HashSet<>();
        objectInstanceSet.add(objectInstance);
        objectTypeTop.setObjectInstances(objectInstanceSet);
        Institution institution = new Institution();
        institution.setName("test");
        institution.setKeyword("test");
        Mockito.doReturn(institution).when(objectManager).getInstitutionInfo(objectInstance);
        assertFalse(objectManager.verifyObjectTypeForRenderingTreeSample(objectTypeTop));
    }

    @Test
    public void verifyObjectTypeForRenderingTreeSampleTestWithNullObjectInstanceSet() {
        objectManager.setSearchParameter("searchParameter");
        ObjectType objectTypeTop = new ObjectType();
        objectTypeTop.setObjectInstances(null);
        assert objectManager.verifyObjectTypeForRenderingTreeSample(objectTypeTop);
    }

    @Test
    public void verifySystemInSessionWithNullSystemInSession() {
        objectManager.setSearchParameter("searchParameter");
        assert objectManager.verifySystemInSessionWithoutSamplesForRenderingTreeSample(null);
    }

    @Test
    public void verifySystemInSessionWithNullObjectInstanceSet() {
        objectManager.setSearchParameter("searchParameter");
        SystemInSession systemInSession = new SystemInSession();
        systemInSession.setObjectInstances(null);
        assert objectManager.verifySystemInSessionWithoutSamplesForRenderingTreeSample(systemInSession);
    }

    @Test
    public void verifySystemInSessionWithObjectInstanceSetMatchingSearchParameter() {
        objectManager.setSearchParameter("searchParameter");
        SystemInSession systemInSession = new SystemInSession();
        ObjectInstance objectInstance = new ObjectInstance();
        objectInstance.setName("searchParameter");
        ObjectType objectTypebottom = new ObjectType();
        objectTypebottom.setDescription("description");
        objectInstance.setObject(objectTypebottom);

        Set<ObjectInstance> objectInstanceSet = new HashSet<>();
        objectInstanceSet.add(objectInstance);
        systemInSession.setObjectInstances(objectInstanceSet);
        assert objectManager.verifySystemInSessionWithoutSamplesForRenderingTreeSample(systemInSession);
    }

    @Test
    public void verifySystemInSessionWithObjectInstanceSetNotMatchingSearchParameter() {
        objectManager.setSearchParameter("searchParameter");
        SystemInSession systemInSession = new SystemInSession();
        ObjectInstance objectInstance = new ObjectInstance();
        objectInstance.setName("name");
        ObjectType objectTypebottom = new ObjectType();
        objectTypebottom.setDescription("description");
        objectInstance.setObject(objectTypebottom);

        Set<ObjectInstance> objectInstanceSet = new HashSet<>();
        objectInstanceSet.add(objectInstance);
        systemInSession.setObjectInstances(objectInstanceSet);
        Institution institution = new Institution();
        institution.setName("test");
        institution.setKeyword("test");
        Mockito.doReturn(institution).when(objectManager).getInstitutionInfo(objectInstance);
        assertFalse(objectManager.verifySystemInSessionWithoutSamplesForRenderingTreeSample(systemInSession));
    }
}
