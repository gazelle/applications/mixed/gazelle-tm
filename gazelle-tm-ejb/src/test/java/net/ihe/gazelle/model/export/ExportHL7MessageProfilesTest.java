package net.ihe.gazelle.model.export;


import net.ihe.gazelle.tf.action.converter.Hl7MessageProfileToXmlConverter;
import net.ihe.gazelle.tf.model.*;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import static net.ihe.gazelle.files.FilesUtils.loadFile;

public class ExportHL7MessageProfilesTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExportHL7MessageProfilesTest.class);

    private List<Hl7MessageProfile> hl7MessageProfiles;
    private int idCounter;

    @Before
    public void setUp() throws Exception {

        this.hl7MessageProfiles = new ArrayList<>();
        this.idCounter = 1;

        hl7MessageProfiles.add(createMessageProfile());
        hl7MessageProfiles.add(createMessageProfile());
    }

    @After
    public void tearDown() throws Exception {
        hl7MessageProfiles = null;
    }

    private int getId(){
        return idCounter ++;
    }

    private Hl7MessageProfile createMessageProfile(){

        Hl7MessageProfile hl7MessageProfile = new Hl7MessageProfile();
        int id = getId();

        hl7MessageProfile.setProfileOid("1.2.3."+id);
        hl7MessageProfile.setHl7Version("v"+id);
        hl7MessageProfile.setDomain(new Domain("DomainKW"+id, "DomainName"+id));
        hl7MessageProfile.setActor(new Actor("KWActor" + id, "NameActor" + id));
        hl7MessageProfile.setTransaction(new Transaction("TRKW"+id, "Name"+id, "Desc"+id));
        hl7MessageProfile.setMessageType("12345678912345"+id);
        hl7MessageProfile.setMessageOrderControlCode("MCO"+id);
        hl7MessageProfile.setAffinityDomains(createAffinityDomains(2));

        return hl7MessageProfile;
    }

    private List<AffinityDomain> createAffinityDomains(int size){
        List<AffinityDomain> affinityDomains = new ArrayList<>();
        for(int i = 0 ; i<size ; i++){
            affinityDomains.add(createAffinityDomain());
        }
        return affinityDomains;
    }

    private AffinityDomain createAffinityDomain(){
        AffinityDomain affinityDomain = new AffinityDomain();
        int id = getId();

        affinityDomain.setId(id);
        affinityDomain.setKeyword("ADKW"+id);
        affinityDomain.setLabelToDisplay("ADLabel"+id);
        affinityDomain.setDescription("This is Affinity Domain n"+id);
        return affinityDomain;
    }

    @Test
    public void configurationsToXmlTest() {

        Hl7MessageProfileToXmlConverter converter = new Hl7MessageProfileToXmlConverter();
        String xmlMessageProfiles = null;
        try {
            xmlMessageProfiles = converter.hl7MessageProfilesToXml(hl7MessageProfiles);
        } catch (JAXBException e){
            LOGGER.error("", e);
            Assert.fail("Error converting rules to xml : " + e.getMessage());
        }

        if (xmlMessageProfiles != null && !xmlMessageProfiles.isEmpty()){
            File file = loadFile("/model/hl7MessageProfiles.xml");
            try {
                String expectedResult = new String(FileUtils.readFileToByteArray(file), StandardCharsets.UTF_8);
                Assert.assertEquals("Result of xml transformation does not match expected format",
                        expectedResult, xmlMessageProfiles);
            }catch (IOException e){
                LOGGER.error("", e);
                Assert.fail("Error reading file with expected result : " + e.getMessage());
            }
        } else {
            Assert.fail("Error converting rules to xml : result should not be null nor empty");
        }
    }
}
