package net.ihe.gazelle.model.export;

import net.ihe.gazelle.tf.action.converter.StandardToXmlConverter;
import net.ihe.gazelle.tf.model.NetworkCommunicationType;
import net.ihe.gazelle.tf.model.Standard;
import net.ihe.gazelle.tf.model.Transaction;
import net.ihe.gazelle.tf.model.export.StandardIE;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import static net.ihe.gazelle.files.FilesUtils.loadFile;

public class ExportStandardTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExportStandardTest.class);

    private List<Standard> standards;
    private int idCounter;

    @Before
    public void setUp() throws Exception {

        this.standards = new ArrayList<>();
        this.idCounter = 1;

        standards.add(createStandard());
        standards.add(createStandard());
    }

    @After
    public void tearDown() throws Exception {
        standards = null;
    }

    private int getId(){
        return idCounter ++;
    }

    private Standard createStandard(){
        Standard standard = new Standard();
        int standardId = getId();
        standard.setId(standardId);
        standard.setKeyword("StandardKW" + standardId);
        standard.setName("StandardName" + standardId);
        standard.setVersion("v"+standardId);
        standard.setNetworkCommunicationType(NetworkCommunicationType.values()[standardId % NetworkCommunicationType.values().length]);
        standard.setUrl("https://www.test" + standardId + ".com");
        List<Transaction> transactions = new ArrayList<>();
        for (int i = 0 ; i < standardId ; i++){
            transactions.add(createTransaction());
        }
        standard.setTransactions(transactions);
        return standard;
    }

    private Transaction createTransaction(){
        Transaction transaction = new Transaction();
        int transactionId = getId();
        transaction.setKeyword("TransactionKW" + transactionId);
        transaction.setName("TransactionName" + transactionId);
        return transaction;
    }

    @Test
    public void standardsToXmlTest() {
        StandardToXmlConverter tfRuleToXmlConverter = new StandardToXmlConverter();
        String xmlRules = null;
        try {
            xmlRules = tfRuleToXmlConverter.standardToXml(standards);
        } catch (JAXBException e){
            LOGGER.error("", e);
            Assert.fail("Error converting rules to xml : " + e.getMessage());
        }

        if (xmlRules != null && !xmlRules.isEmpty()){
            File file = loadFile("/model/standards.xml");
            try {
                String expectedResult = new String(FileUtils.readFileToByteArray(file), StandardCharsets.UTF_8);
                Assert.assertEquals("Result of xml transformation does not match expected format",
                        expectedResult, xmlRules);
            }catch (IOException e){
                LOGGER.error("", e);
                Assert.fail("Error reading file with expected result : " + e.getMessage());
            }
        } else {
            Assert.fail("Error converting rules to xml : result should not be null nor empty");
        }
    }

    @Test
    public void xmlToStandardsTest() {

        File file = loadFile("/model/standards.xml");
        String fileToImport = null;
        boolean match;
        try {
            fileToImport = new String(FileUtils.readFileToByteArray(file), StandardCharsets.UTF_8);
        }catch (IOException e){
            LOGGER.error("", e);
            Assert.fail("Error reading file with xml Standards : " + e.getMessage());
        }

        StandardToXmlConverter standardToXmlConverter = new StandardToXmlConverter();
        StandardIE importedstandard = null;
        try {
            importedstandard = standardToXmlConverter.xmlToStandards(fileToImport);
        } catch (JAXBException e){
            LOGGER.error("", e);
            Assert.fail("Error converting xml to rules : " + e.getMessage());
        }
        if (importedstandard != null && importedstandard.getStandards() != null && !importedstandard.getStandards().isEmpty()
                && importedstandard.getStandards().size()== 2 ){

            for (Standard standard : importedstandard.getStandards()){
                match = false;
                for (Standard expectedStandard : standards){
                    if (standardToXmlConverter.match(standard, expectedStandard)){
                        match = true;
                    }
                }
                if (!match){
                    Assert.fail("Imported Standard does not correspond to expected objects");
                }
            }
        } else {
            Assert.fail("Error converting xml to standard : result should not be null nor empty");
        }
    }

}
