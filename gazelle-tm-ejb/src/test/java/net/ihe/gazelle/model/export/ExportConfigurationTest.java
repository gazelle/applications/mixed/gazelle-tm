package net.ihe.gazelle.model.export;

import net.ihe.gazelle.tf.model.*;
import net.ihe.gazelle.tm.configurations.converter.ConfigurationTypeMappedWithAIPOToXmlConverter;
import net.ihe.gazelle.tm.configurations.model.*;
import org.apache.commons.io.FileUtils;
import org.junit.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import static net.ihe.gazelle.files.FilesUtils.loadFile;

public class ExportConfigurationTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExportConfigurationTest.class);

    private List<ConfigurationTypeMappedWithAIPO> configurations;
    private int idCounter;

    @Before
    public void setUp() throws Exception {

        this.configurations = new ArrayList<>();
        this.idCounter = 1;

        configurations.add(createConfiguration());
        configurations.add(createConfiguration());
    }

    @After
    public void tearDown() throws Exception {
        configurations = null;
    }

    private int getId(){
        return idCounter ++;
    }

    private ConfigurationTypeMappedWithAIPO createConfiguration(){
        ConfigurationTypeMappedWithAIPO configuration = new ConfigurationTypeMappedWithAIPO();
        int configurationId = getId();
        configuration.setId(configurationId);

        configuration.setActorIntegrationProfileOption(createAIPO());

        List<ConfigurationTypeWithPortsWSTypeAndSopClass> configurationTypeWithPortsWSTypeAndSopClassList = new ArrayList<>();
        configurationTypeWithPortsWSTypeAndSopClassList.add(createCTWPWSTASC());
        configuration.setListOfConfigurationTypes(configurationTypeWithPortsWSTypeAndSopClassList);
        return configuration;
    }

    private ActorIntegrationProfileOption createAIPO(){
        ActorIntegrationProfileOption aipo = new ActorIntegrationProfileOption();
        int aipoId = getId();

        aipo.setId(aipoId);
        aipo.setActorIntegrationProfile(createAIP());
        aipo.setIntegrationProfileOption(new IntegrationProfileOption("KWOption" + aipoId,
                "NameOption" + aipoId, "Unused Description" + aipoId));
        aipo.setMaybeSupportive(true);
        return aipo;
    }

    private ActorIntegrationProfile createAIP(){
        ActorIntegrationProfile aip = new ActorIntegrationProfile();
        int aipId = getId();

        aip.setId(aipId);
        aip.setActor(new Actor("KWActor" + aipId, "NameActor" + aipId));
        aip.setIntegrationProfile(new IntegrationProfile(null,
                "KWIP" + aipId, "NameIP" + aipId, "Unused Description" + aipId));
        List<ProfileLink> profileLinks = new ArrayList<>();
        profileLinks.add(new ProfileLink());
        return aip;
    }

    private ConfigurationTypeWithPortsWSTypeAndSopClass createCTWPWSTASC(){
        ConfigurationTypeWithPortsWSTypeAndSopClass conf = new ConfigurationTypeWithPortsWSTypeAndSopClass();
        ConfigurationType configurationType = new ConfigurationType();
        WebServiceType webServiceType = new WebServiceType();
        WSTransactionUsage wsTransactionUsage = new WSTransactionUsage();

        int id = getId();
        conf.setId(id);

        configurationType.setCategory("Category"+id);
        configurationType.setTypeName("Type"+id);
        configurationType.setUsedForProxy(false);

        webServiceType.setDescription("Description"+id);
        webServiceType.setNeedsOID(false);
        webServiceType.setProfile(new IntegrationProfile(null,
                "KWIP" + id, "NameIP" + id, "Unused Description" + id));

        wsTransactionUsage.setUsage("Usage"+id);
        wsTransactionUsage.setTransaction(new Transaction("TRKW"+id, "Name"+id, "Desc"+id));


        conf.setTransportLayer(new TransportLayer("TLKW"+id, "TLName"+id));
        conf.setWsTRansactionUsage(wsTransactionUsage);
        conf.setWebServiceType(webServiceType);
        conf.setConfigurationType(configurationType);
        return conf;
    }

    @Test
    public void configurationsToXmlTest() {

        ConfigurationTypeMappedWithAIPOToXmlConverter converter = new ConfigurationTypeMappedWithAIPOToXmlConverter();
        String xmlConfigs = null;
        try {
            xmlConfigs = converter.configurationsToXml(configurations);
        } catch (JAXBException e){
            LOGGER.error("", e);
            Assert.fail("Error converting rules to xml : " + e.getMessage());
        }

        if (xmlConfigs != null && !xmlConfigs.isEmpty()){
            File file = loadFile("/model/configurations.xml");
            try {
                String expectedResult = new String(FileUtils.readFileToByteArray(file), StandardCharsets.UTF_8);
                Assert.assertEquals("Result of xml transformation does not match expected format",
                        expectedResult, xmlConfigs);
            }catch (IOException e){
                LOGGER.error("", e);
                Assert.fail("Error reading file with expected result : " + e.getMessage());
            }
        } else {
            Assert.fail("Error converting rules to xml : result should not be null nor empty");
        }
    }
}
