package net.ihe.gazelle.model.export;

import net.ihe.gazelle.tf.model.*;
import net.ihe.gazelle.tm.configurations.action.converter.OIDRootDefinitionToXmlConverter;
import net.ihe.gazelle.tm.configurations.model.OIDRequirement;
import net.ihe.gazelle.tm.configurations.model.OIDRootDefinition;
import net.ihe.gazelle.tm.configurations.model.OIDRootDefinitionIE;
import net.ihe.gazelle.tm.configurations.model.OIDRootDefinitionLabel;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static net.ihe.gazelle.files.FilesUtils.loadFile;

public class ExportOIDRootDefinitionTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExportOIDRootDefinitionTest.class);

    private List<OIDRootDefinition> oidRootDefinitions;
    private int idCounter;

    /**
     * Set up for tests.
     */
    @Before
    public void setUp() {

        this.oidRootDefinitions = new ArrayList<>();
        this.idCounter = 0;

        // Prepare ObjectType to export
        this.oidRootDefinitions.add(createOIDRootDefinition());
        this.oidRootDefinitions.add(createOIDRootDefinition());
    }

    /**
     * Get a new id for fake entities.
     *
     * @return an integer.
     */
    private int getId() {
        return idCounter++;
    }

    /**
     * Create a fake OIDRootDefinition.
     *
     * @return an {@link OIDRootDefinition}
     */
    private OIDRootDefinition createOIDRootDefinition() {

        Integer id = getId();

        OIDRootDefinition newOIDRootDefinition = new OIDRootDefinition("" + id, id, createLabel());

        Set<OIDRequirement> oidRequirements = new HashSet<>();
        oidRequirements.add(createOIDRequirement(newOIDRootDefinition));
        oidRequirements.add(createOIDRequirement(newOIDRootDefinition));

        newOIDRootDefinition.setOidRequirements(oidRequirements);
        newOIDRootDefinition.setId(id);
        return newOIDRootDefinition;
    }

    /**
     * Create a label for an OIDRootDefinition.
     *
     * @return an {@link OIDRootDefinitionLabel}
     */
    public OIDRootDefinitionLabel createLabel() {
        Integer id = getId();
        OIDRootDefinitionLabel newLabel = new OIDRootDefinitionLabel("label" + id);
        newLabel.setId(id);
        return newLabel;
    }

    /**
     * Create an OID Requirement
     *
     * @param oidRootDefinition {@link OIDRootDefinition} on which the requirement applies.
     * @return an {@link OIDRequirement}
     */
    public OIDRequirement createOIDRequirement(OIDRootDefinition oidRootDefinition) {

        Integer id = getId();
        List<ActorIntegrationProfileOption> actorIntegrationProfileOptionList = new ArrayList<>();
        actorIntegrationProfileOptionList.add(createAIPO());
        actorIntegrationProfileOptionList.add(createAIPO());
        OIDRequirement oidRequirement = new OIDRequirement("label" + id, actorIntegrationProfileOptionList,
                oidRootDefinition, "prefix" + id);
        oidRequirement.setId(id);
        return oidRequirement;
    }

    /**
     * Create an AIPO
     * @return an {@link ActorIntegrationProfileOption}
     */
    private ActorIntegrationProfileOption createAIPO() {
        ActorIntegrationProfileOption aipo = new ActorIntegrationProfileOption();
        int aipoId = getId();

        aipo.setId(aipoId);
        aipo.setActorIntegrationProfile(createAIP());
        aipo.setIntegrationProfileOption(new IntegrationProfileOption("KWOption" + aipoId,
                "NameOption" + aipoId, "Unused Description" + aipoId));
        aipo.setMaybeSupportive(true);
        return aipo;
    }

    /**
     * Create an AIP
     * @return an {@link ActorIntegrationProfile}
     */
    private ActorIntegrationProfile createAIP() {
        ActorIntegrationProfile aip = new ActorIntegrationProfile();
        int aipId = getId();

        aip.setId(aipId);
        aip.setActor(new Actor("KWActor" + aipId, "NameActor" + aipId));
        aip.setIntegrationProfile(new IntegrationProfile(null,
                "KWIP" + aipId, "NameIP" + aipId, "Unused Description" + aipId));
        List<ProfileLink> profileLinks = new ArrayList<>();
        profileLinks.add(new ProfileLink());
        return aip;
    }

    /**
     * Test converting OIDRootDefinition to XML
     */
    @Test
    public void oidRootDefinitionToXmlTest() {
        OIDRootDefinitionToXmlConverter oidRootDefinitionToXmlConverter = new OIDRootDefinitionToXmlConverter();
        String oidRootDefinitionXML = null;
        try {
            oidRootDefinitionXML = oidRootDefinitionToXmlConverter.oidRootDefinitionToXml(this.oidRootDefinitions);
        } catch (JAXBException e) {
            LOGGER.error("", e);
            Assert.fail("Error converting Samples' Type to xml : " + e.getMessage());
        }

        if (oidRootDefinitionXML != null && !oidRootDefinitionXML.isEmpty()) {
            File file = loadFile("/model/oidRootDefinition.xml");
            try {
                String expectedResult = new String(FileUtils.readFileToByteArray(file), StandardCharsets.UTF_8);
                Assert.assertEquals("Result of xml transformation does not match expected format",
                        expectedResult, oidRootDefinitionXML);
            } catch (IOException e) {
                LOGGER.error("", e);
                Assert.fail("Error reading file with expected result : " + e.getMessage());
            }
        } else {
            Assert.fail("Error converting Samples' Type to xml : result should not be null nor empty");
        }
    }

    /**
     * Test converting XML to OID Root Definition.
     */
    @Test
    public void xmlToOIDRootDefinitionTest() {

        File file = loadFile("/model/oidRootDefinition.xml");
        String fileToImport = null;
        boolean match;
        try {
            fileToImport = new String(FileUtils.readFileToByteArray(file), StandardCharsets.UTF_8);
        } catch (IOException e) {
            LOGGER.error("", e);
            Assert.fail("Error reading file with xml Samples Types : " + e.getMessage());
        }

        OIDRootDefinitionToXmlConverter oidRootDefinitionToXmlConverter = new OIDRootDefinitionToXmlConverter();
        OIDRootDefinitionIE importedOIDRootDefinitions = null;
        try {
            importedOIDRootDefinitions = oidRootDefinitionToXmlConverter.xmlToOIDRootDefinition(fileToImport);
        } catch (JAXBException e) {
            LOGGER.error("", e);
            Assert.fail("Error converting xml to rules : " + e.getMessage());
        }
        if (!(importedOIDRootDefinitions != null && importedOIDRootDefinitions.getOidRootDefinitions() != null && !importedOIDRootDefinitions.getOidRootDefinitions().isEmpty()
                && importedOIDRootDefinitions.getOidRootDefinitions().size() == 2)) {
            Assert.fail("Error converting xml to standard : result should not be null nor empty");
        }
    }
}
