package net.ihe.gazelle.dao;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.ssov7.gum.client.application.Group;
import net.ihe.gazelle.ssov7.gum.client.application.User;
import net.ihe.gazelle.tm.session.TestingSessionService;
import net.ihe.gazelle.tm.systems.model.SystemInSession;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.Collections;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ApplicationPreferenceManager.class, EntityManagerService.class, TestingSession.class, SystemInSessionDAOImpl.class})
@PowerMockIgnore("javax.management.*")
public class SystemInSessionDAOTest {

    ApplicationPreferenceManager mockedApplicationManager;
    User user;
    SystemInSession systemInSession;
    SystemInSessionDAO systemInSessionDAO;
    SystemInSessionDAO spiedSystemInSessionDAO;

    private void mockApplicationManager() {
        if (mockedApplicationManager == null) {
            mockedApplicationManager = PowerMockito.mock(ApplicationPreferenceManager.class);
            PowerMockito.when(mockedApplicationManager.getGazelleHL7ConformanceStatementsPath()).thenReturn("");
            PowerMockito.when(mockedApplicationManager.getGazelleDicomConformanceStatementsPath()).thenReturn("");
        }
    }

    @Before
    public void setUp() throws Exception {
        mockApplicationManager();

        PowerMockito.mockStatic(EntityManagerService.class);
        PowerMockito.mockStatic(TestingSession.class);
        TestingSessionService testingSessionServiceMock = PowerMockito.mock(TestingSessionService.class);
        PowerMockito.spy(SystemInSessionDAOImpl.class);
        PowerMockito.doReturn(testingSessionServiceMock).when(SystemInSessionDAOImpl.class, "getTestingSessionService");

        user = new User();
        user.setGroupIds(Collections.singleton(Group.GRP_TEST_DESIGNER));

        systemInSession = new SystemInSession();
        systemInSession.setId(1);

        systemInSessionDAO = new SystemInSessionDAOImpl();
        spiedSystemInSessionDAO = Mockito.spy(systemInSessionDAO);

    }

    @Test
    public void systemIsNotRelatedToUserWhenListIsNullTest() {
        Mockito.doReturn(new ArrayList<>())
                .when(spiedSystemInSessionDAO).getSystemInSessionRelatedToUser(Mockito.<String>any(), Mockito.<TestingSession>any());

        assertFalse(spiedSystemInSessionDAO.isSystemInSessionRelatedToUser(systemInSession, user.getId()));
    }

    @Test
    public void systemIsRelatedToUserWhenListMatchTest() {
        Mockito.doReturn(Collections.singletonList(systemInSession))
                .when(spiedSystemInSessionDAO).getSystemInSessionRelatedToUser(Mockito.<String>any(), Mockito.<TestingSession>any());

        assertTrue(spiedSystemInSessionDAO.isSystemInSessionRelatedToUser(systemInSession, user.getId()));
    }

    @Test
    public void systemIsNotRelatedToUserWhenListDoesntMatchTest() {
        SystemInSession systemInSessionNotLinkedToUser = new SystemInSession();
        systemInSessionNotLinkedToUser.setId(42);
        Mockito.doReturn(Collections.singletonList(systemInSession))
                .when(spiedSystemInSessionDAO).getSystemInSessionRelatedToUser(Mockito.<String>any(), Mockito.<TestingSession>any());

        assertFalse(spiedSystemInSessionDAO.isSystemInSessionRelatedToUser(systemInSessionNotLinkedToUser, user.getId()));
    }
}
