package net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.instance;

import net.ihe.gazelle.tm.gazelletest.automatedstep.domain.instance.AutomatedTestStepInstanceData;
import net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.instance.service.AutomatedTestStepInstanceInputService;
import net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.instance.service.AutomatedTestStepInstanceInputServiceImpl;
import net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.stub.AutomatedTestStepInstanceFileSystemDAOStub;
import net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.stub.AutomatedTestStepInstanceInputDAOStub;
import net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.stub.TestInstanceDataDAOStub;
import net.ihe.gazelle.tm.gazelletest.domain.AutomatedTestStepInputDomain;
import net.ihe.gazelle.tm.gazelletest.model.definition.AutomatedTestStepEntity;
import net.ihe.gazelle.tm.gazelletest.model.definition.AutomatedTestStepInput;
import net.ihe.gazelle.tm.gazelletest.model.instance.AutomatedTestStepInstanceInput;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstance;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestStepsInstance;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.testng.AssertJUnit.*;


public class AutomatedTestStepInstanceInputServiceTest {

    private static final int TEST_STEP_ID = 1;
    private static final int TEST_STEP_INSTANCE_ID = 0;

    private static final int AUTOMATED_INPUT_INSTANCE_ID = 0;

    private TestInstanceDataDAOStub testInstanceDataDAOStub;
    private AutomatedTestStepInstanceInputDAOStub automatedTestStepInstanceInputDAOStub;
    private AutomatedTestStepInstanceFileSystemDAOStub automatedTestStepInstanceFileSystemDAOStub;
    private AutomatedTestStepInstanceInputService service;

    @Before
    public void init() {
        testInstanceDataDAOStub = new TestInstanceDataDAOStub();
        automatedTestStepInstanceInputDAOStub = new AutomatedTestStepInstanceInputDAOStub();
        automatedTestStepInstanceFileSystemDAOStub = new AutomatedTestStepInstanceFileSystemDAOStub();
        service = new AutomatedTestStepInstanceInputServiceImpl(testInstanceDataDAOStub, automatedTestStepInstanceInputDAOStub, automatedTestStepInstanceFileSystemDAOStub);
    }

    @Test
    public void test_createAutomatedTestStepInstanceInput() {
        AutomatedTestStepInputDomain domain = new AutomatedTestStepInputDomain("key", "label", "description", "type", true);
        AutomatedTestStepInstanceInput input = service.createAutomatedTestStepInstanceInput(domain, 1);
        assertEquals(domain.getKey(), input.getKey());
        assertEquals(1, automatedTestStepInstanceInputDAOStub.getTestDao().size());
        assertEquals(1, automatedTestStepInstanceInputDAOStub.getId().intValue());
    }

    @Test
    public void test_saveInputData() {
        AutomatedTestStepInputDomain domain = new AutomatedTestStepInputDomain("key", "label", "description", "type", true);
        automatedTestStepInstanceInputDAOStub.createAutomatedTestStepInstanceInput(domain, 1);
        assertEquals(1, automatedTestStepInstanceInputDAOStub.getTestDao().size());
        assertNull(automatedTestStepInstanceInputDAOStub.getTestDao().get(AUTOMATED_INPUT_INSTANCE_ID).getTestStepDataId());
        service.saveInputData(0, 1, "fileName", "content");
        assertEquals(1, testInstanceDataDAOStub.getTestStepsDataList().size());
        assertEquals(1, automatedTestStepInstanceFileSystemDAOStub.getFileContents().size());
        assertNotNull(automatedTestStepInstanceInputDAOStub.getTestDao().get(AUTOMATED_INPUT_INSTANCE_ID).getTestStepDataId());
    }

    @Test
    public void test_readInputData() {
        automatedTestStepInstanceFileSystemDAOStub.saveInputDataFile(1, "fileName", "fileContent");
        AutomatedTestStepInstanceData instanceData = new AutomatedTestStepInstanceData();
        instanceData.setFileName("fileName");
        String fileContent = service.readInputData(instanceData);
        assertEquals("fileContent", fileContent);
    }

    @Test
    public void test_deleteInputData() {
        automatedTestStepInstanceFileSystemDAOStub.saveInputDataFile(1, "fileName", "fileContent");
        testInstanceDataDAOStub.createAutomatedTestStepData("fileName", 1);
        AutomatedTestStepInputDomain domain = new AutomatedTestStepInputDomain("key", "label", "description", "type", true);
        automatedTestStepInstanceInputDAOStub.createAutomatedTestStepInstanceInput(domain, 1);
        AutomatedTestStepInstanceData instanceData = new AutomatedTestStepInstanceData();
        instanceData.setFileName("fileName");
        instanceData.setTestStepInstanceInputId(AUTOMATED_INPUT_INSTANCE_ID);
        instanceData.setTestStepDataId(1);
        assertEquals(1, testInstanceDataDAOStub.getTestStepsDataList().size());
        assertEquals(1, automatedTestStepInstanceInputDAOStub.getTestDao().size());
        assertEquals(1, automatedTestStepInstanceFileSystemDAOStub.getFileContents().size());
        service.deleteInputData(instanceData);
        assertEquals(0, testInstanceDataDAOStub.getTestStepsDataList().size());
        assertEquals(0, automatedTestStepInstanceFileSystemDAOStub.getFileContents().size());
        assertEquals(1, automatedTestStepInstanceInputDAOStub.getTestDao().size());
        assertNull(automatedTestStepInstanceInputDAOStub.getTestDao().get(AUTOMATED_INPUT_INSTANCE_ID).getTestStepDataId());
    }

    @Test
    public void test_getInputDataFromTestStepInstance() {
        // Given
        AutomatedTestStepEntity automatedTestStepEntity = new AutomatedTestStepEntity();
        automatedTestStepEntity.setTestScriptID("testScriptID");
        automatedTestStepEntity.setId(TEST_STEP_ID);
        List<AutomatedTestStepInput> automatedTestStepInputs = new ArrayList<>();
        automatedTestStepInputs.add(new AutomatedTestStepInput("key", "label", "description", "type", true, automatedTestStepEntity));
        automatedTestStepEntity.setInputs(automatedTestStepInputs);
        TestStepsInstance testStepsInstance = new TestStepsInstance();
        testStepsInstance.setId(TEST_STEP_INSTANCE_ID);
        testStepsInstance.setTestSteps(automatedTestStepEntity);
        AutomatedTestStepInputDomain domain = new AutomatedTestStepInputDomain("key", "label", "description", "type", true);
        domain.setId(0);
        AutomatedTestStepInstanceInput input = automatedTestStepInstanceInputDAOStub.createAutomatedTestStepInstanceInput(domain, TEST_STEP_INSTANCE_ID);
        List<AutomatedTestStepInstanceData> inputDataList = service.getInputDataFromTestStepInstance(testStepsInstance);
        AutomatedTestStepInstanceData instanceData = inputDataList.get(0);
        assertNull(instanceData.getFileName());

        service.saveInputData(input.getId(), TEST_STEP_INSTANCE_ID, "fileName", "content");

        // When
        inputDataList = service.getInputDataFromTestStepInstance(testStepsInstance);

        // Then
        assertEquals(1, inputDataList.size());
        instanceData = inputDataList.get(0);
        assertEquals("fileName", instanceData.getFileName());
        assertEquals("key", instanceData.getKey());
        assertEquals(1, instanceData.getTestStepDataId().intValue());
    }

    @Test
    public void test_getLatestUploadDate() {
        // Given
        AutomatedTestStepEntity automatedTestStepEntity = new AutomatedTestStepEntity();
        automatedTestStepEntity.setTestScriptID("testScriptID");
        automatedTestStepEntity.setId(TEST_STEP_ID);
        List<AutomatedTestStepInput> automatedTestStepInputs = new ArrayList<>();
        automatedTestStepInputs.add(new AutomatedTestStepInput("key", "label", "description", "type", true, automatedTestStepEntity));
        automatedTestStepEntity.setInputs(automatedTestStepInputs);
        TestStepsInstance testStepsInstance = new TestStepsInstance();
        testStepsInstance.setId(TEST_STEP_INSTANCE_ID);
        testStepsInstance.setTestSteps(automatedTestStepEntity);
        AutomatedTestStepInputDomain domain = new AutomatedTestStepInputDomain("key", "label", "description", "type", true);
        domain.setId(0);
        AutomatedTestStepInstanceInput input = automatedTestStepInstanceInputDAOStub.createAutomatedTestStepInstanceInput(domain, TEST_STEP_INSTANCE_ID);
        Date lastUpdate = input.getLastChanged();
        List<AutomatedTestStepInstanceData> inputDataList = service.getInputDataFromTestStepInstance(testStepsInstance);
        AutomatedTestStepInstanceData instanceData = inputDataList.get(0);
        assertNull(instanceData.getFileName());

        // When
        Date lastUpdateTest = service.getLatestUploadDate(TEST_STEP_INSTANCE_ID);

        //Then
        assertEquals(lastUpdate, lastUpdateTest);
    }

    @Test
    public void test_getInstanceInputForTestInstance() {
        // Given
        AutomatedTestStepEntity automatedTestStepEntity = new AutomatedTestStepEntity();
        automatedTestStepEntity.setTestScriptID("testScriptID");
        automatedTestStepEntity.setId(TEST_STEP_ID);
        List<AutomatedTestStepInput> automatedTestStepInputs = new ArrayList<>();
        automatedTestStepInputs.add(new AutomatedTestStepInput("key", "label", "description", "type", true, automatedTestStepEntity));
        automatedTestStepEntity.setInputs(automatedTestStepInputs);
        TestInstance testInstance = new TestInstance();
        testInstance.setId(1);
        TestStepsInstance testStepsInstance = new TestStepsInstance();
        testStepsInstance.setId(TEST_STEP_INSTANCE_ID);
        testStepsInstance.setTestSteps(automatedTestStepEntity);
        List<TestStepsInstance> testStepsInstances = new ArrayList<>();
        testStepsInstances.add(testStepsInstance);
        testInstance.setTestStepsInstanceList(testStepsInstances);
        AutomatedTestStepInputDomain domain = new AutomatedTestStepInputDomain("key", "label", "description", "type", true);
        domain.setId(0);
        AutomatedTestStepInstanceInput input = automatedTestStepInstanceInputDAOStub.createAutomatedTestStepInstanceInput(domain, TEST_STEP_INSTANCE_ID);
        List<AutomatedTestStepInstanceData> inputDataList = service.getInputDataFromTestStepInstance(testStepsInstance);
        AutomatedTestStepInstanceData instanceData = inputDataList.get(0);
        assertNull(instanceData.getFileName());

        // When
        List<AutomatedTestStepInstanceInput> inputsFromTest = service.getInstanceInputForTestInstance(testInstance);
        List<AutomatedTestStepInstanceInput> inputsFromTestStep = service.getInstanceInputForTestStepInstance(testStepsInstance);

        //Then
        assertEquals(1, inputsFromTest.size());
        assertEquals(input, inputsFromTest.get(0));
        assertEquals(1, inputsFromTestStep.size());
        assertEquals(input, inputsFromTestStep.get(0));
    }
}
