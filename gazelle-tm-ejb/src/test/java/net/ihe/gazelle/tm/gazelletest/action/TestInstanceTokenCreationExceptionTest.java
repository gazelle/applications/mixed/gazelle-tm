package net.ihe.gazelle.tm.gazelletest.action;

import org.junit.Test;

public class TestInstanceTokenCreationExceptionTest {
    /**
     * Dummy Test for coverage
     * @throws TestInstanceTokenCreationException testException
     */
    @Test(expected = TestInstanceTokenCreationException.class)
    public void generateException() throws TestInstanceTokenCreationException {
        throw new TestInstanceTokenCreationException("Test");
    }
    /**
     * Dummy Test for coverage
     * @throws TestInstanceTokenCreationException testException
     */
    @Test(expected = TestInstanceTokenCreationException.class)
    public void generateException1() throws TestInstanceTokenCreationException {
        throw new TestInstanceTokenCreationException("Test",new IllegalArgumentException("test"));
    }

    /**
     * Dummy Test for coverage
     *
     * @throws TestInstanceTokenCreationException testException
     */
    @Test(expected = TestInstanceTokenCreationException.class)
    public void generateException2() throws TestInstanceTokenCreationException {
        throw new TestInstanceTokenCreationException( new IllegalArgumentException("test"));
    }

    /**
     * Dummy Test for coverage
     *
     * @throws TestInstanceTokenCreationException testException
     */
    @Test(expected = TestInstanceTokenCreationException.class)
    public void generateException3() throws TestInstanceTokenCreationException {
        throw new TestInstanceTokenCreationException( );
    }



}