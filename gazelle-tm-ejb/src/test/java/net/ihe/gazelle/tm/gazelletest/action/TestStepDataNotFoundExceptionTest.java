package net.ihe.gazelle.tm.gazelletest.action;

import org.junit.Test;

import static org.junit.Assert.*;

public class TestStepDataNotFoundExceptionTest {
    /**
     * Dummy Test for coverage
     * @throws TestStepDataNotFoundException testException
     */
    @Test(expected = TestStepDataNotFoundException.class)
    public void generateException() throws TestStepDataNotFoundException {
        throw new TestStepDataNotFoundException("Test");
    }
    /**
     * Dummy Test for coverage
     * @throws TestStepDataNotFoundException testException
     */
    @Test(expected = TestStepDataNotFoundException.class)
    public void generateException1() throws TestStepDataNotFoundException {
        throw new TestStepDataNotFoundException("Test",new IllegalArgumentException("test"));
    }

    /**
     * Dummy Test for coverage
     *
     * @throws TestStepDataNotFoundException testException
     */
    @Test(expected = TestStepDataNotFoundException.class)
    public void generateException2() throws TestStepDataNotFoundException {
        throw new TestStepDataNotFoundException( new IllegalArgumentException("test"));
    }

    /**
     * Dummy Test for coverage
     *
     * @throws TestStepDataNotFoundException testException
     */
    @Test(expected = TestStepDataNotFoundException.class)
    public void generateException3() throws TestStepDataNotFoundException {
        throw new TestStepDataNotFoundException( );
    }

}