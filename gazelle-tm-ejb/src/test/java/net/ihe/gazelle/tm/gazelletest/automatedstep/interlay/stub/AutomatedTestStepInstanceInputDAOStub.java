package net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.stub;

import net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.instance.dao.AutomatedTestStepInstanceInputDAO;
import net.ihe.gazelle.tm.gazelletest.domain.AutomatedTestStepInputDomain;
import net.ihe.gazelle.tm.gazelletest.model.instance.AutomatedTestStepInstanceInput;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstance;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AutomatedTestStepInstanceInputDAOStub implements AutomatedTestStepInstanceInputDAO {

    private final Map<Integer, AutomatedTestStepInstanceInput> testDao;
    private Integer id;

    public AutomatedTestStepInstanceInputDAOStub() {
        testDao = new HashMap<>();
        id = 0;
    }

    @Override
    public AutomatedTestStepInstanceInput createAutomatedTestStepInstanceInput(AutomatedTestStepInputDomain inputDefinition, Integer testStepsInstanceId) {
        AutomatedTestStepInstanceInput input = new AutomatedTestStepInstanceInput(testStepsInstanceId, inputDefinition);
        input.setLastChanged(new Timestamp(System.currentTimeMillis()));
        input.setLastModifierId("userId");
        testDao.put(id, input);
        id++;
        return input;
    }

    @Override
    public List<AutomatedTestStepInstanceInput> getInstanceInputWithoutData(Integer testStepsInstanceId) {
        List<AutomatedTestStepInstanceInput> instanceInputs = new ArrayList<>();
        for (AutomatedTestStepInstanceInput input : testDao.values()) {
            if (input.getTestStepsInstanceId().equals(testStepsInstanceId)) {
                instanceInputs.add(input);
            }
        }
        return instanceInputs;
    }

    @Override
    public List<AutomatedTestStepInstanceInput> getInstanceInputForTestInstance(TestInstance testInstance) {
        return new ArrayList<>(testDao.values());
    }

    @Override
    public void setTestStepDataIdToInstanceInput(Integer automatedTestStepInputId, Integer automatedTestStepDataId) {
        AutomatedTestStepInstanceInput input = testDao.get(automatedTestStepInputId);
        input.setTestStepDataId(automatedTestStepDataId);
        testDao.remove(automatedTestStepInputId);
        testDao.put(automatedTestStepInputId, input);
    }

    public Map<Integer, AutomatedTestStepInstanceInput> getTestDao() {
        return testDao;
    }

    public Integer getId() {
        return id;
    }
}
