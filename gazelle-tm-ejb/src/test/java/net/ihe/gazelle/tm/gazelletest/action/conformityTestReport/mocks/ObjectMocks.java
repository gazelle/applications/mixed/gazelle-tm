package net.ihe.gazelle.tm.gazelletest.action.conformityTestReport.mocks;

import net.ihe.gazelle.ssov7.gum.client.application.Role;
import net.ihe.gazelle.ssov7.gum.client.application.User;
import net.ihe.gazelle.tm.gazelletest.model.definition.*;
import net.ihe.gazelle.tm.gazelletest.model.instance.MonitorInSession;
import net.ihe.gazelle.tm.gazelletest.model.instance.Status;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstance;
import net.ihe.gazelle.tm.systems.model.InstitutionSystem;
import net.ihe.gazelle.tm.systems.model.System;
import net.ihe.gazelle.tm.systems.model.SystemInSession;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import net.ihe.gazelle.users.model.DelegatedOrganization;
import net.ihe.gazelle.users.model.Institution;
import net.ihe.gazelle.users.model.InstitutionType;

import java.util.*;

public class ObjectMocks {
   private int number;

   public ObjectMocks(int number) {
      this.number = number;
   }

   public User provideUserWithMandatoryFields(Role role) {
      User user = new User();
      user.setId("testing" + number);
      user.setFirstName("void");
      user.setLastName("void");
      user.setOrganizationId(createInstitutionWithMandatoryFields().getKeyword());
      user.setEmail("jle" + number + "@kereval.com");
      user.setGroupIds(Collections.singleton(role.getName()));
      ++number;
      return user;
   }

   public Institution createInstitutionWithMandatoryFields() {
      Institution institution = new Institution();
      institution.setId(1);
      institution.setKeyword("Ker1");
      institution.setUrl("http://kereval.com");
      institution.setName("Kereval1");
      InstitutionType institutionType = new InstitutionType();
      institutionType.setType("Company1");
      institution.setInstitutionType(institutionType);
      return institution;
   }

   public Institution createDelegatedInstitutionWithMandatoryFields() {
      DelegatedOrganization delegatedOrganization = new DelegatedOrganization();
      delegatedOrganization.setId(2);
      delegatedOrganization.setExternalId("externalId");
      delegatedOrganization.setIdpId("idpId");
      delegatedOrganization.setKeyword("Delegated_Ker2");
      delegatedOrganization.setUrl("http://kereval.com2");
      delegatedOrganization.setName("Delegated Kereval2");
      InstitutionType institutionType = new InstitutionType();
      institutionType.setType("Company2");
      delegatedOrganization.setInstitutionType(institutionType);
      return delegatedOrganization;
   }

   public Institution createInstitutionWithMandatoryFieldsByName(String name) {
      Institution institution = new Institution();
      institution.setId(1);
      institution.setKeyword("Ker" + number);
      institution.setUrl("http://kereval.com");
      institution.setName(name);
      InstitutionType institutionType = new InstitutionType();
      institutionType.setType("Company" + number);
      institution.setInstitutionType(institutionType);
      ++number;
      return institution;
   }

   public Institution createInstitutionWithMandatoryFieldsByKeyword(String keyword) {
      Institution institution = new Institution();
      institution.setId(1);
      institution.setKeyword(keyword);
      institution.setUrl("http://kereval.com");
      institution.setName("Kereval" + number);
      InstitutionType institutionType = new InstitutionType();
      institutionType.setType("Company" + number);
      institution.setInstitutionType(institutionType);
      ++number;
      return institution;
   }

   public TestingSession provideTestingSessionWithMandatoryFields() {
      TestingSession testingSession = new TestingSession();
      testingSession.setName("Connectathon France");
      testingSession.setId(1);
      testingSession.setRegistrationDeadlineDate(new Date());
      testingSession.setDescription("Testing session test");
      testingSession.setContactLastname("toto");
      testingSession.setContactFirstname("tutu");
      testingSession.setContactEmail("tututoto@ihe.com");
      testingSession.setSessionClosed(false);
      testingSession.setAllowOneCompanyPlaySeveralRolesInGroupTests(false);
      testingSession.setAllowOneCompanyPlaySeveralRolesInP2PTests(false);
      testingSession.setAllowParticipantsStartGroupTests(false);
      List<SystemInSession> systemInSessionList = new ArrayList<>();
      systemInSessionList.add(createSystemIneSessionWithMandatoryFields());
      testingSession.setSystemInSessions(systemInSessionList);
      return testingSession;
   }

   public TestingSession provideTestingSessionWithoutSystemInSession() {
      TestingSession testingSession = new TestingSession();
      testingSession.setName("Connectathon France");
      testingSession.setId(1);
      testingSession.setSessionClosed(false);
      testingSession.setRegistrationDeadlineDate(new Date());
      testingSession.setDescription("Testing session test");
      testingSession.setContactLastname("toto");
      testingSession.setContactFirstname("tutu");
      testingSession.setContactEmail("tututoto@ihe.com");
      testingSession.setAllowOneCompanyPlaySeveralRolesInGroupTests(false);
      testingSession.setAllowOneCompanyPlaySeveralRolesInP2PTests(false);
      testingSession.setAllowParticipantsStartGroupTests(false);
      return testingSession;
   }

   public TestingSession provideTestingSessionWithMandatoryFields(int id) {
      TestingSession testingSession = new TestingSession();
      testingSession.setName("Connectathon France");
      testingSession.setId(id);
      testingSession.setSessionClosed(false);
      testingSession.setRegistrationDeadlineDate(new Date());
      testingSession.setDescription("Testing session test");
      testingSession.setContactLastname("toto");
      testingSession.setContactFirstname("tutu");
      testingSession.setContactEmail("tututoto@ihe.com");
      testingSession.setAllowOneCompanyPlaySeveralRolesInGroupTests(false);
      testingSession.setAllowOneCompanyPlaySeveralRolesInP2PTests(false);
      testingSession.setAllowParticipantsStartGroupTests(false);
      List<SystemInSession> systemInSessionList = new ArrayList<>();
      systemInSessionList.add(createSystemIneSessionWithMandatoryFields());
      testingSession.setSystemInSessions(systemInSessionList);
      return testingSession;
   }

   public TestingSession provideTestingSessionWithMandatoryFields(String name) {
      TestingSession testingSession = new TestingSession();
      testingSession.setName("Connectathon France");
      testingSession.setId(1);
      testingSession.setSessionClosed(false);
      testingSession.setRegistrationDeadlineDate(new Date());
      testingSession.setDescription("Testing session test");
      testingSession.setContactLastname("toto");
      testingSession.setContactFirstname("tutu");
      testingSession.setContactEmail("tututoto@ihe.com");
      testingSession.setAllowOneCompanyPlaySeveralRolesInGroupTests(false);
      testingSession.setAllowOneCompanyPlaySeveralRolesInP2PTests(false);
      testingSession.setAllowParticipantsStartGroupTests(false);
      List<SystemInSession> systemInSessionList = new ArrayList<>();
      systemInSessionList.add(
            createSystemIneSessionWithMandatoryFields(name, provideTestingSessionWithMandatoryFields()));
      testingSession.setSystemInSessions(systemInSessionList);
      return testingSession;
   }

   public SystemInSession createSystemIneSessionWithMandatoryFields() {
      SystemInSession sis = new SystemInSession();
      sis.setSystem(createSystemWithMandatoryFields());
      sis.setTestingSession(provideTestingSessionWithoutSystemInSession());
      return sis;
   }

   public SystemInSession createSystemIneSessionWithMandatoryFields(TestingSession session) {
      SystemInSession sis = new SystemInSession();
      sis.setSystem(createSystemWithMandatoryFields());
      sis.setTestingSession(session);
      return sis;
   }

   public SystemInSession createSystemIneSessionWithMandatoryFields(String name, TestingSession session) {
      SystemInSession sis = new SystemInSession();
      sis.setSystem(createSystemWithMandatoryFields(name));
      sis.setTestingSession(session);
      return sis;
   }

   public System createSystemWithMandatoryFields() {
      System sys = new SystemMock();
      sys.setName("TestName");
      sys.setKeyword("test");
      sys.setId(1);
      Role roleUser = new Role(Role.USER);
      sys.setOwnerUserId(provideUserWithMandatoryFields(roleUser).getId());
      sys.setInstitutionForCreation(createInstitutionWithMandatoryFieldsByName("Kereval1"));
      Set<InstitutionSystem> institutionSet = new HashSet<>();
      InstitutionSystem institutionSystem = new InstitutionSystem(sys,
            createInstitutionWithMandatoryFieldsByName("Kereval1"));
      institutionSet.add(institutionSystem);
      sys.setInstitutionSystems(institutionSet);
      return sys;
   }

   private System createSystemWithMandatoryFields(String name) {
      System sys = new SystemMock();
      sys.setName("TestName");
      sys.setKeyword("test");
      sys.setId(1);
      Role roleUser = new Role(Role.USER);
      sys.setOwnerUserId(provideUserWithMandatoryFields(roleUser).getId());
      sys.setInstitutionForCreation(createInstitutionWithMandatoryFieldsByName(name));
      Set<InstitutionSystem> institutionSet = new HashSet<>();
      InstitutionSystem institutionSystem = new InstitutionSystem(sys,
            createInstitutionWithMandatoryFieldsByName("Kereval1"));
      institutionSet.add(institutionSystem);
      sys.setInstitutionSystems(institutionSet);
      return sys;
   }

   public List<TestRoles> createTestRolesWithMandatoryFields() {
      List<TestRoles> list = new ArrayList<>();
      TestRoles testRoles = new TestRoles();
      MetaTest metaTest = new MetaTest();
      metaTest.setKeyword("MetaTestDescription");
      metaTest.setKeyword("MetaTestKeyword");
      testRoles.setMetaTest(metaTest);
      list.add(testRoles);

      testRoles = new TestRoles();
      testRoles.setTest(createConnectathonTest());
      testRoles.setNumberOfTestsToRealize(2);
      testRoles.setTestOption(createTestOptionWithMandatoryFields());
      list.add(testRoles);
      testRoles = new TestRoles();
      testRoles.setTest(createConnectathonTest());
      testRoles.setNumberOfTestsToRealize(2);
      testRoles.setTestOption(createTestOptionWithMandatoryFields());
      list.add(testRoles);
      return list;
   }

   public TestOption createTestOptionWithMandatoryFields() {
      TestOption testOption = new TestOption();
      testOption.setKeyword("option" + number);
      ++number;
      return testOption;
   }

   public Test createConnectathonTest() {
      Test test = new Test();
      test.setTestType(provideTestType_TYPE_CONNECTATHON());
      test.setKeyword("keyword" + number);
      ++number;
      test.setName("name");
      test.setShortDescription("super short");
      return test;
   }

   public TestType provideTestType_TYPE_CONNECTATHON() {
      TestType testType = new TestType();
      testType.setKeyword("connectathon");
      return testType;
   }

   public TestInstance createTestInstance(Status status) {
      TestInstanceMock testInstance = new TestInstanceMock();
      testInstance.setDescription("Super testInstance");
      testInstance.setId(123566);
      testInstance.setTest(createConnectathonTest());
      testInstance.setTestingSession(provideTestingSessionWithMandatoryFields());
      testInstance.setMonitorInSession(createMonitorInSessionWithMandatoryFields(testInstance.getTestingSession()));
      testInstance.setLastStatus(status);

      return testInstance;
   }

   public MonitorInSession createMonitorInSessionWithMandatoryFields(TestingSession testingSession) {
      MonitorInSession monitorInSession = new MonitorInSession();
      monitorInSession.setUserId(Role.ADMIN);
      monitorInSession.setTestingSession(testingSession);
      return monitorInSession;
   }
}