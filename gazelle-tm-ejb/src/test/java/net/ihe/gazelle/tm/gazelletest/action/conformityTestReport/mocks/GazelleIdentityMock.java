package net.ihe.gazelle.tm.gazelletest.action.conformityTestReport.mocks;

import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;

import java.util.ArrayList;
import java.util.List;

public class GazelleIdentityMock implements GazelleIdentity {

    private static final long serialVersionUID = 2788555944965874168L;
    private static final String NOT_PROVIDED = "Not provided";
    private String username;
    private String organization;
    private List<String> roles = new ArrayList<>();

    public GazelleIdentityMock() {
    }

    public GazelleIdentityMock(IdentityTokenBuilder builder) {
        this.username = builder.username;
        this.organization = builder.organization;
        this.roles = builder.roles;
    }

    public static IdentityTokenBuilder builder() {
        return new IdentityTokenBuilder();
    }

    public static class IdentityTokenBuilder {

        private String username;
        private String organization;
        private List<String> roles;


        public IdentityTokenBuilder withUsername(final String username) {
            this.username = username;
            return this;
        }

        public IdentityTokenBuilder withOrganization(final String organization) {
            this.organization = organization;
            return this;
        }

        public IdentityTokenBuilder withRoles(final List<String> roles) {
            this.roles = roles;
            return this;
        }

        public GazelleIdentityMock build() {
            return new GazelleIdentityMock(this);
        }
    }

    @Override
    public boolean isLoggedIn() {
        return username != null;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public String getFirstName() {
        return NOT_PROVIDED;
    }

    @Override
    public String getLastName() {
        return NOT_PROVIDED;
    }

    @Override
    public String getDisplayName() {
        return NOT_PROVIDED;
    }

    @Override
    public String getEmail() {
        return NOT_PROVIDED;
    }

    @Override
    public String getOrganisationKeyword() {
        return organization;
    }

    @Override
    public boolean hasRole(String role) {
        return roles.contains(role);
    }

    @Override
    public boolean hasPermission(String name, String action, Object... contexts) {
        throw new UnsupportedOperationException("Token identity does not implement permission resolving.");
    }

    @Override
    public boolean hasPermission(Object target, String action) {
        throw new UnsupportedOperationException("Token identity does not implement permission resolving.");
    }

    public boolean isAdmin() {
        return roles.contains("admin_role");
    }
}
