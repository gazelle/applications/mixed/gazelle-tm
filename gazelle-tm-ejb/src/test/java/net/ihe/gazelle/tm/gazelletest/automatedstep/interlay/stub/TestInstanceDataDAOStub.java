package net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.stub;

import net.ihe.gazelle.tm.gazelletest.automatedstep.domain.instance.AutomatedTestStepInstanceData;
import net.ihe.gazelle.tm.gazelletest.dao.TestInstanceDataDAO;
import net.ihe.gazelle.tm.gazelletest.model.instance.DataType;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstance;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestStepsData;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class TestInstanceDataDAOStub implements TestInstanceDataDAO {

    private final List<TestStepsData> testStepsDataList;

    public TestInstanceDataDAOStub() {
        testStepsDataList = new ArrayList<>();
    }

    @Override
    public TestStepsData createTestStepData(TestStepsData testStepsData) {
        testStepsDataList.add(testStepsData);
        return testStepsData;
    }

    @Override
    public TestStepsData createAutomatedTestStepData(String fileName, Integer testStepsInstanceId) {
        TestStepsData testStepsData = new TestStepsData();
        testStepsData.setId(1);
        testStepsData.setValue(fileName);
        testStepsData.setDataType(new DataType());
        testStepsData.setComment("");
        testStepsData.setWithoutFileName(true);
        testStepsData.setLastChanged(new Date());
        testStepsData.setLastModifierId("userId");
        testStepsDataList.add(testStepsData);
        return testStepsData;
    }

    @Override
    public TestStepsData instantiateTestStepDataFile(String userId, String fileName) {
        return null;
    }

    @Override
    public TestInstance updateTestInstance(TestInstance testInstance) {
        return null;
    }

    @Override
    public TestInstance getTestInstance(Integer testInstanceId) {
        return null;
    }

    @Override
    public void removeTestStepsDataFromTestStepsInstance(AutomatedTestStepInstanceData inputData) {
        for (Iterator<TestStepsData> it = testStepsDataList.iterator(); it.hasNext();) {
            TestStepsData data = it.next();
            if (inputData.getTestStepDataId().equals(data.getId())) {
                it.remove();
            }
        }
    }

    @Override
    public TestStepsData getTestStepDataById(Integer testStepsDataId) {
        for (TestStepsData data : testStepsDataList) {
            if (data.getId().equals(testStepsDataId)) {
                return data;
            }
        }
        return null;
    }

    @Override
    public void updateTestInstanceAndAddTestStepData(TestStepsData testStepData, Integer instanceId, String userId) {

    }

    @Override
    public void updateTestInstanceAndTestStepDataForLogs(TestStepsData testStepData, Integer instanceId, String userId) {

    }

    @Override
    public void saveFileInTestStepsData(Integer testStepsDataId, byte[] bytesInput) {

    }

    @Override
    public String getPath(String testStepsDataId) {
        return "";
    }

    public List<TestStepsData> getTestStepsDataList() {
        return testStepsDataList;
    }
}
