package net.ihe.gazelle.tm.gazelletest.action;

import org.junit.Test;

public class TokenNotFoundExceptionTest {

    /**
     * Dummy Test for coverage
     * @throws TokenNotFoundException testException
     */
    @Test(expected = TokenNotFoundException.class)
    public void generateException() throws TokenNotFoundException {
        throw new TokenNotFoundException("Test");
    }
    /**
     * Dummy Test for coverage
     * @throws TokenNotFoundException testException
     */
    @Test(expected = TokenNotFoundException.class)
    public void generateException1() throws TokenNotFoundException {
        throw new TokenNotFoundException("Test",new IllegalArgumentException("test"));
    }

    /**
     * Dummy Test for coverage
     *
     * @throws TokenNotFoundException testException
     */
    @Test(expected = TokenNotFoundException.class)
    public void generateException2() throws TokenNotFoundException {
        throw new TokenNotFoundException( new IllegalArgumentException("test"));
    }

    /**
     * Dummy Test for coverage
     *
     * @throws TokenNotFoundException testException
     */
    @Test(expected = TokenNotFoundException.class)
    public void generateException3() throws TokenNotFoundException {
        throw new TokenNotFoundException( );
    }

}