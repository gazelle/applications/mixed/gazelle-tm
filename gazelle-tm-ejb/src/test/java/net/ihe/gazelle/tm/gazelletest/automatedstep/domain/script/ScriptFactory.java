package net.ihe.gazelle.tm.gazelletest.automatedstep.domain.script;

import java.util.ArrayList;
import java.util.List;

public class ScriptFactory {

    public static Property createProperty(String key, String value) {
        return new Property(key, value);
    }

    public static List<Property> createPropertyAsList(String key, String value) {
        List<Property> properties = new ArrayList<>();
        properties.add(createProperty(key, value));
        return properties;
    }

    public static Step createStep(String id, String type, List<Property> properties) {
        return new Step(id, type, properties);
    }

    public static List<Step> createStepAsList(String id, String type, List<Property> properties) {
        List<Step> steps = new ArrayList<>();
        steps.add(createStep(id, type, properties));
        return steps;
    }

    public static InputDefinition createInputDefinition(String key, String label, String description, String type, boolean required) {
        return new InputDefinition(key, label, description, type, required);
    }

    public static List<InputDefinition> createInputAsList(String key, String label, String description, String type, boolean required) {
        List<InputDefinition> inputDefinitions = new ArrayList<>();
        inputDefinitions.add(createInputDefinition(key, label, description, type, required));
        return inputDefinitions;
    }

    public static Script createScript(String name, List<InputDefinition> inputDefinitions, List<Step> steps) {
        return new Script(name, inputDefinitions, steps);
    }

    public static InputContent createInputContent(String key, String value) {
        return new InputContent(key, value);
    }

    public static List<InputContent> createInputContentList(String key, String value) {
        List<InputContent> inputContents = new ArrayList<>();
        inputContents.add(createInputContent(key, value));
        return inputContents;
    }

    public static MaestroRequest createMaestroScript(String clientCallback, String callbackAuthorization, Script script, List<InputContent> inputContents) {
        MaestroRequest request = new MaestroRequest(callbackAuthorization, script, inputContents);
        request.setClientCallback(clientCallback);
        return request;
    }
}
