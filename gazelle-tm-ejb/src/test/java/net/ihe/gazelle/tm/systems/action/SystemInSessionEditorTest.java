package net.ihe.gazelle.tm.systems.action;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.common.pages.Authorization;
import net.ihe.gazelle.dao.SystemInSessionDAO;
import net.ihe.gazelle.dao.SystemInSessionDAOImpl;
import net.ihe.gazelle.menu.Authorizations;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import net.ihe.gazelle.ssov7.gum.client.application.Role;
import net.ihe.gazelle.ssov7.gum.client.application.User;
import net.ihe.gazelle.tm.session.TestingSessionService;
import net.ihe.gazelle.tm.systems.model.System;
import net.ihe.gazelle.tm.systems.model.SystemInSession;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import java.util.Calendar;
import java.util.Date;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ApplicationPreferenceManager.class, User.class, Role.class, Authorizations.class, SystemInSessionDAOImpl.class})
@PowerMockIgnore("javax.management.*")
public class SystemInSessionEditorTest {

   private static Authorization mockedMonitorOrTsmOfSessionAuth;
   ApplicationPreferenceManager mockedApplicationManager;
   SystemInSessionDAO mockedSystemInSessionDAO;
   User ownerUser;
   GazelleIdentity identity;
   SystemInSession systemInSession;
   SystemInSessionEditor systemInSessionEditor;
   SystemInSessionEditor spiedSystemInSessionEditor;

   private void mockStatics() throws Exception {
      if (mockedApplicationManager == null) {
         mockedApplicationManager = PowerMockito.mock(ApplicationPreferenceManager.class);

         PowerMockito.mockStatic(ApplicationPreferenceManager.class);
         PowerMockito.when(mockedApplicationManager.getGazelleHL7ConformanceStatementsPath()).thenReturn("");
         PowerMockito.when(mockedApplicationManager.getGazelleDicomConformanceStatementsPath()).thenReturn("");
      }
      TestingSessionService testingSessionServiceMock = PowerMockito.mock(TestingSessionService.class);

      if (mockedMonitorOrTsmOfSessionAuth == null) {
         PowerMockito.spy(Authorizations.class);
         PowerMockito.doReturn(testingSessionServiceMock).when(Authorizations.class,"getTestingSessionService");
         mockedMonitorOrTsmOfSessionAuth = PowerMockito.mock(Authorizations.MONITOR_OR_TESTING_SESSION_ADMIN_OF_CURRENT_TESTING_SESSION.getClass());
         Whitebox.setInternalState(Authorizations.class, "MONITOR_OR_TESTING_SESSION_ADMIN_OF_CURRENT_TESTING_SESSION",
               mockedMonitorOrTsmOfSessionAuth);
      }
      PowerMockito.spy(SystemInSessionDAOImpl.class);
      PowerMockito.doReturn(testingSessionServiceMock).when(SystemInSessionDAOImpl.class, "getTestingSessionService");
   }

   @Before
   public void setUp() throws Exception {
      //Prepare stubs
      ownerUser = new User();
      ownerUser.setId("ownerUser");

      identity = Mockito.mock(GazelleIdentity.class);
      Mockito.doReturn(true).when(identity).hasRole(Role.VENDOR);
      Mockito.doReturn("testUser").when(identity).getUsername();

      mockStatics();

      TestingSession testingSession = new TestingSession();
      testingSession.setAllowParticipantRegistration(true);
      testingSession.setRegistrationDeadlineDate(addHour(new Date(), 1));
      testingSession.setSessionClosed(false);

      mockedSystemInSessionDAO = Mockito.mock(SystemInSessionDAOImpl.class);
      mockIdentityRelatedToSystem(false);

      systemInSession = new SystemInSession();
      systemInSession.setId(1);
      systemInSession.setTestingSession(testingSession);
      systemInSession.setSystem(Mockito.mock(System.class));
      Mockito.doReturn(ownerUser.getId()).when(systemInSession.getSystem()).getOwnerUserId();

      // Prepare SUT
      systemInSessionEditor = new SystemInSessionEditor();
      systemInSessionEditor.setSystemInSession(systemInSession);
      systemInSessionEditor.setSystem(systemInSession.getSystem());
      systemInSessionEditor.identity = this.identity;
      systemInSessionEditor.systemInSessionDAO = mockedSystemInSessionDAO;
      spiedSystemInSessionEditor = Mockito.spy(systemInSessionEditor);
   }

   @Test
   public void testTestingSessionAdminCanViewSystemNotes() {
      // specific setup
      grantIdentityMonitorOrTsmOfTestingSession();

      assertThat(systemInSessionEditor.canViewSystemNotes(), is(true));

      systemInSession.getTestingSession().setSessionClosed(true);
      assertThat(systemInSessionEditor.canViewSystemNotes(), is(true));

      // specific tear-down
      ungrantIdentityMonitorOrTsmOfTestingSession();
   }

   @Test
   public void testRelatedVendorCanViewSystemNotes() {
      mockIdentityRelatedToSystem(true);
      assertThat(spiedSystemInSessionEditor.canViewSystemNotes(), is(true));
   }

   @Test
   public void testRelatedVendorCanViewSystemNotesSessionClosed() {
      mockIdentityRelatedToSystem(true);
      systemInSession.getTestingSession().setSessionClosed(true);
      assertThat(spiedSystemInSessionEditor.canViewSystemNotes(), is(true));
   }

   @Test
   public void testUnrelatedUserCannotViewSystemNotes() {
      mockIdentityRelatedToSystem(false);
      assertThat(spiedSystemInSessionEditor.canViewSystemNotes(), is(false));
   }

   /**
    * Test that a vendor cannot edit a system of its organisation. In regular situation (member of the orga, while
    * registration open), only Creator or Vendor-admin can edit a system.
    */
   @Test
   public void testRelatedVendorCannotEditSystem() {
      mockIdentityRelatedToSystem(true);
      assertThat(spiedSystemInSessionEditor.canEditSystemInSession(), is(false));
   }

   @Test
   public void testUnrelatedVendorCannotEditSystem() {
      assertThat(spiedSystemInSessionEditor.canEditSystemInSession(), is(false));
   }

   @Test
   public void testVendorCreatorCanEditSystem() {
      mockIdentityAsCreator();
      mockIdentityRelatedToSystem(true);
      assertThat(spiedSystemInSessionEditor.canEditSystemInSession(), is(true));
   }

   @Test
   public void testRelatedVendorAdminCanEditSystem() {
      mockIdentityRelatedToSystem(true);
      mockIdentityHasRole(Role.VENDOR_ADMIN);
      assertThat(spiedSystemInSessionEditor.canEditSystemInSession(), is(true));
   }

   @Test
   public void testRelatedVendorAdminCannotEditSystemWhenSessionClosed() {
      mockIdentityRelatedToSystem(true);
      mockIdentityHasRole(Role.VENDOR_ADMIN);
      systemInSession.getTestingSession().setSessionClosed(true);
      assertThat(spiedSystemInSessionEditor.canEditSystemInSession(), is(false));
   }

   @Test
   public void testRelatedVendorAdminCannotEditSystemWhenRegistrionOver() {
      mockIdentityRelatedToSystem(true);
      mockIdentityHasRole(Role.VENDOR_ADMIN);
      systemInSession.getTestingSession().setSessionClosed(false);
      systemInSession.getTestingSession().setRegistrationDeadlineDate(addHour(new Date(), -1));
      assertThat(spiedSystemInSessionEditor.canEditSystemInSession(), is(false));
   }

   @Test
   public void testAdminCanEditSystemWithSessionClosed() {
      mockIdentityHasRole(Role.ADMIN);
      systemInSession.getTestingSession().setSessionClosed(true);
      assertThat(spiedSystemInSessionEditor.canEditSystemInSession(), is(true));
   }

   @Test
   public void testAdminCanEditSystemWithRegistrationOver() {
      mockIdentityHasRole(Role.ADMIN);
      systemInSession.getTestingSession().setSessionClosed(false);
      systemInSession.getTestingSession().setRegistrationDeadlineDate(addHour(new Date(), -1));
      assertThat(spiedSystemInSessionEditor.canEditSystemInSession(), is(true));
   }

   @Test
   public void testAdminCanEditSystem() {
      mockIdentityHasRole(Role.ADMIN);
      assertThat(spiedSystemInSessionEditor.canEditSystemInSession(), is(true));
   }

   /**
    * Vendor-admins or creators of system having vendor_late_registration_role can still edit their system even if the
    * registrion is over.
    */
   @Test
   public void testVendorLateRegistrationCanEditSystemWhenRegistrationOver() {
      mockIdentityRelatedToSystem(true);
      mockIdentityHasRole(Role.VENDOR_ADMIN);
      mockIdentityHasRole(Role.VENDOR_LATE_REGISTRATION);
      systemInSession.getTestingSession().setRegistrationDeadlineDate(addHour(new Date(), -1));
      assertThat(spiedSystemInSessionEditor.canEditSystemInSession(), is(true));
   }

   /**
    * However Vendor-admins or creators of system having vendor_late_registration_role CANNOT edit their system if the
    * testing session is closed.
    */
   @Test
   public void testVendorLateRegistrationCannotEditSystemWhenSessionClosed() {
      mockIdentityRelatedToSystem(true);
      mockIdentityHasRole(Role.VENDOR_ADMIN);
      mockIdentityHasRole(Role.VENDOR_LATE_REGISTRATION);
      systemInSession.getTestingSession().setSessionClosed(true);
      assertThat(spiedSystemInSessionEditor.canEditSystemInSession(), is(false));
   }

   private void mockIdentityAsCreator() {
      Mockito.doReturn(true).when(identity).hasRole(Role.VENDOR_ADMIN);
      Mockito.doReturn("ownerUser").when(identity).getUsername();
   }

   private void mockIdentityHasRole(String role) {
      Mockito.doReturn(true).when(identity).hasRole(role);
   }

   private void mockIdentityRelatedToSystem(boolean value) {
      Mockito.when(mockedSystemInSessionDAO.isSystemInSessionRelatedToUser(systemInSession, identity.getUsername()))
            .thenReturn(value);
   }

   private static void grantIdentityMonitorOrTsmOfTestingSession() {
      PowerMockito.doReturn(true).when(mockedMonitorOrTsmOfSessionAuth).isGranted();
   }

   private static void ungrantIdentityMonitorOrTsmOfTestingSession() {
      PowerMockito.doReturn(false).when(mockedMonitorOrTsmOfSessionAuth).isGranted();
   }


   private Date addHour(Date date, int nbHour) {
      Calendar cal = Calendar.getInstance();
      cal.setTime(date);
      cal.add(Calendar.HOUR, nbHour);
      return cal.getTime();
   }
}
