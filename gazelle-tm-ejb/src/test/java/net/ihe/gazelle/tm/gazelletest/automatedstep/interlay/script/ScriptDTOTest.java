package net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.script;

import net.ihe.gazelle.tm.gazelletest.automatedstep.domain.script.*;
import net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.script.dto.InputDefinitionDTO;
import net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.script.dto.PropertyDTO;
import net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.script.dto.ScriptDTO;
import net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.script.dto.StepDTO;
import org.junit.Test;

import java.util.List;

import static org.testng.AssertJUnit.assertEquals;

public class ScriptDTOTest {

    @Test
    public void testPropertyDTO() {
        Property property = ScriptFactory.createProperty("key", "value");
        PropertyDTO dto = new PropertyDTO(property);
        assertEquals(property.getKey(), dto.getKey());
        assertEquals(property.getValue(), dto.getValue());
        assertEquals(property, dto.getProperty());
    }

    @Test
    public void testInputDTO() {
        InputDefinition input = ScriptFactory.createInputDefinition("key", "label", "description", "type", true);
        InputDefinitionDTO dto = new InputDefinitionDTO(input);
        assertEquals(input.getKey(), dto.getKey());
        assertEquals(input.getLabel(), dto.getLabel());
        assertEquals(input.getFormat(), dto.getFormat());
        assertEquals(input.getType(), dto.getType());
        assertEquals(input.getRequired(), dto.getRequired());
        assertEquals(input, dto.getInput());
    }

    @Test
    public void testStepDTO() {
        List<Property> properties = ScriptFactory.createPropertyAsList("key", "value");
        Step step = ScriptFactory.createStep("id", "type", properties);
        StepDTO dto = new StepDTO(step);
        assertEquals(step.getId(), dto.getId());
        assertEquals(step.getType(), dto.getType());
        assertEquals(step.getProperties().size(), dto.getProperties().size());
        assertEquals(step, dto.getStep());
    }

    @Test
    public void testScriptDTO() {
        List<Property> properties = ScriptFactory.createPropertyAsList("key", "value");
        List<InputDefinition> inputDefinitions = ScriptFactory.createInputAsList("key", "label", "description", "type", true);
        List<Step> steps = ScriptFactory.createStepAsList("id", "type", properties);
        Script script = ScriptFactory.createScript("name", inputDefinitions, steps);
        ScriptDTO dto = new ScriptDTO(script);
        assertEquals(script.getName(), dto.getName());
        assertEquals(script.getInputs().size(), dto.getInputs().size());
        assertEquals(script.getSteps().size(), dto.getSteps().size());
        assertEquals(script, dto.getScript());
    }
}
