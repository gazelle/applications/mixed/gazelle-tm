package net.ihe.gazelle.tm.gazelletest.action;

import net.ihe.gazelle.tm.gazelletest.model.instance.TestStepsInstance;
import net.ihe.gazelle.tm.tee.status.dao.TestStepsInstanceDAO;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.util.Date;

import static junitx.framework.Assert.assertNotEquals;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;


public class TestStepsInstanceCaptureServiceTest {

    TestStepsInstanceDAO mockTestStepsInstanceDAO = mock(TestStepsInstanceDAO.class);

    TestStepsInstanceCaptureService testStepsInstanceCaptureService = new TestStepsInstanceCaptureServiceImpl(mockTestStepsInstanceDAO);
    ArgumentCaptor<TestStepsInstance> argumentCaptor = ArgumentCaptor.forClass(TestStepsInstance.class);

    @Test
    public void startCaptureTest() {

        TestStepsInstance instance = new TestStepsInstance();

        testStepsInstanceCaptureService.startCapture(instance);
        verify(mockTestStepsInstanceDAO).mergeTestStepsInstance(argumentCaptor.capture());

        TestStepsInstance instancePassedToDAO = argumentCaptor.getValue();

        assertNotNull(instancePassedToDAO.getStartDate());
        assertNull(instancePassedToDAO.getEndDate());
    }

    @Test
    public void endCaptureTest() {
        Date now = new Date();
        TestStepsInstance instance = new TestStepsInstance();
        instance.setStartDate(now);

        testStepsInstanceCaptureService.endCapture(instance);
        verify(mockTestStepsInstanceDAO).mergeTestStepsInstance(argumentCaptor.capture());

        assertNotNull(argumentCaptor.getValue().getStartDate());
        assertEquals(now, argumentCaptor.getValue().getStartDate());
        assertNotNull(argumentCaptor.getValue().getEndDate());
    }

    @Test
    public void restartCaptureTest() {
        Date before = new Date(System.currentTimeMillis() - 10000);
        Date endDate = new Date(System.currentTimeMillis() + 10000);

        TestStepsInstance instanceToRestart = new TestStepsInstance();
        instanceToRestart.setStartDate(before);
        instanceToRestart.setEndDate(endDate);

        testStepsInstanceCaptureService.restartCapture(instanceToRestart);
        verify(mockTestStepsInstanceDAO).mergeTestStepsInstance(argumentCaptor.capture());

        TestStepsInstance instancePassedToDAO = argumentCaptor.getValue();
        assertNotEquals(before, instancePassedToDAO.getStartDate());
        assertNull(instancePassedToDAO.getEndDate());
        assertNotEquals(endDate, instancePassedToDAO.getEndDate());

    }

    @Test
    public void extendCaptureTest() {
        Date before = new Date(System.currentTimeMillis() - 10000);
        Date endDate = new Date(System.currentTimeMillis() + 10000);

        TestStepsInstance instanceToExtend = new TestStepsInstance();
        instanceToExtend.setStartDate(before);
        instanceToExtend.setEndDate(endDate);

        testStepsInstanceCaptureService.extendCapture(instanceToExtend);
        verify(mockTestStepsInstanceDAO).mergeTestStepsInstance(argumentCaptor.capture());

        TestStepsInstance instancePassedToDAO = argumentCaptor.getValue();
        assertEquals(before, instancePassedToDAO.getStartDate());
        assertNull(instancePassedToDAO.getEndDate());
    }

    @Test(expected = IllegalArgumentException.class)
    public void validateTestStepsInstanceTest() {
        testStepsInstanceCaptureService.startCapture(null);
        testStepsInstanceCaptureService.endCapture(null);
        testStepsInstanceCaptureService.restartCapture(null);
        testStepsInstanceCaptureService.extendCapture(null);
    }

    @Test(expected = IllegalStateException.class)
    public void endNeverStartedCaptureTest() {
        testStepsInstanceCaptureService.endCapture(new TestStepsInstance());
    }

    @Test(expected = IllegalStateException.class)
    public void extendNeverStartedCaptureTest() {
        testStepsInstanceCaptureService.extendCapture(new TestStepsInstance());
    }
}