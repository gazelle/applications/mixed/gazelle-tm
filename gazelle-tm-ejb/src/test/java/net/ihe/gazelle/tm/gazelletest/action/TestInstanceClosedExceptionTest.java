package net.ihe.gazelle.tm.gazelletest.action;

import org.junit.Test;

public class TestInstanceClosedExceptionTest {
    /**
     * Dummy Test for coverage
     *
     * @throws TestInstanceClosedException testException
     */
    @Test(expected = TestInstanceClosedException.class)
    public void generateException() throws TestInstanceClosedException {
        throw new TestInstanceClosedException("Test");
    }

    /**
     * Dummy Test for coverage
     *
     * @throws TestInstanceClosedException testException
     */
    @Test(expected = TestInstanceClosedException.class)
    public void generateException1() throws TestInstanceClosedException {
        throw new TestInstanceClosedException("Test", new IllegalArgumentException("test"));
    }

    /**
     * Dummy Test for coverage
     *
     * @throws TestInstanceClosedException testException
     */
    @Test(expected = TestInstanceClosedException.class)
    public void generateException2() throws TestInstanceClosedException {
        throw new TestInstanceClosedException(new IllegalArgumentException("test"));
    }

    /**
     * Dummy Test for coverage
     *
     * @throws TestInstanceClosedException testException
     */
    @Test(expected = TestInstanceClosedException.class)
    public void generateException3() throws TestInstanceClosedException {
        throw new TestInstanceClosedException();
    }
}