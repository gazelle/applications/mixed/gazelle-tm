package net.ihe.gazelle.tm.tee.util;

import org.junit.Test;
import org.w3c.dom.Document;

import java.util.Map;

import static org.junit.Assert.assertEquals;

public class XMLUtilTest {


    @Test
    public void getNodeValueFromDocumentTest() {
        Document document = XMLUtil.loadDocument("src/test/resources/externaltool/testreportsamples/validMinimal.xml");
        assertEquals("PASSED", XMLUtil.getNodeValueFromDocument(document, "status"));
    }

    @Test
    public void listAllAttributesOfNodeFromDocumentValidTest() {
        Document document = XMLUtil.loadDocument("src/test/resources/externaltool/testreportsamples/validMinimal.xml");
        Map<String, String> attributesAndValue = XMLUtil.listAllAttributesOfNodeFromDocument(document, "numberOfTests");
        System.out.println(attributesAndValue.get("failed"));
        assert (attributesAndValue.size() == 6);
        assert (attributesAndValue.get("total").equals("2"));
        assert (attributesAndValue.get("passed").equals("1"));
        assert (attributesAndValue.get("failed").equals("0"));
    }

    @Test
    public void listAllAttributesOfNodeFromDocumentInvalidTest() {
        Document document = XMLUtil.loadDocument("src/test/resources/externaltool/testreportsamples/invalidNoContent.xml");
        Map<String, String> attributesAndValue = XMLUtil.listAllAttributesOfNodeFromDocument(document, "numberOfTests");
        assert (attributesAndValue.size() == 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void listAllAttributesOfNodeFromDocumentException() {
        XMLUtil.listAllAttributesOfNodeFromDocument(null, "numberOfTests");
    }

    @Test(expected = IllegalArgumentException.class)
    public void listAllAttributesOfNodeFromDocumentException2() {
        Document document = XMLUtil.loadDocument("src/test/resources/externaltool/testreportsamples/invalidNoContent.xml");
        XMLUtil.listAllAttributesOfNodeFromDocument(document, null);
    }


    @Test(expected = IllegalArgumentException.class)
    public void getNodeValueFromDocumentException() {
        XMLUtil.getNodeValueFromDocument(null, "status");
    }
}
