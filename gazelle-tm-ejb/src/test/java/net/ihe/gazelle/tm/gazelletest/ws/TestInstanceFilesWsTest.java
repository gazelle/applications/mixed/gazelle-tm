package net.ihe.gazelle.tm.gazelletest.ws;

import net.ihe.gazelle.tm.gazelletest.action.*;
import net.ihe.gazelle.tm.gazelletest.model.instance.DataType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import javax.ws.rs.core.Response;
import java.io.IOException;


@RunWith(PowerMockRunner.class)
@PrepareForTest(DataType.class)
public class TestInstanceFilesWsTest {

    private static final String TOKEN_VALUE = "fb6a3a0c-7390-41c8-b11a-1ba796097d77";
    private static final String USER_NAME = "toto";
    private static final Integer TEST_INSTANCE_ID = 1234;

    private static final String authorizationHeader = "GazelleToken " + TOKEN_VALUE;

    @Mock
    private TestInstanceFilesService testInstanceFilesService;
    private TestInstanceFilesWs testInstanceFilesWs;

    private byte[] file;
    private Response response;


    @Before
    public void setUp() throws Exception {

        file = new byte[5];

        testInstanceFilesWs = new TestInstanceFilesWs();
        testInstanceFilesService = Mockito.mock(TestInstanceFilesService.class);
        testInstanceFilesWs.setTestInstanceFilesService(testInstanceFilesService);
    }

    /**
     * Test to upload logs completed params
     */
    @Test
    public void uploadLogsFile() throws InvalidTokenException, EmptyFileException, TestInstanceClosedException, TestInstanceNotFoundException,
            IOException {
        Mockito.doNothing().when(testInstanceFilesService).saveTestLogsInTestInstance(TOKEN_VALUE, file);
        response = testInstanceFilesWs.uploadTestLogs(authorizationHeader, file);
        Assert.assertEquals("The status shall be 201 as success upload", 201, response.getStatus());
    }

    /**
     * Test to upload logs with empty or null file
     */
    @Test
    public void uploadLogsEmptyFileOrEmpty() throws InvalidTokenException, EmptyFileException, TestInstanceClosedException,
            TestInstanceNotFoundException, IOException {
        file = new byte[0];
        Mockito.doThrow(new EmptyFileException("Invalid File (Null or empty)")).when(testInstanceFilesService).saveTestLogsInTestInstance(TOKEN_VALUE, file);
        response = testInstanceFilesWs.uploadTestLogs(authorizationHeader, file);
        Assert.assertEquals("The status shall be 400 as Bad Request ", 400, response.getStatus());
        Assert.assertEquals("Response shall contain the expected cause : Invalid File (Null or empty)", "Invalid File (Null or empty)",
                response.getEntity().toString());
    }


    /**
     * Test to upload logs with expired token
     */
    @Test
    public void uploadLogsFileWithExpiredToken() throws InvalidTokenException, EmptyFileException, TestInstanceClosedException,
            TestInstanceNotFoundException, IOException {
        Mockito.doThrow(new InvalidTokenException("Invalid expired token")).when(testInstanceFilesService).saveTestLogsInTestInstance(TOKEN_VALUE,
                file);
        response = testInstanceFilesWs.uploadTestLogs(authorizationHeader, file);
        Assert.assertEquals("The status shall be 401 as Unauthorized", 401, response.getStatus());
        Assert.assertEquals("Response shall contain the expected cause : Invalid expired token", "Invalid expired token",
                response.getEntity().toString());
    }

    /**
     * Test to upload logs on a Finished testInstance
     */
    @Test
    public void uploadLogsFileInTestInstanceFinished() throws InvalidTokenException, EmptyFileException, TestInstanceClosedException,
            TestInstanceNotFoundException, IOException {
        Mockito.doThrow(new TestInstanceClosedException("Test Instance Closed")).when(testInstanceFilesService).saveTestLogsInTestInstance(TOKEN_VALUE, file);
        response = testInstanceFilesWs.uploadTestLogs(authorizationHeader, file);
        Assert.assertEquals("The status shall be 410 as Gone", 410, response.getStatus());
        Assert.assertEquals("Response shall contain the expected cause : Test Instance Closed", "Test Instance Closed",
                response.getEntity().toString());
    }


    /**
     * Test to upload logs with empty token value
     */
    @Test
    public void uploadLogsFileEmptyOrNullToken() throws InvalidTokenException, EmptyFileException, TestInstanceClosedException,
            TestInstanceNotFoundException, IOException {
        Mockito.doThrow(new InvalidTokenException("Invalid token either empty or null")).when(testInstanceFilesService).saveTestLogsInTestInstance(TOKEN_VALUE, file);
        response = testInstanceFilesWs.uploadTestLogs("", file);
        Assert.assertEquals("The status shall be 401 as Unauthorized", 401, response.getStatus());
        Assert.assertEquals("Response shall contain the expected cause : Invalid token either empty or null ", "Invalid token either empty or null"
                , response.getEntity().toString());
    }

    /**
     * Test to upload logs with security Exception
     */
    @Test
    public void uploadLogsSecurityException() throws InvalidTokenException, EmptyFileException, TestInstanceClosedException,
            TestInstanceNotFoundException, IOException {
        Mockito.doThrow(new SecurityException("securityException")).when(testInstanceFilesService).saveTestLogsInTestInstance(TOKEN_VALUE, file);
        response = testInstanceFilesWs.uploadTestLogs(authorizationHeader, file);
        Assert.assertEquals("The status shall be 500 as Internal_Server_error", 500, response.getStatus());
        Assert.assertEquals("Response shall contain the expected cause :\"Unable to save the uploaded file : \"securityException\"\"", "Unable to " +
                        "save the uploaded file : securityException"
                , response.getEntity().toString());
    }


    /**
     * Test to upload Report completed params
     */
    @Test
    public void uploadReportFile() {
        response = testInstanceFilesWs.uploadTestReport(authorizationHeader, file);
        Assert.assertEquals("The status shall be 201 as success upload", 201, response.getStatus());
    }

    /**
     * Test to upload Report with empty or null file
     */
    @Test
    public void uploadReportEmptyFile() throws InvalidTokenException, EmptyFileException, TestInstanceClosedException,
            TestInstanceNotFoundException, IOException, InvalidTestReportException {
        file = new byte[0];
        Mockito.doThrow(new EmptyFileException("Invalid File (Null or empty)")).when(testInstanceFilesService).saveTestReportInTestInstance(TOKEN_VALUE, file);
        response = testInstanceFilesWs.uploadTestReport(authorizationHeader, file);
        Assert.assertEquals("The status shall be 400 as Bad Request ", 400, response.getStatus());
        Assert.assertEquals("Response shall contain the expected cause : Invalid File (Null or empty)", "Invalid File (Null or empty)",
                response.getEntity().toString());
    }

    /**
     * Test to upload Report with empty or null file
     */
    @Test
    public void uploadReportInvalidReport() throws InvalidTokenException, EmptyFileException, TestInstanceClosedException,
            TestInstanceNotFoundException, IOException, InvalidTestReportException {
        file = new byte[0];
        Mockito.doThrow(new InvalidTestReportException("Invalid Report")).when(testInstanceFilesService).saveTestReportInTestInstance(TOKEN_VALUE, file);
        response = testInstanceFilesWs.uploadTestReport(authorizationHeader, file);
        Assert.assertEquals("The status shall be 400 as Bad Request ", 400, response.getStatus());
        Assert.assertEquals("Response shall contain the expected cause :\"Invalid Report\"", "Invalid Report : Invalid Report",
                response.getEntity().toString());
    }


    /**
     * Test to upload Report with expired token
     */
    @Test
    public void uploadReportFileWithExpiredToken() throws TokenNotFoundException, InvalidTokenException, EmptyFileException,
            TestInstanceClosedException, TestInstanceNotFoundException, IOException, InvalidTestReportException {
        Mockito.doThrow(new InvalidTokenException("Invalid expired token")).when(testInstanceFilesService).saveTestReportInTestInstance(TOKEN_VALUE
                , file);
        response = testInstanceFilesWs.uploadTestReport(authorizationHeader, file);
        Assert.assertEquals("The status shall be 401 as Unauthorized", 401, response.getStatus());
        Assert.assertEquals("Response shall contain the expected cause : Invalid expired token", "Invalid expired token",
                response.getEntity().toString());
    }

    /**
     * Test to upload Report on a Finished testInstance
     *
     * @throws TestInstanceNotFoundException
     */
    @Test
    public void uploadReportFileInTestInstanceFinished() throws InvalidTokenException, EmptyFileException, TestInstanceClosedException,
            TestInstanceNotFoundException, IOException, InvalidTestReportException {
        Mockito.doThrow(new TestInstanceClosedException("Test Instance Closed")).when(testInstanceFilesService).saveTestReportInTestInstance(TOKEN_VALUE, file);
        response = testInstanceFilesWs.uploadTestReport(authorizationHeader, file);
        Assert.assertEquals("The status shall be 410 as Gone", 410, response.getStatus());
        Assert.assertEquals("Response shall contain the expected cause : Test Instance Closed", "Test Instance Closed",
                response.getEntity().toString());
    }

    /**
     * Test to upload Report with empty token value
     */
    @Test
    public void uploadReportFileEmptyToken() {
        response = testInstanceFilesWs.uploadTestReport("", file);
        Assert.assertEquals("The status shall be 401 as Unauthorized", 401, response.getStatus());
        Assert.assertEquals("Response shall contain the expected cause : Invalid token either empty or null ", "Invalid token either empty or null"
                , response.getEntity().toString());
    }


    /**
     * Test to upload logs with security Exception
     */
    @Test
    public void uploadReportSecurityException() throws InvalidTokenException, EmptyFileException, TestInstanceClosedException,
            TestInstanceNotFoundException, IOException, InvalidTestReportException {
        Mockito.doThrow(new SecurityException("securityException")).when(testInstanceFilesService).saveTestReportInTestInstance(TOKEN_VALUE, file);
        response = testInstanceFilesWs.uploadTestReport(authorizationHeader, file);
        Assert.assertEquals("The status shall be 500 as Internal_Server_error", 500, response.getStatus());
        Assert.assertEquals("Response shall contain the expected cause :\"Unable to save the uploaded file : \"securityException\"\"", "Unable to " +
                "save the uploaded file : securityException", response.getEntity().toString());
    }

    /**
     * Parser header test valid value
     */
    @Test
    public void parseValidTokenInHeaders() throws InvalidTokenException {
        String token = testInstanceFilesWs.parseTokenValueFromAuthorization(authorizationHeader);
        Assert.assertEquals("Parsed Token shall be : " + TOKEN_VALUE, TOKEN_VALUE, token);

    }

    /**
     * Parser header test invalid value
     */
    @Test(expected = InvalidTokenException.class)
    public void parseInValidTokenInHeaders() throws InvalidTokenException {
        testInstanceFilesWs.parseTokenValueFromAuthorization("");
    }
}