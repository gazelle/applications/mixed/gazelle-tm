package net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.stub;

import net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.execution.dao.AutomatedTestStepExecutionDAO;
import net.ihe.gazelle.tm.gazelletest.domain.AutomatedTestStepExecution;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestStepsInstance;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestStepsInstanceStatus;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class AutomatedTestStepExecutionDAOStub implements AutomatedTestStepExecutionDAO {

    private final List<AutomatedTestStepExecution> automatedTestStepExecutions;

    public AutomatedTestStepExecutionDAOStub() {
        automatedTestStepExecutions = new ArrayList<>();
    }

    @Override
    public void createExecution(AutomatedTestStepExecution execution) {
        automatedTestStepExecutions.add(execution);
    }

    @Override
    public void updateExecution(AutomatedTestStepExecution executionToUpdate) {
        for (Iterator<AutomatedTestStepExecution> it = automatedTestStepExecutions.iterator(); it.hasNext();) {
            AutomatedTestStepExecution execution = it.next();
            if (execution.getTestStepsInstanceId().equals(executionToUpdate.getTestStepsInstanceId())) {
                it.remove();
            }
        }
        automatedTestStepExecutions.add(executionToUpdate);
    }

    @Override
    public TestStepsInstance updateTestStepInstanceStatus(Integer testStepInstanceId, TestStepsInstanceStatus status) {
        TestStepsInstance testStepsInstance = new TestStepsInstance();
        testStepsInstance.setId(testStepInstanceId);
        testStepsInstance.setTestStepsInstanceStatus(status);
        return testStepsInstance;
    }

    @Override
    public void sendNotification(String userId, AutomatedTestStepExecution execution, TestStepsInstance testStepsInstance) {
        // do nothing for test
    }

    @Override
    public AutomatedTestStepExecution getExecution(Integer stepInstanceId) {
        for (AutomatedTestStepExecution automatedTestStepExecution : automatedTestStepExecutions) {
            if (automatedTestStepExecution.getTestStepsInstanceId().equals(stepInstanceId)) {
                return automatedTestStepExecution;
            }
        }
        return null;
    }

    public List<AutomatedTestStepExecution> getAutomatedTestStepExecutions() {
        return automatedTestStepExecutions;
    }
}
