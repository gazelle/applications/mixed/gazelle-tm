package net.ihe.gazelle.tm.gazelletest.automatedstep.domain.script;

import org.junit.Test;

import java.util.List;

import static org.testng.AssertJUnit.assertEquals;

public class ScriptTest {

    @Test
    public void testProperty() {
        Property property1 = new Property();
        property1.setKey("key");
        property1.setValue("value");
        Property property2 = ScriptFactory.createProperty("key", "value");
        assertEquals(property1.getKey(), property2.getKey());
        assertEquals(property1.getValue(), property2.getValue());
    }

    @Test
    public void testInputDefinition() {
        InputDefinition input1 = new InputDefinition();
        input1.setKey("key");
        input1.setLabel("label");
        input1.setFormat("description");
        input1.setType("type");
        input1.setRequired(true);
        InputDefinition input2 = ScriptFactory.createInputDefinition("key", "label", "description", "type", true);
        assertEquals(input1.getKey(), input2.getKey());
        assertEquals(input1.getLabel(), input2.getLabel());
        assertEquals(input1.getFormat(), input2.getFormat());
        assertEquals(input1.getType(), input2.getType());
        assertEquals(input1.getRequired(), input2.getRequired());
    }

    @Test
    public void testStep() {
        List<Property> properties = ScriptFactory.createPropertyAsList("key", "value");
        Step step1 = new Step();
        step1.setId("id");
        step1.setType("type");
        step1.setProperties(properties);
        Step step2 = ScriptFactory.createStep("id", "type", properties);
        assertEquals(step1.getId(), step2.getId());
        assertEquals(step1.getType(), step2.getType());
        assertEquals(step1.getProperties(), step2.getProperties());
    }

    @Test
    public void testScript() {
        Script script1 = new Script();
        script1.setName("name");
        List<InputDefinition> inputDefinitions = ScriptFactory.createInputAsList("key", "label", "description", "type", true);
        script1.setInputs(inputDefinitions);
        List<Property> properties = ScriptFactory.createPropertyAsList("key", "value");
        List<Step> steps = ScriptFactory.createStepAsList("id", "type", properties);
        script1.setSteps(steps);
        Script script2 = ScriptFactory.createScript("name", inputDefinitions, steps);
        assertEquals(script1.getName(), script2.getName());
        assertEquals(script1.getInputs(), script2.getInputs());
        assertEquals(script1.getSteps(), script2.getSteps());
    }

    @Test
    public void testInputContent() {
        InputContent input1 = new InputContent();
        input1.setKey("key");
        input1.setValue("value");
        InputContent input2 = ScriptFactory.createInputContent("key", "value");
        assertEquals(input1.getKey(), input2.getKey());
        assertEquals(input1.getValue(), input2.getValue());
    }

    @Test
    public void testMaestroScript() {
        Script script = new Script();
        List<InputContent> inputContentList = ScriptFactory.createInputContentList("key", "value");
        MaestroRequest maestroRequest1 = new MaestroRequest();
        maestroRequest1.setScript(script);
        maestroRequest1.setInputs(inputContentList);
        MaestroRequest maestroRequest2 = ScriptFactory.createMaestroScript("callback", "token", script, inputContentList);
        assertEquals(maestroRequest1.getScript(), maestroRequest2.getScript());
        assertEquals(maestroRequest1.getInputs(), maestroRequest2.getInputs());
    }
}
