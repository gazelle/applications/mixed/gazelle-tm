package net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.stub;

import net.ihe.gazelle.tm.gazelletest.action.TokenNotFoundException;
import net.ihe.gazelle.tm.gazelletest.dao.TestInstanceTokenDAO;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstanceToken;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class TestInstanceTokenDAOStub implements TestInstanceTokenDAO {

    private final List<TestInstanceToken> tokens;

    public TestInstanceTokenDAOStub() {
        this.tokens = new ArrayList<>();
    }

    @Override
    public void createToken(String tokenValue, String username, Integer stepInstanceId, Timestamp timestamp) {
        TestInstanceToken testInstanceToken = new TestInstanceToken(tokenValue, username, stepInstanceId, timestamp);
        tokens.add(testInstanceToken);
    }

    @Override
    public List<TestInstanceToken> getAllTokens() {
        return tokens;
    }

    @Override
    public TestInstanceToken getTestInstanceToken(String tokenValue) {
        for (TestInstanceToken token : tokens) {
            if (token.getValue().equals(tokenValue)) {
                return token;
            }
        }
        throw new TokenNotFoundException("No token found using the token value provided");
    }

    public TestInstanceToken getTestInstanceToken(String username, Integer testStepInstanceId) throws TokenNotFoundException {
        for (TestInstanceToken token : tokens) {
            if (token.getUserId().equals(username) && token.getTestStepsInstanceId().equals(testStepInstanceId)) {
                return token;
            }
        }
        throw new TokenNotFoundException("No token found using the username and the test instance id provided");
    }

    @Override
    public void deleteToken(String tokenValue) throws TokenNotFoundException {
        for (Iterator<TestInstanceToken> it = tokens.iterator(); it.hasNext();) {
            TestInstanceToken token = it.next();
            if (tokenValue.equals(token.getValue())) {
                it.remove();
            }
        }
    }

    @Override
    public void deleteTokenByTestStepInstance(Integer testStepInstanceId) {
        for (Iterator<TestInstanceToken> it = tokens.iterator(); it.hasNext();) {
            TestInstanceToken token = it.next();
            if (testStepInstanceId.equals(token.getTestStepsInstanceId())) {
                it.remove();
            }
        }
    }
}
