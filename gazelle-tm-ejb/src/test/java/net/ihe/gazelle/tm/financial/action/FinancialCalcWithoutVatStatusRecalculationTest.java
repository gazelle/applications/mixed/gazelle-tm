package net.ihe.gazelle.tm.financial.action;

import net.ihe.gazelle.tm.financial.model.Invoice;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import net.ihe.gazelle.users.model.Institution;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class FinancialCalcWithoutVatStatusRecalculationTest {
    FinancialCalcWithoutVatStatusRecalculation financialCalc;

    @Before
    public void before() {
        financialCalc = new FinancialCalcWithoutVatStatusRecalculation();
    }

    @Test
    public void calculateVatAmountWithoutChangingVatIsDueWhenVatDueIsNullTest() {
        assertThat(financialCalc.calculateVatAmount(null, BigDecimal.ONE, BigDecimal.TEN, null), is(BigDecimal.ZERO));
    }

    @Test
    public void calculateVatAmountWithoutChangingVatIsDueWhenVatIsNotDueTest() {
        assertThat(financialCalc.calculateVatAmount(null, BigDecimal.ONE, BigDecimal.TEN, false), is(BigDecimal.ZERO));
    }

    @Test
    public void calculateVatAmountWithoutChangingVatIsDueTest() {
        TestingSession ts = new TestingSession();
        ts.setVatPercent(BigDecimal.valueOf(0.20));

        assertThat(financialCalc.calculateVatAmount(ts, BigDecimal.valueOf(1000.00), BigDecimal.ZERO, true),
                is(BigDecimal.valueOf(200).setScale(2)));
    }

    @Test
    public void calculateVatAmountWithoutChangingVatIsDueWithDiscountTest() {
        TestingSession ts = new TestingSession();
        ts.setVatPercent(BigDecimal.valueOf(0.20));

        assertThat(financialCalc.calculateVatAmount(ts, BigDecimal.valueOf(1000.00), BigDecimal.valueOf(500.00), true),
                is(BigDecimal.valueOf(100).setScale(2)));
    }

    @Test
    public void calculateVatAmountWithoutChangingVatIsDueWithAdditionalFeesTest() {
        TestingSession ts = new TestingSession();
        ts.setVatPercent(BigDecimal.valueOf(0.20));

        assertThat(financialCalc.calculateVatAmount(ts, BigDecimal.valueOf(1000.00), BigDecimal.valueOf(-500.00), true),
                is(BigDecimal.valueOf(300).setScale(2)));
    }

    @Test
    public void calculateVatAmountForCompanyWithoutChangingVatIsDueInvoiceNullTest() {
        assertNull(financialCalc.calculateVatAmountForCompany(null));
    }

    @Test
    public void calculateVatAmountForCompanyWithoutChangingVatIsDueInstitutionNullTest() {
        Invoice invoice = new Invoice();
        invoice.setInstitution(null);

        Invoice resInvoice = financialCalc.calculateVatAmountForCompany(invoice);

        assertEquals(invoice, resInvoice);
        assertNull(resInvoice.getInstitution());
    }

    @Test
    public void calculateVatAmountForCompanyWithoutChangingVatIsDueInstitutionIdNullTest() {
        Invoice invoice = new Invoice();
        Institution institution = new Institution();
        institution.setId(null);
        invoice.setInstitution(institution);

        Invoice resInvoice = financialCalc.calculateVatAmountForCompany(invoice);

        assertEquals(invoice, resInvoice);
        assertEquals(invoice.getInstitution(), resInvoice.getInstitution());
        assertNull(resInvoice.getInstitution().getId());
    }

    @Test
    public void calculateVatAmountForCompanyWithoutChangingVatIsDueVatAmountZeroTest() {
        Invoice invoice = new Invoice();
        invoice.setVatAmount(null);
        invoice.setFeesAmount(BigDecimal.valueOf(5000.00));
        invoice.setFeesDiscount(BigDecimal.valueOf(100));
        invoice.setVatDue(false);
        Institution institution = new Institution();
        institution.setId(1);
        invoice.setInstitution(institution);
        TestingSession ts = new TestingSession();
        ts.setVatPercent(BigDecimal.valueOf(0.10));
        invoice.setTestingSession(ts);

        Invoice resInvoice = financialCalc.calculateVatAmountForCompany(invoice);

        assertNotNull(resInvoice.getVatAmount());
        assertThat(resInvoice.getVatAmount(), is(BigDecimal.ZERO));
    }

    @Test
    public void calculateVatAmountForCompanyWithoutChangingVatIsDueVatTest() {
        Invoice invoice = new Invoice();
        invoice.setVatAmount(null);
        invoice.setFeesAmount(BigDecimal.valueOf(5000.00));
        invoice.setFeesDiscount(BigDecimal.valueOf(100));
        invoice.setVatDue(true);
        Institution institution = new Institution();
        institution.setId(1);
        invoice.setInstitution(institution);
        TestingSession ts = new TestingSession();
        ts.setVatPercent(BigDecimal.valueOf(0.10));
        invoice.setTestingSession(ts);

        Invoice resInvoice = financialCalc.calculateVatAmountForCompany(invoice);

        assertEquals(invoice, resInvoice);
        assertEquals(invoice.getInstitution(), resInvoice.getInstitution());
        assertEquals(invoice.getInstitution().getId(), resInvoice.getInstitution().getId());
        assertNotNull(resInvoice.getVatAmount());
        assertThat(resInvoice.getVatAmount(), is(BigDecimal.valueOf(490.00).setScale(2)));
    }
}