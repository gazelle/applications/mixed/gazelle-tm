package net.ihe.gazelle.tm.gazelletest.dao;


import net.ihe.gazelle.tm.gazelletest.action.TestInstanceTokenCreationException;
import net.ihe.gazelle.tm.gazelletest.action.TestInstanceTokenSavingException;
import net.ihe.gazelle.tm.gazelletest.action.TokenNotFoundException;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstanceToken;
import org.junit.*;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.sql.Timestamp;
import java.util.concurrent.TimeUnit;


public class TestInstanceTokenDAOTest {


    private static final String TOKEN_VALUE = "fb6a3a0c-7390-41c8-b11a-1ba796097d77";
    private static final String TOKEN_VALUE1 = "gb6a3a0c-7390-41c8-b11a-1ba796097d77";
    private static final String TOKEN_VALUE2 = "hb6a3a0c-7390-41c8-b11a-1ba796097d77";

    private static final String USER_NAME = "toto";
    private static final Integer TEST_INSTANCE_ID = 12;
    private static final Timestamp EXPIRATION_TIMESTAMP = new Timestamp(System.currentTimeMillis() + TimeUnit.HOURS.toMillis(1));

    private static final String PERSISTENCE_UNIT_NAME_CONST = "PersistenceUnitTest";

    private static EntityManager entityManager;

    private TestInstanceTokenDAO testInstanceTokenDAO;

    @BeforeClass
    public static void initializeDataBase() {
        entityManager = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME_CONST).createEntityManager();
        entityManager.getTransaction().begin();
        addTestInstanceTokens(entityManager);
        entityManager.getTransaction().commit();
    }

    private static void addTestInstanceTokens(EntityManager entityManagerMock) {
        TestInstanceToken testInstanceToken = new TestInstanceToken(TOKEN_VALUE, USER_NAME, 1, EXPIRATION_TIMESTAMP);
        entityManagerMock.merge(testInstanceToken);
        TestInstanceToken testInstanceToken1 = new TestInstanceToken(TOKEN_VALUE1, USER_NAME, 2, EXPIRATION_TIMESTAMP);
        entityManagerMock.merge(testInstanceToken1);
    }

    /**
     * Close the connection to the database after all tests are executed.
     */
    @AfterClass
    public static void closeDatabase() {
        entityManager.close();
    }

    /**
     * Initialization method to create EntityManager and JPADAO test implementation.
     */
    @Before
    public void initializeEntityManager() {
        entityManager.getTransaction().begin();
        testInstanceTokenDAO = new TestInstanceTokenDAOImpl(entityManager);

    }


    /**
     * Close the transaction corresponding to the test case when it is over.
     */
    @After
    public void commitTransaction() {
        if (entityManager.getTransaction().isActive()) {
            entityManager.getTransaction().commit();
        }
    }


    /**
     * Test to Save a good test Instance
     */
    @Test
    @Ignore
    public void saveAndRetrieveToken() throws Throwable {
        testInstanceTokenDAO.createToken(TOKEN_VALUE2, USER_NAME, TEST_INSTANCE_ID, EXPIRATION_TIMESTAMP);
        TestInstanceToken testInstanceToken = testInstanceTokenDAO.getTestInstanceToken(TOKEN_VALUE2);
        Assert.assertEquals("TokenValue shall be : " + TOKEN_VALUE2, TOKEN_VALUE2, testInstanceToken.getValue());
        Assert.assertEquals("User shall be : " + USER_NAME, USER_NAME, testInstanceToken.getUserId());
        Assert.assertEquals("TestInstanceId shall be : " + TEST_INSTANCE_ID, TEST_INSTANCE_ID, testInstanceToken.getTestStepsInstanceId());
    }


    /**
     * Test to Save a good test Instance
     */
    @Test
    public void deleteToken() {
        try {
            testInstanceTokenDAO.deleteToken(TOKEN_VALUE);
            entityManager.getTransaction().commit();
            testInstanceTokenDAO.getTestInstanceToken(TOKEN_VALUE);
            Assert.fail("An TokenNotFoundException must be throw");
        } catch (TokenNotFoundException testInstanceTokenNotFound) {
            Assert.assertEquals("Message shall be : \"No token found using the token value provided\"", "No token found using the token value " +
                    "provided", testInstanceTokenNotFound.getMessage());
        }
    }

    /**
     * Test To generate Exception Duplicated Key (userName and TestInstance Identifier)
     */
    @Test
    public void catchExceptionOnSavingDuplicatedTestInstanceToken() {
        try {
            testInstanceTokenDAO.createToken(TOKEN_VALUE2 , "frde", TEST_INSTANCE_ID, EXPIRATION_TIMESTAMP);
        } catch (TestInstanceTokenCreationException e) {
            Assert.fail();
        } catch (TestInstanceTokenSavingException e) {
            Assert.assertEquals("Message shall be \"Cannot To Save TestInstanceToken check log or cause\"", "Cannot To Save TestInstanceToken check" +
                    " log or cause", e.getMessage());
        }
    }

    /**
     * Test To generate Exception Duplicated Key (userName and TestInstance Identifier)
     */
    @Test
    @Ignore
    public void catchExceptionOnSavingDuplicatedKeyTestInstanceToken() {
        try {
            testInstanceTokenDAO.createToken(TOKEN_VALUE2 +"a", USER_NAME, TEST_INSTANCE_ID, EXPIRATION_TIMESTAMP);
        } catch (TestInstanceTokenCreationException e) {
            Assert.fail();
        } catch (TestInstanceTokenSavingException e) {
            Assert.assertEquals("Message shall be \"Cannot To Save TestInstanceToken check log or cause\"", "Cannot To Save TestInstanceToken check" +
                    " log or cause", e.getMessage());
        }
    }

    @Test(expected = TestInstanceTokenCreationException.class)
    @Ignore
    public void ExceptionCreateTestInstanceTokenMissingMandatoryParam() throws TestInstanceTokenCreationException, TestInstanceTokenSavingException {
        testInstanceTokenDAO.createToken(null, USER_NAME, TEST_INSTANCE_ID, EXPIRATION_TIMESTAMP);
    }

    /**
     * Test to Save a good test Instance
     */
    @Test
    @Ignore
    public void saveAndRetrieveTokenbis() throws TestInstanceTokenCreationException, TestInstanceTokenSavingException, TokenNotFoundException {
        testInstanceTokenDAO.createToken(TOKEN_VALUE2+"a", "frde", 5, EXPIRATION_TIMESTAMP);
        TestInstanceToken testInstanceToken = testInstanceTokenDAO.getTestInstanceToken(TOKEN_VALUE2+"a");
        Assert.assertEquals("TokenValue shall be : " + TOKEN_VALUE2 +"a", TOKEN_VALUE2 +"a", testInstanceToken.getValue());
        Assert.assertEquals("User shall be : frde", "frde", testInstanceToken.getUserId());
        Assert.assertEquals("TestInstanceId shall be : 5", Integer.valueOf(5), testInstanceToken.getTestStepsInstanceId());
    }
}












