package net.ihe.gazelle.tm.systems.action;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.mock;

@RunWith(MockitoJUnitRunner.class)
public class TestingSessionManagerTest {


    private TestingSessionManager testingSessionManager;

    @Before
    public void setUp() {
        testingSessionManager = mock(TestingSessionManager.class);
    }

    @Test
    public void testGetSamplesPreferenceWithRestrictRoleInvalid() {
        Assert.assertNotNull(testingSessionManager.isAllowedToAccessToAllSamples());
        Assert.assertEquals(false, testingSessionManager.isAllowedToAccessToAllSamples());
    }


}