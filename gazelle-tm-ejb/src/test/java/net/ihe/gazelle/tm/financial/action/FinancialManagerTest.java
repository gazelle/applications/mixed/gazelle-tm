package net.ihe.gazelle.tm.financial.action;

import net.ihe.gazelle.tm.financial.FinancialSummary;
import net.ihe.gazelle.tm.financial.FinancialSummaryOneSystem;
import net.ihe.gazelle.tm.financial.model.Invoice;
import net.ihe.gazelle.tm.session.TestingSessionService;
import net.ihe.gazelle.tm.systems.model.SystemInSession;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import net.ihe.gazelle.users.model.Institution;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Invoice.class, TestingSession.class, FinancialManager.class, SystemInSession.class, TestingSessionService.class})
@PowerMockIgnore("javax.management.*")
public class FinancialManagerTest {

    FinancialManager fm;
    FinancialSummary fs;
    TestingSession ts;
    Institution ins;

    @Mock
    private EntityManager em;
    @Mock
    private TestingSessionService testingSessionService;

    @Before
    public void setUp() {
        em = Mockito.mock(EntityManager.class);
    }

    @Test
    public void computeVatDueStateChangeVatIsNotDueTest() {
        fm = new FinancialManager();
        Invoice invoiceSelected = new Invoice();
        invoiceSelected.setVatDue(false);
        invoiceSelected.setVatAmount(BigDecimal.ONE);
        fm.setInvoiceSelected(invoiceSelected);

        fm.computeVatDueStateChange();
        testingSessionService = Mockito.mock(TestingSessionService.class);

        assertThat(fm.getInvoiceSelected().getVatAmount(), is(BigDecimal.ZERO));
    }

    @Test
    public void computeVatDueStateChangeVatIsDueTest() {
        initFinancialManagerWithAmounts();
        fm.getInvoiceSelected().setVatAmount(BigDecimal.ZERO);

        fm.computeVatDueStateChange();

        assertThat(fm.getInvoiceSelected().getVatAmount(), is(not(BigDecimal.ZERO)));
    }

    @Test
    public void updateFeesDueWithoutChangingVatIsDueNullValuesTest() {
        initFinancialManagerWithAmounts();
        fm.getInvoiceSelected().setFeesDiscount(null);
        fm.getInvoiceSelected().setFeesPaid(null);

        fm.updateFeesDueWithoutChangingVatIsDue();

        assertNotNull(fm.getInvoiceSelected().getFeesDiscount());
        assertNotNull(fm.getInvoiceSelected().getFeesPaid());
        assertNotNull(fm.getInvoiceSelected().getFeesDue());
    }

    @Test
    public void updateFeesDueWithoutChangingVatIsDueTest() {
        initFinancialManagerWithAmounts();
        fm.getInvoiceSelected().setVatAmount(BigDecimal.ZERO);
        fm.getInvoiceSelected().setFeesDue(BigDecimal.ZERO);

        fm.updateFeesDueWithoutChangingVatIsDue();

        assertThat(fm.getInvoiceSelected().getVatAmount(), is(not(BigDecimal.ZERO)));
        assertThat(fm.getInvoiceSelected().getFeesDue(), is(not(BigDecimal.ZERO)));
    }

    @Test
    public void getFinancialSummaryWithCalculatedAmountsWithoutChangingVatIsDueStaticSessionIsNullTest() {
        PowerMockito.mockStatic(FinancialManager.class);
        PowerMockito.when(FinancialManager.getTestingSessionService())
                .thenReturn(testingSessionService);
        initTestingSessionWithAmounts();
        mockGetSelectedTestingSessionStatic(null);

        FinancialSummary resultFinancialSummary = FinancialManager.getFinancialSummaryWithCalculatedAmountsStatic(
                null,
                ins,
                em,
                false);

        assertNull(resultFinancialSummary);
    }

    @Test
    public void getFinancialSummaryWithCalculatedAmountsWithoutChangingVatIsDueStaticNoContractRequiredTest() {
        initTestingSessionWithAmounts();
        mockGetSelectedTestingSessionStatic(ts);
        ts.setContractRequired(false);

        FinancialSummary resultFinancialSummary = FinancialManager.getFinancialSummaryWithCalculatedAmountsStatic(
                ts,
                ins,
                em,
                false);

        assertNull(resultFinancialSummary);
    }

    @Test
    public void getFinancialSummaryWithCalculatedAmountsWithoutChangingVatIsDueStaticInstitutionIsNullTest() {
        initTestingSessionWithAmounts();
        mockGetSelectedTestingSessionStatic(ts);

        FinancialSummary resultFinancialSummary = FinancialManager.getFinancialSummaryWithCalculatedAmountsStatic(
                ts,
                null,
                em,
                false);

        assertNull(resultFinancialSummary);
    }

    @Test
    public void getFinancialSummaryWithCalculatedAmountsWithoutChangingVatIsDueStaticListSystemsInSessionIsNullTest() {
        initTestingSessionWithAmounts();
        mockGetSelectedTestingSessionStatic(ts);
        initInstitution();
        initFinancialManagerWithAmounts();
        mockGetSystemsInSessionForCompanyForSession(null);

        FinancialSummary resultFinancialSummary = FinancialManager.getFinancialSummaryWithCalculatedAmountsStatic(
                ts,
                ins,
                em,
                false);

        assertNull(resultFinancialSummary);
    }

    @Test
    public void calculateFinancialSummaryDTOWithoutChangingVatIsDueFinancialSummaryIsNullTest() {
        initTestingSessionWithAmounts();
        mockGetSelectedTestingSessionStatic(ts);
        mockFinancialManagerStatic(null);
        PowerMockito.when(FinancialManager.getTestingSessionService())
                .thenReturn(testingSessionService);
        initFinancialManagerWithAmounts();

        fm.calculateFinancialSummaryDTO(false);

        assertNull(fm.getFinancialSummaryForOneSystem());
    }

    @Test
    public void calculateFinancialSummaryDTOWithoutChangingVatIsDueTest() {
        initTestingSessionWithAmounts();
        mockGetSelectedTestingSessionStatic(ts);
        initFinancialSummary();
        mockFinancialManagerStatic(fs);
        PowerMockito.when(FinancialManager.getTestingSessionService())
                .thenReturn(testingSessionService);
        initFinancialManagerWithAmounts();

        fm.calculateFinancialSummaryDTO(false);

        assertNotNull(fm.getFinancialSummaryForOneSystem());
        assertThat(fm.getFinancialSummaryForOneSystem().size(), is(1));
        assertThat(fm.getFinancialSummaryForOneSystem().get(0).getId(), is(7));
    }

    /**
     * Private init and mock methods
     **/

    private void mockInvoiceStatic(Invoice forgedInvoiceToReturn) {
        PowerMockito.mockStatic(Invoice.class);
        PowerMockito.when(Invoice.mergeInvoiceIfNotSent(Mockito.<Invoice>any(), Mockito.<EntityManager>any()))
                .thenReturn(forgedInvoiceToReturn);
        PowerMockito.when(Invoice.getInvoiceForAnInstitutionAndATestingSession(Mockito.<EntityManager>any(), Mockito.<Institution>any(), Mockito.<TestingSession>any()))
                .thenReturn(forgedInvoiceToReturn);
    }

    private void initFinancialManagerWithAmounts() {
        TestingSession ts = new TestingSession();
        ts.setVatPercent(BigDecimal.valueOf(0.20));
        Institution inst = new Institution();
        inst.setId(1);
        Invoice invoice = new Invoice();
        invoice.setInstitution(inst);
        invoice.setTestingSession(ts);
        invoice.setFeesAmount(BigDecimal.valueOf(5100.00));
        invoice.setFeesDiscount(BigDecimal.valueOf(100));
        invoice.setVatDue(true);
        fm = new FinancialManager();
        fm.setInvoiceSelected(invoice);
    }

    private void mockFinancialManagerStatic(FinancialSummary forgedFinancialSummaryToReturn) {
        PowerMockito.mockStatic(FinancialManager.class);
        PowerMockito.when(FinancialManager.getFinancialSummaryWithCalculatedAmountsStatic(
                        Mockito.<TestingSession>any(), Mockito.<Institution>any(), Mockito.<EntityManager>any(),
                        Mockito.eq(false)))
                .thenReturn(forgedFinancialSummaryToReturn);
    }

    private void initTestingSessionWithAmounts() {
        ts = new TestingSession();
        ts.setContractRequired(true);
        ts.setVatPercent(BigDecimal.valueOf(0.20));
        ts.setFeeFirstSystem(BigDecimal.valueOf(1500));
        ts.setFeeAdditionalSystem(BigDecimal.valueOf(500));
        ts.setFeeParticipant(BigDecimal.valueOf(100));
    }

    private void mockGetSelectedTestingSessionStatic(TestingSession forgedTestingSessionToReturn) {
        Mockito.when(testingSessionService.getUserTestingSession()).thenReturn(forgedTestingSessionToReturn);
    }

    private void initInstitution() {
        ins = new Institution();
        ins.setId(1);
    }

    private void initFinancialSummary() {
        FinancialSummaryOneSystem fsos = new FinancialSummaryOneSystem();
        fsos.setId(7);
        List<FinancialSummaryOneSystem> financialSummaryOneSystems = Collections.singletonList(fsos);
        fs = new FinancialSummary();
        fs.setFinancialSummaryOneSystems(financialSummaryOneSystems);
    }

    private void mockGetSystemsInSessionForCompanyForSession(List<SystemInSession> forgedSystemsInSession) {
        PowerMockito.mockStatic(SystemInSession.class);
        PowerMockito.when(SystemInSession.getSystemsInSessionForCompanyForSession(
                        Mockito.<EntityManager>any(),
                        Mockito.<Institution>any(),
                        Mockito.<TestingSession>any()))
                .thenReturn(forgedSystemsInSession);
    }
}