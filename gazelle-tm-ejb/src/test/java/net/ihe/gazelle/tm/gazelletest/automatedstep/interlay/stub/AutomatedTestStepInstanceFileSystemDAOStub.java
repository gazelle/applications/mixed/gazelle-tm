package net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.stub;

import net.ihe.gazelle.tm.gazelletest.automatedstep.domain.instance.AutomatedTestStepInstanceData;
import net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.instance.dao.AutomatedTestStepInstanceFileSystemDAO;

import java.util.HashMap;
import java.util.Map;

public class AutomatedTestStepInstanceFileSystemDAOStub implements AutomatedTestStepInstanceFileSystemDAO {

    private final Map<String, String> fileContents;

    public AutomatedTestStepInstanceFileSystemDAOStub() {
        fileContents = new HashMap<>();
    }

    @Override
    public void saveInputDataFile(Integer testStepsDataId, String fileName, String fileContent) {
        fileContents.put(fileName, fileContent);
    }

    @Override
    public String readInputDataFile(AutomatedTestStepInstanceData inputData) {
        return fileContents.get(inputData.getFileName());
    }

    @Override
    public void deleteInputDataFile(AutomatedTestStepInstanceData inputData) {
        fileContents.remove(inputData.getFileName());
    }

    public Map<String, String> getFileContents() {
        return fileContents;
    }
}
