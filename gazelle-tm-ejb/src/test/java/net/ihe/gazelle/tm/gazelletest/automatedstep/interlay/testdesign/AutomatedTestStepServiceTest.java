package net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.testdesign;

import net.ihe.gazelle.tm.gazelletest.automatedstep.domain.script.*;
import net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.script.validation.ScriptValidationServiceImpl;
import net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.stub.AutomatedTestStepDAOStub;
import net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.stub.TestStepScriptDAOStub;
import net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.testdesign.service.AutomatedTestStepService;
import net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.testdesign.service.AutomatedTestStepServiceImpl;
import net.ihe.gazelle.tm.gazelletest.domain.AutomatedTestStepInputDomain;
import net.ihe.gazelle.tm.gazelletest.domain.TestStepDomain;
import net.ihe.gazelle.tm.gazelletest.domain.TestStepType;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertTrue;

public class AutomatedTestStepServiceTest {

    private TestStepScriptDAOStub testStepScriptDAO;
    private AutomatedTestStepDAOStub automatedTestStepDAO;
    private AutomatedTestStepService service;

    @Before
    public void init() {
        testStepScriptDAO = new TestStepScriptDAOStub();
        automatedTestStepDAO = new AutomatedTestStepDAOStub();
        service = new AutomatedTestStepServiceImpl(testStepScriptDAO, automatedTestStepDAO, new ScriptValidationServiceImpl());
    }

    @Test
    public void test_create_test_step() {
        Script script = new Script();
        script.setName("name");
        List<InputDefinition> inputDefinitions = ScriptFactory.createInputAsList("key", "label", "description", "type", true);
        script.setInputs(inputDefinitions);
        List<Property> properties = ScriptFactory.createPropertyAsList("key", "value");
        List<Step> steps = ScriptFactory.createStepAsList("id", "type", properties);
        script.setSteps(steps);

        TestStepDomain testStepDomain = new TestStepDomain();
        testStepDomain.setType(TestStepType.AUTOMATED);
        testStepDomain.setTestScriptID("testScriptID");
        testStepDomain.setInputs(new ArrayList<AutomatedTestStepInputDomain>());

        TestStepDomain created = service.createTestStep(testStepDomain, script);
        assertEquals(1, created.getInputs().size());
        assertTrue(created.isAutomated());
        assertEquals(1, testStepScriptDAO.getTestDao().size());
        assertEquals(1, automatedTestStepDAO.getTestStepDomains().size());
        assertEquals(2, automatedTestStepDAO.getIdSequence().intValue());
    }

    @Test
    public void test_edit_test_step() {
        Script script = new Script();
        script.setName("name");
        List<InputDefinition> inputDefinitions = ScriptFactory.createInputAsList("key", "label", "description", "type", true);
        script.setInputs(inputDefinitions);
        List<Property> properties = ScriptFactory.createPropertyAsList("key", "value");
        List<Step> steps = ScriptFactory.createStepAsList("id", "type", properties);
        script.setSteps(steps);

        TestStepDomain testStepDomain = new TestStepDomain();
        testStepDomain.setType(TestStepType.AUTOMATED);
        testStepDomain.setTestScriptID("testScriptID");
        testStepDomain.setInputs(new ArrayList<AutomatedTestStepInputDomain>());

        String newScriptName = "newScriptName";
        service.createTestStep(testStepDomain, script);
        TestStepDomain edited = service.editTestStep(testStepDomain, newScriptName, script);
        assertEquals(1, edited.getInputs().size());
        assertEquals(1, testStepScriptDAO.getTestDao().size());
        assertEquals(1, automatedTestStepDAO.getTestStepDomains().size());
        assertEquals(2, automatedTestStepDAO.getIdSequence().intValue());
    }

    @Test
    public void test_copy_test_step() {
        Script script = new Script();
        script.setName("name");
        List<InputDefinition> inputDefinitions = ScriptFactory.createInputAsList("key", "label", "description", "type", true);
        script.setInputs(inputDefinitions);
        List<Property> properties = ScriptFactory.createPropertyAsList("key", "value");
        List<Step> steps = ScriptFactory.createStepAsList("id", "type", properties);
        script.setSteps(steps);

        TestStepDomain testStepDomain = new TestStepDomain();
        testStepDomain.setType(TestStepType.AUTOMATED);
        testStepDomain.setTestScriptID("testScriptID");
        testStepDomain.setInputs(new ArrayList<AutomatedTestStepInputDomain>());

        service.createTestStep(testStepDomain, script);
        TestStepDomain copy = service.copyTestStep(testStepDomain, testStepDomain.getId());
        assertEquals(1, copy.getInputs().size());
        assertEquals(2, copy.getId().intValue());
        assertEquals(2, testStepScriptDAO.getTestDao().size());
        assertEquals(2, automatedTestStepDAO.getTestStepDomains().size());
    }

    @Test
    public void test_delete_test_step() {
        Script script = new Script();
        script.setName("name");
        List<InputDefinition> inputDefinitions = ScriptFactory.createInputAsList("key", "label", "description", "type", true);
        script.setInputs(inputDefinitions);
        List<Property> properties = ScriptFactory.createPropertyAsList("key", "value");
        List<Step> steps = ScriptFactory.createStepAsList("id", "type", properties);
        script.setSteps(steps);

        TestStepDomain testStepDomain = new TestStepDomain();
        testStepDomain.setType(TestStepType.AUTOMATED);
        testStepDomain.setTestScriptID("testScriptID");
        testStepDomain.setInputs(new ArrayList<AutomatedTestStepInputDomain>());

        service.createTestStep(testStepDomain, script);
        service.deleteTestStep(testStepDomain);
        assertEquals(0, testStepScriptDAO.getTestDao().size());
        assertEquals(0, automatedTestStepDAO.getTestStepDomains().size());
    }
}
