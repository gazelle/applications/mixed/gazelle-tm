package net.ihe.gazelle.tm.gazelletest.action;

import org.junit.Test;

import static org.junit.Assert.*;

public class EmptyFileExceptionTest {
    /**
     * Dummy Test for coverage
     * @throws EmptyFileException testException
     */
    @Test(expected = EmptyFileException.class)
    public void generateException() throws EmptyFileException {
        throw new EmptyFileException("Test");
    }
    /**
     * Dummy Test for coverage
     * @throws EmptyFileException testException
     */
    @Test(expected = EmptyFileException.class)
    public void generateException1() throws EmptyFileException {
        throw new EmptyFileException("Test",new IllegalArgumentException("test"));
    }

    /**
     * Dummy Test for coverage
     *
     * @throws EmptyFileException testException
     */
    @Test(expected = EmptyFileException.class)
    public void generateException2() throws EmptyFileException {
        throw new EmptyFileException( new IllegalArgumentException("test"));
    }

    /**
     * Dummy Test for coverage
     *
     * @throws EmptyFileException testException
     */
    @Test(expected = EmptyFileException.class)
    public void generateException3() throws EmptyFileException {
        throw new EmptyFileException( );
    }
}