package net.ihe.gazelle.tm.gazelletest.action;

import junit.framework.TestCase;
import net.ihe.gazelle.tm.gazelletest.dao.TestInstanceTokenDAO;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstanceToken;
import org.junit.Assert;
import org.junit.Before;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.sql.Timestamp;
import java.util.concurrent.TimeUnit;

public class TestInstanceTokenServiceTest extends TestCase {

    private final static String TOKEN_VALUE = "fb6a3a0c-7390-41c8-b11a-1ba796097d77";
    @Mock
    private TestInstanceTokenDAO testInstanceTokenDAO;

    private TestInstanceTokenService testInstanceTokenService = new TestInstanceTokenService();

    private Timestamp timestampExpired = new Timestamp(System.currentTimeMillis()
            + TimeUnit.MILLISECONDS.toMillis(100));
    private Timestamp timestampValid = new Timestamp(System.currentTimeMillis()
            + TimeUnit.MINUTES.toMillis(6));

    private TestInstanceToken testInstanceToken = new TestInstanceToken(TOKEN_VALUE, "toto", 1, timestampValid);
    private TestInstanceToken testInstanceTokenExpired = new TestInstanceToken(TOKEN_VALUE, "toto", 1, timestampExpired);


    @Before
    public void setUp() throws Exception {
        testInstanceTokenDAO = Mockito.mock(TestInstanceTokenDAO.class);

        testInstanceTokenService.setTestInstanceTokenDAO(testInstanceTokenDAO);

        Mockito.doNothing().when(testInstanceTokenDAO).deleteToken(TOKEN_VALUE);
        Mockito.when(testInstanceTokenDAO.getTestInstanceToken(TOKEN_VALUE)).thenReturn(testInstanceToken);
    }

    public void testIsTokenValid() throws TokenNotFoundException {
        Assert.assertEquals("The token shall be valid, and return true ", true, testInstanceTokenService.isTokenValid(testInstanceToken));

        Assert.assertEquals("The token shall be expired, and return false", false, testInstanceTokenService.isTokenValid(testInstanceTokenExpired));
    }

    public void testGetTokenByValue() throws TokenNotFoundException {
        TestInstanceToken testInstanceToken1 = testInstanceTokenService.getTokenByValue(TOKEN_VALUE);
        Assert.assertEquals("The token value shall be \"fb6a3a0c-7390-41c8-b11a-1ba796097d77\"", TOKEN_VALUE, testInstanceToken1.getValue());
        Assert.assertEquals("The creationDate shall be " + timestampValid.toString(), timestampValid, testInstanceToken1.getExpirationDateTime());
    }
}