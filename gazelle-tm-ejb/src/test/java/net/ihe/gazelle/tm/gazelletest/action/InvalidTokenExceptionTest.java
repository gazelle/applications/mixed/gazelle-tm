package net.ihe.gazelle.tm.gazelletest.action;

import org.junit.Test;

import static org.junit.Assert.*;

public class InvalidTokenExceptionTest {
    /**
     * Dummy Test for coverage
     * @throws InvalidTokenException testException
     */
    @Test(expected = InvalidTokenException.class)
    public void generateException() throws InvalidTokenException {
        throw new InvalidTokenException("Test");
    }
    /**
     * Dummy Test for coverage
     * @throws InvalidTokenException testException
     */
    @Test(expected = InvalidTokenException.class)
    public void generateException1() throws InvalidTokenException {
        throw new InvalidTokenException("Test",new IllegalArgumentException("test"));
    }

    /**
     * Dummy Test for coverage
     *
     * @throws InvalidTokenException testException
     */
    @Test(expected = InvalidTokenException.class)
    public void generateException2() throws InvalidTokenException {
        throw new InvalidTokenException( new IllegalArgumentException("test"));
    }

    /**
     * Dummy Test for coverage
     *
     * @throws InvalidTokenException testException
     */
    @Test(expected = InvalidTokenException.class)
    public void generateException3() throws InvalidTokenException {
        throw new InvalidTokenException( );
    }

}