package net.ihe.gazelle.tm.gazelletest.action.conformityTestReport;

import net.ihe.gazelle.tm.gazelletest.action.conformitytestreport.model.*;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ConformityTestReportModelTest {


    @Test
    public void testCreateAIPO() {
        AIPOReportModel aipoReportModel = new AIPOReportModel();
        DataOfTFObject actor = new DataOfTFObject("ADT", "Admission");
        aipoReportModel.setActor(actor);
        DataOfTFObject profile = new DataOfTFObject("IHE", "IHE-europe");
        aipoReportModel.setActorIntegrationProfile(profile);
        DataOfTFObject option = new DataOfTFObject("TestOption", "option 1");
        aipoReportModel.setActorIntegrationProfileOption(option);
        List<AbstractTestReportModel> list = new ArrayList<>();
        list.add(generateTestReportItem(1));
        list.add(generateMetaTest(2));
        aipoReportModel.setTestReportList(list);
        Assert.assertEquals(2, aipoReportModel.getTestReportList().size());
    }


    /**
     * Tests On DataOfTfObject
     */

    @Test
    public void createDataOfTfObject() {
        DataOfTFObject dataOfTFObject = new DataOfTFObject();
        Assert.assertNotNull(dataOfTFObject);
        Assert.assertNull(dataOfTFObject.getKeyword());
        Assert.assertNull(dataOfTFObject.getName());
        Assert.assertNull(dataOfTFObject.getTestType());
    }

    @Test
    public void createDataOfTfObjectWithValues() {
        DataOfTFObject dataOfTFObject = new DataOfTFObject("keywordTest", "nameTest", "testtype");
        Assert.assertNotNull(dataOfTFObject);
        Assert.assertNotNull(dataOfTFObject.getKeyword());
        Assert.assertEquals("keywordTest", dataOfTFObject.getKeyword());
        Assert.assertNotNull(dataOfTFObject.getName());
        Assert.assertEquals("nameTest", dataOfTFObject.getName());
        Assert.assertNotNull(dataOfTFObject.getTestType());
        Assert.assertEquals("testtype", dataOfTFObject.getTestType());
    }

    @Test
    public void createDataOfTfObjectChangeValues() {
        DataOfTFObject dataOfTFObject = new DataOfTFObject("keywordTest", "nameTest", "testtype");
        dataOfTFObject.setName("nameTestChanged");
        Assert.assertEquals("nameTestChanged", dataOfTFObject.getName());
        dataOfTFObject.setKeyword("keywordTestChanged");
        Assert.assertEquals("keywordTestChanged", dataOfTFObject.getKeyword());
        dataOfTFObject.setTestType("connectathon");
        Assert.assertEquals("connectathon", dataOfTFObject.getTestType());
    }

    /**
     * Tests on TestInstanceReport
     */

    @Test
    public void createTestInstanceReport() {
        TestInstanceReport testInstanceReport = new TestInstanceReport();
        Assert.assertNotNull(testInstanceReport);
        testInstanceReport = new TestInstanceReport("keywordTest", "123456789", "monitorNameTest", "OK", new Date(), new Date());
        Assert.assertEquals("keywordTest", testInstanceReport.getKeyword());
        Assert.assertEquals("123456789", testInstanceReport.getTestInstanceId());
        Assert.assertEquals("monitorNameTest", testInstanceReport.getMonitorName());
        Assert.assertEquals("OK", testInstanceReport.getResult());
    }

    @Test
    public void useTestInstanceReportConstructor() {
        TestInstanceReport testInstanceReport1 = generateTestInstanceReport(1);
        TestInstanceReport testInstanceReport2 = generateTestInstanceReport(2);
        testInstanceReport2.setResult("KO");
        Assert.assertNotSame(testInstanceReport1, testInstanceReport2);
    }

    /**
     * Tests on  TestReportItem
     */

    @Test
    public void createTestReportItem() {
        TestReportItem testReportItem = new TestReportItem();
        Assert.assertNotNull(testReportItem);
        testReportItem.setKeyword("TestReportKeyword");
        testReportItem.setName("TestReportName");
        Assert.assertEquals("TestReportKeyword", testReportItem.getKeyword());
        Assert.assertEquals("TestReportName", testReportItem.getName());
        testReportItem.setTestInstanceReports(generateTestInstanceList());
        Assert.assertEquals(2, testReportItem.getTestInstanceReports().size());

    }


    /**
     * Test on MetaTest
     */
    @Test
    public void createMetaTest() {
        MetaTestReport metaTestReport = new MetaTestReport();
        Assert.assertNotNull(metaTestReport);
        metaTestReport.setName("metaTestName");
        Assert.assertEquals("metaTestName", metaTestReport.getName());
        metaTestReport.setKeyword("metaTestKeyword");
        Assert.assertEquals("metaTestKeyword", metaTestReport.getKeyword());
        metaTestReport.setTestReportItems(generateListOfTestReportItem());
        Assert.assertEquals(2, metaTestReport.getTestReportItems().size());

    }

    private static MetaTestReport generateMetaTest(Integer id) {
        MetaTestReport metaTestReport = new MetaTestReport();
        metaTestReport.setName("metaTestName" + id.toString());
        metaTestReport.setKeyword("metaTestKeyword" + id);
        metaTestReport.setTestReportItems(generateListOfTestReportItem());
        return metaTestReport;
    }

    private static List<TestReportItem> generateListOfTestReportItem() {
        List<TestReportItem> list = new ArrayList<>();
        list.add(generateTestReportItem(1));
        list.add(generateTestReportItem(2));
        return list;
    }

    private static TestReportItem generateTestReportItem(Integer id) {
        TestReportItem testReportItem = new TestReportItem();
        Assert.assertNotNull(testReportItem);
        testReportItem.setKeyword("TestReportKeyword" + id.toString());
        testReportItem.setName("TestReportName" + id);
        testReportItem.setTestInstanceReports(generateTestInstanceList());
        return testReportItem;
    }

    private static List<TestInstanceReport> generateTestInstanceList() {
        List<TestInstanceReport> list = new ArrayList<>();
        list.add(generateTestInstanceReport(1));
        list.add(generateTestInstanceReport(2));
        return list;
    }

    private static TestInstanceReport generateTestInstanceReport(Integer id) {
        TestInstanceReport testInstanceReport = new TestInstanceReport();
        testInstanceReport.setKeyword("testInstanceKeywordTest" + id.toString());
        testInstanceReport.setMonitorName("monitorNameTest" + id);
        testInstanceReport.setResult("OK or KO");
        testInstanceReport.setTestInstanceId("123456789Test");
        List<DataOfTFObject> dataOfTFObjectList = new ArrayList<>();
        dataOfTFObjectList.add(generateDataOfTFObject(1));
        dataOfTFObjectList.add(generateDataOfTFObject(2));
        return testInstanceReport;
    }

    private static DataOfTFObject generateDataOfTFObject(Integer id) {
        return new DataOfTFObject("keywordTest" + id.toString(), "nameTest" + id);
    }


    public static ConformityTestReportModel generateReport() {
        ConformityTestReportModel reportModel = new ConformityTestReportModel();
        reportModel.setSystem(2, "systemName", "systemKeyword", 4, "sessionDescription", 1);
        reportModel.setTestingSession(2, "description", new Date(), new Date(), new Date(), true, true, true);
        List<AIPOReportModel> list = new ArrayList<>();
        list.add(generateAIPO());
        list.add(generateAIPO());
        reportModel.setAipoModelList(list);
        return reportModel;
    }

    private static AIPOReportModel generateAIPO() {
        AIPOReportModel aipoReportModel = new AIPOReportModel();
        DataOfTFObject actor = new DataOfTFObject("ADT", "Admission");
        aipoReportModel.setActor(actor);
        DataOfTFObject profile = new DataOfTFObject("IHE", "IHE-europe");
        aipoReportModel.setActorIntegrationProfile(profile);
        DataOfTFObject option = new DataOfTFObject("TestOption", "option 1");
        aipoReportModel.setActorIntegrationProfileOption(option);
        List<AbstractTestReportModel> list = new ArrayList<>();
        list.add(generateTestReportItem(1));
        list.add(generateMetaTest(2));
        aipoReportModel.setTestReportList(list);

        return aipoReportModel;
    }


}
