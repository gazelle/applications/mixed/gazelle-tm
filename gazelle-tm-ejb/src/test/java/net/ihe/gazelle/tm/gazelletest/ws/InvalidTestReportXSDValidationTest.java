package net.ihe.gazelle.tm.gazelletest.ws;


import net.ihe.gazelle.tm.gazelletest.action.TestReportXSDValidator;
import net.ihe.gazelle.tm.gazelletest.action.XSDErrorHandler;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.xml.sax.SAXException;

import javax.xml.transform.stream.StreamSource;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class InvalidTestReportXSDValidationTest {

   @Parameterized.Parameters
   public static Collection<Object[]> invalidReportsAndErrors() {
      return Arrays.asList(new Object[][]{

            {"externaltool/testreportsamples/invalidWrongNamespace.xml",
                  new String[]{"Cannot find the declaration of element 'testReport'"}},

            {"externaltool/testreportsamples/invalidNoContent.xml",
                  new String[]{"One of '{\"http://testreport.gazelle.ihe.net/\":testService}' is expected."}},

            {"externaltool/testreportsamples/invalidMissingSystemUnderTest.xml",
                  new String[]{"One of '{\"http://testreport.gazelle.ihe.net/\":systemUnderTest}' is expected."}},

            {"externaltool/testreportsamples/invalidMissingAttributes.xml",
                  new String[]{"Attribute 'uuid' must appear on element 'testReport'",
                        "Attribute 'dateTime' must appear on element 'testReport'",
                        "Attribute 'version' must appear on element 'serviceIdentification'",
                        "Attribute 'total' must appear on element 'numberOfTests'"}},

            {"externaltool/testreportsamples/invalidData.xml",
                  new String[]{"is not facet-valid with respect to pattern '[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}' for type " +
                        "'uuidType'.",
                        "attribute 'uuid' on element 'testReport' is not valid with respect to its type, 'uuidType'.",
                        "is not a valid value for 'dateTime'.",
                        "attribute 'dateTime' on element 'testReport' is not valid with respect to its type, 'dateTime'.",
                        "is not facet-valid with respect to pattern '([0-9A-F]{2}[:-]){5}([0-9A-F]{2})' for type 'macAddressType'.",
                        "element 'macAddress' is not valid.",
                        "is not facet-valid with respect to pattern '(((1?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])\\.){3}(1?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])",
                        "element 'ipAddress' is not valid.",
                        "is not facet-valid with respect to enumeration '[PASSED, FAILED, UNDEFINED]'. It must be a value from the enumeration.",
                        "element 'status' is not valid.",
                        "is not facet-valid with respect to minInclusive '0' for type 'nonNegativeInteger'.",
                        "attribute 'total' on element 'numberOfTests' is not valid with respect to its type, 'nonNegativeInteger'.",
                        "is not a valid value for 'anyURI'.",
                        "element 'urlToTestSuiteResult' is not valid."}}
      });
   }

   private TestReportXSDValidator validator;
   private String testReportResourcePath;
   private String[] expectedErrors;

   public InvalidTestReportXSDValidationTest(String testReportResourcePath, String[] expectedErrors) {
      this.testReportResourcePath = testReportResourcePath;
      this.expectedErrors = expectedErrors;
   }

   @Before
   public void initializeTest() throws SAXException {
      validator = new TestReportXSDValidator(TestReportXSDValidationTest.TEST_REPORT_XSD_LOCATION);
   }

   @Test
   public void testInvalidSampleXSDValidation() throws IOException, SAXException {
      StreamSource content = new StreamSource(TestReportXSDValidationTest.class.getClassLoader().getResourceAsStream(testReportResourcePath));
      XSDErrorHandler errorHandler = validator.validateResource(content);
      Assert.assertEquals(String.format("The XSD Validation of the sample %s must return %s error(s)", testReportResourcePath, expectedErrors.length),
            expectedErrors.length, errorHandler.getErrors().size());
      for (String expectedError : expectedErrors) {
         Assert.assertTrue(String.format("The validation must return an error that contains '%s'", expectedError),
               errorHandler.containsError(expectedError));
      }
   }

}
