package net.ihe.gazelle.tm.organization.ws;

import net.ihe.gazelle.ssov7.gum.client.interlay.ws.OrganizationController;
import net.ihe.gazelle.tm.organization.OrganizationService;
import net.ihe.gazelle.users.model.Institution;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class OrganizationControllerImplTest {

    OrganizationController organizationController;
    OrganizationService organizationService;
    Institution organization;
    List<Institution> organizations;
    @Before
    public void init() {
        organizations = new ArrayList<>();
        organizationService = mock(OrganizationService.class);
        organizationController = new OrganizationControllerImpl(organizationService);
        organization = new Institution(1, "Test Institution", "TEST", "https://www.ihe.net");
        organizations.add(organization);
    }

    @Test
    public void testGetOrganizationById() {
        when(organizationService.getOrganizationById("TEST")).thenReturn(organization);
        Response response = organizationController.getOrganizationById("TEST");
        assertEquals(200, response.getStatus());

    }

    @Test
    public void testGetOrganizationByIdThrowsNoSuchElementException() throws NoSuchElementException {
        when(organizationService.getOrganizationById("TEST1")).thenThrow(new NoSuchElementException());
        Response response = organizationController.getOrganizationById("TEST1");
        assertEquals(404, response.getStatus());
    }

}