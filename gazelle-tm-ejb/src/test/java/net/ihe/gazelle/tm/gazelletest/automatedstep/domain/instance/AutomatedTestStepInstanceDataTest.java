package net.ihe.gazelle.tm.gazelletest.automatedstep.domain.instance;

import org.junit.Test;

import static org.testng.AssertJUnit.assertEquals;

public class AutomatedTestStepInstanceDataTest {

    @Test
    public void testInstanceData() {
        AutomatedTestStepInstanceData data1 = new AutomatedTestStepInstanceData();
        data1.setTestStepInstanceInputId(1);
        data1.setTestStepInstanceId(1);
        data1.setKey("key");
        data1.setLabel("label");
        data1.setDescription("description");
        data1.setRequired(true);
        data1.setTestStepDataId(1);
        data1.setFileName("fileName");
        data1.setUserId("userId");
        data1.setLastUpdate("date");

        AutomatedTestStepInstanceData data2 = new AutomatedTestStepInstanceData(1, 1, "key", "label", "description", true);
        data2.setTestStepDataId(1);
        data2.setFileName("fileName");
        data2.setUserId("userId");
        data2.setLastUpdate("date");
        assertEquals(data1, data2);
    }
}
