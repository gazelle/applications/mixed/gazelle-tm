package net.ihe.gazelle.tm.gazelletest.action;

import org.junit.Test;

public class TestInstanceNotFoundExceptionTest {


    /**
     * Dummy Test for coverage
     *
     * @throws TestInstanceNotFoundException testException
     */
    @Test(expected = TestInstanceNotFoundException.class)
    public void generateException() throws TestInstanceNotFoundException {
        throw new TestInstanceNotFoundException("Test");
    }

    /**
     * Dummy Test for coverage
     *
     * @throws TestInstanceNotFoundException testException
     */
    @Test(expected = TestInstanceNotFoundException.class)
    public void generateException1() throws TestInstanceNotFoundException {
        throw new TestInstanceNotFoundException("Test", new IllegalArgumentException("test"));
    }

    /**
     * Dummy Test for coverage
     *
     * @throws TestInstanceNotFoundException testException
     */
    @Test(expected = TestInstanceNotFoundException.class)
    public void generateException2() throws TestInstanceNotFoundException {
        throw new TestInstanceNotFoundException(new IllegalArgumentException("test"));
    }

    /**
     * Dummy Test for coverage
     *
     * @throws TestInstanceNotFoundException testException
     */
    @Test(expected = TestInstanceNotFoundException.class)
    public void generateException3() throws TestInstanceNotFoundException {
        throw new TestInstanceNotFoundException();
    }
}