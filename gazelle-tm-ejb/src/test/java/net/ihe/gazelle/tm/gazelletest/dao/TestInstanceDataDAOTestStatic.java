package net.ihe.gazelle.tm.gazelletest.dao;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManagerImpl;
import net.ihe.gazelle.tm.gazelletest.action.TestInstanceNotFoundException;
import net.ihe.gazelle.tm.gazelletest.model.instance.DataType;
import net.ihe.gazelle.tm.gazelletest.model.instance.Status;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstance;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestStepsData;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import javax.persistence.EntityManager;
import java.util.Date;

@RunWith(PowerMockRunner.class)
@PrepareForTest({DataType.class, EntityManager.class,ApplicationPreferenceManager.class})
public class TestInstanceDataDAOTestStatic {

    @Mock
    private EntityManager entityManager;
    @Mock
    private TestInstance testInstanceMock;

    @Mock
    private ApplicationPreferenceManager applicationPreferenceManager;
    @Mock
    private TestStepsData testStepsDataMock;
    @Mock
    private TestInstanceDataDAO testInstanceDataDAO;


    @Before
    public void setUp() {
        applicationPreferenceManager = Mockito.mock(ApplicationPreferenceManager.class);
        entityManager = Mockito.mock(EntityManager.class);
        testInstanceMock = Mockito.mock(TestInstance.class);
        Mockito.when(testInstanceMock.getId()).thenReturn(1);
        Mockito.when(testInstanceMock.getLastModifierId()).thenReturn("user2");
        testStepsDataMock = new TestStepsData();
        testStepsDataMock.setLastModifierId("user");
        testStepsDataMock.setLastChanged(new Date());
        testStepsDataMock.setValue("test");
        testStepsDataMock.setDataType(new DataType(1, "file", "file uploaded by user"));
        testStepsDataMock.setComment("");
        testStepsDataMock.setWithoutFileName(true);
        testInstanceDataDAO = new TestInstanceDataDAOImpl(entityManager);
    }

    @Test
    public void updateTestInstanceTest() {
        Mockito.when(entityManager.merge(testInstanceMock)).thenReturn(testInstanceMock);
        Mockito.doNothing().when(entityManager).flush();
        TestInstance testInstance = testInstanceDataDAO.updateTestInstance(testInstanceMock);
        Assert.assertEquals("Id shall be identical", testInstance.getId(), testInstanceMock.getId());

    }

    @Test
    public void getTestInstance() throws TestInstanceNotFoundException {
        Mockito.when(entityManager.find(TestInstance.class,testInstanceMock.getId())).thenReturn(testInstanceMock);
        TestInstance testInstance = testInstanceDataDAO.getTestInstance(testInstanceMock.getId());
        Assert.assertEquals("Id shall be identical", testInstance.getId(), testInstanceMock.getId());
    }

    @Test(expected = TestInstanceNotFoundException.class)
    public void getTestInstanceWithNullId() throws TestInstanceNotFoundException {
        testInstanceDataDAO.getTestInstance(null);
    }


    @Test(expected = TestInstanceNotFoundException.class)
    public void getTestInstanceWithIdNoTestInstance() throws TestInstanceNotFoundException {
        testInstanceDataDAO.getTestInstance(0);
    }

    @Test
    public void updateTestInstanceAndAddStepData() throws TestInstanceNotFoundException {
        Mockito.when(entityManager.find(TestInstance.class,testInstanceMock.getId())).thenReturn((testInstanceMock));
        Mockito.when(entityManager.merge(testInstanceMock)).thenReturn(testInstanceMock);
        Mockito.doNothing().when(entityManager).flush();
        Mockito.doNothing().when(testInstanceMock).setLastStatus(Status.COMPLETED);

        testInstanceDataDAO.updateTestInstanceAndAddTestStepData(testStepsDataMock,testInstanceMock.getId(),"user2");
        Assert.assertEquals("Last modifier shall be : user2","user2",testInstanceMock.getLastModifierId());


    }



    @Test
    public void updateTestInstanceAndTestStepDataForLogs() throws TestInstanceNotFoundException {
        Mockito.when(entityManager.find(TestInstance.class,testInstanceMock.getId())).thenReturn((testInstanceMock));
        Mockito.when(entityManager.merge(testInstanceMock)).thenReturn(testInstanceMock);
        Mockito.doNothing().when(entityManager).flush();

        testInstanceDataDAO.updateTestInstanceAndTestStepDataForLogs(testStepsDataMock,testInstanceMock.getId(),"user2");
        Assert.assertEquals("Last modifier shall be : user2","user2",testInstanceMock.getLastModifierId());

    }

    @Test(expected = IllegalArgumentException.class)
    public void getPathException(){
        testInstanceDataDAO.getPath(null);
    }

    @Ignore
    @Test
    public void getPath(){
        PowerMockito.mockStatic(ApplicationPreferenceManager.class);
        Mockito.when(applicationPreferenceManager.getGazelleDataPath()).thenReturn("/GazellePath");
        Mockito.when(applicationPreferenceManager.getStringValue("file_steps_path")).thenReturn("filePath");
        String path  = testInstanceDataDAO.getPath("1");
        Assert.assertEquals("Path shall be : /GazellePath/filePath/1","/GazellePath/filePath/1",path);

    }




}
