package net.ihe.gazelle.tm.gazelletest.action;

import org.junit.Test;

import static org.junit.Assert.*;

public class TestInstanceTokenSavingExceptionTest {


    /**
     * Dummy Test for coverage
     * @throws TestInstanceTokenSavingException testException
     */
    @Test(expected = TestInstanceTokenSavingException.class)
    public void generateException() throws TestInstanceTokenSavingException {
        throw new TestInstanceTokenSavingException("Test");
    }
    /**
     * Dummy Test for coverage
     * @throws TestInstanceTokenSavingException testException
     */
    @Test(expected = TestInstanceTokenSavingException.class)
    public void generateException1() throws TestInstanceTokenSavingException {
        throw new TestInstanceTokenSavingException("Test",new IllegalArgumentException("test"));
    }

    /**
     * Dummy Test for coverage
     *
     * @throws TestInstanceTokenSavingException testException
     */
    @Test(expected = TestInstanceTokenSavingException.class)
    public void generateException2() throws TestInstanceTokenSavingException {
        throw new TestInstanceTokenSavingException( new IllegalArgumentException("test"));
    }

    /**
     * Dummy Test for coverage
     *
     * @throws TestInstanceTokenSavingException testException
     */
    @Test(expected = TestInstanceTokenSavingException.class)
    public void generateException3() throws TestInstanceTokenSavingException {
        throw new TestInstanceTokenSavingException( );
    }



}