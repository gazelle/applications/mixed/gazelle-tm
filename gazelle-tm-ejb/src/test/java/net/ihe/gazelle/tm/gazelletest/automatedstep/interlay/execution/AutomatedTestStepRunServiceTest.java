package net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.execution;

import net.ihe.gazelle.tm.gazelletest.action.TestInstanceTokenService;
import net.ihe.gazelle.tm.gazelletest.automatedstep.domain.instance.AutomatedTestStepInstanceData;
import net.ihe.gazelle.tm.gazelletest.automatedstep.domain.script.*;
import net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.execution.service.AutomatedTestStepRunService;
import net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.execution.service.AutomatedTestStepRunServiceImpl;
import net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.execution.service.NotRunningException;
import net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.execution.service.StepRunServiceException;
import net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.instance.service.AutomatedTestStepInstanceInputService;
import net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.instance.service.AutomatedTestStepInstanceInputServiceImpl;
import net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.script.validation.ScriptValidationServiceImpl;
import net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.stub.*;
import net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.testdesign.service.AutomatedTestStepService;
import net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.testdesign.service.AutomatedTestStepServiceImpl;
import net.ihe.gazelle.tm.gazelletest.domain.AutomatedTestStepExecution;
import net.ihe.gazelle.tm.gazelletest.domain.AutomatedTestStepExecutionStatus;
import net.ihe.gazelle.tm.gazelletest.domain.AutomatedTestStepInputDomain;
import net.ihe.gazelle.tm.gazelletest.model.definition.AutomatedTestStepEntity;
import net.ihe.gazelle.tm.gazelletest.model.definition.AutomatedTestStepInput;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstanceToken;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestStepsInstance;
import org.junit.Before;
import org.junit.Test;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static org.testng.AssertJUnit.*;

public class AutomatedTestStepRunServiceTest {

    private static final String USER_ID = "userId";
    private static GazelleIdentityStub identity;
    private static final int TEST_STEP_ID = 1;
    private static final int TEST_STEP_INSTANCE_ID = 0;
    private static final int AUTOMATED_INPUT_INSTANCE_ID = 0;

    private TestInstanceTokenDAOStub testInstanceTokenDAOStub;
    private TestInstanceTokenService tokenService;
    private AutomatedTestStepService automatedTestStepService;
    private AutomatedTestStepExecutionDAOStub automatedExecutionDAOStub;
    private AutomatedTestStepInstanceInputService inputDataService;
    private AutomatedTestStepRunService service;

    @Before
    public void init() {
        identity = new GazelleIdentityStub();
        identity.setUserId(USER_ID);
        testInstanceTokenDAOStub = new TestInstanceTokenDAOStub();
        tokenService = new TestInstanceTokenService(testInstanceTokenDAOStub);
        TestInstanceDataDAOStub testInstanceDataDAOStub = new TestInstanceDataDAOStub();
        TestStepScriptDAOStub testStepScriptDAO = new TestStepScriptDAOStub();
        AutomatedTestStepDAOStub automatedTestStepDAO = new AutomatedTestStepDAOStub();
        automatedTestStepService = new AutomatedTestStepServiceImpl(testStepScriptDAO, automatedTestStepDAO, new ScriptValidationServiceImpl());
        automatedExecutionDAOStub = new AutomatedTestStepExecutionDAOStub();
        AutomatedTestStepInstanceInputDAOStub automatedTestStepInstanceInputDAOStub = new AutomatedTestStepInstanceInputDAOStub();
        AutomatedTestStepInstanceFileSystemDAOStub automatedTestStepInstanceFileSystemDAOStub = new AutomatedTestStepInstanceFileSystemDAOStub();
        inputDataService = new AutomatedTestStepInstanceInputServiceImpl(testInstanceDataDAOStub, automatedTestStepInstanceInputDAOStub, automatedTestStepInstanceFileSystemDAOStub);
        service = new AutomatedTestStepRunServiceImpl(new MaestroRestClientStub(), tokenService, testInstanceDataDAOStub, automatedTestStepService, automatedExecutionDAOStub, inputDataService);
    }

    @Test
    public void test_run() throws StepRunServiceException {
        // Given
        AutomatedTestStepEntity automatedTestStepEntity = new AutomatedTestStepEntity();
        automatedTestStepEntity.setTestScriptID("testScriptID");
        automatedTestStepEntity.setId(TEST_STEP_ID);
        List<AutomatedTestStepInput> automatedTestStepInputs = new ArrayList<>();
        automatedTestStepInputs.add(new AutomatedTestStepInput("key", "label", "description", "type", true, automatedTestStepEntity));
        automatedTestStepEntity.setInputs(automatedTestStepInputs);
        AutomatedTestStepInputDomain domain = new AutomatedTestStepInputDomain("key", "label", "description", "type", true);
        inputDataService.createAutomatedTestStepInstanceInput(domain, TEST_STEP_INSTANCE_ID);
        inputDataService.saveInputData(AUTOMATED_INPUT_INSTANCE_ID, TEST_STEP_INSTANCE_ID, "fileName", "fileContent");
        automatedTestStepService.createTestStep(automatedTestStepEntity.asDomain(), buildScript());
        TestStepsInstance testStepsInstance = new TestStepsInstance();
        testStepsInstance.setId(TEST_STEP_INSTANCE_ID);
        testStepsInstance.setTestSteps(automatedTestStepEntity);

        // When
        assertEquals(0, automatedExecutionDAOStub.getAutomatedTestStepExecutions().size());
        service.run(identity, testStepsInstance);

        // Then
        assertEquals(1, automatedExecutionDAOStub.getAutomatedTestStepExecutions().size());
        assertEquals(AutomatedTestStepExecutionStatus.RUNNING, automatedExecutionDAOStub.getAutomatedTestStepExecutions().get(0).getStatus());
        assertEquals(1, testInstanceTokenDAOStub.getAllTokens().size());
        TestInstanceToken token = testInstanceTokenDAOStub.getTestInstanceToken(USER_ID, TEST_STEP_INSTANCE_ID);
        assertTrue(tokenService.isTokenValid(token));
    }

    @Test
    public void test_already_running_execution() {
        // Given
        AutomatedTestStepExecution execution = new AutomatedTestStepExecution(TEST_STEP_INSTANCE_ID, "userId", AutomatedTestStepExecutionStatus.RUNNING, new Date(), "value");
        automatedExecutionDAOStub.createExecution(execution);
        assertTrue(service.isStepRunning(execution));
        TestStepsInstance testStepsInstance = new TestStepsInstance();
        testStepsInstance.setId(TEST_STEP_INSTANCE_ID);
        assertEquals(1, automatedExecutionDAOStub.getAutomatedTestStepExecutions().size());

        // When
        try {
            service.run(identity, testStepsInstance);
            fail();
        } catch (StepRunServiceException e) {
            // Then
            assertEquals("Step was already running", e.getMessage());
            assertEquals(1, automatedExecutionDAOStub.getAutomatedTestStepExecutions().size());
            assertEquals(AutomatedTestStepExecutionStatus.RUNNING, automatedExecutionDAOStub.getAutomatedTestStepExecutions().get(0).getStatus());
        }
    }

    @Test
    public void test_expired_execution() {
        // Given
        AutomatedTestStepEntity automatedTestStepEntity = new AutomatedTestStepEntity();
        automatedTestStepEntity.setTestScriptID("testScriptID");
        automatedTestStepEntity.setId(TEST_STEP_ID);
        List<AutomatedTestStepInput> automatedTestStepInputs = new ArrayList<>();
        automatedTestStepInputs.add(new AutomatedTestStepInput("key", "label", "description", "type", true, automatedTestStepEntity));
        automatedTestStepEntity.setInputs(automatedTestStepInputs);
        AutomatedTestStepInputDomain domain = new AutomatedTestStepInputDomain("key", "label", "description", "type", true);
        inputDataService.createAutomatedTestStepInstanceInput(domain, TEST_STEP_INSTANCE_ID);
        inputDataService.saveInputData(AUTOMATED_INPUT_INSTANCE_ID, TEST_STEP_INSTANCE_ID, "fileName", "fileContent");
        automatedTestStepService.createTestStep(automatedTestStepEntity.asDomain(), buildScript());
        TestStepsInstance testStepsInstance = new TestStepsInstance();
        testStepsInstance.setId(TEST_STEP_INSTANCE_ID);
        testStepsInstance.setTestSteps(automatedTestStepEntity);
        Timestamp expirationTime = new Timestamp(System.currentTimeMillis() - TimeUnit.HOURS.toMillis(1));
        AutomatedTestStepExecution execution = new AutomatedTestStepExecution(TEST_STEP_INSTANCE_ID, "userId", AutomatedTestStepExecutionStatus.RUNNING, expirationTime, "value");
        automatedExecutionDAOStub.createExecution(execution);
        tokenService.generateTokenWithHoursValidity(USER_ID, testStepsInstance, 1);
        assertEquals(1, testInstanceTokenDAOStub.getAllTokens().size());

        // When
        AutomatedTestStepExecution timeOutExecution = service.getExecution(TEST_STEP_INSTANCE_ID);

        // Then
        assertEquals(AutomatedTestStepExecutionStatus.TIMED_OUT, timeOutExecution.getStatus());
        assertEquals(0, testInstanceTokenDAOStub.getAllTokens().size());
    }

    @Test
    public void test_cancel_execution() {
        // Given
        AutomatedTestStepEntity automatedTestStepEntity = new AutomatedTestStepEntity();
        automatedTestStepEntity.setTestScriptID("testScriptID");
        automatedTestStepEntity.setId(TEST_STEP_ID);
        List<AutomatedTestStepInput> automatedTestStepInputs = new ArrayList<>();
        automatedTestStepInputs.add(new AutomatedTestStepInput("key", "label", "description", "type", true, automatedTestStepEntity));
        automatedTestStepEntity.setInputs(automatedTestStepInputs);
        AutomatedTestStepInputDomain domain = new AutomatedTestStepInputDomain("key", "label", "description", "type", true);
        inputDataService.createAutomatedTestStepInstanceInput(domain, TEST_STEP_INSTANCE_ID);
        inputDataService.saveInputData(AUTOMATED_INPUT_INSTANCE_ID, TEST_STEP_INSTANCE_ID, "fileName", "fileContent");
        automatedTestStepService.createTestStep(automatedTestStepEntity.asDomain(), buildScript());
        TestStepsInstance testStepsInstance = new TestStepsInstance();
        testStepsInstance.setId(TEST_STEP_INSTANCE_ID);
        testStepsInstance.setTestSteps(automatedTestStepEntity);
        Timestamp expirationTime = new Timestamp(System.currentTimeMillis() - TimeUnit.HOURS.toMillis(1));
        AutomatedTestStepExecution execution = new AutomatedTestStepExecution(TEST_STEP_INSTANCE_ID, "userId", AutomatedTestStepExecutionStatus.RUNNING, expirationTime, "value");
        automatedExecutionDAOStub.createExecution(execution);
        tokenService.generateTokenWithHoursValidity(USER_ID, testStepsInstance, 1);
        assertEquals(1, testInstanceTokenDAOStub.getAllTokens().size());

        // When
        service.cancelExecution(TEST_STEP_INSTANCE_ID);

        // Then
        assertEquals(0, testInstanceTokenDAOStub.getAllTokens().size());
        assertEquals(1, automatedExecutionDAOStub.getAutomatedTestStepExecutions().size());
        assertEquals(AutomatedTestStepExecutionStatus.DONE_UNDEFINED, automatedExecutionDAOStub.getAutomatedTestStepExecutions().get(0).getStatus());
    }

    @Test
    public void test_receive_execution() {
        // Given
        AutomatedTestStepEntity automatedTestStepEntity = new AutomatedTestStepEntity();
        automatedTestStepEntity.setTestScriptID("testScriptID");
        automatedTestStepEntity.setId(TEST_STEP_ID);
        List<AutomatedTestStepInput> automatedTestStepInputs = new ArrayList<>();
        automatedTestStepInputs.add(new AutomatedTestStepInput("key", "label", "description", "type", true, automatedTestStepEntity));
        automatedTestStepEntity.setInputs(automatedTestStepInputs);
        AutomatedTestStepInputDomain domain = new AutomatedTestStepInputDomain("key", "label", "description", "type", true);
        inputDataService.createAutomatedTestStepInstanceInput(domain, TEST_STEP_INSTANCE_ID);
        inputDataService.saveInputData(AUTOMATED_INPUT_INSTANCE_ID, TEST_STEP_INSTANCE_ID, "fileName", "fileContent");
        automatedTestStepService.createTestStep(automatedTestStepEntity.asDomain(), buildScript());
        TestStepsInstance testStepsInstance = new TestStepsInstance();
        testStepsInstance.setId(TEST_STEP_INSTANCE_ID);
        testStepsInstance.setTestSteps(automatedTestStepEntity);
        AutomatedTestStepExecution execution = new AutomatedTestStepExecution(TEST_STEP_INSTANCE_ID, "userId", AutomatedTestStepExecutionStatus.RUNNING, new Date(), "value");
        automatedExecutionDAOStub.createExecution(execution);
        tokenService.generateTokenWithHoursValidity(USER_ID, testStepsInstance, 1);
        assertEquals(1, testInstanceTokenDAOStub.getAllTokens().size());
        AutomatedTestStepExecution receivedExecution = new AutomatedTestStepExecution(TEST_STEP_INSTANCE_ID, "userId", AutomatedTestStepExecutionStatus.DONE_PASSED, new Date(), "value");
        // When
        service.receiveExecution(identity, receivedExecution);

        // Then
        assertEquals(1, automatedExecutionDAOStub.getAutomatedTestStepExecutions().size());
        assertEquals(AutomatedTestStepExecutionStatus.DONE_PASSED, automatedExecutionDAOStub.getAutomatedTestStepExecutions().get(0).getStatus());
    }

    @Test
    public void test_receive_not_running_execution() {
        // Given
        AutomatedTestStepEntity automatedTestStepEntity = new AutomatedTestStepEntity();
        automatedTestStepEntity.setTestScriptID("testScriptID");
        automatedTestStepEntity.setId(TEST_STEP_ID);
        List<AutomatedTestStepInput> automatedTestStepInputs = new ArrayList<>();
        automatedTestStepInputs.add(new AutomatedTestStepInput("key", "label", "description", "type", true, automatedTestStepEntity));
        automatedTestStepEntity.setInputs(automatedTestStepInputs);
        AutomatedTestStepInputDomain domain = new AutomatedTestStepInputDomain("key", "label", "description", "type", true);
        inputDataService.createAutomatedTestStepInstanceInput(domain, TEST_STEP_INSTANCE_ID);
        inputDataService.saveInputData(AUTOMATED_INPUT_INSTANCE_ID, TEST_STEP_INSTANCE_ID, "fileName", "fileContent");
        automatedTestStepService.createTestStep(automatedTestStepEntity.asDomain(), buildScript());
        TestStepsInstance testStepsInstance = new TestStepsInstance();
        testStepsInstance.setId(TEST_STEP_INSTANCE_ID);
        testStepsInstance.setTestSteps(automatedTestStepEntity);
        AutomatedTestStepExecution execution = new AutomatedTestStepExecution(TEST_STEP_INSTANCE_ID, "userId", AutomatedTestStepExecutionStatus.TIMED_OUT, new Date(), "value");
        automatedExecutionDAOStub.createExecution(execution);
        tokenService.generateTokenWithHoursValidity(USER_ID, testStepsInstance, 1);
        assertEquals(1, testInstanceTokenDAOStub.getAllTokens().size());
        AutomatedTestStepExecution receivedExecution = new AutomatedTestStepExecution(TEST_STEP_INSTANCE_ID, "userId", AutomatedTestStepExecutionStatus.DONE_PASSED, new Date(), "value");
        // When
        try {
            service.receiveExecution(identity, receivedExecution);
            fail();
        } catch (NotRunningException e) {
            // Then
            assertEquals("The step was not running anymore : status not updated", e.getMessage());
            assertEquals(1, automatedExecutionDAOStub.getAutomatedTestStepExecutions().size());
            assertEquals(AutomatedTestStepExecutionStatus.TIMED_OUT, automatedExecutionDAOStub.getAutomatedTestStepExecutions().get(0).getStatus());
        }
    }

    @Test
    public void test_getUpToDateStatusForAllInstanceInput() {
        // Given
        AutomatedTestStepEntity automatedTestStepEntity = new AutomatedTestStepEntity();
        automatedTestStepEntity.setTestScriptID("testScriptID");
        automatedTestStepEntity.setId(TEST_STEP_ID);
        List<AutomatedTestStepInput> automatedTestStepInputs = new ArrayList<>();
        automatedTestStepInputs.add(new AutomatedTestStepInput("key", "label", "description", "type", true, automatedTestStepEntity));
        automatedTestStepEntity.setInputs(automatedTestStepInputs);
        AutomatedTestStepInputDomain domain = new AutomatedTestStepInputDomain("key", "label", "description", "type", true);
        inputDataService.createAutomatedTestStepInstanceInput(domain, TEST_STEP_INSTANCE_ID);
        inputDataService.saveInputData(AUTOMATED_INPUT_INSTANCE_ID, TEST_STEP_INSTANCE_ID, "fileName", "fileContent");
        automatedTestStepService.createTestStep(automatedTestStepEntity.asDomain(), buildScript());
        TestStepsInstance testStepsInstance = new TestStepsInstance();
        testStepsInstance.setId(TEST_STEP_INSTANCE_ID);
        testStepsInstance.setTestSteps(automatedTestStepEntity);
        AutomatedTestStepExecution execution = new AutomatedTestStepExecution(TEST_STEP_INSTANCE_ID, "userId", AutomatedTestStepExecutionStatus.TIMED_OUT, new Date(), "value");
        automatedExecutionDAOStub.createExecution(execution);

        // When
        Map<AutomatedTestStepInstanceData, Boolean> outDatedStatus = service.getUpToDateStatusForAllInstanceInput(testStepsInstance);

        // Then
        assertEquals(1, outDatedStatus.size());
        AutomatedTestStepInstanceData data = outDatedStatus.keySet().iterator().next();
        assertFalse(outDatedStatus.get(data));

        execution = new AutomatedTestStepExecution(TEST_STEP_INSTANCE_ID, "userId", AutomatedTestStepExecutionStatus.DONE_PASSED, new Timestamp(System.currentTimeMillis() - TimeUnit.SECONDS.toMillis(1)), "value");
        automatedExecutionDAOStub.updateExecution(execution);
        outDatedStatus = service.getUpToDateStatusForAllInstanceInput(testStepsInstance);
        assertTrue(outDatedStatus.get(data));
    }

    @Test
    public void test_isRunOutDated() {
        // Given
        AutomatedTestStepEntity automatedTestStepEntity = new AutomatedTestStepEntity();
        automatedTestStepEntity.setTestScriptID("testScriptID");
        automatedTestStepEntity.setId(TEST_STEP_ID);
        List<AutomatedTestStepInput> automatedTestStepInputs = new ArrayList<>();
        automatedTestStepInputs.add(new AutomatedTestStepInput("key", "label", "description", "type", true, automatedTestStepEntity));
        automatedTestStepEntity.setInputs(automatedTestStepInputs);
        automatedTestStepService.createTestStep(automatedTestStepEntity.asDomain(), buildScript());
        TestStepsInstance testStepsInstance = new TestStepsInstance();
        testStepsInstance.setId(TEST_STEP_INSTANCE_ID);
        testStepsInstance.setTestSteps(automatedTestStepEntity);
        AutomatedTestStepInputDomain domain = new AutomatedTestStepInputDomain("key", "label", "description", "type", true);
        inputDataService.createAutomatedTestStepInstanceInput(domain, TEST_STEP_INSTANCE_ID);
        inputDataService.saveInputData(AUTOMATED_INPUT_INSTANCE_ID, TEST_STEP_INSTANCE_ID, "fileName", "fileContent");
        AutomatedTestStepExecution execution = new AutomatedTestStepExecution(TEST_STEP_INSTANCE_ID, "userId", AutomatedTestStepExecutionStatus.DONE_PASSED, new Timestamp(System.currentTimeMillis()), "value");
        automatedExecutionDAOStub.createExecution(execution);
        assertFalse(service.isRunOutDated(TEST_STEP_INSTANCE_ID));

        // When
        execution = new AutomatedTestStepExecution(TEST_STEP_INSTANCE_ID, "userId", AutomatedTestStepExecutionStatus.DONE_PASSED, new Timestamp(System.currentTimeMillis() - TimeUnit.SECONDS.toMillis(1)), "value");
        automatedExecutionDAOStub.updateExecution(execution);
        domain = new AutomatedTestStepInputDomain("key2", "label", "description", "type", true);
        inputDataService.createAutomatedTestStepInstanceInput(domain, TEST_STEP_INSTANCE_ID);
        inputDataService.saveInputData(AUTOMATED_INPUT_INSTANCE_ID, TEST_STEP_INSTANCE_ID, "fileName", "fileContent");

        // Then
        assertTrue(service.isRunOutDated(TEST_STEP_INSTANCE_ID));
    }

    private Script buildScript() {
        Script script = new Script();
        script.setName("name");
        List<InputDefinition> inputDefinitions = ScriptFactory.createInputAsList("key", "label", "description", "type", true);
        script.setInputs(inputDefinitions);
        List<Property> properties = ScriptFactory.createPropertyAsList("key", "value");
        List<Step> steps = ScriptFactory.createStepAsList("id", "type", properties);
        script.setSteps(steps);
        return script;
    }
}
