package net.ihe.gazelle.tm.gazelletest.ws;

import net.ihe.gazelle.tm.gazelletest.action.TestReportXSDValidator;
import net.ihe.gazelle.tm.gazelletest.action.XSDErrorHandler;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import javax.xml.transform.stream.StreamSource;
import java.io.*;

public class TestReportXSDValidationTest {

   private static final Logger LOG = LoggerFactory.getLogger(TestReportXSDValidationTest.class);

   static final String TEST_REPORT_XSD_LOCATION = "test-report/test-report.xsd";

   private static final String VALID_EXHAUSTIVE_REPORT = "externaltool/testreportsamples/validExhaustive.xml";
   private static final String VALID_MINIMAL_REPORT = "externaltool/testreportsamples/validMinimal.xml";
   private static final String VALID_MINIMAL_2_REPORT = "externaltool/testreportsamples/validMinimalWithDetailedResult.xml";

   private  TestReportXSDValidator validator;


   @Before
   public void initializeTest() throws SAXException {
      validator = new TestReportXSDValidator(TEST_REPORT_XSD_LOCATION);
   }

   @Test
   public void testValidExhaustiveTestReport() throws IOException, SAXException {
      StreamSource content = new StreamSource(TestReportXSDValidationTest.class.getClassLoader().getResourceAsStream(VALID_EXHAUSTIVE_REPORT));
      XSDErrorHandler errorHandler = validator.validateResource(content);
      Assert.assertFalse(String.format("The XSD Validation of the sample %s must not return errors", VALID_EXHAUSTIVE_REPORT),
            errorHandler.isErrors());
   }

   @Test
   public void testValidMinimalTestReport() throws IOException, SAXException {
      StreamSource content = new StreamSource(TestReportXSDValidationTest.class.getClassLoader().getResourceAsStream(VALID_MINIMAL_REPORT));
      XSDErrorHandler errorHandler = validator.validateResource(content);
      Assert.assertFalse(String.format("The XSD Validation of the sample %s must not return errors", VALID_MINIMAL_REPORT),
            errorHandler.isErrors());
   }

   @Test
   public void testValidMinimal2TestReport() throws IOException, SAXException {
      StreamSource content = new StreamSource(TestReportXSDValidationTest.class.getClassLoader().getResourceAsStream(VALID_MINIMAL_2_REPORT));
      XSDErrorHandler errorHandler = validator.validateResource(content);
      Assert.assertFalse(String.format("The XSD Validation of the sample %s must not return errors", VALID_MINIMAL_2_REPORT),
            errorHandler.isErrors());
   }

}
