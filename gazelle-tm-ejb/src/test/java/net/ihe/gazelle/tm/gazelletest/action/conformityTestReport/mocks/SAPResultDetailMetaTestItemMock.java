package net.ihe.gazelle.tm.gazelletest.action.conformityTestReport.mocks;

import net.ihe.gazelle.tm.gazelletest.bean.SAPResultDetailMetaTestItem;
import net.ihe.gazelle.tm.gazelletest.model.definition.MetaTest;
import net.ihe.gazelle.tm.gazelletest.model.definition.TestRoles;
import net.ihe.gazelle.tm.systems.model.TestingSession;

import java.util.ArrayList;
import java.util.List;

public class SAPResultDetailMetaTestItemMock extends SAPResultDetailMetaTestItem {
    private List<TestRoles> testRoles = new ArrayList<TestRoles>();

    public SAPResultDetailMetaTestItemMock(TestingSession testingSession, MetaTest metaTest) {
        super(testingSession, metaTest);
    }

    public void setTestRolesList(List<TestRoles> list) {
        this.testRoles = list;
    }

    @Override
    public List<TestRoles> getTestRolesList() {
        return testRoles;
    }
}
