/*
 * Copyright 2016 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.tm.gazelletest.action;

import net.ihe.gazelle.tm.gazelletest.model.instance.TestStepsData;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * <b>Class Description : </b>TestInstanceManagerTest<br>
 * <br>
 *
 * @author Jean-Francois Labbé / IHE-Europe development Project
 * @version 1.0 - 09/03/16
 * @class TestInstanceManagerTest
 * @package net.ihe.gazelle.tm.gazelletest.action
 * @see jean-francois.labbe@ihe-europe.net - http://gazelle.ihe.net
 */

public class TestInstanceManagerTest {

    @Mock
    private TestStepDataWrapper testStepDataWrapper;
    private TestStepsData testStepsData;
    private TestStepsData testStepsData2;
    private TestInstanceManager testInstanceManager;

    @Before
    public void setup() {
        testInstanceManager = new TestInstanceManager();
        testStepDataWrapper = Mockito.mock(TestStepDataWrapper.class);
        testStepsData = Mockito.mock(TestStepsData.class);
    }

    @Test
    public void canPerformRenderingShouldBeTrueTest() {
        testStepsData.setValue("test-report-5193b158-7aa2-11eb-9439-0242ac130002.xml");
        when(testStepDataWrapper.getTsd()).thenReturn(testStepsData);
        when(testStepDataWrapper.getTsd().getValue()).thenReturn("test-report-valid.xml");
        assertFalse("The file should contain the key word test-report and .xml", testInstanceManager.canPerformRendering(testStepDataWrapper));
    }

    @Test
    public void canPerformRenderingShouldBeFalse2Test() {
        testStepsData.setValue("test-report-5193b158-7aa2-11eb-9439.xml");
        when(testStepDataWrapper.getTsd()).thenReturn(testStepsData);
        when(testStepDataWrapper.getTsd().getValue()).thenReturn("test-report-valid.xml");
        assertFalse("The file should contain the key word test-report and .xml", testInstanceManager.canPerformRendering(testStepDataWrapper));
    }

    @Test
    public void canPerformRenderingShouldBeFalseTest() {
        testStepsData.setValue("invalidData.zip");
        when(testStepDataWrapper.getTsd()).thenReturn(testStepsData);
        when(testStepDataWrapper.getTsd().getValue()).thenReturn("invalidData.zip");
        assertFalse("The file should not contain the key word test-report and .xml", testInstanceManager.canPerformRendering(testStepDataWrapper));
    }

    @Test
    public void shouldRenderPassedTestShouldBeTrueTest() {
        when(testStepDataWrapper.getTsd()).thenReturn(testStepsData);
        when(testStepsData.getCompleteFilePath()).thenReturn("src/test/resources/externaltool/testreportsamples/validMinimal.xml");
        assertTrue("Status should be equal to passed", testInstanceManager.shouldRenderPassedTests(testStepDataWrapper));
    }

    @Test
    public void shouldRenderPassedTestShouldBeFalseTest() {
        when(testStepDataWrapper.getTsd()).thenReturn(testStepsData);
        when(testStepsData.getCompleteFilePath()).thenReturn("src/test/resources/externaltool/testreportsamples/invalidData.xml");
        assertFalse("Status should not be equal to passed",testInstanceManager.shouldRenderPassedTests(testStepDataWrapper));
    }

    @Test
    public void getAmountOfValidTestsValidTest() {
        when(testStepsData.getCompleteFilePath()).thenReturn("src/test/resources/externaltool/testreportsamples/validMinimal.xml");
        assertEquals(testInstanceManager.getAmountOfValidTests(testStepsData), "0 tests failed, 1 out of 2 tests passed");
    }

    @Test
    public void getAmountOfValidTestsNullTest() {
        when(testStepsData.getCompleteFilePath()).thenReturn("src/test/resources/externaltool/testreportsamples/invalidNoContent.xml");
        assertEquals(testInstanceManager.getAmountOfValidTests(testStepsData), "0 tests failed, 0 out of 0 tests passed");
    }

    @Test(expected = IllegalArgumentException.class)
    public void canPerformRenderingException(){
        testInstanceManager.canPerformRendering(null);
    }
    @Test(expected = IllegalArgumentException.class)
    public void canPerformRenderingExceptionTSD(){
        testInstanceManager.canPerformRendering(new TestStepDataWrapper(null));
    }
}