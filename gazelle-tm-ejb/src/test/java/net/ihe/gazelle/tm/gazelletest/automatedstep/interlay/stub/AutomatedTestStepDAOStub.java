package net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.stub;

import net.ihe.gazelle.tm.gazelletest.automatedstep.domain.script.InputDefinition;
import net.ihe.gazelle.tm.gazelletest.automatedstep.interlay.testdesign.dao.AutomatedTestStepDAO;
import net.ihe.gazelle.tm.gazelletest.domain.AutomatedTestStepInputDomain;
import net.ihe.gazelle.tm.gazelletest.domain.TestStepDomain;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class AutomatedTestStepDAOStub implements AutomatedTestStepDAO {

    private List<TestStepDomain> testStepDomains;
    private Integer idSequence;

    public AutomatedTestStepDAOStub() {
        this.testStepDomains = new ArrayList<>();
        this.idSequence = 1;
    }

    @Override
    public TestStepDomain createTestStep(TestStepDomain testStep) {
        testStep.setId(idSequence);
        idSequence++;
        testStepDomains.add(testStep);
        return testStep;
    }

    @Override
    public TestStepDomain editTestStep(TestStepDomain testStep, List<InputDefinition> inputDefinitions) {
        List<AutomatedTestStepInputDomain> inputDomains = new ArrayList<>();
        for (InputDefinition inputDefinition : inputDefinitions) {
            AutomatedTestStepInputDomain inputDomain = new AutomatedTestStepInputDomain(inputDefinition.getKey(), inputDefinition.getLabel(), inputDefinition.getFormat(), inputDefinition.getType(), inputDefinition.getRequired());
            inputDomains.add(inputDomain);
        }
        testStep.setInputs(inputDomains);
        for (TestStepDomain testStepDomain : testStepDomains) {
            if (testStep.getId().equals(testStepDomain.getId())) {
                testStepDomains.remove(testStepDomain);
                testStepDomains.add(testStep);
            }
        }
        return testStep;
    }

    @Override
    public void deleteTestStep(TestStepDomain testStep) {
        for (Iterator<TestStepDomain> it = testStepDomains.iterator(); it.hasNext();) {
            TestStepDomain domain = it.next();
            if (testStep.getId().equals(domain.getId())) {
                it.remove();
            }
        }
    }

    public List<TestStepDomain> getTestStepDomains() {
        return testStepDomains;
    }

    public Integer getIdSequence() {
        return idSequence;
    }
}
