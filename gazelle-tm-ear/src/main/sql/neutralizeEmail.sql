-- This script adds 'neutralized' at the begining and at the end of every 'email' field
-- in every known table that has such column (usr_user, usr_person, usr_connectathon_participant, tm_connectathon_participant)
-- The purpose of this is to have a clean database whose it is impossible to accidentally spam with

UPDATE usr_users SET email=concat('neutralized-', concat(email, 'neutralized'))  where position( 'neutralized-' in email) = 0;
UPDATE usr_person SET email=concat('neutralized-', concat(email, 'neutralized'))  where position( 'neutralized-' in email) = 0;
UPDATE tm_connectathon_participant SET email=concat('neutralized-', concat(email, 'neutralized'))  where position( 'neutralized-' in email) = 0;
UPDATE tm_testing_session SET contact_email='notanemail@test.com';
